module ApplicationHelper

  def get_current_vertical
    return @current_vertical if @current_vertical.present?
    current_vertical = VERTICAL_CONFIG[request.subdomain]

    @current_vertical = validate_vertical(current_vertical)
    return @current_vertical

  end

  #TODO:: Harsh review this alias_method
  alias_method :current_vertical, :get_current_vertical

  def validate_vertical(current_vertical)
    current_vertical = (current_vertical == "dataentry" ? "data_entry" : current_vertical)
    unless ["admin", "broker", "carrier", "data_entry", "employer"].include?(current_vertical)
      current_vertical = nil
    end
    return current_vertical
  end

  # Fetch list of unread notification
  # Author:: Vipala
  # Review::
  # Date::03/04/2015
  # TODO Move it to appropriate place
  def get_notifications_count
    notification_start_time = Time.now
    logger.debug("------ Notification code starts #{notification_start_time.to_s}")
    unread_notifications_count = 0
    return if get_current_user.nil?
    notification_service = NotificationService.new(get_current_user)
    unread_notifications_count = notification_service.get_unread_notifications_count
    notification_end_time = Time.now
    logger.debug("------ Notification code end #{notification_end_time.to_s}")
    time_taken = (notification_end_time - notification_start_time) rescue 0
    logger.debug("------ Notifications time taken: #{time_taken}")
    return unread_notifications_count
  end

  def authorize
    user = get_current_user
    if user.nil?
      redirect_to "/", :alert => "Please login to continue."
    else
      controller = params[:controller]
      action = params[:action]
      return true if can_access(user, controller, action)
      redirect_to "/", :alert => "You don't have the appropriate permissions to access the page."
    end
  end

  def file_icon_class(filename)
    extension = filename.to_s.split('.').last
    extension_icon_class_map = @extension_icon_class_map || file_extension_icon_class_map
    font_icon_class = extension_icon_class_map[extension]
    return font_icon_class
  end

  def file_extension_icon_class_map
    if @extension_icon_class_map.blank?
      default_file_icon_class = 'fa-file-o'
      @extension_icon_class_map = Hash.new(default_file_icon_class).merge(
                                  {'xls' => 'fa-file-excel-o',
                                  'xlsx' => 'fa-file-excel-o',
                                  'doc'  => 'fa-file-word-o',
                                  'docx' => 'fa-file-word-o',
                                  'pdf'  => 'fa-file-pdf-o',
                                  'csv'  => 'fa-file-o'})
    end
    return @extension_icon_class_map
  end

  def can_access(user, controller=nil, action=nil)
    user_roles_maps = user.roles.collect(&:role_map)
    user_roles_ids = user.roles.collect(&:id)
    return true if user_roles_maps.include?(Role::SUPER_ADMIN_ROLE_MAP) #role_by_role_maps[Role::SUPER_ADMIN_ROLE_MAP].present? && user_roles.include?(role_by_role_maps[Role::SUPER_ADMIN_ROLE_MAP].id)
    role_by_role_maps = Role.where(:vertical => user.company.vertical).index_by(&:role_map)
    return true if user.company.vertical.name.underscore == get_current_vertical &&
                   user_roles_ids.include?(role_by_role_maps[Role::COMPANY_ADMIN_ROLE_MAP].id)
    # get all roles who can access this route
    permission_roles = Permission.get_roles_by_route(user, controller, action)
    return (user_roles_ids & permission_roles).present?
    # permissions = Permission.user_permissions(user)
    # return permissions[controller][action].present? rescue false
  end

  # can access companies is special case where we need to check vertical
  def can_access_sidebar_companies(user, current_vertical)
    Permission.can_access_sidebar_companies(user, get_current_vertical)
  end

  def get_current_user
    return @current_user if @current_user.present?
    if get_current_vertical.present?
      @current_user = send("current_#{get_current_vertical}")
    end
    return @current_user
  end

  def set_current_vertical
    @current_vertical = VERTICAL_CONFIG[request.subdomain]
  end

  def hide_sidebar
    @toggled = true
  end

  def remove_namespace_from_anchors(pagination_html)
    new_html = pagination_html.gsub!("/#{get_current_vertical}/", "/")
    return "" if new_html.nil?
    return new_html
  end

  def log_request_start_time
    @request_starts_at = Time.now
    logger.debug("------ Request started at #{@request_starts_at.to_s}")
  end

  def log_request_end_time
    @request_ends_at = Time.now
    logger.debug("------ Request ended at #{@request_ends_at.to_s}")
    logger.debug("------ Request time taken #{@request_ends_at - @request_starts_at}")
  end

  def check_for_acting_employer
    user = get_current_user
    if user.type == "Admin" && cookies[:emp].nil? && params[:action] != "login_as"
      redirect_to "/login_as" and return
    elsif user.type == "Admin" && cookies[:emp].present?
      user.representing_company = Company.where(:vertical => Vertical.find_by_name("Employer")).where(id: cookies[:emp]).first
      user.save!
    end
  end

  def employee_benefit_types(employee)
    benefit_type_ids = employee.carrier_user_benefit_type_detail.collect(&:benefit_type_id)
    BenefitType.where(id: benefit_type_ids).collect(&:name).join(',')
  end

  def check_for_acting_carrier
    user = get_current_user
    if user.type == "Admin" && cookies[:carr].nil? && params[:action] != "login_as"
      redirect_to "/login_as" and return
    elsif user.type == "Admin" && cookies[:carr].present?
      user.representing_company = Company.where(:vertical => Vertical.find_by_name("Carrier")).where(id: cookies[:carr]).first
      user.save!
    end
  end

  def date_in_mdy_format(date)
    date.strftime("%m/%d/%Y") rescue nil
  end

  def is_it_support_user(user)
    # Assumption user has only one role.
    (Role::IT_SUPPORT_ROLE_MAP).include? user.roles.first.role_map rescue false
  end

  def number_with_subscript(num, is_currency=false, precision: 2)
    if is_currency
      num = number_to_currency(num.to_f, precision: precision)
    else
      num = number_with_delimiter(number_with_precision(num.to_f, precision: precision))
    end
    arr = num.to_s.split(".")
    return "<span class='subscript_num'>#{arr[0]}<sub>.#{arr[1]}</sub></span>".html_safe
  end

  def custom_number_formatter(number)
    # allow only digits and dot (.) minus (-) sign exclude all other chars
    return 0 if number.blank?
    cleaned_number = number.scan(/[0-9.-]/).join
    return cleaned_number.to_f
  end


  def get_subdomain_for_vertical(vertical)
    env_prefix = case Rails.env
                   when 'staging'
                     "stg-"
                   when 'development'
                     "dev-"
                   when 'prestage'
                     "dev-"
                   when 'production'
                     ""
                 end

    return "#{env_prefix}#{vertical}"
  end

  def get_data_errors_page(data_errors)
    index, page_size, data_errors_size = 0, Census::METADATA_PER_PAGE, data_errors.size
    error_page = []
    while(index < data_errors_size)
      end_index = index + page_size
      end_index = data_errors_size if end_index > data_errors_size
      error_arr = data_errors[index..end_index]
      errors = error_arr.reject(&:blank?)
      if errors.present?
        page_no = index/page_size
        error_page << (page_no + 1)
      end
      index = index + page_size
    end
    return error_page
  end

  def get_employee_status_filters
    ['All', 'Terminated', 'Active']
  end
end

