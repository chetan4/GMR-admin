module Admin::BaseHelper

  def remove_representing_company
    user = get_current_user
    user.representing_company = nil
    user.save!
  end
end
