module Admin::Api::V1::BaseHelper

  def allow_dataentry
    headers['Access-Control-Allow-Origin'] = "#{request.protocol}#{get_subdomain_for_vertical('dataentry')}.#{request.domain}"
    headers['Access-Control-Request-Method'] = "GET, OPTIONS, HEAD"
    headers['Access-Control-Allow-Headers'] = "x-requested-with,Content-Type, Authorization"
  end

  def get_subdomain_for_vertical(vertical)
    env_prefix = case Rails.env
                   when 'staging'
                     "stg-"
                   when 'development'
                     "dev-"
                   when 'prestage'
                     "dev-"
                   when 'production'
                     ""
                 end

    return "#{env_prefix}#{vertical}"
  end
end
