module Admin::TemplatesHelper

  def get_template
    @template = Template.find_by(id: params[:id])
    redirect_to "/templates" and return if @template.nil?
  end

  def get_benefit_type
    @benefit_type = @template.benefit_type
  end

  def get_template_service
    @template_service = TemplateService.new(@benefit_type, get_current_user, @template, cookies[:tlid])
  end

  def is_template_editable?
    error = ""
    redirect_to "/templates", alert: "Template could not be found." and return if @template_service.template_deleted?
    if @template_service.template_edit_allowed?
      @editable = true
      cookies[:tlid] = {value: @template_service.get_locking_uuid, expires: @template_service.get_locking_expiry}
    elsif @template_service.is_template_published?
      if @template_service.get_template.delete_allowed?
        error = "Template cannot be edited when published. Please create a new version and then delete this version if needed."
      else
        error = "Template cannot be edited when published. Please create a new version."
      end
    elsif @template_service.is_locked_by_current_user?
      error = "This template is being edited by you in another session, please close the tab or wait till #{@template_service.get_locking_expiry}."
    else
      locked_by = @template_service.template.locked_by.name rescue "another user"
      error = "This template is being edited by #{locked_by} till #{@template_service.get_locking_expiry}."
    end

    if error.present?
      if request.xhr? == 0
        render :text => error, status: 500 and return
      else
        flash.now.alert = error
      end
    end
  end

  def create_input(template, template_sheet, template_section, template_field, template_column, template_input, value, master_data)
    properties = template_input.properties
    value = value.blank? ? "" : value
    if template_input.empty?
      return ""
    elsif template_input.input?
      return create_text_field(template, template_sheet, template_section, template_field, template_column, template_input, value, master_data)
    elsif template_input.dropdown?
      return create_dropdown(template, template_sheet, template_section, template_field, template_column, template_input, value, master_data)
    end
  end

  def create_text_field(template, template_sheet, template_section, template_field, template_column, template_input, value, master_data)
    is_disabled = false
    if !template_field.is_disabled? && template_input.is_disabled?
      is_disabled = true
    elsif template_field.is_disabled? && template_input.is_disabled?
      is_disabled = true
    end

    disabled = is_disabled || is_sheet_disabled?(template_sheet, master_data)
    alreadyDisabled = template_input.is_disabled?
    required = template_input.is_mandatory? && !template_input.is_disabled?
    name = build_name(template, template_sheet, template_section, template_field, template_column, template_input)
    return text_field_tag("", nil, {name: name, type: template_input.textbox_type, disabled: disabled, alreadyDisabled: alreadyDisabled,
                                    required: required, class: "form-control", value: value}.merge(template_input.get_restrictions))
  end

  def create_dropdown(template, template_sheet, template_section, template_field, template_column, template_input, value, master_data)
    dropdown_options = template_input.dropdown_options
    insert_blank_value_in_drop_down(template_input, dropdown_options)
    disabled = template_input.is_disabled? || template_field.is_disabled? || is_sheet_disabled?(template_sheet, master_data)
    required = template_input.is_mandatory?
    multiple = template_input.is_multiple_dropdown?
    cls = multiple ? "multi-select-tpl-field" : "";

    name = build_name(template, template_sheet, template_section, template_field, template_column, template_input)
    return "<span class='custom-dropdown custom-dropdown--black custom-dropdown--small "+cls+"'>" + select_tag("", options_for_select(dropdown_options, value), {name: name, class: "form-control#{multiple ? ' template-multi-select' : ''}", disabled: disabled, required: required, multiple: multiple}) + "</span>"
  end

  def build_name(template, template_sheet, template_section, template_field, template_column, template_input)
    name = "#{template.benefit_type.name.parameterize}"
    [template_sheet, template_section, template_field, template_column].each do |object|
      next if object.nil? || object.name.blank?
      name = "#{name}[#{object.name.parameterize}]"
    end

    name = "#{name}[]" if template_input.present? && template_input.is_multiple_dropdown?
    return name
  end

  def is_sheet_disabled?(template_sheet, master_data)
    data = @template_service.extract_data(master_data, template_sheet)
    not_relevant_object = OpenStruct.new(:name => "not_relevant")
    return (@template_service.extract_data(data, not_relevant_object) == "true")
  end

  private

  def insert_blank_value_in_drop_down(template_input, dropdown_options)
    if !template_input.is_multiple_dropdown?
      dropdown_options.insert(0, "")
    end
  end

end
