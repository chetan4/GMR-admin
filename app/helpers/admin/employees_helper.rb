module Admin::EmployeesHelper

  def get_company
    @company = nil
    if params[:broker_id]
      vertical = Vertical.find_by_name("Broker")
      company_id = params[:broker_id]
    elsif  params[:carrier_id]
      vertical = Vertical.find_by_name("Carrier")
      company_id = params[:carrier_id]
    elsif  params[:data_entry_id]
      vertical = Vertical.find_by_name("DataEntry")
      company_id = params[:data_entry_id]
    elsif  params[:employer_id]
      vertical = Vertical.find_by_name("Employer")
      company_id = params[:employer_id]
    end

    @company = Company.where(:vertical => vertical, id: company_id).first
  end
end
