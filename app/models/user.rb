# == Schema Information
#
# Table name: users
#
#  id                        :integer          not null, primary key
#  email                     :string           default(""), not null
#  encrypted_password        :string           default("")
#  reset_password_token      :string
#  reset_password_sent_at    :datetime
#  remember_created_at       :datetime
#  sign_in_count             :integer          default(0), not null
#  current_sign_in_at        :datetime
#  last_sign_in_at           :datetime
#  current_sign_in_ip        :inet
#  last_sign_in_ip           :inet
#  type                      :string
#  company_id                :integer
#  created_at                :datetime
#  updated_at                :datetime
#  invitation_token          :string
#  invitation_created_at     :datetime
#  invitation_sent_at        :datetime
#  invitation_accepted_at    :datetime
#  invitation_limit          :integer
#  invited_by_id             :integer
#  invited_by_type           :string
#  invitations_count         :integer          default(0)
#  failed_attempts           :integer          default(0)
#  unlock_token              :string
#  locked_at                 :datetime
#  work_number               :string           not null
#  cell_number               :string           not null
#  name_prefix               :string(10)
#  work_extension            :string(6)
#  first_name                :string(30)       not null
#  middle_name               :string(30)
#  last_name                 :string(30)       not null
#  last_notification_read_at :datetime
#  representing_company_id   :integer
#  status                    :integer          default(0)
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_invitation_token      (invitation_token) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_unlock_token          (unlock_token) UNIQUE
#

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :async, :invitable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :lockable

  has_many :attachments, as: :attachable
  has_many :user_security_questions, dependent: :destroy
  has_many :user_roles, dependent: :destroy
  has_many :carrier_user_benefit_type_detail, dependent: :destroy
  has_many :notifications
  has_many :roles, :through => :user_roles
  has_many :user_roles
  # has_many :roles, -> (object) {
  #   where("roles.vertical_id = ?", Vertical.find_by(name: object.type).id)
  # }, :through => :user_roles

  belongs_to :company
  belongs_to :representing_company, class_name: "Company"
  has_many :attachments, as: :attachable
  has_many :plan_document_requests
  has_many :assigned_tasks, class_name: 'DataEntryTask', foreign_key: 'assigned_to_id'
  has_many :task_assignee, class_name: 'DataEntryTask', foreign_key: 'assigned_by_id'
  has_many :task_operated_by, class_name: 'DataEntryTask', foreign_key: 'last_operated_by_id'
  has_many :task_reviewed_by, class_name: 'DataEntryTask', foreign_key: 'last_reviewed_by_id'
  has_many :attachments_tagged, class_name: 'DataEntryTaskAttachment', foreign_key: 'tagged_by_id'
  has_many :employee_associations
  has_many :initiated_quote_requests, class_name: 'QuoteRequest', foreign_key: 'initiator_id'
  has_many :received_quote_requests, class_name: 'QuoteRequest', foreign_key: 'recipient_id'
  has_many :conversations, class_name: 'Conversation', foreign_key: 'created_by_id'
  has_many :conversation_messages, class_name: 'ConversationMessage', foreign_key: 'created_by_id'

  validate :password_complexity
  before_create :set_last_notification_read_at
  enum file_sub_type: [:profile_pic, :profile_pic_thumbnail]
  enum status: { activate: 0, deactivate: 1 }

  scope :by_name, ->(name) {
    where("CONCAT(first_name,' ',middle_name,' ',last_name) ilike (?) or
      first_name ilike (?) or middle_name ilike (?) or last_name ilike(?)",
      "%#{name}%", "%#{name}%", "%#{name}%","%#{name}%")
  }

  PREFIXES = ["Mr.", "Mrs.", "Ms."].freeze

  FILE_SUB_TYPE_DETAILS = {
    :profile_pic => {
        :width => nil,
        :height => nil,
    },
    :profile_pic_thumbnail => {
        :width => 43,
        :height => 43,
    }
  }.freeze

  def active_for_authentication?
    super && self.activate?
  end

  def self.only
    return self
  end

  def self.gmr_admin_user
    where(email: "admin@gmr.com").first
  end

  def is_gmr_super_admin?
    super_admin_role_ids = Role.where(role_map: Role::SUPER_ADMIN_ROLE_MAP, vertical: Vertical.find_by_name(Vertical::AVAILABLE_VERTICALS_HASH[:admin])).ids
    (self.roles.ids & super_admin_role_ids).present?
  end

  def password_complexity
    if password.present? and not password.match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{6,20}$/)
      errors.add :password, "should contain 6 to 20 characters, one upper case, one lower case and one numeric."
    end
  end

  def assign_roles(role_maps)
    vertical = Vertical.find_by_name(self.type)
    roles = Role.get_roles(role_maps, vertical, self.company)
    begin
      self.roles << roles
    rescue ActiveRecord::RecordNotUnique => e
      env["airbrake.error_id"] = notify_airbrake(e)
    end
  end

  def full_work_number
    _work_number = "#{self.work_number}"
    _work_number = "#{_work_number} extn. #{self.work_extension}" if self.work_extension.present?
    return _work_number
  end

  def full_name
    return "#{self.name_prefix} #{self.name}"
  end

  def name
    _name = self.first_name
    _name = "#{_name} #{self.middle_name}" if self.middle_name.present?
    _name = "#{_name} #{self.last_name}"
    return _name
  end

  def get_profile_pic_url(path)
    "public/users/#{id}_profile_orig#{File.extname(path)}"
  end

  def get_profile_pic_thumbnail_url(path)
    "public/users/#{id}_profile_thumb#{File.extname(path)}"
  end

  def get_profile_pic_thumbnail_url_s3
    profile_pic_thumbnail = self.attachments.where(sub_type: User.file_sub_types[:profile_pic_thumbnail]).last
    if profile_pic_thumbnail.present?
      Util::AwsUtils.get_s3_url(profile_pic_thumbnail.name)
    else
      'profile-placeholder.png'
    end
  end

  def upload_profile_pic(file_to_upload)
    #upload original profile picture to s3
    original_filename = file_to_upload.original_filename
    file_params = {
        original_filename: original_filename,
        file_size: file_to_upload.size,
        url: get_profile_pic_url(file_to_upload.original_filename),
        sub_type: User.file_sub_types[:profile_pic]
    }
    resp = Attachment.upload_image(self, file_to_upload, file_params, {acl: 'public-read', original_filename: original_filename})
    return resp if resp[:status] == 'error'

    #upload thumbnail_profile pic to s3
    path = file_to_upload.tempfile.path
    begin
      width = User::FILE_SUB_TYPE_DETAILS[:profile_pic_thumbnail][:width]
      height = User::FILE_SUB_TYPE_DETAILS[:profile_pic_thumbnail][:height]
      thumbnail_image = Util::ImageUtils.resize_image(path, width, height)
    rescue InvalidFormat => e
      resp = {}
      resp[:status] = 'error'
      resp[:alert] = "Image upload has failed due to invalid format. Please re upload the image."
      return resp
    end
    profile_thumbnail_url = get_profile_pic_thumbnail_url(original_filename)
    profile_thumbnail_params = {original_filename: original_filename,
                                url: profile_thumbnail_url,
                                sub_type: User.file_sub_types[:profile_pic_thumbnail]}
    resp = Attachment.upload_image(self, thumbnail_image, profile_thumbnail_params, {acl: 'public-read', original_filename: original_filename})
    return resp
  end

  # Update last_notification_read_at timestamp with current time
  def update_last_seen_notification_timestamp
    self.update_attributes!(:last_notification_read_at => Time.now)
  end

  def get_associated_benefit_types
    benefit_types_ids = CarrierUserBenefitTypeDetail.where(user_id: self.id).pluck('benefit_type_id')
    BenefitType.where(id: benefit_types_ids).collect(&:name)
  end

  private
    def set_last_notification_read_at
      self.last_notification_read_at = read_attribute(:last_notification_read_at) || Time.now
    end
end
