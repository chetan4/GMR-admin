# == Schema Information
#
# Table name: user_roles
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  role_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_user_roles_on_user_id              (user_id)
#  index_user_roles_on_user_id_and_role_id  (user_id,role_id) UNIQUE
#

class UserRole < ActiveRecord::Base
  belongs_to :user
  belongs_to :role

  before_save :custom_validation

  private

    def custom_validation
      unless self.user.type == self.role.vertical.name
        errors.add(:vertical, 'User and Role do not belong to the same vertical.')
        return false
      end
      unless self.user.company.id == self.role.company_id || self.role.company.name == "default"
        errors.add(:company, 'User and Role do not belong to the same company.')
        return false
      end
      return true
    end
end
