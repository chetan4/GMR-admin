class Admin::DashboardsController < Admin::BaseController
  def show

  end

  def flush_cache
    Util::CacheUtils.flush_all
    render json: {
             status: 'All Application Cache Flushed, successfully. '
           }
  end
end