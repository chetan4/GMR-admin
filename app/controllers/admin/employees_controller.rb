class Admin::EmployeesController < EmployeesController

  before_action :authenticate_admin!
  before_filter :authorize
end
