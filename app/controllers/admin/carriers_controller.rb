class Admin::CarriersController < Admin::CompaniesController

  VERTICAL = Vertical.find_by_name("Carrier")

  def index
    @vertical = VERTICAL
    super
  end

  def new
    @vertical = VERTICAL
    super
  end

  def edit
    @vertical = VERTICAL
    super
  end
end
