class Admin::PlatformFilesController < Admin::BaseController

  layout false, :only => [:search]

  def index
    @filters = params[:filters] || {}
    if @filters[:carrier_company_id].present?
      @filters[:carrier_company] = Company.where(id: @filters[:carrier_company_id]).where(vertical: Vertical.find_by_name("Carrier")).first
    end
    if @filters[:benefit_type_id].present?
      @filters[:benefit_type] = BenefitType.where(id: @filters[:benefit_type_id]).first
    end
  end

  def create
    platform_file, errors = PlatformFileService.create(platform_file_params)
    if errors.any?
      redirect_to "/platform_files", alert: errors
    else
      redirect_to "/platform_files/#{platform_file.id}/edit"
    end
  end

  def edit
    @platform_file_service = PlatformFileService.new(PlatformFile.find(params[:id]), current_admin)
    @dataentry_show_hide_field = true
  end

  def update
    @platform_file_service = PlatformFileService.new(PlatformFile.find(params[:id]), current_admin)
    @platform_file_service.set_data(@platform_file_service.extract_data(params))
    redirect_to "/platform_files/#{@platform_file_service.platform_file.id}/edit", notice: "Platform File Saved"
  end

  def search
    @platform_files = PlatformFileService.search_by(params[:filters], params[:page])
  end

  private

  def platform_file_params
    params.require(:platform_file).permit(:name, :carrier_company_id, :benefit_type_id)
  end
end
