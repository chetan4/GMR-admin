class Admin::AuditLogsController < Admin::BaseController

  before_action :get_current_user

  def index
    params[:verticals] ||= []
    audit_log_service = AuditLogService.new(@current_user)
    @audit_logs, @audit_logs_by_day, errors = audit_log_service.audit_logs_by_day(params)
    flash.now.alert = errors
    @companies = {}
    params[:verticals].each do |vertical|
      next unless (params[:company].present? && params[:company][vertical].present? && params[:company][vertical][:id].present?)
      @companies[vertical] = Company.find(params[:company][vertical][:id])
    end
    @verticals_other_than_admin = Vertical.where("id != ?", 1)
  end
end
