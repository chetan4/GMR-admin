class Admin::Api::V1::BaseController < ActionController::Metal
  include AbstractController::Rendering
  include ActionController::Renderers::All
  include AbstractController::Callbacks
  include Admin::Api::V1::BaseHelper
end
