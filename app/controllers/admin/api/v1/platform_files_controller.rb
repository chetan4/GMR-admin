class Admin::Api::V1::PlatformFilesController < Admin::Api::V1::BaseController
  after_action :allow_dataentry, only: [:data]

  def data
    @platform_file_service = PlatformFileService.new(PlatformFile.find(params[:id]))
    render :json => @platform_file_service.data
  end
end
