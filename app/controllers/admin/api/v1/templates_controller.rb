class Admin::Api::V1::TemplatesController < Admin::Api::V1::BaseController
  after_action :allow_dataentry, only: [:fetch]

  def fetch
    benefit_type = BenefitType.find(params[:benefit_type_id])
    template_service = TemplateService.new(benefit_type)
    render :json => {:template_id => template_service.get_template.id}
  end
end
