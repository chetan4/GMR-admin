class Admin::CompaniesController < Admin::BaseController
  include ApplicationHelper

  before_action :authorize

  VERTICAL = Vertical.new(:id => 0, :name => "Default")

  def index
    @company_details = []
    if @vertical.nil?
      @company_details = Company.all
      @vertical = VERTICAL
    else
      @company_details = Company.where(vertical: @vertical)
    end
    @company_details = @company_details.order(:name).map { |c| c.company_blob }
  end

  def new
    @company = params[:company].present? ? Company.new(company_params.except(:industries)) : Company.new({vertical: @vertical})
    @employee = params[:employee].present? ? User.new(employee_params) : User.new
    get_benefit_types if @company.vertical.name == "Carrier"
  end

  def edit
    @company = Company.find(params[:id])
    @employee = @company.contact_person
    if @company.vertical.name == "Carrier"
      get_benefit_types
      @associated_benefit_types = @company.carrier_benefit_types
    end
  end

  def create
    company_service = CompanyService.new(current_admin)
    result = company_service.create_new(company_params, employee_params)
    company, employee, error = result[:company], result[:employee], result[:error]
    if error.nil?
      if params[:company][:icon].present?
        file_name = params[:company][:icon].original_filename
        if Attachment::ALLOWED_IMAGE_EXTENSIONS.include?(file_name.split('.').last.downcase)
          resp = company.upload_logo(params[:company][:icon])
          if resp[:status] == 'error'
            flash[:alert] = "Something went wrong in Image upload, please try again #{resp[:msg]}"
            redirect_to "/#{company.vertical.name.underscore.pluralize}/#{company.id}/edit" and return
          end
        else
          flash[:alert] = "Input file type, it must be of type either #{Attachment::ALLOWED_IMAGE_EXTENSIONS.join(', ')}"
          redirect_to "/#{company.vertical.name.underscore.pluralize}/#{company.id}/edit" and return
        end
      end
      redirect_to "/#{company.vertical.name.underscore.pluralize}", :company_id => company.id
    else
      redirect_to "#{request.referer.split(MAILER_HOST).second}?#{{:employee => employee_params,
                                         :company => company_params}.to_query}",
                  :alert => error
    end
  end

  def update
    company_service = CompanyService.new(current_admin)
    result = company_service.update_company(params[:id], company_params, employee_params)
    company, employee, error = result[:company], result[:employee], result[:error]

    if error.nil?
      if params[:company][:icon].present?
        file_name = params[:company][:icon].original_filename
        if Attachment::ALLOWED_IMAGE_EXTENSIONS.include?(file_name.split('.').last)
          resp = company.upload_logo(params[:company][:icon])
          if resp[:status] == 'error'
            flash[:alert] = "Something went wrong in Image upload, please try again #{resp[:msg]}"
            redirect_to "/#{company.vertical.name.underscore.pluralize}/#{company.id}/edit" and return
          end
        else
          flash[:alert] = "Input file type, it must be of type either #{Attachment::ALLOWED_IMAGE_EXTENSIONS.join(', ')}"
          redirect_to "/#{company.vertical.name.underscore.pluralize}/#{company.id}/edit" and return
        end
      end
      redirect_to "/#{company.vertical.name.underscore.pluralize}", notice: "Company updated successfully."
    else
      redirect_to "/#{company.vertical.name.underscore.pluralize}/#{company.id}/edit?#{{:employee => employee_params,
                                                                                        :company => company_params}.to_query}",
                  :alert => error
    end
  end

  private
  def company_params
    params.require(:company).permit(:name, :website, :vertical_id, :address, :note, :carrier_benefit_types_attributes => [:benefit_type_id], :industries => [])
  end

  def company_industries
    params.require(:company).permit(:industries => [])
  end

  def employee_params
    company_vertical_name = (Vertical.find_by(id: params[:company][:vertical_id]).name) rescue ''
    if company_vertical_name.blank? || (["Carrier", "Employer"].include? company_vertical_name)
      {}
    else
      params.require(:employee).permit(:name_prefix, :first_name, :middle_name, :last_name, :email, :work_number, :work_extension, :cell_number)
    end
  end

  def get_benefit_types
    @benefit_types = {}
      BenefitType.all.each do |bt|
        @benefit_types[bt.name] = bt.id
      end
      @associated_benefit_types = {}
    end
end
