class Admin::BaseController < ApplicationController
  # TODO:: verify the authentication message
  include Admin::BaseHelper

  before_action :authenticate_admin!
  before_filter :authorize
  before_action :remove_representing_company

end