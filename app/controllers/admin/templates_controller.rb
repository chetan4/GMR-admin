class Admin::TemplatesController < Admin::BaseController

  layout false, :only => [:add_sheet, :add_section, :add_field, :add_column, :add_input,
                          :edit_field, :edit_input, :edit_column, :edit_section,
                          :update_field, :update_input, :update_column, :update_field_order, :update_column_order,]

  include Admin::TemplatesHelper

  before_action :get_template, :get_benefit_type, :get_template_service,
                only: [:edit, :show,
                       :add_sheet, :add_section, :add_field, :add_column, :add_input,
                       :edit_field, :edit_column, :edit_input,
                       :update_field, :update_column, :update_input, :update_field_order, :update_column_order, :update_sheet_order,
                       :done_editing, :publish, :unpublish,
                       :delete, :delete_section, :delete_field, :delete_column]

  before_action :is_template_editable?,
                only: [:edit,
                       :add_sheet, :add_section, :add_field, :add_column, :add_input,
                       :edit_field, :edit_column, :edit_input,
                       :update_field, :update_column, :update_input, :update_field_order, :update_column_order, :update_sheet_order,
                       :done_editing, :publish, :unpublish,
                       :delete, :delete_section, :delete_field, :delete_column]

  def index
    @templates = Template.where("state != #{Template.states[:deleted]}").includes(:benefit_type, :locked_by, :data_entry_sub_tasks, :template_sheets).order(:benefit_type_id, :version, :id).page(params[:page]).per(20)
  end

  def new
    begin
      @benefit_type = BenefitType.find(params[:benefit_type_id])
      @template_service = TemplateService.new(@benefit_type, current_admin, nil, nil)
    rescue Exception => e
      env["airbrake.error_id"] = notify_airbrake(e)
      redirect_to "/templates", alert: "Please select valid a benefit type." and return
    end
  end

  def create
    @benefit_type = BenefitType.find(params[:benefit_type_id])
    template_service = TemplateService.new(@benefit_type, current_admin, nil, nil)
    @template = template_service.get_new_template()
    template_service.set_template(@template)

    if template_service.fetch_lock
      cookies[:tlid] = {value: template_service.get_locking_uuid, expires: template_service.get_locking_expiry}
    end
    if @template
      redirect_to "/templates/#{@template.id}/edit"
    else
      redirect_to "/templates", alert: "Template could not be created for some reason."
    end
  end

  def edit
    @template_edit = true
  end

  def done_editing
    @template_service.destroy_lock
    cookies.delete(:tlid)
    render :json => {:success => true}
  end

  def show
    render :edit
  end

  def add_sheet
    @template_sheet, errors = @template_service.add_sheet(template_sheet_params)
    if @template_sheet.nil?
      render text: errors.first, status: 500 and return
    end
  end

  def add_section
    @template_sheet, @template_section, errors = @template_service.add_section(template_section_params)
    if @template_section.nil?
      render text: errors.first, status: 500 and return
    end
  end

  def add_field
    @template_sheet, @template_section, @template_field, errors = @template_service.add_field(template_field_params)
    if @template_field.nil?
      render text: errors.first, status: 500 and return
    end
  end

  def add_column
    @template_sheet, @template_section, @template_column, errors = @template_service.add_column(template_column_params)
    if @template_column.nil?
      render text: errors.first, status: 500 and return
    end
  end

  def add_input
    @template_sheet, @template_section, @template_field, @template_column, @template_input, errors = @template_service.add_input(template_input_params)
    if @template_input.nil?
      render text: errors.first, status: 500 and return
    end
  end

  def edit_field
    @template_field = TemplateField.find(params[:field_id])
  end

  def edit_column
    @template_column = TemplateColumn.find(params[:column_id])
  end

  def edit_section
    @template_sheet = TemplateSheet.find(params[:sheet_id])
    @template_section = TemplateSection.find(params[:section_id])
  end

  def edit_input
    @template_input = TemplateInput.find(params[:input_id])
  end

  def update_field
    @template_field, errors = @template_service.update_field(update_field_params)
    if errors.any?
      render text: errors.first, status: 500 and return
    end
  end

  def update_field_attribute
    field = TemplateEntity.find(update_field_attribute_params[:field_id]) rescue nil
    render json: {success:  false, errors: "Could not find template field"} and return if field.blank?
    field.update(update_field_attribute_params[:template_entity_params])
    render json: {success: true, errors: nil}
    end

  def update_field_order
    @template_field, errors = @template_service.update_field_order(update_field_order_params)
    if errors.any?
      render :json => {:success => true}
    else
      render :json => {:success => false, errors => errors}
    end
  end

  def update_column
    @template_column, errors = @template_service.update_column(update_column_params)
    if errors.any?
      render text: errors.first, status: 500 and return
    end
  end

  def update_section
    template_section_name = params[:name]
    render json: {success: false, errors: "Template section name cannot be blank"} and return if template_section_name.blank?
    template_section = TemplateSection.find_by(id: params[:section_id], template_sheet_id: params[:sheet_id])
    render json: {success: false, errors: "Template section does not exist"} and return if template_section.blank?
    template = Template.find_by(id: params[:id])
    render json: {success: false, errors: "Template is not valid"} and return if template.blank?
    template_service = TemplateService.new(template.benefit_type)
    template_section_response = template_service.update_template_section_field(update_template_section_params, template_section)
    if template_section_response[:success]
      html = render_to_string(:partial => "section_content",
                              locals: { :edited_section_name => template_section_response[:data][:template_section_name],
                              })
      render json: {success: true, html: html}
    else
      render json: {success: false, errors: template_section_response[:errors]}
    end
  end

  def update_column_order
    @template_column, errors = @template_service.update_column_order(update_column_order_params)
    if errors.any?
      render :json => {:success => true}
    else
      render :json => {:success => false, errors => errors}
    end
  end

  def update_sheet_order
    response = @template_service.update_sheet_order(update_sheet_order_params)
    if response[:errors].any?
      render :json => {:success => true}
    else
      render :json => {:success => false, :errors => response[:errors]}
    end
  end

  def update_input
    @template_sheet, @template_section, @template_field, @template_column, @template_input, errors = @template_service.update_input(update_input_params)
    if errors.any?
      render text: errors.first, status: 500 and return
    end
  end

  def publish
    if @template_service.publish!
      redirect_to request.referrer, notice: "Template has been published."
    else
      redirect_to request.referrer, alert: @template_service.errors
    end
  end

  def unpublish
    if @template_service.unpublish!
      redirect_to request.referrer, notice: "Template has been unpublished."
    else
      redirect_to request.referrer, alert: @template_service.errors
    end
  end

  def delete
    if @template_service.delete!
      redirect_to "/templates", notice: "Template has been deleted."
    else
      redirect_to request.referrer, alert: @template_service.errors
    end
  end

  def delete_section
    status, errors = @template_service.delete_section(delete_section_params)
    render json: { success: status, errors: errors }
  end

  def delete_field
    status, errors = @template_service.delete_field(delete_field_params)
    render json: { success: status, errors: errors }
  end

  def delete_column
    status, errors = @template_service.delete_column(delete_column_params)
    render json: { success: status, errors: errors }
  end

  private

  def template_sheet_params
    params.require(:sheet).permit(:name)
  end

  def template_section_params
    params.require(:section).permit(:template_sheet_id, :name, :order)
  end

  def template_field_params
    params.require(:field).permit(:template_sheet_id, :template_section_id, :name, :order)
  end

  def template_column_params
    params.require(:column).permit(:template_sheet_id, :template_section_id, :name, :order)
  end

  def template_input_params
    params.require(:input).permit(:template_section_id, :template_field_id, :template_column_id)
  end

  def update_field_params
    params.permit(:section_id, :field_id, :name, :order).tap do |whitelisted|
      whitelisted[:properties] = params[:properties]
    end
  end

  def update_field_attribute_params
    params.permit(:field_id, [:template_entity_params => [:initial_marketing,:current_plans] ])
  end

  def update_field_order_params
    params.permit(:section_id, :field_id, :order)
  end

  def update_sheet_order_params
    params.require(:template_sheet_ids)
  end

  def update_column_params
    params.permit(:section_id, :column_id, :name, :order).tap do |whitelisted|
      whitelisted[:properties] = params[:properties]
    end
  end

  def update_column_order_params
    params.permit(:section_id, :column_id, :order)
  end

  def update_input_params
    params.permit(:section_id, :input_id, :input_type).tap do |whitelisted|
      whitelisted[:properties] = params[:properties]
    end
  end

  def delete_section_params
    params.permit(:template_section_id, :template_sheet_id)
  end

  def delete_field_params
    params.permit(:template_section_id, :template_sheet_id, :template_field_id)
  end

  def delete_column_params
    params.permit(:template_section_id, :template_sheet_id, :template_column_id)
  end

  def update_template_section_params
    params.permit(:section_id, :sheet_id, :name)
  end

end
