class HomeController < ApplicationController
  protect_from_forgery with: :exception

  layout "logged_out_application"
  skip_before_action :get_current_user

  def index

  end
end
