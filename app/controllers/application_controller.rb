class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  rescue_from ActionController::InvalidAuthenticityToken do
    flash[:notice] = 'Aw Snap!!. Please retry.'
    redirect_to request.referer || request.url
  end

  include ApplicationHelper
  include Auditable::MakeAuditable

  before_filter :configure_permitted_parameters, if: :devise_controller?


  before_action :get_current_user
  before_action :set_current_vertical
  before_action :session_expiry, unless: "Rails.env.development?"
  before_action :update_activity_time, unless: "Rails.env.development?"
  prepend_before_filter :log_request_start_time
  after_action :log_request_end_time



  def after_sign_out_path_for(resource)
    # if @current_vertical.nil?
    #   @current_vertical = get_current_vertical
    # end
    # if @current_vertical == "data_entry"
    #   "http://#{@current_vertical.delete("_")}.#{request.host}/data_entries/sign_in"
    # else
    #   "http://#{@current_vertical}.#{request.host}/#{@current_vertical.pluralize}/sign_in"
    # end
    # "/#{@current_vertical.pluralize}/sign_in"
    "//#{request.host}"
  end

  def after_sign_in_path_for(resource)
    "//#{request.host}"
  end

  def session_expiry
    get_session_time_left
    unless @session_time_left > 0
      sign_out
      if request.xhr?
        cookies.clear
        render js: %(window.location ='//#{request.host}';) and return
      end
    end
  end

  def update_activity_time
    if @current_user.present?
      session[:expires_at] = 60.minutes.from_now
    end
  end

  def get_session_time_left
    expire_time = session[:expires_at] || Time.now
    @session_time_left = (expire_time.to_time - Time.now).to_i
  end


  protected
  # Update user details for following params
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:name_prefix, :first_name, :middle_name,
                                                                   :last_name, :email, :work_number, :work_extension,
                                                                   :cell_number, :current_password, :password,
                                                                   :password_confirmation
    ) }
  end

end
