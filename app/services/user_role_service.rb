class UserRoleService

  def initialize(user)
    @user = user
  end

  def has_role?(role_map_array=[])
    (@user.roles.collect(&:role_map) & role_map_array.map(&:to_s)).present?
  end

  def has_any_role_other_than?(role_map_array=[])
    (@user.roles.collect(&:role_map) - role_map_array.map(&:to_s)).present?
  end
end