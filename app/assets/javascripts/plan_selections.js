// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

var PlanSelections = function (config) {
  var self = this;

  self.config = config;
};

PlanSelections.method("init", function () {
  var self = this;
  $(document).on('click', '#submitForFurther', function(){
      var parentEl = $(this).parent();
      parentEl.append(gmr.variables().loading_image());
      parentEl.find('.back-to-input').attr('disabled', 'disabled');
      $(this).attr('disabled', 'disabled');
      $("#summaryPageForm").submit();
  });

  $(document).on('click', '.btn_add_spinner', function(e){
      var parentEl = $(this).parent(),
          jForm = $(this).parents('form');
          e.preventDefault();

          if (gmr.helper().validateForm(jForm)) {
              $(window).unbind("beforeunload");
             jForm.submit();
             parentEl.append(gmr.variables().loading_image());
             $(this).attr('disabled', 'disabled');
          }
  });
});

PlanSelections.method("initProductSelection", function (action) {
  var self = this
    , jProductSelectionForm = $("form")
    , reflectCurrentState = function (jForm) {
      if (jForm.find("input[type=checkbox]:checked").length > 0) {
        jForm.find("input[type=submit]").prop("disabled", false);
      } else {
        jForm.find("input[type=submit]").prop("disabled", true);
      }
    };

  jProductSelectionForm.on("change", function (event) {
    reflectCurrentState($(this));
  });

  reflectCurrentState(jProductSelectionForm);
});

function planNameAllowedCharacters(jScope) {
  jScope = jScope || $(document);
  $(jScope).find(".plan_name_allowed_characters").on("keypress", function (evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if ((charCode <= 30) ||
        (charCode >= 48 && charCode <= 57) ||
        (charCode >= 65 && charCode <= 90) ||
        (charCode >= 97 && charCode <= 122) ||
        (charCode === 45) ||
        (charCode === 95) || (charCode === 32))
          {
            return true;
          } else {
            return false;
          }
        });
  $(jScope).find(".plan_name_allowed_characters")
    .on("change",function (event) {
      if (!/^[a-zA-Z0-9\-\_/\s/g]+$/.test($(this).val())) {
        $(this).val("");
        gmr.helper().showAlert('Please enter valid plan name.', 'error');
      }
  });
  $(jScope).find(".plan_name_allowed_characters")
    .on("paste",function (event) {
          var val = event.originalEvent.clipboardData.getData("text/plain");
          if (!/^[a-zA-Z0-9\-\_/\s/g]+$/.test(val)) {
            $(this).val("");
            gmr.helper().showAlert('Please enter valid plan name.', 'error');
          }
  });
}

function policyNumberAllowedCharacters(jScope1) {
  jScope1 = jScope1 || $(document);
  $(jScope1).find(".policy_number_allowed_characters").on("keypress", function (evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if ((charCode <= 30) ||
      (charCode >= 48 && charCode <= 57) ||
        (charCode >= 65 && charCode <= 90) ||
        (charCode >= 97 && charCode <= 122) ||
        (charCode === 32) || (charCode === 45))
          {
            return true;
          } else
          {
            return false;
          }
        });

  $(jScope1).find(".policy_number_allowed_characters")
    .on("change", function (event) {
      if (!/^[a-zA-Z0-9\-_ ]+$/.test($(this).val())) {
        $(this).val("");
      }
    });
  $(jScope1).find(".policy_number_allowed_characters")
    .on("paste", function (event) {
      var val = event.originalEvent.clipboardData.getData("text/plain");
      if (!/^[a-zA-Z0-9\-_ ]+$/.test(val)) {
        $(this).val("");
      }
    });
  }


PlanSelections.method("initInputForms", function (action) {
  var self = this;

  $('#summary_button').click(function () {
    var url = $(this).data("url")
      , params = $("#summary_form").serialize()
      , complete_url = url + "?" + params
      ;

    $("#summary_page").load(complete_url, function () {
      $("#summary_page").trigger("summaryLoaded");
    });
  });

  $("#summary_page").on("summaryLoaded", function () {

    $("#summary_page").show();
    $('.product_basket').hide();

    $(".back-to-input").on("click", function () {
      $("#summary_page").hide();
      $('.product_basket').show();
    });

    $(window).unbind("beforeunload");
  });

  $(".tab-content .tab-pane").on("tabLoaded", function () {
    var jTab = $(this)
      , jForm = jTab.find("form")
      , flag = true
      ;

    gmr.helper().initAjaxForm(jTab);
    gmr.helper().initMaskedFields(jTab);
    gmr.helper().initAllSelect2(jTab);
    gmr.helper().initLettersOnlyField();

    planNameAllowedCharacters();
    policyNumberAllowedCharacters();

    $('#years_with_carrier').on("change", function () {
        var jYearsWithCarrierField = $("form").find("[name='plan[years_with_carrier]']");

        if (jYearsWithCarrierField.val() === "1") {
          $('.disable-class').addClass("select2-container-disabled");
          $('.disable-class').css("pointer-events","none");
        }
        else {
          $('.disable-class').removeClass("select2-container-disabled");
          $('.disable-class').css("pointer-events","visible");
        }
      });

    jForm.data("customValidator", function (jForm) {
      var jYearsWithCarrierField = jForm.find("[name='plan[years_with_carrier]']")
        , jOldCarrier = jForm.find("[name='plan[old_carrier_company_id]']")
        , jAuth = jForm.find("input[name='plan[authorized_to_contact]']")
        , currentCarrier = jForm.find("[name='plan[carrier_company_id]']")
        , flag = true
        ;

      if(jAuth.length > 0 && !jAuth.prop("checked")){
        gmr.helper().showErrorForField("Please authorize us to contact the carrier.", jAuth);
        flag = false;
      }

      if(currentCarrier.val() === '-1'){
        gmr.helper().showErrorForField("Please select a carrier.", currentCarrier);
        flag = false;
      }
      $.each(jForm.find("input[type=email]"), function (index, emailField) {
        var jEmailField = $(emailField)
          , emailValue = jEmailField.val()
          ;

        if ((emailValue.length > 0) && (!gmr.helper().isValidEmail(emailValue))) {
          gmr.helper().showErrorForField("Please enter a valid email.", jEmailField);
          flag = false;
        }
      });

      return flag;
    });

    jForm.on("errorOnPlanSubmit", function (event, response) {
      gmr.helper().hideMask();
      if(jForm.find('.errorMessage').length){
        gmr.helper().scrollToElement(jForm.find('.errorMessage').first().parent(), 300, true);
      }
    });

    jForm.on("beforePlanSend", function (event) {
        gmr.helper().showMask();
    });

    jForm.on("planSubmitted", function (event, response) {
      gmr.helper().hideMask();
      var jForm = $(this)
        , jNewTab = $(response)
        , jNewForm = jNewTab.find("form")
        , planId = jNewTab.data("planId")
        , planName = jNewForm.find("input[name='plan[name]']").val()
        , planPermalink = jNewTab.attr("id")
        , jTabLi = $("<li class='text-center'><a class='ellipsis' href='#" + planPermalink + "' data-toggle='pill'>" + planName + "</a></li>")
        , summaryBtn = $("#summary_button").parent()
        , jTabPane = jForm.closest(".tab-pane")
        ;

      if (!planId) {
        return false;
      }

      $("#summary_form").append("<input type='hidden' name='plan_ids[]' value='" + planId + "'/>");

      summaryBtn.show();
      jTabLi.insertBefore(summaryBtn);
      jNewTab.appendTo($(".product_basket_container .tab-content"));

      gmr.helper().initAjaxForm(jNewTab);
      gmr.helper().initMaskedFields(jNewTab);
      gmr.helper().initAllSelect2(jNewTab);

      gmr.helper().showAlert('Your plan has been recorded.');

      jForm.get(0).reset();
      jForm.find(".select2Input").select2("data", null);
      gmr.helper().scrollToElement(jTabPane);
    });
  });

  $(window).on("beforeunload", function (e) {
    var confirmationMessage = "Are you sure you want to leave without finishing?";

    (e || window.event).returnValue = confirmationMessage;     // Gecko and Trident
    return confirmationMessage;                                // Gecko and WebKit
  });

  gmr.helper().initTabAutoload($(".nav.basket"), true);
  $('.nav.basket a:first').tab("show");
});
