// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

//= require mustache
//= require jquery-file-upload/vendor/jquery.ui.widget.js
//= require jquery-file-upload/jquery.iframe-transport
//= require jquery-file-upload/jquery.fileupload

var Uploads = function (config) {
  var oUpload = this;

  oUpload.config = config || {};
  oUpload.config.allowedFileTypes = ["doc", "docx", "pdf", "xls", "xlsx", "csv", "xlsm"];
};

Uploads.method("init", function () {
  var oUpload = this;

  oUpload.selectedFilesTable = $(".selected_files");
  oUpload.uploadForm = $(".create_upload_form");
  oUpload.fileDetails = $("#fileDetails");

  oUpload.uploadsBlock = $(".carrier_upload_files--upload-block");
  oUpload.benefitSelector = $(".carrier_upload_files--selector");
  oUpload.planSelector = $(".carrier_upload_files--plan-selector");
  oUpload.benefitTypeDetailSelector = $(".carrier_upload_files--sub-selection");

  oUpload.subTypes = $('.carrier_upload_files--sub-selector');
  oUpload.products = $('.carrier_upload_product_name');

  oUpload.uploadsBtn = oUpload.uploadsBlock.find(".uploadBtn");

  oUpload.fileUpload = $("#fileupload");

  oUpload.successModal = $("#upload-success");
  oUpload.progressBarWrap = $('.progressWrap');

  oUpload.currentAddedFileSize = 0;
  oUpload.uploadFailedFlag = false;
});

Uploads.method("initEditForm", function () {
  var oUpload = this;

  oUpload.initFileUploadWidget();
});

Uploads.method("initProductBasketForm", function () {
    var oUpload = this,
    productNameCheck = oUpload.products.find('input[type="checkbox"]');

    // hide sub checkboxes on load
    oUpload.subTypes.hide();
    oUpload.initProductSelector(oUpload.subTypes, productNameCheck);
    oUpload.initFileUploadWidget();
});

Uploads.method("initNewForm", function () {
  var oUpload = this,
    productNameCheck = oUpload.products.find('input[type="checkbox"]');

  // hide sub checkboxes on load
  oUpload.subTypes.hide();
  oUpload.initProductSelector(oUpload.subTypes, productNameCheck);
  oUpload.initFileUploadWidget();
});

Uploads.method("initProductSelector", function (subTypes, productNameCheck) {
  var oUpload = this
    , targetSection
    ;

  // toggle sub checkboxes on checking/unchecking checkboxes
  productNameCheck.on('change', function () {
    targetSection = '.' + $(this).data('showtarget') + '-sub-selector';
    // if main checkbox is checked, find its respective subtype and show it, else hide
    // $(targetSection).show();
    if ($(this).is(':checked')) {
      $(targetSection).show().parent().show();
    } else {
      $(targetSection).hide();
      $(targetSection).find("input[type=checkbox]").prop("checked", false);
      $(targetSection).find("input[type=checkbox]").trigger("change");
    }
  });
});

Uploads.method("addFileRow", function (currentFile) {
  var oUpload = this
    , jFileSelection = $(Mustache.render($("#selectedFileTr").html(), currentFile))
    ;

  oUpload.uploadsBtn.prop("disabled", false).removeAttr("disabled").removeClass("disabled");
  jFileSelection.appendTo(oUpload.selectedFilesTable);

  return jFileSelection;
});

Uploads.method("clearErrorMessages", function () {
  var oUpload = this;

  $(".carrier_upload_files--upload-block .errorMessage").fadeOut().remove();
  $(".carrier_upload_files--selector .errorMessage").fadeOut().remove();
  $(".carrier_upload_files--plan-selector .errorMessage").fadeOut().remove();
  $(".carrier_upload_files--sub-selection .errorMessage").fadeOut().remove();
});

Uploads.method("addFileAllowed", function (file) {
  var oUpload = this
    , fileNameSplit = file.name.split(".")
    , fileNameExt = fileNameSplit[fileNameSplit.length - 1]
    ;

  if ((oUpload.currentAddedFileSize + file.size) > $("#fileupload").data("blueimpFileupload").options.limitMultiFileUploadSize) {
    $("<div class='errorMessage'>10MB total file size limit reached.</div>").prependTo(oUpload.uploadsBlock).fadeIn();
    return false;
  }

  if (!oUpload.isFileTypeAllowed(fileNameExt)) {
    return false;
  }

  oUpload.currentAddedFileSize += file.size;
  return true;
});

Uploads.method("isFileTypeAllowed", function (fileExt) {
  var oUpload = this;

  if (oUpload.config.allowedFileTypes.indexOf(fileExt.toLowerCase()) < 0) {
    $("<div class='errorMessage'>File uploaded is not a valid file type.</div>").prependTo(oUpload.uploadsBlock).fadeIn();
    return false;
  }

  return true;
});

Uploads.method("uploadFailed", function () {
  var oUpload = this;

  oUpload.uploadFailedFlag = true;
  oUpload.fileUpload.removeAttr('disabled');
  oUpload.uploadForm.find("input[type=submit]").prop("disabled", false).removeClass("disabled");
  oUpload.uploadsBtn.text('upload').prop("disabled", false).removeAttr("disabled").removeClass("disabled");
  oUpload.fileDetails.prop("disabled", false).removeClass("disabled");
});

Uploads.method("submitAllowed", function () {
  var oUpload = this;

  oUpload.uploadForm.find("input[type=submit]").prop("disabled", true).addClass("disabled");

  if (oUpload.fileDetails.find("input[type=checkbox][name]:checked").length < 1) {
    if (oUpload.uploadsBlock.find(".errorMessage").length < 1) {
      $("<div class='errorMessage'>Please use the form above to categorize the file/s before uploading.</div>").prependTo(oUpload.uploadsBlock).fadeIn();
    }
    return false;
  } else {

    var flag = true
      , jFirstTarget = null
      , validateSelections = function (jCheckedElement) {
        var showTarget = jCheckedElement.data("showtarget")
          , targetSection = '.' + showTarget + '-sub-selector'
          , jShowTarget = $(targetSection)
          , selectedInputs = jShowTarget.find("input[type=checkbox][name]:checked")
          , f = true
          ;

        if (selectedInputs.length < 1) {
          $("<div class='errorMessage'>Please select at least one option.</div>").prependTo(jShowTarget).fadeIn();
          f = false;
        }

        $.each(selectedInputs, function () {
          var jInput = $(this);

          if (jInput.data("showtarget")) {
            f = f && validateSelections(jInput);
          }
        });

        return f;
      }
      ;

    $.each(oUpload.benefitSelector.find("input[type=checkbox][name]:checked"), function (index, checkedElement) {
      var jCheckedElement = $(checkedElement);

      flag = validateSelections(jCheckedElement);
    });

    if (!flag) {
      gmr.helper().scrollToElement(jFirstTarget);
      return false;
    }
  }

  oUpload.clearErrorMessages();
  oUpload.fileUpload.attr('disabled', 'disabled');
  oUpload.uploadsBtn.text('uploading...').prop("disabled", true).addClass("disabled");
  oUpload.fileDetails.prop("disabled", true).addClass("disabled");

  return true;
});

Uploads.method("fileUploaded", function (fileInfo) {
  var oUpload = this
    , trHTML = Mustache.render($("#uploadedFileTr").html(), fileInfo)
    , jDisplayTable = $(".carrier_upload_files--table")
    , jDisplayTableBody = jDisplayTable.find("tbody")
    ;

  oUpload.uploadForm.removeClass("hidden");
  jDisplayTableBody.append(trHTML).fadeIn();
});

Uploads.method("uploadsFinished", function () {
  var oUpload = this;

  if(!oUpload.uploadFailedFlag){
    oUpload.successModal.modal("show");
    oUpload.uploadsBtn.text('upload finished');
  }

  oUpload.fileUpload.prop("disabled", false).removeClass("disabled");
  oUpload.uploadForm.find("input[type=submit]").prop("disabled", false).removeClass("disabled");
  oUpload.progressBarWrap.fadeOut();

  setTimeout(function () {
    oUpload.resetForm();
  }, 2000);

});

Uploads.method("resetForm", function () {
  var oUpload = this;
  oUpload.fileUpload.removeAttr('disabled');
  if(!oUpload.uploadFailedFlag){
    oUpload.clearErrorMessages();
    oUpload.fileUpload.removeAttr('disabled');
    oUpload.uploadsBtn.text('upload').prop("disabled", true).attr('disabled', 'disabled').addClass("disabled");
    oUpload.successModal.modal("hide");
  }

  oUpload.fileDetails.prop("disabled", false).removeClass("disabled");
  oUpload.benefitSelector.find("input[type=checkbox][name]:checked").prop("checked", false).trigger("change");
  oUpload.subTypes.find("input[type=checkbox][name]:checked").not(":disabled").prop("checked", true);
  oUpload.fileUpload.prop("disabled", false).removeClass("disabled");
  oUpload.progressBarWrap.find('.progress-bar').css('width', '0%').attr('aria-valuenow', 0).find("span").html("0%");
});

Uploads.method("destroyFileUploadWidget", function () {
  var oUpload = this;

  oUpload.fileUpload.fileupload('destroy');
});

Uploads.method("initFileUploadWidget", function () {
  var oUpload = this;

  oUpload.fileUpload.fileupload({
    dataType: 'json',
    acceptFileTypes: /(\.|\/)(doc|docx|pdf|xls|xlsx|csv)$/i,
    limitMultiFileUploadSize: 9765630,
    add: function (e, data) {
      var currentFile = data.files[0];

      oUpload.clearErrorMessages();
      if (!oUpload.addFileAllowed(currentFile)) {
        return false;
      }
      data.context = oUpload.addFileRow(currentFile);

      data.context.find(".delete").on("click", function () {
        var confirmation = confirm("Are you sure you want to deselect the file?");
        if (confirmation) {
          data.context = null;
          $(this).closest(".selectedFile").fadeOut().remove();
          if ($(".selected_files tr").length < 1) {
		          oUpload.clearErrorMessages();
		          oUpload.uploadForm.find("input[type=submit]").prop("disabled", false).removeClass("disabled");
            oUpload.uploadsBtn.text('upload').prop("disabled", true).attr('disabled', 'disabled').addClass("disabled");
          }
          oUpload.currentAddedFileSize -= currentFile.size;
        }
      });

      oUpload.uploadsBtn.click(function () {
        data.submit();
      });
    },
    submit: function (e, data) {
      oUpload.clearErrorMessages();

      if (!data.context || !data.context.is(":visible") || !oUpload.submitAllowed()) {
        return false;
      }

      data.context.find(".delete").prop("disabled", true);
      data.formData = oUpload.fileDetails.serializeArray();
      oUpload.uploadFailedFlag = false;
    },
    done: function (e, data) {

        var currentFileInfo = null,
            errors = data.result.errors || [],
            onError = function(){
                oUpload.uploadFailedFlag = true;
                $.each(data.result.errors, function (index, error) {
                    $("<div class='errorMessage'>" + error + "</div>").prependTo($(".carrier_upload_files--upload-block")).fadeIn();
                });
                $('.progressWrap').fadeOut(function () {
                    $('.progress-bar').css('width', '0%').attr('aria-valuenow', 0).find("span").html("0%");
                });
                data.context.find(".delete").prop("disabled", false);
                oUpload.uploadsBtn.text('upload').prop("disabled", false).removeAttr('disabled').removeClass("disabled");
            };
        if(errors.length){
            onError();
        }
        for(var i=0; i < data.result.files.length; i++){
          currentFileInfo = data.result.files[i];
          if (currentFileInfo) {
              oUpload.fileUploaded(currentFileInfo);
              if(data.context) {
                  data.context.fadeOut().remove();
                  delete data.context;
                  oUpload.currentAddedFileSize -= data.files[0].size;
              }
              data.files = [];
          } else {
              onError();
          }
      }
    },
    stop: function (e, data) {
      oUpload.uploadsFinished();
      data.files = [];
    },
    progressall: function (e, data) {
      var progress = parseInt(data.loaded / data.total * 100, 10);
      $('.progressWrap').show();
      $('.progress-bar').css('width', progress + '%').attr('aria-valuenow', progress).find("span").html(progress + "%");
      if (progress === 100) {
        $('.progress-bar').removeClass("active");
      }
    },
    fail: function (e, data) {
      $("<div class='errorMessage'>" + data.response().errorThrown + "</div>").prependTo(oUpload.uploadsBlock).fadeIn();
      data.context.find(".delete").prop("disabled", false);
      oUpload.uploadFailed();
    }
  });
});
