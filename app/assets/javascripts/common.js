//= require jquery
//= require jquery-ui
//= require jquery_ujs
//= require bootstrap-sprockets
//= require_self
//= require gmr
//= require jquery.alphanum
//= require helper
//= require variables.js.erb
//= require notification
//= require plan_design_sliders

/* Adds methods to a Javascript Prototype Object (pseudo-Class) */
Function.prototype.method = function (name, func) {
  this.prototype[name] = func;
  return this;
};


Number.prototype.formatMoney = function(places, symbol, thousand, decimal) {
    places = !isNaN(places = Math.abs(places)) ? places : 2;
    symbol = symbol !== undefined ? symbol : "$";
    thousand = thousand || ",";
    decimal = decimal || ".";
    var number = this,
        negative = number < 0 ? "-" : "",
        i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
};

Number.prototype.appendPercentage = function() {
    return this + "%";
};

Number.prototype.numberWithSubscript = function (formatCurrency) {
  var thisVal = Number(this).toFixed(2);
  var arr = String(thisVal).split(".");
  var arr_first = formatCurrency ? Number(arr[0]).formatMoney(0, '') : arr[0];
  var arr_second = arr[1];
  return "<span class='subscript_num'>"+ arr_first +"<sub>."+ arr_second +"</sub></span>";
};

(function ($) {
  $.fn.serializeObject = function (options) {

    options = jQuery.extend({}, options);

    var self = this,
      json = {},
      push_counters = {},
      patterns = {
        "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
        "key": /[a-zA-Z0-9_]+|(?=\[\])/g,
        "push": /^$/,
        "fixed": /^\d+$/,
        "named": /^[a-zA-Z0-9_]+$/
      };


    this.build = function (base, key, value) {
      base[key] = value;
      return base;
    };

    this.push_counter = function (key) {
      if (push_counters[key] === undefined) {
        push_counters[key] = 0;
      }
      return push_counters[key]++;
    };

    jQuery.each(jQuery(this).serializeArray(), function () {

      // skip invalid keys
      if (!patterns.validate.test(this.name)) {
        return;
      }

      var k,
        keys = this.name.match(patterns.key),
        merge = this.value,
        reverse_key = this.name;

      while ((k = keys.pop()) !== undefined) {

        // adjust reverse_key
        reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

        // push
        if (k.match(patterns.push)) {
          merge = self.build([], self.push_counter(reverse_key), merge);
        }

        // fixed
        else if (k.match(patterns.fixed)) {
          merge = self.build([], k, merge);
        }

        // named
        else if (k.match(patterns.named)) {
          merge = self.build({}, k, merge);
        }
      }

      json = jQuery.extend(true, json, merge);
    });


    return json;
  };

  $.fn.insertAt = function (i, jEle) {
    if (i === 0) {
      jQuery(this).prepend(jEle);
      return;
    }

    jQuery(this).children("*:nth-child(" + i + ")").after(jEle);
  };

  //SET CURSOR POSITION
  $.fn.setCursorPosition = function (pos) {
    this.each(function (index, elem) {
      if (elem.setSelectionRange) {
        elem.setSelectionRange(pos, pos);
      } else if (elem.createTextRange) {
        var range = elem.createTextRange();
        range.collapse(true);
        range.moveEnd('character', pos);
        range.moveStart('character', pos);
        range.select();
      }
    });
    return this;
  };

  $.fn.copyCSS = function (source) {
    var styles = $(source).getStyleObject();
    this.css(styles);
  };

  $.fn.getStyleObject = function () {
    var dom = this.get(0);
    var style;
    var returns = {};
    if (window.getComputedStyle) {
      var camelize = function (a, b) {
        return b.toUpperCase();
      };
      style = window.getComputedStyle(dom, null);
      for (var i = 0, l = style.length; i < l; i++) {
        var prop = style[i];
        var camel = prop.replace(/\-([a-z])/g, camelize);
        var val = style.getPropertyValue(prop);
        returns[camel] = val;
      }
      return returns;
    }

    if (style = dom.currentStyle) {
      for (var prop_key in style) {
        returns[prop_key] = style[prop_key];
      }
      return returns;
    }
    return this.css();
  };
})(jQuery);

$(function () {
  var dashboard_nav_list = $('#new_nav_id'),
      link_toggle_nav = $('.link_toggle_nav'),
      icon_toggle_nav = link_toggle_nav.find('.icon_link_toggle'),
      hidWidth,
      scrollBarWidths = 40;

  var widthOfList = function(){
    var itemsWidth = 0;
    $('.scroll_nav_list .subitem').each(function(){
      var itemWidth = $(this).outerWidth();
      itemsWidth+=itemWidth;
    });
    return itemsWidth;
  };

  var widthOfHidden = function(){
    return (($('.dashboard_wrapper').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
  };

  var getLeftPosi = function(){
    if($('.scroll_nav_list').length){
      return $('.scroll_nav_list').position().left;
    }
    return 0;
  };

  var reAdjust = function(){
    if (($('.dashboard_wrapper').outerWidth()) < widthOfList()) {
      $('.scroller-right').show();
    }
    else {
      $('.scroller-right').hide();
    }

    if (getLeftPosi()<0) {
      $('.scroller-left').show();
    }
    else {
      $('.item').animate({left:"-="+getLeftPosi()+"px"},'slow');
      $('.scroller-left').hide();
    }
  };

  $(window).on('resize',function(e){
    reAdjust();
  });

  $('.scroller-right').click(function() {

    $('.scroller-left').fadeIn('slow');
    $('.scroller-right').fadeOut('slow');

    $('.scroll_nav_list').stop().animate({left:"+="+widthOfHidden()+"px"},'slow',function(){

    });
  });

  $('.scroller-left').click(function() {

    $('.scroller-right').fadeIn('slow');
    $('.scroller-left').fadeOut('slow');

      $('.scroll_nav_list').stop().animate({left:"-="+getLeftPosi()+"px"},'slow',function(){

      });
  });

  function toggleNavState(){
    gmr.helper().scrollTo(0, 400);
    if(dashboard_nav_list.hasClass('nav_expanded')) {
      dashboard_nav_list.removeClass('nav_expanded');
      $('body').removeClass('nav_visible');
      icon_toggle_nav
        .removeClass('fa-times fade_squeeze')
        .addClass('fa-bars fadeIn');
      dashboard_nav_list.slideUp(500, 'easeInOutBack');
    } else {
      dashboard_nav_list.addClass('nav_expanded');
      $('body').addClass('nav_visible');
      icon_toggle_nav
        .addClass('fa-times fade_squeeze')
        .removeClass('fa-bars fadeIn');
      dashboard_nav_list.slideDown(500, 'easeInOutBack');
      reAdjust();
    }
  }
  dashboard_nav_list.slideUp(0);
  link_toggle_nav.click(toggleNavState);
});
