// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

var GMR = (function (GMR) {

  GMR.Filter = function (config) {
    var self = this;

    self.config = config || {};
    self.namespace = "filter";
  };

  GMR.Filter.method("init", function () {
    var self = this
      , controller = self.config.controller || ""
      , action = self.config.action || ""
      , subController = controller.split(self.namespace + "/")[1]
      ;

      gmr.helper().initDatePickers();
  });

  GMR.Filter.method("initEventListeners", function(filterBarEl) {

      gmr.helper().initAllSelect2(filterBarEl);

      //activate submenus
      filterBarEl.find('.filters_link').click(function(event) {
        event.preventDefault();
        event.stopPropagation();
        filterBarEl.find('.filter_active').not(this).removeClass('filter_active');
        $(".select2-dropdown-open").select2('close');
        $(this).toggleClass('filter_active');
        $(this).closest('.dropdown-submenu').find('.dropdown-menu input').eq(0).focus();
      });

      // reset all open submenus on main filter button click
      filterBarEl.find('.filters_dropdown_button').click(function(e){
        filterBarEl.find('.filter_active').removeClass('filter_active');
      });

      filterBarEl.find('.filters_link + .dropdown-menu').click(function(event) {
        event.stopPropagation();
      });

      $('#ui-datepicker-div').click(function(event) {
        event.stopPropagation();
      });
      

  });
  GMR.Filter.method("isTemplateAdded", function(filterListEl, data) {
      var foundTemplateEl, liEl,
          liData,
          li = filterListEl.find('.filter-list-item');
      for(var i=0; i<li.length; i++){
          liEl = $(li[i]);
          liData = liEl.data();
          if(data.property === liData.property){
              foundTemplateEl = liEl;
              break;
          }
      }
      return foundTemplateEl;
  });
  GMR.Filter.method("appendTemplate", function(filterListEl, data) {
        var tpl = "<li class='filter-list-item filter_list--item fadeIn' data-property='" + data.property  + "' data-value='"+data.value+ "' data-name='"+data.name+ "' title='"+data.displayValue+"'>" +  data.label +": <span class='filter-dis-item'>"+ data.displayValue +"</span>" + " <i class='fa fa-close remove_filter_tag' title='Remove'></i></li>";
        filterListEl.append(tpl);
  });
  GMR.Filter.method("replaceTemplate", function(foundTemplateEl, data) {
      var replaceEl = foundTemplateEl.find('.filter-dis-item');
      foundTemplateEl.attr('data-value', data.value);
      foundTemplateEl.attr('title', data.displayValue);
      foundTemplateEl.data('value', data.value);
      foundTemplateEl.data('name', data.name);
      replaceEl.html(data.displayValue);
  });
  GMR.Filter.method("addTemplate", function(filterBarEl, data) {
      data.displayValue = data.displayValue || data.value;
      var self = this,
          filterListEl = filterBarEl.find('.filter_list'),
          foundTemplateEl = self.isTemplateAdded(filterListEl, data);
      if(foundTemplateEl) {
          self.replaceTemplate(foundTemplateEl, data);
      }else {
          self.appendTemplate(filterListEl, data);
      }
  });
  GMR.Filter.method("getAppliedFilter", function(filterBarEl) {
      var filters = [], data;
      filterBarEl.find('.filter-list-item').each(function(index, item){
          data = $(item).data();
          if(data.property){
            filters.push({
                 property: data.property,
                 value: data.value,
                 name: data.name
            });
          }
      });
      return filters;
  });
  GMR.Filter.method("renderFilters", function(filterBarEl, filtersPref) {
      var self = this,
        i = 0;

      for(;i<filtersPref.length; i++){
          self.addTemplate(filterBarEl, filtersPref[i]);
      }
  });
  GMR.Filter.method("initFilter", function(config) {
      var self = this,
          jScope = config.jScope || $(document),
          filterBarEl = jScope.find('.filter-bar'),
          afterApply = config.afterApply || $.noop,
          filtersPref = config.filtersPref || [];

      self.initEventListeners(filterBarEl);
      self.renderFilters(filterBarEl, filtersPref);


      afterApply({
          filters: self.getAppliedFilter(filterBarEl)
      });

      filterBarEl.on('click','.remove_filter_tag', function(event) {
          event.preventDefault();
          $(this).parent('li').remove();
          afterApply({
              filters: self.getAppliedFilter(filterBarEl)
          });
      });
      filterBarEl.on('click','.reset_filter_button', function(event) {
          event.preventDefault();
          $('.filter_list li').remove();
          afterApply({
              filters: self.getAppliedFilter(filterBarEl)
          });
      });

      filterBarEl.find('.btn_filter_add').click(function(event) {
          var jqEl = $(this),
              parentEL = $(this).closest('.submenu_filter_list'),
              inputEl, val,
              data = jqEl.data(),
              i, select2El, select2Data, inEls,
              ids = [],
              labels = [],
              filterObj = {
                  label: data.filterLabel,
                  property: data.filterName,
                  value: '',
                  name: ''
              };

          switch(data.filterType){
              case "checkbox_autocomplete":
                  select2El = parentEL.find('.select2Input');
                  select2Data = select2El.select2('data');

                  for(i=0;i<select2Data.length; i++){
                      ids.push(select2Data[i].id);
                      labels.push(select2Data[i].text);
                  }
                  inEls =  parentEL.find(".filter-check-box:checked");
                  for(i=0; i<inEls.length; i++){
                      inputEl = $(inEls[i]);
                      ids.push(inputEl.val());
                      labels.push(inputEl.data('label'));
                      inputEl[0].checked = false;
                  }
                  if(!ids.length){
                      return false;
                  }
                  filterObj.value = ids.join(",");
                  filterObj.displayValue = labels.join(", ");
                  select2El.select2('val', "");
              break;
              case "autocomplete":
                  select2El = parentEL.find('.select2Input');
                  select2Data = select2El.select2('data');

                  if(!select2Data.length){
                      return;
                  }
                  for(i=0;i<select2Data.length; i++){
                      ids.push(select2Data[i].id);
                      labels.push(select2Data[i].text);
                  }
                  filterObj.value = ids.join(",");
                  filterObj.displayValue = labels.join(", ");
                  select2El.select2('val', "");
              break;
              case "dropdown":
                  inputEl = parentEL.find('.filter-select-box');
                  var selectedEl = inputEl.find("option:selected");
                  filterObj.value = selectedEl.val();

                  if(!filterObj.value){
                      return false;
                  }
                  inputEl.val("");
                  filterObj.displayValue = selectedEl.text();
              break;

              case "search_text":
                  var selectedElName1 = $(parentEL.find('.filter-input-box')[0]).attr('name');
                  var selectedElName2 = $(parentEL.find('.filter-input-box')[1]).attr('name');
                  var searchName1 = $(parentEL.find('.filter-input-box')[0]).val(),
                      searchName2 = $(parentEL.find('.filter-input-box')[1]).val();
                  if(!searchName1 && !searchName2){
                      return;
                  }else if(searchName1 && !searchName2){
                      filterObj.value = searchName1;
                      filterObj.name = selectedElName1;
                  }else if(!searchName1 && searchName2){
                      filterObj.value = searchName2;
                      filterObj.name = selectedElName2;
                  }else{
                      return;
                  }
                  parentEL.find('.filter-input-box').val("");
                  
              break;
              case "salary_range":
                  var fromRange = parentEL.find('.filter-fromrange-box').val(),
                      toRange = parentEL.find('.filter-torange-box').val();
                  parentEL.find('.errorMessage').remove();
                  if (Number(fromRange) > Number(toRange)){
                    $(parentEL.find('.cal-input-wrap')[0]).append('<span class="errorMessage" style="display:block">From Range should be lesser than To Range</span>');
                    return;
                  }
                  if(!fromRange && !toRange){
                      return;
                  }else if(fromRange && !toRange){
                      filterObj.value = fromRange;
                      filterObj.value += "-";
                      filterObj.value += fromRange;
                  }else if(!fromRange && toRange){
                      filterObj.value = toRange;
                      filterObj.value += "-";
                      filterObj.value += toRange;
                  }else{
                      filterObj.value = fromRange;
                      filterObj.value += "-";
                      filterObj.value += toRange;
                  }
                  parentEL.find('.filter-fromrange-box, .filter-torange-box').val("");
              break;
              case "date_range":
                  var fromDate = parentEL.find('.filter-fromdate-box').val(),
                      toDate = parentEL.find('.filter-todate-box').val();
                  parentEL.find('.errorMessage').remove();
                  if (new Date(fromDate).getTime() > new Date(toDate).getTime()){
                    $(parentEL.find('.input-group.date')[0]).append('<span class="errorMessage" style="display:table-row">From Date should be lesser than To Date</span>');
                    return;
                  }
                  if(!fromDate && !toDate){
                      return;
                  }else if(fromDate && !toDate){
                      filterObj.value = fromDate;
                      filterObj.value += "-";
                      filterObj.value += fromDate;
                  }else if(!fromDate && toDate){
                      filterObj.value = toDate;
                      filterObj.value += "-";
                      filterObj.value += toDate;
                  }else{
                      filterObj.value = fromDate;
                      filterObj.value += "-";
                      filterObj.value += toDate;
                  }
                  parentEL.find('.filter-fromdate-box, .filter-todate-box').val("");
              break;
              case "checkbox":
                inEls =  parentEL.find(".filter-check-box:checked");
                var values = [], displayValues = [];

                for(i=0; i<inEls.length; i++){
                    inputEl = $(inEls[i]);
                    values.push(inputEl.val());
                    displayValues.push(inputEl.data('label'));
                    inputEl[0].checked = false;
                }
                filterObj.value = values.join(",");
                if(!filterObj.value){
                    return false;
                }
                filterObj.displayValue = displayValues.join(", ");
              break;
              case "radio_button":
                inEls =  parentEL.find(".filter-radio-button:checked");
                var values = [], displayValues = [];

                for(i=0; i<inEls.length; i++){
                    inputEl = $(inEls[i]);
                    values.push(inputEl.val());
                    displayValues.push(inputEl.data('label'));
                    inputEl[0].checked = false;
                }
                filterObj.value = values.join(",");
                if(!filterObj.value){
                    return false;
                }
                filterObj.displayValue = displayValues.join(", ");
              break;
              default:
                inputEl = parentEL.find('.filter-input-box');
                filterObj.value = inputEl.val();
                if(!filterObj.value){
                    return false;
                }
                inputEl.val("");
              break;
          }
          self.addTemplate(filterBarEl, filterObj);
          filterBarEl.find('.filters_link.filter_active').trigger('click');
          afterApply({
              filters: self.getAppliedFilter(filterBarEl)
          });
      });

  });

  return GMR;
})(GMR);
