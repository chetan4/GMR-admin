// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

//= require multiselect.min
//= require mustache
//= require select2
//= require jquery-mask/jquery.mask.min

var Employees = function (config) {
  var self = this;

  self.config = config;
};

Employees.method("init", function () {
  var self = this;

});

Employees.method("initEmployeeForm", function (extraParams) {
  var self = this
    , jEmployeeForm = $("#new_user");

    jEmployeeForm.find(".popupMultiSelect").multiselect();

   self.extraParams = extraParams || {};

  jEmployeeForm.find("input[type=submit]").on("click", function (event) {
    var jSubmitButton = $(this)
      , jForm = jSubmitButton.closest("form")
      ;

    event.preventDefault();

    if (gmr.helper().validateForm(jForm)) {
      jForm.submit();
    }
  });

  gmr.helper().initMaskedFields(jEmployeeForm);

   self.initEventListeners();
});

Employees.method("initEventListeners", function () {
  var self = this;
       self.initBenefitTypeDropdown();
});

Employees.method("initBenefitTypeDropdown", function () {

    var self = this,
        extraParams = self.extraParams || {},
        isCarrier = (extraParams['vertical'] === 'carrier'),
        ignore_details_for_roles = extraParams["ignore_details_for_roles"] || [];
    if(isCarrier) {
        var added = {},
            val,
            tpl = $("#userSizeBoxTplId").html(),
            addUserTypes = function (id, name) {
                var html = Mustache.to_html(tpl, {id: id, name: name}),
                    tmp = $(html);
                $(".user_size_wrap").append(tmp);
                tmp.find(".popupMultiSelect").multiselect();
                tmp.find(".location_tag").select2({
                    tags: []
                });
            };
        $("#user_role").change(function(){
             if(ignore_details_for_roles.indexOf(Number($(this).val())) !== -1){
                 $("#benefit_type_ids").data('bs.multiselect').clear();
                 added = {};
                 $("#benefit_type_ids_wrap").hide();
             }else {
                 $("#benefit_type_ids_wrap").show();
             }
        }).trigger('change');

        if (tpl) {
            $("#benefit_type_ids").on('deselectiondone.bs.multiselect', function (evt, obj) {
                val = obj.$element.val();
                $("#user_size_panel_" + val).remove();
                delete added[val];
            });
            $("#benefit_type_ids").on('selectiondone.bs.multiselect', function (evt, obj) {
                val = obj.$element.val();
                if (!added[val]) {
                    added[val] = obj.$element.text();
                    addUserTypes(val, added[val]);
                }
            });
        }
    }
});
