var GMR = (function () {
  /*
   Base App Class. Lazy Loads child classes aka subApps.

   Author: Harshniket
   Review:
   */
  var App = function (params) {
    this.params = params || {};

    var subApps = {
      "helper": "Helper",
      "filter": "Filter",
      "charts": "Charts",
      "variables": "Variables",
      "passwords": "Passwords",
      "invitations": "Invitations",
      "registrations": "Registrations",
      "admin": "Admin",
      "broker": "Broker",
      "carrier": "Carrier",
      "dataEntry": "DataEntry",
      "employer": "Employer",
      "planDesignSliders": "PlanDesignSliders",
      "uploads": "Uploads",
      "notification": "Notification",
      "fileUploads": "FileUploads"
    };

    /*
     Lazy Loading of subApps.
     Goes through subApps and maps a function which initializes the subApp if not initialized,
     otherwise returns the already initialized object of the said class.

     Author: Harshniket
     Review:
     */
    for (var key in subApps) {
      if (!subApps.hasOwnProperty(key)) {
        continue;
      }
      var subAppName = subApps[key];
      App.prototype[key] = (function (subAppName) {
        var obj = '';
        return function (params) {
          if (typeof(obj) === "string") {
            if (App[subAppName]) {
              var SubApp = App[subAppName];
              if (SubApp.get && typeof(SubApp.get) === "function") {
                obj = SubApp.get(params);
              } else {
                obj = new SubApp(params, this);
                if (typeof(obj["init"]) === "function") {
                  obj["init"]();
                }
              }
            } else {
              obj = null;
            }
          }
          return obj;
        };
      }(subAppName));
    }
  };

  /*
   Init Function for Base App class.

   Author: Harshniket
   Review:
   */
  App.method("init", function (params) {
    var self = this;

    self.helper().initAlerts();
    self.helper().initAllSelect2();
    self.helper().initNumberOnlyField();
    self.helper().initAllowDecimal();
    self.helper().initTextOnlyField();
    self.helper().initFieldWithoutZero();
    self.helper().initThousandsSeparator();
    self.helper().initImageLoad();
    self.helper().initRestrictCopy();
    self.helper().initTooltips();
    self.helper().initModal();
    self.helper().slideUpNotice();
    self.notification({
        env: params.env
    });

      $( document ).ajaxError(function(obj, res) {
          if(res.responseText && res.responseText.indexOf('window.location') != -1){
              window.location.reload();
          }
      });

    self.confirmModal = gmr.helper().initConfirmModal();

    setTimeout(function () {
      // Remove messages only if user is not present, as requested by Donald.
      // If any type still needs to be hidden put if outside the if condition.
      if (self.params.userId > 0) {
        self.helper().slideUpAlerts("success");
        self.helper().slideUpAlerts("danger");
        self.helper().slideUpAlerts("info");
      }
    }, 5 * 1000);
  });
  return App;
})
();
