/**
 * Created by saloka on 5/7/15.
 */

var GMR = (function (GMR) {

  GMR.Admin.Templates.Section = function (config) {
    var self = this;
    self.config = config || {};
    self.add_action = "add_section";
    self.update_action = "update_section";
    self.fields = [];
    self.columns = [];
    self.inputs = {};
  };

  GMR.Admin.Templates.Section.method("init", function (jSheet, sectionForm, callback) {
    var self = this;

    if (self.xhr || self.initialized) {
      return false;
    }

    self.sheet = jSheet;
    self.load(sectionForm, callback);

    return true;
  });

  GMR.Admin.Templates.Section.method("initExisting", function (jSheet, jSection) {
    var self = this;
    self.sheet = jSheet;
    self.setContent(jSection);
    self.initEventListeners();
  });

  GMR.Admin.Templates.Section.method("load", function (sectionForm, callback) {
    var self = this;

    self.xhr = $.ajax({
      url: self.add_action,
      data: sectionForm.serialize(),
      method: 'POST',
      success: function (response) {
        self.afterLoad(response, callback);
      },
      error: function (xhr) {
        if (xhr.status !== 200) {
          gmr.helper().showAlert(xhr.responseText, 'error');
        }
      }
    });

    return self.xhr;
  });

  GMR.Admin.Templates.Section.method("afterLoad", function (response, callback) {
    var self = this;
    self.setContent($(response));
    self.appendContent();
    gmr.helper().initPopovers(self.sectionContent);

    self.createSectionLink();
    self.initEventListeners();
    if (typeof callback === "function") {
      callback(self);
    }
  });

  GMR.Admin.Templates.Section.method("createSectionLink", function () {
    var self = this
      , jSectionLink = $('<li><a class="linkScroller" data-target="#' + self.sectionId + '">' + self.order + '</a></li>')
      ;

    return jSectionLink;
  });

  GMR.Admin.Templates.Section.method("initEventListeners", function () {
    var sectionObj = this,
      doNotScroll = true;

		  sectionObj.sectionContent.find('.delete-section').on('click', function () {
				  var getParentDiv = $(this).parent('span');
				  gmr.confirmModal.confirm('Entire Section will be deleted. Are you sure?', function(){
								 var deleteSection = getParentDiv.find('input').serialize();
						  gmr.helper().showMask();
						  $.ajax({
								  url:'delete_section',
								  method: "POST",
								  data: deleteSection,
								  complete: function (){
										  gmr.helper().hideMask();
								  },
								  success: function (response){
										  if(response.success){
												  var removeSection = getParentDiv.data('removeSection');
												  $('#'+removeSection).remove();
												  $('.linkScroller[data-target="#' + removeSection + '"]').closest('li').remove();
										  }
										  else {
												  gmr.helper().showAlert(response.errors, 'error');
										  }
								  }
						  });
				  });
		  });

    $(document).on("createColumnForSection_" + sectionObj.id, function (event) {
      var jForm = $(event.target);

      if (gmr.helper().validateForm(jForm)) {
        var new_column = new GMR.Admin.Templates.Column(sectionObj.config);
        if (new_column.init(sectionObj.sectionContent, jForm, function (columnObj) {
            sectionObj.pushColumn(columnObj);
          })) {
          jForm.get(0).reset();
        }
      }
    });

    $(document).on("createFieldForSection_" + sectionObj.id, function (event) {
      var jForm = $(event.target);

      if (gmr.helper().validateForm(jForm)) {
        var new_field = new GMR.Admin.Templates.Field(sectionObj.config);
        if (new_field.init(sectionObj.sectionContent, jForm, function (fieldObj) {
            sectionObj.pushField(fieldObj);
          })) {
          jForm.get(0).reset();
        }
      }
    });
    $(document).on("updateSection_" + sectionObj.id, function (event) {
      var jForm = $(event.target);
      if (gmr.helper().validateForm(jForm)) {
        $.ajax({
          url: sectionObj.update_action,
          data: jForm.serialize(),
          method: 'POST',
          success: function (response) {
            if(response.success){
            sectionObj.afterUpdate(response.html);
            } else {
              gmr.helper().showAlert(response.errors,'error');
            }
          },
          error: function(){
              gmr.helper().showAlert('Something went wrong! Please try again','error');
          }
        });
      }
    });

    sectionObj.initialized = true;

    sectionObj.initColumnDrag();

    sectionObj.sectionTable.rowSorter({
      handler: "td > span.fieldContent",
      onDrop: function (tableObj) {
        $.each($(tableObj).find(".field"), function (index, field) {
          var jField = $(field)
            , order = index + 1
            ;

          jField.data("order", order);
          jField.trigger("orderChanged", sectionObj.id);
        });
      }
    });
  });

  GMR.Admin.Templates.Section.method("afterUpdate", function (updatedSectionContent) {
    var self = this;
    self.sectionContent.find(".sectionEditContent").replaceWith(updatedSectionContent);
  });


  GMR.Admin.Templates.Section.method("reinitColumnDrag", function (fieldObj) {
    var self = this
      , dragTableObj = self.sectionTable.data().akottrDragtable
      ;

    if (dragTableObj) {
      dragTableObj.redraw();
    } else {
      self.initColumnDrag();
    }
  });

  GMR.Admin.Templates.Section.method("initColumnDrag", function () {
    var sectionObj = this;

    sectionObj.sectionTable.dragtable({
      dragaccept: '.column',
      persistState: function (tableObj) {
        var jTable = tableObj.el
          , columns = jTable.find('.column')
          ;

        $.each(columns, function (index, column) {
          var jColumn = $(column)
            , order = index + 1
            ;

          jColumn.data("order", order);
          jColumn.trigger("orderChanged", sectionObj.id);
        });
      }
    });
  });

  GMR.Admin.Templates.Section.method("pushField", function (fieldObj) {
    var self = this;

    self.attachFieldListeners(fieldObj);
    self.fields.push(fieldObj);
    self.TemplateActionWrapperHeader();
    $.each(self.columns, function (index, columnObj) {

      var jInputContainer = fieldObj.getInputContainer(columnObj)
        , new_template_input = new GMR.Admin.Templates.Input(self.config)
        ;

      if (new_template_input.findExistingOrInit(jInputContainer, self, fieldObj, columnObj)) {
        if (!self.inputs[fieldObj.id]) {
          self.inputs[fieldObj.id] = {};
        }
        self.inputs[fieldObj.id][columnObj.id] = new_template_input;
      }
    });
  });

  GMR.Admin.Templates.Section.method("pushColumn", function (columnObj) {
    var self = this;

    self.reinitColumnDrag();
    self.columns.push(columnObj);
    var tdHTML = columnObj.createInputContainer();

    $.each(self.fields, function (index, fieldObj) {

      var jInputContainer = $(tdHTML);
      fieldObj.fieldContent.find("td:last").before(jInputContainer);

      var new_template_input = new GMR.Admin.Templates.Input(self.config);
      if (new_template_input.init(jInputContainer, self, fieldObj, columnObj)) {
        if (!self.inputs[fieldObj.id]) {
          self.inputs[fieldObj.id] = {};
        }
        self.inputs[fieldObj.id][columnObj.id] = new_template_input;
      }
    });
  });

  GMR.Admin.Templates.Section.method("attachFieldListeners", function (fieldObj) {
    var self = this;

    fieldObj.onUpdate = function (fieldObj) {
      self.fieldUpdated(fieldObj);
    };
  });

  GMR.Admin.Templates.Section.method("fieldUpdated", function (fieldObj) {
    var self = this;

    $.each(self.columns, function (index, columnObj) {
      var inputObj = self.inputs[fieldObj.id][columnObj.id];

      inputObj.reload();
    });

  });

  GMR.Admin.Templates.Section.method("TemplateActionWrapperHeader", function () {
    var self = this;

    if(self.sectionContent.length > 0) {
      $("#previewTemplate").show();
      $("#publishTemplate").show();
    }

  });

  GMR.Admin.Templates.Section.method("appendContent", function () {
    var self = this;
    self.sheet.find(".sheetContent").append(self.sectionContent);
    self.sheet.find(".addSection").data("templateSectionOrder", self.order + 1);
  });

  GMR.Admin.Templates.Section.method("setContent", function (content) {
    var self = this;

    self.sectionContent = content;
    self.sectionId = self.sectionContent.attr("id");
    self.id = self.sectionContent.data("id");
    self.order = self.sectionContent.data("order");
    self.sectionTable = self.sectionContent.find('.gmr-table');
  });

  GMR.Admin.Templates.Section.method("reflectCurrentState", function () {
    var self = this;

    self.reflectFields();
    self.reflectColumns();
    self.reflectInputs();

    self.TemplateActionWrapperHeader();
    
  });

  GMR.Admin.Templates.Section.method("reflectColumns", function () {
    var self = this
      , jColumns = self.sectionContent.find("table > thead > tr:first > th.column")
      ;
    $.each(jColumns, function (index) {

      var jColumn = $(this)
        , new_column = new GMR.Admin.Templates.Column(self.config)
        ;

      new_column.initExisting(jColumn);
      self.columns.push(new_column);
      new_column.reflectCurrentState();
    });
  });

  GMR.Admin.Templates.Section.method("reflectFields", function () {
    var self = this
      , jFields = self.sectionContent.find("table > tbody > tr")
      ;
      
    $.each(jFields, function (index) {

      var jField = $(this)
        , new_field = new GMR.Admin.Templates.Field(self.config)
        ;

      self.attachFieldListeners(new_field);
      new_field.initExisting(jField);
      self.fields.push(new_field);
      new_field.reflectCurrentState();
    });
  });

  GMR.Admin.Templates.Section.method("reflectInputs", function () {
    var self = this;

    for (var fieldIndex = 0; fieldIndex < self.fields.length; fieldIndex++) {

      var fieldObj = self.fields[fieldIndex];
      self.inputs[fieldObj.id] = {};

      for (var columnIndex = 0; columnIndex < self.columns.length; columnIndex++) {

        var columnObj = self.columns[columnIndex]
          , jInputContainer = fieldObj.getInputContainer(columnObj)
          , new_template_input = new GMR.Admin.Templates.Input(self.config)
          ;

        if (new_template_input.findExistingOrInit(jInputContainer, self, fieldObj, columnObj)) {
          self.inputs[fieldObj.id][columnObj.id] = new_template_input;
        }
      }
    }
  });

  return GMR;
})(GMR);
