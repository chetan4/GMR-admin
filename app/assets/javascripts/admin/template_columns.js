/**
 * Created by saloka on 5/7/15.
 */

var GMR = (function (GMR) {

  GMR.Admin.Templates.Column = function (config) {
    var self = this;
    self.config = config || {};
    self.add_action = "add_column";
    self.update_action = "update_column";
    self.order_update_action = "update_column_order";

  };

  GMR.Admin.Templates.Column.method("init", function (jSection, columnForm, callback) {
    var self = this
      ;

    if (self.xhr || self.initialized) {
      return false;
    }

    self.section = jSection;
    self.load(columnForm, callback);

    return true;
  });


  GMR.Admin.Templates.Column.method("load", function (columnForm, callback) {
    var self = this;

    self.xhr = $.ajax({
      url: self.add_action,
      data: columnForm.serialize(),
      method: 'POST',
      success: function (response) {
        self.afterLoad(response, callback);
      },
      error: function(xhr){
        if(xhr.status !== 200){
          gmr.helper().showAlert(xhr.responseText,'error');
        }
      }
    });

    return self.xhr;
  });

  GMR.Admin.Templates.Column.method("afterLoad", function (response, callback) {
    var self = this;

    self.setContent($(response));
    self.appendContent();
    gmr.helper().initPopovers(self.columnContent);

    self.initEventListeners();

    if(typeof callback === "function"){
      callback(self);
    }
  });

  GMR.Admin.Templates.Column.method("initEventListeners", function () {
    var columnObj = this;

		  columnObj.columnContent.find('.delete-column').on('click', function () {
				  var getParentDiv = $(this).parent('span');
				  gmr.confirmModal.confirm('Entire column will be deleted. Are you sure?', function () {
						  var deleteColumn = getParentDiv.find('input').serialize();
						  gmr.helper().showMask();
						  $.ajax({
								  url: 'delete_column',
								  method: "POST",
								  data: deleteColumn,
								  complete: function () {
										  gmr.helper().hideMask();
								  },
								  success: function (response) {
										  if (response.success) {
												  var removeColumns = getParentDiv.data('removeColumn');
												  $('.' + removeColumns).remove();
												  $('#' + removeColumns).remove();
										  }
								  }
						  });
				  });
		  });

    $(document).on("updateColumn_" + columnObj.id, function (event) {
      var jForm = $(event.target);
      if (gmr.helper().validateForm(jForm)) {
        $.ajax({
          url: columnObj.update_action,
          data: jForm.serialize(),
          method: 'POST',
          success: function (response) {
            columnObj.afterUpdate(response);
          },
          error: function(xhr){
            if(xhr.status !== 200){
              gmr.helper().showAlert(xhr.responseText,'error');
            }
          }
        });
      }
    });

    columnObj.initialized = true;

    columnObj.columnContent.on('orderChanged', function (event, sectionId) {
      var jColumn = $(this)
        , new_order = jColumn.data("order")
        ;

      if (columnObj.order !== new_order) {
        columnObj.order = new_order;
        $.ajax({
          url: columnObj.order_update_action,
          data: {section_id: sectionId, column_id: columnObj.id, order: new_order},
          method: 'POST',
          success: function (response) {

          }
        });
      }
    });
  });

  GMR.Admin.Templates.Column.method("afterUpdate", function (updatedColumnContent) {
    var self = this;

    self.columnContent.find(".columnContent").replaceWith(updatedColumnContent);
    if(typeof(self.onUpdate) === "function"){
      self.onUpdate(self);
    }
  });

  GMR.Admin.Templates.Column.method("initExisting", function (jColumn) {
    var self = this;

    self.setContent(jColumn);
    self.initEventListeners();
  });

  GMR.Admin.Templates.Column.method("appendContent", function () {
    var self = this;

    self.section.find("table > thead > tr:first > th:last").before(self.columnContent);
    self.section.find(".addColumn").data("templateColumnOrder", self.order + 1);
  });

  GMR.Admin.Templates.Column.method("setContent", function (content) {
    var self = this;

    self.columnContent = content;
    self.columnId = self.columnContent.attr("id");
    self.id = self.columnContent.data("id");
    self.order = self.columnContent.data("order");
  });

  GMR.Admin.Templates.Column.method("createInputContainer", function () {
    var self = this;

    return "<td class='" + self.columnId + "'></td>";
  });

  GMR.Admin.Templates.Column.method("reflectCurrentState", function () {
    var self = this;


  });

  return GMR;
})(GMR);