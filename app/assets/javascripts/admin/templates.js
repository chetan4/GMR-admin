// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

//= require mustache
//= require select2
//= require multiselect.min
//= require jquery.dragtable
//= require jquery.rowsorter.min
//= require admin/base
//= require_self
//= require admin/template_sheets
//= require admin/template_sections
//= require admin/template_columns
//= require admin/template_fields
//= require admin/template_inputs

var GMR = (function (GMR) {

  GMR.Admin.Templates = function (config) {
    var self = this;
    self.config = config || {};
    self.on_close_action = "done_editing";
    self.order_update_action = "update_sheet_order";
  };

  GMR.Admin.Templates.method("init", function () {
    var self = this;

    self.sheets = [];
    self.templateContainer = $("#templateContainer");
    self.initScrollTop();

    $(".nav.nav-tabs a:first").tab("show");

		  $('.delete-icon').on("mousedown", function (event) {
				  event.stopPropagation();
		  });

  });

  GMR.Admin.Templates.method("initIndexPage", function (action) {
    var self = this;

    $("#newTemplateForm").find("input[type=submit]").on("click", function (event) {
      event.preventDefault();

      var jSubmitButton = $(this)
        , jForm = jSubmitButton.closest("form")
        , jBenefitTypeInput = $("#newTemplateForm").find("input[name=benefit_type_id]")
        ;

      if (jBenefitTypeInput.val().length > 0) {
        jForm.submit();
      } else {
        gmr.helper().showAlert(["Please select a valid benefit type."], "error");
      }
    });
  });

  GMR.Admin.Templates.method("initShowPage", function (action) {
    var self = this;

    $(".multi-select-tpl-field select").each(function (index, item) {
      var el = $(item);
      if (!item._multi_select_loaded) {
        item._multi_select_loaded = true;
        el.multiselect({
          isCopyCSS: false,
          extraCls: "template-multi-select"
        });
      }
    });
    gmr.helper().initPopupMultiSelects();
  });

  GMR.Admin.Templates.method("initTemplateBuilder", function (action) {
    var self = this
      , gmrTable = self.templateContainer.find('.gmr-table')
      ;
    gmr.helper().initPopovers();
    $(".addNetwork").on("click", function () {
      $('.template').toggleClass('hidden');
      gmr.helper().initLettersOnlyField();
    });

    $(document).on("createSheet", function (event) {
      var jForm = $(event.target);

      if (gmr.helper().validateForm(jForm)) {
        var new_sheet = new GMR.Admin.Templates.Sheet(self.config);
        if (new_sheet.init(self.templateContainer, jForm)) {
          self.sheets.push(new_sheet);
          jForm.get(0).reset();
        }
      }
    });

    var sortable_template_tabs = $('.template_tabs').sortable({
      axis: "x",
      containment: "parent",
      cursor: "move,",
      items: "> li",
      update: function( event, ui ) {
        var tabHash = {template_sheet_ids: {}};
        $(this).find('li').each(function(index, ele){
          tabHash['template_sheet_ids'][$(ele).data('uniqueId')] = index;
        });
        self.changeSheetOrder(tabHash);
      }
    });


    $(window).on("beforeunload", function () {
      $.ajax({url: self.on_close_action, async: false});
    });

    if (action === "edit") {
      self.reflectCurrentState();
    }
  });


  GMR.Admin.Templates.method("reflectCurrentState", function () {
    var self = this;

    $.each($(".tab-content > div"), function () {
      var jTabContent = $(this)
        , new_sheet = new GMR.Admin.Templates.Sheet(self.config)
        ;

      new_sheet.initExisting(jTabContent);
      self.sheets.push(new_sheet);
      new_sheet.reflectCurrentState();
    });

    gmr.helper().initLinkScrollers();
  });

    GMR.Admin.Templates.method("changeSheetOrder", function (template_sheet_ids) {
        var self = this;
        $.ajax({
            url: self.order_update_action,
            data: template_sheet_ids,
            method: 'POST',
            success: function (response) {}
        });
    });

  GMR.Admin.Templates.method("initScrollTop", function () {
    var linkScrollToTop = $('.linkScrollToTop');

    linkScrollToTop.click(function(){
      gmr.helper().scrollTo(0, 300);
    });

    $(window).scroll(function(event) {
      if($(window).scrollTop() > 200){
        linkScrollToTop.fadeIn();
      } else {
        linkScrollToTop.fadeOut();
      }
    });
  });

  return GMR;
})(GMR);



