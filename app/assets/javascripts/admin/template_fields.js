var GMR = (function (GMR) {

  GMR.Admin.Templates.Field = function (config) {
    var self = this;
    self.config = config || {};

    self.add_action = "add_field";
    self.update_action = "update_field";
    self.update_action_attribute = "update_field_attribute";
    self.order_update_action = "update_field_order";

  };

  GMR.Admin.Templates.Field.method("init", function (jSection, fieldForm, callback) {
    var self = this
      ;

    if (self.xhr || self.initialized) {
      return false;
    }

    self.section = jSection;
    self.load(jSection, fieldForm, callback);

    return true;
  });


  GMR.Admin.Templates.Field.method("load", function (jSection, fieldForm, callback) {
    var self = this;

    self.xhr = $.ajax({
      url: self.add_action,
      data: fieldForm.serialize(),
      method: 'POST',
      success: function (response) {
        self.afterLoad(response, callback);
      },
      error: function(xhr){
        if(xhr.status !== 200){
          gmr.helper().showAlert(xhr.responseText, "error");
        }
      }
    });

    return self.xhr;
  });

  GMR.Admin.Templates.Field.method("afterLoad", function (response, callback) {
    var self = this;

    self.setContent($(response));
    self.appendContent();
    gmr.helper().initPopovers(self.fieldContent);


		  self.initEventListeners();

    if (typeof callback === "function") {
      callback(self);
    }
  });

  GMR.Admin.Templates.Field.method("afterUpdate", function (updatedFieldContent) {
    var self = this;

    self.fieldContent.find(".fieldContent").replaceWith(updatedFieldContent);
    if(typeof(self.onUpdate) === "function"){
      self.onUpdate(self);
    }
  });

  GMR.Admin.Templates.Field.method("initEventListeners", function () {
    var fieldObj = this;

		  fieldObj.fieldContent.find('.delete-field').on('click', function () {
				  var getParentDiv = $(this).parent('span');
				  gmr.confirmModal.confirm('Entire field will be deleted. Are you sure?', function () {
						  var deleteField = getParentDiv.find('input').serialize();
						  gmr.helper().showMask();
						  $.ajax({
								  url: 'delete_field',
								  method: "POST",
								  data: deleteField,
								  complete: function () {
										  gmr.helper().hideMask();
								  },
								  success: function (response) {
										  if (response.success) {
												  getParentDiv.closest('tr').remove();
										  }
								  }
						  });
				  });
		  });
		  fieldObj.fieldContent.find('td.fieldAttribute input').on('change', function(){
                var thisVal =  $(this).is(':checked') ? "on" : "off";
                $(this).prev('.alt_hidden').val(thisVal);
                  var data = $(this).parent().find('input').serialize();
				  $.ajax({
						  url: fieldObj.update_action_attribute,
						  data: data,
						  method: 'POST',
						  success: function (response) {
								//  fieldObj.afterUpdate(response);
								  $('.popover').hide();
						  }
				  });
		  });
    $(document).on("updateField_" + fieldObj.id, function (event) {
      var jForm = $(event.target);
      if (gmr.helper().validateForm(jForm)) {
        $.ajax({
          url: fieldObj.update_action,
          data: jForm.serialize(),
          method: 'POST',
          success: function (response) {
            fieldObj.afterUpdate(response);
          },
          error: function(xhr){
            if(xhr.status !== 200){
              gmr.helper().showAlert(xhr.responseText,'error');
            }
          }
        });
      }
    });

    fieldObj.initialized = true;

    fieldObj.fieldContent.on('orderChanged', function (event, sectionId) {
      var jField = $(this)
        , new_order = jField.data("order")
        ;
      if (fieldObj.order !== new_order) {
        fieldObj.order = new_order;
        $.ajax({
          url: fieldObj.order_update_action,
          data: {section_id: sectionId, field_id: fieldObj.id, order: new_order},
          method: 'POST',
          success: function (response) {

          }
        });
      }
    });
  });

  GMR.Admin.Templates.Field.method("initExisting", function (jField) {
    var self = this;

    self.setContent(jField);
    self.initEventListeners();
  });

  GMR.Admin.Templates.Field.method("appendContent", function () {
    var self = this;
    self.section.find("table > tbody").append(self.fieldContent);
    self.section.find(".addField").data("templateFieldOrder", self.order + 1);
  });

  GMR.Admin.Templates.Field.method("setContent", function (content) {
    var self = this;

    self.fieldContent = content;
    self.fieldId = self.fieldContent.attr("id");
    self.id = self.fieldContent.data("id");
    self.order = self.fieldContent.data("order");

  });

  GMR.Admin.Templates.Field.method("getInputContainer", function (columnObj) {
    var self = this
      , jInputContainer = self.fieldContent.find("." + columnObj.columnId)
      ;

    return jInputContainer;
  });

  GMR.Admin.Templates.Field.method("reflectCurrentState", function () {
    var self = this;


  });

  return GMR;
})(GMR);
