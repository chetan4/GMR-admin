var GMR = (function (GMR) {

  GMR.Admin.Templates.Input = function (config) {
    var self = this;
    self.config = config || {};
    self.add_action = "add_input";
    self.update_action = "update_input";

  };

  GMR.Admin.Templates.Input.method("init", function (jInputContainer, sectionObj, fieldObj, columnObj) {
    var self = this
      ;

    if (self.xhr || self.initialized) {
      return false;
    }

    self.inputContainer = jInputContainer;
    self.load(sectionObj, fieldObj, columnObj);

    return true;
  });

  GMR.Admin.Templates.Input.method("load", function (sectionObj, fieldObj, columnObj) {
    var self = this;

    self.xhr = $.ajax({
      url: self.add_action,
      data: {input: {template_section_id: sectionObj.id, template_field_id: fieldObj.id, template_column_id: columnObj.id}},
      method: 'POST',
      success: function (response) {
        self.afterLoad(response);
      },
      error: function (xhr) {
        if (xhr.status !== 200) {
          gmr.helper().showAlert(xhr.responseText);
        }
      }
    });

    return self.xhr;
  });

  GMR.Admin.Templates.Input.method("afterLoad", function (response) {
    var self = this;

    self.setContent($(response));
    self.appendContent();

    gmr.helper().initPopovers(self.inputContent);

    self.initEventListeners();
  });

  GMR.Admin.Templates.Input.method("afterUpdate", function (updatedInputContent) {
    var self = this
      , jScope = $(updatedInputContent);

    self.inputContent.find(".inputContent").replaceWith(updatedInputContent);
    gmr.helper().initPopupMultiSelects(self.inputContent);
    //  gmr.helper().initCurrencyField(jScope.find('input')); TODO: Need to think of new plugin for currency
    //  gmr.helper().initTextOnlyField(jScope);
    //  gmr.helper().initNumberOnlyField(jScope);

  });

  GMR.Admin.Templates.Input.method("reload", function (updatedInputContent) {
    var self = this;

    $.ajax({
      url: self.update_action,
      data: {input_id: self.id},
      method: 'POST',
      success: function (response) {
        self.afterUpdate(response);
      }
    });
  });

  GMR.Admin.Templates.Input.method("initEventListeners", function () {
    var self = this;

    $(document).on("updateInput_" + self.id, function (event) {
        self.submitInput(event);
    });
    self.initialized = true;
  });

  GMR.Admin.Templates.Input.method("initExisting", function (jInput) {
    var self = this;

    self.setContent(jInput);
    self.initEventListeners();
    gmr.helper().initPopupMultiSelects(self.inputContent);
    return true;
  });


		GMR.Admin.Templates.Input.method("appendContent", function () {
				var self = this
						, flag = false
						, checkMandatory = ""
						, checkDeactivate = "";

				self.inputContainer.append(self.inputContent);

				var data = {
						section_id : "",
						input_id : "",
						input_type: "input",
						properties: {
								dropdown : {
										mandatory: "false",
										deactivate: "false"
								},
								input : {
										mandatory: "false",
										deactivate: "false",
										type: '1'
								}
						}
				};

				var getDetails = self.inputContent.closest('tr').find('.fieldContentWrap > span.fieldContent')
						, section_id = getDetails.closest('.section').data('id');

				checkMandatory = getDetails.hasClass('mandatory');
				checkDeactivate = getDetails.hasClass('deactivate');
				if(checkMandatory || checkDeactivate ) {
						flag = true;
				}

				if(flag) {
						data.section_id = section_id;
						data.input_id = self.id;
						if(checkMandatory) {
								data.properties.dropdown.mandatory = "true";
								data.properties.input.mandatory = "true";
								// self.inputContent.find('.inputContent').addClass('mandatory');
						}
						if(checkDeactivate){
								self.inputContent.find('.inputContent').addClass('mandatory');
								data.properties.dropdown.deactivate = "true";
								data.properties.input.deactivate = "true";
						}
			self.submitInput(null, data);
				}
		});

  GMR.Admin.Templates.Input.method("submitInput", function (event, data) {
		  var self = this;
		  if(data && !event){
				  $.ajax({
						 url: self.update_action,
						 data: data,
						 method: 'POST',
						 success: function (response) {
								  self.afterUpdate(response);
								  $('.popover').hide();
						 }
				  });
		  }
		  else {
				  var jForm = $(event.target);

				  if (gmr.helper().validateForm(jForm.find('.tab-pane.active'))) {
        $.ajax({
          url: self.update_action,
          data: jForm.serialize(),
          method: 'POST',
          success: function (response) {
            self.afterUpdate(response);
            $('.popover').hide();
          }
        });
						  //if($('.numberField').is(':checked'))
						  //{
								 // if(isNaN($('textArea').val().trim().replace(/\s/g, ''))) {
										//  $('textArea').closest('.field').append('<div class = "errorMessage" style="display: block">Please enter only numbers</div>');
								 // }else{
										//
								 // }
						  //} else{
								 // $.ajax({
										//  url: self.update_action,
										//  data: jForm.serialize(),
										//  method: 'POST',
										//  success: function (response) {
										//		  self.afterUpdate(response);
										//		  $('.popover').hide();
										//  }
								 // });
						  //}
				  }

		  }
  });

  GMR.Admin.Templates.Input.method("setContent", function (content) {
    var self = this;

    self.inputContent = content;
    self.id = self.inputContent.data("id");
  });

  GMR.Admin.Templates.Input.method("findExistingOrInit", function (jInputContainer, sectionObj, fieldObj, columnObj) {
    var self = this
      , jInput = jInputContainer.find("> div.input")
      ;

    if (jInput.length === 0) {
      return self.init(jInputContainer, sectionObj, fieldObj, columnObj);
    } else {
      return self.initExisting(jInput, sectionObj, fieldObj, columnObj);
    }
  });

  GMR.Admin.Templates.Input.method("reflectCurrentState", function () {
    var self = this;

  });

  return GMR;
})(GMR);