// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

var GMR = (function (GMR) {

  GMR.Admin = function (config) {
    var self = this;

    self.config = config || {};
    self.namespace = "admin";
  };

  GMR.Admin.method("init", function () {
    var self = this
      , controller = self.config.controller || ""
      , action = self.config.action || ""
      , subController = controller.split(self.namespace + "/")[1]
      ;

    if (["companies", "brokers", "carriers", "employers", "data_entries"].indexOf(subController) > -1) {
      self.companies = new GMR.Admin.Companies(self.config.companies);
      self.companies.init();

      if (action === "new" || action === "edit") {
        self.companies.initCompanyForm(action);
        gmr.helper().restrictSpace();
      }
    }

    if (["employees"].indexOf(subController) > -1) {
      self.employees = new GMR.Admin.Employees(self.config.employees);
      self.employees.init();

      if (action === "new" || action === "edit") {
        self.employees.initEmployeeForm(action);
      }
    }

    if(["audit_logs"].indexOf(subController) > -1) {
      self.audit_logs = new GMR.Admin.AuditLogs(self.config.audit_logs);
      self.audit_logs.init();

      if(action === "index"){
        self.audit_logs.initIndexPage(action);
      }
    }

    if(["templates"].indexOf(subController) > -1) {
      self.templates = new GMR.Admin.Templates(self.config.templates);
      self.templates.init();

      if(action === "index"){
        self.templates.initIndexPage(action);
      }

      if(action === "show"){
        self.templates.initShowPage(action);
      }

      if(action === "new" || action === "edit"){
        self.templates.initTemplateBuilder(action);
      }
    }

    if(["platform_files"].indexOf(subController) > -1) {
      self.platform_files = new GMR.Admin.PlatformFiles(self.config.platform_files);
      self.platform_files.init();

      if(action === "index"){
        self.platform_files.initIndexPage(action);
      }

      if(action === "edit"){
        self.platform_files.initEditPage(action);
      }
    }
  });

  return GMR;
})(GMR);