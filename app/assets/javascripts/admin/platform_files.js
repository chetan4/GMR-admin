// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

//= require select2
//= require multiselect.min
//= require admin/base

var GMR = (function (GMR) {

  GMR.Admin.PlatformFiles = function (config) {
    var self = this;
    self.config = config || {};

  };

  GMR.Admin.PlatformFiles.method("init", function (action) {
    var self = this;

  });

  GMR.Admin.PlatformFiles.method("initIndexPage", function (action) {
    var self = this;

    self.searchResults = $("#searchResults");
    self.createForm = $(".createFormWrap > form");
    self.searchForm = $(".searchFormWrap > form");

    self.createForm.find("input.form_control").on("change", function () {
      var jInput = $(this),
          jField = jInput.closest(".field");

      gmr.helper().hideErrorMessages(jField);
    });

    $('.resetSelect2').click(function() {
      self.searchForm.find('.form_control.gmr-select2').select2("data", null);
      self.searchForm.find("input.form_control").val("");
      self.searchResults.html("");
      self.resetHistory();
    });

    self.createForm.find("input[type=submit]").on("click", function (event) {
      var jSubmitButton = $(this),
          jForm = jSubmitButton.closest("form");

      event.preventDefault();

      if (gmr.helper().validateForm(jForm)) {
        jForm.submit();
      }
    });

    self.searchForm.find("button[type=submit]").on("click", function (event) {
      event.preventDefault();
      self.initSearch();
    });


    $("a[href='#search']").on("shown.bs.tab", function () {
      window.location.hash = "#search";
      self.initSearch();
      self.searchForm.find('.form_control.gmr-select2').select2("data", null);
      self.searchForm.find("input.form_control").val("");
    });

    $("a[href='#create']").on("shown.bs.tab", function () {
      window.location.hash = "#create";
      self.searchForm.find('.form_control.gmr-select2').select2("data", null);
      self.searchForm.find("input.form_control").val("");
    });

    self.reflectCurrentState();
  });

  GMR.Admin.PlatformFiles.method("initEditPage", function (action) {
    var self = this;

    self.templateContainer = $("#templateContainer");

    self.templateContainer.find("a[data-toggle='tab']:first").tab("show");
    gmr.helper().initPopupMultiSelects(self.templateContainer);

    $("form#platform_edit_form").on("submit", function (event) {
      var jForm = $(this);

      if (!gmr.helper().validateForm(jForm)) {
        event.preventDefault();
      }
    });
  });

  GMR.Admin.PlatformFiles.method("initSearch", function () {
    var self = this;

    self.searchResults.on("searchResultsLoaded", function () {
      self.searchResults.find(".pagination a").on("click", function (event) {
        event.preventDefault();

        var jAnchor = $(this),
            href = jAnchor.attr("href");

        self.loadSearch(href);
      });
    });

    self.fetchSearchResults(self.searchForm);

    self.searchForm.on("change", function (event) {
      self.fetchSearchResults($(this));
    });
  });

  GMR.Admin.PlatformFiles.method("resetHistory", function (jForm) {
      var historyPath = window.location.pathname;
      if(history.pushState){
        history.pushState({path: historyPath}, '', historyPath);
      }
  });

  GMR.Admin.PlatformFiles.method("fetchSearchResults", function (jForm) {
    var self = this,
        href = "/platform_files/search";

    if (jForm) {
      var data = jForm.serialize(),
          historyPath = window.location.pathname + "?" + data + window.location.hash;

      href = href + "?" + data;
      if(history.pushState){
        history.pushState({path: historyPath}, '', historyPath);
      }
    }

    self.loadSearch(href);
  });

  GMR.Admin.PlatformFiles.method("loadSearch", function (href) {
    var self = this;

    self.searchResults.html("<div class='text-center'>" + gmr.variables().loading_image() + "</div>");
    self.searchResults.load(href, function () {
      self.searchResults.trigger("searchResultsLoaded");
    });
  });

  GMR.Admin.PlatformFiles.method("reflectCurrentState", function () {
    var self = this;

    var jTab = [];
    if (window.location.hash) {
      jTab = $("a[href='" + window.location.hash + "']");
    } else {
      jTab = $(".nav.nav-tabs").find("a[data-toggle='tab']:first");
    }
    jTab.tab("show");
  });

  return GMR;
})(GMR);
