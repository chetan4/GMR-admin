// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

//= require multiselect.min
//= require jquery-mask/jquery.mask.min
//= require admin/base


var GMR = (function (GMR) {

  GMR.Admin.Companies = function (config) {
    var self = this;

    self.config = config;
  };

  GMR.Admin.Companies.method("init", function () {
    var self = this;

  });

  GMR.Admin.Companies.method("initCompanyForm", function (action) {
    var self = this
      , jCompanyForm = (action === "new" ? $("#new_company") : $(".edit_company"))
      ;

    jCompanyForm.find("input[type=submit]").on("click", function (event) {
      var jSubmitButton = $(this)
        , jForm = jSubmitButton.closest("form")
        ;
      event.preventDefault();

      if (gmr.helper().validateForm(jForm)) {
        jForm.submit();
      }
    });

    jCompanyForm.find(".popupMultiSelect").multiselect();
    gmr.helper().initMaskedFields(jCompanyForm);
    gmr.helper().initFileUpload(jCompanyForm);
  });

  GMR.Admin.Companies.method("initEventListeners", function () {
    var self = this;


  });

  return GMR;
})(GMR);