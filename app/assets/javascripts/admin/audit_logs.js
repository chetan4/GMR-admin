// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

//= require admin/base
//= require select2
//= require moment
//= require bootstrap-datetimepicker/bootstrap-datetimepicker.min

var GMR = (function (GMR) {

  GMR.Admin.AuditLogs = function (config) {
    var self = this;
    self.config = config || {};

  };

  GMR.Admin.AuditLogs.method("init", function () {
    var self = this;

  });

  GMR.Admin.AuditLogs.method("initIndexPage", function () {
    var self = this;

    self.submitForm = false;
    self.jFilters = $('.mutliSelect');
    self.jEntityForm = self.jFilters.find("form.entitiesForm");
    self.jAdvancedFilters = self.jFilters.find('.advanced_filter_content');
    self.initFilters();
  });

  GMR.Admin.AuditLogs.method("initFilters", function (event) {
    var self = this,
        formEl = $(".entitiesForm");

    $("#resetFilterForm").click(function(){
        formEl[0].reset();
        $(".entitiesForm").find("input[name='start_date'], input[name='end_date']").val("");
    });

    gmr.helper().initAllSelect2();
    gmr.helper().initDatePickers();

    self.jFilters.find('.advance_filter').on('click', function () {
      self.toggleAdvancedFilters();
    });

    self.jEntityForm.on('change', function () {
      self.enableSearchButtons();
    });

    self.jEntityForm.on('reset', self.resetForm);

    self.jFilters.find('input[type=checkbox][data-name]').on('change', function () {
      var jCheckbox = $(this)
        , dataName = jCheckbox.data('name')
        , jSelect2Element = self.jAdvancedFilters.find('.' + dataName)
        , isChecked = jCheckbox.is(':checked')
        ;

      jSelect2Element.select2('enable', isChecked);
      if (!isChecked) {
        jSelect2Element.select2('data', null);
      }
    });


    $('.dropdown--list').on('click', function (event) {
      self.toggleFilter();
    });
  });

  GMR.Admin.AuditLogs.method("toggleAdvancedFilters", function () {
    var self = this;

    self.jAdvancedFilters.slideToggle("fast", function () {
      if (self.jAdvancedFilters.is(":visible")) {
        self.jEntityForm.find('.search').hide();
        self.jFilters.find('.advance_filter i').removeClass('fa-angle-double-up').addClass('fa-angle-double-down');
      } else {
        self.jEntityForm.find('.search').show();
        self.jFilters.find('.advance_filter i').removeClass('fa-angle-double-down').addClass('fa-angle-double-up');
      }
    });
  });

  GMR.Admin.AuditLogs.method("cleanAdvancedFilters", function () {
    var self = this;

    self.jAdvancedFilters.find('.select2Input').select2('data', null);
  });

  GMR.Admin.AuditLogs.method("toggleFilter", function () {
    var self = this;

    if(self.jFilters.is(":visible")){
      $('.dropdown--list span.glyphicon').addClass("glyphicon-triangle-bottom").removeClass("glyphicon-triangle-top");
    } else {
      $('.dropdown--list span.glyphicon').addClass("glyphicon-triangle-top").removeClass("glyphicon-triangle-bottom");
    }

    self.jFilters.slideToggle('fast');
  });

  GMR.Admin.AuditLogs.method("enableSearchButtons", function () {
    var self = this;

    self.jEntityForm.find('button.searchButton').prop('disabled', false);
    self.jEntityForm.find('button.apply_filters').prop('disabled', false);
  });

  GMR.Admin.AuditLogs.method("disableSearchButtons", function () {
    var self = this;

    self.jEntityForm.find('button.searchButton').prop('disabled', true);
    self.jEntityForm.find('button.apply_filters').prop('disabled', true);
  });

  GMR.Admin.AuditLogs.method("resetForm", function (event) {
    event.preventDefault();

    if(window.location.search.length > 0){
      window.location.search = "";
    } else {
      var jForm = $(this);
      jForm.find("input[type=checkbox]").prop("checked", false).trigger("change");
      gmr.admin().audit_logs.disableSearchButtons();
    }
  });

  return GMR;
})(GMR);