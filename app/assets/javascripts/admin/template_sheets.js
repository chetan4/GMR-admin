/**
 * Created by saloka on 5/7/15.
 */

var GMR = (function (GMR) {

  GMR.Admin.Templates.Sheet = function (config) {
    var self = this;
    self.config = config || {};
    self.add_action = "add_sheet";
    self.update_action = "update_sheet";
    self.sections = [];

  };

  GMR.Admin.Templates.Sheet.method("init", function (jContainer, sheetForm) {
    var self = this;

    if (self.xhr || self.initialized) {
      return false;
    }

    self.tabs = jContainer.find(".nav.nav-tabs.gmr-tabs");
    self.tabContentContainer = jContainer.find(".tab-content");
    self.load(sheetForm);

    return true;
  });

  GMR.Admin.Templates.Sheet.method("initExisting", function (jSheetContent) {
    var self = this;

    self.setContent(jSheetContent);
    self.initEventListeners();

		  $(document).on('change','.form_container .select-one', function () {
				  var currentVal = $(this);
				  if(currentVal.val()) {
						  $('.select-one').not(currentVal).val('false').prop('checked', false);
				  }
		  });
  });

  GMR.Admin.Templates.Sheet.method("load", function (sheetForm) {
    var self = this;

    self.xhr = $.ajax({
      url: self.add_action,
      data: sheetForm.serialize(),
      method: 'POST',
      success: function (response) {
        self.afterLoad(response);
      },
      error: function(xhr){
        if(xhr.status !== 200){
          gmr.helper().showAlert(xhr.responseText,'error');
        }
      }
    });

    return self.xhr;
  });


  GMR.Admin.Templates.Sheet.method("afterLoad", function (response) {
    var self = this;

    self.setContent($(response));
    self.appendContent();
    $('.template_tabs').sortable("refresh").trigger('update');

    gmr.helper().initPopovers(self.sheetContent);

		  $(document).on('change','.form_container .select-one', function () {
				  var currentVal = $(this);
				  if(currentVal.val()) {
						  $('.select-one').not(currentVal).val('false').prop('checked', false);
				  }
		  });

    self.initEventListeners();

  });

  GMR.Admin.Templates.Sheet.method("initEventListeners", function () {
    var self = this;

    $(document).on("createSectionForSheet_" + self.id, function (event) {
      var jForm = $(event.target);

      if (gmr.helper().validateForm(jForm)) {
        var new_section = new GMR.Admin.Templates.Section(self.config);
        if (new_section.init(self.sheetContent, jForm, function(sectionObj){ self.addSectionLink(sectionObj);})) {
          self.sections.push(new_section);
          jForm.get(0).reset();
        }
      }
    });

		  $(document).on("shown.bs.popover", function (event) {
				  $('.section').css('overflow', 'hidden');
		  });

		  $(document).on("hidden.bs.popover", function (event) {
				  $('.section').css('overflow', 'auto');
		  });


		  self.initialized = true;
  });
  GMR.Admin.Templates.Sheet.method("addSectionLink", function (sectionObj) {
    var self = this;

    self.templateSectionBarUL.append(sectionObj.createSectionLink());
  });

  GMR.Admin.Templates.Sheet.method("appendContent", function () {
    var self = this;

    self.tab = $("<li data-unique-id='"+self.id+"'><a href='#" + self.sheetId + "' role='tab' data-toggle='tab'>" + self.sheetName + "</a></li>");
    self.tabContentContainer.append(self.sheetContent);
    self.tabs.append(self.tab);
    self.tab.find("a").tab("show");
  });

  GMR.Admin.Templates.Sheet.method("setContent", function (content) {
    var self = this;
    self.sheetContent = content;
    self.sheetId = self.sheetContent.attr("id");
    self.id = self.sheetContent.data("id");
    self.sheetName = self.sheetContent.attr("data-name");
    self.templateSectionBarUL = self.sheetContent.find('.templateSectionBar > ul');
  });


  GMR.Admin.Templates.Sheet.method("reflectCurrentState", function () {
    var self = this;

    $.each(self.sheetContent.find(".sheetContent > div.section"), function () {
      var jSection = $(this)
        , new_section = new GMR.Admin.Templates.Section(self.config)
        ;
      new_section.initExisting(self.sheetContent, jSection);
      self.sections.push(new_section);
      new_section.reflectCurrentState();
    });
  });

  return GMR;
})
(GMR);
