var GMR = (function(GMR) {

    GMR.FileUploads = function(config) {
        var oUpload = this;
        oUpload.config = config || {};
        oUpload.isValidForm = oUpload.config.isValidForm || false;
        oUpload.jForm = oUpload.config.jForm;
        oUpload.config.allowedFileTypes = ["doc", "docx", "pdf", "xls", "xlsx", "csv", "xlsm"];
        oUpload.limitMultiFileUploadSize = (typeof config !== 'undefined' && config.limitMultiFileUploadSize) ? config.limitMultiFileUploadSize : 10000000;
        oUpload.file_ids = [];
    };


    GMR.FileUploads.method("init", function () {
        var oUpload = this;

        oUpload.selectedFilesTable = $(".selected_files");
        oUpload.fileDetails = $("#fileDetails");

        oUpload.uploadsBlock = $(".gmr_file_upload_files--upload-block");

        oUpload.uploadsBtn = oUpload.uploadsBlock.find(".uploadBtn");
        oUpload.successModal = $("#upload-success");
        oUpload.progressBarWrap = $('.progressWrap');

        oUpload.currentAddedFileSize = 0;
        oUpload.uploadFailedFlag = false;
        oUpload.fileUpload = $("#fileupload");
        oUpload.fileUpload_container = $(".gmr_file_upload_files--fileUploadBtn");
        oUpload.hasmultiple = oUpload.fileUpload_container.children('input').is('[multiple]');

        oUpload.initFileUploadWidget();
    });

    GMR.FileUploads.method("addFileRow", function (currentFile) {
        var oUpload = this
            , jFileSelection = $(Mustache.render($("#selectedFileTr").html(), currentFile))
            ;

        oUpload.uploadsBtn.prop("disabled", false).removeClass("disabled");
        jFileSelection.appendTo(oUpload.selectedFilesTable);
        return jFileSelection;
    });

    GMR.FileUploads.method("clearErrorMessages", function () {
        var oUpload = this;

        $(".uploadFiles_btn_container .errorMessage").fadeOut().remove();
        $(".gmr_file_upload_files--selector .errorMessage").fadeOut().remove();
        $(".gmr_file_upload_files--sub-selection .errorMessage").fadeOut().remove();
    });

    GMR.FileUploads.method("addFileAllowed", function (file) {
        var oUpload = this
            , fileNameSplit = file.name.split(".")
            , fileNameExt = fileNameSplit[fileNameSplit.length - 1]
            ;

        if ((oUpload.currentAddedFileSize + file.size) > $("#fileupload").data("blueimpFileupload").options.limitMultiFileUploadSize) {
            $("<div class='errorMessage' style='margin-left: 15px;'>"+ oUpload.limitMultiFileUploadSize / 1000000 +"MB total file size limit reached.</div>").insertAfter(oUpload.uploadsBlock).fadeIn();
            return false;
        } else {
          $('.errorMessage').remove();
        }

        if (!oUpload.isFileTypeAllowed(fileNameExt)) {
            return false;
        }

        oUpload.currentAddedFileSize += file.size;
        return true;
    });

    GMR.FileUploads.method("isFileTypeAllowed", function (fileExt) {
        var oUpload = this;

        if (oUpload.config.allowedFileTypes.indexOf(fileExt.toLowerCase()) < 0) {
            $("<div class='errorMessage'> Please only upload files with doc, docx, pdf, xls, xlsx, or csv extensions.</div>").prependTo(oUpload.uploadsBlock).fadeIn();
            return false;
        }

        return true;
    });

    GMR.FileUploads.method("uploadFailed", function () {
        var oUpload = this;

        oUpload.uploadFailedFlag = true;
        oUpload.uploadsBtn.text('upload').prop("disabled", false).removeClass("disabled");
        oUpload.fileDetails.prop("disabled", false).removeClass("disabled");
    });

    GMR.FileUploads.method("submitAllowed", function () {
        var oUpload = this;

        oUpload.clearErrorMessages();
        oUpload.uploadsBtn.text('uploading...').prop("disabled", true).addClass("disabled");
        oUpload.fileDetails.prop("disabled", true).addClass("disabled");

        return true;
    });

    GMR.FileUploads.method("fileUploaded", function (fileInfo) {
        var oUpload = this
            , trHTML = Mustache.render($("#uploadedFileTr").html(), fileInfo)
            , jDisplayTable = $(".gmr_file_upload_files--table")
            , jDisplayTableBody = jDisplayTable.find("tbody")
            ;

        jDisplayTableBody.append(trHTML).fadeIn();
    });

    GMR.FileUploads.method("uploadsFinished", function () {
        var oUpload = this;

        if(!oUpload.uploadFailedFlag){
            oUpload.successModal.modal("show");
            oUpload.uploadsBtn.text('upload finished');
        }

        oUpload.fileUpload.prop("disabled", false).removeClass("disabled");
        oUpload.progressBarWrap.fadeOut();
        oUpload.fileUpload_container.removeClass("disabled");

        $(document).trigger("uploadFinished", {ids: oUpload.file_ids});
        setTimeout(function () {
            oUpload.resetForm();
        }, 2000);
    });

    GMR.FileUploads.method("resetForm", function () {
        var oUpload = this;
        oUpload.file_ids = [];
        if(!oUpload.uploadFailedFlag){
            oUpload.clearErrorMessages();
            oUpload.uploadsBtn.text('upload').prop("disabled", true).addClass("disabled");
            oUpload.successModal.modal("hide");
        }

        oUpload.fileUpload.prop("disabled", false).removeClass("disabled");
        oUpload.progressBarWrap.find('.progress-bar').css('width', '0%').attr('aria-valuenow', 0).find("span").html("0%");
    });

    GMR.FileUploads.method("destroyFileUploadWidget", function () {
        var oUpload = this;

        oUpload.fileUpload.fileupload('destroy');
    });

    GMR.FileUploads.method("fileuploaded", function (isfileuploaded) {
        var oUpload = this;
            
        if(isfileuploaded){
            $(document).trigger("fileAddeed", isfileuploaded);
            if (!oUpload.hasmultiple) {
              oUpload.fileUpload_container.addClass("disabled");
            }
        }
    });

    GMR.FileUploads.method("initFileUploadWidget", function () {
        var oUpload = this;
        oUpload.fileUpload.fileupload({
            dataType: 'json',
            acceptFileTypes: /(\.|\/)(doc|docx|pdf|xls|xlsx|csv)$/i,
            limitMultiFileUploadSize: oUpload.limitMultiFileUploadSize,
            add: function (e, data) {
                var currentFile = data.files[0];
                oUpload.clearErrorMessages();
                if (!oUpload.addFileAllowed(currentFile)) {
                    return false;
                }
                data.context = oUpload.addFileRow(currentFile);
                var isFileUploaded = true;
                oUpload.fileuploaded(isFileUploaded);

                data.context.find(".delete").on("click", function () {
                    var self = $(this);
                    gmr.confirmModal.confirm("Are you sure, you want to unselect the file?", function () {
                        data.context = null;
                        self.closest(".selectedFile").fadeOut().remove();
                        oUpload.clearErrorMessages();
                        if ($(".selected_files tr").length < 1) {
                            oUpload.uploadsBtn.text('upload').prop("disabled", true).addClass("disabled");
                            oUpload.fileUpload_container.removeClass("disabled");
                        }
                        oUpload.currentAddedFileSize -= currentFile.size;
                    });
                });

                oUpload.uploadsBtn.click(function (e) {
                    if(oUpload.isValidForm){
                         if(gmr.helper().validateForm(oUpload.jForm)){
                            data.submit();
                         }
                    }else {
                        data.submit();
                    }
                    e.preventDefault();

                });
            },
            submit: function (e, data) {
                oUpload.clearErrorMessages();
                if (!data.context || !data.context.is(":visible") || !oUpload.submitAllowed()) {
                    return false;
                }

                data.context.find(".delete").prop("disabled", true);
                data.formData = oUpload.fileDetails.serializeArray();
                oUpload.uploadFailedFlag = false;
            },
            done: function (e, data) {
                var currentFileInfo = null,
                    errors = data.result.errors || [],
                    onError = function(){
                        oUpload.uploadFailedFlag = true;
	                      $(".gmr_file_upload_files--upload-block .errorMessage").remove();
                        $.each(data.result.errors, function (index, error) {
                          $("<div class='errorMessage' style='margin-left: 15px;'>" + error + "</div>").insertAfter(oUpload.uploadsBlock).fadeIn();
                        });

                        $('.progressWrap').fadeOut(function () {
                            $('.progress-bar').css('width', '0%').attr('aria-valuenow', 0).find("span").html("0%");
                        });
                        data.context.find(".delete").prop("disabled", false);
                        oUpload.uploadsBtn.text('upload').prop("disabled", false).removeClass("disabled");
                    };
                if(errors.length){
                    onError();
                } else {
                    var ids = [];
                    if(data.result.files){
                        for(var i=0; i < data.result.files.length; i++){
                            currentFileInfo = data.result.files[i];
                            if (currentFileInfo) {
                                ids.push(currentFileInfo.id);
                                oUpload.file_ids.push(currentFileInfo.id);
                                oUpload.fileUploaded(currentFileInfo);
                                if(data.context) {
                                    data.context.fadeOut().remove();
                                    delete data.context;
                                    oUpload.currentAddedFileSize -= data.files[0].size;
                                }
                                data.files = [];
                            } else {
                                onError();
                            }
                        }
                        if(ids.length){
                            $(document).trigger("uploadedFileIds", {ids: ids});
                        }
                    }else {
                        $(document).trigger("uploadedFileResponse", {html: data.result.html});
                    }
                }
            },
            stop: function (e, data) {
                oUpload.uploadsFinished();
                data.files = [];
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('.progressWrap').show();
                $('.progress-bar').css('width', progress + '%').attr('aria-valuenow', progress).find("span").html(progress + "%");
                if (progress === 100) {
                    $('.progress-bar').removeClass("active");
                }
            },
            fail: function (e, data) {

              $("<div class='errorMessage' style='margin-left: 15px;'>" + data.response().errorThrown + "</div>").insertAfter(oUpload.uploadsBlock).fadeIn();
                data.context.find(".delete").prop("disabled", false);
                oUpload.uploadFailed();
            }
        });
    });

    return GMR;

})(GMR);
