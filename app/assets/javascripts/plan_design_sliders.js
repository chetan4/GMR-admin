//= require mustache


//Above Method is overriden slider function

var GMR = (function (GMR) {

	GMR.PlanDesignSliders = function (config) {
	var self = this;
	self.config = config;
	};

	GMR.PlanDesignSliders.method("overrideSlider", function (extraParams) {
		// OVERRIDEING SLIDER MOUSE CAPTURE FUNCTION
		$.ui.slider.prototype._mouseCapture = function( event ) {
			var position, normValue, distance, closestHandle, index, allowed, offset, mouseOverHandle,
				that = this,
				o = this.options;

			if ( o.disabled ) {
				return false;
			}

			this.elementSize = {
				width: this.element.outerWidth(),
				height: this.element.outerHeight()
			};
			this.elementOffset = this.element.offset();

			position = { x: event.pageX, y: event.pageY };
			normValue = this._normValueFromMouse( position );
			distance = this._valueMax() - this._valueMin() + 1;
			this.handles.each(function( i ) {
				if(i !== 0){
					var thisDistance = Math.abs( normValue - that.values(i) );
					if (( distance > thisDistance ) ||
						( distance === thisDistance &&
						(i === that._lastChangedValue || that.values(i) === o.min ))) {
						distance = thisDistance;
						closestHandle = $( this );
						index = i;
					}
				}
			});

			allowed = this._start( event, index );
			if ( allowed === false ) {
				return false;
			}
			this._mouseSliding = true;

			this._handleIndex = index;

			closestHandle
				.addClass( "ui-state-active" )
				.focus();

			offset = closestHandle.offset();
			mouseOverHandle = !$( event.target ).parents().addBack().is( ".ui-slider-handle" );
			this._clickOffset = mouseOverHandle ? { left: 0, top: 0 } : {
				left: event.pageX - offset.left - ( closestHandle.width() / 2 ),
				top: event.pageY - offset.top -
				( closestHandle.height() / 2 ) -
				( parseInt( closestHandle.css("borderTopWidth"), 10 ) || 0 ) -
				( parseInt( closestHandle.css("borderBottomWidth"), 10 ) || 0) +
				( parseInt( closestHandle.css("marginTop"), 10 ) || 0)
			};

			if ( !this.handles.hasClass( "ui-state-hover" ) ) {
				this._slide( event, index, normValue );
			}
			this._animateOff = true;
			return true;
		};
	});

	GMR.PlanDesignSliders.method("init", function (extraParams) {
	var self = this;
		self.extraParams = extraParams;
		self.overrideSlider();

	if(self.extraParams.is_editable){
	  self.initListeners();
	}else {
	  self.initSliders();
	}

	});

	GMR.PlanDesignSliders.method("initListeners", function () {
    var self = this;
    self.initNewSliderActions();

    $(document).on('change', '.toggle.cbox', function(){
      if($(this).is(':checked')){
        var slide_el = $(this).parents('.controls').siblings('.parentElement').find('.slider-range');
        var hidden_el = $(this).parents('.controls').siblings('.parentElement').find('input.hidden');
        var SliderVal;
        if (slide_el.length > 0 && hidden_el.length > 0) {
          if(slide_el.hasClass('negative_percent_slider')){
            SliderVal = (slide_el.data('uiSlider').max - (slide_el.data('uiSlider').values(1) - slide_el.data('uiSlider').options.min));
          } else {
            SliderVal = slide_el.data('uiSlider').values()[1];
          }
          hidden_el.val(SliderVal);
        }
      }
    });

  $(document).on("click", '.show_plan_design_sliders', function() {
    var id = $(this).data('id');
    var type = $(this).data('type');
    var rowRemoveBtn = $(this).parents('.plan_data_tr').find('.link_remove_plan');
    self.loadPlanDesignSliders(id, type, rowRemoveBtn);
  });

	$(document).on('click', '.pd_slider_send_button', function(){
      var jqEl = $(this),
          conversation_data = {
              plan_ids: [],
              plan_design_slider_ids: []
          },
          planContainerEl = jqEl.closest('.plan_container'),
          sliders = planContainerEl.find('.show_plan_design_sliders');

      sliders.each(function(){
          var dt = $(this).data();
          conversation_data[(dt.type === "SliderWithoutPlan") ? 'plan_design_slider_ids' : 'plan_ids'].push(dt.id);
      });
    self.sendToConversation(conversation_data);
	});

	$(document).on('click', '.close_plan_design_sliders', function(){
	  $(this).closest('.panel').slideUp("slow", function() {
      $(this).remove();
    });
    $(this).closest('tr').prev('.plan_data_tr').find('.link_remove_plan')
      .removeClass('link_disabled');
	});

var toggleCheckbox = function(jqCheckboxEl){
    var pd_slide_parent = jqCheckboxEl.closest(".pd_slide_height"),
    toggle_status = pd_slide_parent.find(".toggle").prop("checked"),
    current_tick_status = pd_slide_parent.find(".current_tick").children(".fa-check").hasClass("hidden"),
    closest_slider = pd_slide_parent.find(".slider-range, .custom_slider"),
    deductible_family_multiple_slider = pd_slide_parent.find('.deductible_family_multiple_slider');
    dollar = closest_slider.hasClass('dollar_slider');
    new_value_slider = closest_slider.find(".new_value_slider"),
    value_left = closest_slider.data("value-left"),
    value_right = closest_slider.data("value-right"),
    min_value_slider = closest_slider.data("min-value"),
    max_value_slider = closest_slider.data("max-value"),
    slider_set_val_right = value_right;

    if(toggle_status) {
      jqCheckboxEl.parent().addClass('check_active');
      closest_slider.slider("enable");
      closest_slider.find(".ui-slider-handle:last").removeClass("hidden");

      if(closest_slider.data('depend_on_hsa') && !slider_set_val_right) {
        if($(event.target).closest('.plan_slider_form').find('.is_hsa_compatible_button').prop('checked')) {
          slider_set_val_right = closest_slider.data('hsa_min_value');
        } else {
          slider_set_val_right = min_value_slider;
        }
      }

      if (slider_set_val_right == "" && (deductible_family_multiple_slider.length == "") && !dollar ){
        closest_slider.find(".new_value_slider").html(max_value_slider);
      }
      else {
        closest_slider.find(".new_value_slider").html(slider_set_val_right);
      }
      closest_slider.slider("option", "values", [ value_left, slider_set_val_right ]);
      closest_slider.siblings(".hidden").val(slider_set_val_right);
      pd_slide_parent.find(".min_val, .max_val").css("opacity", 1);
      closest_slider.parent().find(".hidden").removeProp("disabled");
      closest_slider.find(".hidden").removeProp("disabled");

      if(closest_slider.hasClass('dollar_slider')) {
        new_value_slider.html(Number(new_value_slider.html()).formatMoney(0));
      } else if (closest_slider.hasClass('negative_percent_slider')) {
        new_value_slider.html(Number(new_value_slider.html()).appendPercentage());
        closest_slider.slider("option", "values", [max_value_slider - (value_left - min_value_slider), max_value_slider - (value_right - min_value_slider)]);
      }
    }
    else  {
      jqCheckboxEl.parent().removeClass('check_active');
      if(!pd_slide_parent.hasClass("out_of_network")) {
        pd_slide_parent.find(".min_val, .max_val").css("opacity", 0.5);
      }
      closest_slider.slider("disable");
      closest_slider.find(".ui-slider-handle:last").addClass("hidden");
      closest_slider.siblings(".hidden").val(closest_slider.data("value-right"));
      closest_slider.parent().find(".hidden").prop("disabled", "disabled");
      closest_slider.find(".hidden").prop("disabled", "disabled");
    }

    if($(jqCheckboxEl).parents('.copay_range_section').length){
      self.toggle_copay_sliders(toggle_status, closest_slider, $(jqCheckboxEl));
    }
};

var toggleCustomCheckbox = function(jqCheckboxEl){
  var pd_slide_parent = jqCheckboxEl.parents('.pd_slide_height'),
      toggle_status = pd_slide_parent.find(".rx_check_toggle").prop("checked"),
      current_tick_status = pd_slide_parent.find(".current_tick").children(".fa-check").hasClass("hidden"),
      closest_slider = pd_slide_parent.find(".slider-range"),
      radio_buttons = pd_slide_parent.find(".radio_button");
      if(toggle_status) {
        jqCheckboxEl.parent().addClass('check_active');
        closest_slider.each(function(index,value) {
          $(this).slider('disable');
        });
        pd_slide_parent.find(".radio_button").removeProp("disabled");
        radio_buttons.each(function() {
          if($(this).prop("checked")) {
            var slider_r = $(this).closest('.col-lg-4').find(".slider-range");
            slider_r.slider('enable');
            slider_r.find(".ui-slider-handle:last").removeClass("hidden");
            if(slider_r.hasClass('.negative_percent_slider')) {
              slider_r.slider("option", "values", [slider_r.data('max-value') - (slider_r.data('value-left') - slider_r.data('min-value')), slider_r.data('max-value') - (slider_r.data('value-right') - slider_r.data('min-value'))]);
            }
          }
          else {
            $(this).closest('.col-lg-4').find(".slider-range").slider('disable');
            $(this).closest('.col-lg-4').find(".slider-range").find(".ui-slider-handle:last").addClass("hidden");
          }

        });

        $(document).on('click', '.radio_button', function() {
          var sliderContainer = $(this).closest(".col-lg-4"),
              slider = sliderContainer.find('.negative_percent_slider');

          sliderContainer.find(".slider-range").slider('enable');
          sliderContainer.find(".slider-range").parent().find("input.hidden").removeProp("disabled");
          if(sliderContainer.find('.negative_percent_slider').length) {
            slider.slider("option", "values", [slider.data('max-value') - (slider.data('value-left') - slider.data('min-value')), slider.data('max-value') - (slider.data('value-right') - slider.data('min-value'))]);
          }
          sliderContainer.find(".slider-range").find(".ui-slider-handle:last").removeClass("hidden");
          sliderContainer.find(".input.hidden").prop('disabled', false);
          $(this).closest(".col-lg-4").siblings('.col-lg-4').find(".slider-range").slider('disable');
          $(this).closest(".col-lg-4").siblings('.col-lg-4').find(".slider-range").find(".ui-slider-handle:last").addClass("hidden");
          $(this).closest(".col-lg-4").siblings('.col-lg-4').find(".slider-range").parent().find("input.hidden").prop("disabled", "disabled");
        });
      } else {
        pd_slide_parent.find("input.hidden").attr("disabled","disabled");
        jqCheckboxEl.parent().removeClass('check_active');
        pd_slide_parent.find(".radio_button").prop("disabled", "disabled");
        closest_slider.find(".ui-slider-handle:last").addClass("hidden");
        closest_slider.closest(".col-lg-4").siblings('.col-lg-4').find(".slider-range").find(".ui-slider-handle:last").addClass("hidden");
        closest_slider.each(function(index, value) {
          $(this).slider('disable');
        });
      }
};

  $(document).on('click', '.toggle', function() {
    if ($(this).is('.oon_check_toggle') && $(this).prop('checked') ) {
      var parentForm = $(this).closest('.plan_slider_form'),
          otherCheckbox = parentForm.find('.oon_check_toggle').not(this),
          thisProp = $(this).prop('checked');
          otherCheckbox
            .prop('checked', !thisProp);
            toggleCheckbox(otherCheckbox);
    }
    toggleCheckbox($(this));
  });

	$(document).on('click', '.rx_check_toggle', function(){
    toggleCustomCheckbox($(this));
	});

  $(document).on('click', '.is_out_of_network_covered_button', function() {
    var jqEl = $(this),
    pd_slide_parent = jqEl.closest(".pd_slide_height"),
    closest_section = jqEl.closest("section"),
    is_out_of_network_covered = pd_slide_parent.find(".is_out_of_network_covered_button").prop("checked"),
    all_checkboxes = closest_section.find(".pd_checkbox input[type='checkbox']:not(:first)"),
    all_oon_sliders = closest_section.find(".slider-range");
    jqEl.parent().toggleClass('check_active');
    if(is_out_of_network_covered) {
      closest_section.find(".pd_slide_height:not(:first)").css("opacity", 1);
      closest_section.find(".pd_slide_height:not(:first)").css("background", "#fff");
      closest_section.find(".min_val, .max_val");
      all_checkboxes.attr("disabled", false);
      all_oon_sliders.each(function(index,value) {
        $(this).slider('disable');
      });
    } else {
      all_checkboxes.each(function(index,value) {
        $(this).parent().removeClass('check_active');
      });
      all_checkboxes.prop("checked", false).attr("checked", false);
      all_checkboxes.prop("checked", false).attr("disabled", "disabled");
      all_oon_sliders.each(function(index,value) {
        // $(this).slider("option", "values", [ $(this).data("value-left"), $(this).data("value-right") ]);
        $(this).slider("option", "values", $(this).data('uiSlider').values());
        $(this).find(".ui-slider-handle:last").addClass("hidden");
        $(this).find(".hidden").val($(this).data("value-right"));
        if (!is_out_of_network_covered) {
          $(this).slider('disable');
          $(this).next('.hidden').prop('disabled', true);
        }
      });
    }
  });

  $(document).on('click', '.is_subject_to_medical_deductible_button', function(){

    var jqEl = $(this),
    pd_slide_parent = jqEl.closest(".pd_slide_height"),
    closest_section = jqEl.closest("section").find(".pd_slide_height").slice(-3),
    is_subject_to_medical_deductible = pd_slide_parent.find(".is_subject_to_medical_deductible_button").prop("checked"),
    last_checkboxes = closest_section.find(".pd_checkbox input[type='checkbox']").slice(-3),
    last_oon_sliders = closest_section.find(".custom_slider, .slider-range").slice(-3);

    if(is_subject_to_medical_deductible) {
      closest_section.css("opacity", 0.5);
      closest_section.find(".min_val, .max_val").css("opacity", 0.5);
      last_checkboxes.each(function() {
        $(this).parent().removeClass('check_active');
      });
      last_checkboxes.prop("disabled", "disabled");
      last_checkboxes.removeProp("checked");
      last_oon_sliders.each(function(index,value) {
        $(this).slider('disable');
        $(this).find(".ui-slider-handle:last").addClass("hidden");
      });
      closest_section.find("input:hidden").attr("disabled", "disabled");
    } else {
      closest_section.find("input:hidden").removeAttr("disabled");
      closest_section.css({"opacity": 1 });
      last_checkboxes.removeProp("disabled");
      last_oon_sliders.each(function(index,value) {
        $(this).slider("option", "values", [ $(this).data("value-left"), $(this).data("value-right") ]);
        $(this).find(".ui-slider-handle:last").removeClass("hidden");
        $(this).find(".hidden").val($(this).data("value-right"));
      });
    }
  });

	$(document).on('click', '.is_subject_to_medical_coinsurance_deductible_button', function(){
    var jqEl = $(this),
    pd_slide_parent = jqEl.closest(".pd_slide_height"),
    closest_section = jqEl.closest("section").find(".pd_slide_height"),
    is_subject_to_medical_coinsurance_deductible = pd_slide_parent.find(".is_subject_to_medical_coinsurance_deductible_button").prop("checked"),
    last_checkboxes = closest_section.find(".pd_checkbox input[type='checkbox']").slice(2),
    last_oon_sliders = closest_section.find(".custom_slider, .slider-range"),
    all_input_boxes = closest_section.not(".just_toggle").find("input.hidden");
    if(is_subject_to_medical_coinsurance_deductible) {
      closest_section.not(".just_toggle").css({"opacity": 0.5 });
      closest_section.find(".min_val, .max_val").css("opacity", 0.5);
      last_checkboxes.prop("disabled", "disabled");
      last_checkboxes.removeProp("checked");
      last_oon_sliders.each(function(index,value) {
        $(this).slider('disable');
        $(this).find(".ui-slider-handle:last").addClass("hidden");
      });
      all_input_boxes.prop("disabled","disabled");
      last_checkboxes.each(function() {
        $(this).parent().removeClass('check_active');
      });
      closest_section.find(".radio_button").each(function() {
        $(this).removeProp("checked");
      });

    } else {
      closest_section.css({"opacity": 1 });
      last_checkboxes.removeProp("disabled");
      last_oon_sliders.each(function(index,value) {
        $(this).slider("option", "values", [ $(this).data("value-left"), $(this).data("value-right") ]);
        if($(this).hasClass('negative_percent_slider')) {
          $(this).slider("option", "values", [ $(this).data("value-left"), $(this).data("value-right") ]);
          $(this).slider("option", "values", [$(this).data("max-value") - ($(this).data("value-left") - $(this).data("min-value")), $(this).data("max-value") - ($(this).data("value-right") - $(this).data("min-value"))]);
        }
        $(this).find(".ui-slider-handle:last").removeClass("hidden");
        $(this).find(".hidden").val($(this).data("value-right"));
      });
    }
	});

	var updateSliderCompatibleState = function(jSlider, is_hsa_not_compatible){
	    var values = jSlider.slider( "option", "values"),
	        sliderData = jSlider.data();
	    if(is_hsa_not_compatible) {
	        values[1] = sliderData.minValue;
	        jSlider.data('is_hsa_compatible', false);
	    } else {
	        values[1] = sliderData.hsa_min_value;
	        jSlider.data('is_hsa_compatible', true);
	    }
	    jSlider.slider( "option", "values", values);
	    var val;
	    if(jSlider.hasClass("dollar_slider")){
	        val = Number(values[1]).formatMoney(0);
	    }else if(jSlider.hasClass("negative_percent_slider")){
	        val = Number(values[1]).appendPercentage();
	    }
	    jSlider.find(".new_value_slider").html(val);
      jSlider.siblings('input.hidden').val(values[1]);
	};
	// use class or id here
	$(document).on('click', '.is_hsa_compatible_button', function(evt) {
	      var is_hsa_compatible_element = $(this),
	          is_hsa_not_compatible = !$(this).prop('checked'),
	          parent_form = $(this).closest('.plan_slider_form'),
            check_value = $(this).prop('checked');

	      gmr.confirmModal.confirm("This will reset some of the slider values. Do you want to proceed&nbsp;?", function(){
            updateSliderCompatibleState(parent_form.find('.in_network_deductible_individual_slider'), is_hsa_not_compatible);
            updateSliderCompatibleState(parent_form.find('.in_network_out_of_pocket_maximum_single_slider'), is_hsa_not_compatible);
	          updateSliderCompatibleState(parent_form.find('.oon_network_deductible_individual_slider'), is_hsa_not_compatible);
            $(is_hsa_compatible_element).parent().toggleClass("check_active");
	      }, function() {
	        $(is_hsa_compatible_element).prop("checked", is_hsa_not_compatible);
	      });
	  });

	$(document).on('click', '.pd_slider_save_button', function(event) {
	  event.preventDefault();
	  var slider_form = $(this).closest('.plan_slider_form'),
	  params = slider_form.serializeArray();
	  params.push(
          {
              name: 'id',
              value: parseInt(($(this).parents().closest("tr").attr("class")).split("_")[2])
          }
      );
        params.push(
            {
                name: 'type',
                value: $(this).parents().closest("tr").data('type')
            }
        );
	  self.savePlanDesignSliders(params);
	});

  $(document).on('click', '.radio_button', function(event) {
    var jqEl = $(this),
        jqParent = jqEl.closest('.col-lg-4'),
        jqSlider = jqParent.find(".slider-range");

      if($(this).prop("checked")) {
        // enable self block
        jqSlider.slider('enable');
        jqSlider.find(".ui-slider-handle:last").removeClass("hidden");
        jqSlider.parent().find("input.hidden").prop("disabled", false);
        jqSlider.closest(".col-lg-4").siblings('.col-lg-4').find(".slider-range").slider('disable');
        jqSlider.closest(".col-lg-4").siblings('.col-lg-4').find(".slider-range").find(".ui-slider-handle:last").addClass("hidden");
        jqSlider.parent().find('.min_val, .max_val').css('opacity', '1');
        jqSlider.parents('.parentElement').siblings('.parentElement')
          .find('.min_val, .max_val').css('opacity', '.5');

        var negativeSliderVal = (jqSlider.data('uiSlider').max - (jqSlider.data('uiSlider').values(1) - jqSlider.data('uiSlider').options.min));
        var positiveSliderVal = (jqSlider.data('uiSlider').values(1) - jqSlider.data('uiSlider').options.min);

        // disable other block
        if (jqSlider.hasClass('.negative_percent_slider')) {
          jqSlider.next('input:hidden').val(negativeSliderVal);
        } else {
          jqSlider.next('input:hidden').val(positiveSliderVal);
        }
        jqParent.siblings('.parentElement').find("input.hidden").prop("disabled", true);

      } else {
        jqSlider.slider('disable');
        jqSlider.find(".ui-slider-handle:last").addClass("hidden");
        jqSlider.parent().find("input.hidden").addProp("disabled", "disabled");
        jqSlider.closest(".col-lg-4").siblings('.col-lg-4').find(".slider-range").slider('enable');
        jqSlider.closest(".col-lg-4").siblings('.col-lg-4').find(".slider-range").find(".ui-slider-handle:last").removeClass("hidden");
      }
    });
	});

	GMR.PlanDesignSliders.method("sendToConversation", function (conversation_data) {
	var self = this,
	url = '/plan_design_sliders/slider_conversation';
	gmr.helper().showMask();
	$.ajax({
	url: url,
	data: conversation_data,
	type: "POST",
	success: function (res) {
	  gmr.helper().hideMask();
	  if(res.success){
	    gmr.helper().showAlert("Plan design modeller information sent successfully to carrier.");
	  }else {
	    gmr.helper().showAlert(res.errors.join(", "), 'error');
	  }
	},
	error: function() {
	  gmr.helper().hideMask();
	}
	});
	});

	GMR.PlanDesignSliders.method("loadPlanDesignSliders", function (id, type, rowRemoveBtn) {
    var self = this,
        url = '/plan_design_sliders/get_slider_details';

  gmr.helper().showMask();

  $('.fa-exclamation-triangle').popover();
  $('body').on('click', function (e) {
    $('[data-toggle="popover"]').each(function () {
      if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
        $(this).popover('hide');
      }
    });
  });
	$.ajax({
	    url: url,
	    data: {
	        id: id,
            type: type
	    },
        type: "GET",
            success: function (res) {
                gmr.helper().hideMask();
               if(res.success){
                 $(".slider_tr_"+id ).children().html(res.html);
                    $(".plan_data_tr_"+id ).find(".error_td > i").addClass("hidden");
                    rowRemoveBtn.addClass('link_disabled');
                 setTimeout(function(){
                   self.initSliders();
                 }, 100);

           } else if(res.errors){

                gmr.helper().showAlert('No Plan design data available!', 'error');

                var error_messages_array = Object.keys(res.errors).map(function(key){return res.errors[key];}),
                    error_messages_content = "";

                    for(var i in error_messages_array) {
                      error_messages_content += (parseInt(i)+1) + ". " + error_messages_array[i] + '<br>';
                    }
                $(".plan_data_tr_"+id ).find(".error_td > i").removeClass("hidden").attr("data-content", error_messages_content);
              } else {

               gmr.helper().showAlert('No Plan design data available!', 'error');
               $(".plan_data_tr_"+id ).find(".error_td > i").removeClass("hidden");
              }
            },
            error: function() {
                    gmr.helper().hideMask();
                  }
          });
	});

	GMR.PlanDesignSliders.method("savePlanDesignSliders", function (params) {
		var self = this, url = '/plan_design_sliders/save_slider_details';
		gmr.helper().showMask();
		$.ajax({
		    url: url,
		    data: params,
		    type: "POST",
		    success: function (res) {
		        gmr.helper().hideMask();
		        if(res.success) {
		            gmr.helper().showAlert('Plan design sliders details successfully saved!');
		        }
		    },
        error: function() {
          gmr.helper().hideMask();
        }
	    });

		return false;

	});

  GMR.PlanDesignSliders.method("toggle_copay_sliders", function (checkbox_checked, closest_slider, checkbox_el) {
    var get_min_max_val = closest_slider.closest('.pd_slide_height').find(".min_val, .max_val");
    var get_hidden_class = closest_slider.parent().find(".hidden") ;
    var get_hidden_input = closest_slider.parent().find("input.hidden") ;
    var get_ui_handle_slider = closest_slider.find(".ui-slider-handle:last");
    var slider_command = checkbox_checked ? 'disable' : 'enable';

    closest_slider.slider(slider_command);

    if(checkbox_checked){
      checkbox_el.parent().addClass('check_active');
      get_min_max_val.css('opacity', '.5');
      get_hidden_class.prop("disabled", true);
      get_hidden_input.prop("disabled", true);
      closest_slider.find(".hidden").prop("disabled", "disabled");
      get_ui_handle_slider.addClass("hidden");

    } else {
      checkbox_el.parent().removeClass('check_active');
      get_min_max_val.css('opacity', '1');
      get_hidden_class.removeProp("disabled");
      get_hidden_input.prop("disabled", false).val(closest_slider.data('uiSlider').values(1));
      closest_slider.find(".hidden").prop("disabled", false);
      get_ui_handle_slider.removeClass("hidden");
    }
  });

GMR.PlanDesignSliders.method("initSliders", function () {
	var self = this;

	var all_sliders = $(".slider-range");

	all_sliders.each(function(index, value) {
	var jqEl = $(this),
	min_value_slider = jqEl.data('min-value'),
	max_value_slider = jqEl.data('max-value'),
	value_left = jqEl.data('value-left'),
	value_right = jqEl.data('value-right'),
	step_value = jqEl.data('value-step'),
	parentEl = jqEl.parent(),
	slider_params;

  // opposite condition for copay sliders only
  if($(this).parents('.copay_range_section').length){
    var is_value_defined = typeof value_right !== 'undefined' && value_right.toString().length > 0;
    parentEl.siblings(".controls").find(".toggle").prop("checked", !is_value_defined);
  } else {
    parentEl.siblings(".controls").find(".toggle").prop("checked", +(value_right));
  }


	slider_params = {
	  range : false,
	  min: min_value_slider,
	  max: max_value_slider,
	  step: step_value,
	  values: [value_left, value_right],
	  slide: function( event, ui ) {
	      var parentEl = $(ui.handle).closest('.ui-slider'),
	          data = parentEl.data(),
	          val = ui.values[1],
            fm_value = parentEl.closest('.pd_slide_height').next().find("input.hidden").val();
            if(fm_value < 1) {
              fm_value = 0.5;
            }
            var hsa_compatible_state = $(this).closest('.panel').find('.is_hsa_compatible_button').prop("checked");
            if(data.hsaDepMax && hsa_compatible_state) {
              if(ui.values[1] > (data.hsa_pharma_number/fm_value)) {
                return false;
              }
            }
	      if(data.depend_on_hsa && data.is_hsa_compatible){ // check value is hsa compatible
          if(ui.values[1] < data.hsa_min_value || ui.values[1] > (data.hsa_pharma_number/fm_value)){
              return false;
          }
	      }
	      if($(ui.handle).index() === 0) {
          return false;
	      }

	      if(parentEl.hasClass("dollar_slider")){
          val = Number(val).formatMoney(0);
	      } else if(parentEl.hasClass("negative_percent_slider")){
          val = Number(val).appendPercentage();
	      }
	    $(this).find(".new_value_slider").html(val);

	  },
	  stop: function(event, ui) {
	    $(this).parent().find(".hidden").val(ui.values[1]);
	  }
	};

	if($(this).hasClass("negative_percent_slider")) {
	  slider_params.min = max_value_slider;
	  slider_params.max = min_value_slider;
    slider_params.values = [max_value_slider - (value_left - min_value_slider), max_value_slider - (value_right - min_value_slider)];
	  slider_params.slide = function( event, ui ) {
      $(this).find(".new_value_slider").html(Number(max_value_slider - (ui.values[1] - min_value_slider)).appendPercentage());

	    if($(ui.handle).index() === 0) {
	      return false;
	    }
	  };
	  slider_params.stop = function(event, ui) {
	    $(this).parent().find(".hidden").val(max_value_slider - (ui.values[1] - min_value_slider));
	  };
	}

	$(this).slider(slider_params);
	$(this).siblings(".hidden").val(value_right);
  $(this).find(".ui-slider-handle").eq(0).append('<span class="current_value_slider"></span>');
	$(this).find(".ui-slider-handle").eq(1).append('<span class="new_value_slider"></span>');

  var blue_slider_value, orange_slider_value;

	if(jqEl.hasClass("dollar_slider")){
    blue_slider_value = value_left || min_value_slider;
    orange_slider_value = value_right || min_value_slider;
	  parentEl.find(".min_val").html(Number(min_value_slider).formatMoney(0));
	  parentEl.find(".max_val").html(Number(max_value_slider).formatMoney(0));
    jqEl.find(".new_value_slider").html(Number(orange_slider_value).formatMoney(0));
	  jqEl.find(".current_value_slider").html(Number(blue_slider_value).formatMoney(0));
	}
	else if(jqEl.hasClass("negative_percent_slider")) {
      blue_slider_value = value_left || max_value_slider;
      orange_slider_value = value_right || max_value_slider;
	    parentEl.find(".min_val").html(Number(min_value_slider).appendPercentage());
	    parentEl.find(".max_val").html(Number(max_value_slider).appendPercentage());
      jqEl.find(".new_value_slider").html(Number(orange_slider_value).appendPercentage());
	    jqEl.find(".current_value_slider").html(Number(blue_slider_value).appendPercentage());
	  }
	  else {
      blue_slider_value = value_left || min_value_slider;
      orange_slider_value = value_right || min_value_slider;
	    parentEl.find(".min_val").html(min_value_slider);
	    parentEl.find(".max_val").html(max_value_slider);
      jqEl.find(".new_value_slider").html(orange_slider_value);
	    jqEl.find(".current_value_slider").html(blue_slider_value);
	  }

	if(!self.extraParams.is_editable){
	  jqEl.slider("disable");
	}

});

	var all_custom_sliders = $(".custom_slider");

all_custom_sliders.each(function(index, value) {

	var items = $(this).data('items'),
      blue_slider_range = isNaN(parseFloat($(this).data('value-left'))) ? items.indexOf($(this).data('value-left')) : items.indexOf(parseFloat($(this).data('value-left'))),
      orange_slider_range = isNaN(parseFloat($(this).data('value-right'))) ? items.indexOf($(this).data('value-right')) : items.indexOf(parseFloat($(this).data('value-right')));

      blue_slider_range = (blue_slider_range < 0 ) ? 0 : blue_slider_range;
      orange_slider_range = (orange_slider_range < 0 ) ? 0 : orange_slider_range;

  var custom_slider_params = {
        min: 0,
        max: items.length-1,
        step: 1,
        values:[blue_slider_range, orange_slider_range],
        slide: function(event, ui){
          if($(ui.handle).index() === 1) {
            return false;
          }
        },
        stop: function(event, ui) {
            $(this).parents().eq(1).find(".hidden").val(items[ui.values[1]]);
         }
      };


	$(this).slider(custom_slider_params);

  if(!self.extraParams.is_editable){
    $(this).slider("disable");
  }

	});

  /* Disabling sliders initially based on current value */
  var toggle_prop = $(".toggle");
  toggle_prop.each(function(index, value){
    var get_pd_slide_height =  $(this).closest(".pd_slide_height");
    var get_slider_container_name = get_pd_slide_height.hasClass('out_of_network');
    var closest_slider = get_pd_slide_height.find(".slider-range");
    var get_min_max_val = closest_slider.closest('.pd_slide_height').find(".min_val, .max_val");
    var get_hidden_class = closest_slider.parent().find(".hidden") ;
    var get_ui_handle_slider = closest_slider.find(".ui-slider-handle:last");

    if(!$(this).prop("checked")) {
      if (get_slider_container_name) {
        closest_slider.slider("enable");
        get_min_max_val.css('opacity', '1');
        get_hidden_class.prop("disabled", "disabled");
        closest_slider.find(".hidden").prop("disabled", "disabled");
        get_ui_handle_slider.addClass("hidden");
      } else {
        closest_slider.slider("disable");
        get_min_max_val.css('opacity', '0.5');
        get_hidden_class.prop("disabled", "disabled");
        closest_slider.find(".hidden").prop("disabled", "disabled");
        get_ui_handle_slider.addClass("hidden");
      }
    } else {
      closest_slider.slider("enable");
      get_min_max_val.css('opacity', '1');
      get_hidden_class.removeProp("disabled");
      closest_slider.find(".hidden").removeProp("disabled");
      get_ui_handle_slider.removeClass("hidden");
    }
    if($(value).parents('.copay_range_section').length){
      self.toggle_copay_sliders($(this).prop("checked"), closest_slider, $(value));
    }
  });

	var rx_toggle_prop = $(".rx_check_toggle");
  rx_toggle_prop.each(function(index, value){
  	var closest_slider = $(this).closest(".pd_slide_height").find(".slider-range");
  	if(!$(this).prop("checked")) {
      closest_slider.each(function() {
        $(this).slider("disable");
        closest_slider.parent().find(".hidden").prop("disabled", "disabled");
        closest_slider.find(".hidden").prop("disabled", "disabled");
        $(this).find(".ui-slider-handle:last").addClass("hidden");
      });
  	} else {
      closest_slider.each(function() {
        $(this).slider("enable");
        closest_slider.parent().find(".hidden").removeProp("disabled");
        closest_slider.find(".hidden").removeProp("disabled");
        $(this).find(".ui-slider-handle:last").removeClass("hidden");
      });
  	}
  });

  var all_hidden_rx_provisions = $('.rx_provisions_section input.hidden');
  all_hidden_rx_provisions.each(function() {
    if($(this).val()){
      $(this).removeProp("disabled");
      $(this).closest(".pd_slide_height").find(".toggle").prop("checked", true);

      $(this).parent().closest(".pd_slide_height").find(".radio_button").removeProp("disabled");
      $(this).parent('.col-lg-4').find(".slider-range").slider('enable');
      $(this).parent().find(".slider-range .ui-slider-handle:last").removeClass('hidden');
      $(this).parent().siblings('.col-lg-4').find(".slider-range").slider('disable');
      $(this).parent().siblings('.col-lg-4').find(".slider-range .ui-slider-handle:last").addClass('hidden');
    }
    else {
      $(this).prop("disabled", "disabled");
      $(this).closest(".pd_slide_height").find(".toggle").prop("checked", false);
      $(this).parent().closest(".pd_slide_height").find(".radio_button").prop("disabled", "disabled");
      $(this).parent().siblings('.col-lg-4').find(".slider-range").slider('enable');
      $(this).parent().find(".slider-range .ui-slider-handle:last").addClass('hidden');
      $(this).parent().siblings('.col-lg-4').find(".slider-range .ui-slider-handle:last").removeClass('hidden');
    }
  });

  var rx_toggle_buttons = $('.rx_provisions_section .rx_check_toggle, .rx_provisions_section .toggle ');
  rx_toggle_buttons.each(function() {
    var hidden_fields = $(this).closest('.pd_slide_height').find('input.hidden');
    if(hidden_fields.first().val() || hidden_fields.last().val()) {
      $(this).prop('checked', true);
      $(this).parent().closest('.pd_slide_height').find('.radio_button').prop('disabled', false);
    }
    else {
      $(this).prop('checked', false);
      $(this).parent().closest('.pd_slide_height').find('.radio_button').prop('disabled');
    }

    if(!$(this).prop('checked')) {
      var close_sliders = $(this).closest('.pd_slide_height').find(".slider-range , .custom_slider");
      close_sliders.each(function() {
        $(this).slider('disable');
        $(this).find('.ui-slider-handle:last').addClass('hidden');
      });
    }
  });

	var current_ticks = $(".current_tick");
	current_ticks.each(function(){
		var parentSlider = $(this).parent().find(".slider-range, .custom_slider");

		parentSlider.each(function(){
			var currentSlider = $(this),
				first_slider_handle = currentSlider.find('.ui-slider-handle:first'),
				is_out_of_range = currentSlider.data('is_out_of_range');

            if($(this).children().hasClass("fa-close")) {
				first_slider_handle.addClass("hidden");
			} else {
				first_slider_handle.removeClass("hidden");
			}
			if(is_out_of_range){
				first_slider_handle.addClass("hidden");
			}
		});
	});

    $(".slider-range, .custom_slider").each(function(){
        var currentSlider = $(this),
            first_slider_handle = currentSlider.find('.ui-slider-handle:first');
        if(!currentSlider.data('value-left')) {
            first_slider_handle.addClass("hidden");
        }
    });

	var out_of_network_section = $(".is_out_of_network_covered_button").closest("section");

	if($('body').find(".is_out_of_network_covered_button").prop("checked")) {
  	out_of_network_section.find(".pd_checkbox input[type='checkbox']:not(:first)");
  	out_of_network_section.find(".pd_slide_height:not(:first)").css("background", "#fff");
  	out_of_network_section.find(".toggle").attr("disabled", false);
	} else {
  	out_of_network_section.find(".pd_slide_height:not(:first)");
  	out_of_network_section.find(".toggle").prop("checked", false).attr("checked", false);
  	out_of_network_section.find(".toggle").prop("checked", false).attr("disabled", "disabled");
	}

	if(!self.extraParams.is_editable){
        $(".plan_slider_form .pd_checkbox input[type='checkbox'], .plan_slider_form input[type='radio']").each(function(){
            var jqEl = $(this);
            jqEl.prop({
                "checked": (jqEl.attr('checked') === 'checked'),
                "disabled":"disabled"});
        });
    var disable_sliders = $('.slider-range, .custom_slider');
    disable_sliders.each(function() {
      $(this).slider('disable');
    });

    $('input.hidden').each(function(index, value) {
      var el = $(this);

      if(el.val()) {
        if($(this).parents('.copay_range_section').length){
          $(this).closest('.parentElement').siblings('.controls')
            .find(".toggle, .rx_check_toggle").prop("checked", true).attr("checked", false);
          $(this).closest('.parentElement').siblings('.controls')
            .find(".toggle, .rx_check_toggle").parent().addClass("check_active").css("opacity", "1");
        } else {
          $(this).closest('.parentElement').siblings('.controls')
            .find(".toggle, .rx_check_toggle").prop("checked", true).attr("checked", true);
          $(this).closest('.parentElement').siblings('.controls')
            .find(".toggle, .rx_check_toggle").parent().addClass("check_active").css("opacity", "0.5");
        }

      } else {
        if($(this).parents('.copay_range_section').length){
          $(this).closest('.parentElement').siblings('.controls')
            .find(".toggle, .rx_check_toggle").prop("checked", true).attr("checked", true);
          $(this).closest('.parentElement').siblings('.controls')
            .find(".toggle, .rx_check_toggle").parent().addClass("check_active").css("opacity", ".5");
        }
      }
    });
	}

  $('.is_subject_to_medical_deductible_button, .is_subject_to_medical_coinsurance_deductible_button').each(function(){
    if($(this).attr('checked') === 'checked'){
      $(this).trigger('click');
    }
  });


  $('.cbox').each(function(index,value) {
    var el = $(this),
    check_value = el.prop('checked');
    if(check_value) {
      el.parent().addClass("check_active");
    }
    else {
      el.parent().removeClass("check_active");
    }
  });


	});

  GMR.PlanDesignSliders.method("initNewSliderActions", function () {
    var self = this;
    var modal_add_new_slider = $('#modal_add_new_slider'),
        employer_company_id = modal_add_new_slider.find('.employer_company_id'),
        carrier_company_id = modal_add_new_slider.find('.carrier_company_id'),
        benefit_type_id = modal_add_new_slider.find('.benefit_type_id'),
        link_remove_plan = $('.link_remove_plan');

    function removePlanRow(thisLink){
      $(thisLink).parents('tr').remove();
    }

    function renderRow(id, name, type){
      var row_markup = Mustache.to_html($("#newSliderRow").html(), {
          id: id,
          type: type,
          name: name
      });
      $('.slider_wizard_active').find('.plan_design_tbody').append(row_markup);
      gmr.helper().scrollToElement($('.plan_design_tbody').find('tr').last(), 400);
    }

    $(document).on('click', '.link_remove_plan', function(e){
      e.preventDefault();
      var thisLink = $(this);
      var data_id = $(this).parents('.plan_data_tr').find('[data-id]').attr('data-id');
      var data_type = $(this).parents('.plan_data_tr').find('[data-type]').attr('data-type');


      gmr.confirmModal.confirm('Do you want to remove this plan ?', function(){
        gmr.helper().showMask();
        $.ajax({
          url: '/plan_design_sliders/delete_new_slider',
          type: 'POST',
          data: {
              id: data_id,
              type: data_type
          },
        })
        .done(function(response) {
          if(response.success){
            removePlanRow(thisLink);
          }
          gmr.helper().hideMask();
        })
        .fail(function() {
        })
        .always(function() {
          gmr.helper().hideMask();
        });
      });
    });

    modal_add_new_slider.on('show.bs.modal', function (e) {
      var parentContainer = $(e.relatedTarget).parents('.plan_container');
      parentContainer.addClass('slider_wizard_active');
      employer_company_id.val(parentContainer.data('employer_company_id'));
      carrier_company_id.val(parentContainer.data('carrier_company_id'));
      benefit_type_id.val(parentContainer.data('benefit_type_id'));
    });

    modal_add_new_slider.on('hide.bs.modal', function (e) {
      $('.slider_wizard_active').removeClass('slider_wizard_active');
      $('.plan_name_field').val('');
      employer_company_id.val('');
      carrier_company_id.val('');
      benefit_type_id.val('');
    });

    $('.form_add_slider').submit(function(event) {
      var add_query_path = $(this).attr('action');
      var form_data = $(this).serialize();
      event.preventDefault();

      if(gmr.helper().validateForm($(this))){
        gmr.helper().showMask();
        $.ajax({
          url: add_query_path,
          type: 'POST',
          data: form_data,
        })
        .done(function(response) {
          if(response.success){
            renderRow(response.id, response.name, response.type);
          }
          gmr.helper().hideMask();
          modal_add_new_slider.modal('hide');
        })
        .fail(function() {
          gmr.helper().hideMask();
        })
        .always(function() {
          gmr.helper().hideMask();
          modal_add_new_slider.modal('hide');
        });
      }
    });
  });

	return GMR;
})(GMR);
