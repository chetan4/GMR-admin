/*
 Helper Functions.

 Author: Harshniket
 Review:
 */
var _Helper = {
  showMask: function () {
    $(".gmr-mask, .gmr-maskLoder").show();
  },
  hideMask: function () {
    $(".gmr-mask, .gmr-maskLoder").hide();
  },
  showAlert: function (msg, error, jwrap) {
    var jMessageEL = jwrap || $(".notify-alert");
    if (msg) {
      if (typeof(msg) !== "string") {
        msg = msg.join("</br>");
      }

      jMessageEL.html(msg);
      if (error === "error") {
        jMessageEL.removeClass("hidden alert_success")
          .addClass('alert-danger').slideDown();
      } else {
        jMessageEL.removeClass("hidden alert-danger")
          .addClass('alert_success').slideDown();
      }
      setTimeout(function () {
        jMessageEL.slideUp();
      }, 3000);
    }
  },
  initConfirmModal: function(){
      var jConfirmModal = $("#confirm-popup");

      jConfirmModal.on('click', 'button', function () {
            if($(this).data('action') === 'ok' && jConfirmModal.data('callback')){
                  jConfirmModal.data('callback')();
            }else if($(this).data('action') === 'yes' && jConfirmModal.data('confirmCallback')){
                  jConfirmModal.data('confirmCallback')();
            }else if(jConfirmModal.data('cancelCallback')){
                  jConfirmModal.data('cancelCallback')();
            }
            jConfirmModal.modal('hide');
      });
      jConfirmModal.on('show.bs.modal', function () {
          var confirmText = jConfirmModal.data('confirmText') || 'Are you sure?';
          jConfirmModal.find('.modal-body').html(confirmText);
      });
      jConfirmModal.confirm = function(text, callback, cancelCallback){
          jConfirmModal.removeClass('alertModal');
          jConfirmModal.addClass('confirmModal');
          jConfirmModal.data('confirmText', text);
          jConfirmModal.data('confirmCallback', callback);
          jConfirmModal.data('cancelCallback', cancelCallback);
          jConfirmModal.modal('show');
      };
      jConfirmModal.alert = function(text, callback){
          jConfirmModal.addClass('alertModal');
          jConfirmModal.removeClass('confirmModal');
          jConfirmModal.data('confirmText', text);
          jConfirmModal.data('callback', callback);
          jConfirmModal.modal('show');
      };
      return jConfirmModal;
  },
  /*
   Init All date picker components.
   Author: Harshniket

   Review:
   */
  initDatePickers: function (jScope, options) {
    var self = this,
      defOptions = {
        format: 'MM/DD/YYYY',
        maxDate: new Date()
      };

    jScope = jScope || $(document);

    $.extend(true, defOptions, options);

    jScope.find(".date").datetimepicker(defOptions);

    jScope.find(".date").on('dp.change', function (evt) {
      var el = $(this),
        data = el.data(),
        date = data.date,
        parentEl = $(this).closest('.gmr-date-parent');
      if (date) {
        if (el.hasClass('gmr-from-date')) {
          parentEl.find('.gmr-to-date').data().DateTimePicker.minDate(date);
        } else if (el.hasClass('gmr-to-date')) {
          parentEl.find('.gmr-from-date').data().DateTimePicker.maxDate(date);
        }
      }
    });

    jScope.find(".date").trigger('dp.change');

    $.each($(".date").find("input"), function (index, inputEle) {
      $(inputEle).unbind('click').on("click", function () {
        var jInputEle = $(this),
          jCalendarSpan = jInputEle.siblings("span");
        jCalendarSpan.trigger("click");
      });
    });

  },
  /*
   Parses data attributes from a DOM Element for Select2

   Author: Harshniket
   Review:
   */
  parseSelect2DataAttr: function (jElement) {
    var elementData = jElement.data()
      , options = {}
      ;

    options.minimumInputLength = elementData.minimumInputLength || 0;

    options.placeholder = elementData.placeholder;

    if (jElement.is("input") && elementData.url) {
      options.ajax = {
        results: function (data, page) {
          return {
            results: data.results
          };
        },
        type: elementData.method || "POST",
        dataType: 'json',
        data: function (term, page) {
          var extraParams = _Helper.getParamsForSelect2Ele(this);
          return $.extend({
            q: term, // search term
            page_limit: 10
          }, extraParams);
        }
      };
      options.ajax.url = elementData.url;
    }

    if (elementData.disableSearch) {
      options.minimumResultsForSearch = -1;
    }

    if (jElement.attr("multiple")) {
      options.multiple = true;
    }

    return options;
  },
  /*
   Initializes all select2 Elements in the provided scope.

   Author: Harshniket
   Review:
   */
  initAllSelect2: function (jScope) {
    var self = this;
    jScope = jScope || $(document);

    var _initAllSelect2 = function (index, element) {
        var jElement = $(element)
          , options = _Helper.parseSelect2DataAttr(jElement)
          ;

        self.initSelect2(jElement, options);
      }
      ;

    var select2Inputs = jScope.hasClass("select2Input") ? jScope : jScope.find("input.select2Input");
    $.each(select2Inputs, _initAllSelect2);

    var select2Options = jScope.hasClass("select2Option") ? jScope : jScope.find("select.select2Option");
    $.each(select2Options, _initAllSelect2);

  },
  /*
   Initializes a particular Select2 component.

   Author: Harshniket
   Review:
   */
  initSelect2: function (jEle, options) {
    var self = this
      , currentData = (options.multiple ? jEle.data("values") : jEle.data("value"))
      ;

    options = options || {};

    if (jEle.select2) {
      jEle.select2(options);
      currentData && jEle.select2("data", currentData);
    } else {
      throw("Please include the select2 js.");
    }

  },
  /*
   Gets params for a particular Select2 element. Useful for cascading Select2 Elements.

   Author: Harshniket
   Review:
   */
  getParamsForSelect2Ele: function (jEle) {
    var self = this
      , dependentOn = jEle.data("dependentOn")
      , params = jEle.data("params") || {}
      ;
    if (!dependentOn) {
      return params;
    }

    var dependentSelectors = dependentOn.split(",");

    $.each(dependentSelectors, function (i, eleSelector) {
      var selectedElement = $(eleSelector)
        , paramKey = selectedElement.data("paramName")
        , paramValue = selectedElement.val();

      if (paramKey) {
        params[paramKey] = paramValue;
      }
    });

    return params;
  },
  /*
   Initializes number only fields components.

   Author: Saloka
   Review:
   */

  disallowSpecialCharacterField: function (jScope) {
    jScope = jScope || $(document);
    $(jScope).on("keypress", ".disallowSpecialCharacters", function (evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode;
      if(!((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode === 8 || charCode === 32 || (charCode >= 48 && charCode <= 57))) {
        return false;
      }
      return true;
    });

    jScope.find(".disallowSpecialCharacters").on('paste', function (e) {
      var val = e.originalEvent.clipboardData.getData("text/plain");
      if (!/[-!$%^&*()@_+|~=`\\#{}\[\]:";'<>?,.\/]/.test(val)) {
        return true;
      }
    });
  },

  /*
   Initializes number only fields components.

   Author: Harshniket
   Review:
   */
  initNumberOnlyField: function (jScope) {
    jScope = jScope || $(document);
    $(jScope).on("keypress", ".numberOnlyField", function (evt) {
      var charCode = (evt.which) ? evt.which : evt.keyCode,
          ctrlKey = evt.ctrlKey,
          metaKey = evt.metaKey;
      if (ctrlKey || metaKey || ([9, 37, 39].indexOf(charCode) !== -1)) {
        return true;
      }
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
      return true;
    });
  },
  /*
   Initializes text only fields components.
   * Allows only characters a-z, A-Z, -, _, Backspace, Tab.

   Author: Harshniket
   Review:
   @updated_by: radhe
   @date: 14 May 2015
   */
  initTextOnlyField: function (jScope) {
    jScope = jScope || $(document);

    jScope.find(".textOnlyField").on("keypress", function (event) {
      var charCode = (event.which) ? event.which : event.keyCode;
      if (((charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 123) && charCode !== 45 && charCode !== 95 && charCode !== 8 && charCode !== 9  && charCode !== 39)) {
        return false;
      }
      return true;
    });

    // if some browser not support paste event for those browser it replace value.
    jScope.find(".textOnlyField").on('change', function (event) {
      if (!/^[a-zA-Z-_'\s]*$/.test($(this).val())) {
        $(this).val("");
      }
    });
    // prevent from pasteing special charactor
    jScope.find(".textOnlyField").on('paste', function (e) {
      var val = e.originalEvent.clipboardData.getData("text/plain");
      if (!/^[a-zA-Z-_'\s]*$/.test(val)) {
        return false;
      }
    });
  },
  /*
   Initializes password fields.
   * Blocks space.

   Author: Vipala
   Review:
   */
  initPasswordOnlyField: function (jScope) {
    jScope = jScope || $(document);

    jScope.find(".passwordOnlyField").on("keypress", function (event) {
      var charCode = (event.which) ? event.which : event.keyCode
        , character = String.fromCharCode(charCode)
        ;

      if (character !== " ") {
        return true;
      }

      return false;
    });
  },
  /*
   Initializes decimals number fields in a given scope.
   ** Dependency: http://jquerypriceformat.com/

   Author: Vipala
   Review:
   */
  initPriceOnlyField: function (jScope) {
    jScope = jScope || $(document);

    $.each(jScope.find(".floatOnlyField"), function (i, ele) {
      var jEle = $(ele)
        , number = jEle.html()
        , floatNumber = Number(number).toLocaleString()
        ;

      jEle.html(floatNumber);
    });

    $.each(jScope.find("input.floatOnlyField"), function (i, ele) {
      var jEle = $(ele)
        , options = jEle.data()
        , jContainer = $("<div>", {class: "floatFieldInputWrap"})
        , jFloatInput = jEle.clone()
        , defaultOptions
        ;

      defaultOptions = {prefix: '', centsLimit: 2};
      options = $.extend({}, defaultOptions, options);

      jEle.removeClass("floatOnlyField");

      jFloatInput.attr("type", "text");
      jFloatInput.removeClass("floatOnlyField");
      jFloatInput.addClass("initializedFloatOnlyField");
      jFloatInput.removeAttr("name");

      jEle.attr("type", "hidden");

      jFloatInput.priceFormat(options);

      jFloatInput.on("keyup", function (event) {
        var jFltInput = $(this)
          , floatField = jFltInput.val()
          , number = Number(floatField.replace(/[^0-9\.]+/g, ""))
          ;

        jEle.val(number);

      });

      jEle.replaceWith(jContainer);
      jContainer.append(jEle);
      jContainer.append(jFloatInput);

    });
  },



  /*
   Initializes text only fields components.
   * Allows only characters a-z, A-Z, -, _, ., space, Backspace, Tab.

   Author: Divya
   Review:

   updated with to allow: ", ',
   updated by: Saloka

   updated to allow: (, ),
   updated on: 8oct 2015
   updated by: Divya
   */

  initLettersOnlyField: function (jScope) {
    jScope = jScope || $(document);

    jScope.find(".lettersOnlyField").on("keypress", function (event) {
      var charCode = (event.which) ? event.which : event.keyCode;
      if (((charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122) && charCode !== 45 && charCode !== 95 && charCode !== 8 && charCode !== 38 && charCode !== 9 && charCode !== 46 && charCode !== 32 && charCode !== 13 && charCode !== 34 && charCode !== 39 && charCode !== 40 && charCode !== 41)) {
        return false;
      }
      return true;
    });

    // if some browsers do not support paste event for those browsers it replaces value.
    jScope.find(".lettersOnlyField").on('change', function (event) {
      if (!/^(?=.*[a-zA-Z])([a-zA-Z]+[_. "'\)\(-]+)*[a-zA-Z&_. "'\)\(-]+$/.test($(this).val())) {
        $(this).val("");
      }
    });
    // prevent from pasteing special charactor
    jScope.find(".lettersOnlyField").on('paste', function (e) {
      var val = e.originalEvent.clipboardData.getData("text/plain");
      if (!/^(?=.*[a-zA-Z])([a-zA-Z]+[_. "'\)\(-]+)*[a-zA-Z&_. "'\)\(-]+$/.test(val)) {
        return false;
      }
    });
  },


  /*
   Initializes text fields for restricting zero as first digit.
   * Restricts Typing '0' as first digit in an input field.

   Author: Divya
   Review:
   updated on: 13may 2016
   updated by: Divya
   */

  initFieldWithoutZero: function (jScope) {
    jScope = jScope || $(document);

    $(jScope).on("keypress", ".fieldWithoutZero", function (event) {
        var target = event.target ? event.target : event.srcElement;
        if(target.value.length == 0 && event.charCode == 48) {
            return false;
        }
        else if (event.charCode == 48 || event.charCode == 127 || event.charCode == 9 || event.charCode == 0) {
          return true;
        }
        else if(isNaN(this.value+""+String.fromCharCode(event.charCode))) {
          return false;
        }
        else if (event.charCode == 46)
        {
          //(event.target.hasAttribute('allowDecimal')) ? true : false;
          if (event.target.hasAttribute('allowDecimal')){
            return true;
          }
          else{
            return false;
          }
        }
        else {
          return true;
        }
    });

    // This will take care of all copy paste(by mouse or key) events in the browser
    $(jScope).on('input propertychange', ".fieldWithoutZero", function(event){
      var el = event.target;
      if(el.hasAttribute('allowDecimal')) {
          if(/^([1-9][0-9]*(\.\d{1,4}))|([1-9][0-9]*\.)|([1-9][0-9]*)$/.test(event.target.value)) {
            return true;
          }
          else {
            el.value = '';
          }
        }
      else if(!/^[1-9][0-9]*$/.test(el.value)) {
        el.value = '';
      }
    });
  },

  /*
   Replaces profile image with default image when error occurs.

   Author: Divya
   Date: 5 Nov 2015

   */

  initImageLoad: function (jScope) {
    jScope = jScope || $(document);
    jScope.find(".dropdown_user_img").error(function () {
      $(this).error = null;
      $(this).attr('src', $(this).data("error_image_path"));
    });
  },

/*
   Initializes text and number fields components.
   * Allows characters numbers 0-9 a-z, A-Z, -, _, ., ", ', space, Backspace, Tab.

   Author: Saloka
   Review:

   */

  initTextNumberFields: function (jScope) {
    jScope = jScope || $(document);

    jScope.find(".textNumberFields").on("keypress", function (event) {
      var charCode = (event.which) ? event.which : event.keyCode;
      if (((charCode < 48 || charCode > 57) && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122) && charCode !== 45 && charCode !== 95 && charCode !== 8 && charCode !== 38 && charCode !== 9 && charCode !== 46 && charCode !== 32 && charCode !== 13 && charCode !== 34 && charCode !== 39)) {
        return false;
      }
      return true;
    });

    // if some browser not support paste event for those browser it replace value.
    jScope.find(".textNumberFields").on('change', function (event) {
      if (!/^(?=.*[0-9a-zA-Z&_. "\-')([0-9a-zA-Z]+[&_. \-"']+)*[0-9a-zA-Z&_. \-"']+$/.test($(this).val())) {
        $(this).val("");
      }
    });
    // prevent from pasteing special charactor
    jScope.find(".textNumberFields").on('paste', function (e) {
      var val = e.originalEvent.clipboardData.getData("text/plain");
      if (!/^(?=.*[0-9a-zA-Z&_. "\-'])([0-9a-zA-Z]+[&_. \-"']+)*[0-9a-zA-Z&_. \-"']+$/.test(val)) {
        return false;
      }
    });
  },

  /**
   *   restrict space from input.
   *   @author: Radhe Nazarkar
   *   @desc: add this attribute "restrict-space" into input and call this function on page init
   **/
  restrictSpace: function () {
    $(document).on("keydown", "input[restrict-space]", function (e) {
      return e.which !== 32;
    });
  },

  initAllowDecimal: function (jScope) {
    jScope = jScope || $(document);

    $(document).on('focus', '[allowdecimal], [allowNegativeDecimal]', function(event){
        var targetEl = $(event.target), targetData, decimalPoints, maxPreDecimalPlaces;
      if (!targetEl.is('[data-numeric="on"]')) {
            targetData = targetEl.data();
            maxPreDecimalPlaces = targetData.maxlength || 10;
            decimalPoints = targetData.decimalPoints || 4,
            allowThouSep = (targetEl[0] && targetEl[0].hasAttribute('thousSeparator')) ? true : false;

          if(targetData.decimalPoints === 0){
              decimalPoints = 0;
          }

          if(targetData.percentInput){
              maxPreDecimalPlaces = 3;
              decimalPoints = 2;
          }
        targetEl.numeric({
            allowSpace         : false,
            allowNumeric       : true,
            allowMinus         : targetEl.is('[allowNegativeDecimal]') ? true : false,
            allowThouSep       : allowThouSep,
            maxDecimalPlaces   : decimalPoints,
            maxPreDecimalPlaces: maxPreDecimalPlaces
        });
        targetEl.attr('data-numeric', 'on');
      }
    });
  },

  /* Thousands separator while typing in an input field
   * Author: Divya
   * add this attribute into text fields */

  initThousandsSeparator: function (jScope) {
    jScope = jScope || $(document);
    var self = this;

    $(window).load(function() {
        $("input[thousSeparator]").val(function(index, value){
          return self.initNumberWithCommas(value);
       });
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $("input[thousSeparator]").val(function(index, value){
          return self.initNumberWithCommas(value);
        });
    });

    $('.modal').on('shown.bs.modal', function (e) {
        $("input[thousSeparator]").val(function(index, value){
          return self.initNumberWithCommas(value);
        });
    });

    $(jScope).on("input propertychange", 'input[thousSeparator]', function (event) {
      $("input[thousSeparator]").val(function(index, value){
        
        if(!($(this).parents().hasClass('employee_percentage_paid_ele'))) {
          return self.initNumberWithCommas(value);
        }
        else {
          return value;
        }
      });
    });

    $(jScope).on("keyup blur", 'input[thousSeparator]', function (event) {
      // skip for arrow keys
      if(event.which >= 37 && event.which <= 40){
       event.preventDefault();
      }

      $(this).val(function(index, value) {
          var checkDecimal = checkDecimals(value);
          if (checkDecimal.length > 1) {
              value = value.slice(0, -1);
          }

          var numberWithoutCommas =  numberDeFormat(value);
          $(this).siblings('input[type="hidden"]').val(numberWithoutCommas).trigger('change');
          return self.initNumberWithCommas(value);
      });
    });

    function checkDecimals(value) {
        var count;
        value.match(/\./g) === null ? count = 0 : count = value.match(/\./g);
        return count;
    }

    

    function numberDeFormat (x){
      x = x.replace(/\,/g,'');
      return x;
    }
  },

  initNumberWithCommas: function (x) {
    var parts = x.toString().split(".");
    if(parts.length === 1) {
      parts[0] = x;
      parts[0] = parts[0].replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    if(parts.length === 2) {
      parts[0] = parts[0].replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      parts[1] = parts[1].replace(/\D/g, "");
    }
      return parts.join(".");
  },

  /* Restrict cut-copy-paste for password fields
   * Author: Divya
   * add this attribute into password fields */

  initRestrictCopy: function (jScope) {
    jScope = jScope || $(document);

    $(jScope).on("cut copy paste", 'input[restrictcopy]', function (e) {
      //e.preventDefault();
    });
  },

  /*
   Initializes percentage fields in a given scope.
   ** Dependency: http://jquerypriceformat.com/

   Author: Harshniket
   Review:
   */
  initPercentageField: function (jScope) {
    jScope = jScope || $(document);

    var percentageFields = jScope.hasClass("percentageField") && (jScope.is("span") || jScope.is("div")) ?
      jScope : jScope.find("span.percentageField, div.percentageField");

    $.each(percentageFields, function (i, element) {
      var jElement = $(element)
        , options = jElement.data()
        , defaultOptions = {prefix: "", suffix: '%', centsLimit: 2}
        ;

      options = $.extend({}, defaultOptions, options);

      jElement.priceFormat(options);

      jElement.on("change", function () {
        $(this).trigger("focusout.price_format");
      });
    });

    var percentageFieldInputs = jScope.hasClass("percentageField") && jScope.is("input") ?
      jScope : jScope.find("input.percentageField");

    $.each(percentageFieldInputs, function (i, ele) {
      var jEle = $(ele)
        , options = jEle.data()
        , jContainer = $("<div>", {class: "percentageFieldInputWrap"})
        , jPercentageInput = jEle.clone()
        , defaultOptions
        ;

      defaultOptions = {prefix: "", suffix: '%', centsLimit: 2, limit: 4};
      options = $.extend({}, defaultOptions, options);

      jEle.removeClass("percentageField");

      jPercentageInput.attr("type", "text");
      jPercentageInput.removeClass("percentageField");
      jPercentageInput.addClass("initializedPercentageField");
      jPercentageInput.removeAttr("name");

      jEle.attr("type", "hidden");

      jPercentageInput.priceFormat(options);

      jPercentageInput.on("keyup", function (event) {
        var jPerInput = $(this)
          , percent = jPerInput.val()
          , number = Number(percent.replace(/[^0-9\.]+/g, ""))
          ;

        if ([37, 38, 39, 40].indexOf(event.keyCode) !== -1) {
          return;
        }

        if (number > 100 || number < 0) {
          number = Math.max(Math.min(number, 100), 0);
          jPerInput.val(number + "%");
        }
        jEle.val(number);
        var cursorPos = (number + "%").length - 1;
        jPerInput.setCursorPosition(cursorPos, cursorPos);

      });

      jEle.replaceWith(jContainer);
      jContainer.append(jEle);
      jContainer.append(jPercentageInput);

    });

  },
  /*
   Initializes Currency Fields.
   ** Dependency: http://jquerypriceformat.com/

   Author: Harshniket Seta
   Review:
   */
  initCurrencyField: function (jScope) {
    jScope = jScope || $(document);

    var currencyFields = jScope.hasClass("currencyField") && (jScope.is("span") || jScope.is("div")) ?
      jScope : jScope.find("span.currencyField, div.currencyField");

    $.each(currencyFields, function (i, element) {
      var jElement = $(element)
        , number = Number(jElement.html())
        , options = jElement.data()
        , defaultOptions = {prefix: '', centsLimit: 0}
        ;

      options = $.extend({}, defaultOptions, options);

      jElement.priceFormat(options);
      jElement.on("change", function () {
        $(this).trigger("focusout.price_format");
      });
    });

    var currencyFieldInputs = jScope.hasClass("currencyField") && jScope.is("input") ?
      jScope : jScope.find("input.currencyField");

    $.each(currencyFieldInputs, function (i, ele) {
      var jEle = $(ele)
        , options = jEle.data()
        , jContainer = $("<div>", {class: "currencyFieldInputWrap"})
        , jCurrencyInput = jEle.clone()
        , defaultOptions = {prefix: '', centsLimit: 0}
        , valueLimit = 8
        ;

      options = $.extend({}, defaultOptions, options);

      if (!options.limit) {
        options.limit = valueLimit + options.centsLimit;
      }

      jEle.removeClass("currencyField");

      jCurrencyInput.attr("type", "text");
      jCurrencyInput.removeClass("currencyField");
      jCurrencyInput.addClass("initializedCurrencyField");
      jCurrencyInput.removeAttr("name");
      if (jEle.val().length === 0) {
        jEle.val(0);
      }

      jEle.attr("type", "hidden");

      jCurrencyInput.priceFormat(options);

      jCurrencyInput.on("keyup", function (event) {
        var jCurrInput = $(this)
          , currency = jCurrInput.val()
          , number = (options.allowNegative ?
            Number(currency.replace(/[^-0-9\.]+/g, "")) : Number(currency.replace(/[^0-9\.]+/g, "")))
          ;

        jEle.val(number);

      });

      jEle.replaceWith(jContainer);
      jContainer.append(jEle);
      jContainer.append(jCurrencyInput);

      jEle.on("change", function () {
        $(this).trigger("focusout.price_format");
      });
    });
  },
  /*
   Initializes Inputs which report length.
   Author: Harshniket

   Review:
   */
  initLengthReportedInput: function (jScope) {
    jScope = jScope || $(document);

    var toReportInputFields = jScope.hasClass("lengthReportedInput") ? jScope : jScope.find("input.lengthReportedInput")
      , renderReport = function (jInputField) {
        var inputVal = jInputField.val()
          , inputValLength = inputVal.length
          , inputId = jInputField.attr("id")
          , maxLength = jInputField.attr("maxLength")
          , reportPrefix = jInputField.data("reportPrefix") || ""
          , paddedReportPrefix = (reportPrefix.length > 0 ? reportPrefix + " " : "")
          , reportPostfix = jInputField.data("reportPostfix") || ""
          , paddedReportPostfix = (reportPostfix.length > 0 ? " " + reportPostfix : "")
          , reportField = $("#" + inputId + "Report")
          , diff = (maxLength - inputValLength)
          , remainingInputLength = Math.max(Math.min(diff, 30), 0)
          ;

        $(reportField).html(paddedReportPrefix + remainingInputLength + paddedReportPostfix);
      }
      ;

    $.each(toReportInputFields, function (index, inputField) {
      var jInputField = $(inputField);

      renderReport(jInputField);
      jInputField.on("keyup", function () {
        renderReport($(this));
      });
    });
  },
  /*
   Inits Form.

   Author: Harshniket
   Review:
   */
  initForm: function (jForm) {
    var self = this;

    jForm.validate();
  },
  /*
   Init Number Only Fields components.

   Author: Harshniket
   Review:
   */
  initAlerts: function () {
    $(".close.close-sm").on("click", function () {
      var jEle = $(this)
        , jAlert = jEle.closest(".alert")
        ;

      jAlert.fadeOut();
    });
  },
  /*
   Author: Harshniket
   Initializing Bootstrap Tooltip
   */
  initTooltips: function (jScope) {
    jScope = jScope || $(document);

    jScope.find("a[rel~=tooltip], .has-tooltip").tooltip();
  },
  /*
   Author: Harshniket
   Initializing Bootstrap Modal
   */
  initModal: function () {
    var jModal = $('.modal');

    if (jModal.length > 0) {
      jModal.modal({
        backdrop: 'static',
        show: false
      });
    }
  },
  /*
   Reads location.hash to change tab, if tab not found fires event with said hash.
   Everything in the hash after '_' is considered event data.

   Author: Harshniket
   Review:
   */
  makeTabsPersistent: function () {
    if (location.hash !== '') {
      var tabPage = $('a[href="' + location.hash + '"][role="tab"]');

      if (tabPage.parent().is(":first-child")) {
        location.hash = "";
      }
      if (tabPage.length > 0) {
        tabPage.tab('show');
      } else {
        setTimeout(function () {
          var hash_split = location.hash.split("#")[1].split("_")
            , eventName = hash_split[0]
            , data = hash_split.slice(1, hash_split.length)
            ;

          $(document).trigger(eventName, data);
        }, 100);
      }
    }
    return $('a[data-toggle="tab"]').on('click', function (e) {
      return location.hash = $(e.target).attr('href').substr(1);
    });
  },

  /*
   Modal show and callbacks helper.

   Author: Harshniket
   Review:
   */
  showModal: function (selector, options, onNegative, onPositive) {
    var jModal = (typeof(selector) === "string" ? $(selector) : selector);

    var existingOptions = jModal.data("bs.modal").options;

    options = $.extend({}, existingOptions, options);

    jModal.data("bs.modal").options = options;
    jModal.modal('show');

    jModal.find(".btn.negative").on("click", function () {
      onNegative();
    });

    jModal.find(".btn.positive").on("click", function () {
      onPositive();
    });
  },
  /*
   Modal hide.

   Author: Harshniket
   Review:
   */
  hideModal: function (selector, options, onNegative, onPositive) {
    var jModal = (typeof(selector) === "string" ? $(selector) : selector);
    jModal.modal('hide');
  },
  /*
   Slides up alerts.
   Author: Harshniket

   Review:
   */
  slideUpAlerts: function (type) {
    $(".alert.alert-" + type).slideUp("slow");
  },

  slideUpNotice: function (type) {
    if ($('.notice_alt').length) {
      $('.notice_alt').delay(5000).slideUp();
    }
  },
  /*
   Generates UUID
   Author: Harshniket

   Review:
   */
  uuid: function () {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c === 'x' ? r : (r & 0x7 | 0x8)).toString(16);
    });

    return uuid;
  },
  /*
   Initializes Renderers.
   Author: Harshniket

   Review:
   */
  initRenderers: function (jScope) {
    jScope = jScope || $(document);

    var jRendererButtons = jScope.is("[data-render-to]") ? jScope : jScope.find("[data-render-to]");

    jRendererButtons.on("click", function (event) {
      var jBtn = $(this)
        , data = jBtn.data()
        , renderTo = data.renderTo
        , jRenderTo = $(renderTo)
        , url = data.url ? data.url.trim() : ""
        , eventName = data.eventName
        ;

      event.preventDefault();

      _Helper.doRender(jRenderTo, url, eventName);
    });
  },
  /*
   Initializes Ajax Forms.
   Author: Harshniket

   Review:
   */
  initAjaxForm: function (jScope) {
    jScope = jScope || $(document);

    var jAjaxForms = jScope.is("form[data-do-ajax]") ? jScope : jScope.find("form[data-do-ajax]");

    jAjaxForms.on("submit", function (event) {
      event.preventDefault();

      var jForm = $(this)
        , url = jForm.attr("action")
        , params = jForm.serialize()
        , method = jForm.attr("method")
        , data = jForm.data()
        , onSuccessEventName = data.onSuccessEventName
        , onErrorEventName = data.onErrorEventName
        , onBeforeSubmitEventName = data.onBeforeSubmitEventName
        ;

      if (!gmr.helper().validateForm(jForm)) {
        jForm.trigger(onErrorEventName, {errors: ["Form invalid."]});
        return false;
      }
      if(onBeforeSubmitEventName){
        jForm.trigger(onBeforeSubmitEventName);
      }
      $.ajax({
        url: url,
        method: method,
        data: params,
        success: function (response) {
          jForm.trigger(onSuccessEventName, response);
        },
        error: function (response) {
          jForm.trigger(onErrorEventName, response);
        }
      });

    });
  },
  /*
   Initializes Ajax Buttons.
   Author: Harshniket

   Review:
   */
  initAjaxButtons: function (jScope) {
    jScope = jScope || $(document);

    var jAjaxButtons = jScope.is("[data-do-ajax]") ? jScope : jScope.find("[data-do-ajax]");

    jAjaxButtons.on('click', function (event) {
      var jAjaxBtn = $(this)
        , url = jAjaxBtn.attr("href")
        , data = jAjaxBtn.data()
        ;

      event.preventDefault();
      $.ajax({
        url: url,
        method: data.method,
        success: function (response) {
          jAjaxBtn.trigger(data.eventName, response);
        },
        error: function () {
        }
      });
    });
  },
  /*
   Initializes tab auto loading.
   Author: Harshniket

   Review:
   */
  initTabAutoload: function (jTabs, once) {
    if (once) {
      jTabs.find("a[data-render-url]").one("show.bs.tab", function (event) {
        var jTabA = $(this);

        _Helper.renderTab(jTabA);
      });
    } else {
      jTabs.find("a[data-render-url]").on("show.bs.tab", function (event) {
        var jTabA = $(this);

        _Helper.renderTab(jTabA);
      });
    }
  },
  /*
   Renders tab.
   Author: Harshniket

   Review:
   */
  renderTab: function (jTab, includeSearchParam) {
    var renderTo = jTab.attr("href")
      , jRenderTo = $(renderTo)
      , renderUrl = jTab.data("renderUrl")
      , eventName = jTab.data("eventName")
      ;

    includeSearchParam = includeSearchParam || 'true';

    _Helper.doRender(jRenderTo, renderUrl, eventName, includeSearchParam);
  },
  /*
   Renders response to a particular DOM element.
   Author: Harshniket

   Review:
   */
  doRender: function (jRenderTo, renderUrl, eventName, includeSearchParam) {
    if (renderUrl && renderUrl.length > 0) {
      if (includeSearchParam === 'true') {
        renderUrl += (renderUrl.indexOf("?") < 0 ? window.location.search : "");
      }
      jRenderTo.html("<div class='text-center'>" + gmr.variables().loading_image() + "</div>");
      jRenderTo.load(renderUrl, function (response, status, xhr) {
        jRenderTo.trigger(eventName, response, status, xhr);
      });
    }
  },
  /*
   Formats credit card field.
   * Inserts delimiter after 4 characters in credit card numbers.
   Author: Harshniket

   Review:
   */
  formatCreditCard: function (jCreditCardInput) {
    jCreditCardInput.keyup(function () {
      var origInputVal = $(this).val()
        , origInputValLength = origInputVal.length
        , inputVal = origInputVal.split("-").join("")
        , inputValLength = inputVal.length
        ;
      if (event.which !== 8 && (inputValLength === 4 || inputValLength === 8 || inputValLength === 12)) {
        $(this).val(inputVal.match(new RegExp('.{1,4}', 'g')).join("-") + "-");
      }
      if (event.which === 8 && (origInputValLength === 5 || origInputValLength === 10 || origInputValLength === 15)) {
        var origInputValArray = origInputVal.split("");
        if (origInputValArray[origInputVal.length - 1] === "-") {
          var newValue = origInputValArray.splice(0, origInputVal.length - 1);
          $(this).val(newValue.join(""));
        }

      }
    });
  },
  hideErrorMessages: function (jScope) {
    jScope = jScope || $(document);

    jScope.find(".errorMessage").fadeOut().remove();
  },
  /*
   Validates form and show error messages.
   Author: Harshniket

   Updated 'allowDecimal' to allow commas
   Updated by: Divya

   Review:
   */
  validateForm: function (jForm, doNotScroll) {
    var success = true
      , jFirstField = null
      ;

    _Helper.hideErrorMessages(jForm);

    $.each(jForm.find(".form_control, .form-control"), function (index, field) {
      var jField = $(field)
        , isFieldRequired = ((jField.attr("required") === "required") && !jField.prop("disabled") && !jField.hasClass('disabled'))
        ;

      if (!isFieldRequired) {
        return true;
      }

      var fieldType = jField.attr("type")
        , isMaskedField = (jField.attr("mask") !== undefined)
        , fieldVal = (isMaskedField ? jField.cleanVal() : (jField.val() || ""))
        , minLength = jField.data('minLength')
        , errorMessage = null
        , regex
        , defaultEmptyErrorMessage = "This field is required."
        , defaultMinLengthMessage = minLength ? "Minimum " + minLength + " digits required." : null
        , defaultInvalidFormatMessage = "Data format is invalid."
        ;

      if(jField.prop("type") === "select-multiple"){// IE <10 FIX
          var values = [];
          jField.find('option').each(function(idx, item){
              if($(item).attr('selected')){
                values.push($(item).val());
              }
          });
          if(values.length === 0 ){
            $.each(jField.val(), function(index, val) {
              values.push(val);
            });
          }
          fieldVal = values;
      }
      fieldVal = (typeof(fieldVal) === "string" ? fieldVal.trim() : fieldVal);
      var fieldValLength = fieldVal.length;
      if (fieldValLength === 0) {

        success = false;
        errorMessage = jField.data("onEmptyMessage") || defaultEmptyErrorMessage;

      } else if (minLength && fieldValLength < minLength) {

        success = false;
        errorMessage = jField.data("minLengthErrorMessage") || defaultMinLengthMessage;

      } else if (fieldType === "url" && !_Helper.isURLEmail(fieldVal) || (fieldVal.indexOf('..') > -1)) {

        success = false;
        errorMessage = jField.data("onFormatErrorMessage") || defaultInvalidFormatMessage;

      } else if (fieldType === "email" && (!_Helper.isValidEmail(fieldVal) || (fieldVal.indexOf('@-') > -1))) {
        success = false;
        errorMessage = jField.data("onFormatErrorMessage") || defaultInvalidFormatMessage;
      } else if (jField.attr('url-input') === "true" && fieldType === "text" && !_Helper.isURLEmail(fieldVal) || (fieldVal.indexOf('..') > -1)) {
        success = false;
        errorMessage = jField.data("onFormatErrorMessage") || defaultInvalidFormatMessage;
      }else if(field.attributes.getNamedItem('allowdecimal')){
          regex = /^(\d{1,10}(\.\d{1,4})?|\d{1,3}(,\d{3})*(\.\d{1,4})?)$/;
          var decimalPoints = jField.data('decimalPoints') || 4;
          decimalPoints = +(decimalPoints);

          if (decimalPoints === 2) {
            regex = /^(\d{1,10}(\.\d{1,2})?|\d{1,3}(,\d{3})*(\.\d{1,2})?)$/;
          }
          if(!regex.test(fieldVal)){
            success = false;
            errorMessage = 'Please enter decimal value.';
          }
      } else if(field.attributes.getNamedItem('allowNegativeDecimal')){
            regex = /^(-?)\d{0,10}(\.\d{1,4})?$/;
            var negativeDecimalPoints = jField.data('decimalPoints') || 4;
            negativeDecimalPoints = +(negativeDecimalPoints);

            if (negativeDecimalPoints === 2) {
              regex = /^(-?)\d{0,10}(\.\d{1,2})?$/;
            }
            if(!regex.test(fieldVal)){
              success = false;
              errorMessage = 'Please enter decimal value.';
            }
      }else if($(field).hasClass('disallowSpecialCharacters')){
        regex = /[-!$%^&*()@_+|~=`\\#{}\[\]:";'<>?,.\/]/;
          if(regex.test(fieldVal)) {
            success = false;
            errorMessage = 'Special characters not allowed.';
          }
        }

      _Helper.showErrorForField(errorMessage, jField);

      if (!success && !jFirstField) {
        jFirstField = jField;
      }
    });

    var customValidator = jForm.data("customValidator");
    if (customValidator && typeof(customValidator) === "function") {
      if (!customValidator(jForm)) {
        success = false;
      }
    }

    if (jFirstField && !doNotScroll) {
      _Helper.scrollToElement(jFirstField.closest(".field"), null, true);
    }
    return success;
  },
  /*
   Checks if email is valid.
   Author: Harshniket

   Review:
   */
  isValidEmail: function (value) {
    return (/^((([^<>()[\]\\.,;:\s@"]{2,})+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|((([a-zA-Z\-0-9]{2,})+\.)+[a-zA-Z]{2,}))$/.test(value));
  },
  /*
   Checks if url is valid.
   Author: Harshniket

   Review:
   */
  isURLEmail: function (value) {
    return (/^((https?):\/\/)?([w|W]{3}\.)?[a-zA-Z0-9\-\._]{2,}\.[a-zA-Z]{2,}(\.[a-zA-Z]{2,})?$/.test(value));
  },
  /*
   Shows error message for a particular field.
   Author: Harshniket

   Review:
   */
  showErrorForField: function (errorMessage, jField) {
    var fieldWrap = jField.closest(".field,.form-group");

    if (errorMessage) {
      var jErrorMessage = $("<div class='errorMessage'>" + errorMessage + "</div>");
      fieldWrap.append(jErrorMessage);
      jErrorMessage.fadeIn();
    }
  },
  /*
   Initialize masked field.
   Author: Harshniket

   Review:
   */
  initMaskedFields: function (jScope) {
    jScope = jScope || $(document);

    var maskedFields = jScope.attr("mask") ? jScope : jScope.find("[mask]")
      ;
    $.each(maskedFields, function (index, maskedField) {
      var jMaskedField = $(maskedField)
        , mask = jMaskedField.attr("mask")
        , options = jMaskedField.data()
        ;

      jMaskedField.mask(mask, options);
    });

  },
  /*
   Initialize file upload field.
   Author: Harshniket

   Review:
   */
  scrollToElement: function (jScrollTo, scrollSpeed, reduceFixedHeaderHt) {
    if (!jScrollTo || jScrollTo.length === 0) {
      return false;
    }

    var scrollVal = $(jScrollTo).offset().top
      ;

    if (scrollVal && reduceFixedHeaderHt) {
      scrollVal = scrollVal - $(".site_head_fixed").height();
    }

    _Helper.scrollTo(scrollVal, scrollSpeed);
  },
  /*
   Scrolls to a particular value on the window.
   Author: Harshniket

   Review:
   */
  scrollTo: function (scrollVal, easeTime) {
    easeTime = easeTime || 0;

    $('html, body').animate({
      scrollTop: scrollVal
    }, easeTime);
  },
  /*
   Initializes link scrollers.
   Author: Harshniket

   Review:
   */
  initLinkScrollers: function (jScope) {
    jScope = jScope || $(document);

    jScope.on('click', '.linkScroller', function (event) {
      var jLink = $(this)
        , scrollSpeed = (jLink.data('speed') || 1000)
        , scrollToId = jLink.data('target')
        , jScrollTo = $(scrollToId)
        ;
      _Helper.scrollToElement(jScrollTo, scrollSpeed, true);
    });
  },
  /*
   Adds url to history.
   Author: Harshniket

   Review:
   */
  addToHistory: function (URL) {
    if (URL) {
      history.pushState(null, null, URL);
    }
  },
  /*
   Attaches handlers to popover content.
   Author: Harshniket

   Review:
   */
  initPopoverContentEventListeners: function (jPopoverLink, jPopoverContent) {

    jPopoverContent.find("form").on("submit", function (event) {
      event.preventDefault();

      var jForm = $(this)
        , doValidate = jForm.data("doValidate")
        , data = jForm.data()
        , eventName = data.eventName
        , doNotScroll = data.doNotScroll
        ;

      if (doValidate === undefined) {
        doValidate = true;
      }

      if (doValidate && _Helper.validateForm(jForm, doNotScroll)) {
        if (eventName) {
          jForm.trigger(eventName);
          jPopoverLink.popover("hide");
        }
      }else {
        jForm.trigger(eventName);
      }
    });
  },
  /*
   Parses popover options.
   Author: Harshniket

   Review:
   */
  parsePopoverOptions: function (jPopover) {
    var defOptions = {}
      , options = jPopover.data()
      , finalOptions = $.extend({}, defOptions, options)
      ;

    if (options.contentTemplateSelector) {
      finalOptions.content = function () {
        var jSelectorTemplate = $(options.contentTemplateSelector);

        return Mustache.render(jSelectorTemplate.html(), options);
      };

      finalOptions.html = true;
    }

    if (options.extraCls) {
      jPopover.data('template', '<div class="popup-div popover ' + options.extraCls + '" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>');
    }

    if (options.contentUrl) {
      finalOptions.content = function () {
        var url = options.contentUrl;

        return $.ajax({url: url, async: false}).responseText;
      };

      finalOptions.html = true;
    }

    if (["top", "bottom", "left", "right", "forceRight"].indexOf(options.placement) > 0) {
      finalOptions.placement = function (context, source) {
        var position = $(source).position();

		      if (options.placement === "forceRight"){
				      return "right";
		      }

        if (options.placement !== "left" && position.left > window.innerWidth - 475) {
          return "left";
        }

        if (options.placement !== "right" && position.left < 0) {
          return "right";
        }

        if (options.placement !== "bottom" && position.top < 110){
         return "bottom";
        }

        if (options.placement !== "top" && position.top > 110){
         return "right";
        }

        return options.placement;
      };
    }

    return finalOptions;
  },
  /*
   Initializes popovers.
   Author: Harshniket

   Review:
   */
  initPopovers: function (jScope) {
    jScope = jScope || $(document);

    $.each(jScope.find('[data-toggle="popover"]'), function () {
      var jPopoverLink = $(this);

      if (jPopoverLink.hasClass('prevent-mouse-down')) {
        jPopoverLink.on("mousedown", function (event) {
          event.stopPropagation();
        });
      }

      jPopoverLink.popover(_Helper.parsePopoverOptions(jPopoverLink));

      jPopoverLink.on("show.bs.popover", function (event) {
        $('[data-toggle="popover"]').not(this).popover("hide");
      });

      jPopoverLink.on("shown.bs.popover", function (event) {
        var jPopoverLink = $(this)
          , jPopover = $("#" + $(this).attr("aria-describedby"))
          , jPopoverContent = jPopover.find(".popover-content")
          , popoverWidth = jPopoverLink.data("popoverWidth")
          ;

        if (popoverWidth) {
          jPopoverContent.css({width: popoverWidth});
        }

        _Helper.initPopoverContentEventListeners(jPopoverLink, jPopoverContent);
        var inputFieldContent = jPopoverContent.find("input.form-control:first, input.form_control:first");
        var val = inputFieldContent.val() || "";
        var inputFieldContentLength = val.length;
        inputFieldContent.focus();
		       try { inputFieldContent[0].setSelectionRange(inputFieldContentLength, inputFieldContentLength); } catch (e) {
				       }
       // inputFieldContent[0].setSelectionRange(inputFieldContentLength, inputFieldContentLength);
      });
    });

    $(document).on('click', function (event) {
      //did not click a popover toggle, or icon in popover toggle, or popover
      if ($(event.target).data('toggle') !== 'popover' &&
        $(event.target).parents('[data-toggle="popover"]').length === 0 &&
        $(event.target).parents('.popover.in').length === 0) {
        $('[data-toggle="popover"]').popover('hide');
      }
    });
  },

  initPopupMultiSelects: function (jScope) {
    jScope = jScope || $(document);

    jScope.find(".multi-select-tpl-field").on("click", function () {
      var jMultiSelectWrap = $(this);

      jMultiSelectWrap.find("select").multiselect("show");
    });
    jScope.find(".multi-select-tpl-field select").multiselect();
  },
  /*
   Initializes file upload field.
   Author: Harshniket

   Review:
   */
  initFileUpload: function (jScope) {
    jScope = jScope || $(document);
    var fileUploadElements = jScope.find("input[type='file']")
      , validateSelectedFile = function (fileInput) {
        var jFileInput = $(fileInput)
          , maxFileSize = jFileInput.data("maxFileSize")
          , selectedFile = fileInput.files[0] || {size: 0}
          , selectedFileSize = selectedFile.size
          ;

        if (selectedFileSize > maxFileSize) {
          jFileInput.val("");
          var errorMessage = jFileInput.data("onFileSizeLimitError");
          if (!errorMessage) {
            var limitInMB = maxFileSize / (1024 * 1024);
            errorMessage = "Please upload a file under " + limitInMB + " MB";
          }
          var jErrorMessage = $("<div class='errorMessage'>" + errorMessage + "</div>");
          var jField = jFileInput.closest(".field");
          jField.find(".errorMessage").fadeOut().remove();
          jField.append(jErrorMessage);
          jErrorMessage.fadeIn();
          return false;
        }
        return true;
      }
      , showSelectedFileName = function (fileInput) {
        var jFileInput = $(fileInput)
          , selectedFile = fileInput.files[0] || {}
          , name = selectedFile.name
          , jUploadContainer = jFileInput.closest(".uploadContainer")
          , jSelectedFileInfo = jUploadContainer.find(".selectFileInfo")
          ;

        if (name) {
          jSelectedFileInfo.find("label").html(name);
          jSelectedFileInfo.show();
        } else {
          jSelectedFileInfo.find("label").html("");
          jSelectedFileInfo.hide();
        }
      }
      ;

    $.each(fileUploadElements, function (index, element) {
      var jElement = $(element);

      jElement.on("change", function (event) {
        if (validateSelectedFile(this)) {
          showSelectedFileName(this);
        }
      });

      jElement.closest(".uploadContainer").find(".removeSelectedFile").on("click", function () {
        var jRemoveSelectedFile = $(this)
          , jUploadContainer = jRemoveSelectedFile.closest(".uploadContainer")
          , jFileInput = jUploadContainer.find("input[type='file']")
          , confirmation = confirm("Are you sure you want to deselect the file?")
          ;

        if (confirmation) {
          jFileInput.val("").change();
        }
      });
    });
  },
  createCookie: function (name, value, days) {
    var expires = "";

    if (days) {
      var date = new Date();
      date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
      expires = "; expires=" + date.toGMTString();
    }

    document.cookie = name + "=" + value + expires + "; path=/";
  },
  readCookie: function (name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) === ' ') {
        c = c.substring(1, c.length);
      }
      if (c.indexOf(nameEQ) === 0) {
        return c.substring(nameEQ.length, c.length);
      }
    }
    return null;
  },
  eraseCookie: function (name) {
    _Helper.createCookie(name, "", -1);
  },

  initTDSum : function(config){
      var inputWrapCls = config.parentCls + ' ' + config.inputCls;

      $(document).on('keyup', inputWrapCls, function(){
          var parentEl = $(this).closest(config.parentCls),
              sumTotal = 0,
              inputVal,
              val,
              valueFound = false,
              sumEl = parentEl.find(config.sumTotalCls);

          parentEl.find(config.inputCls).each(function(index, el) {
            if(el.hasAttribute('thousSeparator')){
              el.value = el.value.replace(/\,/g,'');
            }
            val = $(el).val();
            inputVal = Number(val);
            if(!valueFound && val){
                valueFound = true;
            }
            sumTotal += inputVal;
          });
          if(valueFound){
              sumEl.val(sumTotal);
          }else {
              sumEl.val(sumTotal || "" );
          }
          sumEl.trigger('keyup');
          $('input[thousSeparator]').trigger('change');

          $(document).on("change", 'input[thousSeparator]', function (event) {

            // skip for arrow keys
            if(event.which >= 37 && event.which <= 40){
             event.preventDefault();
            }

            $(this).val(function(index, value) {
                var checkDecimal = checkDecimals(value);
                if (checkDecimal.length > 1) {
                    value = value.slice(0, -1);
                }
                return self.initNumberWithCommas(value);
            });
          });

          function checkDecimals(value) {
              var count;
              value.match(/\./g) === null ? count = 0 : count = value.match(/\./g);
              return count;
          }

      });
  },
  initConvSliderInfo: function(){
    var self = this;
    $(document).on('click', '#chat_content a[data-href-type="slider"]', function(e){
      self.showMask();
      e.preventDefault();
      var jqEl = $(this);
      $('#getConvSliderInfo').remove();
      $.ajax({
        url: '/conversations/plan_design_sliders_info',
        data: jqEl.data()
      })
      .done(function(res) {
        if(res.success){
            $("body").append(res.html);
            $('#getConvSliderInfo').modal('show');
            $('#getConvSliderInfo').on('shown.bs.modal', function(){
                self.plan_design_sliders =  new window.GMR.PlanDesignSliders();
                self.plan_design_sliders.init({
                    is_editable: false
                });
            });
        }
      })
      .always(function() {
        self.hideMask();
      });
    });
  }
};


/*
 Helper Singleton wrapper.

 Author: Harshniket
 Review:
 */
var GMR = (function (GMR) {

  GMR.Helper = (function () {
    var instance;

    function createInstance() {
      return _Helper;
    }

    return {
      get: function () {
        if (!instance) {
          instance = createInstance();
        }
        return instance;
      }
    };
  })();

  return GMR;
})(GMR);
