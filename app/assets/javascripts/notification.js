/*

 Author: Saloka
 Review:
 */

var GMR = (function (GMR) {

  GMR.Notification = function (config) {
    var self = this;

    self.config = config;
  };
  GMR.Notification.method('init', function () {
    var self = this
      , jNotification = $("#notification")
      // Setting timeout of 10min for all environment
      // timeout =  self.config.env == 'development' ?  (60000 * 10) : 60000 
      , timeout = 600000
      , countUrl = jNotification.data("countUrl")
      ;

    jNotification.find(".notifications-reload").on('click', function (event) {
      var jAjaxBtn = $(this)
        , url = jAjaxBtn.attr("href")
        , renderTo = jAjaxBtn.data("renderTo")
        , jRenderTo = $(renderTo)
        ;

      event.preventDefault();
      $('#notificationCount').addClass('hidden');
      jRenderTo.load(url, function () {
        $('.notification-dropdown').find('.ajax-loader').hide();
      });
    });

    var interval = setInterval(function () {
      if(!countUrl){
          clearInterval(interval);
          return false;
      }
      var wrapper = jNotification.find(".notification-count");
      $.ajax({
        url: countUrl,
        success: function (response) {
          if (parseInt(response) === 0) {
            jNotification.find('#notificationCount').addClass('hidden');
          } else {
            jNotification.find('#notificationCount').removeClass('hidden');
          }
        }
       });
    }, timeout);
  });

  return GMR;
})(GMR);



/*
 Author: Saloka
 Review:
 */

