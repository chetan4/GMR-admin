# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default("")
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default("0"), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  type                   :string(255)
#  company_id             :integer
#  created_at             :datetime
#  updated_at             :datetime
#  invitation_token       :string
#  invitation_created_at  :datetime
#  invitation_sent_at     :datetime
#  invitation_accepted_at :datetime
#  invitation_limit       :integer
#  invited_by_id          :integer
#  invited_by_type        :string
#  invitations_count      :integer          default("0")
#  failed_attempts        :integer          default("0")
#  unlock_token           :string
#  locked_at              :datetime
#  name                   :string           not null
#  work_number            :string           not null
#  cell_number            :string           not null
#  name_prefix            :string(10)
#  work_extension         :string(6)
#  first_name             :string(30)       not null
#  middle_name            :string(30)
#  last_name              :string(30)       not null
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_invitation_token      (invitation_token) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_unlock_token          (unlock_token) UNIQUE
#

require 'test_helper'

class UserTest < ActiveSupport::TestCase

  def setup
    @user = users(:default)
    end

  def teardown
    @user = nil
  end

  test "should be valid" do
    assert @user.valid?
  end

  # TODO: add required validation in model as well in migration

  # test "first name should not be blank" do
  #   @user.first_name = "   "
  #   assert_not @user.valid?
  # end
  #
  # test "middle name should not be blank" do
  #    @user.middle_name = "   "
  #    assert_not @user.valid?
  # end
  #
  # test "last name should not be blank" do
  #    @user.last_name = "   "
  #    assert_not @user.valid?
  # end

  test "name should combination of first_name, middle_name and last_name" do
    @user.first_name = 'first'
    @user.middle_name = 'middle'
    @user.last_name = 'last'
    assert_equal 'first middle last', @user.name
   end

  test "email should be present" do
    @user.email = "     "
    assert_not @user.valid?
  end

  test "email address should be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end

  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end

  test "password should match criteria (Smallchar, Capital, number)" do
    @user.password = @user.password_confirmation = 'test123'
    assert_not @user.valid?
  end

  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@foo.com user_1@foo.org example.user@foo.com]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end

  #TODO Verify foo@bar_baz.com, foo@bar+baz.com
  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example. ]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end

  test 'has full work number with extension' do
    @user.work_extension = '123'
    assert_equal @user.full_work_number, "#{@user.work_number} extn. #{@user.work_extension}"
  end

  test 'has full work number if no extension present' do
    assert_equal @user.full_work_number, "#{@user.work_number}"
  end

  test 'returns full name' do
    assert_equal @user.full_name, "#{@user.name_prefix} #{@user.name}"
  end

end


