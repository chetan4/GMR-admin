# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "./log/cron_log.log"
require File.expand_path(File.dirname(__FILE__) + "/environment")
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever


every 1.day, at: '2.00 am' do
  rake "db:pg_dump"
end

every 5.minutes do
  rake "employee_mark_terminated", :environment => Rails.env
end