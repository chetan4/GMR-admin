require File.expand_path('../boot', __FILE__)

require 'rails/all'
require 'wicked_pdf'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Gmr
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # config.autoload_paths += %W(#{config.root}/lib)

    config.autoload_paths += Dir[ Rails.root.join('app', 'services', '**/'),
                                  Rails.root.join('lib', '**/'),
                                  Rails.root.join('notifications', '**/')]

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true

    config.assets.paths << "#{Rails.root}/lib/assets/stylesheets"
    config.assets.paths << "#{Rails.root}/lib/assets/images"
    config.assets.paths << "#{Rails.root}/lib/assets/javascripts"
    config.middleware.use WickedPdf::Middleware
    # Use Minitest for generating new tests.
    config.generators do |g|
      g.test_framework :minitest
    end
    config.active_job.queue_adapter = :sidekiq
  end
end
