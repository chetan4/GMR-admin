Devise::Async.setup do |config|
  # Enable/Disable Devise Async
  config.enabled = false
  # config.enabled = (Rails.env != "development")

  # Supported options: :resque, :sidekiq, :delayed_job, :queue_classic, :torquebox, :backburner, :que
  # config.backend = :sidekiq
end

