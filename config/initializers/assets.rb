# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )


Rails.application.config.assets.precompile += %w( bootstrap/glyphicons-halflings-regular.eot )
Rails.application.config.assets.precompile += %w( bootstrap/glyphicons-halflings-regular.svg )
Rails.application.config.assets.precompile += %w( bootstrap/glyphicons-halflings-regular.ttf )
Rails.application.config.assets.precompile += %w( bootstrap/glyphicons-halflings-regular.woff )
Rails.application.config.assets.precompile += %w( bootstrap/glyphicons-halflings-regular.woff2 )

Rails.application.config.assets.precompile += %w( home.js )
Rails.application.config.assets.precompile += %w( home.css )

# Assets common to all verticals. Looping over verticals here.
["admin", "broker", "carrier", "data_entry", "employer"].each do |vertical|
  Rails.application.config.assets.precompile += [ "#{vertical}/sessions.js" ]
  Rails.application.config.assets.precompile += [ "#{vertical}/sessions.css" ]

  Rails.application.config.assets.precompile += [ "#{vertical}/registrations.js" ]
  Rails.application.config.assets.precompile += [ "#{vertical}/registrations.css" ]

  Rails.application.config.assets.precompile += [ "#{vertical}/passwords.js" ]
  Rails.application.config.assets.precompile += [ "#{vertical}/passwords.css" ]

  Rails.application.config.assets.precompile += [ "#{vertical}/invitations.js" ]
  Rails.application.config.assets.precompile += [ "#{vertical}/invitations.css" ]

  Rails.application.config.assets.precompile += ["#{vertical}/dashboards.js"]
  Rails.application.config.assets.precompile += ["#{vertical}/dashboards.css"]

  Rails.application.config.assets.precompile += ["#{vertical}/employees.js"]
  Rails.application.config.assets.precompile += ["#{vertical}/employees.css"]

  Rails.application.config.assets.precompile += ["#{vertical}/notifications.js"]
  Rails.application.config.assets.precompile += ["#{vertical}/notifications.css"]

  Rails.application.config.assets.precompile += ["#{vertical}/uploads.js"]
  Rails.application.config.assets.precompile += ["#{vertical}/uploads.css"]
end

# Controllers only under admin vertical. Looping over controllers here.
["admins", "brokers", "carriers", "data_entries", "employers", "companies", "audit_logs", "templates", "platform_files"].each do |controller|
  Rails.application.config.assets.precompile += ["admin/#{controller}.js"]
  Rails.application.config.assets.precompile += ["admin/#{controller}.css"]
end

# Controllers only under data_entry vertical. Looping over controllers here.

["tasks", "plans", "plan_selections", "plan_designs", "medical_claims", "large_claims", "dental_claims",
 "underwritings", "renewals", "packages", "renewals_task", "conversations",
 "quote_packages", "quotes", "quote_tasks", "censuses", "recommendations",
 "commissions", "employee_records"].each do |controller|
  Rails.application.config.assets.precompile += ["data_entry/#{controller}.js"]
  Rails.application.config.assets.precompile += ["data_entry/#{controller}.css"]
end

# added version 2 & version 3of files here
Rails.application.config.assets.precompile += ["data_entry/tasks_v2.css"]
Rails.application.config.assets.precompile += ["data_entry/tasks_v3.css"]

# Controllers only under carrier vertical. Looping over controllers here.
["home", "quote_requests", "renewals", "employers"].each do |controller|
  Rails.application.config.assets.precompile += ["carrier/#{controller}.js"]
  Rails.application.config.assets.precompile += ["carrier/#{controller}.css"]
end

# Controllers only under employer vertical. Looping over controllers here.
["home", "onboardings", "plans", "employer_structures", "employee_records", "current_plans",
 "contributions", "renewals", "demographics", "renewal_dates", "recommendations", "plan_marketings", "performance_indicators",
 "dashboard/vendor_market_place", "quotes", "plan_design_sliders", "underwritings", "benchmarkings", "reports", "compliance_and_resources", "commissions", "buyout_calculators"].each do |controller|
  Rails.application.config.assets.precompile += ["employer/#{controller}.js"]
  Rails.application.config.assets.precompile += ["employer/#{controller}.css"]
end
