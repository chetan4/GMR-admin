ActionMailer::Base.delivery_method = :smtp
ActionMailer::Base.smtp_settings = {
    :user_name => SENDGRID_USERNAME,
    :password => SENDGRID_PASSWORD,
    :domain => MAILER_HOST,
    :address => 'smtp.sendgrid.net',
    :port => 587,
    :authentication => :plain,
    :enable_starttls_auto => true
}