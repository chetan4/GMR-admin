PROJECT_ROOT = './'

# Sidekiq Process
pid_file = './tmp/pids/sidekiq.pid'

God.watch do |w|
  w.name = "sidekiq"
  w.dir = PROJECT_ROOT
  w.interval = 30.seconds
  w.start_grace = 10.seconds
  w.pid_file = pid_file
  w.behavior(:clean_pid_file)

  w.start = "cd #{PROJECT_ROOT}; bundle exec sidekiq -C config/sidekiq.yml"
  w.stop = "cd #{PROJECT_ROOT}; bundle exec sidekiqctl stop #{pid_file}"

  w.log = File.join(PROJECT_ROOT, 'log/sidekiq.log')

  # determine the state on startup
  w.transition(:init, {true => :up, false => :start}) do |on|
    on.condition(:process_running) do |c|
      c.running = true
    end
  end

  # start if process is not running
  w.transition(:up, :start) do |on|
    on.condition(:process_exits)
  end

  # lifecycle
  w.lifecycle do |on|
    on.condition(:flapping) do |c|
      c.to_state = [:start, :restart]
      c.times = 5
      c.within = 5.minute
      c.transition = :unmonitored
      c.retry_in = 10.minutes
      c.retry_times = 5
      c.retry_within = 2.hours
    end
  end
end