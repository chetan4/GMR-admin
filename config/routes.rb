Rails.application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"

  root 'home#index'

  devise_for :admins, controllers: {
                        sessions: 'admin/sessions',
                        registrations: 'admin/registrations',
                        passwords: 'admin/passwords',
                        invitations: 'admin/invitations'
                    }

  devise_for :brokers, controllers: {
                         sessions: 'broker/sessions',
                         registrations: 'broker/registrations',
                         passwords: 'broker/passwords',
                         invitations: 'broker/invitations'
                     }

  devise_for :carriers, controllers: {
                          sessions: 'carrier/sessions',
                          registrations: 'carrier/registrations',
                          passwords: 'carrier/passwords',
                          invitations: 'carrier/invitations'
                      }

  devise_for :data_entries, controllers: {
                              sessions: 'data_entry/sessions',
                              registrations: 'data_entry/registrations',
                              passwords: 'data_entry/passwords',
                              invitations: 'data_entry/invitations'
                          }

  devise_for :employers, controllers: {
                           sessions: 'employer/sessions',
                           registrations: 'employer/registrations',
                           passwords: 'employer/passwords',
                           invitations: 'employer/invitations'
                       }


  concern :notifiable do
    resources :notifications, only: [:index, :create] do
      collection do
        get 'fetch_count'
        get 'fetch_latest'
      end
    end
  end

  concern :jobs do
    resources :jobs, only: [] do
      collection do
        get :check_job_status
      end
    end
  end

  concern :conversationable do
    constraints(id: /[0-9]+/) do
      resources :conversations, only: [:index, :show]  do
        member do
          post :create_message
        end
        collection do
          get :search
          get :plan_design_sliders_info
        end
      end
    end
  end

  namespace :admin do
    devise_scope :admin do
      root 'dashboards#show'
      get '/terms_of_use' => '/home#terms_of_use'

      get '/flush_cache' => 'dashboards#flush_cache'
      resource :dashboard
      resources :companies, :only => [:index, :create, :update]
      resources :admins, :brokers, :carriers, :employers, :data_entries
      concerns :notifiable
      resources :audit_logs
      resources :employees do
        collection do
          patch "/update_status/:id" => "employees#update_status"
        end
      end
      resources :templates do
        member do
          post :new
          post :add_sheet
          post :add_section
          post :add_field
          post :add_column
          post :add_input
          get :edit_field
          get :edit_column
          get :edit_section
          get :edit_input
          post :update_sheet
          post :update_field
          post :update_field_attribute
          post :update_column
          post :update_section
          post :update_input
          post :update_field_order
          post :update_column_order
          post :update_sheet_order
          get :done_editing
          post :publish
          post :unpublish
          post :delete
          post :delete_section
          post :delete_field
          post :delete_column
        end
      end
      resources :platform_files do
        collection do
          get :search
        end
      end
      require 'sidekiq/web'
      Sidekiq::Web.use Rack::Auth::Basic do |username, password|
        username == 'admin' && password == 'test123'
      end
      mount Sidekiq::Web => '/sidekiq'


      post "/records/:type" => "records#index"
    end

    namespace :api do
      namespace :v1 do
        resources :platform_files, only: [] do
          member do
            get :data
          end
        end

        resources :templates, only: [] do
          collection do
            get "fetch/:benefit_type_id" => "templates#fetch"
          end
        end
      end
    end
  end

  namespace :broker do
    devise_scope :broker do
      root 'dashboards#show'
      get '/terms_of_use' => '/home#terms_of_use'
      get '/privacy' => '/home#privacy'

      resource :dashboard
      resources :employees
      concerns :notifiable

      post "/records/:type" => "records#index"
    end
  end

  namespace :carrier do
    devise_scope :carrier do
      root 'home#show'
      get '/terms_of_use' => '/home#terms_of_use'
      get '/privacy' => '/home#privacy'

      get "login_as" => "home#login_as"
      post "login_as" => "home#login_as"

      resource :dashboard, only: [:show] do
        collection do
          post :get_upload_details
        end
      end

      resource :quote_requests, only: [:show] do
        collection do
          get :filtered_quote_requests
          get :get_plan_info
          post :accept_offer
          post :add_terms_and_conditions
          post :remove_attachment
        end
      end

      resources :employers, only: [:show] do
        member do
          get :download_excel
          get :get_rfp_content
          post :send_rfp
        end
      end

      resource :renewals, only: [:show] do
        collection do
          get :get_renewals
          get :get_plan_info
          post :accept_offer
          post :add_terms_and_conditions
          post :remove_attachment
        end
      end

      resources :employees do
        member do
          get :associations
          post :create_associations
        end

        collection do
          patch "/update_status/:id" => "employees#update_status"
        end
      end

      resources :uploads do
        collection do
          post :attachment
          get :update_product_basket
          post :updated_basket
        end
        member do
          post :data_entry_post_information
        end
      end
      concerns :notifiable
      concerns :conversationable
      post "/records/:type" => "records#index"
    end
  end

  namespace :data_entry do
    devise_scope :data_entry do
      root 'dashboards#show'
      get '/terms_of_use' => '/home#terms_of_use'
      get '/privacy' => '/home#privacy'

      resource :dashboard, only: :show
      resources :commissions, only: [:index, :show] do
        collection do
         post :update_commissions
         get :plans
        end
      end
      resource :renewals, only: [:show] do
        collection do
          get :get_renewal_search
          get :get_search_results
          post :update_renewal_plan_name
          get '/create_renewal_plan/:plan_id' => 'renewals#create_renewal_plan'
        end
      end

      resource :recommendations, only: [:show] do
        collection do
          post :add_recommendation
          post :update_recommendation
          delete :delete_recommendation
          get :recommendation_template
          get :get_recommendations
        end
      end

      resource :renewals_task, only: [:show] do
        collection do
          get '/get_rate_option_labels'  => 'renewals_task#get_rate_option_labels'
          put '/plan/:renewal_plan_id/update' => 'renewals_task#update'
          post '/verify_renewal' => 'renewals_task#verify_renewal'
          post '/submit_renewal' => 'renewals_task#submit_renewal'
          get :rate_history
          get '/get_renewal_estimates'  => 'renewals_task#get_renewal_estimates'
          post '/save_estimate_enrollments' => 'renewals_task#save_estimate_enrollments'
          post '/save_volume' => 'renewals_task#save_volume'
        end
      end

      resources :employee_records, only: [:index] do
        collection do
          get :get_employer_records
          post :change_employee_status
          post :validate_employee_records
          post :terminate_employees
          get :import_records
        end
      end

      resources :tasks, only: [:index, :show] do
        resource :plan_design do
          post :reset
        end

        resource :medical_claim, only: [:create] do
          post :reset
          get :standard_template
          get :current_data
          post :import
          get :history
          get :filtered_history
          get "historical_data/:medical_claim_datum_id" => "medical_claims#historical_data"
        end

        resource :dental_claim, only: [:create] do
          post :reset
          get :standard_template
          get :current_data
          post :import
          get :history
          get :filtered_history
          patch :update_historical_data
          get "historical_data/:dental_claim_datum_id" => "dental_claims#historical_data"
        end

        resource :large_claim, only: [:create] do
          post :reset
          get :standard_template
          get :current_data
          post :import
          get :history
          get :filtered_history
          get "historical_data/:large_claim_datum_id" => "large_claims#historical_data"
        end

        resource :plan_change_history do
          post :perform_action
          post :add_or_update
          post :add_or_update_with_date
          post :delete
          post :fetch_plan_design
        end

        resource :census, only: [:create] do
          get :current_census
          post :import
          get :history
          get :filtered_history
          get :historical_data
        end

        resource :rates, only: [:create] do
          post :reset
        end

        resource :contributions, only: [:create] do
          post :add
          get :get_contribution
          get :remove_contribution
          get :copy_contribution
          post :update_contribution
          post :reset
        end

        member do
          get :task_details
          get :preview_template
          get :show_retag
          get :employee_structure_request
          post :save_retagged_files
          get :get_rate_option_labels
          post :save_rate_options
          post :add_comments
          post :request_information
          post :create_sub_task
          post :update_task_assignment
          post :reset_rate_options
          post :attach_plan
          get :search_plans
          post :upgrade_template
          get :conversion_utility
          post :update_conversion_utility_data
          post :process_census_subtask
          post :reopen_sub_task
          post :plan_options
          post :set_actuary_percentage
          get :get_census_data
          post :save_plan_description
        end

      end
      resources :uploads do
        collection do
          post :attachment
        end
      end

      resources :underwritings, :only => [:show] do
        collection do
          get :select_plan
          get :search
          get :reset
          get "/:plan_id/history" => "underwritings#history"
          post :create_or_update

          post :attach_plan
          get :search_plans

          get :fetch_period
          get :override_claim_period_cal
          get "/:plan_id/fetch_retention" => "underwritings#fetch_retention"
          post :publish
          post :unpublish
        end
      end

      resources :packages, :only => [:index, :create, :show] do
        collection do
          get :package_list
          post :add_renewal_plan
          post :delete_renewal_plan
          post :preview_update_enrollments_and_rates
          delete :delete_tagged_file
          post :upload_tag_file
          get :get_tagged_files
          post :save_package_option
          get :index
          get :search_packages
          post :update_package_option_name
          post :delete_package_option
          post :clone_package_plan
          get :update_plan_package
          post "/new_package_option" => "packages#new_package_option"
          get :get_tag_data
          post :add_tags
          post :create_sub_package
          post :associate_or_disassociate_sub_package
        end
      end

      get "/packages/:package_id/package_plan_renewals/:package_plan_renewal_id" => "renewals_task#package_plan_renewals_show"
      get "/packages/:package_id/package_plan_renewals/:package_plan_renewal_id/plan_design" => "renewals_task#package_plan_renewals_plan_design_show"
      get "/packages/:package_id/package_option_renewal_options/:package_option_renewal_option_id" => "renewals_task#package_option_renewal_option_show"
      post "/packages/:package_id/package_plan_renewals/:package_plan_renewal_id/save" => "renewals_task#save_rate_options"
      post "/packages/:package_id/package_option_renewal_options/:package_option_renewal_option_id/save" => "renewals_task#save_rate_options"
      post "/packages/:package_id/package_plan_renewals/:package_plan_renewal_id/reset" => "renewals_task#reset_rate_options"
      post "/packages/:package_id/package_option_renewal_options/:package_option_renewal_option_id/reset" => "renewals_task#reset_rate_options"
      post '/renewals_task/plan/:renewal_plan_id/reset_plan_design' => 'renewals_task#reset_plan_design'

      resources :employees do
        collection do
          patch "/update_status/:id" => "employees#update_status"
        end
      end
      resource :plan_selection do
        collection do
          get :step2, to: redirect('/plan_selection')
          post :step2
          get :summary_page
          post :done
        end
      end
      resources :plans, :only => [:new, :create] do
        collection do
          get :setup
          get :search
          post :add_date
          get :orphan_quote
          post :create_orphan
          get :create_quote
          get :orphan_renewal
          get '/create_renewal_for_orphan_plan/:plan_id' => 'plans#create_renewal_for_orphan_plan'
        end
      end

      concerns :notifiable
      concerns :jobs

      concerns :conversationable

      scope 'conversations' do
        get 'tasks' => 'conversations#conversation_tasks'
        get 'tasks/get_tasks' => 'conversations#get_conversation_tasks'
      end

      post "/records/:type" => "records#index"

      resources :quote_packages, :only => [:new, :create, :show] do
        collection do
          get :search
          post "/new_quote_package_option" => "quote_packages#new_quote_package_option"
          post :update_quote_package_option_name
          post :delete_quote_package_option
          post :add_quote_plan
          post :delete_quote_plan
          get :package_list
          post :preview_update_enrollments_and_rates_quote_package
          post :save_quote_package_option
          delete :delete_tagged_file
          post :upload_tag_file
          get :get_tagged_files
          post :clone_quote_package_plan
          get :update_quote_plan_package
          post :add_tags
          get :get_tag_data
          post :save_quote_package_option
          post :create_sub_package
          post :associate_or_disassociate_sub_package
        end

      end

      get "/quote_packages/:quote_package_id/quotes/:quote_id" => "quote_tasks#package_plan_quote_show"
      post "/quote_packages/:quote_package_id/quotes/:quote_id/save" => "quote_tasks#save_rate_options"
      post "/quote_packages/:quote_package_id/quotes/:quote_id/reset" => "quote_tasks#reset_rate_options"
      get "/quote_packages/:quote_package_id/quotes/:quote_id/plan_design" => "quote_tasks#quote_plan_design_show"


      resource :quote_tasks, only: [:show] do
        collection do
          get :get_rate_option_labels
          get :get_quote_estimates
          post :save_estimate_enrollments
          post :submit_quote
          post :save_volume
          post :verify_quote
          put '/quote/:quote_id/update' => 'quote_tasks#update'
          post '/quote/:quote_id/reset_plan_design' => 'quote_tasks#reset_plan_design'
        end
      end


      resources :quotes, :only => [:new, :create, :index] do
        collection do
          get :search
          post :create_quote_plan_option
          post :update_quote_plan_name
        end
      end

      resources :simple_project_renewals do
        collection do
          post :create_or_update
          post :publish
          post :simple_project_renewal_form
          post :unpublish
        end
      end

      resources :simple_current_rate_components do
        collection do
          post :create_or_update
          post :publish
          post :simple_current_rate_components_form
          post :unpublish
          post :simple_current_rate_publish_form
        end
      end

      resource :administration, only: [] do
        collection do
          post :create_or_update
          post :create_or_update_quote_attributes
          post :create_or_update_renewal_attributes
        end
      end

    end
  end

  namespace :employer do
    devise_scope :employer do
      root 'home#show'
      get '/terms_of_use' => '/home#terms_of_use'
      get '/privacy' => '/home#privacy'

      get "login_as" => "home#login_as"
      post "login_as" => "home#login_as"
      get "get_graph_data" => "home#get_graph_data"

      resource :dashboard, only: [:show]  do
        collection do
          get :get_plan_details
        end
      end
      namespace :dashboard do
        namespace :vendor_market_place do
          get "/" => :index
          get "benefit_type"
          get "carrier"
          post :quote_requests
          post :plan_requests
        end
      end
      resources :uploads do
        collection do
          post :attachment
        end
      end

      resource :quotes, only: [:show] do
        collection do
          get :employer_quote_requests
          get :plan_marketing
          get :compare_quotes
          get :get_plan_marketing_data
          get :get_rate_compare_data
          get :get_rate_compare_data_report
          get :get_file_viewer_compare_data
          get :get_plan_design_compare_data
          get :get_plan_design_compare_data_report
          post :finalize_plans
          post :confirm_package_option
          post :add_terms_and_conditions
          get :confirmed_packages
          post :change_package_status
          post :confirm_offer
          get :get_plan_info
          patch :update_package_likes
          post :remove_attachment
        end
      end

      resources :underwritings, only: [:show, :index]  do
        member do
          get :show_details
          get :calculate
          get :send_mail
        end
      end

      resource :current_plans, only: [:show] do
        collection do
          get :get_plan_info
          get :get_plan_info_report
          get :get_plan_details
          get :employer_current_plans
          get :employer_current_plans_report
          get :employer_plan_demographics
          get :employer_prior_plans
        end
      end
      get '/prior_plans' => 'current_plans#prior_plans'

      resource :plan_design_sliders, only: [:index] do
        collection do
          get  :index
          get  :get_slider_details
          post :save_slider_details
          post :slider_conversation
          post :add_new_slider
          post :delete_new_slider
        end
      end

      resource :demographics, only: [:show] do
        collection do
          get :get_employee_graphics_info
          get :business_units
        end
      end

      resource :renewals, only: [:show] do
        collection do
          get :get_renewals_plans
          get :renew_plan
          get :compare_plans
          get :plan_marketing
          get :get_plan_marketing_data
          get :get_rate_compare_data
          get :get_rate_compare_data_report
          get :get_file_viewer_compare_data
          get :get_plan_design_compare_data
          get :get_plan_design_compare_data_report
          post :finalize_plans
          post :finalize_renewal_plans
          get :confirmed_packages
          post :change_package_status
          post :confirm_offer
          post :add_terms_and_conditions
          patch :update_package_likes
          get :get_plan_info
          post :remove_attachment
          post :finalize_renewal_plans_status
          post :add_renewal_plans_to_basket
          post :confirm_renewal_plans
          post :delete_renewal_plans
        end
      end

      resource :renewal_dates, only: [:show] do
        collection do
          get :get_renewal_dates_plans
          get :request_date_now
          get :update_renewal_dates
        end
      end

      resource :recommendations, only: [:index] do
        collection do
          get :index
          get :questionnaire
          post :update_response
        end
      end

      resource :performance_indicators, only: [:show] do
        collection do
          get :get_kpi_data
        end
      end

      resource :benchmarkings, only: [:show] do
        collection do
          get :benchmarking_medical
          get :reports
        end
      end

      resource :reports, only: [:show] do

      end

      resource :buyout_calculators, only: [:show] do
        collection do
          post :buyout_details
          post :delete_buyout_calculator
        end
      end

      resource :contributions, only: [:show] do
        collection do
          get :get_coverage_details
          get :get_model

          get :get_contribution
          get :copy_contribution
          get :back
          get :remove_contribution
          post :add_contribution
          post :update_contribution
          put :update_enrollment


          get "/:benefit_type_id/model" => "contributions#model"
          get "/:benefit_type_id/model/new" => "contributions#new"
          post "/:benefit_type_id/model/create_model" => "contributions#create_model"
          get "/:benefit_type_id/model/:model_id/edit" => "contributions#edit"
          post "/:benefit_type_id/model/:model_id/update_model" => "contributions#update_model"
        end
      end

      resource :employee_records, only: [:show] do
        collection do
          get :get_employer_records
        end
      end

      resources :employees do
        collection do
          patch "/update_status/:id" => "employees#update_status"
        end
      end

      get 'emp_structure' => "employer_structures#show"
      resource :employer_structure, only: [:show] do
        collection do
          get :table
          get :table_for_class_type
          post :add_employee_class
          post :update_employee_class
          get :get_employee_class
          post :associate
        end
      end
      resource :onboarding do
        collection do
          get :step2, to: redirect('/onboarding')
          post :step2
          get :summary_page
          post :done
        end
      end
      resources :plans, :only => [:new, :create]
      concerns :notifiable
      concerns :conversationable

      post "/records/:type" => "records#index"

      resource :plan_marketings, only: [:show] do
        collection do
          get :get_sub_plans
          get :market_plan
        end
      end

      resource :compliance_and_resources, only: [:show] do
      end

      resources :commissions, only: :index
      concerns :jobs

    end
  end

end

