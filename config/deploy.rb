require 'mina/bundler'
require 'mina/rails'
require 'mina/git'
require 'mina_sidekiq/tasks'
# require 'mina/rbenv'  # for rbenv support. (http://rbenv.org)
require 'mina/rvm' # for rvm support. (http://rvm.io)
require 'mina/whenever'

# Basic settings:
#   domain       - The hostname to SSH to.
#   deploy_to    - Path to deploy into.
#   repository   - Git repo to clone from. (needed by mina/git)
#   branch       - Branch name to deploy. (needed by mina/git)

user = %x(git config user.name).delete("\n")

branch = (ENV['branch'].nil? ? %x(git symbolic-ref --short -q HEAD).delete("\n") : ENV['branch'])
branch = "develop" if branch == ""

puts "Please select the server."
puts "1. gmr-dev"
puts "2. gmr-test"
puts "3. gmr-staging"

STDOUT.flush
input = STDIN.gets.chomp
case input.upcase
  when "1"
    server = 'gmr-dev.idyllic-software.com' # this is development instance
    server_uri = 'gmr-dev.idyllic-software.com'
    env = 'test' # the dev server is in test env
  when "2"
    server = 'gmr-test.idyllic-software.com'
    server_uri = 'gmr-test.idyllic-software.com'
    env = 'test_1'
  when "3"
    server = 'gmr-staging.idyllic-software.com' # this is staging instance
    server_uri = 'gmr-staging.idyllic-software.com'
    env = 'staging'
    branch = "develop" # Only develop to be deployed on staging.
  else
    abort("Please select a valid server and try again.")
end

# print "Skip asset pre-compilation.. skipping not recommended (y/n): "
# input = STDIN.gets.chomp.downcase
# skip_migration = (input == 'y')
#
# puts "Skipping Asset precompilation... " if skip_migration

set :domain, server
set :deploy_to, '/var/app/gmr'
set :repository, 'git@github.com:idyllicsoftware/GMR.git'
set :branch, branch
set :rails_env, env

# For system-wide RVM install.
#   set :rvm_path, '/usr/local/rvm/bin/rvm'

# Manually create these paths in shared/ (eg: shared/config/database.yml) in your server.
# They will be linked in the 'deploy:link_shared_paths' step.
set :shared_paths, ['config/database.yml', 'log']

# Optional settings:
set :user, 'ubuntu' # Username in the server to SSH to.
set :port, '22' # SSH port number.
set :forward_agent, true # SSH forward_agent.

# This task is the environment that is loaded for most commands, such as
# `mina deploy` or `mina rake`.
task :environment do
  # If you're using rbenv, use this to load the rbenv environment.
  # Be sure to commit your .ruby-version or .rbenv-version to your repository.
  # invoke :'rbenv:load'

  # For those using RVM, use this to load an RVM version@gemset.
  invoke :'rvm:use[ruby-2.2.0@gmr]'
end

# Put any custom mkdir's in here for when `mina setup` is ran.
# For Rails apps, we'll make some of the shared paths that are shared between
# all releases.
task :setup => :environment do
  # sidekiq needs a place to store its pid file and log file
  queue! %[mkdir -p "#{deploy_to}/#{shared_path}/pids"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/#{shared_path}/pids"]

  queue! %[mkdir -p "#{deploy_to}/#{shared_path}/log"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/#{shared_path}/log"]

  queue! %[mkdir -p "#{deploy_to}/#{shared_path}/config"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/#{shared_path}/config"]

  queue! %[touch "#{deploy_to}/#{shared_path}/config/database.yml"]
  queue %[echo "-----> Be sure to edit '#{deploy_to}/#{shared_path}/config/database.yml'."]
end

desc "Deploys the current version to the server."
task :deploy => :environment do
  deploy do

    queue! %[curl -X POST --data-urlencode 'payload={"channel": "#gmr_integrations", "username": "mina", "text": "@pushkar, @sups, @kedar, @sonali: Deploy started for branch <https://github.com/idyllicsoftware/GMR/tree/#{branch}|#{branch}> on #{server_uri} by #{user}.", "icon_emoji": ":rocket:"}' https://hooks.slack.com/services/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]
    # stop accepting new workers
    invoke :'sidekiq:quiet'
    # Put things that will set up an empty directory into a fully set-up
    # instance of your project.
    invoke :'git:clone'
    invoke :'deploy:link_shared_paths'
    invoke :'bundle:install'
    invoke :'jshint'
    invoke :'rails:db_migrate'
    invoke :'role_permissions_populate'
    invoke :'rails:assets_precompile' #unless skip_migration
    invoke :'deploy:cleanup'
    invoke :'whenever:update'
    queue! %[curl -X POST --data-urlencode 'payload={"channel": "#gmr_integrations", "username": "mina", "text": "@pushkar, @sups, @kedar, @sonali: Branch <https://github.com/idyllicsoftware/GMR/tree/#{branch}|#{branch}> deployed on #{server_uri} by #{user}. Please run test suite!!", "icon_emoji": ":bell:"}' https://hooks.slack.com/services/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]
    to :launch do
      queue "mkdir -p #{deploy_to}/#{current_path}/tmp/"
      queue "touch #{deploy_to}/#{current_path}/tmp/restart.txt"
      invoke :'sidekiq:restart'
    end
  end
end

desc "Seed data to the database"
task :seed => :environment do
  queue "cd #{deploy_to}/current"
  queue "bundle exec rake db:seed RAILS_ENV=#{env}"
end

desc "Run jshint on deployed code."
task :jshint => :environment do
  queue 'echo -e "\n\r\033[32m-----> \033[30mRunning JSHint"'
  queue "cd #{deploy_to}/current"
  queue "bundle exec rake jshint RAILS_ENV=#{env}"
end

desc "Run role permissions populate."
task :role_permissions_populate => :environment do
  queue 'echo -e "\n\r\033[32m-----> \033[30mRunning Role Permission Population"'
  queue "bundle exec rake seed:populate_roles_and_permissions RAILS_ENV=#{env}"
end
# For help in making your deploy script, see the Mina documentation:
#
#  - http://nadarei.co/mina
#  - http://nadarei.co/mina/tasks
#  - http://nadarei.co/mina/settings
#  - http://nadarei.co/mina/helpers

