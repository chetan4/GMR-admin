--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.4
-- Dumped by pg_dump version 9.4.4
-- Started on 2016-01-28 13:55:11 IST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

DROP INDEX public.index_template_sheets_on_template_id_and_key;
DROP INDEX public.index_template_sheets_on_template_id;
ALTER TABLE ONLY public.template_sheets DROP CONSTRAINT template_sheets_pkey;
ALTER TABLE public.template_sheets ALTER COLUMN id DROP DEFAULT;
DROP SEQUENCE public.template_sheets_id_seq;
DROP TABLE public.template_sheets;
SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 359 (class 1259 OID 205215)
-- Name: template_sheets; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE template_sheets (
    id integer NOT NULL,
    template_id integer NOT NULL,
    name character varying(50),
    "order" integer DEFAULT (-1) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    key character varying(50)
);


--
-- TOC entry 360 (class 1259 OID 205219)
-- Name: template_sheets_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE template_sheets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2950 (class 0 OID 0)
-- Dependencies: 360
-- Name: template_sheets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE template_sheets_id_seq OWNED BY template_sheets.id;


--
-- TOC entry 2830 (class 2604 OID 205380)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY template_sheets ALTER COLUMN id SET DEFAULT nextval('template_sheets_id_seq'::regclass);


--
-- TOC entry 2944 (class 0 OID 205215)
-- Dependencies: 359
-- Data for Name: template_sheets; Type: TABLE DATA; Schema: public; Owner: -
--

COPY template_sheets (id, template_id, name, "order", created_at, updated_at, key) FROM stdin;
1	1	IN-NETWORK	-1	2016-01-15 06:45:17.256381	2016-01-15 06:45:17.256381	in-network
2	1	OUT-OF-NETWORK	-1	2016-01-15 06:45:55.307164	2016-01-15 06:45:55.307164	out-of-network
3	2	Short Term Disability (STD)	-1	2016-01-15 08:34:46.19584	2016-01-15 08:34:46.19584	short-term-disability-std
4	3	Long Term Disability	-1	2016-01-15 09:11:46.934801	2016-01-15 09:11:46.934801	long-term-disability
5	4	AD&D Insurance	-1	2016-01-15 11:11:45.19242	2016-01-15 11:11:45.19242	ad-d-insurance
6	5	Life Insurance	-1	2016-01-15 11:57:15.979373	2016-01-15 11:57:15.979373	life-insurance
7	6	Out-of-Network	-1	2016-01-18 10:45:45.266558	2016-01-18 10:45:45.266558	out-of-network
8	7	Out-of-Network	-1	2016-01-18 12:18:55.284792	2016-01-18 12:18:55.284792	out-of-network
9	7	IN_NETWORK	-1	2016-01-18 12:20:20.206918	2016-01-18 12:20:20.206918	in_network
10	8	Out-of-Network	-1	2016-01-18 17:55:34.532677	2016-01-18 17:55:34.532677	out-of-network
11	9	AD&D Insurance	-1	2016-01-18 18:31:07.545572	2016-01-18 18:31:07.545572	ad-d-insurance
12	10	Life Insurance	-1	2016-01-18 18:35:33.194171	2016-01-18 18:35:33.194171	life-insurance
13	8	In_Network	-1	2016-01-19 07:11:57.154874	2016-01-19 07:11:57.154874	in_network
14	12	IN_NETWORK	-1	2016-01-19 08:04:01.819	2016-01-19 08:04:01.819	in_network
15	12	Out-of-Network	-1	2016-01-19 08:32:01.758545	2016-01-19 08:32:01.758545	out-of-network
16	13	In-Network	-1	2016-01-19 08:54:00.907326	2016-01-19 08:54:00.907326	in-network
17	14	Long Term Disability	-1	2016-01-19 13:04:30.598874	2016-01-19 13:04:30.598874	long-term-disability
19	15	IN-NETWORK	-1	2016-01-20 09:48:54.577429	2016-01-20 09:48:54.577429	in-network
21	15	OUT-OF-NETWORK	-1	2016-01-20 09:49:00.896379	2016-01-20 09:49:00.896379	out-of-network
22	16	IN-NETWORK	-1	2016-01-20 09:50:07.465546	2016-01-20 09:50:07.465546	in-network
24	16	OUT-OF-NETWORK	-1	2016-01-20 09:50:14.132451	2016-01-20 09:50:14.132451	out-of-network
25	17	IN-NETWORK	-1	2016-01-20 09:54:26.955785	2016-01-20 09:54:26.955785	in-network
27	17	OUT-OF-NETWORK	-1	2016-01-20 09:54:33.232752	2016-01-20 09:54:33.232752	out-of-network
28	18	IN-NETWORK	-1	2016-01-20 10:10:20.952331	2016-01-20 10:10:20.952331	in-network
30	18	OUT-OF-NETWORK	-1	2016-01-20 10:10:27.772179	2016-01-20 10:10:27.772179	out-of-network
31	19	IN-NETWORK	-1	2016-01-20 10:59:26.368267	2016-01-20 10:59:26.368267	in-network
32	20	IN-NETWORK	-1	2016-01-20 11:26:32.352204	2016-01-20 11:26:32.352204	in-network
33	21	HRA	-1	2016-01-20 11:32:55.827498	2016-01-20 11:32:55.827498	hra
34	22	IN-NETWORK	-1	2016-01-20 11:33:40.71562	2016-01-20 11:33:40.71562	in-network
35	23	IN-NETWORK	-1	2016-01-20 11:47:48.084588	2016-01-20 11:47:48.084588	in-network
37	23	OUT-OF-NETWORK	-1	2016-01-20 11:47:54.748175	2016-01-20 11:47:54.748175	out-of-network
38	13	OUT-OF-NETWORK	-1	2016-01-20 11:50:47.413733	2016-01-20 11:50:47.413733	out-of-network
39	25	HEALTH SAVINGS ACCOUNT	-1	2016-01-20 12:10:50.903942	2016-01-20 12:10:50.903942	health-savings-account
40	13	IN-NETWORK (SECONDARY NETWORK)	-1	2016-01-20 12:23:30.332274	2016-01-20 12:23:30.332274	in-network-secondary-network
41	26	IN-NETWORK	-1	2016-01-20 19:58:41.446246	2016-01-20 19:58:41.446246	in-network
42	27	IN-NETWORK	-1	2016-01-20 19:59:49.0946	2016-01-20 19:59:49.0946	in-network
43	28	HRA	-1	2016-01-20 20:10:25.881451	2016-01-20 20:10:25.881451	hra
44	29	IN-NETWORK	-1	2016-01-21 09:05:56.748876	2016-01-21 09:05:56.748876	in-network
46	29	OUT-OF-NETWORK	-1	2016-01-21 09:06:02.906323	2016-01-21 09:06:02.906323	out-of-network
18	1	IN-NETWORK (SECONDARY NETWORK)	-1	2016-01-19 13:45:56.689407	2016-01-19 13:45:56.689407	in-network-secondary-network
20	15	IN-NETWORK (SECONDARY NETWORK)	-1	2016-01-20 09:49:00.455749	2016-01-20 09:49:00.455749	in-network-secondary-network
23	16	IN-NETWORK (SECONDARY NETWORK)	-1	2016-01-20 09:50:13.697811	2016-01-20 09:50:13.697811	in-network-secondary-network
26	17	IN-NETWORK (SECONDARY NETWORK)	-1	2016-01-20 09:54:32.808058	2016-01-20 09:54:32.808058	in-network-secondary-network
29	18	IN-NETWORK (SECONDARY NETWORK)	-1	2016-01-20 10:10:27.344014	2016-01-20 10:10:27.344014	in-network-secondary-network
36	23	IN-NETWORK (SECONDARY NETWORK)	-1	2016-01-20 11:47:54.319297	2016-01-20 11:47:54.319297	in-network-secondary-network
45	29	IN-NETWORK (SECONDARY NETWORK)	-1	2016-01-21 09:06:02.41083	2016-01-21 09:06:02.41083	in-network-secondary-network
50	31	In-Network	-1	2016-01-21 15:35:39.424781	2016-01-21 15:35:39.424781	in-network
51	31	OUT-OF-NETWORK	-1	2016-01-21 15:35:40.854441	2016-01-21 15:35:40.854441	out-of-network
52	31	IN-NETWORK (SECONDARY NETWORK)	-1	2016-01-21 15:35:41.715801	2016-01-21 15:35:41.715801	in-network-secondary-network
53	32	In-Network	-1	2016-01-21 16:46:00.262838	2016-01-21 16:46:00.262838	in-network
54	32	OUT-OF-NETWORK	-1	2016-01-21 16:46:01.776794	2016-01-21 16:46:01.776794	out-of-network
55	32	IN-NETWORK (SECONDARY NETWORK)	-1	2016-01-21 16:46:02.657978	2016-01-21 16:46:02.657978	in-network-secondary-network
47	30	IN-NETWORK	0	2016-01-21 13:31:24.412642	2016-01-22 07:20:28.3956	in-network
64	39	IN_NETWORK	-1	2016-01-23 15:03:34.270705	2016-01-23 15:03:34.270705	in_network
65	39	Out-of-Network	-1	2016-01-23 15:03:35.529798	2016-01-23 15:03:35.529798	out-of-network
48	30	OUT-OF-NETWORK	1	2016-01-21 13:31:30.036483	2016-01-22 07:20:37.002246	out-of-network
49	30	IN-NETWORK (SECONDARY NETWORK)	2	2016-01-21 13:31:33.514866	2016-01-22 07:20:37.010464	in-network-secondary-network
56	33	IN_NETWORK	-1	2016-01-22 13:25:29.277303	2016-01-22 13:25:29.277303	in_network
57	33	Out-of-Network	-1	2016-01-22 13:25:30.634528	2016-01-22 13:25:30.634528	out-of-network
58	34	IN_NETWORK	-1	2016-01-22 13:33:29.912118	2016-01-22 13:33:29.912118	in_network
59	34	Out-of-Network	-1	2016-01-22 13:33:31.076214	2016-01-22 13:33:31.076214	out-of-network
60	35	Life Insurance	-1	2016-01-23 14:54:16.464356	2016-01-23 14:54:16.464356	life-insurance
61	36	AD&D Insurance	-1	2016-01-23 14:57:43.403806	2016-01-23 14:57:43.403806	ad-d-insurance
62	37	Long Term Disability	-1	2016-01-23 14:59:16.16914	2016-01-23 14:59:16.16914	long-term-disability
63	38	Short Term Disability (STD)	-1	2016-01-23 15:01:20.326505	2016-01-23 15:01:20.326505	short-term-disability-std
66	40	IN-NETWORK	0	2016-01-25 09:08:53.05894	2016-01-25 09:08:53.05894	in-network
67	40	OUT-OF-NETWORK	1	2016-01-25 09:08:58.928885	2016-01-25 09:08:58.928885	out-of-network
68	40	IN-NETWORK (SECONDARY NETWORK)	2	2016-01-25 09:09:02.260822	2016-01-25 09:09:02.260822	in-network-secondary-network
69	41	IN-NETWORK	0	2016-01-25 09:10:00.435768	2016-01-25 09:10:00.435768	in-network
70	41	OUT-OF-NETWORK	1	2016-01-25 09:10:06.231324	2016-01-25 09:10:06.231324	out-of-network
71	41	IN-NETWORK (SECONDARY NETWORK)	2	2016-01-25 09:10:09.692844	2016-01-25 09:10:09.692844	in-network-secondary-network
\.


--
-- TOC entry 2951 (class 0 OID 0)
-- Dependencies: 360
-- Name: template_sheets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('template_sheets_id_seq', 71, true);


--
-- TOC entry 2834 (class 2606 OID 205610)
-- Name: template_sheets_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY template_sheets
    ADD CONSTRAINT template_sheets_pkey PRIMARY KEY (id);


--
-- TOC entry 2831 (class 1259 OID 205688)
-- Name: index_template_sheets_on_template_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_template_sheets_on_template_id ON template_sheets USING btree (template_id);


--
-- TOC entry 2832 (class 1259 OID 205689)
-- Name: index_template_sheets_on_template_id_and_key; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_template_sheets_on_template_id_and_key ON template_sheets USING btree (template_id, key);


-- Completed on 2016-01-28 13:55:11 IST

--
-- PostgreSQL database dump complete
--

