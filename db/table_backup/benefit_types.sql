--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.4
-- Dumped by pg_dump version 9.4.4
-- Started on 2016-01-28 13:55:10 IST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

DROP INDEX public.index_benefit_types_on_name_map;
ALTER TABLE ONLY public.benefit_types DROP CONSTRAINT benefit_types_pkey;
ALTER TABLE public.benefit_types ALTER COLUMN id DROP DEFAULT;
DROP SEQUENCE public.benefit_types_id_seq;
DROP TABLE public.benefit_types;
SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 184 (class 1259 OID 204533)
-- Name: benefit_types; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE benefit_types (
    id integer NOT NULL,
    name character varying NOT NULL,
    name_map character varying NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    benefit_category_id integer,
    subtypes text,
    type character varying,
    priority integer
);


--
-- TOC entry 185 (class 1259 OID 204539)
-- Name: benefit_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE benefit_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2948 (class 0 OID 0)
-- Dependencies: 185
-- Name: benefit_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE benefit_types_id_seq OWNED BY benefit_types.id;


--
-- TOC entry 2829 (class 2604 OID 205293)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY benefit_types ALTER COLUMN id SET DEFAULT nextval('benefit_types_id_seq'::regclass);


--
-- TOC entry 2942 (class 0 OID 204533)
-- Dependencies: 184
-- Data for Name: benefit_types; Type: TABLE DATA; Schema: public; Owner: -
--

COPY benefit_types (id, name, name_map, created_at, updated_at, benefit_category_id, subtypes, type, priority) FROM stdin;
2	Wellness	wellness	2015-12-28 00:47:13.94306	2015-12-28 00:47:13.94306	1	\N	MainBenefitType	\N
3	Flexible Spending Account	flexible_spending_account	2015-12-28 00:47:14.035541	2015-12-28 00:47:14.035541	1	\N	MainBenefitType	\N
4	HRA - Gap Plan	hra_-_gap_plan	2015-12-28 00:47:14.136579	2015-12-28 00:47:14.136579	1	\N	MainBenefitType	\N
5	HRA - Reimbursement	hra_-_reimbursement	2015-12-28 00:47:14.243061	2015-12-28 00:47:14.243061	1	\N	MainBenefitType	\N
6	Healthcare Spending Account (HSA)	healthcare_spending_account_(hsa)	2015-12-28 00:47:14.338563	2015-12-28 00:47:14.338563	1	\N	MainBenefitType	\N
13	Travel Accident	travel_accident	2015-12-28 00:47:15.015359	2015-12-28 00:47:15.015359	2	\N	MainBenefitType	\N
14	Stop Loss	stop_loss	2015-12-28 00:47:15.138125	2015-12-28 00:47:15.138125	2	\N	MainBenefitType	\N
15	Voluntary Dental	voluntary_dental	2015-12-28 00:47:15.239678	2015-12-28 00:47:15.239678	3	\N	MainBenefitType	\N
16	Voluntary Life Insurance	voluntary_life_insurance	2015-12-28 00:47:15.332503	2015-12-28 00:47:15.332503	3	\N	MainBenefitType	\N
17	Voluntary AD&D	voluntary_ad&d	2015-12-28 00:47:15.421448	2015-12-28 00:47:15.421448	3	\N	MainBenefitType	\N
18	Voluntary Long Term Disability (VLTD)	voluntary_long_term_disability_(vltd)	2015-12-28 00:47:15.512108	2015-12-28 00:47:15.512108	3	\N	MainBenefitType	\N
19	Voluntary Short Term Disability (VSTD)	voluntary_short_term_disability_(vstd)	2015-12-28 00:47:15.603283	2015-12-28 00:47:15.603283	3	\N	MainBenefitType	\N
20	Voluntary Vision	voluntary_vision	2015-12-28 00:47:15.705023	2015-12-28 00:47:15.705023	3	\N	MainBenefitType	\N
21	Accidental Insurance	accidental_insurance	2015-12-28 00:47:15.812643	2015-12-28 00:47:15.812643	4	\N	MainBenefitType	\N
22	Critical Illness	critical_illness	2015-12-28 00:47:15.904799	2015-12-28 00:47:15.904799	4	\N	MainBenefitType	\N
23	Auto/Homeowner	auto/homeowner	2015-12-28 00:47:15.994682	2015-12-28 00:47:15.994682	4	\N	MainBenefitType	\N
24	Umbrella	umbrella	2015-12-28 00:47:16.088102	2015-12-28 00:47:16.088102	4	\N	MainBenefitType	\N
25	Prepaid Legal Services	prepaid_legal_services	2015-12-28 00:47:16.181901	2015-12-28 00:47:16.181901	4	\N	MainBenefitType	\N
26	Long Term Care (LTC)	long_term_care_(ltc)	2015-12-28 00:47:16.27532	2015-12-28 00:47:16.27532	4	\N	MainBenefitType	\N
27	Pet Insurance	pet_insurance	2015-12-28 00:47:16.369061	2015-12-28 00:47:16.369061	4	\N	MainBenefitType	\N
28	Transportation/Parking Reimbursement	transportation/parking_reimbursement	2015-12-28 00:47:16.460889	2015-12-28 00:47:16.460889	4	\N	MainBenefitType	\N
29	Employee Assistance Program (EAP)	employee_assistance_program_(eap)	2015-12-28 00:47:16.55838	2015-12-28 00:47:16.55838	5	\N	MainBenefitType	\N
30	Tuition Reimbursement Program	tuition_reimbursement_program	2015-12-28 00:47:16.654472	2015-12-28 00:47:16.654472	5	\N	MainBenefitType	\N
31	Adoption Assistance Program	adoption_assistance_program	2015-12-28 00:47:16.750734	2015-12-28 00:47:16.750734	5	\N	MainBenefitType	\N
32	Day Care	day_care	2015-12-28 00:47:16.847418	2015-12-28 00:47:16.847418	5	\N	MainBenefitType	\N
33	401k	401k	2015-12-28 00:47:16.951721	2015-12-28 00:47:16.951721	6	\N	MainBenefitType	\N
34	403b	403b	2015-12-28 00:47:17.052742	2015-12-28 00:47:17.052742	6	\N	MainBenefitType	\N
35	Roth 401k	roth_401k	2015-12-28 00:47:17.15931	2015-12-28 00:47:17.15931	6	\N	MainBenefitType	\N
36	Roth 403b	roth_403b	2015-12-28 00:47:17.250172	2015-12-28 00:47:17.250172	6	\N	MainBenefitType	\N
37	Declined Benefit Pension	declined_benefit_pension	2015-12-28 00:47:17.344566	2015-12-28 00:47:17.344566	6	\N	MainBenefitType	\N
38	Section 529 college savings plan	section_529_college_savings_plan	2015-12-28 00:47:17.435456	2015-12-28 00:47:17.435456	6	\N	MainBenefitType	\N
39	Retirement Planning	retirement_planning	2015-12-28 00:47:17.528931	2015-12-28 00:47:17.528931	6	\N	MainBenefitType	\N
40	Executive 401k Mirror Plan	executive_401k_mirror_plan	2015-12-28 00:47:17.627712	2015-12-28 00:47:17.627712	7	\N	MainBenefitType	\N
41	Executive 457(b) Plan	executive_457(b)_plan	2015-12-28 00:47:17.723887	2015-12-28 00:47:17.723887	7	\N	MainBenefitType	\N
42	Executive Bonus Plan	executive_bonus_plan	2015-12-28 00:47:17.815666	2015-12-28 00:47:17.815666	7	\N	MainBenefitType	\N
43	Executive Expense Reimbursement Plan (MERP)	executive_expense_reimbursement_plan_(merp)	2015-12-28 00:47:17.908618	2015-12-28 00:47:17.908618	7	\N	MainBenefitType	\N
44	Executive Non Qualified Deferred Compensation Plan	executive_non_qualified_deferred_compensation_plan	2015-12-28 00:47:18.006336	2015-12-28 00:47:18.006336	7	\N	MainBenefitType	\N
45	Executive Split Dollar Life Insurance Plan	executive_split_dollar_life_insurance_plan	2015-12-28 00:47:18.100733	2015-12-28 00:47:18.100733	7	\N	MainBenefitType	\N
46	Executive Supplemental Employee Retirement Plan (SERP)	executive_supplemental_employee_retirement_plan_(serp)	2015-12-28 00:47:18.192517	2015-12-28 00:47:18.192517	7	\N	MainBenefitType	\N
1	Medical	medical	2015-12-28 00:47:13.821456	2016-01-19 10:43:08.524577	1	\N	MainBenefitType	1
7	Dental	dental	2015-12-28 00:47:14.440453	2016-01-19 10:43:08.532014	2	\N	MainBenefitType	2
8	Life Insurance	life_insurance	2015-12-28 00:47:14.53593	2016-01-19 10:43:08.534403	2	\N	MainBenefitType	4
9	AD&D	ad&d	2015-12-28 00:47:14.632106	2016-01-19 10:43:08.536778	2	\N	MainBenefitType	5
10	Long Term Disability (LTD)	long_term_disability_(ltd)	2015-12-28 00:47:14.735718	2016-01-19 10:43:08.539163	2	\N	MainBenefitType	6
11	Short Term Disability (STD)	short_term_disability_(std)	2015-12-28 00:47:14.829631	2016-01-19 10:43:08.54148	2	\N	MainBenefitType	7
12	Vision	vision	2015-12-28 00:47:14.923412	2016-01-19 10:43:08.543783	2	\N	MainBenefitType	3
\.


--
-- TOC entry 2949 (class 0 OID 0)
-- Dependencies: 185
-- Name: benefit_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('benefit_types_id_seq', 46, true);


--
-- TOC entry 2831 (class 2606 OID 205436)
-- Name: benefit_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY benefit_types
    ADD CONSTRAINT benefit_types_pkey PRIMARY KEY (id);


--
-- TOC entry 2832 (class 1259 OID 205649)
-- Name: index_benefit_types_on_name_map; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_benefit_types_on_name_map ON benefit_types USING btree (name_map);


-- Completed on 2016-01-28 13:55:10 IST

--
-- PostgreSQL database dump complete
--

