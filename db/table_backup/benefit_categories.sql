--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.4
-- Dumped by pg_dump version 9.4.4
-- Started on 2016-01-28 13:55:10 IST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

ALTER TABLE ONLY public.benefit_categories DROP CONSTRAINT benefit_categories_pkey;
ALTER TABLE public.benefit_categories ALTER COLUMN id DROP DEFAULT;
DROP SEQUENCE public.benefit_categories_id_seq;
DROP TABLE public.benefit_categories;
SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 178 (class 1259 OID 204509)
-- Name: benefit_categories; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE benefit_categories (
    id integer NOT NULL,
    permalink character varying(150) NOT NULL,
    name character varying(100) NOT NULL,
    description character varying
);


--
-- TOC entry 179 (class 1259 OID 204515)
-- Name: benefit_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE benefit_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2947 (class 0 OID 0)
-- Dependencies: 179
-- Name: benefit_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE benefit_categories_id_seq OWNED BY benefit_categories.id;


--
-- TOC entry 2829 (class 2604 OID 205290)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY benefit_categories ALTER COLUMN id SET DEFAULT nextval('benefit_categories_id_seq'::regclass);


--
-- TOC entry 2941 (class 0 OID 204509)
-- Dependencies: 178
-- Data for Name: benefit_categories; Type: TABLE DATA; Schema: public; Owner: -
--

COPY benefit_categories (id, permalink, name, description) FROM stdin;
1	medical-benefits	Medical Benefits	\N
2	core-benefits	Core Benefits	Partially or Fully Paid by Employer
3	core-voluntary-benefits	Core Voluntary Benefits	100% Paid by Employees
4	additional-voluntary-benefits	Additional Voluntary Benefits	\N
5	work-life-benefits	Work/Life Benefits	\N
6	savings-and-retirement	Savings and Retirement	\N
7	executive-benefits	Executive Benefits	\N
\.


--
-- TOC entry 2948 (class 0 OID 0)
-- Dependencies: 179
-- Name: benefit_categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('benefit_categories_id_seq', 7, true);


--
-- TOC entry 2831 (class 2606 OID 205430)
-- Name: benefit_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY benefit_categories
    ADD CONSTRAINT benefit_categories_pkey PRIMARY KEY (id);


-- Completed on 2016-01-28 13:55:10 IST

--
-- PostgreSQL database dump complete
--

