--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.4
-- Dumped by pg_dump version 9.4.4
-- Started on 2016-01-28 13:55:11 IST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

DROP INDEX public.index_template_entities_on_type_and_template_section_id_and_key;
DROP INDEX public.index_template_entities_on_type_and_template_section_id;
ALTER TABLE ONLY public.template_entities DROP CONSTRAINT template_entities_pkey;
ALTER TABLE public.template_entities ALTER COLUMN id DROP DEFAULT;
DROP SEQUENCE public.template_entities_id_seq;
DROP TABLE public.template_entities;
SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 353 (class 1259 OID 205192)
-- Name: template_entities; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE template_entities (
    id integer NOT NULL,
    type character varying,
    template_section_id integer NOT NULL,
    name character varying NOT NULL,
    "order" integer DEFAULT (-1) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    properties hstore,
    key character varying(50),
    initial_marketing boolean,
    current_plans boolean
);


--
-- TOC entry 354 (class 1259 OID 205199)
-- Name: template_entities_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE template_entities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2950 (class 0 OID 0)
-- Dependencies: 354
-- Name: template_entities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE template_entities_id_seq OWNED BY template_entities.id;


--
-- TOC entry 2830 (class 2604 OID 205377)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY template_entities ALTER COLUMN id SET DEFAULT nextval('template_entities_id_seq'::regclass);


--
-- TOC entry 2944 (class 0 OID 205192)
-- Dependencies: 353
-- Data for Name: template_entities; Type: TABLE DATA; Schema: public; Owner: -
--

COPY template_entities (id, type, template_section_id, name, "order", created_at, updated_at, properties, key, initial_marketing, current_plans) FROM stdin;
1	TemplateColumn	1	ENTRIES	1	2016-01-15 06:46:39.763625	2016-01-15 06:46:39.763625		entries	\N	\N
4	TemplateField	1	Referrals Needed	5	2016-01-15 06:50:50.676601	2016-01-15 14:46:48.100871		referrals-needed	t	t
9	TemplateField	1	Coinsurance	15	2016-01-15 06:57:53.247282	2016-01-15 14:46:48.687299		coinsurance	t	t
5	TemplateField	1	Deductible Individual	7	2016-01-15 06:53:52.200738	2016-01-15 14:46:48.12787		deductible-individual	t	t
20	TemplateColumn	2	Subject to Deductible & Coinsurance	1	2016-01-15 07:14:15.161075	2016-01-15 07:14:15.161075		subject-to-deductible-coinsurance	\N	\N
21	TemplateColumn	2	COPAY	2	2016-01-15 07:14:27.694695	2016-01-15 07:14:27.694695		copay	\N	\N
22	TemplateColumn	2	LIMIT	3	2016-01-15 07:15:28.473623	2016-01-15 07:15:28.473623		limit	\N	\N
25	TemplateField	2	General Radiology	4	2016-01-15 07:19:40.835262	2016-01-15 07:19:40.835262		general-radiology	\N	\N
26	TemplateField	2	CT/MRI/PET Scans	5	2016-01-15 07:21:33.454374	2016-01-15 07:21:33.454374		ct-mri-pet-scans	\N	\N
27	TemplateField	2	Annual Physical Exam	6	2016-01-15 07:22:37.477729	2016-01-15 07:22:37.477729		annual-physical-exam	\N	\N
28	TemplateField	2	Well Child Exams	7	2016-01-15 07:23:28.296268	2016-01-15 07:23:28.296268		well-child-exams	\N	\N
29	TemplateField	2	Pediatric Dental	8	2016-01-15 07:24:24.021982	2016-01-15 07:24:24.021982		pediatric-dental	\N	\N
32	TemplateField	2	Surgery/Anesthesiology	11	2016-01-15 07:28:17.647879	2016-01-15 07:28:17.647879		surgery-anesthesiology	\N	\N
36	TemplateColumn	2	COPAY LIMIT	4	2016-01-15 07:34:39.592309	2016-01-15 07:34:39.592309		copay-limit	\N	\N
37	TemplateColumn	2	SEPERATE DEDUCTIBLE	5	2016-01-15 07:38:42.882741	2016-01-15 07:38:42.882741		seperate-deductible	\N	\N
38	TemplateColumn	2	Separate Coinsurance	6	2016-01-15 08:29:49.532742	2016-01-15 08:29:49.532742		separate-coinsurance	\N	\N
40	TemplateColumn	3	Entries	1	2016-01-15 08:35:08.523776	2016-01-15 08:35:08.523776		entries	\N	\N
43	TemplateColumn	4	Earnings Determination	1	2016-01-15 08:36:52.650647	2016-01-15 08:36:52.650647		earnings-determination	\N	\N
44	TemplateColumn	4	Plus Commissions	2	2016-01-15 08:37:07.519571	2016-01-15 08:37:07.519571		plus-commissions	\N	\N
45	TemplateColumn	4	Plus Bonus	3	2016-01-15 08:37:26.160573	2016-01-15 08:37:26.160573		plus-bonus	\N	\N
46	TemplateColumn	4	Plus Overtime	4	2016-01-15 08:37:42.269561	2016-01-15 08:37:42.269561		plus-overtime	\N	\N
47	TemplateColumn	5	Entries	1	2016-01-15 08:40:29.190728	2016-01-15 08:40:29.190728		entries	\N	\N
55	TemplateColumn	6	Entries	1	2016-01-15 08:48:06.063451	2016-01-15 08:48:06.063451		entries	\N	\N
57	TemplateField	6	Employer FICA match	2	2016-01-15 08:48:36.841553	2016-01-15 08:48:36.841553		employer-fica-match	\N	\N
58	TemplateField	6	Lump Sum Survivor Benefit	3	2016-01-15 08:48:49.947706	2016-01-15 08:48:49.947706		lump-sum-survivor-benefit	\N	\N
59	TemplateField	6	Partial Disability	4	2016-01-15 08:49:03.945109	2016-01-15 08:49:03.945109		partial-disability	\N	\N
60	TemplateField	6	Recurrent Disability	5	2016-01-15 08:50:32.190849	2016-01-15 08:50:32.190849		recurrent-disability	\N	\N
61	TemplateField	6	Vocational Rehab Incentive	6	2016-01-15 08:50:44.425444	2016-01-15 08:50:44.425444		vocational-rehab-incentive	\N	\N
42	TemplateField	4	Definition of Earnings	1	2016-01-15 08:36:39.441868	2016-01-15 09:03:37.087739		definition-of-earnings	\N	t
50	TemplateField	5	Employer Gross Up	3	2016-01-15 08:41:44.791259	2016-01-15 09:05:03.404555		employer-gross-up	\N	t
48	TemplateField	5	Definition of Disability	1	2016-01-15 08:41:03.846715	2016-01-15 09:03:48.591703		definition-of-disability	t	t
49	TemplateField	5	Elimination Period	2	2016-01-15 08:41:29.811252	2016-01-15 09:04:58.85801		elimination-period	t	t
53	TemplateField	5	Minimum Weekly Benefit	6	2016-01-15 08:43:56.306025	2016-01-15 09:05:23.434193		minimum-weekly-benefit	\N	t
51	TemplateField	5	Maximum Benefit Duration	4	2016-01-15 08:42:37.285915	2016-01-15 09:05:10.198421		maximum-benefit-duration	t	t
52	TemplateField	5	Maximum Weekly Benefit	5	2016-01-15 08:42:52.957325	2016-01-15 09:05:18.22003		maximum-weekly-benefit	t	t
56	TemplateField	6	Benefit Integration Offsets	1	2016-01-15 08:48:21.379471	2016-01-15 09:05:39.95922		benefit-integration-offsets	\N	t
54	TemplateField	5	Pre-existing Condition Exclusions	7	2016-01-15 08:44:15.067738	2016-01-15 09:05:33.035638		pre-existing-condition-exclusions	t	t
2	TemplateField	1	Plan Type	1	2016-01-15 06:46:57.986055	2016-01-15 14:44:49.717768		plan-type	t	t
6	TemplateField	1	Deductible Family	9	2016-01-15 06:55:00.124905	2016-01-15 14:46:48.37297		deductible-family	t	t
7	TemplateField	1	Deductible - Family Multiple	11	2016-01-15 06:56:25.755531	2016-01-15 14:46:48.454572		deductible-family-multiple	\N	\N
15	TemplateField	1	Gym Reimbursement	31	2016-01-15 07:06:30.911571	2016-01-15 14:48:38.663488		gym-reimbursement	\N	\N
11	TemplateField	1	Out-of-Pocket Max Family (includes deductible)	19	2016-01-15 07:02:06.255007	2016-01-15 14:46:48.739869		out-of-pocket-max-family-includes-deductible	t	t
24	TemplateField	2	Lab	3	2016-01-15 07:18:16.617439	2016-01-15 14:51:19.487116		lab	\N	t
3	TemplateField	1	Network	3	2016-01-15 06:48:09.27441	2016-01-15 14:46:48.074637		network	t	t
8	TemplateField	1	Deductible Format	13	2016-01-15 06:57:11.334788	2016-01-15 14:46:48.507543		deductible-format	\N	t
14	TemplateField	1	Annual Maximum	29	2016-01-15 07:05:48.965896	2016-01-15 14:48:38.05347		annual-maximum	\N	\N
18	TemplateField	1	International Employees Covered	37	2016-01-15 07:11:46.325048	2016-01-15 14:48:38.702179		international-employees-covered	\N	\N
17	TemplateField	1	Nurseline	35	2016-01-15 07:10:59.155999	2016-01-15 14:48:38.701021		nurseline	\N	\N
12	TemplateField	1	In & Out-of-Network Deductibles Cross Accumulation	23	2016-01-15 07:03:50.977839	2016-01-15 14:50:57.563		in-out-of-network-deductibles-cross-accumulation	\N	t
13	TemplateField	1	OON Reimbursement Level	27	2016-01-15 07:04:48.073728	2016-01-15 14:51:02.566405		oon-reimbursement-level	t	t
19	TemplateField	2	Primary Care Office Visit	1	2016-01-15 07:13:46.089597	2016-01-15 14:51:16.818807		primary-care-office-visit	t	t
16	TemplateField	1	Customer Service Days/Hours	33	2016-01-15 07:10:21.693848	2016-01-15 14:48:38.665039		customer-service-days-hours	\N	\N
23	TemplateField	2	Specialist Office Visit	2	2016-01-15 07:16:29.361125	2016-01-15 14:51:18.139973		specialist-office-visit	t	t
31	TemplateField	2	Hospitalization - Outpatient	10	2016-01-15 07:26:25.737979	2016-01-15 14:51:32.745495		hospitalization-outpatient	\N	t
30	TemplateField	2	Hospitalization - Inpatient	9	2016-01-15 07:25:35.296861	2016-01-15 14:51:31.627071		hospitalization-inpatient	t	t
34	TemplateField	2	Urgent Care	13	2016-01-15 07:30:03.905304	2016-01-15 14:51:43.654495		urgent-care	\N	t
33	TemplateField	2	Emergency Room	12	2016-01-15 07:29:23.217431	2016-01-15 14:51:35.94347		emergency-room	t	t
35	TemplateField	2	Mental Nervous - Inpatient Coverage	14	2016-01-15 07:31:54.832868	2016-01-15 14:51:47.659273		mental-nervous-inpatient-coverage	\N	t
39	TemplateField	2	Mental Nervous - Outpatient Coverage	15	2016-01-15 08:30:15.481887	2016-01-15 14:51:48.863175		mental-nervous-outpatient-coverage	\N	t
62	TemplateField	6	W2 services provided	7	2016-01-15 08:50:54.580273	2016-01-15 08:50:54.580273		w2-services-provided	\N	\N
63	TemplateColumn	7	Employee Class	1	2016-01-15 08:54:58.61879	2016-01-15 08:54:58.61879		employee-class	\N	\N
64	TemplateColumn	7	Employee Classes	2	2016-01-15 08:55:09.773311	2016-01-15 08:55:09.773311		employee-classes	\N	\N
65	TemplateColumn	7	Waiting Period	3	2016-01-15 08:55:38.803455	2016-01-15 08:55:38.803455		waiting-period	\N	\N
66	TemplateColumn	7	Hours Requirement	4	2016-01-15 08:55:52.195547	2016-01-15 08:55:52.195547		hours-requirement	\N	\N
68	TemplateField	7	Employee Eligibility Class #1	1	2016-01-15 08:56:08.978814	2016-01-15 08:56:08.978814		employee-eligibility-class-1	\N	\N
69	TemplateField	7	Employee Eligibility Class #2	2	2016-01-15 08:56:21.889957	2016-01-15 08:56:21.889957		employee-eligibility-class-2	\N	\N
70	TemplateField	7	Employee Eligibility Class #3	3	2016-01-15 08:56:34.201713	2016-01-15 08:56:34.201713		employee-eligibility-class-3	\N	\N
71	TemplateField	7	Employee Eligibility Class #4	4	2016-01-15 08:56:47.699798	2016-01-15 08:56:47.699798		employee-eligibility-class-4	\N	\N
41	TemplateField	3	STD Amount	1	2016-01-15 08:35:28.683626	2016-01-15 09:03:23.794245		std-amount	t	t
73	TemplateField	2	Physical Therapy - Inpatient Coverage	18	2016-01-15 09:09:34.258002	2016-01-15 09:09:34.258002		physical-therapy-inpatient-coverage	\N	\N
75	TemplateField	2	Occupational Therapy - Inpatient Coverage	20	2016-01-15 09:10:34.728021	2016-01-15 09:10:34.728021		occupational-therapy-inpatient-coverage	\N	\N
76	TemplateField	2	Occupational Therapy - Outpatient Coverage	21	2016-01-15 09:11:00.781372	2016-01-15 09:11:00.781372		occupational-therapy-outpatient-coverage	\N	\N
77	TemplateField	2	Speech Therapy - Inpatient Coverage	22	2016-01-15 09:11:17.425433	2016-01-15 09:11:17.425433		speech-therapy-inpatient-coverage	\N	\N
78	TemplateField	2	Speech Therapy - Outpatient Coverage	23	2016-01-15 09:13:23.775374	2016-01-15 09:13:23.775374		speech-therapy-outpatient-coverage	\N	\N
79	TemplateField	2	Pregnancy & Maternity Care - Office Visits	24	2016-01-15 09:13:40.991307	2016-01-15 09:13:40.991307		pregnancy-maternity-care-office-visits	\N	\N
81	TemplateColumn	8	Percentage of Earnings	1	2016-01-15 09:14:14.709753	2016-01-15 09:14:14.709753		percentage-of-earnings	\N	\N
82	TemplateField	2	Pregnancy & Maternity Care - Labor & Delivery	25	2016-01-15 09:14:15.548933	2016-01-15 09:14:15.548933		pregnancy-maternity-care-labor-delivery	\N	\N
83	TemplateColumn	8	Flat Dollar Amount	2	2016-01-15 09:14:24.540182	2016-01-15 09:14:24.540182		flat-dollar-amount	\N	\N
85	TemplateField	2	Ambulance	27	2016-01-15 09:14:48.689027	2016-01-15 09:14:48.689027		ambulance	\N	\N
86	TemplateField	2	Hospice	28	2016-01-15 09:14:58.06891	2016-01-15 09:14:58.06891		hospice	\N	\N
87	TemplateField	2	Home Healthcare	29	2016-01-15 09:16:14.206581	2016-01-15 09:16:14.206581		home-healthcare	\N	\N
88	TemplateField	2	Skilled Nursing	30	2016-01-15 09:16:23.326707	2016-01-15 09:16:23.326707		skilled-nursing	\N	\N
91	TemplateField	2	Prosthetics	32	2016-01-15 09:16:45.227178	2016-01-15 09:16:45.227178		prosthetics	\N	\N
92	TemplateColumn	9	Earnings Determination	1	2016-01-15 09:16:55.824595	2016-01-15 09:16:55.824595		earnings-determination	\N	\N
94	TemplateColumn	9	Plus Commission	2	2016-01-15 09:17:06.882273	2016-01-15 09:17:06.882273		plus-commission	\N	\N
95	TemplateField	2	Hearing Devices	34	2016-01-15 09:17:10.397474	2016-01-15 09:17:10.397474		hearing-devices	\N	\N
96	TemplateField	2	Vision Exams	35	2016-01-15 09:17:16.552019	2016-01-15 09:17:16.552019		vision-exams	\N	\N
97	TemplateColumn	9	Plus Bonus	3	2016-01-15 09:17:32.940825	2016-01-15 09:17:32.940825		plus-bonus	\N	\N
98	TemplateField	2	Short Term Rehabilitation- Inpatient	36	2016-01-15 09:17:32.951726	2016-01-15 09:17:32.951726		short-term-rehabilitation-inpatient	\N	\N
99	TemplateColumn	9	PLUS OVERTIME	4	2016-01-15 09:17:47.097256	2016-01-15 09:17:47.097256		plus-overtime	\N	\N
100	TemplateField	2	Short Term Rehabilitation - Outpatient	37	2016-01-15 09:17:49.900435	2016-01-15 09:17:49.900435		short-term-rehabilitation-outpatient	\N	\N
101	TemplateField	2	Telemedicine	38	2016-01-15 09:18:05.900701	2016-01-15 09:18:05.900701		telemedicine	\N	\N
102	TemplateField	2	Speciality Rx Coverage	39	2016-01-15 09:18:16.110274	2016-01-15 09:18:16.110274		speciality-rx-coverage	\N	\N
103	TemplateColumn	10	Entries	1	2016-01-15 09:19:16.861848	2016-01-15 09:19:16.861848		entries	\N	\N
109	TemplateColumn	11	Entries	1	2016-01-15 09:22:43.826092	2016-01-15 09:22:43.826092		entries	\N	\N
112	TemplateField	11	Earnings Test	3	2016-01-15 09:23:25.572719	2016-01-15 09:23:25.572719		earnings-test	\N	\N
115	TemplateField	11	Partial Disability Provision	6	2016-01-15 09:25:30.546397	2016-01-15 09:25:30.546397		partial-disability-provision	\N	\N
116	TemplateField	11	Benefit Integration Offsets	7	2016-01-15 09:25:43.039038	2016-01-15 09:25:43.039038		benefit-integration-offsets	\N	\N
117	TemplateField	11	Recurrent Disability	8	2016-01-15 09:26:00.434192	2016-01-15 09:26:00.434192		recurrent-disability	\N	\N
118	TemplateField	11	Work Incentive Benefit	9	2016-01-15 09:43:43.623936	2016-01-15 09:43:43.623936		work-incentive-benefit	\N	\N
90	TemplateField	9	Definition of Earnings	1	2016-01-15 09:16:43.569737	2016-01-15 10:39:14.358547		definition-of-earnings	\N	t
106	TemplateField	10	Maximum benefit	3	2016-01-15 09:19:55.494544	2016-01-15 10:39:28.140044		maximum-benefit	t	t
105	TemplateField	10	Minimum Benefit	2	2016-01-15 09:19:42.327278	2016-01-15 10:39:21.48709		minimum-benefit	\N	t
108	TemplateField	10	Definition of Disability	5	2016-01-15 09:20:20.235813	2016-01-15 10:39:28.871542		definition-of-disability	t	t
107	TemplateField	10	Guaranteed Issue Limit	4	2016-01-15 09:20:07.149122	2016-01-15 10:39:22.488138		guaranteed-issue-limit	\N	t
111	TemplateField	11	Social Security Integartion	2	2016-01-15 09:23:11.442677	2016-01-15 10:39:37.084938		social-security-integartion	\N	t
104	TemplateField	10	Elimination Period	1	2016-01-15 09:19:30.504466	2016-01-15 10:39:27.559984		elimination-period	t	t
110	TemplateField	11	Zero Day Residual	1	2016-01-15 09:22:57.942876	2016-01-15 10:39:36.257259		zero-day-residual	t	t
113	TemplateField	11	Maximum Benefit Duration	4	2016-01-15 09:23:37.558231	2016-01-15 10:39:42.614775		maximum-benefit-duration	\N	t
114	TemplateField	11	Partial Disability Covered	5	2016-01-15 09:23:59.015785	2016-01-15 10:39:43.258885		partial-disability-covered	\N	t
67	TemplateField	2	Substance Abuse - Inpatient Coverage	16	2016-01-15 08:56:06.479294	2016-01-15 14:51:56.282112		substance-abuse-inpatient-coverage	\N	t
84	TemplateField	2	Chiropractic Services	26	2016-01-15 09:14:39.861876	2016-01-15 14:52:13.050942		chiropractic-services	\N	t
74	TemplateField	2	Physical Therapy - Outpatient Coverage	19	2016-01-15 09:09:51.027308	2016-01-15 14:52:16.102628		physical-therapy-outpatient-coverage	\N	t
93	TemplateField	2	Durable Medical Equipment	33	2016-01-15 09:17:04.272368	2016-01-15 14:52:28.023406		durable-medical-equipment	\N	t
89	TemplateField	2	Infertility Coverage	31	2016-01-15 09:16:33.734949	2016-01-15 14:52:29.587378		infertility-coverage	\N	t
119	TemplateField	11	Rehabilitation	10	2016-01-15 09:44:23.30281	2016-01-15 09:44:23.30281		rehabilitation	\N	\N
120	TemplateField	11	Pre-existing Condition Exclusions	11	2016-01-15 09:45:13.635837	2016-01-15 09:45:13.635837		pre-existing-condition-exclusions	\N	\N
123	TemplateField	11	Self Reported Disability Limitation	14	2016-01-15 09:50:15.519296	2016-01-15 09:50:15.519296		self-reported-disability-limitation	\N	\N
124	TemplateField	11	W2 Services Provided	15	2016-01-15 09:50:45.328315	2016-01-15 09:50:45.328315		w2-services-provided	\N	\N
125	TemplateField	11	Employer FICA match	16	2016-01-15 09:51:41.889152	2016-01-15 09:51:41.889152		employer-fica-match	\N	\N
127	TemplateField	11	International Employees Covered	18	2016-01-15 09:53:00.618676	2016-01-15 09:53:00.618676		international-employees-covered	\N	\N
128	TemplateField	11	EAP	19	2016-01-15 09:53:30.493215	2016-01-15 09:53:30.493215		eap	\N	\N
129	TemplateField	12	Employee Eligibility Class #1	1	2016-01-15 09:54:44.358864	2016-01-15 09:54:44.358864		employee-eligibility-class-1	\N	\N
130	TemplateField	12	Employee Eligibility Class #2	2	2016-01-15 09:55:00.162336	2016-01-15 09:55:00.162336		employee-eligibility-class-2	\N	\N
131	TemplateField	12	Employee Eligibility Class #3	2	2016-01-15 09:56:21.169448	2016-01-15 09:56:21.169448		employee-eligibility-class-3	\N	\N
132	TemplateField	12	Employee Eligibility Class #4	3	2016-01-15 09:56:35.72519	2016-01-15 09:56:35.72519		employee-eligibility-class-4	\N	\N
133	TemplateColumn	12	Employee Class	1	2016-01-15 09:56:49.335012	2016-01-15 09:56:49.335012		employee-class	\N	\N
134	TemplateColumn	12	Employee Classes	2	2016-01-15 09:56:53.760079	2016-01-15 09:56:53.760079		employee-classes	\N	\N
135	TemplateColumn	12	Waiting Period	3	2016-01-15 09:57:01.297382	2016-01-15 09:57:01.297382		waiting-period	\N	\N
136	TemplateColumn	12	Hours Requirement	4	2016-01-15 09:57:11.909613	2016-01-15 09:57:11.909613		hours-requirement	\N	\N
80	TemplateField	8	LTD Amount	1	2016-01-15 09:14:05.254232	2016-01-15 10:39:08.479601		ltd-amount	t	t
121	TemplateField	11	Mental Nervous Limitation	12	2016-01-15 09:46:03.861338	2016-01-15 10:40:51.870237		mental-nervous-limitation	t	t
122	TemplateField	11	Drug & Alcohol Limitation	13	2016-01-15 09:49:47.998861	2016-01-15 10:40:52.747977		drug-alcohol-limitation	t	t
126	TemplateField	11	Gross Up	17	2016-01-15 09:52:23.471357	2016-01-15 10:40:59.50088		gross-up	\N	t
137	TemplateColumn	13	X Multiple of Earnings	1	2016-01-15 11:14:17.547561	2016-01-15 11:14:17.547561		x-multiple-of-earnings	\N	\N
138	TemplateColumn	13	Flat Amount	2	2016-01-15 11:14:45.091831	2016-01-15 11:14:45.091831		flat-amount	\N	\N
141	TemplateColumn	14	Earnings Determination	1	2016-01-15 11:16:28.34891	2016-01-15 11:16:28.34891		earnings-determination	\N	\N
142	TemplateColumn	14	Plus Commission	2	2016-01-15 11:17:55.232872	2016-01-15 11:17:55.232872		plus-commission	\N	\N
144	TemplateField	15	Subject to Medical Deductible & Coinsurance	2	2016-01-15 11:18:54.282094	2016-01-15 11:18:54.282094		subject-to-medical-deductible-coinsurance	\N	\N
146	TemplateField	15	Mandatory Generic	4	2016-01-15 11:19:14.495147	2016-01-15 11:19:14.495147		mandatory-generic	\N	\N
147	TemplateField	15	Step Therapy/Precertification Applies	5	2016-01-15 11:19:31.690291	2016-01-15 11:19:31.690291		step-therapy-precertification-applies	\N	\N
148	TemplateField	15	Oral Contraceptive	6	2016-01-15 11:19:58.857005	2016-01-15 11:19:58.857005		oral-contraceptive	\N	\N
150	TemplateColumn	15	ENTRIES	1	2016-01-15 11:20:22.723128	2016-01-15 11:20:22.723128		entries	\N	\N
151	TemplateColumn	14	Plus Bonus	3	2016-01-15 11:22:28.05886	2016-01-15 11:22:28.05886		plus-bonus	\N	\N
152	TemplateColumn	14	Plus Overtime	4	2016-01-15 11:22:48.880713	2016-01-15 11:22:48.880713		plus-overtime	\N	\N
153	TemplateColumn	14	Value of	5	2016-01-15 11:23:03.264172	2016-01-15 11:23:03.264172		value-of	\N	\N
154	TemplateColumn	14	Dollars	6	2016-01-15 11:23:17.659455	2016-01-15 11:23:17.659455		dollars	\N	\N
155	TemplateColumn	16	Entries	1	2016-01-15 11:23:46.822395	2016-01-15 11:23:46.822395		entries	\N	\N
160	TemplateColumn	17	DEDUCTIBLE	1	2016-01-15 11:26:34.494636	2016-01-15 11:26:34.494636		deductible	\N	\N
161	TemplateColumn	17	PER MEMBER	2	2016-01-15 11:26:40.409807	2016-01-15 11:26:40.409807		per-member	\N	\N
168	TemplateColumn	18	SUBJECT TO RX DEDUCTIBLE	1	2016-01-15 11:31:00.137345	2016-01-15 11:31:00.137345		subject-to-rx-deductible	\N	\N
169	TemplateColumn	18	COPAY	2	2016-01-15 11:31:07.227307	2016-01-15 11:31:07.227307		copay	\N	\N
170	TemplateColumn	18	COINSURANCE	3	2016-01-15 11:31:16.186101	2016-01-15 11:31:16.186101		coinsurance	\N	\N
172	TemplateColumn	18	PER SCRIPT COINSURANCE LIMIT	4	2016-01-15 11:31:39.389758	2016-01-15 11:31:39.389758		per-script-coinsurance-limit	\N	\N
173	TemplateColumn	18	PER SCRIPT COINSURANCE MINIMUM	5	2016-01-15 11:31:47.944087	2016-01-15 11:31:47.944087		per-script-coinsurance-minimum	\N	\N
164	TemplateField	16	Benefit Reduced from Original Amount at age 65 by	5	2016-01-15 11:29:51.666545	2016-01-15 11:54:13.128734		benefit-reduced-from-original-amount-at-age-65-by	\N	t
140	TemplateField	14	Definition of Earnings	1	2016-01-15 11:15:54.958403	2016-01-15 11:53:05.87969		definition-of-earnings	t	t
139	TemplateField	13	AD&D Amount	1	2016-01-15 11:15:05.005088	2016-01-15 11:53:26.799562		ad-d-amount	t	t
163	TemplateField	16	Benefit Reduced from Original Amount at age 60 by	4	2016-01-15 11:29:33.519255	2016-01-15 11:53:56.966844		benefit-reduced-from-original-amount-at-age-60-by	\N	t
157	TemplateField	16	Guaranteed Issue $ Limit	2	2016-01-15 11:24:06.860676	2016-01-15 11:53:56.099474		guaranteed-issue-limit	t	t
156	TemplateField	16	AD&D Maximum Benefit	1	2016-01-15 11:23:57.874482	2016-01-15 11:54:04.596887		ad-d-maximum-benefit	t	t
165	TemplateField	16	Benefit Reduced from Original Amount at age 70 by	6	2016-01-15 11:30:01.461415	2016-01-15 11:54:17.684674		benefit-reduced-from-original-amount-at-age-70-by	\N	t
166	TemplateField	16	Benefit Reduced from Original Amount at age 75 by	7	2016-01-15 11:30:42.48792	2016-01-15 11:54:25.318337		benefit-reduced-from-original-amount-at-age-75-by	\N	t
167	TemplateField	16	Benefit Reduced from Original Amount at age 80 by	8	2016-01-15 11:30:52.969023	2016-01-15 11:54:25.504925		benefit-reduced-from-original-amount-at-age-80-by	\N	t
171	TemplateField	16	Benefit Reduced from Original Amount at age 85 by	9	2016-01-15 11:31:28.313035	2016-01-15 11:55:03.795167		benefit-reduced-from-original-amount-at-age-85-by	\N	t
174	TemplateField	16	Benefit Reduced from Original Amount at age 90 by	10	2016-01-15 11:32:26.559822	2016-01-15 11:55:04.474374		benefit-reduced-from-original-amount-at-age-90-by	\N	t
143	TemplateField	15	Subject to Medical Deductible	1	2016-01-15 11:18:36.477777	2016-01-15 14:52:38.143771		subject-to-medical-deductible	\N	t
145	TemplateField	15	Network	3	2016-01-15 11:19:01.939736	2016-01-15 14:52:39.000716		network	\N	t
288	TemplateField	32	Plan Type	1	2016-01-18 12:18:55.308711	2016-01-18 12:18:55.308711		plan-type	t	t
158	TemplateField	17	Rx Deductible - Individual	1	2016-01-15 11:26:12.063237	2016-01-15 14:52:51.208896		rx-deductible-individual	t	t
159	TemplateField	17	Rx Deductible - Family	2	2016-01-15 11:26:26.664222	2016-01-15 14:52:50.551403		rx-deductible-family	t	t
289	TemplateField	32	Materials Coverage	2	2016-01-18 12:18:55.316691	2016-01-18 12:18:55.316691		materials-coverage	t	t
185	TemplateColumn	19	Entries	1	2016-01-15 11:38:49.374929	2016-01-15 11:38:49.374929		entries	\N	\N
186	TemplateField	19	Life	1	2016-01-15 11:39:02.889572	2016-01-15 11:39:02.889572		life	\N	\N
187	TemplateField	19	Both Hands	2	2016-01-15 11:39:16.728924	2016-01-15 11:39:16.728924		both-hands	\N	\N
188	TemplateField	19	Both Feet	3	2016-01-15 11:39:27.433666	2016-01-15 11:39:27.433666		both-feet	\N	\N
189	TemplateField	19	Sight of Both Eyes	4	2016-01-15 11:39:38.146666	2016-01-15 11:39:38.146666		sight-of-both-eyes	\N	\N
190	TemplateField	19	Speech and Hearing	5	2016-01-15 11:40:27.474163	2016-01-15 11:40:27.474163		speech-and-hearing	\N	\N
191	TemplateField	19	One Hand	6	2016-01-15 11:40:44.298798	2016-01-15 11:40:44.298798		one-hand	\N	\N
192	TemplateField	19	One Foot	7	2016-01-15 11:40:54.135352	2016-01-15 11:40:54.135352		one-foot	\N	\N
193	TemplateField	19	Sight of One Eye	8	2016-01-15 11:41:06.744389	2016-01-15 11:41:06.744389		sight-of-one-eye	\N	\N
194	TemplateField	19	Speech	9	2016-01-15 11:41:18.226956	2016-01-15 11:41:18.226956		speech	\N	\N
195	TemplateField	19	Hearing	10	2016-01-15 11:41:36.63636	2016-01-15 11:41:36.63636		hearing	\N	\N
196	TemplateField	19	Quadraplegia	11	2016-01-15 11:41:48.477314	2016-01-15 11:41:48.477314		quadraplegia	\N	\N
197	TemplateField	19	Paraplegia	12	2016-01-15 11:41:58.555753	2016-01-15 11:41:58.555753		paraplegia	\N	\N
198	TemplateField	19	Hemiplegia	13	2016-01-15 11:42:09.145598	2016-01-15 11:42:09.145598		hemiplegia	\N	\N
199	TemplateField	19	Uniplegia	14	2016-01-15 11:42:19.209266	2016-01-15 11:42:19.209266		uniplegia	\N	\N
200	TemplateField	20	Employee Eligibility Class #1	1	2016-01-15 11:46:28.881208	2016-01-15 11:46:28.881208		employee-eligibility-class-1	\N	\N
201	TemplateField	20	Employee Eligibility Class #2	2	2016-01-15 11:46:36.999346	2016-01-15 11:46:36.999346		employee-eligibility-class-2	\N	\N
202	TemplateField	20	Employee Eligibility Class #3	3	2016-01-15 11:46:46.906992	2016-01-15 11:46:46.906992		employee-eligibility-class-3	\N	\N
203	TemplateField	20	Employee Eligibility Class #4	4	2016-01-15 11:46:56.763245	2016-01-15 11:46:56.763245		employee-eligibility-class-4	\N	\N
204	TemplateColumn	20	Employee Class	1	2016-01-15 11:47:01.645916	2016-01-15 11:47:01.645916		employee-class	\N	\N
205	TemplateColumn	20	Employee Classes	2	2016-01-15 11:47:05.960451	2016-01-15 11:47:05.960451		employee-classes	\N	\N
206	TemplateColumn	20	Waiting Period	3	2016-01-15 11:47:11.20421	2016-01-15 11:47:11.20421		waiting-period	\N	\N
207	TemplateColumn	20	Hours Requirement	4	2016-01-15 11:47:29.696972	2016-01-15 11:47:29.696972		hours-requirement	\N	\N
162	TemplateField	16	Guaranteed Issue Multiple of Earnings Limit	3	2016-01-15 11:29:13.622275	2016-01-15 11:53:56.877184		guaranteed-issue-multiple-of-earnings-limit	\N	t
175	TemplateField	16	Benefit Reduced from Original Amount at age 95 by	11	2016-01-15 11:32:44.547427	2016-01-15 11:55:06.120109		benefit-reduced-from-original-amount-at-age-95-by	\N	t
178	TemplateField	16	Benefit Reduced from Original Amount at age 100 by	12	2016-01-15 11:33:11.982565	2016-01-15 11:55:06.661303		benefit-reduced-from-original-amount-at-age-100-by	\N	t
208	TemplateColumn	21	X Multiple of Earnings	1	2016-01-15 11:57:34.951252	2016-01-15 11:57:51.572922		x-multiple-of-earnings	\N	\N
210	TemplateColumn	21	Flat Amount	2	2016-01-15 11:58:19.80948	2016-01-15 11:58:19.80948		flat-amount	\N	\N
212	TemplateColumn	22	Earnings Determination	1	2016-01-15 12:01:06.990377	2016-01-15 12:01:06.990377		earnings-determination	\N	\N
213	TemplateColumn	22	Plus Commission	2	2016-01-15 12:01:18.290342	2016-01-15 12:01:18.290342		plus-commission	\N	\N
214	TemplateColumn	22	Plus Bonus	3	2016-01-15 12:01:33.164122	2016-01-15 12:01:33.164122		plus-bonus	\N	\N
215	TemplateColumn	22	Plus Overtime	4	2016-01-15 12:01:48.550207	2016-01-15 12:01:48.550207		plus-overtime	\N	\N
216	TemplateColumn	22	Value of	5	2016-01-15 12:02:01.04291	2016-01-15 12:02:01.04291		value-of	\N	\N
217	TemplateColumn	22	Dollars	6	2016-01-15 12:02:12.597444	2016-01-15 12:02:12.597444		dollars	\N	\N
221	TemplateColumn	23	Entries	1	2016-01-15 12:05:43.909373	2016-01-15 12:05:43.909373		entries	\N	\N
220	TemplateField	23	Guaranteed Issue Multiple of Earnings Limit	3	2016-01-15 12:05:33.055722	2016-01-15 12:32:01.528247		guaranteed-issue-multiple-of-earnings-limit	\N	t
211	TemplateField	22	Definition of Earnings	1	2016-01-15 12:00:56.2159	2016-01-15 12:31:51.537126		definition-of-earnings	t	t
218	TemplateField	23	Life Maximum Benefit	1	2016-01-15 12:05:09.756505	2016-01-15 12:32:00.565292		life-maximum-benefit	t	t
219	TemplateField	23	Guaranteed Issue $ Limit	2	2016-01-15 12:05:22.361892	2016-01-15 12:31:59.52747		guaranteed-issue-limit	t	t
222	TemplateField	23	Benefit Reduced from Original Amount at age 60 by	4	2016-01-15 12:06:49.689952	2016-01-15 12:32:02.190611		benefit-reduced-from-original-amount-at-age-60-by	\N	t
223	TemplateField	23	Benefit Reduced from Original Amount at age 65 by	5	2016-01-15 12:10:23.971144	2016-01-15 12:32:08.194301		benefit-reduced-from-original-amount-at-age-65-by	\N	t
224	TemplateField	23	Benefit Reduced from Original Amount at age 70 by	6	2016-01-15 12:10:34.165322	2016-01-15 12:32:08.895921		benefit-reduced-from-original-amount-at-age-70-by	\N	t
226	TemplateField	23	Benefit Reduced from Original Amount at age 80 by	8	2016-01-15 12:10:54.866798	2016-01-15 12:32:11.775073		benefit-reduced-from-original-amount-at-age-80-by	\N	t
227	TemplateField	23	Benefit Reduced from Original Amount at age 85 by	9	2016-01-15 12:11:30.306613	2016-01-15 12:32:12.462097		benefit-reduced-from-original-amount-at-age-85-by	\N	t
228	TemplateField	23	Benefit Reduced from Original Amount at age 90 by	10	2016-01-15 12:11:52.54133	2016-01-15 12:32:20.136683		benefit-reduced-from-original-amount-at-age-90-by	\N	t
229	TemplateField	23	Benefit Reduced from Original Amount at age 95 by	11	2016-01-15 12:12:06.105461	2016-01-15 12:32:20.911078		benefit-reduced-from-original-amount-at-age-95-by	\N	t
230	TemplateField	23	Benefit Reduced from Original Amount at age 100 by	12	2016-01-15 12:12:18.259346	2016-01-15 12:32:23.155592		benefit-reduced-from-original-amount-at-age-100-by	\N	t
184	TemplateField	18	Specialty Medications  -Retail	61	2016-01-15 11:35:13.774193	2016-01-18 15:04:04.171722		specialty-medications-retail	\N	\N
231	TemplateField	23	Coverage disabled before age 60	13	2016-01-15 12:12:28.610376	2016-01-15 12:32:24.802087		coverage-disabled-before-age-60	t	t
176	TemplateField	18	Retail Rx (Tier 1)	1	2016-01-15 11:32:48.742587	2016-01-15 14:52:59.401359		retail-rx-tier-1	t	t
179	TemplateField	18	Retail Rx (Tier 3)	13	2016-01-15 11:33:30.899913	2016-01-18 15:03:13.467022		retail-rx-tier-3	t	t
182	TemplateField	18	Mail Order Rx (Tier 1)	31	2016-01-15 11:34:10.020139	2016-01-18 15:03:13.475137		mail-order-rx-tier-1	\N	t
180	TemplateField	18	Retail Rx (Tier 4)	19	2016-01-15 11:33:44.883993	2016-01-18 15:03:13.46248		retail-rx-tier-4	\N	\N
183	TemplateField	18	Specialty Medications -Mail Order	67	2016-01-15 11:34:54.239578	2016-01-18 15:04:04.180985		specialty-medications-mail-order	\N	f
181	TemplateField	18	Retail Rx (Tier 5)	25	2016-01-15 11:33:53.509612	2016-01-18 15:03:13.462785		retail-rx-tier-5	\N	\N
232	TemplateField	23	Coverage disabled after age 60	14	2016-01-15 12:15:36.755723	2016-01-15 12:15:36.755723		coverage-disabled-after-age-60	\N	\N
233	TemplateField	23	Family Medical Leave Continuation	15	2016-01-15 12:18:48.311938	2016-01-15 12:18:48.311938		family-medical-leave-continuation	\N	\N
234	TemplateField	23	Military Leave Continuation	16	2016-01-15 12:19:02.072859	2016-01-15 12:19:02.072859		military-leave-continuation	\N	\N
235	TemplateField	23	Military Convalescence Support Continuation	17	2016-01-15 12:19:14.268325	2016-01-15 12:19:14.268325		military-convalescence-support-continuation	\N	\N
236	TemplateColumn	24	Entries	1	2016-01-15 12:21:17.370418	2016-01-15 12:21:17.370418		entries	\N	\N
238	TemplateField	24	Portability	2	2016-01-15 12:21:58.680096	2016-01-15 12:21:58.680096		portability	\N	\N
239	TemplateField	24	International Employees Covered	3	2016-01-15 12:22:10.994854	2016-01-15 12:22:10.994854		international-employees-covered	\N	\N
240	TemplateField	24	Suicide Exclusions	4	2016-01-15 12:22:21.131539	2016-01-15 12:22:21.131539		suicide-exclusions	\N	\N
241	TemplateField	24	Military Service Exclusions	5	2016-01-15 12:22:38.492134	2016-01-15 12:22:38.492134		military-service-exclusions	\N	\N
242	TemplateField	24	Travel Assistance	6	2016-01-15 12:22:48.657874	2016-01-15 12:22:48.657874		travel-assistance	\N	\N
243	TemplateField	24	Bereavement Counseling	7	2016-01-15 12:22:58.817285	2016-01-15 12:22:58.817285		bereavement-counseling	\N	\N
244	TemplateField	24	EAP	8	2016-01-15 12:23:09.012229	2016-01-15 12:23:09.012229		eap	\N	\N
245	TemplateField	24	Act of  War Exclusion	9	2016-01-15 12:23:18.968754	2016-01-15 12:23:18.968754		act-of-war-exclusion	\N	\N
246	TemplateField	25	Accelerated Death Benefit	1	2016-01-15 12:25:28.312098	2016-01-15 12:25:28.312098		accelerated-death-benefit	\N	\N
247	TemplateColumn	25	Benefit Paid	1	2016-01-15 12:25:36.746096	2016-01-15 12:25:36.746096		benefit-paid	\N	\N
248	TemplateColumn	25	Maximum	2	2016-01-15 12:25:46.291483	2016-01-15 12:25:46.291483		maximum	\N	\N
249	TemplateColumn	25	Flat Amount	3	2016-01-15 12:25:57.427184	2016-01-15 12:25:57.427184		flat-amount	\N	\N
250	TemplateField	26	Employee Eligibility Class #1	1	2016-01-15 12:27:16.907199	2016-01-15 12:27:16.907199		employee-eligibility-class-1	\N	\N
251	TemplateField	26	Employee Eligibility Class #2	2	2016-01-15 12:27:23.093496	2016-01-15 12:27:23.093496		employee-eligibility-class-2	\N	\N
252	TemplateField	26	Employee Eligibility Class #3	3	2016-01-15 12:27:27.110324	2016-01-15 12:27:27.110324		employee-eligibility-class-3	\N	\N
253	TemplateField	26	Employee Eligibility Class #4	4	2016-01-15 12:27:37.624159	2016-01-15 12:27:37.624159		employee-eligibility-class-4	\N	\N
254	TemplateColumn	26	Employee Class	1	2016-01-15 12:27:42.669782	2016-01-15 12:27:42.669782		employee-class	\N	\N
255	TemplateColumn	26	Employee Classes	2	2016-01-15 12:27:46.782921	2016-01-15 12:27:46.782921		employee-classes	\N	\N
256	TemplateColumn	26	Waiting Period	3	2016-01-15 12:27:56.944399	2016-01-15 12:27:56.944399		waiting-period	\N	\N
257	TemplateColumn	26	Hours Requirement	4	2016-01-15 12:28:03.465508	2016-01-15 12:28:03.465508		hours-requirement	\N	\N
209	TemplateField	21	Life Amount	1	2016-01-15 11:57:40.903439	2016-01-15 12:31:44.365197		life-amount	t	t
225	TemplateField	23	Benefit Reduced from Original Amount at age 75 by	7	2016-01-15 12:10:43.208868	2016-01-15 12:32:09.747517		benefit-reduced-from-original-amount-at-age-75-by	\N	t
237	TemplateField	24	Conversion	1	2016-01-15 12:21:48.685772	2016-01-15 12:32:32.756116		conversion	\N	t
258	TemplateColumn	27	IDENTIFICATION METHODS	1	2016-01-15 14:04:51.019731	2016-01-15 14:04:51.019731		identification-methods	\N	\N
259	TemplateColumn	27	NUMBER OF CONDITIONS TRACKED	2	2016-01-15 14:05:11.135428	2016-01-15 14:05:11.135428		number-of-conditions-tracked	\N	\N
260	TemplateColumn	27	OUTREACH METHODS	3	2016-01-15 14:05:24.446707	2016-01-15 14:05:24.446707		outreach-methods	\N	\N
261	TemplateColumn	27	REPORT FREQUENCY	4	2016-01-15 14:05:38.858879	2016-01-15 14:05:38.858879		report-frequency	\N	\N
263	TemplateField	28	Integrated Wellness Plan	1	2016-01-15 14:43:51.328554	2016-01-15 14:43:51.328554		integrated-wellness-plan	\N	\N
264	TemplateColumn	28	Entries	1	2016-01-15 14:43:57.189654	2016-01-15 14:43:57.189654		entries	\N	\N
270	TemplateField	29	International Employees Covered	3	2016-01-18 10:48:46.171552	2016-01-18 10:48:46.171552		international-employees-covered	\N	\N
10	TemplateField	1	Out-of-Pocket Max Individual (includes deductible)	17	2016-01-15 06:59:48.963757	2016-01-15 14:46:48.713038		out-of-pocket-max-individual-includes-deductible	t	t
271	TemplateColumn	30	Limit	1	2016-01-18 10:49:49.514337	2016-01-18 10:49:49.514337		limit	\N	\N
272	TemplateColumn	30	Coinsurance	2	2016-01-18 10:49:58.546397	2016-01-18 10:49:58.546397		coinsurance	\N	\N
265	TemplateField	1	Out-of-Pocket Maximum - Family Multiple	21	2016-01-15 14:46:44.462749	2016-01-15 14:50:50.913604		out-of-pocket-maximum-family-multiple	\N	f
266	TemplateField	1	In & Out-of-Network OOP Maximum Cross Accumulation	25	2016-01-15 14:48:32.87071	2016-01-15 14:51:00.562257		in-out-of-network-oop-maximum-cross-accumulation	\N	t
72	TemplateField	2	Substance Abuse - Outpatient Coverage	17	2016-01-15 08:59:12.153861	2016-01-15 14:51:58.447844		substance-abuse-outpatient-coverage	\N	t
267	TemplateColumn	29	Entries	1	2016-01-18 10:48:10.510759	2016-01-18 10:48:10.510759		entries	\N	\N
276	TemplateField	30	Lenses - Lined Bifocal	4	2016-01-18 10:52:36.763324	2016-01-18 10:52:36.763324		lenses-lined-bifocal	\N	\N
277	TemplateField	30	Lenses - Lined Trifocal	5	2016-01-18 10:53:24.341598	2016-01-18 10:53:24.341598		lenses-lined-trifocal	\N	\N
278	TemplateField	30	Lenses - Lenticular	6	2016-01-18 10:54:07.135562	2016-01-18 10:54:07.135562		lenses-lenticular	\N	\N
280	TemplateColumn	31	Allowance	1	2016-01-18 11:00:36.978853	2016-01-18 11:00:36.978853		allowance	\N	\N
283	TemplateField	31	Contacts - Non Selection	3	2016-01-18 11:01:15.318632	2016-01-18 11:01:15.318632		contacts-non-selection	\N	\N
284	TemplateField	31	Contacts - Elective Disposable	4	2016-01-18 11:01:28.64664	2016-01-18 11:01:28.64664		contacts-elective-disposable	\N	\N
285	TemplateField	31	Contacts -Necessary Lenses	5	2016-01-18 11:01:44.659351	2016-01-18 11:01:44.659351		contacts-necessary-lenses	\N	\N
286	TemplateField	31	Premium Progressive Lenses	6	2016-01-18 11:02:43.159996	2016-01-18 11:02:43.159996		premium-progressive-lenses	\N	\N
287	TemplateField	32	International Employees Covered	3	2016-01-18 12:18:55.298794	2016-01-18 12:18:55.298794		international-employees-covered	\N	\N
268	TemplateField	29	Plan Type	1	2016-01-18 10:48:24.250602	2016-01-18 11:06:05.567741		plan-type	t	t
269	TemplateField	29	Materials Coverage	2	2016-01-18 10:48:35.63987	2016-01-18 11:06:04.82709		materials-coverage	t	t
273	TemplateField	30	Exams	1	2016-01-18 10:50:12.508143	2016-01-18 11:06:17.709477		exams	t	t
274	TemplateField	30	Frames	2	2016-01-18 10:50:21.728008	2016-01-18 11:06:15.67605		frames	t	t
275	TemplateField	30	Lenses - Single Vision	3	2016-01-18 10:51:53.542837	2016-01-18 11:06:15.164592		lenses-single-vision	t	t
279	TemplateField	30	Contacts	7	2016-01-18 10:54:54.688531	2016-01-18 11:06:27.179012		contacts	t	t
281	TemplateField	31	Retail Frames	1	2016-01-18 11:00:48.677831	2016-01-18 11:06:37.118443		retail-frames	t	t
282	TemplateField	31	Contacts - Covered Selection	2	2016-01-18 11:01:01.257522	2016-01-18 11:06:36.502102		contacts-covered-selection	t	t
290	TemplateColumn	32	Entries	1	2016-01-18 12:18:55.3263	2016-01-18 12:18:55.3263		entries	\N	\N
291	TemplateField	33	Contacts - Non Selection	3	2016-01-18 12:18:55.383849	2016-01-18 12:18:55.383849		contacts-non-selection	\N	\N
292	TemplateField	33	Contacts - Elective Disposable	4	2016-01-18 12:18:55.391212	2016-01-18 12:18:55.391212		contacts-elective-disposable	\N	\N
293	TemplateField	33	Contacts -Necessary Lenses	5	2016-01-18 12:18:55.399507	2016-01-18 12:18:55.399507		contacts-necessary-lenses	\N	\N
294	TemplateField	33	Premium Progressive Lenses	6	2016-01-18 12:18:55.406788	2016-01-18 12:18:55.406788		premium-progressive-lenses	\N	\N
295	TemplateField	33	Retail Frames	1	2016-01-18 12:18:55.416155	2016-01-18 12:18:55.416155		retail-frames	t	t
296	TemplateField	33	Contacts - Covered Selection	2	2016-01-18 12:18:55.423603	2016-01-18 12:18:55.423603		contacts-covered-selection	t	t
297	TemplateColumn	33	Allowance	1	2016-01-18 12:18:55.433726	2016-01-18 12:18:55.433726		allowance	\N	\N
298	TemplateField	34	Lenses - Lined Bifocal	4	2016-01-18 12:18:55.529072	2016-01-18 12:18:55.529072		lenses-lined-bifocal	\N	\N
299	TemplateField	34	Lenses - Lined Trifocal	5	2016-01-18 12:18:55.53624	2016-01-18 12:18:55.53624		lenses-lined-trifocal	\N	\N
300	TemplateField	34	Lenses - Lenticular	6	2016-01-18 12:18:55.543047	2016-01-18 12:18:55.543047		lenses-lenticular	\N	\N
301	TemplateField	34	Exams	1	2016-01-18 12:18:55.550228	2016-01-18 12:18:55.550228		exams	t	t
302	TemplateField	34	Frames	2	2016-01-18 12:18:55.558053	2016-01-18 12:18:55.558053		frames	t	t
303	TemplateField	34	Lenses - Single Vision	3	2016-01-18 12:18:55.564979	2016-01-18 12:18:55.564979		lenses-single-vision	t	t
304	TemplateField	34	Contacts	7	2016-01-18 12:18:55.573105	2016-01-18 12:18:55.573105		contacts	t	t
305	TemplateColumn	34	Coinsurance	2	2016-01-18 12:18:55.583334	2016-01-18 12:18:55.583334		coinsurance	\N	\N
306	TemplateColumn	34	Limit	1	2016-01-18 12:18:55.590536	2016-01-18 12:18:55.590536		limit	\N	\N
307	TemplateColumn	35	Entries	1	2016-01-18 12:20:48.260601	2016-01-18 12:20:48.260601		entries	\N	\N
308	TemplateField	35	Plan Type	1	2016-01-18 12:20:57.041657	2016-01-18 12:20:57.041657		plan-type	\N	\N
309	TemplateField	35	Materials Coverage	2	2016-01-18 12:21:06.238151	2016-01-18 12:21:06.238151		materials-coverage	\N	\N
310	TemplateField	35	International Employees Covered	3	2016-01-18 12:21:17.178058	2016-01-18 12:21:17.178058		international-employees-covered	\N	\N
311	TemplateColumn	36	Limit	1	2016-01-18 12:25:20.137048	2016-01-18 12:25:20.137048		limit	\N	\N
312	TemplateColumn	36	Coinsurance	2	2016-01-18 12:25:29.792388	2016-01-18 12:25:29.792388		coinsurance	\N	\N
313	TemplateColumn	36	copay	3	2016-01-18 12:25:39.888783	2016-01-18 12:25:39.888783		copay	\N	\N
314	TemplateField	36	Exams	1	2016-01-18 12:25:59.778321	2016-01-18 12:25:59.778321		exams	\N	\N
315	TemplateField	36	Frames	2	2016-01-18 12:26:09.820172	2016-01-18 12:26:09.820172		frames	\N	\N
316	TemplateField	36	Lenses - Single Vision	3	2016-01-18 12:26:30.98583	2016-01-18 12:26:30.98583		lenses-single-vision	\N	\N
317	TemplateField	36	Lenses - Lined Bifocal	4	2016-01-18 12:26:46.335799	2016-01-18 12:26:46.335799		lenses-lined-bifocal	\N	\N
318	TemplateField	36	Lenses - Lined Trifocal	5	2016-01-18 12:27:00.416172	2016-01-18 12:27:00.416172		lenses-lined-trifocal	\N	\N
319	TemplateField	36	Lenses - Lenticular	6	2016-01-18 12:27:11.851294	2016-01-18 12:27:11.851294		lenses-lenticular	\N	\N
320	TemplateField	36	Contacts	7	2016-01-18 12:27:26.244993	2016-01-18 12:27:26.244993		contacts	\N	\N
321	TemplateColumn	37	Discount (Overage or General)	1	2016-01-18 12:32:45.739726	2016-01-18 12:32:45.739726		discount-overage-or-general	\N	\N
322	TemplateColumn	37	Allowance	2	2016-01-18 12:32:58.298958	2016-01-18 12:32:58.298958		allowance	\N	\N
323	TemplateField	37	Retail Frames	1	2016-01-18 12:33:09.122598	2016-01-18 12:33:09.122598		retail-frames	\N	\N
324	TemplateField	37	Contacts - Covered Selection	2	2016-01-18 12:33:17.913759	2016-01-18 12:33:17.913759		contacts-covered-selection	\N	\N
325	TemplateField	37	Contacts - Non Selection	3	2016-01-18 12:33:28.845341	2016-01-18 12:33:28.845341		contacts-non-selection	\N	\N
326	TemplateField	37	Contacts - Elective Disposable	4	2016-01-18 12:33:38.504062	2016-01-18 12:33:38.504062		contacts-elective-disposable	\N	\N
327	TemplateField	37	Contacts -Necessary Lenses	5	2016-01-18 12:33:47.513123	2016-01-18 12:33:47.513123		contacts-necessary-lenses	\N	\N
328	TemplateField	37	Premium Progressive Lenses	6	2016-01-18 12:33:57.332387	2016-01-18 12:33:57.332387		premium-progressive-lenses	\N	\N
329	TemplateColumn	38	Employee Class	1	2016-01-18 12:47:45.43395	2016-01-18 12:47:45.43395		employee-class	\N	\N
330	TemplateColumn	38	Employee Classes	2	2016-01-18 12:47:50.534833	2016-01-18 12:47:50.534833		employee-classes	\N	\N
331	TemplateColumn	38	Waiting Period	3	2016-01-18 12:47:57.242743	2016-01-18 12:47:57.242743		waiting-period	\N	\N
332	TemplateColumn	38	Hours Requirement	4	2016-01-18 12:48:04.396924	2016-01-18 12:48:04.396924		hours-requirement	\N	\N
333	TemplateColumn	38	Spouse Eligible	5	2016-01-18 12:48:19.322984	2016-01-18 12:48:19.322984		spouse-eligible	\N	\N
334	TemplateColumn	38	Dependent Children Eligible	6	2016-01-18 12:48:28.230207	2016-01-18 12:48:28.230207		dependent-children-eligible	\N	\N
335	TemplateColumn	38	Domestic Partner Eligible	7	2016-01-18 12:48:56.043984	2016-01-18 12:48:56.043984		domestic-partner-eligible	\N	\N
336	TemplateColumn	38	Dependent Child Limiting Age	8	2016-01-18 12:49:06.514183	2016-01-18 12:49:06.514183		dependent-child-limiting-age	\N	\N
337	TemplateColumn	38	Dependent Child Coverage Ends	9	2016-01-18 12:49:21.051178	2016-01-18 12:49:21.051178		dependent-child-coverage-ends	\N	\N
149	TemplateField	15	Prescriptions Covered Out of Network	7	2016-01-15 11:20:12.030721	2016-01-18 14:39:12.083638		prescriptions-covered-out-of-network	\N	\N
262	TemplateField	27	Disease Management	1	2016-01-15 14:05:52.297092	2016-01-18 14:55:44.112549		disease-management	\N	\N
177	TemplateField	18	Retail Rx (Tier 2)	7	2016-01-15 11:33:07.047101	2016-01-18 15:03:13.464281		retail-rx-tier-2	t	t
341	TemplateField	18	Mail Order Rx (Tier 5)	55	2016-01-18 15:03:07.79276	2016-01-18 15:03:46.582749		mail-order-rx-tier-5	\N	\N
342	TemplateField	39	Employee Eligibility Class #1	1	2016-01-18 15:07:51.864288	2016-01-18 15:07:51.864288		employee-eligibility-class-1	\N	\N
340	TemplateField	18	Mail Order Rx (Tier 4)	49	2016-01-18 15:02:58.784553	2016-01-18 15:03:46.580303		mail-order-rx-tier-4	\N	\N
343	TemplateField	39	Employee Eligibility Class #2	2	2016-01-18 15:08:03.276109	2016-01-18 15:08:03.276109		employee-eligibility-class-2	\N	\N
339	TemplateField	18	Mail Order Rx (Tier 3)	43	2016-01-18 15:02:49.098478	2016-01-18 17:34:20.797571		mail-order-rx-tier-3	\N	t
344	TemplateField	39	Employee Eligibility Class #3	3	2016-01-18 15:08:11.839042	2016-01-18 15:08:11.839042		employee-eligibility-class-3	\N	\N
345	TemplateField	39	Employee Eligibility Class #4	4	2016-01-18 15:08:20.830701	2016-01-18 15:08:20.830701		employee-eligibility-class-4	\N	\N
346	TemplateColumn	39	Employee Class	1	2016-01-18 15:08:44.166425	2016-01-18 15:08:44.166425		employee-class	\N	\N
347	TemplateColumn	39	Employee Classes	2	2016-01-18 15:09:10.033682	2016-01-18 15:09:10.033682		employee-classes	\N	\N
348	TemplateColumn	39	Waiting Period	3	2016-01-18 15:09:41.032036	2016-01-18 15:09:41.032036		waiting-period	\N	\N
349	TemplateColumn	39	Hours Requirement	4	2016-01-18 15:14:25.025446	2016-01-18 15:14:25.025446		hours-requirement	\N	\N
350	TemplateColumn	39	Spouse Eligible	5	2016-01-18 15:14:39.138207	2016-01-18 15:14:39.138207		spouse-eligible	\N	\N
351	TemplateColumn	39	Dependent Children Eligible	6	2016-01-18 15:15:04.199282	2016-01-18 15:15:04.199282		dependent-children-eligible	\N	\N
352	TemplateColumn	39	Domestic Partner Eligible	7	2016-01-18 15:15:18.878899	2016-01-18 15:15:18.878899		domestic-partner-eligible	\N	\N
353	TemplateColumn	39	Dependent Child Limiting Age	8	2016-01-18 15:15:40.190302	2016-01-18 15:15:40.190302		dependent-child-limiting-age	\N	\N
354	TemplateColumn	39	Dependent Child Coverage Ends	9	2016-01-18 15:16:02.905153	2016-01-18 15:16:02.905153		dependent-child-coverage-ends	\N	\N
338	TemplateField	18	Mail Order Rx (Tier 2)	37	2016-01-18 15:02:41.198869	2016-01-18 17:34:19.140864		mail-order-rx-tier-2	\N	t
355	TemplateField	40	International Employees Covered	3	2016-01-18 17:55:34.545961	2016-01-18 17:55:34.545961		international-employees-covered	\N	\N
356	TemplateField	40	Plan Type	1	2016-01-18 17:55:34.554212	2016-01-18 17:55:34.554212		plan-type	t	t
357	TemplateField	40	Materials Coverage	2	2016-01-18 17:55:34.561767	2016-01-18 17:55:34.561767		materials-coverage	t	t
358	TemplateColumn	40	Entries	1	2016-01-18 17:55:34.570488	2016-01-18 17:55:34.570488		entries	\N	\N
359	TemplateField	41	Contacts - Non Selection	3	2016-01-18 17:55:34.614077	2016-01-18 17:55:34.614077		contacts-non-selection	\N	\N
360	TemplateField	41	Contacts - Elective Disposable	4	2016-01-18 17:55:34.621765	2016-01-18 17:55:34.621765		contacts-elective-disposable	\N	\N
361	TemplateField	41	Contacts -Necessary Lenses	5	2016-01-18 17:55:34.629523	2016-01-18 17:55:34.629523		contacts-necessary-lenses	\N	\N
362	TemplateField	41	Premium Progressive Lenses	6	2016-01-18 17:55:34.63681	2016-01-18 17:55:34.63681		premium-progressive-lenses	\N	\N
363	TemplateField	41	Retail Frames	1	2016-01-18 17:55:34.644797	2016-01-18 17:55:34.644797		retail-frames	t	t
364	TemplateField	41	Contacts - Covered Selection	2	2016-01-18 17:55:34.654139	2016-01-18 17:55:34.654139		contacts-covered-selection	t	t
365	TemplateColumn	41	Allowance	1	2016-01-18 17:55:34.663433	2016-01-18 17:55:34.663433		allowance	\N	\N
366	TemplateField	42	Lenses - Lined Bifocal	4	2016-01-18 17:55:34.75987	2016-01-18 17:55:34.75987		lenses-lined-bifocal	\N	\N
367	TemplateField	42	Lenses - Lined Trifocal	5	2016-01-18 17:55:34.767138	2016-01-18 17:55:34.767138		lenses-lined-trifocal	\N	\N
368	TemplateField	42	Lenses - Lenticular	6	2016-01-18 17:55:34.774315	2016-01-18 17:55:34.774315		lenses-lenticular	\N	\N
369	TemplateField	42	Exams	1	2016-01-18 17:55:34.783173	2016-01-18 17:55:34.783173		exams	t	t
370	TemplateField	42	Frames	2	2016-01-18 17:55:34.790574	2016-01-18 17:55:34.790574		frames	t	t
371	TemplateField	42	Lenses - Single Vision	3	2016-01-18 17:55:34.799195	2016-01-18 17:55:34.799195		lenses-single-vision	t	t
372	TemplateField	42	Contacts	7	2016-01-18 17:55:34.806942	2016-01-18 17:55:34.806942		contacts	t	t
373	TemplateColumn	42	Limit	1	2016-01-18 17:55:34.815741	2016-01-18 17:55:34.815741		limit	\N	\N
374	TemplateColumn	42	Coinsurance	2	2016-01-18 17:55:34.823137	2016-01-18 17:55:34.823137		coinsurance	\N	\N
375	TemplateField	43	Life	1	2016-01-18 18:31:07.558147	2016-01-18 18:31:07.558147		life	\N	\N
376	TemplateField	43	Both Hands	2	2016-01-18 18:31:07.56703	2016-01-18 18:31:07.56703		both-hands	\N	\N
377	TemplateField	43	Both Feet	3	2016-01-18 18:31:07.574274	2016-01-18 18:31:07.574274		both-feet	\N	\N
378	TemplateField	43	Sight of Both Eyes	4	2016-01-18 18:31:07.583306	2016-01-18 18:31:07.583306		sight-of-both-eyes	\N	\N
379	TemplateField	43	Speech and Hearing	5	2016-01-18 18:31:07.59074	2016-01-18 18:31:07.59074		speech-and-hearing	\N	\N
380	TemplateField	43	One Hand	6	2016-01-18 18:31:07.597831	2016-01-18 18:31:07.597831		one-hand	\N	\N
381	TemplateField	43	One Foot	7	2016-01-18 18:31:07.606143	2016-01-18 18:31:07.606143		one-foot	\N	\N
382	TemplateField	43	Sight of One Eye	8	2016-01-18 18:31:07.614208	2016-01-18 18:31:07.614208		sight-of-one-eye	\N	\N
383	TemplateField	43	Speech	9	2016-01-18 18:31:07.621825	2016-01-18 18:31:07.621825		speech	\N	\N
384	TemplateField	43	Hearing	10	2016-01-18 18:31:07.629627	2016-01-18 18:31:07.629627		hearing	\N	\N
385	TemplateField	43	Quadraplegia	11	2016-01-18 18:31:07.636862	2016-01-18 18:31:07.636862		quadraplegia	\N	\N
386	TemplateField	43	Paraplegia	12	2016-01-18 18:31:07.644106	2016-01-18 18:31:07.644106		paraplegia	\N	\N
387	TemplateField	43	Hemiplegia	13	2016-01-18 18:31:07.651638	2016-01-18 18:31:07.651638		hemiplegia	\N	\N
388	TemplateField	43	Uniplegia	14	2016-01-18 18:31:07.658999	2016-01-18 18:31:07.658999		uniplegia	\N	\N
389	TemplateColumn	43	Entries	1	2016-01-18 18:31:07.668064	2016-01-18 18:31:07.668064		entries	\N	\N
390	TemplateField	44	AD&D Amount	1	2016-01-18 18:31:07.816224	2016-01-18 18:31:07.816224		ad-d-amount	t	t
391	TemplateColumn	44	X Multiple of Earnings	1	2016-01-18 18:31:07.825564	2016-01-18 18:31:07.825564		x-multiple-of-earnings	\N	\N
392	TemplateColumn	44	Flat Amount	2	2016-01-18 18:31:07.834891	2016-01-18 18:31:07.834891		flat-amount	\N	\N
393	TemplateField	45	Benefit Reduced from Original Amount at age 65 by	5	2016-01-18 18:31:07.874761	2016-01-18 18:31:07.874761		benefit-reduced-from-original-amount-at-age-65-by	\N	t
394	TemplateField	45	Benefit Reduced from Original Amount at age 60 by	4	2016-01-18 18:31:07.883124	2016-01-18 18:31:07.883124		benefit-reduced-from-original-amount-at-age-60-by	\N	t
395	TemplateField	45	Guaranteed Issue $ Limit	2	2016-01-18 18:31:07.890158	2016-01-18 18:31:07.890158		guaranteed-issue-limit	t	t
396	TemplateField	45	AD&D Maximum Benefit	1	2016-01-18 18:31:07.897317	2016-01-18 18:31:07.897317		ad-d-maximum-benefit	t	t
397	TemplateField	45	Benefit Reduced from Original Amount at age 70 by	6	2016-01-18 18:31:07.904497	2016-01-18 18:31:07.904497		benefit-reduced-from-original-amount-at-age-70-by	\N	t
398	TemplateField	45	Benefit Reduced from Original Amount at age 75 by	7	2016-01-18 18:31:07.911812	2016-01-18 18:31:07.911812		benefit-reduced-from-original-amount-at-age-75-by	\N	t
399	TemplateField	45	Benefit Reduced from Original Amount at age 80 by	8	2016-01-18 18:31:07.919504	2016-01-18 18:31:07.919504		benefit-reduced-from-original-amount-at-age-80-by	\N	t
400	TemplateField	45	Benefit Reduced from Original Amount at age 85 by	9	2016-01-18 18:31:07.927078	2016-01-18 18:31:07.927078		benefit-reduced-from-original-amount-at-age-85-by	\N	t
401	TemplateField	45	Benefit Reduced from Original Amount at age 90 by	10	2016-01-18 18:31:07.934274	2016-01-18 18:31:07.934274		benefit-reduced-from-original-amount-at-age-90-by	\N	t
402	TemplateField	45	Guaranteed Issue Multiple of Earnings Limit	3	2016-01-18 18:31:07.941575	2016-01-18 18:31:07.941575		guaranteed-issue-multiple-of-earnings-limit	\N	t
403	TemplateField	45	Benefit Reduced from Original Amount at age 95 by	11	2016-01-18 18:31:07.948708	2016-01-18 18:31:07.948708		benefit-reduced-from-original-amount-at-age-95-by	\N	t
404	TemplateField	45	Benefit Reduced from Original Amount at age 100 by	12	2016-01-18 18:31:07.956279	2016-01-18 18:31:07.956279		benefit-reduced-from-original-amount-at-age-100-by	\N	t
405	TemplateColumn	45	Entries	1	2016-01-18 18:31:07.965967	2016-01-18 18:31:07.965967		entries	\N	\N
406	TemplateField	46	Definition of Earnings	1	2016-01-18 18:31:08.114657	2016-01-18 18:31:08.114657		definition-of-earnings	t	t
407	TemplateColumn	46	Earnings Determination	1	2016-01-18 18:31:08.124167	2016-01-18 18:31:08.124167		earnings-determination	\N	\N
408	TemplateColumn	46	Plus Commission	2	2016-01-18 18:31:08.131671	2016-01-18 18:31:08.131671		plus-commission	\N	\N
409	TemplateColumn	46	Plus Bonus	3	2016-01-18 18:31:08.139176	2016-01-18 18:31:08.139176		plus-bonus	\N	\N
410	TemplateColumn	46	Plus Overtime	4	2016-01-18 18:31:08.146255	2016-01-18 18:31:08.146255		plus-overtime	\N	\N
411	TemplateColumn	46	Value of	5	2016-01-18 18:31:08.153366	2016-01-18 18:31:08.153366		value-of	\N	\N
412	TemplateColumn	46	Dollars	6	2016-01-18 18:31:08.160834	2016-01-18 18:31:08.160834		dollars	\N	\N
413	TemplateField	47	Employee Eligibility Class #1	1	2016-01-18 18:31:08.229986	2016-01-18 18:31:08.229986		employee-eligibility-class-1	\N	\N
414	TemplateField	47	Employee Eligibility Class #2	2	2016-01-18 18:31:08.237189	2016-01-18 18:31:08.237189		employee-eligibility-class-2	\N	\N
415	TemplateField	47	Employee Eligibility Class #3	3	2016-01-18 18:31:08.244468	2016-01-18 18:31:08.244468		employee-eligibility-class-3	\N	\N
416	TemplateField	47	Employee Eligibility Class #4	4	2016-01-18 18:31:08.251614	2016-01-18 18:31:08.251614		employee-eligibility-class-4	\N	\N
417	TemplateColumn	47	Employee Class	1	2016-01-18 18:31:08.260389	2016-01-18 18:31:08.260389		employee-class	\N	\N
418	TemplateColumn	47	Employee Classes	2	2016-01-18 18:31:08.267769	2016-01-18 18:31:08.267769		employee-classes	\N	\N
419	TemplateColumn	47	Waiting Period	3	2016-01-18 18:31:08.290817	2016-01-18 18:31:08.290817		waiting-period	\N	\N
420	TemplateColumn	47	Hours Requirement	4	2016-01-18 18:31:08.298103	2016-01-18 18:31:08.298103		hours-requirement	\N	\N
421	TemplateField	48	Accelerated Death Benefit	1	2016-01-18 18:35:33.210685	2016-01-18 18:35:33.210685		accelerated-death-benefit	\N	\N
422	TemplateColumn	48	Benefit Paid	1	2016-01-18 18:35:33.220811	2016-01-18 18:35:33.220811		benefit-paid	\N	\N
423	TemplateColumn	48	Maximum	2	2016-01-18 18:35:33.231388	2016-01-18 18:35:33.231388		maximum	\N	\N
424	TemplateColumn	48	Flat Amount	3	2016-01-18 18:35:33.240385	2016-01-18 18:35:33.240385		flat-amount	\N	\N
425	TemplateField	49	Definition of Earnings	1	2016-01-18 18:35:33.289368	2016-01-18 18:35:33.289368		definition-of-earnings	t	t
426	TemplateColumn	49	Earnings Determination	1	2016-01-18 18:35:33.300814	2016-01-18 18:35:33.300814		earnings-determination	\N	\N
427	TemplateColumn	49	Plus Commission	2	2016-01-18 18:35:33.308446	2016-01-18 18:35:33.308446		plus-commission	\N	\N
428	TemplateColumn	49	Plus Bonus	3	2016-01-18 18:35:33.316283	2016-01-18 18:35:33.316283		plus-bonus	\N	\N
429	TemplateColumn	49	Plus Overtime	4	2016-01-18 18:35:33.323799	2016-01-18 18:35:33.323799		plus-overtime	\N	\N
430	TemplateColumn	49	Value of	5	2016-01-18 18:35:33.331429	2016-01-18 18:35:33.331429		value-of	\N	\N
431	TemplateColumn	49	Dollars	6	2016-01-18 18:35:33.338731	2016-01-18 18:35:33.338731		dollars	\N	\N
432	TemplateField	50	Employee Eligibility Class #1	1	2016-01-18 18:35:33.412832	2016-01-18 18:35:33.412832		employee-eligibility-class-1	\N	\N
433	TemplateField	50	Employee Eligibility Class #2	2	2016-01-18 18:35:33.42067	2016-01-18 18:35:33.42067		employee-eligibility-class-2	\N	\N
434	TemplateField	50	Employee Eligibility Class #3	3	2016-01-18 18:35:33.427758	2016-01-18 18:35:33.427758		employee-eligibility-class-3	\N	\N
435	TemplateField	50	Employee Eligibility Class #4	4	2016-01-18 18:35:33.434934	2016-01-18 18:35:33.434934		employee-eligibility-class-4	\N	\N
436	TemplateColumn	50	Employee Class	1	2016-01-18 18:35:33.443856	2016-01-18 18:35:33.443856		employee-class	\N	\N
437	TemplateColumn	50	Employee Classes	2	2016-01-18 18:35:33.450974	2016-01-18 18:35:33.450974		employee-classes	\N	\N
438	TemplateColumn	50	Waiting Period	3	2016-01-18 18:35:33.459078	2016-01-18 18:35:33.459078		waiting-period	\N	\N
439	TemplateColumn	50	Hours Requirement	4	2016-01-18 18:35:33.466026	2016-01-18 18:35:33.466026		hours-requirement	\N	\N
440	TemplateField	51	Life Amount	1	2016-01-18 18:35:33.6471	2016-01-18 18:35:33.6471		life-amount	t	t
441	TemplateColumn	51	X Multiple of Earnings	1	2016-01-18 18:35:33.656415	2016-01-18 18:35:33.656415		x-multiple-of-earnings	\N	\N
442	TemplateColumn	51	Flat Amount	2	2016-01-18 18:35:33.663845	2016-01-18 18:35:33.663845		flat-amount	\N	\N
443	TemplateField	52	Guaranteed Issue Multiple of Earnings Limit	3	2016-01-18 18:35:33.700115	2016-01-18 18:35:33.700115		guaranteed-issue-multiple-of-earnings-limit	\N	t
444	TemplateField	52	Life Maximum Benefit	1	2016-01-18 18:35:33.708255	2016-01-18 18:35:33.708255		life-maximum-benefit	t	t
445	TemplateField	52	Guaranteed Issue $ Limit	2	2016-01-18 18:35:33.716165	2016-01-18 18:35:33.716165		guaranteed-issue-limit	t	t
446	TemplateField	52	Benefit Reduced from Original Amount at age 60 by	4	2016-01-18 18:35:33.723495	2016-01-18 18:35:33.723495		benefit-reduced-from-original-amount-at-age-60-by	\N	t
447	TemplateField	52	Benefit Reduced from Original Amount at age 65 by	5	2016-01-18 18:35:33.73086	2016-01-18 18:35:33.73086		benefit-reduced-from-original-amount-at-age-65-by	\N	t
448	TemplateField	52	Benefit Reduced from Original Amount at age 70 by	6	2016-01-18 18:35:33.73823	2016-01-18 18:35:33.73823		benefit-reduced-from-original-amount-at-age-70-by	\N	t
449	TemplateField	52	Benefit Reduced from Original Amount at age 80 by	8	2016-01-18 18:35:33.745582	2016-01-18 18:35:33.745582		benefit-reduced-from-original-amount-at-age-80-by	\N	t
450	TemplateField	52	Benefit Reduced from Original Amount at age 85 by	9	2016-01-18 18:35:33.752919	2016-01-18 18:35:33.752919		benefit-reduced-from-original-amount-at-age-85-by	\N	t
451	TemplateField	52	Benefit Reduced from Original Amount at age 90 by	10	2016-01-18 18:35:33.760974	2016-01-18 18:35:33.760974		benefit-reduced-from-original-amount-at-age-90-by	\N	t
452	TemplateField	52	Benefit Reduced from Original Amount at age 95 by	11	2016-01-18 18:35:33.768788	2016-01-18 18:35:33.768788		benefit-reduced-from-original-amount-at-age-95-by	\N	t
453	TemplateField	52	Benefit Reduced from Original Amount at age 100 by	12	2016-01-18 18:35:33.77856	2016-01-18 18:35:33.77856		benefit-reduced-from-original-amount-at-age-100-by	\N	t
454	TemplateField	52	Coverage disabled before age 60	13	2016-01-18 18:35:33.786202	2016-01-18 18:35:33.786202		coverage-disabled-before-age-60	t	t
455	TemplateField	52	Coverage disabled after age 60	14	2016-01-18 18:35:33.794075	2016-01-18 18:35:33.794075		coverage-disabled-after-age-60	\N	\N
456	TemplateField	52	Family Medical Leave Continuation	15	2016-01-18 18:35:33.802681	2016-01-18 18:35:33.802681		family-medical-leave-continuation	\N	\N
457	TemplateField	52	Military Leave Continuation	16	2016-01-18 18:35:33.810024	2016-01-18 18:35:33.810024		military-leave-continuation	\N	\N
458	TemplateField	52	Military Convalescence Support Continuation	17	2016-01-18 18:35:33.817286	2016-01-18 18:35:33.817286		military-convalescence-support-continuation	\N	\N
459	TemplateField	52	Benefit Reduced from Original Amount at age 75 by	7	2016-01-18 18:35:33.824556	2016-01-18 18:35:33.824556		benefit-reduced-from-original-amount-at-age-75-by	\N	t
460	TemplateColumn	52	Entries	1	2016-01-18 18:35:33.833563	2016-01-18 18:35:33.833563		entries	\N	\N
461	TemplateField	53	Portability	2	2016-01-18 18:35:34.036658	2016-01-18 18:35:34.036658		portability	\N	\N
462	TemplateField	53	International Employees Covered	3	2016-01-18 18:35:34.044165	2016-01-18 18:35:34.044165		international-employees-covered	\N	\N
463	TemplateField	53	Suicide Exclusions	4	2016-01-18 18:35:34.052108	2016-01-18 18:35:34.052108		suicide-exclusions	\N	\N
464	TemplateField	53	Military Service Exclusions	5	2016-01-18 18:35:34.059916	2016-01-18 18:35:34.059916		military-service-exclusions	\N	\N
465	TemplateField	53	Travel Assistance	6	2016-01-18 18:35:34.06757	2016-01-18 18:35:34.06757		travel-assistance	\N	\N
466	TemplateField	53	Bereavement Counseling	7	2016-01-18 18:35:34.075642	2016-01-18 18:35:34.075642		bereavement-counseling	\N	\N
467	TemplateField	53	EAP	8	2016-01-18 18:35:34.083733	2016-01-18 18:35:34.083733		eap	\N	\N
468	TemplateField	53	Act of  War Exclusion	9	2016-01-18 18:35:34.091086	2016-01-18 18:35:34.091086		act-of-war-exclusion	\N	\N
469	TemplateField	53	Conversion	1	2016-01-18 18:35:34.098546	2016-01-18 18:35:34.098546		conversion	\N	t
470	TemplateColumn	53	Entries	1	2016-01-18 18:35:34.107643	2016-01-18 18:35:34.107643		entries	\N	\N
472	TemplateColumn	54	ENTRIES	1	2016-01-19 06:17:12.900284	2016-01-19 06:17:12.900284		entries	\N	\N
477	TemplateField	54	Deductible - Family Multiple	6	2016-01-19 06:20:29.811955	2016-01-19 06:20:29.811955		deductible-family-multiple	\N	\N
482	TemplateField	54	Out-of-Pocket Maximum - Family Multiple	11	2016-01-19 06:28:29.906253	2016-01-19 06:28:29.906253		out-of-pocket-maximum-family-multiple	\N	\N
486	TemplateField	54	Annual Maximum	15	2016-01-19 06:33:36.597562	2016-01-19 06:33:36.597562		annual-maximum	\N	\N
487	TemplateField	54	Gym Reimbursement	16	2016-01-19 06:34:08.983701	2016-01-19 06:34:08.983701		gym-reimbursement	\N	\N
488	TemplateField	54	Customer Service Days/Hours	17	2016-01-19 06:34:40.785405	2016-01-19 06:34:40.785405		customer-service-days-hours	\N	\N
489	TemplateField	54	Nurseline	18	2016-01-19 06:35:07.848223	2016-01-19 06:35:07.848223		nurseline	\N	\N
490	TemplateField	54	International Employees Covered	19	2016-01-19 06:35:42.372294	2016-01-19 06:35:42.372294		international-employees-covered	\N	\N
498	TemplateField	55	General Radiology	4	2016-01-19 06:45:48.282087	2016-01-19 06:45:48.282087		general-radiology	\N	\N
471	TemplateField	54	Plan Type	1	2016-01-19 06:16:35.618265	2016-01-19 06:38:48.322961		plan-type	t	t
499	TemplateField	55	CT/MRI/PET Scans	5	2016-01-19 06:46:01.072234	2016-01-19 06:46:01.072234		ct-mri-pet-scans	\N	\N
473	TemplateField	54	Network	2	2016-01-19 06:18:15.778399	2016-01-19 06:38:50.146885		network	t	t
500	TemplateField	55	Annual Physical Exam	6	2016-01-19 06:46:15.668838	2016-01-19 06:46:15.668838		annual-physical-exam	\N	\N
474	TemplateField	54	Referrals Needed	3	2016-01-19 06:18:32.297988	2016-01-19 06:42:32.106627		referrals-needed	t	t
501	TemplateField	55	Well Child Exams	7	2016-01-19 06:46:33.863909	2016-01-19 06:46:33.863909		well-child-exams	\N	\N
475	TemplateField	54	Deductible Individual	4	2016-01-19 06:19:03.767137	2016-01-19 06:42:35.315405		deductible-individual	t	t
502	TemplateField	55	Pediatric Dental	8	2016-01-19 06:46:46.946033	2016-01-19 06:46:46.946033		pediatric-dental	\N	\N
476	TemplateField	54	Deductible Family	5	2016-01-19 06:19:49.168221	2016-01-19 06:42:37.912924		deductible-family	t	t
478	TemplateField	54	Deductible Format	7	2016-01-19 06:21:11.769037	2016-01-19 06:42:42.401031		deductible-format	\N	t
479	TemplateField	54	Coinsurance	8	2016-01-19 06:26:10.832832	2016-01-19 06:42:50.957231		coinsurance	t	t
480	TemplateField	54	Out-of-Pocket Max Individual (includes deductible)	9	2016-01-19 06:26:41.059778	2016-01-19 06:42:55.558263		out-of-pocket-max-individual-includes-deductible	t	t
505	TemplateField	55	Surgery/Anesthesiology	11	2016-01-19 06:47:29.445704	2016-01-19 06:47:29.445704		surgery-anesthesiology	\N	\N
481	TemplateField	54	Out-of-Pocket Max Family (includes deductible)	10	2016-01-19 06:27:25.88746	2016-01-19 06:43:03.486528		out-of-pocket-max-family-includes-deductible	t	t
483	TemplateField	54	In & Out-of-Network Deductibles Cross Accumulation	12	2016-01-19 06:29:07.117746	2016-01-19 06:43:09.145956		in-out-of-network-deductibles-cross-accumulation	\N	t
484	TemplateField	54	In & Out-of-Network OOP Maximum Cross Accumulation	13	2016-01-19 06:29:45.375569	2016-01-19 06:43:15.419789		in-out-of-network-oop-maximum-cross-accumulation	\N	t
485	TemplateField	54	OON Reimbursement Level	14	2016-01-19 06:32:25.626698	2016-01-19 06:43:18.655231		oon-reimbursement-level	t	t
492	TemplateColumn	55	Subject to Deductible & Coinsurance	1	2016-01-19 06:44:29.177561	2016-01-19 06:44:29.177561		subject-to-deductible-coinsurance	\N	\N
493	TemplateColumn	55	LIMIT	2	2016-01-19 06:44:33.843106	2016-01-19 06:44:33.843106		limit	\N	\N
494	TemplateColumn	55	SEPERATE DEDUCTIBLE	3	2016-01-19 06:44:40.879127	2016-01-19 06:44:40.879127		seperate-deductible	\N	\N
495	TemplateColumn	55	SEPERATE COINSURANCE	4	2016-01-19 06:44:53.849283	2016-01-19 06:44:53.849283		seperate-coinsurance	\N	\N
512	TemplateField	55	Physical Therapy - Inpatient Coverage	18	2016-01-19 06:53:28.802769	2016-01-19 06:53:28.802769		physical-therapy-inpatient-coverage	\N	\N
514	TemplateField	55	Occupational Therapy - Inpatient Coverage	20	2016-01-19 06:54:00.51908	2016-01-19 06:54:00.51908		occupational-therapy-inpatient-coverage	\N	\N
515	TemplateField	55	Occupational Therapy - Outpatient Coverage	21	2016-01-19 06:54:18.902998	2016-01-19 06:54:18.902998		occupational-therapy-outpatient-coverage	\N	\N
516	TemplateField	55	Speech Therapy - Inpatient Coverage	22	2016-01-19 06:54:33.323433	2016-01-19 06:54:33.323433		speech-therapy-inpatient-coverage	\N	\N
517	TemplateField	55	Speech Therapy - Outpatient Coverage	23	2016-01-19 06:54:51.024976	2016-01-19 06:54:51.024976		speech-therapy-outpatient-coverage	\N	\N
518	TemplateField	55	Pregnancy & Maternity Care - Office Visits	24	2016-01-19 06:55:06.084788	2016-01-19 06:55:06.084788		pregnancy-maternity-care-office-visits	\N	\N
519	TemplateField	55	Pregnancy & Maternity Care - Labor & Delivery	25	2016-01-19 06:55:19.150076	2016-01-19 06:55:19.150076		pregnancy-maternity-care-labor-delivery	\N	\N
521	TemplateField	55	Ambulance	27	2016-01-19 06:55:47.175545	2016-01-19 06:55:47.175545		ambulance	\N	\N
522	TemplateField	55	Hospice	28	2016-01-19 06:55:56.931265	2016-01-19 06:55:56.931265		hospice	\N	\N
497	TemplateField	55	Lab	3	2016-01-19 06:45:34.187465	2016-01-19 13:38:51.590322		lab	\N	t
496	TemplateField	55	Specialist Office Visit	2	2016-01-19 06:45:23.692968	2016-01-19 13:38:50.143456		specialist-office-visit	t	t
504	TemplateField	55	Hospitalization - Outpatient	10	2016-01-19 06:47:15.058325	2016-01-19 13:39:04.765162		hospitalization-outpatient	\N	t
503	TemplateField	55	Hospitalization - Inpatient	9	2016-01-19 06:46:59.611532	2016-01-19 13:39:03.945547		hospitalization-inpatient	t	t
507	TemplateField	55	Urgent Care	13	2016-01-19 06:47:50.24785	2016-01-19 13:39:30.338507		urgent-care	\N	t
506	TemplateField	55	Emergency Room	12	2016-01-19 06:47:40.961146	2016-01-19 13:39:07.708623		emergency-room	t	t
509	TemplateField	55	Mental Nervous - Outpatient Coverage	15	2016-01-19 06:52:44.635678	2016-01-19 13:39:34.405608		mental-nervous-outpatient-coverage	\N	t
510	TemplateField	55	Substance Abuse - Inpatient Coverage	16	2016-01-19 06:52:59.419858	2016-01-19 13:39:44.149034		substance-abuse-inpatient-coverage	\N	t
511	TemplateField	55	Substance Abuse - Outpatient Coverage	17	2016-01-19 06:53:14.393493	2016-01-19 13:39:45.526926		substance-abuse-outpatient-coverage	\N	t
513	TemplateField	55	Physical Therapy - Outpatient Coverage	19	2016-01-19 06:53:42.331534	2016-01-19 13:39:53.882938		physical-therapy-outpatient-coverage	\N	t
520	TemplateField	55	Chiropractic Services	26	2016-01-19 06:55:32.974946	2016-01-19 13:40:01.734005		chiropractic-services	\N	t
524	TemplateField	56	Plan Type	1	2016-01-19 07:12:26.844556	2016-01-19 07:12:26.844556		plan-type	\N	\N
525	TemplateColumn	56	Entries	1	2016-01-19 07:12:47.823954	2016-01-19 07:12:47.823954		entries	\N	\N
526	TemplateField	56	Materials Coverage	2	2016-01-19 07:13:13.989389	2016-01-19 07:13:13.989389		materials-coverage	\N	\N
527	TemplateField	56	International Employees Covered	3	2016-01-19 07:13:32.199706	2016-01-19 07:13:32.199706		international-employees-covered	\N	\N
523	TemplateField	55	Home Healthcare	29	2016-01-19 06:56:02.979489	2016-01-19 08:01:11.952763		home-healthcare	\N	\N
528	TemplateColumn	57	Entries	1	2016-01-19 08:04:25.037542	2016-01-19 08:04:25.037542		entries	\N	\N
531	TemplateField	55	Skilled Nursing	30	2016-01-19 08:04:52.533345	2016-01-19 08:04:52.533345		skilled-nursing	\N	\N
532	TemplateField	57	International Employees Covered	3	2016-01-19 08:04:54.71314	2016-01-19 08:04:54.71314		international-employees-covered	\N	\N
534	TemplateField	55	Prosthetics	32	2016-01-19 08:05:08.499808	2016-01-19 08:05:08.499808		prosthetics	\N	\N
536	TemplateField	55	Hearing Devices	34	2016-01-19 08:05:36.53422	2016-01-19 08:05:36.53422		hearing-devices	\N	\N
537	TemplateField	55	Vision Exams	35	2016-01-19 08:05:42.168393	2016-01-19 08:05:42.168393		vision-exams	\N	\N
538	TemplateColumn	58	Limit	1	2016-01-19 08:06:03.901957	2016-01-19 08:06:03.901957		limit	\N	\N
539	TemplateColumn	58	Coinsurance	2	2016-01-19 08:06:51.782564	2016-01-19 08:06:51.782564		coinsurance	\N	\N
540	TemplateField	55	Short Term Rehabilitation - Inpatient	36	2016-01-19 08:06:59.659913	2016-01-19 08:06:59.659913		short-term-rehabilitation-inpatient	\N	\N
542	TemplateField	55	Short Term Rehabilitation - Outpatient	37	2016-01-19 08:07:13.951847	2016-01-19 08:07:13.951847		short-term-rehabilitation-outpatient	\N	\N
543	TemplateColumn	58	copay	3	2016-01-19 08:07:25.136544	2016-01-19 08:07:25.136544		copay	\N	\N
544	TemplateField	55	Telemedicine	38	2016-01-19 08:07:31.57357	2016-01-19 08:07:31.57357		telemedicine	\N	\N
545	TemplateField	55	Specialty Rx Coverage	39	2016-01-19 08:07:45.61408	2016-01-19 08:07:45.61408		specialty-rx-coverage	\N	\N
548	TemplateField	58	Lenses - Lined Bifocal	4	2016-01-19 08:08:16.592875	2016-01-19 08:08:16.592875		lenses-lined-bifocal	\N	\N
549	TemplateField	58	Lenses - Lined Trifocal	5	2016-01-19 08:08:34.877595	2016-01-19 08:08:34.877595		lenses-lined-trifocal	\N	\N
550	TemplateField	58	Lenses - Lenticular	6	2016-01-19 08:08:45.622647	2016-01-19 08:08:45.622647		lenses-lenticular	\N	\N
552	TemplateColumn	59	Discount (Overage or General)	1	2016-01-19 08:13:08.707117	2016-01-19 08:13:08.707117		discount-overage-or-general	\N	\N
553	TemplateColumn	59	Allowance	2	2016-01-19 08:13:18.467547	2016-01-19 08:13:18.467547		allowance	\N	\N
556	TemplateField	59	Contacts - Non Selection	3	2016-01-19 08:13:54.24265	2016-01-19 08:13:54.24265		contacts-non-selection	\N	\N
557	TemplateField	59	Contacts - Elective Disposable	4	2016-01-19 08:14:08.127052	2016-01-19 08:14:08.127052		contacts-elective-disposable	\N	\N
558	TemplateField	59	Contacts -Necessary Lenses	5	2016-01-19 08:14:18.477827	2016-01-19 08:14:18.477827		contacts-necessary-lenses	\N	\N
559	TemplateField	59	Premium Progressive Lenses	6	2016-01-19 08:14:28.461874	2016-01-19 08:14:28.461874		premium-progressive-lenses	\N	\N
560	TemplateColumn	60	Employee Class	1	2016-01-19 08:15:02.884005	2016-01-19 08:15:02.884005		employee-class	\N	\N
561	TemplateColumn	60	Employee Classes	2	2016-01-19 08:15:06.748449	2016-01-19 08:15:06.748449		employee-classes	\N	\N
562	TemplateColumn	60	Waiting Period	3	2016-01-19 08:15:10.197457	2016-01-19 08:15:10.197457		waiting-period	\N	\N
563	TemplateColumn	60	Hours Requirement	4	2016-01-19 08:15:31.146659	2016-01-19 08:15:31.146659		hours-requirement	\N	\N
564	TemplateColumn	60	Spouse Eligible	5	2016-01-19 08:15:40.937927	2016-01-19 08:15:40.937927		spouse-eligible	\N	\N
565	TemplateColumn	60	Dependent Children Eligible	6	2016-01-19 08:19:54.02812	2016-01-19 08:19:54.02812		dependent-children-eligible	\N	\N
566	TemplateColumn	60	Domestic Partner Eligible	7	2016-01-19 08:20:16.767919	2016-01-19 08:20:16.767919		domestic-partner-eligible	\N	\N
567	TemplateColumn	60	Dependent Child Limiting Age	8	2016-01-19 08:20:50.378949	2016-01-19 08:20:50.378949		dependent-child-limiting-age	\N	\N
568	TemplateColumn	60	Dependent Child Coverage Ends	9	2016-01-19 08:21:05.31354	2016-01-19 08:21:05.31354		dependent-child-coverage-ends	\N	\N
569	TemplateField	60	Employee Eligibility Class #1	1	2016-01-19 08:21:31.414515	2016-01-19 08:21:31.414515		employee-eligibility-class-1	\N	\N
570	TemplateField	60	Employee Eligibility Class #2	2	2016-01-19 08:21:35.83444	2016-01-19 08:21:35.83444		employee-eligibility-class-2	\N	\N
571	TemplateField	60	Employee Eligibility Class #3	3	2016-01-19 08:21:40.467562	2016-01-19 08:21:40.467562		employee-eligibility-class-3	\N	\N
572	TemplateField	60	Employee Eligibility Class #4	4	2016-01-19 08:21:44.908088	2016-01-19 08:21:44.908088		employee-eligibility-class-4	\N	\N
573	TemplateColumn	61	Entries	1	2016-01-19 08:32:20.858382	2016-01-19 08:32:20.858382		entries	\N	\N
576	TemplateField	61	International Employees Covered	3	2016-01-19 08:32:49.940039	2016-01-19 08:32:49.940039		international-employees-covered	\N	\N
577	TemplateColumn	62	Limit	1	2016-01-19 08:33:51.298473	2016-01-19 08:33:51.298473		limit	\N	\N
578	TemplateColumn	62	Coinsurance	2	2016-01-19 08:33:59.844451	2016-01-19 08:33:59.844451		coinsurance	\N	\N
582	TemplateField	62	Lenses - Lined Bifocal	4	2016-01-19 08:40:11.90966	2016-01-19 08:40:11.90966		lenses-lined-bifocal	\N	\N
583	TemplateField	62	Lenses - Lined Trifocal	5	2016-01-19 08:40:45.95287	2016-01-19 08:40:45.95287		lenses-lined-trifocal	\N	\N
584	TemplateField	62	Lenses - Lenticular	6	2016-01-19 08:41:10.078669	2016-01-19 08:41:10.078669		lenses-lenticular	\N	\N
533	TemplateField	55	Infertility Coverage	31	2016-01-19 08:05:01.145025	2016-01-19 13:40:08.804876		infertility-coverage	\N	t
530	TemplateField	57	Materials Coverage	2	2016-01-19 08:04:44.207853	2016-01-19 08:49:14.128173		materials-coverage	t	t
541	TemplateField	58	Exams	1	2016-01-19 08:07:05.942718	2016-01-19 08:49:24.15501		exams	t	t
546	TemplateField	58	Frames	2	2016-01-19 08:07:55.461682	2016-01-19 08:49:22.137172		frames	t	t
547	TemplateField	58	Lenses - Single Vision	3	2016-01-19 08:08:04.14126	2016-01-19 08:49:29.327108		lenses-single-vision	t	t
551	TemplateField	58	Contacts	7	2016-01-19 08:08:56.281407	2016-01-19 08:49:41.886054		contacts	t	t
554	TemplateField	59	Retail Frames	1	2016-01-19 08:13:30.212196	2016-01-19 08:49:51.697133		retail-frames	t	t
575	TemplateField	61	Materials Coverage	2	2016-01-19 08:32:37.279583	2016-01-19 08:50:22.712529		materials-coverage	t	t
574	TemplateField	61	Plan Type	1	2016-01-19 08:32:28.102934	2016-01-19 08:50:59.331515		plan-type	t	t
579	TemplateField	62	Exams	1	2016-01-19 08:36:47.02001	2016-01-19 08:50:35.8657		exams	t	t
580	TemplateField	62	Frames	2	2016-01-19 08:39:30.433351	2016-01-19 08:50:35.164025		frames	t	t
581	TemplateField	62	Lenses - Single Vision	3	2016-01-19 08:39:39.220013	2016-01-19 08:50:34.602587		lenses-single-vision	t	t
535	TemplateField	55	Durable Medical Equipment	33	2016-01-19 08:05:25.392629	2016-01-19 13:40:11.914835		durable-medical-equipment	\N	t
586	TemplateColumn	63	Allowance	1	2016-01-19 08:41:51.380798	2016-01-19 08:41:51.380798		allowance	\N	\N
589	TemplateField	63	Contacts - Non Selection	3	2016-01-19 08:44:24.78361	2016-01-19 08:44:24.78361		contacts-non-selection	\N	\N
590	TemplateField	63	Contacts - Elective Disposable	4	2016-01-19 08:44:37.20108	2016-01-19 08:44:37.20108		contacts-elective-disposable	\N	\N
591	TemplateField	63	Contacts -Necessary Lenses	5	2016-01-19 08:44:55.667299	2016-01-19 08:44:55.667299		contacts-necessary-lenses	\N	\N
592	TemplateField	63	Premium Progressive Lenses	6	2016-01-19 08:45:05.285325	2016-01-19 08:45:05.285325		premium-progressive-lenses	\N	\N
529	TemplateField	57	Plan Type	1	2016-01-19 08:04:34.451839	2016-01-19 08:49:12.801867		plan-type	t	t
555	TemplateField	59	Contacts - Covered Selection	2	2016-01-19 08:13:42.195556	2016-01-19 08:49:50.806381		contacts-covered-selection	t	t
585	TemplateField	62	Contacts	7	2016-01-19 08:41:20.141509	2016-01-19 08:50:42.597001		contacts	t	t
636	TemplateColumn	68	DEDUCTIBLE	1	2016-01-19 10:54:51.099504	2016-01-19 10:54:51.099504		deductible	\N	\N
588	TemplateField	63	Contacts - Covered Selection	2	2016-01-19 08:44:12.931962	2016-01-19 08:50:50.007559		contacts-covered-selection	t	t
587	TemplateField	63	Retail Frames	1	2016-01-19 08:44:03.690203	2016-01-19 08:50:51.855703		retail-frames	t	t
593	TemplateColumn	64	Entries	1	2016-01-19 08:54:33.253391	2016-01-19 08:54:33.253391		entries	\N	\N
599	TemplateField	64	Deductible -Calendar or Policy Year	6	2016-01-19 08:58:00.335931	2016-01-19 08:58:00.335931		deductible-calendar-or-policy-year	\N	\N
604	TemplateField	64	International Employees Covered	11	2016-01-19 08:59:48.433064	2016-01-19 08:59:48.433064		international-employees-covered	\N	\N
609	TemplateColumn	65	Covered	1	2016-01-19 09:09:52.877818	2016-01-19 09:09:52.877818		covered	\N	\N
610	TemplateColumn	65	Not Covered	2	2016-01-19 09:10:29.931238	2016-01-19 09:10:29.931238		not-covered	\N	\N
611	TemplateColumn	65	Subject to Deductible	3	2016-01-19 09:11:17.747595	2016-01-19 09:11:17.747595		subject-to-deductible	\N	\N
612	TemplateColumn	65	Coinsurance	4	2016-01-19 09:12:02.522301	2016-01-19 09:12:02.522301		coinsurance	\N	\N
613	TemplateColumn	65	Copays Apply	5	2016-01-19 09:12:45.35926	2016-01-19 09:12:45.35926		copays-apply	\N	\N
614	TemplateColumn	66	Entries	1	2016-01-19 09:17:44.687502	2016-01-19 09:17:44.687502		entries	\N	\N
626	TemplateField	67	Subject to Medical Deductible & Coinsurance	2	2016-01-19 10:46:21.593517	2016-01-19 10:46:21.593517		subject-to-medical-deductible-coinsurance	\N	\N
628	TemplateField	67	Step Therapy/Precertification Applies	4	2016-01-19 10:47:38.522183	2016-01-19 10:47:38.522183		step-therapy-precertification-applies	\N	\N
630	TemplateField	67	Mandatory Generic	5	2016-01-19 10:48:31.925274	2016-01-19 10:48:31.925274		mandatory-generic	\N	\N
631	TemplateField	67	Oral Contraceptive	6	2016-01-19 10:48:41.987607	2016-01-19 10:48:41.987607		oral-contraceptive	\N	\N
632	TemplateField	67	Are Prescriptions Covered Out of Network	7	2016-01-19 10:49:09.764136	2016-01-19 10:49:09.764136		are-prescriptions-covered-out-of-network	\N	\N
633	TemplateColumn	67	ENTRIES	1	2016-01-19 10:50:47.98172	2016-01-19 10:50:47.98172		entries	\N	\N
637	TemplateColumn	68	PER MEMBER	2	2016-01-19 10:54:57.049257	2016-01-19 10:54:57.049257		per-member	\N	\N
639	TemplateColumn	69	SUBJECT TO RX DEDUCTIBLE	1	2016-01-19 11:15:00.230873	2016-01-19 11:15:00.230873		subject-to-rx-deductible	\N	\N
640	TemplateColumn	69	COINSURANCE	2	2016-01-19 12:29:47.006757	2016-01-19 12:29:47.006757		coinsurance	\N	\N
644	TemplateField	69	Retail Rx (Tier 4)	4	2016-01-19 12:31:24.79549	2016-01-19 12:31:24.79549		retail-rx-tier-4	\N	\N
645	TemplateField	69	Retail Rx (Tier 5)	5	2016-01-19 12:32:43.474751	2016-01-19 12:32:43.474751		retail-rx-tier-5	\N	\N
642	TemplateField	69	Retail Rx (Tier 2)	2	2016-01-19 12:31:12.807822	2016-01-19 13:00:05.7548		retail-rx-tier-2	t	t
625	TemplateField	67	Subject to Medical Deductible	1	2016-01-19 10:45:59.933424	2016-01-19 13:40:21.37954		subject-to-medical-deductible	\N	t
641	TemplateField	69	Retail Rx (Tier 1)	1	2016-01-19 12:31:06.803478	2016-01-19 13:00:04.948428		retail-rx-tier-1	t	t
643	TemplateField	69	Retail Rx (Tier 3)	3	2016-01-19 12:31:18.34554	2016-01-19 13:00:07.845302		retail-rx-tier-3	t	t
634	TemplateField	68	Rx Deductible - Family	1	2016-01-19 10:54:30.490168	2016-01-19 13:00:21.899525		rx-deductible-family	t	t
627	TemplateField	67	Network	3	2016-01-19 10:46:27.100252	2016-01-19 13:40:22.945289		network	\N	t
595	TemplateField	64	Network	2	2016-01-19 08:54:59.672224	2016-01-20 11:49:16.138136		network	\N	t
594	TemplateField	64	Plan Type	1	2016-01-19 08:54:46.567504	2016-01-20 11:49:09.947724		plan-type	t	t
597	TemplateField	64	Deductible Family	4	2016-01-19 08:57:04.409653	2016-01-20 11:49:22.970948		deductible-family	t	t
598	TemplateField	64	Deductible Waived for Preventive Care	5	2016-01-19 08:57:29.783351	2016-01-20 11:49:23.54338		deductible-waived-for-preventive-care	t	t
601	TemplateField	64	Annual Rollover Benefit	8	2016-01-19 08:58:30.431318	2016-01-20 11:49:35.532388		annual-rollover-benefit	\N	t
596	TemplateField	64	Deductible Individual	3	2016-01-19 08:55:16.117655	2016-01-20 11:49:22.402881		deductible-individual	t	t
600	TemplateField	64	Annual Plan Maximum (Per Enrollee)	7	2016-01-19 08:58:15.525661	2016-01-20 11:49:29.60778		annual-plan-maximum-per-enrollee	t	t
602	TemplateField	64	Annual Rollover Maximum	9	2016-01-19 08:58:49.24309	2016-01-20 11:49:36.712566		annual-rollover-maximum	\N	t
603	TemplateField	64	OON Reimbursement Level	10	2016-01-19 08:59:11.463873	2016-01-20 11:49:36.777404		oon-reimbursement-level	\N	t
605	TemplateField	64	Waiting Period for Services	12	2016-01-19 09:00:11.630131	2016-01-20 11:49:41.162543		waiting-period-for-services	\N	t
615	TemplateField	66	Cleanings	1	2016-01-19 09:17:46.937592	2016-01-20 11:50:01.97739		cleanings	\N	t
606	TemplateField	65	Diagnostic and Preventive (Type A)	1	2016-01-19 09:09:08.080749	2016-01-20 11:49:56.038324		diagnostic-and-preventive-type-a	t	t
607	TemplateField	65	Basic Services (Type B)	2	2016-01-19 09:09:18.355756	2016-01-20 11:49:54.928635		basic-services-type-b	t	t
608	TemplateField	65	Major Services (Type C)	3	2016-01-19 09:09:31.117069	2016-01-20 11:49:54.291495		major-services-type-c	t	t
616	TemplateField	66	Sealants	2	2016-01-19 09:17:56.116948	2016-01-20 11:50:02.574692		sealants	\N	t
617	TemplateField	66	Fillings (Amalgam & Composite)	3	2016-01-19 09:18:07.148207	2016-01-20 11:50:03.207946		fillings-amalgam-composite	\N	t
618	TemplateField	66	Oral Surgery	4	2016-01-19 09:18:19.026918	2016-01-20 11:50:04.598106		oral-surgery	\N	t
619	TemplateField	66	Periodontics	5	2016-01-19 09:18:33.393886	2016-01-20 11:50:05.257771		periodontics	\N	t
620	TemplateField	66	Endodontics	6	2016-01-19 09:18:47.952656	2016-01-20 11:50:05.902472		endodontics	\N	t
621	TemplateField	66	Implants	7	2016-01-19 09:18:59.501598	2016-01-20 11:50:07.1216		implants	\N	t
622	TemplateField	66	Prosthodontics (including Dentures)	8	2016-01-19 09:19:10.137645	2016-01-20 11:50:07.592686		prosthodontics-including-dentures	\N	t
623	TemplateField	66	Inlays/Onlays/Crowns	9	2016-01-19 09:19:21.145695	2016-01-20 11:50:08.166962		inlays-onlays-crowns	\N	t
624	TemplateField	66	Simple Extractions	10	2016-01-19 09:19:30.185794	2016-01-20 11:50:08.788303		simple-extractions	\N	t
649	TemplateField	69	Mail Order Rx (Tier 4)	9	2016-01-19 12:33:58.951727	2016-01-19 12:33:58.951727		mail-order-rx-tier-4	\N	\N
650	TemplateField	69	Mail Order Rx (Tier 5)	10	2016-01-19 12:34:06.521983	2016-01-19 12:34:06.521983		mail-order-rx-tier-5	\N	\N
651	TemplateField	69	Speciality Medications - Mail Order	11	2016-01-19 12:34:29.775263	2016-01-19 12:34:29.775263		speciality-medications-mail-order	\N	\N
652	TemplateField	69	Speciality Medications - Retail	12	2016-01-19 12:35:41.568668	2016-01-19 12:35:41.568668		speciality-medications-retail	\N	\N
653	TemplateField	70	Disease Management	1	2016-01-19 12:53:11.626536	2016-01-19 12:53:11.626536		disease-management	\N	\N
654	TemplateColumn	70	IDENTIFICATION METHODS	1	2016-01-19 12:53:24.525852	2016-01-19 12:53:24.525852		identification-methods	\N	\N
655	TemplateColumn	70	NUMBER OF CONDITIONS TRACKED	2	2016-01-19 12:53:32.270062	2016-01-19 12:53:32.270062		number-of-conditions-tracked	\N	\N
656	TemplateColumn	70	OUTREACH METHODS	3	2016-01-19 12:53:47.40193	2016-01-19 12:53:47.40193		outreach-methods	\N	\N
657	TemplateColumn	70	REPORT FREQUENCY	4	2016-01-19 12:54:24.266935	2016-01-19 12:54:24.266935		report-frequency	\N	\N
658	TemplateField	71	Integrated Wellness Plan	1	2016-01-19 12:58:47.931141	2016-01-19 12:58:47.931141		integrated-wellness-plan	\N	\N
659	TemplateColumn	71	ENTRIES	1	2016-01-19 12:58:55.815588	2016-01-19 12:58:55.815588		entries	\N	\N
646	TemplateField	69	Mail Order Rx (Tier 1)	6	2016-01-19 12:33:33.136804	2016-01-19 12:59:29.37957		mail-order-rx-tier-1	\N	t
648	TemplateField	69	Mail Order Rx (Tier 3)	8	2016-01-19 12:33:53.70642	2016-01-19 12:59:33.849454		mail-order-rx-tier-3	\N	t
647	TemplateField	69	Mail Order Rx (Tier 2 )	7	2016-01-19 12:33:39.899609	2016-01-19 13:00:10.614583		mail-order-rx-tier-2	\N	t
635	TemplateField	68	Rx Deductible - Individual	2	2016-01-19 10:54:41.617675	2016-01-19 13:00:20.364772		rx-deductible-individual	t	t
660	TemplateField	72	LTD Amount	1	2016-01-19 13:04:30.616231	2016-01-19 13:04:30.616231		ltd-amount	t	t
661	TemplateColumn	72	Flat Dollar Amount	2	2016-01-19 13:04:30.629191	2016-01-19 13:04:30.629191		flat-dollar-amount	\N	\N
662	TemplateColumn	72	Percentage of Earnings	1	2016-01-19 13:04:30.639256	2016-01-19 13:04:30.639256		percentage-of-earnings	\N	\N
663	TemplateField	73	Definition of Earnings	1	2016-01-19 13:04:30.679125	2016-01-19 13:04:30.679125		definition-of-earnings	\N	t
664	TemplateColumn	73	Earnings Determination	1	2016-01-19 13:04:30.690475	2016-01-19 13:04:30.690475		earnings-determination	\N	\N
665	TemplateColumn	73	Plus Bonus	3	2016-01-19 13:04:30.699116	2016-01-19 13:04:30.699116		plus-bonus	\N	\N
666	TemplateColumn	73	Plus Commission	2	2016-01-19 13:04:30.707805	2016-01-19 13:04:30.707805		plus-commission	\N	\N
667	TemplateColumn	73	PLUS OVERTIME	4	2016-01-19 13:04:30.716293	2016-01-19 13:04:30.716293		plus-overtime	\N	\N
668	TemplateField	74	Maximum benefit	3	2016-01-19 13:04:30.769133	2016-01-19 13:04:30.769133		maximum-benefit	t	t
669	TemplateField	74	Minimum Benefit	2	2016-01-19 13:04:30.778173	2016-01-19 13:04:30.778173		minimum-benefit	\N	t
670	TemplateField	74	Definition of Disability	5	2016-01-19 13:04:30.787465	2016-01-19 13:04:30.787465		definition-of-disability	t	t
671	TemplateField	74	Guaranteed Issue Limit	4	2016-01-19 13:04:30.796404	2016-01-19 13:04:30.796404		guaranteed-issue-limit	\N	t
672	TemplateField	74	Elimination Period	1	2016-01-19 13:04:30.806748	2016-01-19 13:04:30.806748		elimination-period	t	t
673	TemplateColumn	74	Entries	1	2016-01-19 13:04:30.817427	2016-01-19 13:04:30.817427		entries	\N	\N
674	TemplateField	75	Earnings Test	3	2016-01-19 13:04:30.902963	2016-01-19 13:04:30.902963		earnings-test	\N	\N
675	TemplateField	75	Partial Disability Provision	6	2016-01-19 13:04:30.912193	2016-01-19 13:04:30.912193		partial-disability-provision	\N	\N
676	TemplateField	75	Benefit Integration Offsets	7	2016-01-19 13:04:30.920637	2016-01-19 13:04:30.920637		benefit-integration-offsets	\N	\N
677	TemplateField	75	Recurrent Disability	8	2016-01-19 13:04:30.929012	2016-01-19 13:04:30.929012		recurrent-disability	\N	\N
678	TemplateField	75	Work Incentive Benefit	9	2016-01-19 13:04:30.937758	2016-01-19 13:04:30.937758		work-incentive-benefit	\N	\N
679	TemplateField	75	Social Security Integartion	2	2016-01-19 13:04:30.947754	2016-01-19 13:04:30.947754		social-security-integartion	\N	t
680	TemplateField	75	Zero Day Residual	1	2016-01-19 13:04:30.956763	2016-01-19 13:04:30.956763		zero-day-residual	t	t
681	TemplateField	75	Maximum Benefit Duration	4	2016-01-19 13:04:30.965486	2016-01-19 13:04:30.965486		maximum-benefit-duration	\N	t
682	TemplateField	75	Partial Disability Covered	5	2016-01-19 13:04:30.974275	2016-01-19 13:04:30.974275		partial-disability-covered	\N	t
683	TemplateField	75	Rehabilitation	10	2016-01-19 13:04:30.983253	2016-01-19 13:04:30.983253		rehabilitation	\N	\N
684	TemplateField	75	Pre-existing Condition Exclusions	11	2016-01-19 13:04:30.99191	2016-01-19 13:04:30.99191		pre-existing-condition-exclusions	\N	\N
685	TemplateField	75	Self Reported Disability Limitation	14	2016-01-19 13:04:31.000828	2016-01-19 13:04:31.000828		self-reported-disability-limitation	\N	\N
686	TemplateField	75	W2 Services Provided	15	2016-01-19 13:04:31.009636	2016-01-19 13:04:31.009636		w2-services-provided	\N	\N
687	TemplateField	75	Employer FICA match	16	2016-01-19 13:04:31.018596	2016-01-19 13:04:31.018596		employer-fica-match	\N	\N
688	TemplateField	75	International Employees Covered	18	2016-01-19 13:04:31.027064	2016-01-19 13:04:31.027064		international-employees-covered	\N	\N
689	TemplateField	75	EAP	19	2016-01-19 13:04:31.036206	2016-01-19 13:04:31.036206		eap	\N	\N
690	TemplateField	75	Mental Nervous Limitation	12	2016-01-19 13:04:31.044826	2016-01-19 13:04:31.044826		mental-nervous-limitation	t	t
691	TemplateField	75	Drug & Alcohol Limitation	13	2016-01-19 13:04:31.053957	2016-01-19 13:04:31.053957		drug-alcohol-limitation	t	t
692	TemplateField	75	Gross Up	17	2016-01-19 13:04:31.062551	2016-01-19 13:04:31.062551		gross-up	\N	t
693	TemplateColumn	75	Entries	1	2016-01-19 13:04:31.073012	2016-01-19 13:04:31.073012		entries	\N	\N
694	TemplateField	76	Employee Eligibility Class #1	1	2016-01-19 13:04:31.265487	2016-01-19 13:04:31.265487		employee-eligibility-class-1	\N	\N
695	TemplateField	76	Employee Eligibility Class #2	2	2016-01-19 13:04:31.275517	2016-01-19 13:04:31.275517		employee-eligibility-class-2	\N	\N
696	TemplateField	76	Employee Eligibility Class #3	2	2016-01-19 13:04:31.284062	2016-01-19 13:04:31.284062		employee-eligibility-class-3	\N	\N
697	TemplateField	76	Employee Eligibility Class #4	3	2016-01-19 13:04:31.292786	2016-01-19 13:04:31.292786		employee-eligibility-class-4	\N	\N
698	TemplateColumn	76	Employee Class	1	2016-01-19 13:04:31.303239	2016-01-19 13:04:31.303239		employee-class	\N	\N
699	TemplateColumn	76	Employee Classes	2	2016-01-19 13:04:31.311881	2016-01-19 13:04:31.311881		employee-classes	\N	\N
700	TemplateColumn	76	Waiting Period	3	2016-01-19 13:04:31.320896	2016-01-19 13:04:31.320896		waiting-period	\N	\N
701	TemplateColumn	76	Hours Requirement	4	2016-01-19 13:04:31.329289	2016-01-19 13:04:31.329289		hours-requirement	\N	\N
491	TemplateField	55	Primary Care Office Visit	1	2016-01-19 06:44:10.054409	2016-01-19 13:38:47.70288		primary-care-office-visit	t	t
508	TemplateField	55	Mental Nervous - Inpatient Coverage	14	2016-01-19 06:52:29.816512	2016-01-19 13:39:31.943691		mental-nervous-inpatient-coverage	\N	t
702	TemplateField	77	Plan Type	1	2016-01-19 13:55:11.719501	2016-01-19 13:55:11.719501		plan-type	\N	\N
703	TemplateField	77	Network	2	2016-01-19 13:55:16.716507	2016-01-19 13:55:16.716507		network	\N	\N
704	TemplateField	77	Referrals Needed	3	2016-01-19 13:55:22.081512	2016-01-19 13:55:22.081512		referrals-needed	\N	\N
705	TemplateField	77	Deductible Individual	4	2016-01-19 13:55:26.603177	2016-01-19 13:55:43.318244		deductible-individual	\N	\N
706	TemplateField	77	Deductible Family	5	2016-01-19 13:55:55.798959	2016-01-19 13:55:55.798959		deductible-family	\N	\N
707	TemplateField	77	Deductible - Family Multiple	6	2016-01-19 13:56:22.147122	2016-01-19 13:56:22.147122		deductible-family-multiple	\N	\N
708	TemplateField	77	Deductible Format	7	2016-01-19 13:56:31.425491	2016-01-19 13:56:31.425491		deductible-format	\N	\N
709	TemplateColumn	77	ENTRIES	1	2016-01-19 13:57:14.502706	2016-01-19 13:57:14.502706		entries	\N	\N
710	TemplateField	77	Coinsurance	8	2016-01-19 13:59:21.885669	2016-01-19 13:59:21.885669		coinsurance	\N	\N
711	TemplateField	77	Out-of-Pocket Max Individual (includes deductible)	9	2016-01-19 15:47:41.712906	2016-01-19 15:47:41.712906		out-of-pocket-max-individual-includes-deductible	\N	\N
712	TemplateField	77	Out-of-Pocket Max Family (includes deductible)	10	2016-01-19 15:47:54.443234	2016-01-19 15:47:54.443234		out-of-pocket-max-family-includes-deductible	\N	\N
713	TemplateField	77	Out-of-Pocket Maximum - Family Multiple	11	2016-01-19 15:48:08.01904	2016-01-19 15:48:08.01904		out-of-pocket-maximum-family-multiple	\N	\N
714	TemplateField	77	In & Out-of-Network Deductibles Cross Accumulation	12	2016-01-19 15:48:21.285255	2016-01-19 15:48:21.285255		in-out-of-network-deductibles-cross-accumulation	\N	\N
715	TemplateField	77	In & Out-of-Network OOP Maximum Cross Accumulation	13	2016-01-19 15:48:35.318851	2016-01-19 15:48:35.318851		in-out-of-network-oop-maximum-cross-accumulation	\N	\N
716	TemplateField	77	Annual Maximum	14	2016-01-19 15:48:47.665992	2016-01-19 15:48:47.665992		annual-maximum	\N	\N
717	TemplateField	77	Gym Reimbursement	15	2016-01-19 15:48:56.035837	2016-01-19 15:48:56.035837		gym-reimbursement	\N	\N
718	TemplateField	77	Customer Service Days/Hours	16	2016-01-19 15:49:08.830709	2016-01-19 15:49:08.830709		customer-service-days-hours	\N	\N
719	TemplateField	77	OON Reimbursement Level	17	2016-01-19 15:49:56.946164	2016-01-19 15:49:56.946164		oon-reimbursement-level	\N	\N
720	TemplateField	77	Nurseline	18	2016-01-19 15:50:02.988898	2016-01-19 15:50:02.988898		nurseline	\N	\N
721	TemplateField	77	International Employees Covered	19	2016-01-19 15:50:21.318805	2016-01-19 15:50:21.318805		international-employees-covered	\N	\N
1190	TemplateField	110	Network	3	2016-01-20 09:54:27.072694	2016-01-20 09:54:27.072694		network	t	t
722	TemplateField	78	Referrals Needed	5	2016-01-20 09:48:54.600441	2016-01-20 09:48:54.600441		referrals-needed	t	t
723	TemplateField	78	Coinsurance	15	2016-01-20 09:48:54.61819	2016-01-20 09:48:54.61819		coinsurance	t	t
724	TemplateField	78	Deductible Individual	7	2016-01-20 09:48:54.629164	2016-01-20 09:48:54.629164		deductible-individual	t	t
725	TemplateField	78	Plan Type	1	2016-01-20 09:48:54.640157	2016-01-20 09:48:54.640157		plan-type	t	t
726	TemplateField	78	Deductible Family	9	2016-01-20 09:48:54.650898	2016-01-20 09:48:54.650898		deductible-family	t	t
727	TemplateField	78	Deductible - Family Multiple	11	2016-01-20 09:48:54.662048	2016-01-20 09:48:54.662048		deductible-family-multiple	\N	\N
728	TemplateField	78	Gym Reimbursement	31	2016-01-20 09:48:54.675637	2016-01-20 09:48:54.675637		gym-reimbursement	\N	\N
729	TemplateField	78	Out-of-Pocket Max Family (includes deductible)	19	2016-01-20 09:48:54.686393	2016-01-20 09:48:54.686393		out-of-pocket-max-family-includes-deductible	t	t
730	TemplateField	78	Network	3	2016-01-20 09:48:54.69756	2016-01-20 09:48:54.69756		network	t	t
731	TemplateField	78	Deductible Format	13	2016-01-20 09:48:54.70841	2016-01-20 09:48:54.70841		deductible-format	\N	t
732	TemplateField	78	Annual Maximum	29	2016-01-20 09:48:54.721787	2016-01-20 09:48:54.721787		annual-maximum	\N	\N
733	TemplateField	78	International Employees Covered	37	2016-01-20 09:48:54.750713	2016-01-20 09:48:54.750713		international-employees-covered	\N	\N
734	TemplateField	78	Nurseline	35	2016-01-20 09:48:54.765854	2016-01-20 09:48:54.765854		nurseline	\N	\N
735	TemplateField	78	In & Out-of-Network Deductibles Cross Accumulation	23	2016-01-20 09:48:54.776909	2016-01-20 09:48:54.776909		in-out-of-network-deductibles-cross-accumulation	\N	t
736	TemplateField	78	OON Reimbursement Level	27	2016-01-20 09:48:54.787947	2016-01-20 09:48:54.787947		oon-reimbursement-level	t	t
737	TemplateField	78	Customer Service Days/Hours	33	2016-01-20 09:48:54.799108	2016-01-20 09:48:54.799108		customer-service-days-hours	\N	\N
738	TemplateField	78	Out-of-Pocket Max Individual (includes deductible)	17	2016-01-20 09:48:54.810479	2016-01-20 09:48:54.810479		out-of-pocket-max-individual-includes-deductible	t	t
739	TemplateField	78	Out-of-Pocket Maximum - Family Multiple	21	2016-01-20 09:48:54.821793	2016-01-20 09:48:54.821793		out-of-pocket-maximum-family-multiple	\N	f
740	TemplateField	78	In & Out-of-Network OOP Maximum Cross Accumulation	25	2016-01-20 09:48:54.833503	2016-01-20 09:48:54.833503		in-out-of-network-oop-maximum-cross-accumulation	\N	t
741	TemplateColumn	78	ENTRIES	1	2016-01-20 09:48:54.849748	2016-01-20 09:48:54.849748		entries	\N	\N
742	TemplateField	79	General Radiology	4	2016-01-20 09:48:55.102758	2016-01-20 09:48:55.102758		general-radiology	\N	\N
743	TemplateField	79	CT/MRI/PET Scans	5	2016-01-20 09:48:55.113584	2016-01-20 09:48:55.113584		ct-mri-pet-scans	\N	\N
744	TemplateField	79	Annual Physical Exam	6	2016-01-20 09:48:55.124654	2016-01-20 09:48:55.124654		annual-physical-exam	\N	\N
745	TemplateField	79	Well Child Exams	7	2016-01-20 09:48:55.135053	2016-01-20 09:48:55.135053		well-child-exams	\N	\N
746	TemplateField	79	Pediatric Dental	8	2016-01-20 09:48:55.145655	2016-01-20 09:48:55.145655		pediatric-dental	\N	\N
747	TemplateField	79	Surgery/Anesthesiology	11	2016-01-20 09:48:55.155992	2016-01-20 09:48:55.155992		surgery-anesthesiology	\N	\N
748	TemplateField	79	Lab	3	2016-01-20 09:48:55.167455	2016-01-20 09:48:55.167455		lab	\N	t
749	TemplateField	79	Primary Care Office Visit	1	2016-01-20 09:48:55.177814	2016-01-20 09:48:55.177814		primary-care-office-visit	t	t
750	TemplateField	79	Specialist Office Visit	2	2016-01-20 09:48:55.188338	2016-01-20 09:48:55.188338		specialist-office-visit	t	t
751	TemplateField	79	Hospitalization - Outpatient	10	2016-01-20 09:48:55.198813	2016-01-20 09:48:55.198813		hospitalization-outpatient	\N	t
752	TemplateField	79	Hospitalization - Inpatient	9	2016-01-20 09:48:55.209641	2016-01-20 09:48:55.209641		hospitalization-inpatient	t	t
753	TemplateField	79	Urgent Care	13	2016-01-20 09:48:55.22039	2016-01-20 09:48:55.22039		urgent-care	\N	t
754	TemplateField	79	Emergency Room	12	2016-01-20 09:48:55.230804	2016-01-20 09:48:55.230804		emergency-room	t	t
755	TemplateField	79	Mental Nervous - Inpatient Coverage	14	2016-01-20 09:48:55.241739	2016-01-20 09:48:55.241739		mental-nervous-inpatient-coverage	\N	t
756	TemplateField	79	Mental Nervous - Outpatient Coverage	15	2016-01-20 09:48:55.252501	2016-01-20 09:48:55.252501		mental-nervous-outpatient-coverage	\N	t
757	TemplateField	79	Physical Therapy - Inpatient Coverage	18	2016-01-20 09:48:55.262887	2016-01-20 09:48:55.262887		physical-therapy-inpatient-coverage	\N	\N
758	TemplateField	79	Occupational Therapy - Inpatient Coverage	20	2016-01-20 09:48:55.273182	2016-01-20 09:48:55.273182		occupational-therapy-inpatient-coverage	\N	\N
759	TemplateField	79	Occupational Therapy - Outpatient Coverage	21	2016-01-20 09:48:55.283289	2016-01-20 09:48:55.283289		occupational-therapy-outpatient-coverage	\N	\N
760	TemplateField	79	Speech Therapy - Inpatient Coverage	22	2016-01-20 09:48:55.293891	2016-01-20 09:48:55.293891		speech-therapy-inpatient-coverage	\N	\N
761	TemplateField	79	Speech Therapy - Outpatient Coverage	23	2016-01-20 09:48:55.304392	2016-01-20 09:48:55.304392		speech-therapy-outpatient-coverage	\N	\N
762	TemplateField	79	Pregnancy & Maternity Care - Office Visits	24	2016-01-20 09:48:55.315099	2016-01-20 09:48:55.315099		pregnancy-maternity-care-office-visits	\N	\N
763	TemplateField	79	Pregnancy & Maternity Care - Labor & Delivery	25	2016-01-20 09:48:55.325415	2016-01-20 09:48:55.325415		pregnancy-maternity-care-labor-delivery	\N	\N
764	TemplateField	79	Ambulance	27	2016-01-20 09:48:55.335948	2016-01-20 09:48:55.335948		ambulance	\N	\N
765	TemplateField	79	Hospice	28	2016-01-20 09:48:55.34608	2016-01-20 09:48:55.34608		hospice	\N	\N
766	TemplateField	79	Home Healthcare	29	2016-01-20 09:48:55.356026	2016-01-20 09:48:55.356026		home-healthcare	\N	\N
767	TemplateField	79	Skilled Nursing	30	2016-01-20 09:48:55.366118	2016-01-20 09:48:55.366118		skilled-nursing	\N	\N
768	TemplateField	79	Prosthetics	32	2016-01-20 09:48:55.376302	2016-01-20 09:48:55.376302		prosthetics	\N	\N
769	TemplateField	79	Hearing Devices	34	2016-01-20 09:48:55.386727	2016-01-20 09:48:55.386727		hearing-devices	\N	\N
770	TemplateField	79	Vision Exams	35	2016-01-20 09:48:55.396822	2016-01-20 09:48:55.396822		vision-exams	\N	\N
771	TemplateField	79	Short Term Rehabilitation- Inpatient	36	2016-01-20 09:48:55.40702	2016-01-20 09:48:55.40702		short-term-rehabilitation-inpatient	\N	\N
772	TemplateField	79	Short Term Rehabilitation - Outpatient	37	2016-01-20 09:48:55.417902	2016-01-20 09:48:55.417902		short-term-rehabilitation-outpatient	\N	\N
773	TemplateField	79	Telemedicine	38	2016-01-20 09:48:55.429302	2016-01-20 09:48:55.429302		telemedicine	\N	\N
774	TemplateField	79	Speciality Rx Coverage	39	2016-01-20 09:48:55.439595	2016-01-20 09:48:55.439595		speciality-rx-coverage	\N	\N
775	TemplateField	79	Substance Abuse - Inpatient Coverage	16	2016-01-20 09:48:55.450059	2016-01-20 09:48:55.450059		substance-abuse-inpatient-coverage	\N	t
776	TemplateField	79	Chiropractic Services	26	2016-01-20 09:48:55.461462	2016-01-20 09:48:55.461462		chiropractic-services	\N	t
777	TemplateField	79	Physical Therapy - Outpatient Coverage	19	2016-01-20 09:48:55.472357	2016-01-20 09:48:55.472357		physical-therapy-outpatient-coverage	\N	t
778	TemplateField	79	Durable Medical Equipment	33	2016-01-20 09:48:55.483061	2016-01-20 09:48:55.483061		durable-medical-equipment	\N	t
779	TemplateField	79	Infertility Coverage	31	2016-01-20 09:48:55.494042	2016-01-20 09:48:55.494042		infertility-coverage	\N	t
780	TemplateField	79	Substance Abuse - Outpatient Coverage	17	2016-01-20 09:48:55.504663	2016-01-20 09:48:55.504663		substance-abuse-outpatient-coverage	\N	t
781	TemplateColumn	79	Subject to Deductible & Coinsurance	1	2016-01-20 09:48:55.517608	2016-01-20 09:48:55.517608		subject-to-deductible-coinsurance	\N	\N
782	TemplateColumn	79	COPAY	2	2016-01-20 09:48:55.528179	2016-01-20 09:48:55.528179		copay	\N	\N
783	TemplateColumn	79	LIMIT	3	2016-01-20 09:48:55.538813	2016-01-20 09:48:55.538813		limit	\N	\N
784	TemplateColumn	79	COPAY LIMIT	4	2016-01-20 09:48:55.550899	2016-01-20 09:48:55.550899		copay-limit	\N	\N
785	TemplateColumn	79	SEPERATE DEDUCTIBLE	5	2016-01-20 09:48:55.561552	2016-01-20 09:48:55.561552		seperate-deductible	\N	\N
786	TemplateColumn	79	Separate Coinsurance	6	2016-01-20 09:48:55.571919	2016-01-20 09:48:55.571919		separate-coinsurance	\N	\N
787	TemplateField	80	Subject to Medical Deductible & Coinsurance	2	2016-01-20 09:48:58.387132	2016-01-20 09:48:58.387132		subject-to-medical-deductible-coinsurance	\N	\N
788	TemplateField	80	Mandatory Generic	4	2016-01-20 09:48:58.397643	2016-01-20 09:48:58.397643		mandatory-generic	\N	\N
789	TemplateField	80	Step Therapy/Precertification Applies	5	2016-01-20 09:48:58.407927	2016-01-20 09:48:58.407927		step-therapy-precertification-applies	\N	\N
790	TemplateField	80	Oral Contraceptive	6	2016-01-20 09:48:58.418048	2016-01-20 09:48:58.418048		oral-contraceptive	\N	\N
791	TemplateField	80	Subject to Medical Deductible	1	2016-01-20 09:48:58.428032	2016-01-20 09:48:58.428032		subject-to-medical-deductible	\N	t
792	TemplateField	80	Network	3	2016-01-20 09:48:58.438049	2016-01-20 09:48:58.438049		network	\N	t
793	TemplateField	80	Prescriptions Covered Out of Network	7	2016-01-20 09:48:58.448034	2016-01-20 09:48:58.448034		prescriptions-covered-out-of-network	\N	\N
794	TemplateColumn	80	ENTRIES	1	2016-01-20 09:48:58.460338	2016-01-20 09:48:58.460338		entries	\N	\N
795	TemplateField	81	Rx Deductible - Individual	1	2016-01-20 09:48:58.554254	2016-01-20 09:48:58.554254		rx-deductible-individual	t	t
796	TemplateField	81	Rx Deductible - Family	2	2016-01-20 09:48:58.56493	2016-01-20 09:48:58.56493		rx-deductible-family	t	t
797	TemplateColumn	81	DEDUCTIBLE	1	2016-01-20 09:48:58.577254	2016-01-20 09:48:58.577254		deductible	\N	\N
798	TemplateColumn	81	PER MEMBER	2	2016-01-20 09:48:58.587385	2016-01-20 09:48:58.587385		per-member	\N	\N
799	TemplateField	82	Specialty Medications  -Retail	61	2016-01-20 09:48:58.653078	2016-01-20 09:48:58.653078		specialty-medications-retail	\N	\N
800	TemplateField	82	Retail Rx (Tier 1)	1	2016-01-20 09:48:58.663307	2016-01-20 09:48:58.663307		retail-rx-tier-1	t	t
801	TemplateField	82	Retail Rx (Tier 3)	13	2016-01-20 09:48:58.673919	2016-01-20 09:48:58.673919		retail-rx-tier-3	t	t
802	TemplateField	82	Mail Order Rx (Tier 1)	31	2016-01-20 09:48:58.684536	2016-01-20 09:48:58.684536		mail-order-rx-tier-1	\N	t
803	TemplateField	82	Retail Rx (Tier 4)	19	2016-01-20 09:48:58.694641	2016-01-20 09:48:58.694641		retail-rx-tier-4	\N	\N
804	TemplateField	82	Specialty Medications -Mail Order	67	2016-01-20 09:48:58.704732	2016-01-20 09:48:58.704732		specialty-medications-mail-order	\N	f
805	TemplateField	82	Retail Rx (Tier 5)	25	2016-01-20 09:48:58.715032	2016-01-20 09:48:58.715032		retail-rx-tier-5	\N	\N
806	TemplateField	82	Retail Rx (Tier 2)	7	2016-01-20 09:48:58.725331	2016-01-20 09:48:58.725331		retail-rx-tier-2	t	t
807	TemplateField	82	Mail Order Rx (Tier 5)	55	2016-01-20 09:48:58.735954	2016-01-20 09:48:58.735954		mail-order-rx-tier-5	\N	\N
808	TemplateField	82	Mail Order Rx (Tier 4)	49	2016-01-20 09:48:58.746391	2016-01-20 09:48:58.746391		mail-order-rx-tier-4	\N	\N
809	TemplateField	82	Mail Order Rx (Tier 3)	43	2016-01-20 09:48:58.757292	2016-01-20 09:48:58.757292		mail-order-rx-tier-3	\N	t
810	TemplateField	82	Mail Order Rx (Tier 2)	37	2016-01-20 09:48:58.768533	2016-01-20 09:48:58.768533		mail-order-rx-tier-2	\N	t
811	TemplateColumn	82	SUBJECT TO RX DEDUCTIBLE	1	2016-01-20 09:48:58.784327	2016-01-20 09:48:58.784327		subject-to-rx-deductible	\N	\N
812	TemplateColumn	82	COPAY	2	2016-01-20 09:48:58.794884	2016-01-20 09:48:58.794884		copay	\N	\N
813	TemplateColumn	82	COINSURANCE	3	2016-01-20 09:48:58.805642	2016-01-20 09:48:58.805642		coinsurance	\N	\N
814	TemplateColumn	82	PER SCRIPT COINSURANCE LIMIT	4	2016-01-20 09:48:58.816015	2016-01-20 09:48:58.816015		per-script-coinsurance-limit	\N	\N
815	TemplateColumn	82	PER SCRIPT COINSURANCE MINIMUM	5	2016-01-20 09:48:58.826871	2016-01-20 09:48:58.826871		per-script-coinsurance-minimum	\N	\N
816	TemplateField	83	Disease Management	1	2016-01-20 09:48:59.733725	2016-01-20 09:48:59.733725		disease-management	\N	\N
817	TemplateColumn	83	IDENTIFICATION METHODS	1	2016-01-20 09:48:59.747527	2016-01-20 09:48:59.747527		identification-methods	\N	\N
818	TemplateColumn	83	NUMBER OF CONDITIONS TRACKED	2	2016-01-20 09:48:59.75836	2016-01-20 09:48:59.75836		number-of-conditions-tracked	\N	\N
819	TemplateColumn	83	OUTREACH METHODS	3	2016-01-20 09:48:59.769328	2016-01-20 09:48:59.769328		outreach-methods	\N	\N
820	TemplateColumn	83	REPORT FREQUENCY	4	2016-01-20 09:48:59.780563	2016-01-20 09:48:59.780563		report-frequency	\N	\N
821	TemplateField	84	Integrated Wellness Plan	1	2016-01-20 09:48:59.857591	2016-01-20 09:48:59.857591		integrated-wellness-plan	\N	\N
822	TemplateColumn	84	Entries	1	2016-01-20 09:48:59.870419	2016-01-20 09:48:59.870419		entries	\N	\N
823	TemplateField	85	Employee Eligibility Class #1	1	2016-01-20 09:48:59.903595	2016-01-20 09:48:59.903595		employee-eligibility-class-1	\N	\N
824	TemplateField	85	Employee Eligibility Class #2	2	2016-01-20 09:48:59.914309	2016-01-20 09:48:59.914309		employee-eligibility-class-2	\N	\N
825	TemplateField	85	Employee Eligibility Class #3	3	2016-01-20 09:48:59.925243	2016-01-20 09:48:59.925243		employee-eligibility-class-3	\N	\N
826	TemplateField	85	Employee Eligibility Class #4	4	2016-01-20 09:48:59.936116	2016-01-20 09:48:59.936116		employee-eligibility-class-4	\N	\N
827	TemplateColumn	85	Employee Class	1	2016-01-20 09:48:59.949107	2016-01-20 09:48:59.949107		employee-class	\N	\N
828	TemplateColumn	85	Employee Classes	2	2016-01-20 09:48:59.95931	2016-01-20 09:48:59.95931		employee-classes	\N	\N
829	TemplateColumn	85	Waiting Period	3	2016-01-20 09:48:59.96957	2016-01-20 09:48:59.96957		waiting-period	\N	\N
830	TemplateColumn	85	Hours Requirement	4	2016-01-20 09:48:59.979714	2016-01-20 09:48:59.979714		hours-requirement	\N	\N
831	TemplateColumn	85	Spouse Eligible	5	2016-01-20 09:48:59.990868	2016-01-20 09:48:59.990868		spouse-eligible	\N	\N
832	TemplateColumn	85	Dependent Children Eligible	6	2016-01-20 09:49:00.001479	2016-01-20 09:49:00.001479		dependent-children-eligible	\N	\N
833	TemplateColumn	85	Domestic Partner Eligible	7	2016-01-20 09:49:00.011892	2016-01-20 09:49:00.011892		domestic-partner-eligible	\N	\N
834	TemplateColumn	85	Dependent Child Limiting Age	8	2016-01-20 09:49:00.022473	2016-01-20 09:49:00.022473		dependent-child-limiting-age	\N	\N
835	TemplateColumn	85	Dependent Child Coverage Ends	9	2016-01-20 09:49:00.033558	2016-01-20 09:49:00.033558		dependent-child-coverage-ends	\N	\N
836	TemplateField	86	Plan Type	1	2016-01-20 09:49:00.47323	2016-01-20 09:49:00.47323		plan-type	\N	\N
837	TemplateField	86	Network	2	2016-01-20 09:49:00.483903	2016-01-20 09:49:00.483903		network	\N	\N
838	TemplateField	86	Referrals Needed	3	2016-01-20 09:49:00.495043	2016-01-20 09:49:00.495043		referrals-needed	\N	\N
839	TemplateField	86	Deductible Individual	4	2016-01-20 09:49:00.506136	2016-01-20 09:49:00.506136		deductible-individual	\N	\N
840	TemplateField	86	Deductible Family	5	2016-01-20 09:49:00.517532	2016-01-20 09:49:00.517532		deductible-family	\N	\N
841	TemplateField	86	Deductible - Family Multiple	6	2016-01-20 09:49:00.5282	2016-01-20 09:49:00.5282		deductible-family-multiple	\N	\N
842	TemplateField	86	Deductible Format	7	2016-01-20 09:49:00.538944	2016-01-20 09:49:00.538944		deductible-format	\N	\N
843	TemplateField	86	Coinsurance	8	2016-01-20 09:49:00.552225	2016-01-20 09:49:00.552225		coinsurance	\N	\N
844	TemplateField	86	Out-of-Pocket Max Individual (includes deductible)	9	2016-01-20 09:49:00.563349	2016-01-20 09:49:00.563349		out-of-pocket-max-individual-includes-deductible	\N	\N
845	TemplateField	86	Out-of-Pocket Max Family (includes deductible)	10	2016-01-20 09:49:00.574191	2016-01-20 09:49:00.574191		out-of-pocket-max-family-includes-deductible	\N	\N
846	TemplateField	86	Out-of-Pocket Maximum - Family Multiple	11	2016-01-20 09:49:00.585305	2016-01-20 09:49:00.585305		out-of-pocket-maximum-family-multiple	\N	\N
847	TemplateField	86	In & Out-of-Network Deductibles Cross Accumulation	12	2016-01-20 09:49:00.596084	2016-01-20 09:49:00.596084		in-out-of-network-deductibles-cross-accumulation	\N	\N
848	TemplateField	86	In & Out-of-Network OOP Maximum Cross Accumulation	13	2016-01-20 09:49:00.606868	2016-01-20 09:49:00.606868		in-out-of-network-oop-maximum-cross-accumulation	\N	\N
849	TemplateField	86	Annual Maximum	14	2016-01-20 09:49:00.617413	2016-01-20 09:49:00.617413		annual-maximum	\N	\N
850	TemplateField	86	Gym Reimbursement	15	2016-01-20 09:49:00.628157	2016-01-20 09:49:00.628157		gym-reimbursement	\N	\N
851	TemplateField	86	Customer Service Days/Hours	16	2016-01-20 09:49:00.638864	2016-01-20 09:49:00.638864		customer-service-days-hours	\N	\N
852	TemplateField	86	OON Reimbursement Level	17	2016-01-20 09:49:00.649431	2016-01-20 09:49:00.649431		oon-reimbursement-level	\N	\N
853	TemplateField	86	Nurseline	18	2016-01-20 09:49:00.660503	2016-01-20 09:49:00.660503		nurseline	\N	\N
854	TemplateField	86	International Employees Covered	19	2016-01-20 09:49:00.671495	2016-01-20 09:49:00.671495		international-employees-covered	\N	\N
855	TemplateColumn	86	ENTRIES	1	2016-01-20 09:49:00.684407	2016-01-20 09:49:00.684407		entries	\N	\N
856	TemplateField	87	Deductible - Family Multiple	6	2016-01-20 09:49:00.914508	2016-01-20 09:49:00.914508		deductible-family-multiple	\N	\N
857	TemplateField	87	Out-of-Pocket Maximum - Family Multiple	11	2016-01-20 09:49:00.925642	2016-01-20 09:49:00.925642		out-of-pocket-maximum-family-multiple	\N	\N
858	TemplateField	87	Annual Maximum	15	2016-01-20 09:49:00.936922	2016-01-20 09:49:00.936922		annual-maximum	\N	\N
859	TemplateField	87	Gym Reimbursement	16	2016-01-20 09:49:00.94844	2016-01-20 09:49:00.94844		gym-reimbursement	\N	\N
860	TemplateField	87	Customer Service Days/Hours	17	2016-01-20 09:49:00.960315	2016-01-20 09:49:00.960315		customer-service-days-hours	\N	\N
861	TemplateField	87	Nurseline	18	2016-01-20 09:49:00.971689	2016-01-20 09:49:00.971689		nurseline	\N	\N
862	TemplateField	87	International Employees Covered	19	2016-01-20 09:49:00.98304	2016-01-20 09:49:00.98304		international-employees-covered	\N	\N
863	TemplateField	87	Plan Type	1	2016-01-20 09:49:00.99396	2016-01-20 09:49:00.99396		plan-type	t	t
864	TemplateField	87	Network	2	2016-01-20 09:49:01.005282	2016-01-20 09:49:01.005282		network	t	t
865	TemplateField	87	Referrals Needed	3	2016-01-20 09:49:01.016452	2016-01-20 09:49:01.016452		referrals-needed	t	t
866	TemplateField	87	Deductible Individual	4	2016-01-20 09:49:01.028002	2016-01-20 09:49:01.028002		deductible-individual	t	t
867	TemplateField	87	Deductible Family	5	2016-01-20 09:49:01.039665	2016-01-20 09:49:01.039665		deductible-family	t	t
868	TemplateField	87	Deductible Format	7	2016-01-20 09:49:01.051212	2016-01-20 09:49:01.051212		deductible-format	\N	t
869	TemplateField	87	Coinsurance	8	2016-01-20 09:49:01.062889	2016-01-20 09:49:01.062889		coinsurance	t	t
870	TemplateField	87	Out-of-Pocket Max Individual (includes deductible)	9	2016-01-20 09:49:01.074104	2016-01-20 09:49:01.074104		out-of-pocket-max-individual-includes-deductible	t	t
871	TemplateField	87	Out-of-Pocket Max Family (includes deductible)	10	2016-01-20 09:49:01.085121	2016-01-20 09:49:01.085121		out-of-pocket-max-family-includes-deductible	t	t
872	TemplateField	87	In & Out-of-Network Deductibles Cross Accumulation	12	2016-01-20 09:49:01.096358	2016-01-20 09:49:01.096358		in-out-of-network-deductibles-cross-accumulation	\N	t
873	TemplateField	87	In & Out-of-Network OOP Maximum Cross Accumulation	13	2016-01-20 09:49:01.107307	2016-01-20 09:49:01.107307		in-out-of-network-oop-maximum-cross-accumulation	\N	t
874	TemplateField	87	OON Reimbursement Level	14	2016-01-20 09:49:01.118232	2016-01-20 09:49:01.118232		oon-reimbursement-level	t	t
875	TemplateColumn	87	ENTRIES	1	2016-01-20 09:49:01.131252	2016-01-20 09:49:01.131252		entries	\N	\N
876	TemplateField	88	General Radiology	4	2016-01-20 09:49:01.413897	2016-01-20 09:49:01.413897		general-radiology	\N	\N
877	TemplateField	88	CT/MRI/PET Scans	5	2016-01-20 09:49:01.424135	2016-01-20 09:49:01.424135		ct-mri-pet-scans	\N	\N
878	TemplateField	88	Annual Physical Exam	6	2016-01-20 09:49:01.434317	2016-01-20 09:49:01.434317		annual-physical-exam	\N	\N
879	TemplateField	88	Well Child Exams	7	2016-01-20 09:49:01.444362	2016-01-20 09:49:01.444362		well-child-exams	\N	\N
880	TemplateField	88	Pediatric Dental	8	2016-01-20 09:49:01.454679	2016-01-20 09:49:01.454679		pediatric-dental	\N	\N
881	TemplateField	88	Surgery/Anesthesiology	11	2016-01-20 09:49:01.465094	2016-01-20 09:49:01.465094		surgery-anesthesiology	\N	\N
882	TemplateField	88	Physical Therapy - Inpatient Coverage	18	2016-01-20 09:49:01.475877	2016-01-20 09:49:01.475877		physical-therapy-inpatient-coverage	\N	\N
883	TemplateField	88	Occupational Therapy - Inpatient Coverage	20	2016-01-20 09:49:01.486275	2016-01-20 09:49:01.486275		occupational-therapy-inpatient-coverage	\N	\N
884	TemplateField	88	Occupational Therapy - Outpatient Coverage	21	2016-01-20 09:49:01.496988	2016-01-20 09:49:01.496988		occupational-therapy-outpatient-coverage	\N	\N
885	TemplateField	88	Speech Therapy - Inpatient Coverage	22	2016-01-20 09:49:01.508216	2016-01-20 09:49:01.508216		speech-therapy-inpatient-coverage	\N	\N
886	TemplateField	88	Speech Therapy - Outpatient Coverage	23	2016-01-20 09:49:01.518901	2016-01-20 09:49:01.518901		speech-therapy-outpatient-coverage	\N	\N
887	TemplateField	88	Pregnancy & Maternity Care - Office Visits	24	2016-01-20 09:49:01.529552	2016-01-20 09:49:01.529552		pregnancy-maternity-care-office-visits	\N	\N
888	TemplateField	88	Pregnancy & Maternity Care - Labor & Delivery	25	2016-01-20 09:49:01.54013	2016-01-20 09:49:01.54013		pregnancy-maternity-care-labor-delivery	\N	\N
889	TemplateField	88	Ambulance	27	2016-01-20 09:49:01.550494	2016-01-20 09:49:01.550494		ambulance	\N	\N
890	TemplateField	88	Hospice	28	2016-01-20 09:49:01.5612	2016-01-20 09:49:01.5612		hospice	\N	\N
891	TemplateField	88	Lab	3	2016-01-20 09:49:01.571548	2016-01-20 09:49:01.571548		lab	\N	t
892	TemplateField	88	Specialist Office Visit	2	2016-01-20 09:49:01.582116	2016-01-20 09:49:01.582116		specialist-office-visit	t	t
893	TemplateField	88	Hospitalization - Outpatient	10	2016-01-20 09:49:01.592572	2016-01-20 09:49:01.592572		hospitalization-outpatient	\N	t
894	TemplateField	88	Hospitalization - Inpatient	9	2016-01-20 09:49:01.603189	2016-01-20 09:49:01.603189		hospitalization-inpatient	t	t
895	TemplateField	88	Urgent Care	13	2016-01-20 09:49:01.614325	2016-01-20 09:49:01.614325		urgent-care	\N	t
896	TemplateField	88	Emergency Room	12	2016-01-20 09:49:01.624624	2016-01-20 09:49:01.624624		emergency-room	t	t
897	TemplateField	88	Mental Nervous - Outpatient Coverage	15	2016-01-20 09:49:01.635709	2016-01-20 09:49:01.635709		mental-nervous-outpatient-coverage	\N	t
898	TemplateField	88	Substance Abuse - Inpatient Coverage	16	2016-01-20 09:49:01.646462	2016-01-20 09:49:01.646462		substance-abuse-inpatient-coverage	\N	t
899	TemplateField	88	Substance Abuse - Outpatient Coverage	17	2016-01-20 09:49:01.658155	2016-01-20 09:49:01.658155		substance-abuse-outpatient-coverage	\N	t
900	TemplateField	88	Physical Therapy - Outpatient Coverage	19	2016-01-20 09:49:01.670607	2016-01-20 09:49:01.670607		physical-therapy-outpatient-coverage	\N	t
901	TemplateField	88	Chiropractic Services	26	2016-01-20 09:49:01.681291	2016-01-20 09:49:01.681291		chiropractic-services	\N	t
902	TemplateField	88	Home Healthcare	29	2016-01-20 09:49:01.692093	2016-01-20 09:49:01.692093		home-healthcare	\N	\N
903	TemplateField	88	Skilled Nursing	30	2016-01-20 09:49:01.70535	2016-01-20 09:49:01.70535		skilled-nursing	\N	\N
904	TemplateField	88	Prosthetics	32	2016-01-20 09:49:01.716016	2016-01-20 09:49:01.716016		prosthetics	\N	\N
905	TemplateField	88	Hearing Devices	34	2016-01-20 09:49:01.731292	2016-01-20 09:49:01.731292		hearing-devices	\N	\N
906	TemplateField	88	Vision Exams	35	2016-01-20 09:49:01.741554	2016-01-20 09:49:01.741554		vision-exams	\N	\N
907	TemplateField	88	Short Term Rehabilitation - Inpatient	36	2016-01-20 09:49:01.75204	2016-01-20 09:49:01.75204		short-term-rehabilitation-inpatient	\N	\N
908	TemplateField	88	Short Term Rehabilitation - Outpatient	37	2016-01-20 09:49:01.762378	2016-01-20 09:49:01.762378		short-term-rehabilitation-outpatient	\N	\N
909	TemplateField	88	Telemedicine	38	2016-01-20 09:49:01.772634	2016-01-20 09:49:01.772634		telemedicine	\N	\N
910	TemplateField	88	Specialty Rx Coverage	39	2016-01-20 09:49:01.783754	2016-01-20 09:49:01.783754		specialty-rx-coverage	\N	\N
911	TemplateField	88	Infertility Coverage	31	2016-01-20 09:49:01.794833	2016-01-20 09:49:01.794833		infertility-coverage	\N	t
912	TemplateField	88	Durable Medical Equipment	33	2016-01-20 09:49:01.805376	2016-01-20 09:49:01.805376		durable-medical-equipment	\N	t
913	TemplateField	88	Primary Care Office Visit	1	2016-01-20 09:49:01.816062	2016-01-20 09:49:01.816062		primary-care-office-visit	t	t
914	TemplateField	88	Mental Nervous - Inpatient Coverage	14	2016-01-20 09:49:01.826771	2016-01-20 09:49:01.826771		mental-nervous-inpatient-coverage	\N	t
915	TemplateColumn	88	Subject to Deductible & Coinsurance	1	2016-01-20 09:49:01.8399	2016-01-20 09:49:01.8399		subject-to-deductible-coinsurance	\N	\N
916	TemplateColumn	88	LIMIT	2	2016-01-20 09:49:01.850449	2016-01-20 09:49:01.850449		limit	\N	\N
917	TemplateColumn	88	SEPERATE DEDUCTIBLE	3	2016-01-20 09:49:01.861303	2016-01-20 09:49:01.861303		seperate-deductible	\N	\N
918	TemplateColumn	88	SEPERATE COINSURANCE	4	2016-01-20 09:49:01.872229	2016-01-20 09:49:01.872229		seperate-coinsurance	\N	\N
919	TemplateField	89	Subject to Medical Deductible & Coinsurance	2	2016-01-20 09:49:03.746543	2016-01-20 09:49:03.746543		subject-to-medical-deductible-coinsurance	\N	\N
920	TemplateField	89	Step Therapy/Precertification Applies	4	2016-01-20 09:49:03.757957	2016-01-20 09:49:03.757957		step-therapy-precertification-applies	\N	\N
921	TemplateField	89	Mandatory Generic	5	2016-01-20 09:49:03.768818	2016-01-20 09:49:03.768818		mandatory-generic	\N	\N
922	TemplateField	89	Oral Contraceptive	6	2016-01-20 09:49:03.780294	2016-01-20 09:49:03.780294		oral-contraceptive	\N	\N
923	TemplateField	89	Are Prescriptions Covered Out of Network	7	2016-01-20 09:49:03.791602	2016-01-20 09:49:03.791602		are-prescriptions-covered-out-of-network	\N	\N
924	TemplateField	89	Subject to Medical Deductible	1	2016-01-20 09:49:03.802605	2016-01-20 09:49:03.802605		subject-to-medical-deductible	\N	t
925	TemplateField	89	Network	3	2016-01-20 09:49:03.81386	2016-01-20 09:49:03.81386		network	\N	t
926	TemplateColumn	89	ENTRIES	1	2016-01-20 09:49:03.827535	2016-01-20 09:49:03.827535		entries	\N	\N
927	TemplateField	90	Rx Deductible - Family	1	2016-01-20 09:49:03.928967	2016-01-20 09:49:03.928967		rx-deductible-family	t	t
928	TemplateField	90	Rx Deductible - Individual	2	2016-01-20 09:49:03.939882	2016-01-20 09:49:03.939882		rx-deductible-individual	t	t
929	TemplateColumn	90	DEDUCTIBLE	1	2016-01-20 09:49:03.952812	2016-01-20 09:49:03.952812		deductible	\N	\N
930	TemplateColumn	90	PER MEMBER	2	2016-01-20 09:49:03.963984	2016-01-20 09:49:03.963984		per-member	\N	\N
931	TemplateField	91	Retail Rx (Tier 4)	4	2016-01-20 09:49:04.034828	2016-01-20 09:49:04.034828		retail-rx-tier-4	\N	\N
932	TemplateField	91	Retail Rx (Tier 5)	5	2016-01-20 09:49:04.045726	2016-01-20 09:49:04.045726		retail-rx-tier-5	\N	\N
933	TemplateField	91	Retail Rx (Tier 2)	2	2016-01-20 09:49:04.056667	2016-01-20 09:49:04.056667		retail-rx-tier-2	t	t
934	TemplateField	91	Retail Rx (Tier 1)	1	2016-01-20 09:49:04.067671	2016-01-20 09:49:04.067671		retail-rx-tier-1	t	t
935	TemplateField	91	Retail Rx (Tier 3)	3	2016-01-20 09:49:04.078861	2016-01-20 09:49:04.078861		retail-rx-tier-3	t	t
936	TemplateField	91	Mail Order Rx (Tier 4)	9	2016-01-20 09:49:04.090052	2016-01-20 09:49:04.090052		mail-order-rx-tier-4	\N	\N
937	TemplateField	91	Mail Order Rx (Tier 5)	10	2016-01-20 09:49:04.100751	2016-01-20 09:49:04.100751		mail-order-rx-tier-5	\N	\N
938	TemplateField	91	Speciality Medications - Mail Order	11	2016-01-20 09:49:04.111611	2016-01-20 09:49:04.111611		speciality-medications-mail-order	\N	\N
939	TemplateField	91	Speciality Medications - Retail	12	2016-01-20 09:49:04.122074	2016-01-20 09:49:04.122074		speciality-medications-retail	\N	\N
940	TemplateField	91	Mail Order Rx (Tier 1)	6	2016-01-20 09:49:04.132385	2016-01-20 09:49:04.132385		mail-order-rx-tier-1	\N	t
941	TemplateField	91	Mail Order Rx (Tier 3)	8	2016-01-20 09:49:04.142922	2016-01-20 09:49:04.142922		mail-order-rx-tier-3	\N	t
942	TemplateField	91	Mail Order Rx (Tier 2 )	7	2016-01-20 09:49:04.153333	2016-01-20 09:49:04.153333		mail-order-rx-tier-2	\N	t
943	TemplateColumn	91	SUBJECT TO RX DEDUCTIBLE	1	2016-01-20 09:49:04.166193	2016-01-20 09:49:04.166193		subject-to-rx-deductible	\N	\N
944	TemplateColumn	91	COINSURANCE	2	2016-01-20 09:49:04.177059	2016-01-20 09:49:04.177059		coinsurance	\N	\N
945	TemplateField	92	Disease Management	1	2016-01-20 09:49:04.466432	2016-01-20 09:49:04.466432		disease-management	\N	\N
946	TemplateColumn	92	IDENTIFICATION METHODS	1	2016-01-20 09:49:04.479253	2016-01-20 09:49:04.479253		identification-methods	\N	\N
947	TemplateColumn	92	NUMBER OF CONDITIONS TRACKED	2	2016-01-20 09:49:04.490225	2016-01-20 09:49:04.490225		number-of-conditions-tracked	\N	\N
948	TemplateColumn	92	OUTREACH METHODS	3	2016-01-20 09:49:04.501317	2016-01-20 09:49:04.501317		outreach-methods	\N	\N
949	TemplateColumn	92	REPORT FREQUENCY	4	2016-01-20 09:49:04.511651	2016-01-20 09:49:04.511651		report-frequency	\N	\N
950	TemplateField	93	Integrated Wellness Plan	1	2016-01-20 09:49:04.582412	2016-01-20 09:49:04.582412		integrated-wellness-plan	\N	\N
951	TemplateColumn	93	ENTRIES	1	2016-01-20 09:49:04.595249	2016-01-20 09:49:04.595249		entries	\N	\N
952	TemplateField	94	Referrals Needed	5	2016-01-20 09:50:07.487171	2016-01-20 09:50:07.487171		referrals-needed	t	t
953	TemplateField	94	Coinsurance	15	2016-01-20 09:50:07.502634	2016-01-20 09:50:07.502634		coinsurance	t	t
954	TemplateField	94	Deductible Individual	7	2016-01-20 09:50:07.513579	2016-01-20 09:50:07.513579		deductible-individual	t	t
955	TemplateField	94	Plan Type	1	2016-01-20 09:50:07.525012	2016-01-20 09:50:07.525012		plan-type	t	t
956	TemplateField	94	Deductible Family	9	2016-01-20 09:50:07.536541	2016-01-20 09:50:07.536541		deductible-family	t	t
957	TemplateField	94	Deductible - Family Multiple	11	2016-01-20 09:50:07.547797	2016-01-20 09:50:07.547797		deductible-family-multiple	\N	\N
958	TemplateField	94	Gym Reimbursement	31	2016-01-20 09:50:07.561077	2016-01-20 09:50:07.561077		gym-reimbursement	\N	\N
959	TemplateField	94	Out-of-Pocket Max Family (includes deductible)	19	2016-01-20 09:50:07.572019	2016-01-20 09:50:07.572019		out-of-pocket-max-family-includes-deductible	t	t
960	TemplateField	94	Network	3	2016-01-20 09:50:07.583466	2016-01-20 09:50:07.583466		network	t	t
961	TemplateField	94	Deductible Format	13	2016-01-20 09:50:07.594354	2016-01-20 09:50:07.594354		deductible-format	\N	t
962	TemplateField	94	Annual Maximum	29	2016-01-20 09:50:07.607613	2016-01-20 09:50:07.607613		annual-maximum	\N	\N
963	TemplateField	94	International Employees Covered	37	2016-01-20 09:50:07.618483	2016-01-20 09:50:07.618483		international-employees-covered	\N	\N
964	TemplateField	94	Nurseline	35	2016-01-20 09:50:07.628927	2016-01-20 09:50:07.628927		nurseline	\N	\N
965	TemplateField	94	In & Out-of-Network Deductibles Cross Accumulation	23	2016-01-20 09:50:07.639575	2016-01-20 09:50:07.639575		in-out-of-network-deductibles-cross-accumulation	\N	t
966	TemplateField	94	OON Reimbursement Level	27	2016-01-20 09:50:07.650758	2016-01-20 09:50:07.650758		oon-reimbursement-level	t	t
967	TemplateField	94	Customer Service Days/Hours	33	2016-01-20 09:50:07.661345	2016-01-20 09:50:07.661345		customer-service-days-hours	\N	\N
968	TemplateField	94	Out-of-Pocket Max Individual (includes deductible)	17	2016-01-20 09:50:07.672215	2016-01-20 09:50:07.672215		out-of-pocket-max-individual-includes-deductible	t	t
969	TemplateField	94	Out-of-Pocket Maximum - Family Multiple	21	2016-01-20 09:50:07.683035	2016-01-20 09:50:07.683035		out-of-pocket-maximum-family-multiple	\N	f
970	TemplateField	94	In & Out-of-Network OOP Maximum Cross Accumulation	25	2016-01-20 09:50:07.694042	2016-01-20 09:50:07.694042		in-out-of-network-oop-maximum-cross-accumulation	\N	t
971	TemplateColumn	94	ENTRIES	1	2016-01-20 09:50:07.709508	2016-01-20 09:50:07.709508		entries	\N	\N
972	TemplateField	95	General Radiology	4	2016-01-20 09:50:07.988669	2016-01-20 09:50:07.988669		general-radiology	\N	\N
973	TemplateField	95	CT/MRI/PET Scans	5	2016-01-20 09:50:08.000128	2016-01-20 09:50:08.000128		ct-mri-pet-scans	\N	\N
974	TemplateField	95	Annual Physical Exam	6	2016-01-20 09:50:08.011838	2016-01-20 09:50:08.011838		annual-physical-exam	\N	\N
975	TemplateField	95	Well Child Exams	7	2016-01-20 09:50:08.023205	2016-01-20 09:50:08.023205		well-child-exams	\N	\N
976	TemplateField	95	Pediatric Dental	8	2016-01-20 09:50:08.034353	2016-01-20 09:50:08.034353		pediatric-dental	\N	\N
977	TemplateField	95	Surgery/Anesthesiology	11	2016-01-20 09:50:08.045537	2016-01-20 09:50:08.045537		surgery-anesthesiology	\N	\N
978	TemplateField	95	Lab	3	2016-01-20 09:50:08.05718	2016-01-20 09:50:08.05718		lab	\N	t
979	TemplateField	95	Primary Care Office Visit	1	2016-01-20 09:50:08.067819	2016-01-20 09:50:08.067819		primary-care-office-visit	t	t
980	TemplateField	95	Specialist Office Visit	2	2016-01-20 09:50:08.079403	2016-01-20 09:50:08.079403		specialist-office-visit	t	t
981	TemplateField	95	Hospitalization - Outpatient	10	2016-01-20 09:50:08.090885	2016-01-20 09:50:08.090885		hospitalization-outpatient	\N	t
982	TemplateField	95	Hospitalization - Inpatient	9	2016-01-20 09:50:08.102956	2016-01-20 09:50:08.102956		hospitalization-inpatient	t	t
983	TemplateField	95	Urgent Care	13	2016-01-20 09:50:08.113997	2016-01-20 09:50:08.113997		urgent-care	\N	t
984	TemplateField	95	Emergency Room	12	2016-01-20 09:50:08.124773	2016-01-20 09:50:08.124773		emergency-room	t	t
985	TemplateField	95	Mental Nervous - Inpatient Coverage	14	2016-01-20 09:50:08.136404	2016-01-20 09:50:08.136404		mental-nervous-inpatient-coverage	\N	t
986	TemplateField	95	Mental Nervous - Outpatient Coverage	15	2016-01-20 09:50:08.147346	2016-01-20 09:50:08.147346		mental-nervous-outpatient-coverage	\N	t
987	TemplateField	95	Physical Therapy - Inpatient Coverage	18	2016-01-20 09:50:08.158119	2016-01-20 09:50:08.158119		physical-therapy-inpatient-coverage	\N	\N
988	TemplateField	95	Occupational Therapy - Inpatient Coverage	20	2016-01-20 09:50:08.169121	2016-01-20 09:50:08.169121		occupational-therapy-inpatient-coverage	\N	\N
989	TemplateField	95	Occupational Therapy - Outpatient Coverage	21	2016-01-20 09:50:08.18358	2016-01-20 09:50:08.18358		occupational-therapy-outpatient-coverage	\N	\N
990	TemplateField	95	Speech Therapy - Inpatient Coverage	22	2016-01-20 09:50:08.194942	2016-01-20 09:50:08.194942		speech-therapy-inpatient-coverage	\N	\N
991	TemplateField	95	Speech Therapy - Outpatient Coverage	23	2016-01-20 09:50:08.205725	2016-01-20 09:50:08.205725		speech-therapy-outpatient-coverage	\N	\N
992	TemplateField	95	Pregnancy & Maternity Care - Office Visits	24	2016-01-20 09:50:08.216954	2016-01-20 09:50:08.216954		pregnancy-maternity-care-office-visits	\N	\N
993	TemplateField	95	Pregnancy & Maternity Care - Labor & Delivery	25	2016-01-20 09:50:08.22802	2016-01-20 09:50:08.22802		pregnancy-maternity-care-labor-delivery	\N	\N
994	TemplateField	95	Ambulance	27	2016-01-20 09:50:08.239553	2016-01-20 09:50:08.239553		ambulance	\N	\N
995	TemplateField	95	Hospice	28	2016-01-20 09:50:08.250699	2016-01-20 09:50:08.250699		hospice	\N	\N
996	TemplateField	95	Home Healthcare	29	2016-01-20 09:50:08.261583	2016-01-20 09:50:08.261583		home-healthcare	\N	\N
997	TemplateField	95	Skilled Nursing	30	2016-01-20 09:50:08.272791	2016-01-20 09:50:08.272791		skilled-nursing	\N	\N
998	TemplateField	95	Prosthetics	32	2016-01-20 09:50:08.283669	2016-01-20 09:50:08.283669		prosthetics	\N	\N
999	TemplateField	95	Hearing Devices	34	2016-01-20 09:50:08.294642	2016-01-20 09:50:08.294642		hearing-devices	\N	\N
1000	TemplateField	95	Vision Exams	35	2016-01-20 09:50:08.305169	2016-01-20 09:50:08.305169		vision-exams	\N	\N
1001	TemplateField	95	Short Term Rehabilitation- Inpatient	36	2016-01-20 09:50:08.316347	2016-01-20 09:50:08.316347		short-term-rehabilitation-inpatient	\N	\N
1002	TemplateField	95	Short Term Rehabilitation - Outpatient	37	2016-01-20 09:50:08.327403	2016-01-20 09:50:08.327403		short-term-rehabilitation-outpatient	\N	\N
1003	TemplateField	95	Telemedicine	38	2016-01-20 09:50:08.338098	2016-01-20 09:50:08.338098		telemedicine	\N	\N
1004	TemplateField	95	Speciality Rx Coverage	39	2016-01-20 09:50:08.349137	2016-01-20 09:50:08.349137		speciality-rx-coverage	\N	\N
1005	TemplateField	95	Substance Abuse - Inpatient Coverage	16	2016-01-20 09:50:08.360325	2016-01-20 09:50:08.360325		substance-abuse-inpatient-coverage	\N	t
1006	TemplateField	95	Chiropractic Services	26	2016-01-20 09:50:08.371341	2016-01-20 09:50:08.371341		chiropractic-services	\N	t
1007	TemplateField	95	Physical Therapy - Outpatient Coverage	19	2016-01-20 09:50:08.382489	2016-01-20 09:50:08.382489		physical-therapy-outpatient-coverage	\N	t
1008	TemplateField	95	Durable Medical Equipment	33	2016-01-20 09:50:08.394244	2016-01-20 09:50:08.394244		durable-medical-equipment	\N	t
1009	TemplateField	95	Infertility Coverage	31	2016-01-20 09:50:08.404781	2016-01-20 09:50:08.404781		infertility-coverage	\N	t
1010	TemplateField	95	Substance Abuse - Outpatient Coverage	17	2016-01-20 09:50:08.415947	2016-01-20 09:50:08.415947		substance-abuse-outpatient-coverage	\N	t
1011	TemplateColumn	95	Subject to Deductible & Coinsurance	1	2016-01-20 09:50:08.429544	2016-01-20 09:50:08.429544		subject-to-deductible-coinsurance	\N	\N
1012	TemplateColumn	95	COPAY	2	2016-01-20 09:50:08.44031	2016-01-20 09:50:08.44031		copay	\N	\N
1013	TemplateColumn	95	LIMIT	3	2016-01-20 09:50:08.451596	2016-01-20 09:50:08.451596		limit	\N	\N
1014	TemplateColumn	95	COPAY LIMIT	4	2016-01-20 09:50:08.462422	2016-01-20 09:50:08.462422		copay-limit	\N	\N
1015	TemplateColumn	95	SEPERATE DEDUCTIBLE	5	2016-01-20 09:50:08.473442	2016-01-20 09:50:08.473442		seperate-deductible	\N	\N
1016	TemplateColumn	95	Separate Coinsurance	6	2016-01-20 09:50:08.485012	2016-01-20 09:50:08.485012		separate-coinsurance	\N	\N
1017	TemplateField	96	Subject to Medical Deductible & Coinsurance	2	2016-01-20 09:50:11.743051	2016-01-20 09:50:11.743051		subject-to-medical-deductible-coinsurance	\N	\N
1018	TemplateField	96	Mandatory Generic	4	2016-01-20 09:50:11.753826	2016-01-20 09:50:11.753826		mandatory-generic	\N	\N
1019	TemplateField	96	Step Therapy/Precertification Applies	5	2016-01-20 09:50:11.764211	2016-01-20 09:50:11.764211		step-therapy-precertification-applies	\N	\N
1020	TemplateField	96	Oral Contraceptive	6	2016-01-20 09:50:11.775222	2016-01-20 09:50:11.775222		oral-contraceptive	\N	\N
1021	TemplateField	96	Subject to Medical Deductible	1	2016-01-20 09:50:11.786027	2016-01-20 09:50:11.786027		subject-to-medical-deductible	\N	t
1022	TemplateField	96	Network	3	2016-01-20 09:50:11.796183	2016-01-20 09:50:11.796183		network	\N	t
1023	TemplateField	96	Prescriptions Covered Out of Network	7	2016-01-20 09:50:11.806323	2016-01-20 09:50:11.806323		prescriptions-covered-out-of-network	\N	\N
1024	TemplateColumn	96	ENTRIES	1	2016-01-20 09:50:11.819061	2016-01-20 09:50:11.819061		entries	\N	\N
1025	TemplateField	97	Rx Deductible - Individual	1	2016-01-20 09:50:11.914648	2016-01-20 09:50:11.914648		rx-deductible-individual	t	t
1026	TemplateField	97	Rx Deductible - Family	2	2016-01-20 09:50:11.924972	2016-01-20 09:50:11.924972		rx-deductible-family	t	t
1027	TemplateColumn	97	DEDUCTIBLE	1	2016-01-20 09:50:11.937182	2016-01-20 09:50:11.937182		deductible	\N	\N
1028	TemplateColumn	97	PER MEMBER	2	2016-01-20 09:50:11.949405	2016-01-20 09:50:11.949405		per-member	\N	\N
1029	TemplateField	98	Specialty Medications  -Retail	61	2016-01-20 09:50:12.015699	2016-01-20 09:50:12.015699		specialty-medications-retail	\N	\N
1030	TemplateField	98	Retail Rx (Tier 1)	1	2016-01-20 09:50:12.026254	2016-01-20 09:50:12.026254		retail-rx-tier-1	t	t
1031	TemplateField	98	Retail Rx (Tier 3)	13	2016-01-20 09:50:12.037809	2016-01-20 09:50:12.037809		retail-rx-tier-3	t	t
1032	TemplateField	98	Mail Order Rx (Tier 1)	31	2016-01-20 09:50:12.048251	2016-01-20 09:50:12.048251		mail-order-rx-tier-1	\N	t
1033	TemplateField	98	Retail Rx (Tier 4)	19	2016-01-20 09:50:12.058618	2016-01-20 09:50:12.058618		retail-rx-tier-4	\N	\N
1034	TemplateField	98	Specialty Medications -Mail Order	67	2016-01-20 09:50:12.07176	2016-01-20 09:50:12.07176		specialty-medications-mail-order	\N	f
1035	TemplateField	98	Retail Rx (Tier 5)	25	2016-01-20 09:50:12.082035	2016-01-20 09:50:12.082035		retail-rx-tier-5	\N	\N
1036	TemplateField	98	Retail Rx (Tier 2)	7	2016-01-20 09:50:12.092365	2016-01-20 09:50:12.092365		retail-rx-tier-2	t	t
1037	TemplateField	98	Mail Order Rx (Tier 5)	55	2016-01-20 09:50:12.102723	2016-01-20 09:50:12.102723		mail-order-rx-tier-5	\N	\N
1038	TemplateField	98	Mail Order Rx (Tier 4)	49	2016-01-20 09:50:12.113095	2016-01-20 09:50:12.113095		mail-order-rx-tier-4	\N	\N
1039	TemplateField	98	Mail Order Rx (Tier 3)	43	2016-01-20 09:50:12.123534	2016-01-20 09:50:12.123534		mail-order-rx-tier-3	\N	t
1040	TemplateField	98	Mail Order Rx (Tier 2)	37	2016-01-20 09:50:12.133975	2016-01-20 09:50:12.133975		mail-order-rx-tier-2	\N	t
1041	TemplateColumn	98	SUBJECT TO RX DEDUCTIBLE	1	2016-01-20 09:50:12.146271	2016-01-20 09:50:12.146271		subject-to-rx-deductible	\N	\N
1042	TemplateColumn	98	COPAY	2	2016-01-20 09:50:12.156285	2016-01-20 09:50:12.156285		copay	\N	\N
1043	TemplateColumn	98	COINSURANCE	3	2016-01-20 09:50:12.166267	2016-01-20 09:50:12.166267		coinsurance	\N	\N
1044	TemplateColumn	98	PER SCRIPT COINSURANCE LIMIT	4	2016-01-20 09:50:12.17666	2016-01-20 09:50:12.17666		per-script-coinsurance-limit	\N	\N
1045	TemplateColumn	98	PER SCRIPT COINSURANCE MINIMUM	5	2016-01-20 09:50:12.187722	2016-01-20 09:50:12.187722		per-script-coinsurance-minimum	\N	\N
1046	TemplateField	99	Disease Management	1	2016-01-20 09:50:12.978907	2016-01-20 09:50:12.978907		disease-management	\N	\N
1047	TemplateColumn	99	IDENTIFICATION METHODS	1	2016-01-20 09:50:12.992254	2016-01-20 09:50:12.992254		identification-methods	\N	\N
1048	TemplateColumn	99	NUMBER OF CONDITIONS TRACKED	2	2016-01-20 09:50:13.003461	2016-01-20 09:50:13.003461		number-of-conditions-tracked	\N	\N
1049	TemplateColumn	99	OUTREACH METHODS	3	2016-01-20 09:50:13.014696	2016-01-20 09:50:13.014696		outreach-methods	\N	\N
1050	TemplateColumn	99	REPORT FREQUENCY	4	2016-01-20 09:50:13.025348	2016-01-20 09:50:13.025348		report-frequency	\N	\N
1051	TemplateField	100	Integrated Wellness Plan	1	2016-01-20 09:50:13.095213	2016-01-20 09:50:13.095213		integrated-wellness-plan	\N	\N
1052	TemplateColumn	100	Entries	1	2016-01-20 09:50:13.108457	2016-01-20 09:50:13.108457		entries	\N	\N
1053	TemplateField	101	Employee Eligibility Class #1	1	2016-01-20 09:50:13.142605	2016-01-20 09:50:13.142605		employee-eligibility-class-1	\N	\N
1054	TemplateField	101	Employee Eligibility Class #2	2	2016-01-20 09:50:13.153726	2016-01-20 09:50:13.153726		employee-eligibility-class-2	\N	\N
1055	TemplateField	101	Employee Eligibility Class #3	3	2016-01-20 09:50:13.166396	2016-01-20 09:50:13.166396		employee-eligibility-class-3	\N	\N
1056	TemplateField	101	Employee Eligibility Class #4	4	2016-01-20 09:50:13.177569	2016-01-20 09:50:13.177569		employee-eligibility-class-4	\N	\N
1057	TemplateColumn	101	Employee Class	1	2016-01-20 09:50:13.191392	2016-01-20 09:50:13.191392		employee-class	\N	\N
1058	TemplateColumn	101	Employee Classes	2	2016-01-20 09:50:13.203117	2016-01-20 09:50:13.203117		employee-classes	\N	\N
1059	TemplateColumn	101	Waiting Period	3	2016-01-20 09:50:13.214853	2016-01-20 09:50:13.214853		waiting-period	\N	\N
1060	TemplateColumn	101	Hours Requirement	4	2016-01-20 09:50:13.226077	2016-01-20 09:50:13.226077		hours-requirement	\N	\N
1061	TemplateColumn	101	Spouse Eligible	5	2016-01-20 09:50:13.237005	2016-01-20 09:50:13.237005		spouse-eligible	\N	\N
1062	TemplateColumn	101	Dependent Children Eligible	6	2016-01-20 09:50:13.248675	2016-01-20 09:50:13.248675		dependent-children-eligible	\N	\N
1063	TemplateColumn	101	Domestic Partner Eligible	7	2016-01-20 09:50:13.259895	2016-01-20 09:50:13.259895		domestic-partner-eligible	\N	\N
1064	TemplateColumn	101	Dependent Child Limiting Age	8	2016-01-20 09:50:13.270613	2016-01-20 09:50:13.270613		dependent-child-limiting-age	\N	\N
1065	TemplateColumn	101	Dependent Child Coverage Ends	9	2016-01-20 09:50:13.281479	2016-01-20 09:50:13.281479		dependent-child-coverage-ends	\N	\N
1066	TemplateField	102	Plan Type	1	2016-01-20 09:50:13.714963	2016-01-20 09:50:13.714963		plan-type	\N	\N
1067	TemplateField	102	Network	2	2016-01-20 09:50:13.725516	2016-01-20 09:50:13.725516		network	\N	\N
1068	TemplateField	102	Referrals Needed	3	2016-01-20 09:50:13.73594	2016-01-20 09:50:13.73594		referrals-needed	\N	\N
1069	TemplateField	102	Deductible Individual	4	2016-01-20 09:50:13.746391	2016-01-20 09:50:13.746391		deductible-individual	\N	\N
1070	TemplateField	102	Deductible Family	5	2016-01-20 09:50:13.757831	2016-01-20 09:50:13.757831		deductible-family	\N	\N
1071	TemplateField	102	Deductible - Family Multiple	6	2016-01-20 09:50:13.769224	2016-01-20 09:50:13.769224		deductible-family-multiple	\N	\N
1072	TemplateField	102	Deductible Format	7	2016-01-20 09:50:13.779548	2016-01-20 09:50:13.779548		deductible-format	\N	\N
1073	TemplateField	102	Coinsurance	8	2016-01-20 09:50:13.790253	2016-01-20 09:50:13.790253		coinsurance	\N	\N
1074	TemplateField	102	Out-of-Pocket Max Individual (includes deductible)	9	2016-01-20 09:50:13.800553	2016-01-20 09:50:13.800553		out-of-pocket-max-individual-includes-deductible	\N	\N
1075	TemplateField	102	Out-of-Pocket Max Family (includes deductible)	10	2016-01-20 09:50:13.810786	2016-01-20 09:50:13.810786		out-of-pocket-max-family-includes-deductible	\N	\N
1076	TemplateField	102	Out-of-Pocket Maximum - Family Multiple	11	2016-01-20 09:50:13.821351	2016-01-20 09:50:13.821351		out-of-pocket-maximum-family-multiple	\N	\N
1077	TemplateField	102	In & Out-of-Network Deductibles Cross Accumulation	12	2016-01-20 09:50:13.831929	2016-01-20 09:50:13.831929		in-out-of-network-deductibles-cross-accumulation	\N	\N
1078	TemplateField	102	In & Out-of-Network OOP Maximum Cross Accumulation	13	2016-01-20 09:50:13.84276	2016-01-20 09:50:13.84276		in-out-of-network-oop-maximum-cross-accumulation	\N	\N
1079	TemplateField	102	Annual Maximum	14	2016-01-20 09:50:13.853173	2016-01-20 09:50:13.853173		annual-maximum	\N	\N
1080	TemplateField	102	Gym Reimbursement	15	2016-01-20 09:50:13.867168	2016-01-20 09:50:13.867168		gym-reimbursement	\N	\N
1081	TemplateField	102	Customer Service Days/Hours	16	2016-01-20 09:50:13.877889	2016-01-20 09:50:13.877889		customer-service-days-hours	\N	\N
1082	TemplateField	102	OON Reimbursement Level	17	2016-01-20 09:50:13.888659	2016-01-20 09:50:13.888659		oon-reimbursement-level	\N	\N
1083	TemplateField	102	Nurseline	18	2016-01-20 09:50:13.899211	2016-01-20 09:50:13.899211		nurseline	\N	\N
1084	TemplateField	102	International Employees Covered	19	2016-01-20 09:50:13.911637	2016-01-20 09:50:13.911637		international-employees-covered	\N	\N
1085	TemplateColumn	102	ENTRIES	1	2016-01-20 09:50:13.933273	2016-01-20 09:50:13.933273		entries	\N	\N
1086	TemplateField	103	Deductible - Family Multiple	6	2016-01-20 09:50:14.149478	2016-01-20 09:50:14.149478		deductible-family-multiple	\N	\N
1087	TemplateField	103	Out-of-Pocket Maximum - Family Multiple	11	2016-01-20 09:50:14.160266	2016-01-20 09:50:14.160266		out-of-pocket-maximum-family-multiple	\N	\N
1088	TemplateField	103	Annual Maximum	15	2016-01-20 09:50:14.193074	2016-01-20 09:50:14.193074		annual-maximum	\N	\N
1089	TemplateField	103	Gym Reimbursement	16	2016-01-20 09:50:14.204516	2016-01-20 09:50:14.204516		gym-reimbursement	\N	\N
1090	TemplateField	103	Customer Service Days/Hours	17	2016-01-20 09:50:14.215472	2016-01-20 09:50:14.215472		customer-service-days-hours	\N	\N
1091	TemplateField	103	Nurseline	18	2016-01-20 09:50:14.227654	2016-01-20 09:50:14.227654		nurseline	\N	\N
1092	TemplateField	103	International Employees Covered	19	2016-01-20 09:50:14.237989	2016-01-20 09:50:14.237989		international-employees-covered	\N	\N
1093	TemplateField	103	Plan Type	1	2016-01-20 09:50:14.248779	2016-01-20 09:50:14.248779		plan-type	t	t
1094	TemplateField	103	Network	2	2016-01-20 09:50:14.259336	2016-01-20 09:50:14.259336		network	t	t
1095	TemplateField	103	Referrals Needed	3	2016-01-20 09:50:14.270463	2016-01-20 09:50:14.270463		referrals-needed	t	t
1096	TemplateField	103	Deductible Individual	4	2016-01-20 09:50:14.281366	2016-01-20 09:50:14.281366		deductible-individual	t	t
1097	TemplateField	103	Deductible Family	5	2016-01-20 09:50:14.292061	2016-01-20 09:50:14.292061		deductible-family	t	t
1098	TemplateField	103	Deductible Format	7	2016-01-20 09:50:14.302662	2016-01-20 09:50:14.302662		deductible-format	\N	t
1099	TemplateField	103	Coinsurance	8	2016-01-20 09:50:14.314136	2016-01-20 09:50:14.314136		coinsurance	t	t
1100	TemplateField	103	Out-of-Pocket Max Individual (includes deductible)	9	2016-01-20 09:50:14.325415	2016-01-20 09:50:14.325415		out-of-pocket-max-individual-includes-deductible	t	t
1101	TemplateField	103	Out-of-Pocket Max Family (includes deductible)	10	2016-01-20 09:50:14.336415	2016-01-20 09:50:14.336415		out-of-pocket-max-family-includes-deductible	t	t
1102	TemplateField	103	In & Out-of-Network Deductibles Cross Accumulation	12	2016-01-20 09:50:14.347154	2016-01-20 09:50:14.347154		in-out-of-network-deductibles-cross-accumulation	\N	t
1103	TemplateField	103	In & Out-of-Network OOP Maximum Cross Accumulation	13	2016-01-20 09:50:14.35766	2016-01-20 09:50:14.35766		in-out-of-network-oop-maximum-cross-accumulation	\N	t
1104	TemplateField	103	OON Reimbursement Level	14	2016-01-20 09:50:14.368335	2016-01-20 09:50:14.368335		oon-reimbursement-level	t	t
1105	TemplateColumn	103	ENTRIES	1	2016-01-20 09:50:14.381417	2016-01-20 09:50:14.381417		entries	\N	\N
1106	TemplateField	104	General Radiology	4	2016-01-20 09:50:14.630338	2016-01-20 09:50:14.630338		general-radiology	\N	\N
1107	TemplateField	104	CT/MRI/PET Scans	5	2016-01-20 09:50:14.640968	2016-01-20 09:50:14.640968		ct-mri-pet-scans	\N	\N
1108	TemplateField	104	Annual Physical Exam	6	2016-01-20 09:50:14.652437	2016-01-20 09:50:14.652437		annual-physical-exam	\N	\N
1109	TemplateField	104	Well Child Exams	7	2016-01-20 09:50:14.66309	2016-01-20 09:50:14.66309		well-child-exams	\N	\N
1110	TemplateField	104	Pediatric Dental	8	2016-01-20 09:50:14.674278	2016-01-20 09:50:14.674278		pediatric-dental	\N	\N
1111	TemplateField	104	Surgery/Anesthesiology	11	2016-01-20 09:50:14.685455	2016-01-20 09:50:14.685455		surgery-anesthesiology	\N	\N
1112	TemplateField	104	Physical Therapy - Inpatient Coverage	18	2016-01-20 09:50:14.696068	2016-01-20 09:50:14.696068		physical-therapy-inpatient-coverage	\N	\N
1113	TemplateField	104	Occupational Therapy - Inpatient Coverage	20	2016-01-20 09:50:14.706439	2016-01-20 09:50:14.706439		occupational-therapy-inpatient-coverage	\N	\N
1114	TemplateField	104	Occupational Therapy - Outpatient Coverage	21	2016-01-20 09:50:14.717723	2016-01-20 09:50:14.717723		occupational-therapy-outpatient-coverage	\N	\N
1115	TemplateField	104	Speech Therapy - Inpatient Coverage	22	2016-01-20 09:50:14.728476	2016-01-20 09:50:14.728476		speech-therapy-inpatient-coverage	\N	\N
1116	TemplateField	104	Speech Therapy - Outpatient Coverage	23	2016-01-20 09:50:14.739533	2016-01-20 09:50:14.739533		speech-therapy-outpatient-coverage	\N	\N
1117	TemplateField	104	Pregnancy & Maternity Care - Office Visits	24	2016-01-20 09:50:14.750352	2016-01-20 09:50:14.750352		pregnancy-maternity-care-office-visits	\N	\N
1118	TemplateField	104	Pregnancy & Maternity Care - Labor & Delivery	25	2016-01-20 09:50:14.761918	2016-01-20 09:50:14.761918		pregnancy-maternity-care-labor-delivery	\N	\N
1119	TemplateField	104	Ambulance	27	2016-01-20 09:50:14.773193	2016-01-20 09:50:14.773193		ambulance	\N	\N
1120	TemplateField	104	Hospice	28	2016-01-20 09:50:14.783995	2016-01-20 09:50:14.783995		hospice	\N	\N
1121	TemplateField	104	Lab	3	2016-01-20 09:50:14.795251	2016-01-20 09:50:14.795251		lab	\N	t
1122	TemplateField	104	Specialist Office Visit	2	2016-01-20 09:50:14.806049	2016-01-20 09:50:14.806049		specialist-office-visit	t	t
1123	TemplateField	104	Hospitalization - Outpatient	10	2016-01-20 09:50:14.817308	2016-01-20 09:50:14.817308		hospitalization-outpatient	\N	t
1124	TemplateField	104	Hospitalization - Inpatient	9	2016-01-20 09:50:14.827938	2016-01-20 09:50:14.827938		hospitalization-inpatient	t	t
1125	TemplateField	104	Urgent Care	13	2016-01-20 09:50:14.838506	2016-01-20 09:50:14.838506		urgent-care	\N	t
1126	TemplateField	104	Emergency Room	12	2016-01-20 09:50:14.849442	2016-01-20 09:50:14.849442		emergency-room	t	t
1127	TemplateField	104	Mental Nervous - Outpatient Coverage	15	2016-01-20 09:50:14.861156	2016-01-20 09:50:14.861156		mental-nervous-outpatient-coverage	\N	t
1128	TemplateField	104	Substance Abuse - Inpatient Coverage	16	2016-01-20 09:50:14.871807	2016-01-20 09:50:14.871807		substance-abuse-inpatient-coverage	\N	t
1129	TemplateField	104	Substance Abuse - Outpatient Coverage	17	2016-01-20 09:50:14.882271	2016-01-20 09:50:14.882271		substance-abuse-outpatient-coverage	\N	t
1130	TemplateField	104	Physical Therapy - Outpatient Coverage	19	2016-01-20 09:50:14.893358	2016-01-20 09:50:14.893358		physical-therapy-outpatient-coverage	\N	t
1131	TemplateField	104	Chiropractic Services	26	2016-01-20 09:50:14.904402	2016-01-20 09:50:14.904402		chiropractic-services	\N	t
1132	TemplateField	104	Home Healthcare	29	2016-01-20 09:50:14.914626	2016-01-20 09:50:14.914626		home-healthcare	\N	\N
1133	TemplateField	104	Skilled Nursing	30	2016-01-20 09:50:14.925611	2016-01-20 09:50:14.925611		skilled-nursing	\N	\N
1134	TemplateField	104	Prosthetics	32	2016-01-20 09:50:14.935858	2016-01-20 09:50:14.935858		prosthetics	\N	\N
1135	TemplateField	104	Hearing Devices	34	2016-01-20 09:50:14.946612	2016-01-20 09:50:14.946612		hearing-devices	\N	\N
1136	TemplateField	104	Vision Exams	35	2016-01-20 09:50:14.957195	2016-01-20 09:50:14.957195		vision-exams	\N	\N
1137	TemplateField	104	Short Term Rehabilitation - Inpatient	36	2016-01-20 09:50:14.967463	2016-01-20 09:50:14.967463		short-term-rehabilitation-inpatient	\N	\N
1138	TemplateField	104	Short Term Rehabilitation - Outpatient	37	2016-01-20 09:50:14.977971	2016-01-20 09:50:14.977971		short-term-rehabilitation-outpatient	\N	\N
1139	TemplateField	104	Telemedicine	38	2016-01-20 09:50:14.988515	2016-01-20 09:50:14.988515		telemedicine	\N	\N
1140	TemplateField	104	Specialty Rx Coverage	39	2016-01-20 09:50:14.99893	2016-01-20 09:50:14.99893		specialty-rx-coverage	\N	\N
1141	TemplateField	104	Infertility Coverage	31	2016-01-20 09:50:15.009614	2016-01-20 09:50:15.009614		infertility-coverage	\N	t
1142	TemplateField	104	Durable Medical Equipment	33	2016-01-20 09:50:15.020503	2016-01-20 09:50:15.020503		durable-medical-equipment	\N	t
1143	TemplateField	104	Primary Care Office Visit	1	2016-01-20 09:50:15.030862	2016-01-20 09:50:15.030862		primary-care-office-visit	t	t
1144	TemplateField	104	Mental Nervous - Inpatient Coverage	14	2016-01-20 09:50:15.041382	2016-01-20 09:50:15.041382		mental-nervous-inpatient-coverage	\N	t
1145	TemplateColumn	104	Subject to Deductible & Coinsurance	1	2016-01-20 09:50:15.054047	2016-01-20 09:50:15.054047		subject-to-deductible-coinsurance	\N	\N
1146	TemplateColumn	104	LIMIT	2	2016-01-20 09:50:15.065252	2016-01-20 09:50:15.065252		limit	\N	\N
1147	TemplateColumn	104	SEPERATE DEDUCTIBLE	3	2016-01-20 09:50:15.075928	2016-01-20 09:50:15.075928		seperate-deductible	\N	\N
1148	TemplateColumn	104	SEPERATE COINSURANCE	4	2016-01-20 09:50:15.08674	2016-01-20 09:50:15.08674		seperate-coinsurance	\N	\N
1149	TemplateField	105	Subject to Medical Deductible & Coinsurance	2	2016-01-20 09:50:16.953813	2016-01-20 09:50:16.953813		subject-to-medical-deductible-coinsurance	\N	\N
1150	TemplateField	105	Step Therapy/Precertification Applies	4	2016-01-20 09:50:16.964886	2016-01-20 09:50:16.964886		step-therapy-precertification-applies	\N	\N
1151	TemplateField	105	Mandatory Generic	5	2016-01-20 09:50:16.975669	2016-01-20 09:50:16.975669		mandatory-generic	\N	\N
1152	TemplateField	105	Oral Contraceptive	6	2016-01-20 09:50:16.986123	2016-01-20 09:50:16.986123		oral-contraceptive	\N	\N
1153	TemplateField	105	Are Prescriptions Covered Out of Network	7	2016-01-20 09:50:16.996506	2016-01-20 09:50:16.996506		are-prescriptions-covered-out-of-network	\N	\N
1154	TemplateField	105	Subject to Medical Deductible	1	2016-01-20 09:50:17.008165	2016-01-20 09:50:17.008165		subject-to-medical-deductible	\N	t
1155	TemplateField	105	Network	3	2016-01-20 09:50:17.019296	2016-01-20 09:50:17.019296		network	\N	t
1156	TemplateColumn	105	ENTRIES	1	2016-01-20 09:50:17.031995	2016-01-20 09:50:17.031995		entries	\N	\N
1157	TemplateField	106	Rx Deductible - Family	1	2016-01-20 09:50:17.131843	2016-01-20 09:50:17.131843		rx-deductible-family	t	t
1158	TemplateField	106	Rx Deductible - Individual	2	2016-01-20 09:50:17.143309	2016-01-20 09:50:17.143309		rx-deductible-individual	t	t
1159	TemplateColumn	106	DEDUCTIBLE	1	2016-01-20 09:50:17.156114	2016-01-20 09:50:17.156114		deductible	\N	\N
1160	TemplateColumn	106	PER MEMBER	2	2016-01-20 09:50:17.166884	2016-01-20 09:50:17.166884		per-member	\N	\N
1161	TemplateField	107	Retail Rx (Tier 4)	4	2016-01-20 09:50:17.239922	2016-01-20 09:50:17.239922		retail-rx-tier-4	\N	\N
1162	TemplateField	107	Retail Rx (Tier 5)	5	2016-01-20 09:50:17.250201	2016-01-20 09:50:17.250201		retail-rx-tier-5	\N	\N
1163	TemplateField	107	Retail Rx (Tier 2)	2	2016-01-20 09:50:17.260785	2016-01-20 09:50:17.260785		retail-rx-tier-2	t	t
1164	TemplateField	107	Retail Rx (Tier 1)	1	2016-01-20 09:50:17.271309	2016-01-20 09:50:17.271309		retail-rx-tier-1	t	t
1165	TemplateField	107	Retail Rx (Tier 3)	3	2016-01-20 09:50:17.282121	2016-01-20 09:50:17.282121		retail-rx-tier-3	t	t
1166	TemplateField	107	Mail Order Rx (Tier 4)	9	2016-01-20 09:50:17.293372	2016-01-20 09:50:17.293372		mail-order-rx-tier-4	\N	\N
1167	TemplateField	107	Mail Order Rx (Tier 5)	10	2016-01-20 09:50:17.304437	2016-01-20 09:50:17.304437		mail-order-rx-tier-5	\N	\N
1168	TemplateField	107	Speciality Medications - Mail Order	11	2016-01-20 09:50:17.315227	2016-01-20 09:50:17.315227		speciality-medications-mail-order	\N	\N
1169	TemplateField	107	Speciality Medications - Retail	12	2016-01-20 09:50:17.325559	2016-01-20 09:50:17.325559		speciality-medications-retail	\N	\N
1170	TemplateField	107	Mail Order Rx (Tier 1)	6	2016-01-20 09:50:17.336094	2016-01-20 09:50:17.336094		mail-order-rx-tier-1	\N	t
1171	TemplateField	107	Mail Order Rx (Tier 3)	8	2016-01-20 09:50:17.347016	2016-01-20 09:50:17.347016		mail-order-rx-tier-3	\N	t
1172	TemplateField	107	Mail Order Rx (Tier 2 )	7	2016-01-20 09:50:17.358173	2016-01-20 09:50:17.358173		mail-order-rx-tier-2	\N	t
1173	TemplateColumn	107	SUBJECT TO RX DEDUCTIBLE	1	2016-01-20 09:50:17.371024	2016-01-20 09:50:17.371024		subject-to-rx-deductible	\N	\N
1174	TemplateColumn	107	COINSURANCE	2	2016-01-20 09:50:17.381445	2016-01-20 09:50:17.381445		coinsurance	\N	\N
1175	TemplateField	108	Disease Management	1	2016-01-20 09:50:17.665876	2016-01-20 09:50:17.665876		disease-management	\N	\N
1176	TemplateColumn	108	IDENTIFICATION METHODS	1	2016-01-20 09:50:17.678707	2016-01-20 09:50:17.678707		identification-methods	\N	\N
1177	TemplateColumn	108	NUMBER OF CONDITIONS TRACKED	2	2016-01-20 09:50:17.69169	2016-01-20 09:50:17.69169		number-of-conditions-tracked	\N	\N
1178	TemplateColumn	108	OUTREACH METHODS	3	2016-01-20 09:50:17.70226	2016-01-20 09:50:17.70226		outreach-methods	\N	\N
1179	TemplateColumn	108	REPORT FREQUENCY	4	2016-01-20 09:50:17.712766	2016-01-20 09:50:17.712766		report-frequency	\N	\N
1180	TemplateField	109	Integrated Wellness Plan	1	2016-01-20 09:50:17.781293	2016-01-20 09:50:17.781293		integrated-wellness-plan	\N	\N
1181	TemplateColumn	109	ENTRIES	1	2016-01-20 09:50:17.795436	2016-01-20 09:50:17.795436		entries	\N	\N
1182	TemplateField	110	Referrals Needed	5	2016-01-20 09:54:26.979046	2016-01-20 09:54:26.979046		referrals-needed	t	t
1183	TemplateField	110	Coinsurance	15	2016-01-20 09:54:26.994631	2016-01-20 09:54:26.994631		coinsurance	t	t
1184	TemplateField	110	Deductible Individual	7	2016-01-20 09:54:27.005557	2016-01-20 09:54:27.005557		deductible-individual	t	t
1185	TemplateField	110	Plan Type	1	2016-01-20 09:54:27.016728	2016-01-20 09:54:27.016728		plan-type	t	t
1186	TemplateField	110	Deductible Family	9	2016-01-20 09:54:27.027472	2016-01-20 09:54:27.027472		deductible-family	t	t
1187	TemplateField	110	Deductible - Family Multiple	11	2016-01-20 09:54:27.038379	2016-01-20 09:54:27.038379		deductible-family-multiple	\N	\N
1188	TemplateField	110	Gym Reimbursement	31	2016-01-20 09:54:27.051476	2016-01-20 09:54:27.051476		gym-reimbursement	\N	\N
1189	TemplateField	110	Out-of-Pocket Max Family (includes deductible)	19	2016-01-20 09:54:27.062229	2016-01-20 09:54:27.062229		out-of-pocket-max-family-includes-deductible	t	t
1191	TemplateField	110	Deductible Format	13	2016-01-20 09:54:27.083264	2016-01-20 09:54:27.083264		deductible-format	\N	t
1192	TemplateField	110	Annual Maximum	29	2016-01-20 09:54:27.096191	2016-01-20 09:54:27.096191		annual-maximum	\N	\N
1193	TemplateField	110	International Employees Covered	37	2016-01-20 09:54:27.107535	2016-01-20 09:54:27.107535		international-employees-covered	\N	\N
1194	TemplateField	110	Nurseline	35	2016-01-20 09:54:27.118561	2016-01-20 09:54:27.118561		nurseline	\N	\N
1195	TemplateField	110	In & Out-of-Network Deductibles Cross Accumulation	23	2016-01-20 09:54:27.1317	2016-01-20 09:54:27.1317		in-out-of-network-deductibles-cross-accumulation	\N	t
1196	TemplateField	110	OON Reimbursement Level	27	2016-01-20 09:54:27.142416	2016-01-20 09:54:27.142416		oon-reimbursement-level	t	t
1197	TemplateField	110	Customer Service Days/Hours	33	2016-01-20 09:54:27.154752	2016-01-20 09:54:27.154752		customer-service-days-hours	\N	\N
1198	TemplateField	110	Out-of-Pocket Max Individual (includes deductible)	17	2016-01-20 09:54:27.165989	2016-01-20 09:54:27.165989		out-of-pocket-max-individual-includes-deductible	t	t
1199	TemplateField	110	Out-of-Pocket Maximum - Family Multiple	21	2016-01-20 09:54:27.177706	2016-01-20 09:54:27.177706		out-of-pocket-maximum-family-multiple	\N	f
1200	TemplateField	110	In & Out-of-Network OOP Maximum Cross Accumulation	25	2016-01-20 09:54:27.188392	2016-01-20 09:54:27.188392		in-out-of-network-oop-maximum-cross-accumulation	\N	t
1201	TemplateColumn	110	ENTRIES	1	2016-01-20 09:54:27.203345	2016-01-20 09:54:27.203345		entries	\N	\N
1202	TemplateField	111	General Radiology	4	2016-01-20 09:54:27.467543	2016-01-20 09:54:27.467543		general-radiology	\N	\N
1203	TemplateField	111	CT/MRI/PET Scans	5	2016-01-20 09:54:27.478359	2016-01-20 09:54:27.478359		ct-mri-pet-scans	\N	\N
1204	TemplateField	111	Annual Physical Exam	6	2016-01-20 09:54:27.49052	2016-01-20 09:54:27.49052		annual-physical-exam	\N	\N
1205	TemplateField	111	Well Child Exams	7	2016-01-20 09:54:27.501328	2016-01-20 09:54:27.501328		well-child-exams	\N	\N
1206	TemplateField	111	Pediatric Dental	8	2016-01-20 09:54:27.512044	2016-01-20 09:54:27.512044		pediatric-dental	\N	\N
1207	TemplateField	111	Surgery/Anesthesiology	11	2016-01-20 09:54:27.522985	2016-01-20 09:54:27.522985		surgery-anesthesiology	\N	\N
1208	TemplateField	111	Lab	3	2016-01-20 09:54:27.534051	2016-01-20 09:54:27.534051		lab	\N	t
1209	TemplateField	111	Primary Care Office Visit	1	2016-01-20 09:54:27.544938	2016-01-20 09:54:27.544938		primary-care-office-visit	t	t
1210	TemplateField	111	Specialist Office Visit	2	2016-01-20 09:54:27.555574	2016-01-20 09:54:27.555574		specialist-office-visit	t	t
1211	TemplateField	111	Hospitalization - Outpatient	10	2016-01-20 09:54:27.566681	2016-01-20 09:54:27.566681		hospitalization-outpatient	\N	t
1212	TemplateField	111	Hospitalization - Inpatient	9	2016-01-20 09:54:27.577468	2016-01-20 09:54:27.577468		hospitalization-inpatient	t	t
1213	TemplateField	111	Urgent Care	13	2016-01-20 09:54:27.587943	2016-01-20 09:54:27.587943		urgent-care	\N	t
1214	TemplateField	111	Emergency Room	12	2016-01-20 09:54:27.598329	2016-01-20 09:54:27.598329		emergency-room	t	t
1215	TemplateField	111	Mental Nervous - Inpatient Coverage	14	2016-01-20 09:54:27.608894	2016-01-20 09:54:27.608894		mental-nervous-inpatient-coverage	\N	t
1216	TemplateField	111	Mental Nervous - Outpatient Coverage	15	2016-01-20 09:54:27.620649	2016-01-20 09:54:27.620649		mental-nervous-outpatient-coverage	\N	t
1217	TemplateField	111	Physical Therapy - Inpatient Coverage	18	2016-01-20 09:54:27.631424	2016-01-20 09:54:27.631424		physical-therapy-inpatient-coverage	\N	\N
1218	TemplateField	111	Occupational Therapy - Inpatient Coverage	20	2016-01-20 09:54:27.642148	2016-01-20 09:54:27.642148		occupational-therapy-inpatient-coverage	\N	\N
1219	TemplateField	111	Occupational Therapy - Outpatient Coverage	21	2016-01-20 09:54:27.65288	2016-01-20 09:54:27.65288		occupational-therapy-outpatient-coverage	\N	\N
1220	TemplateField	111	Speech Therapy - Inpatient Coverage	22	2016-01-20 09:54:27.663809	2016-01-20 09:54:27.663809		speech-therapy-inpatient-coverage	\N	\N
1221	TemplateField	111	Speech Therapy - Outpatient Coverage	23	2016-01-20 09:54:27.674801	2016-01-20 09:54:27.674801		speech-therapy-outpatient-coverage	\N	\N
1339	TemplateField	120	Well Child Exams	7	2016-01-20 09:54:33.789379	2016-01-20 09:54:33.789379		well-child-exams	\N	\N
1222	TemplateField	111	Pregnancy & Maternity Care - Office Visits	24	2016-01-20 09:54:27.685355	2016-01-20 09:54:27.685355		pregnancy-maternity-care-office-visits	\N	\N
1223	TemplateField	111	Pregnancy & Maternity Care - Labor & Delivery	25	2016-01-20 09:54:27.696412	2016-01-20 09:54:27.696412		pregnancy-maternity-care-labor-delivery	\N	\N
1224	TemplateField	111	Ambulance	27	2016-01-20 09:54:27.70688	2016-01-20 09:54:27.70688		ambulance	\N	\N
1225	TemplateField	111	Hospice	28	2016-01-20 09:54:27.717133	2016-01-20 09:54:27.717133		hospice	\N	\N
1226	TemplateField	111	Home Healthcare	29	2016-01-20 09:54:27.727615	2016-01-20 09:54:27.727615		home-healthcare	\N	\N
1227	TemplateField	111	Skilled Nursing	30	2016-01-20 09:54:27.737936	2016-01-20 09:54:27.737936		skilled-nursing	\N	\N
1228	TemplateField	111	Prosthetics	32	2016-01-20 09:54:27.7484	2016-01-20 09:54:27.7484		prosthetics	\N	\N
1229	TemplateField	111	Hearing Devices	34	2016-01-20 09:54:27.759113	2016-01-20 09:54:27.759113		hearing-devices	\N	\N
1230	TemplateField	111	Vision Exams	35	2016-01-20 09:54:27.769818	2016-01-20 09:54:27.769818		vision-exams	\N	\N
1231	TemplateField	111	Short Term Rehabilitation- Inpatient	36	2016-01-20 09:54:27.780914	2016-01-20 09:54:27.780914		short-term-rehabilitation-inpatient	\N	\N
1232	TemplateField	111	Short Term Rehabilitation - Outpatient	37	2016-01-20 09:54:27.792302	2016-01-20 09:54:27.792302		short-term-rehabilitation-outpatient	\N	\N
1233	TemplateField	111	Telemedicine	38	2016-01-20 09:54:27.802911	2016-01-20 09:54:27.802911		telemedicine	\N	\N
1234	TemplateField	111	Speciality Rx Coverage	39	2016-01-20 09:54:27.813688	2016-01-20 09:54:27.813688		speciality-rx-coverage	\N	\N
1235	TemplateField	111	Substance Abuse - Inpatient Coverage	16	2016-01-20 09:54:27.824167	2016-01-20 09:54:27.824167		substance-abuse-inpatient-coverage	\N	t
1236	TemplateField	111	Chiropractic Services	26	2016-01-20 09:54:27.834942	2016-01-20 09:54:27.834942		chiropractic-services	\N	t
1237	TemplateField	111	Physical Therapy - Outpatient Coverage	19	2016-01-20 09:54:27.845648	2016-01-20 09:54:27.845648		physical-therapy-outpatient-coverage	\N	t
1238	TemplateField	111	Durable Medical Equipment	33	2016-01-20 09:54:27.856327	2016-01-20 09:54:27.856327		durable-medical-equipment	\N	t
1239	TemplateField	111	Infertility Coverage	31	2016-01-20 09:54:27.866887	2016-01-20 09:54:27.866887		infertility-coverage	\N	t
1240	TemplateField	111	Substance Abuse - Outpatient Coverage	17	2016-01-20 09:54:27.878237	2016-01-20 09:54:27.878237		substance-abuse-outpatient-coverage	\N	t
1241	TemplateColumn	111	Subject to Deductible & Coinsurance	1	2016-01-20 09:54:27.891588	2016-01-20 09:54:27.891588		subject-to-deductible-coinsurance	\N	\N
1242	TemplateColumn	111	COPAY	2	2016-01-20 09:54:27.90215	2016-01-20 09:54:27.90215		copay	\N	\N
1243	TemplateColumn	111	LIMIT	3	2016-01-20 09:54:27.912743	2016-01-20 09:54:27.912743		limit	\N	\N
1244	TemplateColumn	111	COPAY LIMIT	4	2016-01-20 09:54:27.923737	2016-01-20 09:54:27.923737		copay-limit	\N	\N
1245	TemplateColumn	111	SEPERATE DEDUCTIBLE	5	2016-01-20 09:54:27.934436	2016-01-20 09:54:27.934436		seperate-deductible	\N	\N
1246	TemplateColumn	111	Separate Coinsurance	6	2016-01-20 09:54:27.945354	2016-01-20 09:54:27.945354		separate-coinsurance	\N	\N
1247	TemplateField	112	Subject to Medical Deductible & Coinsurance	2	2016-01-20 09:54:30.804358	2016-01-20 09:54:30.804358		subject-to-medical-deductible-coinsurance	\N	\N
1248	TemplateField	112	Mandatory Generic	4	2016-01-20 09:54:30.814776	2016-01-20 09:54:30.814776		mandatory-generic	\N	\N
1249	TemplateField	112	Step Therapy/Precertification Applies	5	2016-01-20 09:54:30.82513	2016-01-20 09:54:30.82513		step-therapy-precertification-applies	\N	\N
1250	TemplateField	112	Oral Contraceptive	6	2016-01-20 09:54:30.835346	2016-01-20 09:54:30.835346		oral-contraceptive	\N	\N
1251	TemplateField	112	Subject to Medical Deductible	1	2016-01-20 09:54:30.846948	2016-01-20 09:54:30.846948		subject-to-medical-deductible	\N	t
1252	TemplateField	112	Network	3	2016-01-20 09:54:30.857732	2016-01-20 09:54:30.857732		network	\N	t
1253	TemplateField	112	Prescriptions Covered Out of Network	7	2016-01-20 09:54:30.868067	2016-01-20 09:54:30.868067		prescriptions-covered-out-of-network	\N	\N
1254	TemplateColumn	112	ENTRIES	1	2016-01-20 09:54:30.881083	2016-01-20 09:54:30.881083		entries	\N	\N
1255	TemplateField	113	Rx Deductible - Individual	1	2016-01-20 09:54:30.977122	2016-01-20 09:54:30.977122		rx-deductible-individual	t	t
1256	TemplateField	113	Rx Deductible - Family	2	2016-01-20 09:54:30.988118	2016-01-20 09:54:30.988118		rx-deductible-family	t	t
1257	TemplateColumn	113	DEDUCTIBLE	1	2016-01-20 09:54:31.001015	2016-01-20 09:54:31.001015		deductible	\N	\N
1258	TemplateColumn	113	PER MEMBER	2	2016-01-20 09:54:31.013306	2016-01-20 09:54:31.013306		per-member	\N	\N
1259	TemplateField	114	Specialty Medications  -Retail	61	2016-01-20 09:54:31.080304	2016-01-20 09:54:31.080304		specialty-medications-retail	\N	\N
1260	TemplateField	114	Retail Rx (Tier 1)	1	2016-01-20 09:54:31.090686	2016-01-20 09:54:31.090686		retail-rx-tier-1	t	t
1261	TemplateField	114	Retail Rx (Tier 3)	13	2016-01-20 09:54:31.100983	2016-01-20 09:54:31.100983		retail-rx-tier-3	t	t
1262	TemplateField	114	Mail Order Rx (Tier 1)	31	2016-01-20 09:54:31.111566	2016-01-20 09:54:31.111566		mail-order-rx-tier-1	\N	t
1263	TemplateField	114	Retail Rx (Tier 4)	19	2016-01-20 09:54:31.121783	2016-01-20 09:54:31.121783		retail-rx-tier-4	\N	\N
1264	TemplateField	114	Specialty Medications -Mail Order	67	2016-01-20 09:54:31.131652	2016-01-20 09:54:31.131652		specialty-medications-mail-order	\N	f
1265	TemplateField	114	Retail Rx (Tier 5)	25	2016-01-20 09:54:31.14179	2016-01-20 09:54:31.14179		retail-rx-tier-5	\N	\N
1266	TemplateField	114	Retail Rx (Tier 2)	7	2016-01-20 09:54:31.152358	2016-01-20 09:54:31.152358		retail-rx-tier-2	t	t
1267	TemplateField	114	Mail Order Rx (Tier 5)	55	2016-01-20 09:54:31.192259	2016-01-20 09:54:31.192259		mail-order-rx-tier-5	\N	\N
1268	TemplateField	114	Mail Order Rx (Tier 4)	49	2016-01-20 09:54:31.204188	2016-01-20 09:54:31.204188		mail-order-rx-tier-4	\N	\N
1269	TemplateField	114	Mail Order Rx (Tier 3)	43	2016-01-20 09:54:31.214702	2016-01-20 09:54:31.214702		mail-order-rx-tier-3	\N	t
1270	TemplateField	114	Mail Order Rx (Tier 2)	37	2016-01-20 09:54:31.225238	2016-01-20 09:54:31.225238		mail-order-rx-tier-2	\N	t
1271	TemplateColumn	114	SUBJECT TO RX DEDUCTIBLE	1	2016-01-20 09:54:31.237629	2016-01-20 09:54:31.237629		subject-to-rx-deductible	\N	\N
1272	TemplateColumn	114	COPAY	2	2016-01-20 09:54:31.248589	2016-01-20 09:54:31.248589		copay	\N	\N
1273	TemplateColumn	114	COINSURANCE	3	2016-01-20 09:54:31.258612	2016-01-20 09:54:31.258612		coinsurance	\N	\N
1274	TemplateColumn	114	PER SCRIPT COINSURANCE LIMIT	4	2016-01-20 09:54:31.268892	2016-01-20 09:54:31.268892		per-script-coinsurance-limit	\N	\N
1275	TemplateColumn	114	PER SCRIPT COINSURANCE MINIMUM	5	2016-01-20 09:54:31.281608	2016-01-20 09:54:31.281608		per-script-coinsurance-minimum	\N	\N
1276	TemplateField	115	Disease Management	1	2016-01-20 09:54:32.101167	2016-01-20 09:54:32.101167		disease-management	\N	\N
1277	TemplateColumn	115	IDENTIFICATION METHODS	1	2016-01-20 09:54:32.114574	2016-01-20 09:54:32.114574		identification-methods	\N	\N
1278	TemplateColumn	115	NUMBER OF CONDITIONS TRACKED	2	2016-01-20 09:54:32.125735	2016-01-20 09:54:32.125735		number-of-conditions-tracked	\N	\N
1279	TemplateColumn	115	OUTREACH METHODS	3	2016-01-20 09:54:32.136516	2016-01-20 09:54:32.136516		outreach-methods	\N	\N
1280	TemplateColumn	115	REPORT FREQUENCY	4	2016-01-20 09:54:32.147398	2016-01-20 09:54:32.147398		report-frequency	\N	\N
1281	TemplateField	116	Integrated Wellness Plan	1	2016-01-20 09:54:32.21649	2016-01-20 09:54:32.21649		integrated-wellness-plan	\N	\N
1282	TemplateColumn	116	Entries	1	2016-01-20 09:54:32.229821	2016-01-20 09:54:32.229821		entries	\N	\N
1283	TemplateField	117	Employee Eligibility Class #1	1	2016-01-20 09:54:32.262688	2016-01-20 09:54:32.262688		employee-eligibility-class-1	\N	\N
1284	TemplateField	117	Employee Eligibility Class #2	2	2016-01-20 09:54:32.273751	2016-01-20 09:54:32.273751		employee-eligibility-class-2	\N	\N
1285	TemplateField	117	Employee Eligibility Class #3	3	2016-01-20 09:54:32.285264	2016-01-20 09:54:32.285264		employee-eligibility-class-3	\N	\N
1286	TemplateField	117	Employee Eligibility Class #4	4	2016-01-20 09:54:32.295859	2016-01-20 09:54:32.295859		employee-eligibility-class-4	\N	\N
1287	TemplateColumn	117	Employee Class	1	2016-01-20 09:54:32.308479	2016-01-20 09:54:32.308479		employee-class	\N	\N
1288	TemplateColumn	117	Employee Classes	2	2016-01-20 09:54:32.318799	2016-01-20 09:54:32.318799		employee-classes	\N	\N
1289	TemplateColumn	117	Waiting Period	3	2016-01-20 09:54:32.329071	2016-01-20 09:54:32.329071		waiting-period	\N	\N
1290	TemplateColumn	117	Hours Requirement	4	2016-01-20 09:54:32.339645	2016-01-20 09:54:32.339645		hours-requirement	\N	\N
1291	TemplateColumn	117	Spouse Eligible	5	2016-01-20 09:54:32.35053	2016-01-20 09:54:32.35053		spouse-eligible	\N	\N
1292	TemplateColumn	117	Dependent Children Eligible	6	2016-01-20 09:54:32.361284	2016-01-20 09:54:32.361284		dependent-children-eligible	\N	\N
1293	TemplateColumn	117	Domestic Partner Eligible	7	2016-01-20 09:54:32.37174	2016-01-20 09:54:32.37174		domestic-partner-eligible	\N	\N
1294	TemplateColumn	117	Dependent Child Limiting Age	8	2016-01-20 09:54:32.382376	2016-01-20 09:54:32.382376		dependent-child-limiting-age	\N	\N
1295	TemplateColumn	117	Dependent Child Coverage Ends	9	2016-01-20 09:54:32.393317	2016-01-20 09:54:32.393317		dependent-child-coverage-ends	\N	\N
1296	TemplateField	118	Plan Type	1	2016-01-20 09:54:32.825246	2016-01-20 09:54:32.825246		plan-type	\N	\N
1297	TemplateField	118	Network	2	2016-01-20 09:54:32.835544	2016-01-20 09:54:32.835544		network	\N	\N
1298	TemplateField	118	Referrals Needed	3	2016-01-20 09:54:32.846471	2016-01-20 09:54:32.846471		referrals-needed	\N	\N
1299	TemplateField	118	Deductible Individual	4	2016-01-20 09:54:32.857446	2016-01-20 09:54:32.857446		deductible-individual	\N	\N
1300	TemplateField	118	Deductible Family	5	2016-01-20 09:54:32.868206	2016-01-20 09:54:32.868206		deductible-family	\N	\N
1301	TemplateField	118	Deductible - Family Multiple	6	2016-01-20 09:54:32.878683	2016-01-20 09:54:32.878683		deductible-family-multiple	\N	\N
1302	TemplateField	118	Deductible Format	7	2016-01-20 09:54:32.889475	2016-01-20 09:54:32.889475		deductible-format	\N	\N
1303	TemplateField	118	Coinsurance	8	2016-01-20 09:54:32.90015	2016-01-20 09:54:32.90015		coinsurance	\N	\N
1304	TemplateField	118	Out-of-Pocket Max Individual (includes deductible)	9	2016-01-20 09:54:32.910942	2016-01-20 09:54:32.910942		out-of-pocket-max-individual-includes-deductible	\N	\N
1305	TemplateField	118	Out-of-Pocket Max Family (includes deductible)	10	2016-01-20 09:54:32.921703	2016-01-20 09:54:32.921703		out-of-pocket-max-family-includes-deductible	\N	\N
1364	TemplateField	120	Prosthetics	32	2016-01-20 09:54:34.063607	2016-01-20 09:54:34.063607		prosthetics	\N	\N
1306	TemplateField	118	Out-of-Pocket Maximum - Family Multiple	11	2016-01-20 09:54:32.932738	2016-01-20 09:54:32.932738		out-of-pocket-maximum-family-multiple	\N	\N
1307	TemplateField	118	In & Out-of-Network Deductibles Cross Accumulation	12	2016-01-20 09:54:32.943631	2016-01-20 09:54:32.943631		in-out-of-network-deductibles-cross-accumulation	\N	\N
1308	TemplateField	118	In & Out-of-Network OOP Maximum Cross Accumulation	13	2016-01-20 09:54:32.954566	2016-01-20 09:54:32.954566		in-out-of-network-oop-maximum-cross-accumulation	\N	\N
1309	TemplateField	118	Annual Maximum	14	2016-01-20 09:54:32.964936	2016-01-20 09:54:32.964936		annual-maximum	\N	\N
1310	TemplateField	118	Gym Reimbursement	15	2016-01-20 09:54:32.975287	2016-01-20 09:54:32.975287		gym-reimbursement	\N	\N
1311	TemplateField	118	Customer Service Days/Hours	16	2016-01-20 09:54:32.985834	2016-01-20 09:54:32.985834		customer-service-days-hours	\N	\N
1312	TemplateField	118	OON Reimbursement Level	17	2016-01-20 09:54:32.997208	2016-01-20 09:54:32.997208		oon-reimbursement-level	\N	\N
1313	TemplateField	118	Nurseline	18	2016-01-20 09:54:33.0078	2016-01-20 09:54:33.0078		nurseline	\N	\N
1314	TemplateField	118	International Employees Covered	19	2016-01-20 09:54:33.018918	2016-01-20 09:54:33.018918		international-employees-covered	\N	\N
1315	TemplateColumn	118	ENTRIES	1	2016-01-20 09:54:33.0319	2016-01-20 09:54:33.0319		entries	\N	\N
1316	TemplateField	119	Deductible - Family Multiple	6	2016-01-20 09:54:33.249494	2016-01-20 09:54:33.249494		deductible-family-multiple	\N	\N
1317	TemplateField	119	Out-of-Pocket Maximum - Family Multiple	11	2016-01-20 09:54:33.259752	2016-01-20 09:54:33.259752		out-of-pocket-maximum-family-multiple	\N	\N
1318	TemplateField	119	Annual Maximum	15	2016-01-20 09:54:33.27042	2016-01-20 09:54:33.27042		annual-maximum	\N	\N
1319	TemplateField	119	Gym Reimbursement	16	2016-01-20 09:54:33.280471	2016-01-20 09:54:33.280471		gym-reimbursement	\N	\N
1320	TemplateField	119	Customer Service Days/Hours	17	2016-01-20 09:54:33.291058	2016-01-20 09:54:33.291058		customer-service-days-hours	\N	\N
1321	TemplateField	119	Nurseline	18	2016-01-20 09:54:33.301378	2016-01-20 09:54:33.301378		nurseline	\N	\N
1322	TemplateField	119	International Employees Covered	19	2016-01-20 09:54:33.311617	2016-01-20 09:54:33.311617		international-employees-covered	\N	\N
1323	TemplateField	119	Plan Type	1	2016-01-20 09:54:33.322119	2016-01-20 09:54:33.322119		plan-type	t	t
1324	TemplateField	119	Network	2	2016-01-20 09:54:33.332603	2016-01-20 09:54:33.332603		network	t	t
1325	TemplateField	119	Referrals Needed	3	2016-01-20 09:54:33.344694	2016-01-20 09:54:33.344694		referrals-needed	t	t
1326	TemplateField	119	Deductible Individual	4	2016-01-20 09:54:33.35521	2016-01-20 09:54:33.35521		deductible-individual	t	t
1327	TemplateField	119	Deductible Family	5	2016-01-20 09:54:33.365961	2016-01-20 09:54:33.365961		deductible-family	t	t
1328	TemplateField	119	Deductible Format	7	2016-01-20 09:54:33.376371	2016-01-20 09:54:33.376371		deductible-format	\N	t
1329	TemplateField	119	Coinsurance	8	2016-01-20 09:54:33.386921	2016-01-20 09:54:33.386921		coinsurance	t	t
1330	TemplateField	119	Out-of-Pocket Max Individual (includes deductible)	9	2016-01-20 09:54:33.399117	2016-01-20 09:54:33.399117		out-of-pocket-max-individual-includes-deductible	t	t
1331	TemplateField	119	Out-of-Pocket Max Family (includes deductible)	10	2016-01-20 09:54:33.40931	2016-01-20 09:54:33.40931		out-of-pocket-max-family-includes-deductible	t	t
1332	TemplateField	119	In & Out-of-Network Deductibles Cross Accumulation	12	2016-01-20 09:54:33.419603	2016-01-20 09:54:33.419603		in-out-of-network-deductibles-cross-accumulation	\N	t
1333	TemplateField	119	In & Out-of-Network OOP Maximum Cross Accumulation	13	2016-01-20 09:54:33.43008	2016-01-20 09:54:33.43008		in-out-of-network-oop-maximum-cross-accumulation	\N	t
1334	TemplateField	119	OON Reimbursement Level	14	2016-01-20 09:54:33.440526	2016-01-20 09:54:33.440526		oon-reimbursement-level	t	t
1335	TemplateColumn	119	ENTRIES	1	2016-01-20 09:54:33.453328	2016-01-20 09:54:33.453328		entries	\N	\N
1336	TemplateField	120	General Radiology	4	2016-01-20 09:54:33.750642	2016-01-20 09:54:33.750642		general-radiology	\N	\N
1337	TemplateField	120	CT/MRI/PET Scans	5	2016-01-20 09:54:33.761877	2016-01-20 09:54:33.761877		ct-mri-pet-scans	\N	\N
1338	TemplateField	120	Annual Physical Exam	6	2016-01-20 09:54:33.772057	2016-01-20 09:54:33.772057		annual-physical-exam	\N	\N
1340	TemplateField	120	Pediatric Dental	8	2016-01-20 09:54:33.800094	2016-01-20 09:54:33.800094		pediatric-dental	\N	\N
1341	TemplateField	120	Surgery/Anesthesiology	11	2016-01-20 09:54:33.811258	2016-01-20 09:54:33.811258		surgery-anesthesiology	\N	\N
1342	TemplateField	120	Physical Therapy - Inpatient Coverage	18	2016-01-20 09:54:33.821517	2016-01-20 09:54:33.821517		physical-therapy-inpatient-coverage	\N	\N
1343	TemplateField	120	Occupational Therapy - Inpatient Coverage	20	2016-01-20 09:54:33.83206	2016-01-20 09:54:33.83206		occupational-therapy-inpatient-coverage	\N	\N
1344	TemplateField	120	Occupational Therapy - Outpatient Coverage	21	2016-01-20 09:54:33.846589	2016-01-20 09:54:33.846589		occupational-therapy-outpatient-coverage	\N	\N
1345	TemplateField	120	Speech Therapy - Inpatient Coverage	22	2016-01-20 09:54:33.857786	2016-01-20 09:54:33.857786		speech-therapy-inpatient-coverage	\N	\N
1346	TemplateField	120	Speech Therapy - Outpatient Coverage	23	2016-01-20 09:54:33.8687	2016-01-20 09:54:33.8687		speech-therapy-outpatient-coverage	\N	\N
1347	TemplateField	120	Pregnancy & Maternity Care - Office Visits	24	2016-01-20 09:54:33.879589	2016-01-20 09:54:33.879589		pregnancy-maternity-care-office-visits	\N	\N
1348	TemplateField	120	Pregnancy & Maternity Care - Labor & Delivery	25	2016-01-20 09:54:33.890234	2016-01-20 09:54:33.890234		pregnancy-maternity-care-labor-delivery	\N	\N
1349	TemplateField	120	Ambulance	27	2016-01-20 09:54:33.901246	2016-01-20 09:54:33.901246		ambulance	\N	\N
1350	TemplateField	120	Hospice	28	2016-01-20 09:54:33.912256	2016-01-20 09:54:33.912256		hospice	\N	\N
1351	TemplateField	120	Lab	3	2016-01-20 09:54:33.92286	2016-01-20 09:54:33.92286		lab	\N	t
1352	TemplateField	120	Specialist Office Visit	2	2016-01-20 09:54:33.933763	2016-01-20 09:54:33.933763		specialist-office-visit	t	t
1353	TemplateField	120	Hospitalization - Outpatient	10	2016-01-20 09:54:33.944965	2016-01-20 09:54:33.944965		hospitalization-outpatient	\N	t
1354	TemplateField	120	Hospitalization - Inpatient	9	2016-01-20 09:54:33.955782	2016-01-20 09:54:33.955782		hospitalization-inpatient	t	t
1355	TemplateField	120	Urgent Care	13	2016-01-20 09:54:33.966885	2016-01-20 09:54:33.966885		urgent-care	\N	t
1356	TemplateField	120	Emergency Room	12	2016-01-20 09:54:33.977834	2016-01-20 09:54:33.977834		emergency-room	t	t
1357	TemplateField	120	Mental Nervous - Outpatient Coverage	15	2016-01-20 09:54:33.988385	2016-01-20 09:54:33.988385		mental-nervous-outpatient-coverage	\N	t
1358	TemplateField	120	Substance Abuse - Inpatient Coverage	16	2016-01-20 09:54:33.998957	2016-01-20 09:54:33.998957		substance-abuse-inpatient-coverage	\N	t
1359	TemplateField	120	Substance Abuse - Outpatient Coverage	17	2016-01-20 09:54:34.009745	2016-01-20 09:54:34.009745		substance-abuse-outpatient-coverage	\N	t
1360	TemplateField	120	Physical Therapy - Outpatient Coverage	19	2016-01-20 09:54:34.020589	2016-01-20 09:54:34.020589		physical-therapy-outpatient-coverage	\N	t
1361	TemplateField	120	Chiropractic Services	26	2016-01-20 09:54:34.031172	2016-01-20 09:54:34.031172		chiropractic-services	\N	t
1362	TemplateField	120	Home Healthcare	29	2016-01-20 09:54:34.041539	2016-01-20 09:54:34.041539		home-healthcare	\N	\N
1363	TemplateField	120	Skilled Nursing	30	2016-01-20 09:54:34.051947	2016-01-20 09:54:34.051947		skilled-nursing	\N	\N
1365	TemplateField	120	Hearing Devices	34	2016-01-20 09:54:34.074497	2016-01-20 09:54:34.074497		hearing-devices	\N	\N
1366	TemplateField	120	Vision Exams	35	2016-01-20 09:54:34.084976	2016-01-20 09:54:34.084976		vision-exams	\N	\N
1367	TemplateField	120	Short Term Rehabilitation - Inpatient	36	2016-01-20 09:54:34.095398	2016-01-20 09:54:34.095398		short-term-rehabilitation-inpatient	\N	\N
1368	TemplateField	120	Short Term Rehabilitation - Outpatient	37	2016-01-20 09:54:34.106273	2016-01-20 09:54:34.106273		short-term-rehabilitation-outpatient	\N	\N
1369	TemplateField	120	Telemedicine	38	2016-01-20 09:54:34.116887	2016-01-20 09:54:34.116887		telemedicine	\N	\N
1370	TemplateField	120	Specialty Rx Coverage	39	2016-01-20 09:54:34.127354	2016-01-20 09:54:34.127354		specialty-rx-coverage	\N	\N
1371	TemplateField	120	Infertility Coverage	31	2016-01-20 09:54:34.137891	2016-01-20 09:54:34.137891		infertility-coverage	\N	t
1372	TemplateField	120	Durable Medical Equipment	33	2016-01-20 09:54:34.148375	2016-01-20 09:54:34.148375		durable-medical-equipment	\N	t
1373	TemplateField	120	Primary Care Office Visit	1	2016-01-20 09:54:34.16574	2016-01-20 09:54:34.16574		primary-care-office-visit	t	t
1374	TemplateField	120	Mental Nervous - Inpatient Coverage	14	2016-01-20 09:54:34.176635	2016-01-20 09:54:34.176635		mental-nervous-inpatient-coverage	\N	t
1375	TemplateColumn	120	Subject to Deductible & Coinsurance	1	2016-01-20 09:54:34.189874	2016-01-20 09:54:34.189874		subject-to-deductible-coinsurance	\N	\N
1376	TemplateColumn	120	LIMIT	2	2016-01-20 09:54:34.200616	2016-01-20 09:54:34.200616		limit	\N	\N
1377	TemplateColumn	120	SEPERATE DEDUCTIBLE	3	2016-01-20 09:54:34.211109	2016-01-20 09:54:34.211109		seperate-deductible	\N	\N
1378	TemplateColumn	120	SEPERATE COINSURANCE	4	2016-01-20 09:54:34.221299	2016-01-20 09:54:34.221299		seperate-coinsurance	\N	\N
1379	TemplateField	121	Subject to Medical Deductible & Coinsurance	2	2016-01-20 09:54:36.072861	2016-01-20 09:54:36.072861		subject-to-medical-deductible-coinsurance	\N	\N
1380	TemplateField	121	Step Therapy/Precertification Applies	4	2016-01-20 09:54:36.083522	2016-01-20 09:54:36.083522		step-therapy-precertification-applies	\N	\N
1381	TemplateField	121	Mandatory Generic	5	2016-01-20 09:54:36.094004	2016-01-20 09:54:36.094004		mandatory-generic	\N	\N
1382	TemplateField	121	Oral Contraceptive	6	2016-01-20 09:54:36.104528	2016-01-20 09:54:36.104528		oral-contraceptive	\N	\N
1383	TemplateField	121	Are Prescriptions Covered Out of Network	7	2016-01-20 09:54:36.115225	2016-01-20 09:54:36.115225		are-prescriptions-covered-out-of-network	\N	\N
1384	TemplateField	121	Subject to Medical Deductible	1	2016-01-20 09:54:36.127511	2016-01-20 09:54:36.127511		subject-to-medical-deductible	\N	t
1385	TemplateField	121	Network	3	2016-01-20 09:54:36.138341	2016-01-20 09:54:36.138341		network	\N	t
1386	TemplateColumn	121	ENTRIES	1	2016-01-20 09:54:36.150868	2016-01-20 09:54:36.150868		entries	\N	\N
1387	TemplateField	122	Rx Deductible - Family	1	2016-01-20 09:54:36.248485	2016-01-20 09:54:36.248485		rx-deductible-family	t	t
1388	TemplateField	122	Rx Deductible - Individual	2	2016-01-20 09:54:36.259127	2016-01-20 09:54:36.259127		rx-deductible-individual	t	t
1389	TemplateColumn	122	DEDUCTIBLE	1	2016-01-20 09:54:36.271726	2016-01-20 09:54:36.271726		deductible	\N	\N
1390	TemplateColumn	122	PER MEMBER	2	2016-01-20 09:54:36.282152	2016-01-20 09:54:36.282152		per-member	\N	\N
1391	TemplateField	123	Retail Rx (Tier 4)	4	2016-01-20 09:54:36.349766	2016-01-20 09:54:36.349766		retail-rx-tier-4	\N	\N
1392	TemplateField	123	Retail Rx (Tier 5)	5	2016-01-20 09:54:36.360169	2016-01-20 09:54:36.360169		retail-rx-tier-5	\N	\N
1393	TemplateField	123	Retail Rx (Tier 2)	2	2016-01-20 09:54:36.370452	2016-01-20 09:54:36.370452		retail-rx-tier-2	t	t
1394	TemplateField	123	Retail Rx (Tier 1)	1	2016-01-20 09:54:36.381049	2016-01-20 09:54:36.381049		retail-rx-tier-1	t	t
1395	TemplateField	123	Retail Rx (Tier 3)	3	2016-01-20 09:54:36.391853	2016-01-20 09:54:36.391853		retail-rx-tier-3	t	t
1396	TemplateField	123	Mail Order Rx (Tier 4)	9	2016-01-20 09:54:36.402351	2016-01-20 09:54:36.402351		mail-order-rx-tier-4	\N	\N
1397	TemplateField	123	Mail Order Rx (Tier 5)	10	2016-01-20 09:54:36.413358	2016-01-20 09:54:36.413358		mail-order-rx-tier-5	\N	\N
1398	TemplateField	123	Speciality Medications - Mail Order	11	2016-01-20 09:54:36.423577	2016-01-20 09:54:36.423577		speciality-medications-mail-order	\N	\N
1399	TemplateField	123	Speciality Medications - Retail	12	2016-01-20 09:54:36.433974	2016-01-20 09:54:36.433974		speciality-medications-retail	\N	\N
1400	TemplateField	123	Mail Order Rx (Tier 1)	6	2016-01-20 09:54:36.444418	2016-01-20 09:54:36.444418		mail-order-rx-tier-1	\N	t
1401	TemplateField	123	Mail Order Rx (Tier 3)	8	2016-01-20 09:54:36.455012	2016-01-20 09:54:36.455012		mail-order-rx-tier-3	\N	t
1402	TemplateField	123	Mail Order Rx (Tier 2 )	7	2016-01-20 09:54:36.465442	2016-01-20 09:54:36.465442		mail-order-rx-tier-2	\N	t
1403	TemplateColumn	123	SUBJECT TO RX DEDUCTIBLE	1	2016-01-20 09:54:36.478684	2016-01-20 09:54:36.478684		subject-to-rx-deductible	\N	\N
1404	TemplateColumn	123	COINSURANCE	2	2016-01-20 09:54:36.489318	2016-01-20 09:54:36.489318		coinsurance	\N	\N
1405	TemplateField	124	Disease Management	1	2016-01-20 09:54:36.779577	2016-01-20 09:54:36.779577		disease-management	\N	\N
1406	TemplateColumn	124	IDENTIFICATION METHODS	1	2016-01-20 09:54:36.793195	2016-01-20 09:54:36.793195		identification-methods	\N	\N
1407	TemplateColumn	124	NUMBER OF CONDITIONS TRACKED	2	2016-01-20 09:54:36.803697	2016-01-20 09:54:36.803697		number-of-conditions-tracked	\N	\N
1408	TemplateColumn	124	OUTREACH METHODS	3	2016-01-20 09:54:36.813894	2016-01-20 09:54:36.813894		outreach-methods	\N	\N
1409	TemplateColumn	124	REPORT FREQUENCY	4	2016-01-20 09:54:36.824111	2016-01-20 09:54:36.824111		report-frequency	\N	\N
1410	TemplateField	125	Integrated Wellness Plan	1	2016-01-20 09:54:36.888104	2016-01-20 09:54:36.888104		integrated-wellness-plan	\N	\N
1411	TemplateColumn	125	ENTRIES	1	2016-01-20 09:54:36.900081	2016-01-20 09:54:36.900081		entries	\N	\N
1412	TemplateField	126	Referrals Needed	5	2016-01-20 10:10:20.97573	2016-01-20 10:10:20.97573		referrals-needed	t	t
1413	TemplateField	126	Coinsurance	15	2016-01-20 10:10:20.991966	2016-01-20 10:10:20.991966		coinsurance	t	t
1414	TemplateField	126	Deductible Individual	7	2016-01-20 10:10:21.002794	2016-01-20 10:10:21.002794		deductible-individual	t	t
1415	TemplateField	126	Plan Type	1	2016-01-20 10:10:21.013932	2016-01-20 10:10:21.013932		plan-type	t	t
1416	TemplateField	126	Deductible Family	9	2016-01-20 10:10:21.024391	2016-01-20 10:10:21.024391		deductible-family	t	t
1417	TemplateField	126	Deductible - Family Multiple	11	2016-01-20 10:10:21.03646	2016-01-20 10:10:21.03646		deductible-family-multiple	\N	\N
1418	TemplateField	126	Gym Reimbursement	31	2016-01-20 10:10:21.049567	2016-01-20 10:10:21.049567		gym-reimbursement	\N	\N
1419	TemplateField	126	Out-of-Pocket Max Family (includes deductible)	19	2016-01-20 10:10:21.061025	2016-01-20 10:10:21.061025		out-of-pocket-max-family-includes-deductible	t	t
1420	TemplateField	126	Network	3	2016-01-20 10:10:21.072467	2016-01-20 10:10:21.072467		network	t	t
1421	TemplateField	126	Deductible Format	13	2016-01-20 10:10:21.084288	2016-01-20 10:10:21.084288		deductible-format	\N	t
1422	TemplateField	126	Annual Maximum	29	2016-01-20 10:10:21.099626	2016-01-20 10:10:21.099626		annual-maximum	\N	\N
1423	TemplateField	126	International Employees Covered	37	2016-01-20 10:10:21.119473	2016-01-20 10:10:21.119473		international-employees-covered	\N	\N
1424	TemplateField	126	Nurseline	35	2016-01-20 10:10:21.130486	2016-01-20 10:10:21.130486		nurseline	\N	\N
1540	TemplateField	134	Gym Reimbursement	15	2016-01-20 10:10:27.511308	2016-01-20 10:10:27.511308		gym-reimbursement	\N	\N
1425	TemplateField	126	In & Out-of-Network Deductibles Cross Accumulation	23	2016-01-20 10:10:21.142831	2016-01-20 10:10:21.142831		in-out-of-network-deductibles-cross-accumulation	\N	t
1426	TemplateField	126	OON Reimbursement Level	27	2016-01-20 10:10:21.153843	2016-01-20 10:10:21.153843		oon-reimbursement-level	t	t
1427	TemplateField	126	Customer Service Days/Hours	33	2016-01-20 10:10:21.164999	2016-01-20 10:10:21.164999		customer-service-days-hours	\N	\N
1428	TemplateField	126	Out-of-Pocket Max Individual (includes deductible)	17	2016-01-20 10:10:21.175779	2016-01-20 10:10:21.175779		out-of-pocket-max-individual-includes-deductible	t	t
1429	TemplateField	126	Out-of-Pocket Maximum - Family Multiple	21	2016-01-20 10:10:21.189726	2016-01-20 10:10:21.189726		out-of-pocket-maximum-family-multiple	\N	f
1430	TemplateField	126	In & Out-of-Network OOP Maximum Cross Accumulation	25	2016-01-20 10:10:21.200985	2016-01-20 10:10:21.200985		in-out-of-network-oop-maximum-cross-accumulation	\N	t
1431	TemplateColumn	126	ENTRIES	1	2016-01-20 10:10:21.220528	2016-01-20 10:10:21.220528		entries	\N	\N
1432	TemplateField	127	General Radiology	4	2016-01-20 10:10:21.495856	2016-01-20 10:10:21.495856		general-radiology	\N	\N
1433	TemplateField	127	CT/MRI/PET Scans	5	2016-01-20 10:10:21.506936	2016-01-20 10:10:21.506936		ct-mri-pet-scans	\N	\N
1434	TemplateField	127	Annual Physical Exam	6	2016-01-20 10:10:21.520929	2016-01-20 10:10:21.520929		annual-physical-exam	\N	\N
1435	TemplateField	127	Well Child Exams	7	2016-01-20 10:10:21.532783	2016-01-20 10:10:21.532783		well-child-exams	\N	\N
1436	TemplateField	127	Pediatric Dental	8	2016-01-20 10:10:21.544455	2016-01-20 10:10:21.544455		pediatric-dental	\N	\N
1437	TemplateField	127	Surgery/Anesthesiology	11	2016-01-20 10:10:21.557936	2016-01-20 10:10:21.557936		surgery-anesthesiology	\N	\N
1438	TemplateField	127	Lab	3	2016-01-20 10:10:21.569254	2016-01-20 10:10:21.569254		lab	\N	t
1439	TemplateField	127	Primary Care Office Visit	1	2016-01-20 10:10:21.580681	2016-01-20 10:10:21.580681		primary-care-office-visit	t	t
1440	TemplateField	127	Specialist Office Visit	2	2016-01-20 10:10:21.593248	2016-01-20 10:10:21.593248		specialist-office-visit	t	t
1441	TemplateField	127	Hospitalization - Outpatient	10	2016-01-20 10:10:21.605334	2016-01-20 10:10:21.605334		hospitalization-outpatient	\N	t
1442	TemplateField	127	Hospitalization - Inpatient	9	2016-01-20 10:10:21.617024	2016-01-20 10:10:21.617024		hospitalization-inpatient	t	t
1443	TemplateField	127	Urgent Care	13	2016-01-20 10:10:21.628862	2016-01-20 10:10:21.628862		urgent-care	\N	t
1444	TemplateField	127	Emergency Room	12	2016-01-20 10:10:21.64023	2016-01-20 10:10:21.64023		emergency-room	t	t
1445	TemplateField	127	Mental Nervous - Inpatient Coverage	14	2016-01-20 10:10:21.651567	2016-01-20 10:10:21.651567		mental-nervous-inpatient-coverage	\N	t
1446	TemplateField	127	Mental Nervous - Outpatient Coverage	15	2016-01-20 10:10:21.664013	2016-01-20 10:10:21.664013		mental-nervous-outpatient-coverage	\N	t
1447	TemplateField	127	Physical Therapy - Inpatient Coverage	18	2016-01-20 10:10:21.675491	2016-01-20 10:10:21.675491		physical-therapy-inpatient-coverage	\N	\N
1448	TemplateField	127	Occupational Therapy - Inpatient Coverage	20	2016-01-20 10:10:21.686474	2016-01-20 10:10:21.686474		occupational-therapy-inpatient-coverage	\N	\N
1449	TemplateField	127	Occupational Therapy - Outpatient Coverage	21	2016-01-20 10:10:21.697151	2016-01-20 10:10:21.697151		occupational-therapy-outpatient-coverage	\N	\N
1450	TemplateField	127	Speech Therapy - Inpatient Coverage	22	2016-01-20 10:10:21.708297	2016-01-20 10:10:21.708297		speech-therapy-inpatient-coverage	\N	\N
1451	TemplateField	127	Speech Therapy - Outpatient Coverage	23	2016-01-20 10:10:21.719305	2016-01-20 10:10:21.719305		speech-therapy-outpatient-coverage	\N	\N
1452	TemplateField	127	Pregnancy & Maternity Care - Office Visits	24	2016-01-20 10:10:21.730507	2016-01-20 10:10:21.730507		pregnancy-maternity-care-office-visits	\N	\N
1453	TemplateField	127	Pregnancy & Maternity Care - Labor & Delivery	25	2016-01-20 10:10:21.741596	2016-01-20 10:10:21.741596		pregnancy-maternity-care-labor-delivery	\N	\N
1454	TemplateField	127	Ambulance	27	2016-01-20 10:10:21.752246	2016-01-20 10:10:21.752246		ambulance	\N	\N
1455	TemplateField	127	Hospice	28	2016-01-20 10:10:21.76328	2016-01-20 10:10:21.76328		hospice	\N	\N
1456	TemplateField	127	Home Healthcare	29	2016-01-20 10:10:21.775624	2016-01-20 10:10:21.775624		home-healthcare	\N	\N
1457	TemplateField	127	Skilled Nursing	30	2016-01-20 10:10:21.786659	2016-01-20 10:10:21.786659		skilled-nursing	\N	\N
1458	TemplateField	127	Prosthetics	32	2016-01-20 10:10:21.797487	2016-01-20 10:10:21.797487		prosthetics	\N	\N
1459	TemplateField	127	Hearing Devices	34	2016-01-20 10:10:21.808037	2016-01-20 10:10:21.808037		hearing-devices	\N	\N
1460	TemplateField	127	Vision Exams	35	2016-01-20 10:10:21.820358	2016-01-20 10:10:21.820358		vision-exams	\N	\N
1461	TemplateField	127	Short Term Rehabilitation- Inpatient	36	2016-01-20 10:10:21.834274	2016-01-20 10:10:21.834274		short-term-rehabilitation-inpatient	\N	\N
1462	TemplateField	127	Short Term Rehabilitation - Outpatient	37	2016-01-20 10:10:21.855349	2016-01-20 10:10:21.855349		short-term-rehabilitation-outpatient	\N	\N
1463	TemplateField	127	Telemedicine	38	2016-01-20 10:10:21.866532	2016-01-20 10:10:21.866532		telemedicine	\N	\N
1464	TemplateField	127	Speciality Rx Coverage	39	2016-01-20 10:10:21.877713	2016-01-20 10:10:21.877713		speciality-rx-coverage	\N	\N
1465	TemplateField	127	Substance Abuse - Inpatient Coverage	16	2016-01-20 10:10:21.888986	2016-01-20 10:10:21.888986		substance-abuse-inpatient-coverage	\N	t
1466	TemplateField	127	Chiropractic Services	26	2016-01-20 10:10:21.900186	2016-01-20 10:10:21.900186		chiropractic-services	\N	t
1467	TemplateField	127	Physical Therapy - Outpatient Coverage	19	2016-01-20 10:10:21.911379	2016-01-20 10:10:21.911379		physical-therapy-outpatient-coverage	\N	t
1468	TemplateField	127	Durable Medical Equipment	33	2016-01-20 10:10:21.921985	2016-01-20 10:10:21.921985		durable-medical-equipment	\N	t
1469	TemplateField	127	Infertility Coverage	31	2016-01-20 10:10:21.933579	2016-01-20 10:10:21.933579		infertility-coverage	\N	t
1470	TemplateField	127	Substance Abuse - Outpatient Coverage	17	2016-01-20 10:10:21.944455	2016-01-20 10:10:21.944455		substance-abuse-outpatient-coverage	\N	t
1471	TemplateColumn	127	Subject to Deductible & Coinsurance	1	2016-01-20 10:10:21.958839	2016-01-20 10:10:21.958839		subject-to-deductible-coinsurance	\N	\N
1472	TemplateColumn	127	COPAY	2	2016-01-20 10:10:21.970701	2016-01-20 10:10:21.970701		copay	\N	\N
1473	TemplateColumn	127	LIMIT	3	2016-01-20 10:10:21.982815	2016-01-20 10:10:21.982815		limit	\N	\N
1474	TemplateColumn	127	COPAY LIMIT	4	2016-01-20 10:10:21.994224	2016-01-20 10:10:21.994224		copay-limit	\N	\N
1475	TemplateColumn	127	SEPERATE DEDUCTIBLE	5	2016-01-20 10:10:22.005883	2016-01-20 10:10:22.005883		seperate-deductible	\N	\N
1476	TemplateColumn	127	Separate Coinsurance	6	2016-01-20 10:10:22.017329	2016-01-20 10:10:22.017329		separate-coinsurance	\N	\N
1477	TemplateField	128	Subject to Medical Deductible & Coinsurance	2	2016-01-20 10:10:25.193711	2016-01-20 10:10:25.193711		subject-to-medical-deductible-coinsurance	\N	\N
1478	TemplateField	128	Mandatory Generic	4	2016-01-20 10:10:25.204148	2016-01-20 10:10:25.204148		mandatory-generic	\N	\N
1479	TemplateField	128	Step Therapy/Precertification Applies	5	2016-01-20 10:10:25.215005	2016-01-20 10:10:25.215005		step-therapy-precertification-applies	\N	\N
1480	TemplateField	128	Oral Contraceptive	6	2016-01-20 10:10:25.225456	2016-01-20 10:10:25.225456		oral-contraceptive	\N	\N
1599	TemplateField	136	Telemedicine	38	2016-01-20 10:10:28.620014	2016-01-20 10:10:28.620014		telemedicine	\N	\N
1481	TemplateField	128	Subject to Medical Deductible	1	2016-01-20 10:10:25.236141	2016-01-20 10:10:25.236141		subject-to-medical-deductible	\N	t
1482	TemplateField	128	Network	3	2016-01-20 10:10:25.246895	2016-01-20 10:10:25.246895		network	\N	t
1483	TemplateField	128	Prescriptions Covered Out of Network	7	2016-01-20 10:10:25.25751	2016-01-20 10:10:25.25751		prescriptions-covered-out-of-network	\N	\N
1484	TemplateColumn	128	ENTRIES	1	2016-01-20 10:10:25.270421	2016-01-20 10:10:25.270421		entries	\N	\N
1485	TemplateField	129	Rx Deductible - Individual	1	2016-01-20 10:10:25.366438	2016-01-20 10:10:25.366438		rx-deductible-individual	t	t
1486	TemplateField	129	Rx Deductible - Family	2	2016-01-20 10:10:25.377446	2016-01-20 10:10:25.377446		rx-deductible-family	t	t
1487	TemplateColumn	129	DEDUCTIBLE	1	2016-01-20 10:10:25.390284	2016-01-20 10:10:25.390284		deductible	\N	\N
1488	TemplateColumn	129	PER MEMBER	2	2016-01-20 10:10:25.400556	2016-01-20 10:10:25.400556		per-member	\N	\N
1489	TemplateField	130	Specialty Medications  -Retail	61	2016-01-20 10:10:25.469028	2016-01-20 10:10:25.469028		specialty-medications-retail	\N	\N
1490	TemplateField	130	Retail Rx (Tier 1)	1	2016-01-20 10:10:25.479364	2016-01-20 10:10:25.479364		retail-rx-tier-1	t	t
1491	TemplateField	130	Retail Rx (Tier 3)	13	2016-01-20 10:10:25.490261	2016-01-20 10:10:25.490261		retail-rx-tier-3	t	t
1492	TemplateField	130	Mail Order Rx (Tier 1)	31	2016-01-20 10:10:25.501403	2016-01-20 10:10:25.501403		mail-order-rx-tier-1	\N	t
1493	TemplateField	130	Retail Rx (Tier 4)	19	2016-01-20 10:10:25.512263	2016-01-20 10:10:25.512263		retail-rx-tier-4	\N	\N
1494	TemplateField	130	Specialty Medications -Mail Order	67	2016-01-20 10:10:25.523883	2016-01-20 10:10:25.523883		specialty-medications-mail-order	\N	f
1495	TemplateField	130	Retail Rx (Tier 5)	25	2016-01-20 10:10:25.534953	2016-01-20 10:10:25.534953		retail-rx-tier-5	\N	\N
1496	TemplateField	130	Retail Rx (Tier 2)	7	2016-01-20 10:10:25.54751	2016-01-20 10:10:25.54751		retail-rx-tier-2	t	t
1497	TemplateField	130	Mail Order Rx (Tier 5)	55	2016-01-20 10:10:25.558926	2016-01-20 10:10:25.558926		mail-order-rx-tier-5	\N	\N
1498	TemplateField	130	Mail Order Rx (Tier 4)	49	2016-01-20 10:10:25.57204	2016-01-20 10:10:25.57204		mail-order-rx-tier-4	\N	\N
1499	TemplateField	130	Mail Order Rx (Tier 3)	43	2016-01-20 10:10:25.582941	2016-01-20 10:10:25.582941		mail-order-rx-tier-3	\N	t
1500	TemplateField	130	Mail Order Rx (Tier 2)	37	2016-01-20 10:10:25.594189	2016-01-20 10:10:25.594189		mail-order-rx-tier-2	\N	t
1501	TemplateColumn	130	SUBJECT TO RX DEDUCTIBLE	1	2016-01-20 10:10:25.607513	2016-01-20 10:10:25.607513		subject-to-rx-deductible	\N	\N
1502	TemplateColumn	130	COPAY	2	2016-01-20 10:10:25.618158	2016-01-20 10:10:25.618158		copay	\N	\N
1503	TemplateColumn	130	COINSURANCE	3	2016-01-20 10:10:25.62908	2016-01-20 10:10:25.62908		coinsurance	\N	\N
1504	TemplateColumn	130	PER SCRIPT COINSURANCE LIMIT	4	2016-01-20 10:10:25.63971	2016-01-20 10:10:25.63971		per-script-coinsurance-limit	\N	\N
1505	TemplateColumn	130	PER SCRIPT COINSURANCE MINIMUM	5	2016-01-20 10:10:25.652387	2016-01-20 10:10:25.652387		per-script-coinsurance-minimum	\N	\N
1506	TemplateField	131	Disease Management	1	2016-01-20 10:10:26.621066	2016-01-20 10:10:26.621066		disease-management	\N	\N
1507	TemplateColumn	131	IDENTIFICATION METHODS	1	2016-01-20 10:10:26.634421	2016-01-20 10:10:26.634421		identification-methods	\N	\N
1508	TemplateColumn	131	NUMBER OF CONDITIONS TRACKED	2	2016-01-20 10:10:26.646549	2016-01-20 10:10:26.646549		number-of-conditions-tracked	\N	\N
1509	TemplateColumn	131	OUTREACH METHODS	3	2016-01-20 10:10:26.657824	2016-01-20 10:10:26.657824		outreach-methods	\N	\N
1510	TemplateColumn	131	REPORT FREQUENCY	4	2016-01-20 10:10:26.668899	2016-01-20 10:10:26.668899		report-frequency	\N	\N
1511	TemplateField	132	Integrated Wellness Plan	1	2016-01-20 10:10:26.739643	2016-01-20 10:10:26.739643		integrated-wellness-plan	\N	\N
1512	TemplateColumn	132	Entries	1	2016-01-20 10:10:26.752772	2016-01-20 10:10:26.752772		entries	\N	\N
1513	TemplateField	133	Employee Eligibility Class #1	1	2016-01-20 10:10:26.785592	2016-01-20 10:10:26.785592		employee-eligibility-class-1	\N	\N
1514	TemplateField	133	Employee Eligibility Class #2	2	2016-01-20 10:10:26.796427	2016-01-20 10:10:26.796427		employee-eligibility-class-2	\N	\N
1515	TemplateField	133	Employee Eligibility Class #3	3	2016-01-20 10:10:26.807195	2016-01-20 10:10:26.807195		employee-eligibility-class-3	\N	\N
1516	TemplateField	133	Employee Eligibility Class #4	4	2016-01-20 10:10:26.817596	2016-01-20 10:10:26.817596		employee-eligibility-class-4	\N	\N
1517	TemplateColumn	133	Employee Class	1	2016-01-20 10:10:26.830365	2016-01-20 10:10:26.830365		employee-class	\N	\N
1518	TemplateColumn	133	Employee Classes	2	2016-01-20 10:10:26.841012	2016-01-20 10:10:26.841012		employee-classes	\N	\N
1519	TemplateColumn	133	Waiting Period	3	2016-01-20 10:10:26.852037	2016-01-20 10:10:26.852037		waiting-period	\N	\N
1520	TemplateColumn	133	Hours Requirement	4	2016-01-20 10:10:26.862729	2016-01-20 10:10:26.862729		hours-requirement	\N	\N
1521	TemplateColumn	133	Spouse Eligible	5	2016-01-20 10:10:26.873389	2016-01-20 10:10:26.873389		spouse-eligible	\N	\N
1522	TemplateColumn	133	Dependent Children Eligible	6	2016-01-20 10:10:26.883965	2016-01-20 10:10:26.883965		dependent-children-eligible	\N	\N
1523	TemplateColumn	133	Domestic Partner Eligible	7	2016-01-20 10:10:26.894442	2016-01-20 10:10:26.894442		domestic-partner-eligible	\N	\N
1524	TemplateColumn	133	Dependent Child Limiting Age	8	2016-01-20 10:10:26.90615	2016-01-20 10:10:26.90615		dependent-child-limiting-age	\N	\N
1525	TemplateColumn	133	Dependent Child Coverage Ends	9	2016-01-20 10:10:26.916586	2016-01-20 10:10:26.916586		dependent-child-coverage-ends	\N	\N
1526	TemplateField	134	Plan Type	1	2016-01-20 10:10:27.361458	2016-01-20 10:10:27.361458		plan-type	\N	\N
1527	TemplateField	134	Network	2	2016-01-20 10:10:27.372382	2016-01-20 10:10:27.372382		network	\N	\N
1528	TemplateField	134	Referrals Needed	3	2016-01-20 10:10:27.383186	2016-01-20 10:10:27.383186		referrals-needed	\N	\N
1529	TemplateField	134	Deductible Individual	4	2016-01-20 10:10:27.393769	2016-01-20 10:10:27.393769		deductible-individual	\N	\N
1530	TemplateField	134	Deductible Family	5	2016-01-20 10:10:27.404171	2016-01-20 10:10:27.404171		deductible-family	\N	\N
1531	TemplateField	134	Deductible - Family Multiple	6	2016-01-20 10:10:27.414532	2016-01-20 10:10:27.414532		deductible-family-multiple	\N	\N
1532	TemplateField	134	Deductible Format	7	2016-01-20 10:10:27.425127	2016-01-20 10:10:27.425127		deductible-format	\N	\N
1533	TemplateField	134	Coinsurance	8	2016-01-20 10:10:27.436215	2016-01-20 10:10:27.436215		coinsurance	\N	\N
1534	TemplateField	134	Out-of-Pocket Max Individual (includes deductible)	9	2016-01-20 10:10:27.447341	2016-01-20 10:10:27.447341		out-of-pocket-max-individual-includes-deductible	\N	\N
1535	TemplateField	134	Out-of-Pocket Max Family (includes deductible)	10	2016-01-20 10:10:27.458093	2016-01-20 10:10:27.458093		out-of-pocket-max-family-includes-deductible	\N	\N
1536	TemplateField	134	Out-of-Pocket Maximum - Family Multiple	11	2016-01-20 10:10:27.468251	2016-01-20 10:10:27.468251		out-of-pocket-maximum-family-multiple	\N	\N
1537	TemplateField	134	In & Out-of-Network Deductibles Cross Accumulation	12	2016-01-20 10:10:27.479432	2016-01-20 10:10:27.479432		in-out-of-network-deductibles-cross-accumulation	\N	\N
1538	TemplateField	134	In & Out-of-Network OOP Maximum Cross Accumulation	13	2016-01-20 10:10:27.490147	2016-01-20 10:10:27.490147		in-out-of-network-oop-maximum-cross-accumulation	\N	\N
1539	TemplateField	134	Annual Maximum	14	2016-01-20 10:10:27.501098	2016-01-20 10:10:27.501098		annual-maximum	\N	\N
1541	TemplateField	134	Customer Service Days/Hours	16	2016-01-20 10:10:27.521923	2016-01-20 10:10:27.521923		customer-service-days-hours	\N	\N
1542	TemplateField	134	OON Reimbursement Level	17	2016-01-20 10:10:27.532457	2016-01-20 10:10:27.532457		oon-reimbursement-level	\N	\N
1543	TemplateField	134	Nurseline	18	2016-01-20 10:10:27.543442	2016-01-20 10:10:27.543442		nurseline	\N	\N
1544	TemplateField	134	International Employees Covered	19	2016-01-20 10:10:27.554276	2016-01-20 10:10:27.554276		international-employees-covered	\N	\N
1545	TemplateColumn	134	ENTRIES	1	2016-01-20 10:10:27.567166	2016-01-20 10:10:27.567166		entries	\N	\N
1546	TemplateField	135	Deductible - Family Multiple	6	2016-01-20 10:10:27.789121	2016-01-20 10:10:27.789121		deductible-family-multiple	\N	\N
1547	TemplateField	135	Out-of-Pocket Maximum - Family Multiple	11	2016-01-20 10:10:27.799822	2016-01-20 10:10:27.799822		out-of-pocket-maximum-family-multiple	\N	\N
1548	TemplateField	135	Annual Maximum	15	2016-01-20 10:10:27.81428	2016-01-20 10:10:27.81428		annual-maximum	\N	\N
1549	TemplateField	135	Gym Reimbursement	16	2016-01-20 10:10:27.826193	2016-01-20 10:10:27.826193		gym-reimbursement	\N	\N
1550	TemplateField	135	Customer Service Days/Hours	17	2016-01-20 10:10:27.837962	2016-01-20 10:10:27.837962		customer-service-days-hours	\N	\N
1551	TemplateField	135	Nurseline	18	2016-01-20 10:10:27.849177	2016-01-20 10:10:27.849177		nurseline	\N	\N
1552	TemplateField	135	International Employees Covered	19	2016-01-20 10:10:27.860268	2016-01-20 10:10:27.860268		international-employees-covered	\N	\N
1553	TemplateField	135	Plan Type	1	2016-01-20 10:10:27.871206	2016-01-20 10:10:27.871206		plan-type	t	t
1554	TemplateField	135	Network	2	2016-01-20 10:10:27.882623	2016-01-20 10:10:27.882623		network	t	t
1555	TemplateField	135	Referrals Needed	3	2016-01-20 10:10:27.893396	2016-01-20 10:10:27.893396		referrals-needed	t	t
1556	TemplateField	135	Deductible Individual	4	2016-01-20 10:10:27.904282	2016-01-20 10:10:27.904282		deductible-individual	t	t
1557	TemplateField	135	Deductible Family	5	2016-01-20 10:10:27.915392	2016-01-20 10:10:27.915392		deductible-family	t	t
1558	TemplateField	135	Deductible Format	7	2016-01-20 10:10:27.926199	2016-01-20 10:10:27.926199		deductible-format	\N	t
1559	TemplateField	135	Coinsurance	8	2016-01-20 10:10:27.936783	2016-01-20 10:10:27.936783		coinsurance	t	t
1560	TemplateField	135	Out-of-Pocket Max Individual (includes deductible)	9	2016-01-20 10:10:27.947389	2016-01-20 10:10:27.947389		out-of-pocket-max-individual-includes-deductible	t	t
1561	TemplateField	135	Out-of-Pocket Max Family (includes deductible)	10	2016-01-20 10:10:27.959006	2016-01-20 10:10:27.959006		out-of-pocket-max-family-includes-deductible	t	t
1562	TemplateField	135	In & Out-of-Network Deductibles Cross Accumulation	12	2016-01-20 10:10:27.969947	2016-01-20 10:10:27.969947		in-out-of-network-deductibles-cross-accumulation	\N	t
1563	TemplateField	135	In & Out-of-Network OOP Maximum Cross Accumulation	13	2016-01-20 10:10:27.98078	2016-01-20 10:10:27.98078		in-out-of-network-oop-maximum-cross-accumulation	\N	t
1564	TemplateField	135	OON Reimbursement Level	14	2016-01-20 10:10:27.992582	2016-01-20 10:10:27.992582		oon-reimbursement-level	t	t
1565	TemplateColumn	135	ENTRIES	1	2016-01-20 10:10:28.005249	2016-01-20 10:10:28.005249		entries	\N	\N
1566	TemplateField	136	General Radiology	4	2016-01-20 10:10:28.262458	2016-01-20 10:10:28.262458		general-radiology	\N	\N
1567	TemplateField	136	CT/MRI/PET Scans	5	2016-01-20 10:10:28.27453	2016-01-20 10:10:28.27453		ct-mri-pet-scans	\N	\N
1568	TemplateField	136	Annual Physical Exam	6	2016-01-20 10:10:28.285326	2016-01-20 10:10:28.285326		annual-physical-exam	\N	\N
1569	TemplateField	136	Well Child Exams	7	2016-01-20 10:10:28.296297	2016-01-20 10:10:28.296297		well-child-exams	\N	\N
1570	TemplateField	136	Pediatric Dental	8	2016-01-20 10:10:28.307067	2016-01-20 10:10:28.307067		pediatric-dental	\N	\N
1571	TemplateField	136	Surgery/Anesthesiology	11	2016-01-20 10:10:28.317713	2016-01-20 10:10:28.317713		surgery-anesthesiology	\N	\N
1572	TemplateField	136	Physical Therapy - Inpatient Coverage	18	2016-01-20 10:10:28.328526	2016-01-20 10:10:28.328526		physical-therapy-inpatient-coverage	\N	\N
1573	TemplateField	136	Occupational Therapy - Inpatient Coverage	20	2016-01-20 10:10:28.339503	2016-01-20 10:10:28.339503		occupational-therapy-inpatient-coverage	\N	\N
1574	TemplateField	136	Occupational Therapy - Outpatient Coverage	21	2016-01-20 10:10:28.350794	2016-01-20 10:10:28.350794		occupational-therapy-outpatient-coverage	\N	\N
1575	TemplateField	136	Speech Therapy - Inpatient Coverage	22	2016-01-20 10:10:28.362015	2016-01-20 10:10:28.362015		speech-therapy-inpatient-coverage	\N	\N
1576	TemplateField	136	Speech Therapy - Outpatient Coverage	23	2016-01-20 10:10:28.37282	2016-01-20 10:10:28.37282		speech-therapy-outpatient-coverage	\N	\N
1577	TemplateField	136	Pregnancy & Maternity Care - Office Visits	24	2016-01-20 10:10:28.383839	2016-01-20 10:10:28.383839		pregnancy-maternity-care-office-visits	\N	\N
1578	TemplateField	136	Pregnancy & Maternity Care - Labor & Delivery	25	2016-01-20 10:10:28.394187	2016-01-20 10:10:28.394187		pregnancy-maternity-care-labor-delivery	\N	\N
1579	TemplateField	136	Ambulance	27	2016-01-20 10:10:28.404941	2016-01-20 10:10:28.404941		ambulance	\N	\N
1580	TemplateField	136	Hospice	28	2016-01-20 10:10:28.415905	2016-01-20 10:10:28.415905		hospice	\N	\N
1581	TemplateField	136	Lab	3	2016-01-20 10:10:28.426751	2016-01-20 10:10:28.426751		lab	\N	t
1582	TemplateField	136	Specialist Office Visit	2	2016-01-20 10:10:28.437721	2016-01-20 10:10:28.437721		specialist-office-visit	t	t
1583	TemplateField	136	Hospitalization - Outpatient	10	2016-01-20 10:10:28.448312	2016-01-20 10:10:28.448312		hospitalization-outpatient	\N	t
1584	TemplateField	136	Hospitalization - Inpatient	9	2016-01-20 10:10:28.458921	2016-01-20 10:10:28.458921		hospitalization-inpatient	t	t
1585	TemplateField	136	Urgent Care	13	2016-01-20 10:10:28.469824	2016-01-20 10:10:28.469824		urgent-care	\N	t
1586	TemplateField	136	Emergency Room	12	2016-01-20 10:10:28.48037	2016-01-20 10:10:28.48037		emergency-room	t	t
1587	TemplateField	136	Mental Nervous - Outpatient Coverage	15	2016-01-20 10:10:28.492847	2016-01-20 10:10:28.492847		mental-nervous-outpatient-coverage	\N	t
1588	TemplateField	136	Substance Abuse - Inpatient Coverage	16	2016-01-20 10:10:28.503644	2016-01-20 10:10:28.503644		substance-abuse-inpatient-coverage	\N	t
1589	TemplateField	136	Substance Abuse - Outpatient Coverage	17	2016-01-20 10:10:28.514145	2016-01-20 10:10:28.514145		substance-abuse-outpatient-coverage	\N	t
1590	TemplateField	136	Physical Therapy - Outpatient Coverage	19	2016-01-20 10:10:28.524401	2016-01-20 10:10:28.524401		physical-therapy-outpatient-coverage	\N	t
1591	TemplateField	136	Chiropractic Services	26	2016-01-20 10:10:28.534977	2016-01-20 10:10:28.534977		chiropractic-services	\N	t
1592	TemplateField	136	Home Healthcare	29	2016-01-20 10:10:28.545651	2016-01-20 10:10:28.545651		home-healthcare	\N	\N
1593	TemplateField	136	Skilled Nursing	30	2016-01-20 10:10:28.556057	2016-01-20 10:10:28.556057		skilled-nursing	\N	\N
1594	TemplateField	136	Prosthetics	32	2016-01-20 10:10:28.568721	2016-01-20 10:10:28.568721		prosthetics	\N	\N
1595	TemplateField	136	Hearing Devices	34	2016-01-20 10:10:28.579343	2016-01-20 10:10:28.579343		hearing-devices	\N	\N
1596	TemplateField	136	Vision Exams	35	2016-01-20 10:10:28.589526	2016-01-20 10:10:28.589526		vision-exams	\N	\N
1597	TemplateField	136	Short Term Rehabilitation - Inpatient	36	2016-01-20 10:10:28.599928	2016-01-20 10:10:28.599928		short-term-rehabilitation-inpatient	\N	\N
1598	TemplateField	136	Short Term Rehabilitation - Outpatient	37	2016-01-20 10:10:28.610082	2016-01-20 10:10:28.610082		short-term-rehabilitation-outpatient	\N	\N
1600	TemplateField	136	Specialty Rx Coverage	39	2016-01-20 10:10:28.631093	2016-01-20 10:10:28.631093		specialty-rx-coverage	\N	\N
1601	TemplateField	136	Infertility Coverage	31	2016-01-20 10:10:28.641254	2016-01-20 10:10:28.641254		infertility-coverage	\N	t
1602	TemplateField	136	Durable Medical Equipment	33	2016-01-20 10:10:28.65152	2016-01-20 10:10:28.65152		durable-medical-equipment	\N	t
1603	TemplateField	136	Primary Care Office Visit	1	2016-01-20 10:10:28.685319	2016-01-20 10:10:28.685319		primary-care-office-visit	t	t
1604	TemplateField	136	Mental Nervous - Inpatient Coverage	14	2016-01-20 10:10:28.701594	2016-01-20 10:10:28.701594		mental-nervous-inpatient-coverage	\N	t
1605	TemplateColumn	136	Subject to Deductible & Coinsurance	1	2016-01-20 10:10:28.715715	2016-01-20 10:10:28.715715		subject-to-deductible-coinsurance	\N	\N
1606	TemplateColumn	136	LIMIT	2	2016-01-20 10:10:28.726852	2016-01-20 10:10:28.726852		limit	\N	\N
1607	TemplateColumn	136	SEPERATE DEDUCTIBLE	3	2016-01-20 10:10:28.73799	2016-01-20 10:10:28.73799		seperate-deductible	\N	\N
1608	TemplateColumn	136	SEPERATE COINSURANCE	4	2016-01-20 10:10:28.74902	2016-01-20 10:10:28.74902		seperate-coinsurance	\N	\N
1609	TemplateField	137	Subject to Medical Deductible & Coinsurance	2	2016-01-20 10:10:30.639291	2016-01-20 10:10:30.639291		subject-to-medical-deductible-coinsurance	\N	\N
1610	TemplateField	137	Step Therapy/Precertification Applies	4	2016-01-20 10:10:30.653236	2016-01-20 10:10:30.653236		step-therapy-precertification-applies	\N	\N
1611	TemplateField	137	Mandatory Generic	5	2016-01-20 10:10:30.664462	2016-01-20 10:10:30.664462		mandatory-generic	\N	\N
1612	TemplateField	137	Oral Contraceptive	6	2016-01-20 10:10:30.67587	2016-01-20 10:10:30.67587		oral-contraceptive	\N	\N
1613	TemplateField	137	Are Prescriptions Covered Out of Network	7	2016-01-20 10:10:30.687508	2016-01-20 10:10:30.687508		are-prescriptions-covered-out-of-network	\N	\N
1614	TemplateField	137	Subject to Medical Deductible	1	2016-01-20 10:10:30.69918	2016-01-20 10:10:30.69918		subject-to-medical-deductible	\N	t
1615	TemplateField	137	Network	3	2016-01-20 10:10:30.710799	2016-01-20 10:10:30.710799		network	\N	t
1616	TemplateColumn	137	ENTRIES	1	2016-01-20 10:10:30.724192	2016-01-20 10:10:30.724192		entries	\N	\N
1617	TemplateField	138	Rx Deductible - Family	1	2016-01-20 10:10:30.825147	2016-01-20 10:10:30.825147		rx-deductible-family	t	t
1618	TemplateField	138	Rx Deductible - Individual	2	2016-01-20 10:10:30.836272	2016-01-20 10:10:30.836272		rx-deductible-individual	t	t
1619	TemplateColumn	138	DEDUCTIBLE	1	2016-01-20 10:10:30.849642	2016-01-20 10:10:30.849642		deductible	\N	\N
1620	TemplateColumn	138	PER MEMBER	2	2016-01-20 10:10:30.861457	2016-01-20 10:10:30.861457		per-member	\N	\N
1621	TemplateField	139	Retail Rx (Tier 4)	4	2016-01-20 10:10:30.932063	2016-01-20 10:10:30.932063		retail-rx-tier-4	\N	\N
1622	TemplateField	139	Retail Rx (Tier 5)	5	2016-01-20 10:10:30.943378	2016-01-20 10:10:30.943378		retail-rx-tier-5	\N	\N
1623	TemplateField	139	Retail Rx (Tier 2)	2	2016-01-20 10:10:30.954527	2016-01-20 10:10:30.954527		retail-rx-tier-2	t	t
1624	TemplateField	139	Retail Rx (Tier 1)	1	2016-01-20 10:10:30.965974	2016-01-20 10:10:30.965974		retail-rx-tier-1	t	t
1625	TemplateField	139	Retail Rx (Tier 3)	3	2016-01-20 10:10:30.977005	2016-01-20 10:10:30.977005		retail-rx-tier-3	t	t
1626	TemplateField	139	Mail Order Rx (Tier 4)	9	2016-01-20 10:10:30.98862	2016-01-20 10:10:30.98862		mail-order-rx-tier-4	\N	\N
1627	TemplateField	139	Mail Order Rx (Tier 5)	10	2016-01-20 10:10:30.999694	2016-01-20 10:10:30.999694		mail-order-rx-tier-5	\N	\N
1628	TemplateField	139	Speciality Medications - Mail Order	11	2016-01-20 10:10:31.010113	2016-01-20 10:10:31.010113		speciality-medications-mail-order	\N	\N
1629	TemplateField	139	Speciality Medications - Retail	12	2016-01-20 10:10:31.020903	2016-01-20 10:10:31.020903		speciality-medications-retail	\N	\N
1630	TemplateField	139	Mail Order Rx (Tier 1)	6	2016-01-20 10:10:31.031753	2016-01-20 10:10:31.031753		mail-order-rx-tier-1	\N	t
1631	TemplateField	139	Mail Order Rx (Tier 3)	8	2016-01-20 10:10:31.042964	2016-01-20 10:10:31.042964		mail-order-rx-tier-3	\N	t
1632	TemplateField	139	Mail Order Rx (Tier 2 )	7	2016-01-20 10:10:31.054303	2016-01-20 10:10:31.054303		mail-order-rx-tier-2	\N	t
1633	TemplateColumn	139	SUBJECT TO RX DEDUCTIBLE	1	2016-01-20 10:10:31.067909	2016-01-20 10:10:31.067909		subject-to-rx-deductible	\N	\N
1634	TemplateColumn	139	COINSURANCE	2	2016-01-20 10:10:31.079656	2016-01-20 10:10:31.079656		coinsurance	\N	\N
1635	TemplateField	140	Disease Management	1	2016-01-20 10:10:31.401	2016-01-20 10:10:31.401		disease-management	\N	\N
1636	TemplateColumn	140	IDENTIFICATION METHODS	1	2016-01-20 10:10:31.414003	2016-01-20 10:10:31.414003		identification-methods	\N	\N
1637	TemplateColumn	140	NUMBER OF CONDITIONS TRACKED	2	2016-01-20 10:10:31.424941	2016-01-20 10:10:31.424941		number-of-conditions-tracked	\N	\N
1638	TemplateColumn	140	OUTREACH METHODS	3	2016-01-20 10:10:31.435332	2016-01-20 10:10:31.435332		outreach-methods	\N	\N
1639	TemplateColumn	140	REPORT FREQUENCY	4	2016-01-20 10:10:31.445546	2016-01-20 10:10:31.445546		report-frequency	\N	\N
1640	TemplateField	141	Integrated Wellness Plan	1	2016-01-20 10:10:31.512512	2016-01-20 10:10:31.512512		integrated-wellness-plan	\N	\N
1641	TemplateColumn	141	ENTRIES	1	2016-01-20 10:10:31.52572	2016-01-20 10:10:31.52572		entries	\N	\N
1642	TemplateField	142	Type of Plan	1	2016-01-20 10:59:55.9831	2016-01-20 10:59:55.9831		type-of-plan	\N	\N
1643	TemplateField	142	Biometric Screening	2	2016-01-20 11:00:04.874081	2016-01-20 11:00:04.874081		biometric-screening	\N	\N
1644	TemplateField	142	Health Risk Assesment	3	2016-01-20 11:00:11.444217	2016-01-20 11:00:11.444217		health-risk-assesment	\N	\N
1645	TemplateField	142	Incentives	4	2016-01-20 11:00:18.84803	2016-01-20 11:00:18.84803		incentives	\N	\N
1646	TemplateField	142	Health Coaching	5	2016-01-20 11:00:25.738324	2016-01-20 11:00:25.738324		health-coaching	\N	\N
1647	TemplateField	142	Management Sponsored	6	2016-01-20 11:00:34.805181	2016-01-20 11:00:34.805181		management-sponsored	\N	\N
1648	TemplateField	142	Discounts of Wellness Products	7	2016-01-20 11:00:48.788673	2016-01-20 11:00:48.788673		discounts-of-wellness-products	\N	\N
1649	TemplateField	142	Online Health Management Tools	8	2016-01-20 11:00:58.345819	2016-01-20 11:00:58.345819		online-health-management-tools	\N	\N
1650	TemplateField	142	Communication Materials	9	2016-01-20 11:01:06.184539	2016-01-20 11:01:06.184539		communication-materials	\N	\N
1651	TemplateColumn	142	ENTRIES	1	2016-01-20 11:01:29.204767	2016-01-20 11:01:29.204767		entries	\N	\N
1652	TemplateField	143	Employee Eligibility Class #1	1	2016-01-20 11:06:54.255434	2016-01-20 11:06:54.255434		employee-eligibility-class-1	\N	\N
1653	TemplateField	143	Employee Eligibility Class #2	2	2016-01-20 11:06:58.870851	2016-01-20 11:06:58.870851		employee-eligibility-class-2	\N	\N
1654	TemplateField	143	Employee Eligibility Class #3	3	2016-01-20 11:07:03.064294	2016-01-20 11:07:03.064294		employee-eligibility-class-3	\N	\N
1655	TemplateField	143	Employee Eligibility Class #4	4	2016-01-20 11:07:07.987979	2016-01-20 11:07:07.987979		employee-eligibility-class-4	\N	\N
1656	TemplateColumn	143	EMPLOYEE CLASS	1	2016-01-20 11:07:17.024835	2016-01-20 11:07:17.024835		employee-class	\N	\N
1659	TemplateColumn	143	HOURS REQUIREMENT	4	2016-01-20 11:07:37.899299	2016-01-20 11:07:37.899299		hours-requirement	\N	\N
1681	TemplateColumn	145	DOMESTIC PARTNER ELIGIBLE	7	2016-01-20 11:28:43.838747	2016-01-20 11:28:43.838747		domestic-partner-eligible	\N	\N
1682	TemplateColumn	145	DEPENDENT CHILD LIMITING AGE	8	2016-01-20 11:29:33.519328	2016-01-20 11:29:33.519328		dependent-child-limiting-age	\N	\N
1657	TemplateColumn	143	WAITING PERIOD	3	2016-01-20 11:07:22.596128	2016-01-20 11:10:55.658744		waiting-period	\N	\N
1658	TemplateColumn	143	EMPLOYEE CLASSES	2	2016-01-20 11:07:30.630845	2016-01-20 11:11:36.394818		employee-classes	\N	\N
1660	TemplateColumn	143	SPOUSE ELIGIBLE	5	2016-01-20 11:12:33.570671	2016-01-20 11:12:33.570671		spouse-eligible	\N	\N
1661	TemplateField	144	Type of Plan	1	2016-01-20 11:26:32.370909	2016-01-20 11:26:32.370909		type-of-plan	\N	\N
1662	TemplateField	144	Biometric Screening	2	2016-01-20 11:26:32.383505	2016-01-20 11:26:32.383505		biometric-screening	\N	\N
1663	TemplateField	144	Health Risk Assesment	3	2016-01-20 11:26:32.394627	2016-01-20 11:26:32.394627		health-risk-assesment	\N	\N
1664	TemplateField	144	Incentives	4	2016-01-20 11:26:32.406176	2016-01-20 11:26:32.406176		incentives	\N	\N
1665	TemplateField	144	Health Coaching	5	2016-01-20 11:26:32.418242	2016-01-20 11:26:32.418242		health-coaching	\N	\N
1666	TemplateField	144	Management Sponsored	6	2016-01-20 11:26:32.43037	2016-01-20 11:26:32.43037		management-sponsored	\N	\N
1667	TemplateField	144	Discounts of Wellness Products	7	2016-01-20 11:26:32.441677	2016-01-20 11:26:32.441677		discounts-of-wellness-products	\N	\N
1668	TemplateField	144	Online Health Management Tools	8	2016-01-20 11:26:32.453584	2016-01-20 11:26:32.453584		online-health-management-tools	\N	\N
1669	TemplateField	144	Communication Materials	9	2016-01-20 11:26:32.465547	2016-01-20 11:26:32.465547		communication-materials	\N	\N
1670	TemplateColumn	144	ENTRIES	1	2016-01-20 11:26:32.479669	2016-01-20 11:26:32.479669		entries	\N	\N
1671	TemplateField	145	Employee Eligibility Class #1	1	2016-01-20 11:26:32.960514	2016-01-20 11:26:32.960514		employee-eligibility-class-1	\N	\N
1672	TemplateField	145	Employee Eligibility Class #2	2	2016-01-20 11:26:32.971262	2016-01-20 11:26:32.971262		employee-eligibility-class-2	\N	\N
1673	TemplateField	145	Employee Eligibility Class #3	3	2016-01-20 11:26:32.981517	2016-01-20 11:26:32.981517		employee-eligibility-class-3	\N	\N
1674	TemplateField	145	Employee Eligibility Class #4	4	2016-01-20 11:26:32.992309	2016-01-20 11:26:32.992309		employee-eligibility-class-4	\N	\N
1675	TemplateColumn	145	EMPLOYEE CLASS	1	2016-01-20 11:26:33.004938	2016-01-20 11:26:33.004938		employee-class	\N	\N
1676	TemplateColumn	145	HOURS REQUIREMENT	4	2016-01-20 11:26:33.015606	2016-01-20 11:26:33.015606		hours-requirement	\N	\N
1677	TemplateColumn	145	WAITING PERIOD	3	2016-01-20 11:26:33.026493	2016-01-20 11:26:33.026493		waiting-period	\N	\N
1678	TemplateColumn	145	EMPLOYEE CLASSES	2	2016-01-20 11:26:33.036825	2016-01-20 11:26:33.036825		employee-classes	\N	\N
1679	TemplateColumn	145	SPOUSE ELIGIBLE	5	2016-01-20 11:26:33.047198	2016-01-20 11:26:33.047198		spouse-eligible	\N	\N
1680	TemplateColumn	145	DEPENDENT CHILDREN ELIGIBLE	6	2016-01-20 11:27:37.129073	2016-01-20 11:27:37.129073		dependent-children-eligible	\N	\N
1683	TemplateColumn	145	DEPENDENT CHILD COVERAGE ENDS	9	2016-01-20 11:30:50.9402	2016-01-20 11:30:50.9402		dependent-child-coverage-ends	\N	\N
1689	TemplateField	146	Management Sponsored	6	2016-01-20 11:33:40.792115	2016-01-20 11:33:40.792115		management-sponsored	\N	\N
1685	TemplateField	146	Biometric Screening	2	2016-01-20 11:33:40.751745	2016-01-20 11:33:51.113321		biometric-screening	t	t
1686	TemplateField	146	Health Risk Assesment	3	2016-01-20 11:33:40.761693	2016-01-20 11:33:53.260004		health-risk-assesment	t	t
1687	TemplateField	146	Incentives	4	2016-01-20 11:33:40.771569	2016-01-20 11:33:54.88362		incentives	t	t
1688	TemplateField	146	Health Coaching	5	2016-01-20 11:33:40.781866	2016-01-20 11:33:57.124007		health-coaching	t	t
1690	TemplateField	146	Discounts of Wellness Products	7	2016-01-20 11:33:40.801988	2016-01-20 11:34:02.771143		discounts-of-wellness-products	t	t
1691	TemplateField	146	Online Health Management Tools	8	2016-01-20 11:33:40.812129	2016-01-20 11:33:40.812129		online-health-management-tools	\N	\N
1692	TemplateField	146	Communication Materials	9	2016-01-20 11:33:40.822127	2016-01-20 11:33:40.822127		communication-materials	\N	\N
1693	TemplateColumn	146	ENTRIES	1	2016-01-20 11:33:40.835915	2016-01-20 11:33:40.835915		entries	\N	\N
1694	TemplateField	147	Employee Eligibility Class #1	1	2016-01-20 11:33:40.955421	2016-01-20 11:33:40.955421		employee-eligibility-class-1	\N	\N
1695	TemplateField	147	Employee Eligibility Class #2	2	2016-01-20 11:33:40.965739	2016-01-20 11:33:40.965739		employee-eligibility-class-2	\N	\N
1696	TemplateField	147	Employee Eligibility Class #3	3	2016-01-20 11:33:40.976729	2016-01-20 11:33:40.976729		employee-eligibility-class-3	\N	\N
1697	TemplateField	147	Employee Eligibility Class #4	4	2016-01-20 11:33:40.988379	2016-01-20 11:33:40.988379		employee-eligibility-class-4	\N	\N
1698	TemplateColumn	147	DOMESTIC PARTNER ELIGIBLE	7	2016-01-20 11:33:41.001341	2016-01-20 11:33:41.001341		domestic-partner-eligible	\N	\N
1699	TemplateColumn	147	DEPENDENT CHILD LIMITING AGE	8	2016-01-20 11:33:41.011554	2016-01-20 11:33:41.011554		dependent-child-limiting-age	\N	\N
1700	TemplateColumn	147	EMPLOYEE CLASS	1	2016-01-20 11:33:41.021542	2016-01-20 11:33:41.021542		employee-class	\N	\N
1701	TemplateColumn	147	HOURS REQUIREMENT	4	2016-01-20 11:33:41.031757	2016-01-20 11:33:41.031757		hours-requirement	\N	\N
1702	TemplateColumn	147	WAITING PERIOD	3	2016-01-20 11:33:41.041651	2016-01-20 11:33:41.041651		waiting-period	\N	\N
1703	TemplateColumn	147	EMPLOYEE CLASSES	2	2016-01-20 11:33:41.051788	2016-01-20 11:33:41.051788		employee-classes	\N	\N
1704	TemplateColumn	147	SPOUSE ELIGIBLE	5	2016-01-20 11:33:41.062056	2016-01-20 11:33:41.062056		spouse-eligible	\N	\N
1705	TemplateColumn	147	DEPENDENT CHILDREN ELIGIBLE	6	2016-01-20 11:33:41.07209	2016-01-20 11:33:41.07209		dependent-children-eligible	\N	\N
1706	TemplateColumn	147	DEPENDENT CHILD COVERAGE ENDS	9	2016-01-20 11:33:41.082039	2016-01-20 11:33:41.082039		dependent-child-coverage-ends	\N	\N
1684	TemplateField	146	Type of Plan	1	2016-01-20 11:33:40.736802	2016-01-20 11:33:49.491758		type-of-plan	t	t
1707	TemplateField	148	HRA Vendor Relationship	1	2016-01-20 11:35:05.213175	2016-01-20 11:35:05.213175		hra-vendor-relationship	\N	\N
1709	TemplateColumn	148	ENTRIES	1	2016-01-20 11:35:20.574199	2016-01-20 11:35:20.574199		entries	\N	\N
1711	TemplateField	148	Type of HRA	2	2016-01-20 11:35:39.008626	2016-01-20 11:35:39.008626		type-of-hra	\N	\N
1712	TemplateColumn	149	Covered	1	2016-01-20 11:35:39.522591	2016-01-20 11:35:39.522591		covered	\N	\N
1713	TemplateColumn	149	Not Covered	2	2016-01-20 11:35:48.686377	2016-01-20 11:35:48.686377		not-covered	\N	\N
1714	TemplateColumn	149	Ortho Deductible	3	2016-01-20 11:36:00.989914	2016-01-20 11:36:00.989914		ortho-deductible	\N	\N
1715	TemplateColumn	149	Coinsurance	4	2016-01-20 11:36:14.576645	2016-01-20 11:36:14.576645		coinsurance	\N	\N
1716	TemplateColumn	149	Lifetime Maximum	5	2016-01-20 11:36:26.309365	2016-01-20 11:36:26.309365		lifetime-maximum	\N	\N
1717	TemplateField	150	Account Balance options upon employee disenrollm23	1	2016-01-20 11:36:27.366816	2016-01-20 11:36:27.366816		account-balance-options-upon-employee-disenrollm23	\N	\N
1718	TemplateColumn	149	Copays Apply	6	2016-01-20 11:36:36.875411	2016-01-20 11:36:36.875411		copays-apply	\N	\N
1719	TemplateField	150	Claim Adjudication Options	2	2016-01-20 11:36:44.188407	2016-01-20 11:36:44.188407		claim-adjudication-options	\N	\N
1721	TemplateField	150	If Yes, what is the limit?	4	2016-01-20 11:37:17.076953	2016-01-20 11:37:17.076953		if-yes-what-is-the-limit	\N	\N
1720	TemplateField	150	Do Funds Roll Over?	3	2016-01-20 11:36:59.705082	2016-01-20 11:37:23.867017		do-funds-roll-over	\N	\N
1722	TemplateField	150	what Funds are eligible for the HRA?	5	2016-01-20 11:37:38.716674	2016-01-20 11:37:38.716674		what-funds-are-eligible-for-the-hra	\N	\N
1723	TemplateColumn	150	ENTRIES	1	2016-01-20 11:37:51.185995	2016-01-20 11:37:51.185995		entries	\N	\N
1726	TemplateColumn	152	Employee Class	1	2016-01-20 11:39:33.883909	2016-01-20 11:39:33.883909		employee-class	\N	\N
1728	TemplateColumn	152	Employee Classes	2	2016-01-20 11:39:43.393298	2016-01-20 11:39:43.393298		employee-classes	\N	\N
1738	TemplateField	152	Employee Eligibility Class #3	3	2016-01-20 11:41:59.522652	2016-01-20 11:41:59.522652		employee-eligibility-class-3	\N	\N
1729	TemplateColumn	152	Waiting Period	3	2016-01-20 11:39:55.837697	2016-01-20 11:39:55.837697		waiting-period	\N	\N
1731	TemplateColumn	152	Hours Requirement	4	2016-01-20 11:40:22.071906	2016-01-20 11:40:25.668028		hours-requirement	\N	\N
1730	TemplateColumn	152	Spouse Eligible	5	2016-01-20 11:40:05.131397	2016-01-20 11:40:26.048766		spouse-eligible	\N	\N
1732	TemplateColumn	152	Dependent Children Eligible	6	2016-01-20 11:40:36.640185	2016-01-20 11:40:36.640185		dependent-children-eligible	\N	\N
1733	TemplateColumn	152	Domestic Partner Eligible	7	2016-01-20 11:40:53.785685	2016-01-20 11:40:53.785685		domestic-partner-eligible	\N	\N
1724	TemplateField	151	Employee	1	2016-01-20 11:39:19.073833	2016-01-20 11:41:29.48116		employee	\N	\N
1725	TemplateField	151	Employee +1	2	2016-01-20 11:39:27.295268	2016-01-20 11:41:36.05192		employee-1	\N	\N
1736	TemplateField	152	Employee Eligibility Class #1	1	2016-01-20 11:41:48.478499	2016-01-20 11:41:48.478499		employee-eligibility-class-1	\N	\N
1727	TemplateField	151	Employee +2 or more	3	2016-01-20 11:39:35.869144	2016-01-20 11:41:51.911957		employee-2-or-more	\N	\N
1737	TemplateField	152	Employee Eligibility Class #2	2	2016-01-20 11:41:52.926593	2016-01-20 11:41:52.926593		employee-eligibility-class-2	\N	\N
1739	TemplateField	151	Employee + Child(ren)	4	2016-01-20 11:42:02.960304	2016-01-20 11:42:02.960304		employee-child-ren	\N	\N
1740	TemplateField	152	Employee Eligibility Class #4	4	2016-01-20 11:42:03.711611	2016-01-20 11:42:03.711611		employee-eligibility-class-4	\N	\N
1741	TemplateField	151	Employee + Spouse	5	2016-01-20 11:42:12.66235	2016-01-20 11:42:12.66235		employee-spouse	\N	\N
1742	TemplateField	151	Famliy	6	2016-01-20 11:42:17.920082	2016-01-20 11:42:17.920082		famliy	\N	\N
1743	TemplateColumn	151	REIMBURSEMENT AMOUNT	1	2016-01-20 11:42:33.154783	2016-01-20 11:42:33.154783		reimbursement-amount	\N	\N
1744	TemplateField	153	Co-Insurance	1	2016-01-20 11:44:04.630443	2016-01-20 11:44:04.630443		co-insurance	\N	\N
1745	TemplateField	153	Deductible Family	2	2016-01-20 11:44:14.798697	2016-01-20 11:44:14.798697		deductible-family	\N	\N
1746	TemplateField	153	Deductible Individual	3	2016-01-20 11:44:25.718757	2016-01-20 11:44:25.718757		deductible-individual	\N	\N
1747	TemplateField	153	Emergency Room	4	2016-01-20 11:44:44.742359	2016-01-20 11:44:44.742359		emergency-room	\N	\N
1748	TemplateField	154	Plan Type	1	2016-01-20 11:47:48.102982	2016-01-20 11:47:48.102982		plan-type	t	t
1749	TemplateField	154	Network	3	2016-01-20 11:47:48.116222	2016-01-20 11:47:48.116222		network	t	t
1750	TemplateField	154	Referrals Needed	5	2016-01-20 11:47:48.127065	2016-01-20 11:47:48.127065		referrals-needed	t	t
1710	TemplateField	149	Child Orthodontia	2	2016-01-20 11:35:28.022657	2016-01-20 11:50:16.246679		child-orthodontia	t	t
1735	TemplateColumn	152	Dependent Child Coverage Ends	9	2016-01-20 11:41:21.887659	2016-01-20 19:37:54.176825		dependent-child-coverage-ends	\N	\N
1734	TemplateColumn	152	Dependent Child Limiting Age	8	2016-01-20 11:41:05.199116	2016-01-20 19:38:09.83902		dependent-child-limiting-age	\N	\N
1751	TemplateField	154	Deductible Individual	7	2016-01-20 11:47:48.137494	2016-01-20 11:47:48.137494		deductible-individual	t	t
1752	TemplateField	154	Deductible Family	9	2016-01-20 11:47:48.151862	2016-01-20 11:47:48.151862		deductible-family	t	t
1753	TemplateField	154	Deductible - Family Multiple	11	2016-01-20 11:47:48.161821	2016-01-20 11:47:48.161821		deductible-family-multiple	\N	\N
1754	TemplateField	154	Deductible Format	13	2016-01-20 11:47:48.178801	2016-01-20 11:47:48.178801		deductible-format	\N	t
1755	TemplateField	154	Coinsurance	15	2016-01-20 11:47:48.190811	2016-01-20 11:47:48.190811		coinsurance	t	t
1756	TemplateField	154	Out-of-Pocket Max Family (includes deductible)	19	2016-01-20 11:47:48.201733	2016-01-20 11:47:48.201733		out-of-pocket-max-family-includes-deductible	t	t
1757	TemplateField	154	In & Out-of-Network Deductibles Cross Accumulation	23	2016-01-20 11:47:48.21281	2016-01-20 11:47:48.21281		in-out-of-network-deductibles-cross-accumulation	\N	t
1758	TemplateField	154	OON Reimbursement Level	27	2016-01-20 11:47:48.225241	2016-01-20 11:47:48.225241		oon-reimbursement-level	t	t
1759	TemplateField	154	Annual Maximum	29	2016-01-20 11:47:48.235714	2016-01-20 11:47:48.235714		annual-maximum	\N	\N
1760	TemplateField	154	Gym Reimbursement	31	2016-01-20 11:47:48.24713	2016-01-20 11:47:48.24713		gym-reimbursement	\N	\N
1761	TemplateField	154	Customer Service Days/Hours	33	2016-01-20 11:47:48.25727	2016-01-20 11:47:48.25727		customer-service-days-hours	\N	\N
1762	TemplateField	154	Nurseline	35	2016-01-20 11:47:48.267458	2016-01-20 11:47:48.267458		nurseline	\N	\N
1763	TemplateField	154	International Employees Covered	37	2016-01-20 11:47:48.279076	2016-01-20 11:47:48.279076		international-employees-covered	\N	\N
1764	TemplateField	154	Out-of-Pocket Max Individual (includes deductible)	17	2016-01-20 11:47:48.289779	2016-01-20 11:47:48.289779		out-of-pocket-max-individual-includes-deductible	t	t
1765	TemplateField	154	Out-of-Pocket Maximum - Family Multiple	21	2016-01-20 11:47:48.299783	2016-01-20 11:47:48.299783		out-of-pocket-maximum-family-multiple	\N	f
1766	TemplateField	154	In & Out-of-Network OOP Maximum Cross Accumulation	25	2016-01-20 11:47:48.309758	2016-01-20 11:47:48.309758		in-out-of-network-oop-maximum-cross-accumulation	\N	t
1767	TemplateColumn	154	ENTRIES	1	2016-01-20 11:47:48.322085	2016-01-20 11:47:48.322085		entries	\N	\N
1768	TemplateField	155	Primary Care Office Visit	1	2016-01-20 11:47:48.582378	2016-01-20 11:47:48.582378		primary-care-office-visit	t	t
1769	TemplateField	155	Specialist Office Visit	2	2016-01-20 11:47:48.593195	2016-01-20 11:47:48.593195		specialist-office-visit	t	t
1770	TemplateField	155	Lab	3	2016-01-20 11:47:48.603682	2016-01-20 11:47:48.603682		lab	\N	t
1771	TemplateField	155	General Radiology	4	2016-01-20 11:47:48.614205	2016-01-20 11:47:48.614205		general-radiology	\N	\N
1772	TemplateField	155	CT/MRI/PET Scans	5	2016-01-20 11:47:48.625088	2016-01-20 11:47:48.625088		ct-mri-pet-scans	\N	\N
1773	TemplateField	155	Annual Physical Exam	6	2016-01-20 11:47:48.636129	2016-01-20 11:47:48.636129		annual-physical-exam	\N	\N
1774	TemplateField	155	Well Child Exams	7	2016-01-20 11:47:48.646842	2016-01-20 11:47:48.646842		well-child-exams	\N	\N
1775	TemplateField	155	Pediatric Dental	8	2016-01-20 11:47:48.657988	2016-01-20 11:47:48.657988		pediatric-dental	\N	\N
1776	TemplateField	155	Hospitalization - Inpatient	9	2016-01-20 11:47:48.668656	2016-01-20 11:47:48.668656		hospitalization-inpatient	t	t
1777	TemplateField	155	Hospitalization - Outpatient	10	2016-01-20 11:47:48.679182	2016-01-20 11:47:48.679182		hospitalization-outpatient	\N	t
1778	TemplateField	155	Surgery/Anesthesiology	11	2016-01-20 11:47:48.68971	2016-01-20 11:47:48.68971		surgery-anesthesiology	\N	\N
1779	TemplateField	155	Emergency Room	12	2016-01-20 11:47:48.70015	2016-01-20 11:47:48.70015		emergency-room	t	t
1780	TemplateField	155	Urgent Care	13	2016-01-20 11:47:48.710447	2016-01-20 11:47:48.710447		urgent-care	\N	t
1781	TemplateField	155	Mental Nervous - Inpatient Coverage	14	2016-01-20 11:47:48.72761	2016-01-20 11:47:48.72761		mental-nervous-inpatient-coverage	\N	t
1782	TemplateField	155	Mental Nervous - Outpatient Coverage	15	2016-01-20 11:47:48.740026	2016-01-20 11:47:48.740026		mental-nervous-outpatient-coverage	\N	t
1783	TemplateField	155	Substance Abuse - Inpatient Coverage	16	2016-01-20 11:47:48.750809	2016-01-20 11:47:48.750809		substance-abuse-inpatient-coverage	\N	t
1784	TemplateField	155	Physical Therapy - Inpatient Coverage	18	2016-01-20 11:47:48.761312	2016-01-20 11:47:48.761312		physical-therapy-inpatient-coverage	\N	\N
1785	TemplateField	155	Physical Therapy - Outpatient Coverage	19	2016-01-20 11:47:48.773081	2016-01-20 11:47:48.773081		physical-therapy-outpatient-coverage	\N	t
1786	TemplateField	155	Occupational Therapy - Inpatient Coverage	20	2016-01-20 11:47:48.783533	2016-01-20 11:47:48.783533		occupational-therapy-inpatient-coverage	\N	\N
1787	TemplateField	155	Occupational Therapy - Outpatient Coverage	21	2016-01-20 11:47:48.796097	2016-01-20 11:47:48.796097		occupational-therapy-outpatient-coverage	\N	\N
1788	TemplateField	155	Speech Therapy - Inpatient Coverage	22	2016-01-20 11:47:48.806852	2016-01-20 11:47:48.806852		speech-therapy-inpatient-coverage	\N	\N
1789	TemplateField	155	Speech Therapy - Outpatient Coverage	23	2016-01-20 11:47:48.817462	2016-01-20 11:47:48.817462		speech-therapy-outpatient-coverage	\N	\N
1790	TemplateField	155	Pregnancy & Maternity Care - Office Visits	24	2016-01-20 11:47:48.827975	2016-01-20 11:47:48.827975		pregnancy-maternity-care-office-visits	\N	\N
1791	TemplateField	155	Pregnancy & Maternity Care - Labor & Delivery	25	2016-01-20 11:47:48.83823	2016-01-20 11:47:48.83823		pregnancy-maternity-care-labor-delivery	\N	\N
1792	TemplateField	155	Chiropractic Services	26	2016-01-20 11:47:48.849423	2016-01-20 11:47:48.849423		chiropractic-services	\N	t
1793	TemplateField	155	Ambulance	27	2016-01-20 11:47:48.860247	2016-01-20 11:47:48.860247		ambulance	\N	\N
1794	TemplateField	155	Hospice	28	2016-01-20 11:47:48.871035	2016-01-20 11:47:48.871035		hospice	\N	\N
1795	TemplateField	155	Home Healthcare	29	2016-01-20 11:47:48.881613	2016-01-20 11:47:48.881613		home-healthcare	\N	\N
1796	TemplateField	155	Skilled Nursing	30	2016-01-20 11:47:48.892901	2016-01-20 11:47:48.892901		skilled-nursing	\N	\N
1797	TemplateField	155	Infertility Coverage	31	2016-01-20 11:47:48.904097	2016-01-20 11:47:48.904097		infertility-coverage	\N	t
1798	TemplateField	155	Prosthetics	32	2016-01-20 11:47:48.914747	2016-01-20 11:47:48.914747		prosthetics	\N	\N
1799	TemplateField	155	Durable Medical Equipment	33	2016-01-20 11:47:48.925994	2016-01-20 11:47:48.925994		durable-medical-equipment	\N	t
1800	TemplateField	155	Hearing Devices	34	2016-01-20 11:47:48.93687	2016-01-20 11:47:48.93687		hearing-devices	\N	\N
1801	TemplateField	155	Vision Exams	35	2016-01-20 11:47:48.947357	2016-01-20 11:47:48.947357		vision-exams	\N	\N
1802	TemplateField	155	Short Term Rehabilitation- Inpatient	36	2016-01-20 11:47:48.957572	2016-01-20 11:47:48.957572		short-term-rehabilitation-inpatient	\N	\N
1803	TemplateField	155	Short Term Rehabilitation - Outpatient	37	2016-01-20 11:47:48.967909	2016-01-20 11:47:48.967909		short-term-rehabilitation-outpatient	\N	\N
1804	TemplateField	155	Telemedicine	38	2016-01-20 11:47:48.977972	2016-01-20 11:47:48.977972		telemedicine	\N	\N
1805	TemplateField	155	Speciality Rx Coverage	39	2016-01-20 11:47:48.988189	2016-01-20 11:47:48.988189		speciality-rx-coverage	\N	\N
1806	TemplateField	155	Substance Abuse - Outpatient Coverage	17	2016-01-20 11:47:48.998597	2016-01-20 11:47:48.998597		substance-abuse-outpatient-coverage	\N	t
1807	TemplateColumn	155	Subject to Deductible & Coinsurance	1	2016-01-20 11:47:49.011865	2016-01-20 11:47:49.011865		subject-to-deductible-coinsurance	\N	\N
1808	TemplateColumn	155	COPAY	2	2016-01-20 11:47:49.023238	2016-01-20 11:47:49.023238		copay	\N	\N
1809	TemplateColumn	155	LIMIT	3	2016-01-20 11:47:49.033429	2016-01-20 11:47:49.033429		limit	\N	\N
1810	TemplateColumn	155	COPAY LIMIT	4	2016-01-20 11:47:49.046653	2016-01-20 11:47:49.046653		copay-limit	\N	\N
1811	TemplateColumn	155	SEPERATE DEDUCTIBLE	5	2016-01-20 11:47:49.057255	2016-01-20 11:47:49.057255		seperate-deductible	\N	\N
1812	TemplateColumn	155	Separate Coinsurance	6	2016-01-20 11:47:49.067336	2016-01-20 11:47:49.067336		separate-coinsurance	\N	\N
1813	TemplateField	156	Subject to Medical Deductible	1	2016-01-20 11:47:51.951201	2016-01-20 11:47:51.951201		subject-to-medical-deductible	\N	t
1814	TemplateField	156	Subject to Medical Deductible & Coinsurance	2	2016-01-20 11:47:51.962978	2016-01-20 11:47:51.962978		subject-to-medical-deductible-coinsurance	\N	\N
1815	TemplateField	156	Network	3	2016-01-20 11:47:51.973832	2016-01-20 11:47:51.973832		network	\N	t
1816	TemplateField	156	Mandatory Generic	4	2016-01-20 11:47:51.985829	2016-01-20 11:47:51.985829		mandatory-generic	\N	\N
1817	TemplateField	156	Step Therapy/Precertification Applies	5	2016-01-20 11:47:51.996471	2016-01-20 11:47:51.996471		step-therapy-precertification-applies	\N	\N
1818	TemplateField	156	Oral Contraceptive	6	2016-01-20 11:47:52.008334	2016-01-20 11:47:52.008334		oral-contraceptive	\N	\N
1819	TemplateField	156	Prescriptions Covered Out of Network	7	2016-01-20 11:47:52.019576	2016-01-20 11:47:52.019576		prescriptions-covered-out-of-network	\N	\N
1820	TemplateColumn	156	ENTRIES	1	2016-01-20 11:47:52.034867	2016-01-20 11:47:52.034867		entries	\N	\N
1821	TemplateField	157	Rx Deductible - Individual	1	2016-01-20 11:47:52.143311	2016-01-20 11:47:52.143311		rx-deductible-individual	t	t
1822	TemplateField	157	Rx Deductible - Family	2	2016-01-20 11:47:52.154363	2016-01-20 11:47:52.154363		rx-deductible-family	t	t
1823	TemplateColumn	157	DEDUCTIBLE	1	2016-01-20 11:47:52.167699	2016-01-20 11:47:52.167699		deductible	\N	\N
1824	TemplateColumn	157	PER MEMBER	2	2016-01-20 11:47:52.184963	2016-01-20 11:47:52.184963		per-member	\N	\N
1825	TemplateField	158	Retail Rx (Tier 1)	1	2016-01-20 11:47:52.258233	2016-01-20 11:47:52.258233		retail-rx-tier-1	t	t
1826	TemplateField	158	Retail Rx (Tier 3)	13	2016-01-20 11:47:52.268894	2016-01-20 11:47:52.268894		retail-rx-tier-3	t	t
1827	TemplateField	158	Retail Rx (Tier 4)	19	2016-01-20 11:47:52.279608	2016-01-20 11:47:52.279608		retail-rx-tier-4	\N	\N
1828	TemplateField	158	Retail Rx (Tier 5)	25	2016-01-20 11:47:52.289978	2016-01-20 11:47:52.289978		retail-rx-tier-5	\N	\N
1829	TemplateField	158	Mail Order Rx (Tier 1)	31	2016-01-20 11:47:52.300524	2016-01-20 11:47:52.300524		mail-order-rx-tier-1	\N	t
1830	TemplateField	158	Specialty Medications -Mail Order	67	2016-01-20 11:47:52.311137	2016-01-20 11:47:52.311137		specialty-medications-mail-order	\N	f
1831	TemplateField	158	Specialty Medications  -Retail	61	2016-01-20 11:47:52.322501	2016-01-20 11:47:52.322501		specialty-medications-retail	\N	\N
1832	TemplateField	158	Mail Order Rx (Tier 3)	43	2016-01-20 11:47:52.333102	2016-01-20 11:47:52.333102		mail-order-rx-tier-3	\N	t
1833	TemplateField	158	Mail Order Rx (Tier 4)	49	2016-01-20 11:47:52.344156	2016-01-20 11:47:52.344156		mail-order-rx-tier-4	\N	\N
1834	TemplateField	158	Mail Order Rx (Tier 5)	55	2016-01-20 11:47:52.354701	2016-01-20 11:47:52.354701		mail-order-rx-tier-5	\N	\N
1835	TemplateField	158	Retail Rx (Tier 2)	7	2016-01-20 11:47:52.365379	2016-01-20 11:47:52.365379		retail-rx-tier-2	t	t
1836	TemplateField	158	Mail Order Rx (Tier 2)	37	2016-01-20 11:47:52.376165	2016-01-20 11:47:52.376165		mail-order-rx-tier-2	\N	t
1837	TemplateColumn	158	SUBJECT TO RX DEDUCTIBLE	1	2016-01-20 11:47:52.389414	2016-01-20 11:47:52.389414		subject-to-rx-deductible	\N	\N
1838	TemplateColumn	158	COPAY	2	2016-01-20 11:47:52.399796	2016-01-20 11:47:52.399796		copay	\N	\N
1839	TemplateColumn	158	COINSURANCE	3	2016-01-20 11:47:52.411292	2016-01-20 11:47:52.411292		coinsurance	\N	\N
1840	TemplateColumn	158	PER SCRIPT COINSURANCE LIMIT	4	2016-01-20 11:47:52.423065	2016-01-20 11:47:52.423065		per-script-coinsurance-limit	\N	\N
1841	TemplateColumn	158	PER SCRIPT COINSURANCE MINIMUM	5	2016-01-20 11:47:52.433437	2016-01-20 11:47:52.433437		per-script-coinsurance-minimum	\N	\N
1842	TemplateField	159	Disease Management	1	2016-01-20 11:47:53.330078	2016-01-20 11:47:53.330078		disease-management	\N	\N
1843	TemplateColumn	159	IDENTIFICATION METHODS	1	2016-01-20 11:47:53.344365	2016-01-20 11:47:53.344365		identification-methods	\N	\N
1844	TemplateColumn	159	NUMBER OF CONDITIONS TRACKED	2	2016-01-20 11:47:53.357483	2016-01-20 11:47:53.357483		number-of-conditions-tracked	\N	\N
1845	TemplateColumn	159	OUTREACH METHODS	3	2016-01-20 11:47:53.368649	2016-01-20 11:47:53.368649		outreach-methods	\N	\N
1846	TemplateColumn	159	REPORT FREQUENCY	4	2016-01-20 11:47:53.379607	2016-01-20 11:47:53.379607		report-frequency	\N	\N
1847	TemplateField	160	Integrated Wellness Plan	1	2016-01-20 11:47:53.45207	2016-01-20 11:47:53.45207		integrated-wellness-plan	\N	\N
1848	TemplateColumn	160	Entries	1	2016-01-20 11:47:53.464869	2016-01-20 11:47:53.464869		entries	\N	\N
1849	TemplateField	161	Employee Eligibility Class #1	1	2016-01-20 11:47:53.503511	2016-01-20 11:47:53.503511		employee-eligibility-class-1	\N	\N
1850	TemplateField	161	Employee Eligibility Class #2	2	2016-01-20 11:47:53.514322	2016-01-20 11:47:53.514322		employee-eligibility-class-2	\N	\N
1851	TemplateField	161	Employee Eligibility Class #3	3	2016-01-20 11:47:53.527248	2016-01-20 11:47:53.527248		employee-eligibility-class-3	\N	\N
1852	TemplateField	161	Employee Eligibility Class #4	4	2016-01-20 11:47:53.538286	2016-01-20 11:47:53.538286		employee-eligibility-class-4	\N	\N
1853	TemplateColumn	161	Employee Class	1	2016-01-20 11:47:53.553558	2016-01-20 11:47:53.553558		employee-class	\N	\N
1854	TemplateColumn	161	Employee Classes	2	2016-01-20 11:47:53.56485	2016-01-20 11:47:53.56485		employee-classes	\N	\N
1855	TemplateColumn	161	Waiting Period	3	2016-01-20 11:47:53.575482	2016-01-20 11:47:53.575482		waiting-period	\N	\N
1856	TemplateColumn	161	Hours Requirement	4	2016-01-20 11:47:53.58776	2016-01-20 11:47:53.58776		hours-requirement	\N	\N
1857	TemplateColumn	161	Spouse Eligible	5	2016-01-20 11:47:53.598771	2016-01-20 11:47:53.598771		spouse-eligible	\N	\N
1858	TemplateColumn	161	Dependent Children Eligible	6	2016-01-20 11:47:53.612182	2016-01-20 11:47:53.612182		dependent-children-eligible	\N	\N
1859	TemplateColumn	161	Domestic Partner Eligible	7	2016-01-20 11:47:53.62345	2016-01-20 11:47:53.62345		domestic-partner-eligible	\N	\N
1860	TemplateColumn	161	Dependent Child Limiting Age	8	2016-01-20 11:47:53.634255	2016-01-20 11:47:53.634255		dependent-child-limiting-age	\N	\N
1861	TemplateColumn	161	Dependent Child Coverage Ends	9	2016-01-20 11:47:53.647635	2016-01-20 11:47:53.647635		dependent-child-coverage-ends	\N	\N
1862	TemplateField	162	Plan Type	1	2016-01-20 11:47:54.336628	2016-01-20 11:47:54.336628		plan-type	\N	\N
1863	TemplateField	162	Network	2	2016-01-20 11:47:54.347669	2016-01-20 11:47:54.347669		network	\N	\N
1864	TemplateField	162	Referrals Needed	3	2016-01-20 11:47:54.357992	2016-01-20 11:47:54.357992		referrals-needed	\N	\N
1865	TemplateField	162	Deductible Individual	4	2016-01-20 11:47:54.368188	2016-01-20 11:47:54.368188		deductible-individual	\N	\N
1866	TemplateField	162	Deductible Family	5	2016-01-20 11:47:54.378507	2016-01-20 11:47:54.378507		deductible-family	\N	\N
1867	TemplateField	162	Deductible - Family Multiple	6	2016-01-20 11:47:54.388831	2016-01-20 11:47:54.388831		deductible-family-multiple	\N	\N
1868	TemplateField	162	Deductible Format	7	2016-01-20 11:47:54.399046	2016-01-20 11:47:54.399046		deductible-format	\N	\N
1869	TemplateField	162	Coinsurance	8	2016-01-20 11:47:54.409528	2016-01-20 11:47:54.409528		coinsurance	\N	\N
1870	TemplateField	162	Out-of-Pocket Max Individual (includes deductible)	9	2016-01-20 11:47:54.420164	2016-01-20 11:47:54.420164		out-of-pocket-max-individual-includes-deductible	\N	\N
1871	TemplateField	162	Out-of-Pocket Max Family (includes deductible)	10	2016-01-20 11:47:54.431104	2016-01-20 11:47:54.431104		out-of-pocket-max-family-includes-deductible	\N	\N
1872	TemplateField	162	Out-of-Pocket Maximum - Family Multiple	11	2016-01-20 11:47:54.441664	2016-01-20 11:47:54.441664		out-of-pocket-maximum-family-multiple	\N	\N
1873	TemplateField	162	In & Out-of-Network Deductibles Cross Accumulation	12	2016-01-20 11:47:54.454275	2016-01-20 11:47:54.454275		in-out-of-network-deductibles-cross-accumulation	\N	\N
1874	TemplateField	162	In & Out-of-Network OOP Maximum Cross Accumulation	13	2016-01-20 11:47:54.465227	2016-01-20 11:47:54.465227		in-out-of-network-oop-maximum-cross-accumulation	\N	\N
1875	TemplateField	162	Annual Maximum	14	2016-01-20 11:47:54.47586	2016-01-20 11:47:54.47586		annual-maximum	\N	\N
1876	TemplateField	162	Gym Reimbursement	15	2016-01-20 11:47:54.487909	2016-01-20 11:47:54.487909		gym-reimbursement	\N	\N
1877	TemplateField	162	Customer Service Days/Hours	16	2016-01-20 11:47:54.498454	2016-01-20 11:47:54.498454		customer-service-days-hours	\N	\N
1878	TemplateField	162	OON Reimbursement Level	17	2016-01-20 11:47:54.509021	2016-01-20 11:47:54.509021		oon-reimbursement-level	\N	\N
1879	TemplateField	162	Nurseline	18	2016-01-20 11:47:54.519221	2016-01-20 11:47:54.519221		nurseline	\N	\N
1880	TemplateField	162	International Employees Covered	19	2016-01-20 11:47:54.52939	2016-01-20 11:47:54.52939		international-employees-covered	\N	\N
1881	TemplateColumn	162	ENTRIES	1	2016-01-20 11:47:54.542465	2016-01-20 11:47:54.542465		entries	\N	\N
1882	TemplateField	163	Plan Type	1	2016-01-20 11:47:54.767959	2016-01-20 11:47:54.767959		plan-type	t	t
1883	TemplateField	163	Network	2	2016-01-20 11:47:54.778397	2016-01-20 11:47:54.778397		network	t	t
1884	TemplateField	163	Referrals Needed	3	2016-01-20 11:47:54.790103	2016-01-20 11:47:54.790103		referrals-needed	t	t
1885	TemplateField	163	Deductible Individual	4	2016-01-20 11:47:54.800648	2016-01-20 11:47:54.800648		deductible-individual	t	t
1886	TemplateField	163	Deductible Family	5	2016-01-20 11:47:54.811511	2016-01-20 11:47:54.811511		deductible-family	t	t
1887	TemplateField	163	Deductible - Family Multiple	6	2016-01-20 11:47:54.82248	2016-01-20 11:47:54.82248		deductible-family-multiple	\N	\N
1888	TemplateField	163	Deductible Format	7	2016-01-20 11:47:54.832788	2016-01-20 11:47:54.832788		deductible-format	\N	t
1889	TemplateField	163	Coinsurance	8	2016-01-20 11:47:54.843357	2016-01-20 11:47:54.843357		coinsurance	t	t
1890	TemplateField	163	Out-of-Pocket Max Individual (includes deductible)	9	2016-01-20 11:47:54.853734	2016-01-20 11:47:54.853734		out-of-pocket-max-individual-includes-deductible	t	t
1891	TemplateField	163	Out-of-Pocket Max Family (includes deductible)	10	2016-01-20 11:47:54.864962	2016-01-20 11:47:54.864962		out-of-pocket-max-family-includes-deductible	t	t
1892	TemplateField	163	Out-of-Pocket Maximum - Family Multiple	11	2016-01-20 11:47:54.875488	2016-01-20 11:47:54.875488		out-of-pocket-maximum-family-multiple	\N	\N
1893	TemplateField	163	In & Out-of-Network Deductibles Cross Accumulation	12	2016-01-20 11:47:54.88614	2016-01-20 11:47:54.88614		in-out-of-network-deductibles-cross-accumulation	\N	t
1894	TemplateField	163	In & Out-of-Network OOP Maximum Cross Accumulation	13	2016-01-20 11:47:54.896553	2016-01-20 11:47:54.896553		in-out-of-network-oop-maximum-cross-accumulation	\N	t
1895	TemplateField	163	OON Reimbursement Level	14	2016-01-20 11:47:54.906982	2016-01-20 11:47:54.906982		oon-reimbursement-level	t	t
1896	TemplateField	163	Annual Maximum	15	2016-01-20 11:47:54.917312	2016-01-20 11:47:54.917312		annual-maximum	\N	\N
1897	TemplateField	163	Gym Reimbursement	16	2016-01-20 11:47:54.927266	2016-01-20 11:47:54.927266		gym-reimbursement	\N	\N
1898	TemplateField	163	Customer Service Days/Hours	17	2016-01-20 11:47:54.937315	2016-01-20 11:47:54.937315		customer-service-days-hours	\N	\N
1899	TemplateField	163	Nurseline	18	2016-01-20 11:47:54.947553	2016-01-20 11:47:54.947553		nurseline	\N	\N
1900	TemplateField	163	International Employees Covered	19	2016-01-20 11:47:54.958374	2016-01-20 11:47:54.958374		international-employees-covered	\N	\N
1901	TemplateColumn	163	ENTRIES	1	2016-01-20 11:47:54.971151	2016-01-20 11:47:54.971151		entries	\N	\N
1902	TemplateField	164	General Radiology	4	2016-01-20 11:47:55.220051	2016-01-20 11:47:55.220051		general-radiology	\N	\N
1903	TemplateField	164	CT/MRI/PET Scans	5	2016-01-20 11:47:55.230482	2016-01-20 11:47:55.230482		ct-mri-pet-scans	\N	\N
1904	TemplateField	164	Annual Physical Exam	6	2016-01-20 11:47:55.240811	2016-01-20 11:47:55.240811		annual-physical-exam	\N	\N
1905	TemplateField	164	Well Child Exams	7	2016-01-20 11:47:55.251306	2016-01-20 11:47:55.251306		well-child-exams	\N	\N
1906	TemplateField	164	Pediatric Dental	8	2016-01-20 11:47:55.261521	2016-01-20 11:47:55.261521		pediatric-dental	\N	\N
1907	TemplateField	164	Hospitalization - Inpatient	9	2016-01-20 11:47:55.271812	2016-01-20 11:47:55.271812		hospitalization-inpatient	t	t
1908	TemplateField	164	Hospitalization - Outpatient	10	2016-01-20 11:47:55.282346	2016-01-20 11:47:55.282346		hospitalization-outpatient	\N	t
1909	TemplateField	164	Surgery/Anesthesiology	11	2016-01-20 11:47:55.292558	2016-01-20 11:47:55.292558		surgery-anesthesiology	\N	\N
1910	TemplateField	164	Emergency Room	12	2016-01-20 11:47:55.303185	2016-01-20 11:47:55.303185		emergency-room	t	t
1911	TemplateField	164	Specialist Office Visit	2	2016-01-20 11:47:55.313942	2016-01-20 11:47:55.313942		specialist-office-visit	t	t
1912	TemplateField	164	Lab	3	2016-01-20 11:47:55.324293	2016-01-20 11:47:55.324293		lab	\N	t
1913	TemplateField	164	Urgent Care	13	2016-01-20 11:47:55.334879	2016-01-20 11:47:55.334879		urgent-care	\N	t
1914	TemplateField	164	Mental Nervous - Outpatient Coverage	15	2016-01-20 11:47:55.345476	2016-01-20 11:47:55.345476		mental-nervous-outpatient-coverage	\N	t
1915	TemplateField	164	Substance Abuse - Inpatient Coverage	16	2016-01-20 11:47:55.356879	2016-01-20 11:47:55.356879		substance-abuse-inpatient-coverage	\N	t
1916	TemplateField	164	Substance Abuse - Outpatient Coverage	17	2016-01-20 11:47:55.367799	2016-01-20 11:47:55.367799		substance-abuse-outpatient-coverage	\N	t
1917	TemplateField	164	Physical Therapy - Inpatient Coverage	18	2016-01-20 11:47:55.378046	2016-01-20 11:47:55.378046		physical-therapy-inpatient-coverage	\N	\N
1918	TemplateField	164	Physical Therapy - Outpatient Coverage	19	2016-01-20 11:47:55.388253	2016-01-20 11:47:55.388253		physical-therapy-outpatient-coverage	\N	t
1919	TemplateField	164	Occupational Therapy - Inpatient Coverage	20	2016-01-20 11:47:55.398677	2016-01-20 11:47:55.398677		occupational-therapy-inpatient-coverage	\N	\N
1920	TemplateField	164	Occupational Therapy - Outpatient Coverage	21	2016-01-20 11:47:55.409221	2016-01-20 11:47:55.409221		occupational-therapy-outpatient-coverage	\N	\N
1921	TemplateField	164	Speech Therapy - Inpatient Coverage	22	2016-01-20 11:47:55.419731	2016-01-20 11:47:55.419731		speech-therapy-inpatient-coverage	\N	\N
1922	TemplateField	164	Speech Therapy - Outpatient Coverage	23	2016-01-20 11:47:55.430075	2016-01-20 11:47:55.430075		speech-therapy-outpatient-coverage	\N	\N
1923	TemplateField	164	Pregnancy & Maternity Care - Office Visits	24	2016-01-20 11:47:55.440084	2016-01-20 11:47:55.440084		pregnancy-maternity-care-office-visits	\N	\N
1924	TemplateField	164	Pregnancy & Maternity Care - Labor & Delivery	25	2016-01-20 11:47:55.450331	2016-01-20 11:47:55.450331		pregnancy-maternity-care-labor-delivery	\N	\N
1925	TemplateField	164	Chiropractic Services	26	2016-01-20 11:47:55.460592	2016-01-20 11:47:55.460592		chiropractic-services	\N	t
1926	TemplateField	164	Ambulance	27	2016-01-20 11:47:55.470771	2016-01-20 11:47:55.470771		ambulance	\N	\N
1927	TemplateField	164	Hospice	28	2016-01-20 11:47:55.481207	2016-01-20 11:47:55.481207		hospice	\N	\N
1928	TemplateField	164	Home Healthcare	29	2016-01-20 11:47:55.491594	2016-01-20 11:47:55.491594		home-healthcare	\N	\N
1929	TemplateField	164	Skilled Nursing	30	2016-01-20 11:47:55.502055	2016-01-20 11:47:55.502055		skilled-nursing	\N	\N
1930	TemplateField	164	Infertility Coverage	31	2016-01-20 11:47:55.512254	2016-01-20 11:47:55.512254		infertility-coverage	\N	t
1931	TemplateField	164	Prosthetics	32	2016-01-20 11:47:55.522633	2016-01-20 11:47:55.522633		prosthetics	\N	\N
1932	TemplateField	164	Durable Medical Equipment	33	2016-01-20 11:47:55.533085	2016-01-20 11:47:55.533085		durable-medical-equipment	\N	t
1933	TemplateField	164	Hearing Devices	34	2016-01-20 11:47:55.543592	2016-01-20 11:47:55.543592		hearing-devices	\N	\N
1934	TemplateField	164	Vision Exams	35	2016-01-20 11:47:55.554113	2016-01-20 11:47:55.554113		vision-exams	\N	\N
1935	TemplateField	164	Short Term Rehabilitation - Inpatient	36	2016-01-20 11:47:55.564962	2016-01-20 11:47:55.564962		short-term-rehabilitation-inpatient	\N	\N
1936	TemplateField	164	Short Term Rehabilitation - Outpatient	37	2016-01-20 11:47:55.575251	2016-01-20 11:47:55.575251		short-term-rehabilitation-outpatient	\N	\N
1937	TemplateField	164	Telemedicine	38	2016-01-20 11:47:55.58543	2016-01-20 11:47:55.58543		telemedicine	\N	\N
1938	TemplateField	164	Specialty Rx Coverage	39	2016-01-20 11:47:55.595639	2016-01-20 11:47:55.595639		specialty-rx-coverage	\N	\N
1939	TemplateField	164	Primary Care Office Visit	1	2016-01-20 11:47:55.606253	2016-01-20 11:47:55.606253		primary-care-office-visit	t	t
1940	TemplateField	164	Mental Nervous - Inpatient Coverage	14	2016-01-20 11:47:55.616926	2016-01-20 11:47:55.616926		mental-nervous-inpatient-coverage	\N	t
1941	TemplateColumn	164	Subject to Deductible & Coinsurance	1	2016-01-20 11:47:55.629698	2016-01-20 11:47:55.629698		subject-to-deductible-coinsurance	\N	\N
1942	TemplateColumn	164	LIMIT	2	2016-01-20 11:47:55.640011	2016-01-20 11:47:55.640011		limit	\N	\N
1943	TemplateColumn	164	SEPERATE DEDUCTIBLE	3	2016-01-20 11:47:55.650229	2016-01-20 11:47:55.650229		seperate-deductible	\N	\N
1944	TemplateColumn	164	SEPERATE COINSURANCE	4	2016-01-20 11:47:55.660879	2016-01-20 11:47:55.660879		seperate-coinsurance	\N	\N
1945	TemplateField	165	Subject to Medical Deductible	1	2016-01-20 11:47:57.506488	2016-01-20 11:47:57.506488		subject-to-medical-deductible	\N	t
1946	TemplateField	165	Subject to Medical Deductible & Coinsurance	2	2016-01-20 11:47:57.517562	2016-01-20 11:47:57.517562		subject-to-medical-deductible-coinsurance	\N	\N
1947	TemplateField	165	Network	3	2016-01-20 11:47:57.527771	2016-01-20 11:47:57.527771		network	\N	t
1948	TemplateField	165	Step Therapy/Precertification Applies	4	2016-01-20 11:47:57.538311	2016-01-20 11:47:57.538311		step-therapy-precertification-applies	\N	\N
1949	TemplateField	165	Mandatory Generic	5	2016-01-20 11:47:57.548849	2016-01-20 11:47:57.548849		mandatory-generic	\N	\N
1950	TemplateField	165	Oral Contraceptive	6	2016-01-20 11:47:57.559128	2016-01-20 11:47:57.559128		oral-contraceptive	\N	\N
1951	TemplateField	165	Are Prescriptions Covered Out of Network	7	2016-01-20 11:47:57.569331	2016-01-20 11:47:57.569331		are-prescriptions-covered-out-of-network	\N	\N
1952	TemplateColumn	165	ENTRIES	1	2016-01-20 11:47:57.581927	2016-01-20 11:47:57.581927		entries	\N	\N
1953	TemplateField	166	Rx Deductible - Family	1	2016-01-20 11:47:57.678882	2016-01-20 11:47:57.678882		rx-deductible-family	t	t
1954	TemplateField	166	Rx Deductible - Individual	2	2016-01-20 11:47:57.689598	2016-01-20 11:47:57.689598		rx-deductible-individual	t	t
1955	TemplateColumn	166	DEDUCTIBLE	1	2016-01-20 11:47:57.702579	2016-01-20 11:47:57.702579		deductible	\N	\N
1956	TemplateColumn	166	PER MEMBER	2	2016-01-20 11:47:57.713578	2016-01-20 11:47:57.713578		per-member	\N	\N
1957	TemplateField	167	Retail Rx (Tier 1)	1	2016-01-20 11:47:57.78279	2016-01-20 11:47:57.78279		retail-rx-tier-1	t	t
1958	TemplateField	167	Retail Rx (Tier 2)	2	2016-01-20 11:47:57.79347	2016-01-20 11:47:57.79347		retail-rx-tier-2	t	t
1959	TemplateField	167	Retail Rx (Tier 3)	3	2016-01-20 11:47:57.80406	2016-01-20 11:47:57.80406		retail-rx-tier-3	t	t
1960	TemplateField	167	Retail Rx (Tier 4)	4	2016-01-20 11:47:57.814609	2016-01-20 11:47:57.814609		retail-rx-tier-4	\N	\N
1961	TemplateField	167	Retail Rx (Tier 5)	5	2016-01-20 11:47:57.825035	2016-01-20 11:47:57.825035		retail-rx-tier-5	\N	\N
1962	TemplateField	167	Mail Order Rx (Tier 1)	6	2016-01-20 11:47:57.835301	2016-01-20 11:47:57.835301		mail-order-rx-tier-1	\N	t
1963	TemplateField	167	Mail Order Rx (Tier 2 )	7	2016-01-20 11:47:57.846238	2016-01-20 11:47:57.846238		mail-order-rx-tier-2	\N	t
1964	TemplateField	167	Mail Order Rx (Tier 3)	8	2016-01-20 11:47:57.860413	2016-01-20 11:47:57.860413		mail-order-rx-tier-3	\N	t
1965	TemplateField	167	Mail Order Rx (Tier 4)	9	2016-01-20 11:47:57.871233	2016-01-20 11:47:57.871233		mail-order-rx-tier-4	\N	\N
1966	TemplateField	167	Mail Order Rx (Tier 5)	10	2016-01-20 11:47:57.882148	2016-01-20 11:47:57.882148		mail-order-rx-tier-5	\N	\N
1967	TemplateField	167	Speciality Medications - Mail Order	11	2016-01-20 11:47:57.892893	2016-01-20 11:47:57.892893		speciality-medications-mail-order	\N	\N
1968	TemplateField	167	Speciality Medications - Retail	12	2016-01-20 11:47:57.904008	2016-01-20 11:47:57.904008		speciality-medications-retail	\N	\N
1969	TemplateColumn	167	SUBJECT TO RX DEDUCTIBLE	1	2016-01-20 11:47:57.917534	2016-01-20 11:47:57.917534		subject-to-rx-deductible	\N	\N
1970	TemplateColumn	167	COINSURANCE	2	2016-01-20 11:47:57.928187	2016-01-20 11:47:57.928187		coinsurance	\N	\N
1971	TemplateField	168	Disease Management	1	2016-01-20 11:47:58.219711	2016-01-20 11:47:58.219711		disease-management	\N	\N
1972	TemplateColumn	168	IDENTIFICATION METHODS	1	2016-01-20 11:47:58.232572	2016-01-20 11:47:58.232572		identification-methods	\N	\N
1973	TemplateColumn	168	NUMBER OF CONDITIONS TRACKED	2	2016-01-20 11:47:58.242814	2016-01-20 11:47:58.242814		number-of-conditions-tracked	\N	\N
1974	TemplateColumn	168	OUTREACH METHODS	3	2016-01-20 11:47:58.253172	2016-01-20 11:47:58.253172		outreach-methods	\N	\N
1975	TemplateColumn	168	REPORT FREQUENCY	4	2016-01-20 11:47:58.2634	2016-01-20 11:47:58.2634		report-frequency	\N	\N
1976	TemplateField	169	Integrated Wellness Plan	1	2016-01-20 11:47:58.328619	2016-01-20 11:47:58.328619		integrated-wellness-plan	\N	\N
1977	TemplateColumn	169	ENTRIES	1	2016-01-20 11:47:58.340703	2016-01-20 11:47:58.340703		entries	\N	\N
1708	TemplateField	149	Adult Orthodontia	1	2016-01-20 11:35:19.463017	2016-01-20 11:50:17.396498		adult-orthodontia	t	t
1978	TemplateColumn	170	ENTRIES	1	2016-01-20 11:51:57.409217	2016-01-20 11:51:57.409217		entries	\N	\N
1980	TemplateField	170	Network	2	2016-01-20 11:52:32.585896	2016-01-20 13:04:24.756526		network	\N	t
1984	TemplateField	170	Deductible -Calendar or Policy Year	6	2016-01-20 11:53:10.225981	2016-01-20 13:04:42.577569		deductible-calendar-or-policy-year	\N	t
1981	TemplateField	170	Deductible Individual	3	2016-01-20 11:52:41.066681	2016-01-20 13:04:27.001353		deductible-individual	t	t
1982	TemplateField	170	Deductible Family	4	2016-01-20 11:52:50.365572	2016-01-20 13:04:31.985878		deductible-family	t	t
1985	TemplateField	170	Annual Plan Maximum (Per Enrollee)	7	2016-01-20 11:54:10.71992	2016-01-20 13:04:44.466628		annual-plan-maximum-per-enrollee	t	t
1989	TemplateField	170	International Employees Covered	11	2016-01-20 11:55:10.862537	2016-01-20 11:55:10.862537		international-employees-covered	\N	\N
1994	TemplateColumn	171	Covered	1	2016-01-20 12:00:05.867047	2016-01-20 12:00:05.867047		covered	\N	\N
1995	TemplateColumn	171	Not Covered	2	2016-01-20 12:00:37.032824	2016-01-20 12:00:37.032824		not-covered	\N	\N
1996	TemplateColumn	171	Subject to Deductible	3	2016-01-20 12:01:35.896195	2016-01-20 12:01:35.896195		subject-to-deductible	\N	\N
1997	TemplateField	153	Hospitalization - Inpatient	5	2016-01-20 12:01:49.989624	2016-01-20 12:01:49.989624		hospitalization-inpatient	\N	\N
1998	TemplateField	153	Hospitalization - Outpatient	6	2016-01-20 12:01:59.485758	2016-01-20 12:01:59.485758		hospitalization-outpatient	\N	\N
1999	TemplateColumn	171	Coinsurance	4	2016-01-20 12:02:14.071873	2016-01-20 12:02:14.071873		coinsurance	\N	\N
2000	TemplateField	153	Primary Care Office Visits	7	2016-01-20 12:02:14.411847	2016-01-20 12:02:14.411847		primary-care-office-visits	\N	\N
2001	TemplateField	153	Rx Deductible - Family	8	2016-01-20 12:02:26.258923	2016-01-20 12:02:26.258923		rx-deductible-family	\N	\N
2002	TemplateField	153	Rx Deductible - Individual	9	2016-01-20 12:02:35.233017	2016-01-20 12:02:35.233017		rx-deductible-individual	\N	\N
2003	TemplateField	153	Rx Drug (Tier 1)	10	2016-01-20 12:02:56.134846	2016-01-20 12:02:56.134846		rx-drug-tier-1	\N	\N
2004	TemplateField	153	Rx Drug (Tier 2)	11	2016-01-20 12:03:01.011593	2016-01-20 12:03:01.011593		rx-drug-tier-2	\N	\N
2005	TemplateColumn	172	Entries	1	2016-01-20 12:03:03.032185	2016-01-20 12:03:03.032185		entries	\N	\N
2006	TemplateField	153	Rx Drug (Tier 3)	12	2016-01-20 12:03:05.967194	2016-01-20 12:03:05.967194		rx-drug-tier-3	\N	\N
2007	TemplateField	153	Rx Drug (Tier 4)	13	2016-01-20 12:03:11.315731	2016-01-20 12:03:11.315731		rx-drug-tier-4	\N	\N
2008	TemplateField	153	Rx Drug (Tier 5)	14	2016-01-20 12:03:18.793173	2016-01-20 12:03:18.793173		rx-drug-tier-5	\N	\N
2009	TemplateField	153	Specialist Office Visit	15	2016-01-20 12:03:30.206538	2016-01-20 12:03:30.206538		specialist-office-visit	\N	\N
2011	TemplateColumn	153	REIMBURSEMENT AMOUNT	1	2016-01-20 12:03:49.33669	2016-01-20 12:03:49.33669		reimbursement-amount	\N	\N
2023	TemplateColumn	173	Covered	1	2016-01-20 12:07:47.032727	2016-01-20 12:07:47.032727		covered	\N	\N
2024	TemplateColumn	173	Not Covered	2	2016-01-20 12:08:23.709935	2016-01-20 12:08:23.709935		not-covered	\N	\N
2028	TemplateField	174	Investment Options	4	2016-01-20 12:11:57.875813	2016-01-20 12:11:57.875813		investment-options	\N	\N
2029	TemplateColumn	174	ENTRIES	1	2016-01-20 12:12:06.142193	2016-01-20 12:12:06.142193		entries	\N	\N
2030	TemplateField	174	Mutual Fund Minimum Investment	5	2016-01-20 12:22:09.909735	2016-01-20 12:22:09.909735		mutual-fund-minimum-investment	\N	\N
2031	TemplateColumn	175	ENTRIES	1	2016-01-20 12:26:06.098667	2016-01-20 12:26:06.098667		entries	\N	\N
2040	TemplateField	175	Annual Rollover Maximum	9	2016-01-20 12:32:40.707115	2016-01-20 12:32:40.707115		annual-rollover-maximum	\N	\N
2042	TemplateField	175	International Employees Covered	11	2016-01-20 12:33:06.187209	2016-01-20 12:33:06.187209		international-employees-covered	\N	\N
2043	TemplateField	175	Waiting Period for Services	12	2016-01-20 12:33:14.652038	2016-01-20 12:33:14.652038		waiting-period-for-services	\N	\N
2033	TemplateField	175	Network	2	2016-01-20 12:26:24.467883	2016-01-20 13:01:49.807751		network	\N	t
2035	TemplateField	175	Deductible Family	4	2016-01-20 12:27:39.765871	2016-01-20 13:01:59.857265		deductible-family	t	t
2041	TemplateField	175	OON Reimbursement Level	10	2016-01-20 12:32:50.282871	2016-01-20 13:02:12.06284		oon-reimbursement-level	\N	t
2034	TemplateField	175	Deductible Individual	3	2016-01-20 12:26:34.607053	2016-01-20 13:01:59.252388		deductible-individual	t	t
2039	TemplateField	175	Annual Rollover Benefit	8	2016-01-20 12:32:20.892573	2016-01-20 13:02:18.122788		annual-rollover-benefit	\N	t
2037	TemplateField	175	Deductible -Calendar or Policy Year	6	2016-01-20 12:31:46.414794	2016-01-20 13:02:31.587606		deductible-calendar-or-policy-year	\N	t
1986	TemplateField	170	Annual Rollover Benefit	8	2016-01-20 11:54:21.224892	2016-01-20 13:04:45.160739		annual-rollover-benefit	\N	t
2038	TemplateField	175	Annual Plan Maximum (Per Enrollee)	7	2016-01-20 12:32:10.724637	2016-01-20 13:02:33.849529		annual-plan-maximum-per-enrollee	t	t
2044	TemplateField	176	Diagnostic and Preventive (Type A)	1	2016-01-20 12:46:06.33939	2016-01-20 13:02:51.512292		diagnostic-and-preventive-type-a	t	t
2046	TemplateField	176	Major Services (Type C)	3	2016-01-20 12:46:24.793607	2016-01-20 13:02:48.132033		major-services-type-c	t	t
2045	TemplateField	176	Basic Services (Type B)	2	2016-01-20 12:46:15.274389	2016-01-20 13:02:50.471722		basic-services-type-b	t	t
1987	TemplateField	170	Annual Rollover Maximum	9	2016-01-20 11:54:41.458527	2016-01-20 13:04:45.971368		annual-rollover-maximum	\N	t
1988	TemplateField	170	OON Reimbursement Level	10	2016-01-20 11:54:52.728157	2016-01-20 13:04:47.150956		oon-reimbursement-level	\N	t
1990	TemplateField	170	Waiting Period for Services	12	2016-01-20 11:55:20.662467	2016-01-20 13:04:52.955557		waiting-period-for-services	\N	t
2010	TemplateField	172	Cleanings	1	2016-01-20 12:03:47.795181	2016-01-20 13:05:09.786974		cleanings	\N	t
1991	TemplateField	171	Diagnostic and Preventive (Type A)	1	2016-01-20 11:59:36.618186	2016-01-20 13:05:02.010798		diagnostic-and-preventive-type-a	t	t
1992	TemplateField	171	Basic Services (Type B)	2	2016-01-20 11:59:47.208161	2016-01-20 13:05:01.175458		basic-services-type-b	t	t
1993	TemplateField	171	Major Services (Type C)	3	2016-01-20 11:59:57.113153	2016-01-20 13:05:00.527943		major-services-type-c	t	t
2012	TemplateField	172	Sealants	2	2016-01-20 12:03:58.842041	2016-01-20 13:05:10.516518		sealants	\N	t
2013	TemplateField	172	Fillings (Amalgam & Composite)	3	2016-01-20 12:04:11.170886	2016-01-20 13:05:11.20044		fillings-amalgam-composite	\N	t
2014	TemplateField	172	Simple Extractions	4	2016-01-20 12:04:21.638599	2016-01-20 13:05:12.641509		simple-extractions	\N	t
2015	TemplateField	172	Oral Surgery	5	2016-01-20 12:04:33.169399	2016-01-20 13:05:13.36071		oral-surgery	\N	t
2016	TemplateField	172	Periodontics	6	2016-01-20 12:04:46.960065	2016-01-20 13:05:14.881759		periodontics	\N	t
2018	TemplateField	172	Implants	8	2016-01-20 12:05:06.807909	2016-01-20 13:05:16.201888		implants	\N	t
2019	TemplateField	172	Prosthodontics (including Dentures)	9	2016-01-20 12:05:16.998807	2016-01-20 13:05:16.942614		prosthodontics-including-dentures	\N	t
2017	TemplateField	172	Endodontics	7	2016-01-20 12:04:55.808674	2016-01-20 13:05:17.68041		endodontics	\N	t
2020	TemplateField	172	Inlays/Onlays/Crowns	10	2016-01-20 12:05:40.832555	2016-01-20 13:05:19.160949		inlays-onlays-crowns	\N	t
2025	TemplateField	174	Claim Adjudication	1	2016-01-20 12:11:14.727841	2016-01-20 20:02:35.115309		claim-adjudication	\N	t
2021	TemplateField	173	Adult Orthodontia	1	2016-01-20 12:07:30.925831	2016-01-20 13:05:26.322609		adult-orthodontia	t	t
2022	TemplateField	173	Child Orthodontia	2	2016-01-20 12:07:39.636661	2016-01-20 13:05:25.351519		child-orthodontia	t	t
2026	TemplateField	174	Employer Funding	2	2016-01-20 12:11:24.526688	2016-01-20 20:02:38.973282		employer-funding	\N	t
2027	TemplateField	174	HSA Vendor Relationship	3	2016-01-20 12:11:37.136861	2016-01-20 20:03:03.587018		hsa-vendor-relationship	\N	t
2047	TemplateColumn	176	Covered	1	2016-01-20 12:46:36.839291	2016-01-20 12:46:36.839291		covered	\N	\N
2048	TemplateColumn	176	Not Covered	2	2016-01-20 12:46:50.073399	2016-01-20 12:46:50.073399		not-covered	\N	\N
2049	TemplateColumn	176	Subject to Deductible	3	2016-01-20 12:48:19.172696	2016-01-20 12:48:19.172696		subject-to-deductible	\N	\N
2050	TemplateColumn	176	Coinsurance	4	2016-01-20 12:49:01.149561	2016-01-20 12:49:01.149561		coinsurance	\N	\N
2051	TemplateColumn	176	Copays Apply	5	2016-01-20 12:49:10.780132	2016-01-20 12:49:10.780132		copays-apply	\N	\N
2052	TemplateColumn	177	Entries	1	2016-01-20 12:50:54.987977	2016-01-20 12:50:54.987977		entries	\N	\N
2065	TemplateColumn	178	Covered	1	2016-01-20 12:57:39.160369	2016-01-20 12:57:39.160369		covered	\N	\N
2066	TemplateColumn	178	Not Covered	2	2016-01-20 12:57:51.450129	2016-01-20 12:57:51.450129		not-covered	\N	\N
2067	TemplateColumn	178	Ortho Deductible	3	2016-01-20 12:58:34.277013	2016-01-20 12:58:34.277013		ortho-deductible	\N	\N
2068	TemplateColumn	178	Coinsurance	4	2016-01-20 12:58:45.416762	2016-01-20 12:58:45.416762		coinsurance	\N	\N
2069	TemplateColumn	178	Lifetime Maximum	5	2016-01-20 12:58:57.379278	2016-01-20 12:58:57.379278		lifetime-maximum	\N	\N
2070	TemplateColumn	178	Copays Apply	6	2016-01-20 12:59:08.137567	2016-01-20 12:59:08.137567		copays-apply	\N	\N
2032	TemplateField	175	Plan Type	1	2016-01-20 12:26:15.041322	2016-01-20 13:01:58.562782		plan-type	t	t
2036	TemplateField	175	Deductible Waived for Preventive Care	5	2016-01-20 12:27:48.200622	2016-01-20 13:02:00.372361		deductible-waived-for-preventive-care	t	t
2053	TemplateField	177	Cleanings	1	2016-01-20 12:51:06.398567	2016-01-20 13:02:58.597116		cleanings	\N	t
2054	TemplateField	177	Sealants	2	2016-01-20 12:51:24.401303	2016-01-20 13:02:59.193422		sealants	\N	t
2055	TemplateField	177	Fillings (Amalgam & Composite)	3	2016-01-20 12:51:46.031035	2016-01-20 13:03:00.131876		fillings-amalgam-composite	\N	t
2057	TemplateField	177	Oral Surgery	5	2016-01-20 12:52:48.770317	2016-01-20 13:03:02.3378		oral-surgery	\N	t
2056	TemplateField	177	Simple Extractions	4	2016-01-20 12:52:34.765345	2016-01-20 13:03:02.391624		simple-extractions	\N	t
2058	TemplateField	177	Periodontics	6	2016-01-20 12:53:02.674578	2016-01-20 13:03:02.96777		periodontics	\N	t
2059	TemplateField	177	Endodontics	7	2016-01-20 12:53:13.164371	2016-01-20 13:03:04.531324		endodontics	\N	t
2060	TemplateField	177	Implants	8	2016-01-20 12:53:23.29005	2016-01-20 13:03:04.983329		implants	\N	t
2061	TemplateField	177	Prosthodontics (including Dentures)	9	2016-01-20 12:53:33.394177	2016-01-20 13:03:05.737744		prosthodontics-including-dentures	\N	t
2062	TemplateField	177	Inlays/Onlays/Crowns	10	2016-01-20 12:53:42.854835	2016-01-20 13:03:06.443059		inlays-onlays-crowns	\N	t
2064	TemplateField	178	Child Orthodontia	2	2016-01-20 12:57:27.846918	2016-01-20 13:03:14.087474		child-orthodontia	t	t
2063	TemplateField	178	Adult Orthodontia	1	2016-01-20 12:57:18.532055	2016-01-20 13:03:15.047496		adult-orthodontia	t	t
1979	TemplateField	170	Plan Type	1	2016-01-20 11:52:11.58285	2016-01-20 13:04:23.651865		plan-type	t	t
1983	TemplateField	170	Deductible Waived for Preventive Care	5	2016-01-20 11:53:01.506485	2016-01-20 13:04:30.990806		deductible-waived-for-preventive-care	t	t
2071	TemplateColumn	173	Ortho Deductible	3	2016-01-20 19:43:54.841574	2016-01-20 19:43:54.841574		ortho-deductible	\N	\N
2072	TemplateColumn	173	Coinsurance	4	2016-01-20 19:44:07.264188	2016-01-20 19:44:07.264188		coinsurance	\N	\N
2073	TemplateColumn	173	Lifetime Maximum	5	2016-01-20 19:44:23.996379	2016-01-20 19:44:23.996379		lifetime-maximum	\N	\N
2074	TemplateField	179	Biometric Screening	2	2016-01-20 19:58:41.468124	2016-01-20 19:58:41.468124		biometric-screening	t	t
2075	TemplateField	179	Health Risk Assesment	3	2016-01-20 19:58:41.483288	2016-01-20 19:58:41.483288		health-risk-assesment	t	t
2076	TemplateField	179	Incentives	4	2016-01-20 19:58:41.493833	2016-01-20 19:58:41.493833		incentives	t	t
2077	TemplateField	179	Health Coaching	5	2016-01-20 19:58:41.504315	2016-01-20 19:58:41.504315		health-coaching	t	t
2078	TemplateField	179	Management Sponsored	6	2016-01-20 19:58:41.514777	2016-01-20 19:58:41.514777		management-sponsored	\N	\N
2079	TemplateField	179	Discounts of Wellness Products	7	2016-01-20 19:58:41.527566	2016-01-20 19:58:41.527566		discounts-of-wellness-products	t	t
2080	TemplateField	179	Online Health Management Tools	8	2016-01-20 19:58:41.539026	2016-01-20 19:58:41.539026		online-health-management-tools	\N	\N
2081	TemplateField	179	Communication Materials	9	2016-01-20 19:58:41.549359	2016-01-20 19:58:41.549359		communication-materials	\N	\N
2082	TemplateField	179	Type of Plan	1	2016-01-20 19:58:41.559879	2016-01-20 19:58:41.559879		type-of-plan	t	t
2083	TemplateColumn	179	ENTRIES	1	2016-01-20 19:58:41.574717	2016-01-20 19:58:41.574717		entries	\N	\N
2084	TemplateField	180	Employee Eligibility Class #1	1	2016-01-20 19:58:41.704398	2016-01-20 19:58:41.704398		employee-eligibility-class-1	\N	\N
2085	TemplateField	180	Employee Eligibility Class #2	2	2016-01-20 19:58:41.715205	2016-01-20 19:58:41.715205		employee-eligibility-class-2	\N	\N
2086	TemplateField	180	Employee Eligibility Class #3	3	2016-01-20 19:58:41.726007	2016-01-20 19:58:41.726007		employee-eligibility-class-3	\N	\N
2087	TemplateField	180	Employee Eligibility Class #4	4	2016-01-20 19:58:41.736847	2016-01-20 19:58:41.736847		employee-eligibility-class-4	\N	\N
2088	TemplateColumn	180	DOMESTIC PARTNER ELIGIBLE	7	2016-01-20 19:58:41.750406	2016-01-20 19:58:41.750406		domestic-partner-eligible	\N	\N
2089	TemplateColumn	180	DEPENDENT CHILD LIMITING AGE	8	2016-01-20 19:58:41.761536	2016-01-20 19:58:41.761536		dependent-child-limiting-age	\N	\N
2090	TemplateColumn	180	EMPLOYEE CLASS	1	2016-01-20 19:58:41.77228	2016-01-20 19:58:41.77228		employee-class	\N	\N
2091	TemplateColumn	180	HOURS REQUIREMENT	4	2016-01-20 19:58:41.782809	2016-01-20 19:58:41.782809		hours-requirement	\N	\N
2092	TemplateColumn	180	WAITING PERIOD	3	2016-01-20 19:58:41.793685	2016-01-20 19:58:41.793685		waiting-period	\N	\N
2093	TemplateColumn	180	EMPLOYEE CLASSES	2	2016-01-20 19:58:41.804425	2016-01-20 19:58:41.804425		employee-classes	\N	\N
2094	TemplateColumn	180	SPOUSE ELIGIBLE	5	2016-01-20 19:58:41.815023	2016-01-20 19:58:41.815023		spouse-eligible	\N	\N
2095	TemplateColumn	180	DEPENDENT CHILDREN ELIGIBLE	6	2016-01-20 19:58:41.82574	2016-01-20 19:58:41.82574		dependent-children-eligible	\N	\N
2096	TemplateColumn	180	DEPENDENT CHILD COVERAGE ENDS	9	2016-01-20 19:58:41.836259	2016-01-20 19:58:41.836259		dependent-child-coverage-ends	\N	\N
2097	TemplateField	181	Biometric Screening	2	2016-01-20 19:59:49.112492	2016-01-20 19:59:49.112492		biometric-screening	t	t
2098	TemplateField	181	Health Risk Assesment	3	2016-01-20 19:59:49.124218	2016-01-20 19:59:49.124218		health-risk-assesment	t	t
2099	TemplateField	181	Incentives	4	2016-01-20 19:59:49.134975	2016-01-20 19:59:49.134975		incentives	t	t
2100	TemplateField	181	Health Coaching	5	2016-01-20 19:59:49.145169	2016-01-20 19:59:49.145169		health-coaching	t	t
2101	TemplateField	181	Management Sponsored	6	2016-01-20 19:59:49.155315	2016-01-20 19:59:49.155315		management-sponsored	\N	\N
2103	TemplateField	181	Online Health Management Tools	8	2016-01-20 19:59:49.175483	2016-01-20 19:59:49.175483		online-health-management-tools	\N	\N
2104	TemplateField	181	Communication Materials	9	2016-01-20 19:59:49.185571	2016-01-20 19:59:49.185571		communication-materials	\N	\N
2105	TemplateField	181	Type of Plan	1	2016-01-20 19:59:49.196181	2016-01-20 19:59:49.196181		type-of-plan	t	t
2107	TemplateField	182	Employee Eligibility Class #1	1	2016-01-20 19:59:49.325278	2016-01-20 19:59:49.325278		employee-eligibility-class-1	\N	\N
2108	TemplateField	182	Employee Eligibility Class #2	2	2016-01-20 19:59:49.33642	2016-01-20 19:59:49.33642		employee-eligibility-class-2	\N	\N
2109	TemplateField	182	Employee Eligibility Class #3	3	2016-01-20 19:59:49.346751	2016-01-20 19:59:49.346751		employee-eligibility-class-3	\N	\N
2110	TemplateField	182	Employee Eligibility Class #4	4	2016-01-20 19:59:49.357205	2016-01-20 19:59:49.357205		employee-eligibility-class-4	\N	\N
2102	TemplateField	181	Discounts on Wellness Products	7	2016-01-20 19:59:49.165448	2016-01-20 19:59:58.763951		discounts-on-wellness-products	t	t
2120	TemplateField	183	Employee	1	2016-01-20 20:04:31.873707	2016-01-20 20:04:31.873707		employee	\N	\N
2121	TemplateField	183	Employee +1	2	2016-01-20 20:04:43.987746	2016-01-20 20:04:43.987746		employee-1	\N	\N
2122	TemplateField	183	Employee +2 or more	3	2016-01-20 20:04:56.545795	2016-01-20 20:04:56.545795		employee-2-or-more	\N	\N
2123	TemplateField	183	Employee + Child(ren)	4	2016-01-20 20:05:12.12516	2016-01-20 20:05:12.12516		employee-child-ren	\N	\N
2124	TemplateField	183	Employee & Spouse	5	2016-01-20 20:05:23.448965	2016-01-20 20:05:23.448965		employee-spouse	\N	\N
2125	TemplateColumn	183	Annual Funding Amount	1	2016-01-20 20:05:46.054076	2016-01-20 20:05:46.054076		annual-funding-amount	\N	\N
2126	TemplateField	184	HRA Vendor Relationship	1	2016-01-20 20:10:25.901241	2016-01-20 20:10:25.901241		hra-vendor-relationship	\N	\N
2127	TemplateField	184	Type of HRA	2	2016-01-20 20:10:25.912426	2016-01-20 20:10:25.912426		type-of-hra	\N	\N
2130	TemplateField	185	Claim Adjudication Options	2	2016-01-20 20:10:25.98703	2016-01-20 20:10:25.98703		claim-adjudication-options	\N	\N
2131	TemplateField	185	Do Funds Roll Over?	3	2016-01-20 20:10:25.998179	2016-01-20 20:10:25.998179		do-funds-roll-over	\N	\N
2132	TemplateField	185	If Yes, what is the limit?	4	2016-01-20 20:10:26.009752	2016-01-20 20:10:26.009752		if-yes-what-is-the-limit	\N	\N
2135	TemplateField	186	Employee	1	2016-01-20 20:10:26.115405	2016-01-20 20:10:26.115405		employee	\N	\N
2136	TemplateField	186	Employee +1	2	2016-01-20 20:10:26.126335	2016-01-20 20:10:26.126335		employee-1	\N	\N
2137	TemplateField	186	Employee +2 or more	3	2016-01-20 20:10:26.137372	2016-01-20 20:10:26.137372		employee-2-or-more	\N	\N
2138	TemplateField	186	Employee + Child(ren)	4	2016-01-20 20:10:26.14784	2016-01-20 20:10:26.14784		employee-child-ren	\N	\N
2139	TemplateField	186	Employee + Spouse	5	2016-01-20 20:10:26.158285	2016-01-20 20:10:26.158285		employee-spouse	\N	\N
2140	TemplateField	186	Famliy	6	2016-01-20 20:10:26.168954	2016-01-20 20:10:26.168954		famliy	\N	\N
2141	TemplateColumn	186	REIMBURSEMENT AMOUNT	1	2016-01-20 20:10:26.18694	2016-01-20 20:10:26.18694		reimbursement-amount	\N	\N
2142	TemplateField	187	Co-Insurance	1	2016-01-20 20:10:26.289877	2016-01-20 20:10:26.289877		co-insurance	\N	\N
2129	TemplateField	185	Account balance options upon EE disenrollmen	1	2016-01-20 20:10:25.97546	2016-01-20 20:11:11.153823		account-balance-options-upon-ee-disenrollmen	\N	\N
2133	TemplateField	185	What funds are eligible for the HRA?	5	2016-01-20 20:10:26.020437	2016-01-20 20:11:37.844133		what-funds-are-eligible-for-the-hra	\N	\N
2143	TemplateField	187	Deductible Family	3	2016-01-20 20:10:26.302135	2016-01-20 20:12:16.648224		deductible-family	\N	\N
2144	TemplateField	187	Deductible Individual	5	2016-01-20 20:10:26.3129	2016-01-20 20:12:16.714439		deductible-individual	\N	\N
2146	TemplateField	187	Hospitalization - Inpatient	9	2016-01-20 20:10:26.336064	2016-01-20 20:12:16.780301		hospitalization-inpatient	\N	\N
2145	TemplateField	187	Emergency Room	7	2016-01-20 20:10:26.323389	2016-01-20 20:12:16.809963		emergency-room	\N	\N
2147	TemplateField	187	Hospitalization - Outpatient	11	2016-01-20 20:10:26.346705	2016-01-20 20:12:16.840914		hospitalization-outpatient	\N	\N
2106	TemplateColumn	181	Entries	1	2016-01-20 19:59:49.208387	2016-01-23 14:49:51.511024		entries	\N	\N
2113	TemplateColumn	182	Employee Class	1	2016-01-20 19:59:49.39003	2016-01-23 15:52:21.03227		employee-class	\N	\N
2116	TemplateColumn	182	Employee Classes	2	2016-01-20 19:59:49.420864	2016-01-23 15:52:28.322833		employee-classes	\N	\N
2115	TemplateColumn	182	Waiting Period	3	2016-01-20 19:59:49.41027	2016-01-23 15:52:56.227224		waiting-period	\N	\N
2114	TemplateColumn	182	Hours Requirement	4	2016-01-20 19:59:49.399996	2016-01-23 15:53:09.916967		hours-requirement	\N	\N
2117	TemplateColumn	182	Spouse Eligible	5	2016-01-20 19:59:49.431246	2016-01-23 15:53:24.966088		spouse-eligible	\N	\N
2111	TemplateColumn	182	Domestic Partner Eligible	7	2016-01-20 19:59:49.369742	2016-01-23 15:54:08.966192		domestic-partner-eligible	\N	\N
2112	TemplateColumn	182	Dependent Child Limiting Age	8	2016-01-20 19:59:49.379821	2016-01-23 15:54:23.525938		dependent-child-limiting-age	\N	\N
2119	TemplateColumn	182	Dependent Child Coverage Ends	9	2016-01-20 19:59:49.452054	2016-01-23 15:54:38.36069		dependent-child-coverage-ends	\N	\N
2128	TemplateColumn	184	Entries	1	2016-01-20 20:10:25.928241	2016-01-23 15:56:41.919225		entries	\N	\N
2134	TemplateColumn	185	Entries	1	2016-01-20 20:10:26.033643	2016-01-23 15:56:46.092699		entries	\N	\N
2156	TemplateField	187	Specialist Office Visit	15	2016-01-20 20:10:26.453519	2016-01-20 20:10:26.453519		specialist-office-visit	\N	\N
2157	TemplateColumn	187	REIMBURSEMENT AMOUNT	1	2016-01-20 20:10:26.467045	2016-01-20 20:10:26.467045		reimbursement-amount	\N	\N
2148	TemplateField	187	Primary Care Office Visits	13	2016-01-20 20:10:26.357856	2016-01-20 20:12:16.872489		primary-care-office-visits	\N	\N
2151	TemplateField	187	Rx Drug (Tier 1)	21	2016-01-20 20:10:26.395062	2016-01-20 20:12:16.964231		rx-drug-tier-1	\N	\N
2152	TemplateField	187	Rx Drug (Tier 2)	23	2016-01-20 20:10:26.40727	2016-01-20 20:12:16.994626		rx-drug-tier-2	\N	\N
2153	TemplateField	187	Rx Drug (Tier 3)	25	2016-01-20 20:10:26.418417	2016-01-20 20:12:17.026684		rx-drug-tier-3	\N	\N
2154	TemplateField	187	Rx Drug (Tier 4)	27	2016-01-20 20:10:26.429927	2016-01-20 20:12:17.057084		rx-drug-tier-4	\N	\N
2155	TemplateField	187	Rx Drug (Tier 5)	29	2016-01-20 20:10:26.441362	2016-01-20 20:12:17.086802		rx-drug-tier-5	\N	\N
2150	TemplateField	187	Rx Deductible - Individual	17	2016-01-20 20:10:26.3838	2016-01-20 20:17:51.975359		rx-deductible-individual	\N	\N
2149	TemplateField	187	Rx Deductible - Family	19	2016-01-20 20:10:26.371487	2016-01-20 20:17:52.037985		rx-deductible-family	\N	\N
2158	TemplateField	188	Plan Type	1	2016-01-21 09:05:56.770203	2016-01-21 09:05:56.770203		plan-type	t	t
2159	TemplateField	188	Network	3	2016-01-21 09:05:56.787829	2016-01-21 09:05:56.787829		network	t	t
2160	TemplateField	188	Referrals Needed	5	2016-01-21 09:05:56.798159	2016-01-21 09:05:56.798159		referrals-needed	t	t
2161	TemplateField	188	Deductible Individual	7	2016-01-21 09:05:56.808242	2016-01-21 09:05:56.808242		deductible-individual	t	t
2162	TemplateField	188	Deductible Family	9	2016-01-21 09:05:56.8186	2016-01-21 09:05:56.8186		deductible-family	t	t
2163	TemplateField	188	Deductible - Family Multiple	11	2016-01-21 09:05:56.828984	2016-01-21 09:05:56.828984		deductible-family-multiple	\N	\N
2164	TemplateField	188	Deductible Format	13	2016-01-21 09:05:56.840302	2016-01-21 09:05:56.840302		deductible-format	\N	t
2165	TemplateField	188	Coinsurance	15	2016-01-21 09:05:56.851369	2016-01-21 09:05:56.851369		coinsurance	t	t
2166	TemplateField	188	Out-of-Pocket Max Family (includes deductible)	19	2016-01-21 09:05:56.861412	2016-01-21 09:05:56.861412		out-of-pocket-max-family-includes-deductible	t	t
2167	TemplateField	188	In & Out-of-Network Deductibles Cross Accumulation	23	2016-01-21 09:05:56.871653	2016-01-21 09:05:56.871653		in-out-of-network-deductibles-cross-accumulation	\N	t
2168	TemplateField	188	OON Reimbursement Level	27	2016-01-21 09:05:56.881755	2016-01-21 09:05:56.881755		oon-reimbursement-level	t	t
2169	TemplateField	188	Annual Maximum	29	2016-01-21 09:05:56.891745	2016-01-21 09:05:56.891745		annual-maximum	\N	\N
2170	TemplateField	188	Gym Reimbursement	31	2016-01-21 09:05:56.901665	2016-01-21 09:05:56.901665		gym-reimbursement	\N	\N
2171	TemplateField	188	Customer Service Days/Hours	33	2016-01-21 09:05:56.911593	2016-01-21 09:05:56.911593		customer-service-days-hours	\N	\N
2172	TemplateField	188	Nurseline	35	2016-01-21 09:05:56.921448	2016-01-21 09:05:56.921448		nurseline	\N	\N
2173	TemplateField	188	International Employees Covered	37	2016-01-21 09:05:56.932771	2016-01-21 09:05:56.932771		international-employees-covered	\N	\N
2174	TemplateField	188	Out-of-Pocket Max Individual (includes deductible)	17	2016-01-21 09:05:56.942871	2016-01-21 09:05:56.942871		out-of-pocket-max-individual-includes-deductible	t	t
2175	TemplateField	188	Out-of-Pocket Maximum - Family Multiple	21	2016-01-21 09:05:56.952926	2016-01-21 09:05:56.952926		out-of-pocket-maximum-family-multiple	\N	f
2176	TemplateField	188	In & Out-of-Network OOP Maximum Cross Accumulation	25	2016-01-21 09:05:56.963006	2016-01-21 09:05:56.963006		in-out-of-network-oop-maximum-cross-accumulation	\N	t
2177	TemplateColumn	188	ENTRIES	1	2016-01-21 09:05:56.982298	2016-01-21 09:05:56.982298		entries	\N	\N
2178	TemplateField	189	Primary Care Office Visit	1	2016-01-21 09:05:57.23705	2016-01-21 09:05:57.23705		primary-care-office-visit	t	t
2179	TemplateField	189	Specialist Office Visit	2	2016-01-21 09:05:57.247153	2016-01-21 09:05:57.247153		specialist-office-visit	t	t
2180	TemplateField	189	Lab	3	2016-01-21 09:05:57.257229	2016-01-21 09:05:57.257229		lab	\N	t
2181	TemplateField	189	General Radiology	4	2016-01-21 09:05:57.267475	2016-01-21 09:05:57.267475		general-radiology	\N	\N
2182	TemplateField	189	CT/MRI/PET Scans	5	2016-01-21 09:05:57.277136	2016-01-21 09:05:57.277136		ct-mri-pet-scans	\N	\N
2183	TemplateField	189	Annual Physical Exam	6	2016-01-21 09:05:57.2874	2016-01-21 09:05:57.2874		annual-physical-exam	\N	\N
2184	TemplateField	189	Well Child Exams	7	2016-01-21 09:05:57.297835	2016-01-21 09:05:57.297835		well-child-exams	\N	\N
2185	TemplateField	189	Pediatric Dental	8	2016-01-21 09:05:57.30985	2016-01-21 09:05:57.30985		pediatric-dental	\N	\N
2186	TemplateField	189	Hospitalization - Inpatient	9	2016-01-21 09:05:57.320164	2016-01-21 09:05:57.320164		hospitalization-inpatient	t	t
2187	TemplateField	189	Hospitalization - Outpatient	10	2016-01-21 09:05:57.330256	2016-01-21 09:05:57.330256		hospitalization-outpatient	\N	t
2188	TemplateField	189	Surgery/Anesthesiology	11	2016-01-21 09:05:57.340325	2016-01-21 09:05:57.340325		surgery-anesthesiology	\N	\N
2189	TemplateField	189	Emergency Room	12	2016-01-21 09:05:57.350291	2016-01-21 09:05:57.350291		emergency-room	t	t
2190	TemplateField	189	Urgent Care	13	2016-01-21 09:05:57.360421	2016-01-21 09:05:57.360421		urgent-care	\N	t
2191	TemplateField	189	Mental Nervous - Inpatient Coverage	14	2016-01-21 09:05:57.370522	2016-01-21 09:05:57.370522		mental-nervous-inpatient-coverage	\N	t
2192	TemplateField	189	Mental Nervous - Outpatient Coverage	15	2016-01-21 09:05:57.380595	2016-01-21 09:05:57.380595		mental-nervous-outpatient-coverage	\N	t
2193	TemplateField	189	Substance Abuse - Inpatient Coverage	16	2016-01-21 09:05:57.390372	2016-01-21 09:05:57.390372		substance-abuse-inpatient-coverage	\N	t
2194	TemplateField	189	Physical Therapy - Inpatient Coverage	18	2016-01-21 09:05:57.400311	2016-01-21 09:05:57.400311		physical-therapy-inpatient-coverage	\N	\N
2195	TemplateField	189	Physical Therapy - Outpatient Coverage	19	2016-01-21 09:05:57.410078	2016-01-21 09:05:57.410078		physical-therapy-outpatient-coverage	\N	t
2196	TemplateField	189	Occupational Therapy - Inpatient Coverage	20	2016-01-21 09:05:57.420444	2016-01-21 09:05:57.420444		occupational-therapy-inpatient-coverage	\N	\N
2197	TemplateField	189	Occupational Therapy - Outpatient Coverage	21	2016-01-21 09:05:57.430881	2016-01-21 09:05:57.430881		occupational-therapy-outpatient-coverage	\N	\N
2198	TemplateField	189	Speech Therapy - Inpatient Coverage	22	2016-01-21 09:05:57.441381	2016-01-21 09:05:57.441381		speech-therapy-inpatient-coverage	\N	\N
2199	TemplateField	189	Speech Therapy - Outpatient Coverage	23	2016-01-21 09:05:57.451488	2016-01-21 09:05:57.451488		speech-therapy-outpatient-coverage	\N	\N
2200	TemplateField	189	Pregnancy & Maternity Care - Office Visits	24	2016-01-21 09:05:57.461521	2016-01-21 09:05:57.461521		pregnancy-maternity-care-office-visits	\N	\N
2201	TemplateField	189	Pregnancy & Maternity Care - Labor & Delivery	25	2016-01-21 09:05:57.471318	2016-01-21 09:05:57.471318		pregnancy-maternity-care-labor-delivery	\N	\N
2202	TemplateField	189	Chiropractic Services	26	2016-01-21 09:05:57.481327	2016-01-21 09:05:57.481327		chiropractic-services	\N	t
2203	TemplateField	189	Ambulance	27	2016-01-21 09:05:57.49134	2016-01-21 09:05:57.49134		ambulance	\N	\N
2204	TemplateField	189	Hospice	28	2016-01-21 09:05:57.501127	2016-01-21 09:05:57.501127		hospice	\N	\N
2205	TemplateField	189	Home Healthcare	29	2016-01-21 09:05:57.512834	2016-01-21 09:05:57.512834		home-healthcare	\N	\N
2206	TemplateField	189	Skilled Nursing	30	2016-01-21 09:05:57.522703	2016-01-21 09:05:57.522703		skilled-nursing	\N	\N
2207	TemplateField	189	Infertility Coverage	31	2016-01-21 09:05:57.532459	2016-01-21 09:05:57.532459		infertility-coverage	\N	t
2208	TemplateField	189	Prosthetics	32	2016-01-21 09:05:57.542357	2016-01-21 09:05:57.542357		prosthetics	\N	\N
2209	TemplateField	189	Durable Medical Equipment	33	2016-01-21 09:05:57.552626	2016-01-21 09:05:57.552626		durable-medical-equipment	\N	t
2210	TemplateField	189	Hearing Devices	34	2016-01-21 09:05:57.562628	2016-01-21 09:05:57.562628		hearing-devices	\N	\N
2211	TemplateField	189	Vision Exams	35	2016-01-21 09:05:57.572674	2016-01-21 09:05:57.572674		vision-exams	\N	\N
2212	TemplateField	189	Short Term Rehabilitation- Inpatient	36	2016-01-21 09:05:57.582573	2016-01-21 09:05:57.582573		short-term-rehabilitation-inpatient	\N	\N
2213	TemplateField	189	Short Term Rehabilitation - Outpatient	37	2016-01-21 09:05:57.59252	2016-01-21 09:05:57.59252		short-term-rehabilitation-outpatient	\N	\N
2214	TemplateField	189	Telemedicine	38	2016-01-21 09:05:57.602319	2016-01-21 09:05:57.602319		telemedicine	\N	\N
2215	TemplateField	189	Speciality Rx Coverage	39	2016-01-21 09:05:57.612247	2016-01-21 09:05:57.612247		speciality-rx-coverage	\N	\N
2216	TemplateField	189	Substance Abuse - Outpatient Coverage	17	2016-01-21 09:05:57.622125	2016-01-21 09:05:57.622125		substance-abuse-outpatient-coverage	\N	t
2217	TemplateColumn	189	Subject to Deductible & Coinsurance	1	2016-01-21 09:05:57.634156	2016-01-21 09:05:57.634156		subject-to-deductible-coinsurance	\N	\N
2218	TemplateColumn	189	COPAY	2	2016-01-21 09:05:57.643881	2016-01-21 09:05:57.643881		copay	\N	\N
2219	TemplateColumn	189	LIMIT	3	2016-01-21 09:05:57.653606	2016-01-21 09:05:57.653606		limit	\N	\N
2220	TemplateColumn	189	COPAY LIMIT	4	2016-01-21 09:05:57.663474	2016-01-21 09:05:57.663474		copay-limit	\N	\N
2221	TemplateColumn	189	SEPERATE DEDUCTIBLE	5	2016-01-21 09:05:57.673388	2016-01-21 09:05:57.673388		seperate-deductible	\N	\N
2222	TemplateColumn	189	Separate Coinsurance	6	2016-01-21 09:05:57.683262	2016-01-21 09:05:57.683262		separate-coinsurance	\N	\N
2223	TemplateField	190	Subject to Medical Deductible	1	2016-01-21 09:06:00.387606	2016-01-21 09:06:00.387606		subject-to-medical-deductible	\N	t
2224	TemplateField	190	Subject to Medical Deductible & Coinsurance	2	2016-01-21 09:06:00.397727	2016-01-21 09:06:00.397727		subject-to-medical-deductible-coinsurance	\N	\N
2225	TemplateField	190	Network	3	2016-01-21 09:06:00.407755	2016-01-21 09:06:00.407755		network	\N	t
2226	TemplateField	190	Mandatory Generic	4	2016-01-21 09:06:00.417653	2016-01-21 09:06:00.417653		mandatory-generic	\N	\N
2227	TemplateField	190	Step Therapy/Precertification Applies	5	2016-01-21 09:06:00.428661	2016-01-21 09:06:00.428661		step-therapy-precertification-applies	\N	\N
2228	TemplateField	190	Oral Contraceptive	6	2016-01-21 09:06:00.441861	2016-01-21 09:06:00.441861		oral-contraceptive	\N	\N
2229	TemplateField	190	Prescriptions Covered Out of Network	7	2016-01-21 09:06:00.452548	2016-01-21 09:06:00.452548		prescriptions-covered-out-of-network	\N	\N
2230	TemplateColumn	190	ENTRIES	1	2016-01-21 09:06:00.465025	2016-01-21 09:06:00.465025		entries	\N	\N
2231	TemplateField	191	Rx Deductible - Individual	1	2016-01-21 09:06:00.556025	2016-01-21 09:06:00.556025		rx-deductible-individual	t	t
2232	TemplateField	191	Rx Deductible - Family	2	2016-01-21 09:06:00.566417	2016-01-21 09:06:00.566417		rx-deductible-family	t	t
2233	TemplateColumn	191	DEDUCTIBLE	1	2016-01-21 09:06:00.578827	2016-01-21 09:06:00.578827		deductible	\N	\N
2234	TemplateColumn	191	PER MEMBER	2	2016-01-21 09:06:00.604234	2016-01-21 09:06:00.604234		per-member	\N	\N
2235	TemplateField	192	Retail Rx (Tier 1)	1	2016-01-21 09:06:00.669526	2016-01-21 09:06:00.669526		retail-rx-tier-1	t	t
2236	TemplateField	192	Retail Rx (Tier 3)	13	2016-01-21 09:06:00.680424	2016-01-21 09:06:00.680424		retail-rx-tier-3	t	t
2237	TemplateField	192	Retail Rx (Tier 4)	19	2016-01-21 09:06:00.691021	2016-01-21 09:06:00.691021		retail-rx-tier-4	\N	\N
2238	TemplateField	192	Retail Rx (Tier 5)	25	2016-01-21 09:06:00.701396	2016-01-21 09:06:00.701396		retail-rx-tier-5	\N	\N
2239	TemplateField	192	Mail Order Rx (Tier 1)	31	2016-01-21 09:06:00.712358	2016-01-21 09:06:00.712358		mail-order-rx-tier-1	\N	t
2240	TemplateField	192	Specialty Medications -Mail Order	67	2016-01-21 09:06:00.723394	2016-01-21 09:06:00.723394		specialty-medications-mail-order	\N	f
2241	TemplateField	192	Specialty Medications  -Retail	61	2016-01-21 09:06:00.733934	2016-01-21 09:06:00.733934		specialty-medications-retail	\N	\N
2242	TemplateField	192	Mail Order Rx (Tier 3)	43	2016-01-21 09:06:00.744454	2016-01-21 09:06:00.744454		mail-order-rx-tier-3	\N	t
2243	TemplateField	192	Mail Order Rx (Tier 4)	49	2016-01-21 09:06:00.754725	2016-01-21 09:06:00.754725		mail-order-rx-tier-4	\N	\N
2244	TemplateField	192	Mail Order Rx (Tier 5)	55	2016-01-21 09:06:00.765329	2016-01-21 09:06:00.765329		mail-order-rx-tier-5	\N	\N
2245	TemplateField	192	Retail Rx (Tier 2)	7	2016-01-21 09:06:00.776292	2016-01-21 09:06:00.776292		retail-rx-tier-2	t	t
2246	TemplateField	192	Mail Order Rx (Tier 2)	37	2016-01-21 09:06:00.787215	2016-01-21 09:06:00.787215		mail-order-rx-tier-2	\N	t
2247	TemplateColumn	192	SUBJECT TO RX DEDUCTIBLE	1	2016-01-21 09:06:00.800983	2016-01-21 09:06:00.800983		subject-to-rx-deductible	\N	\N
2248	TemplateColumn	192	COPAY	2	2016-01-21 09:06:00.811618	2016-01-21 09:06:00.811618		copay	\N	\N
2249	TemplateColumn	192	COINSURANCE	3	2016-01-21 09:06:00.822371	2016-01-21 09:06:00.822371		coinsurance	\N	\N
2250	TemplateColumn	192	PER SCRIPT COINSURANCE LIMIT	4	2016-01-21 09:06:00.832926	2016-01-21 09:06:00.832926		per-script-coinsurance-limit	\N	\N
2251	TemplateColumn	192	PER SCRIPT COINSURANCE MINIMUM	5	2016-01-21 09:06:00.843795	2016-01-21 09:06:00.843795		per-script-coinsurance-minimum	\N	\N
2252	TemplateField	193	Disease Management	1	2016-01-21 09:06:01.617716	2016-01-21 09:06:01.617716		disease-management	\N	\N
2253	TemplateColumn	193	IDENTIFICATION METHODS	1	2016-01-21 09:06:01.630615	2016-01-21 09:06:01.630615		identification-methods	\N	\N
2254	TemplateColumn	193	NUMBER OF CONDITIONS TRACKED	2	2016-01-21 09:06:01.641177	2016-01-21 09:06:01.641177		number-of-conditions-tracked	\N	\N
2255	TemplateColumn	193	OUTREACH METHODS	3	2016-01-21 09:06:01.651493	2016-01-21 09:06:01.651493		outreach-methods	\N	\N
2256	TemplateColumn	193	REPORT FREQUENCY	4	2016-01-21 09:06:01.661837	2016-01-21 09:06:01.661837		report-frequency	\N	\N
2257	TemplateField	194	Integrated Wellness Plan	1	2016-01-21 09:06:01.723402	2016-01-21 09:06:01.723402		integrated-wellness-plan	\N	\N
2258	TemplateColumn	194	Entries	1	2016-01-21 09:06:01.737712	2016-01-21 09:06:01.737712		entries	\N	\N
2259	TemplateField	195	Employee Eligibility Class #1	1	2016-01-21 09:06:01.769674	2016-01-21 09:06:01.769674		employee-eligibility-class-1	\N	\N
2260	TemplateField	195	Employee Eligibility Class #2	2	2016-01-21 09:06:01.779694	2016-01-21 09:06:01.779694		employee-eligibility-class-2	\N	\N
2261	TemplateField	195	Employee Eligibility Class #3	3	2016-01-21 09:06:01.790625	2016-01-21 09:06:01.790625		employee-eligibility-class-3	\N	\N
2262	TemplateField	195	Employee Eligibility Class #4	4	2016-01-21 09:06:01.814626	2016-01-21 09:06:01.814626		employee-eligibility-class-4	\N	\N
2263	TemplateColumn	195	Employee Class	1	2016-01-21 09:06:01.828781	2016-01-21 09:06:01.828781		employee-class	\N	\N
2264	TemplateColumn	195	Employee Classes	2	2016-01-21 09:06:01.843199	2016-01-21 09:06:01.843199		employee-classes	\N	\N
2265	TemplateColumn	195	Waiting Period	3	2016-01-21 09:06:01.854603	2016-01-21 09:06:01.854603		waiting-period	\N	\N
2266	TemplateColumn	195	Hours Requirement	4	2016-01-21 09:06:01.866386	2016-01-21 09:06:01.866386		hours-requirement	\N	\N
2267	TemplateColumn	195	Spouse Eligible	5	2016-01-21 09:06:01.87773	2016-01-21 09:06:01.87773		spouse-eligible	\N	\N
2268	TemplateColumn	195	Dependent Children Eligible	6	2016-01-21 09:06:01.890383	2016-01-21 09:06:01.890383		dependent-children-eligible	\N	\N
2269	TemplateColumn	195	Domestic Partner Eligible	7	2016-01-21 09:06:01.901801	2016-01-21 09:06:01.901801		domestic-partner-eligible	\N	\N
2270	TemplateColumn	195	Dependent Child Limiting Age	8	2016-01-21 09:06:01.91317	2016-01-21 09:06:01.91317		dependent-child-limiting-age	\N	\N
2271	TemplateColumn	195	Dependent Child Coverage Ends	9	2016-01-21 09:06:01.924308	2016-01-21 09:06:01.924308		dependent-child-coverage-ends	\N	\N
2272	TemplateField	196	Plan Type	1	2016-01-21 09:06:02.43317	2016-01-21 09:06:02.43317		plan-type	\N	\N
2273	TemplateField	196	Network	2	2016-01-21 09:06:02.447959	2016-01-21 09:06:02.447959		network	\N	\N
2274	TemplateField	196	Referrals Needed	3	2016-01-21 09:06:02.462825	2016-01-21 09:06:02.462825		referrals-needed	\N	\N
2275	TemplateField	196	Deductible Individual	4	2016-01-21 09:06:02.47763	2016-01-21 09:06:02.47763		deductible-individual	\N	\N
2276	TemplateField	196	Deductible Family	5	2016-01-21 09:06:02.492182	2016-01-21 09:06:02.492182		deductible-family	\N	\N
2277	TemplateField	196	Deductible - Family Multiple	6	2016-01-21 09:06:02.505627	2016-01-21 09:06:02.505627		deductible-family-multiple	\N	\N
2278	TemplateField	196	Deductible Format	7	2016-01-21 09:06:02.517246	2016-01-21 09:06:02.517246		deductible-format	\N	\N
2279	TemplateField	196	Coinsurance	8	2016-01-21 09:06:02.528152	2016-01-21 09:06:02.528152		coinsurance	\N	\N
2280	TemplateField	196	Out-of-Pocket Max Individual (includes deductible)	9	2016-01-21 09:06:02.53979	2016-01-21 09:06:02.53979		out-of-pocket-max-individual-includes-deductible	\N	\N
2281	TemplateField	196	Out-of-Pocket Max Family (includes deductible)	10	2016-01-21 09:06:02.550919	2016-01-21 09:06:02.550919		out-of-pocket-max-family-includes-deductible	\N	\N
2282	TemplateField	196	Out-of-Pocket Maximum - Family Multiple	11	2016-01-21 09:06:02.562471	2016-01-21 09:06:02.562471		out-of-pocket-maximum-family-multiple	\N	\N
2283	TemplateField	196	In & Out-of-Network Deductibles Cross Accumulation	12	2016-01-21 09:06:02.573957	2016-01-21 09:06:02.573957		in-out-of-network-deductibles-cross-accumulation	\N	\N
2284	TemplateField	196	In & Out-of-Network OOP Maximum Cross Accumulation	13	2016-01-21 09:06:02.58566	2016-01-21 09:06:02.58566		in-out-of-network-oop-maximum-cross-accumulation	\N	\N
2285	TemplateField	196	Annual Maximum	14	2016-01-21 09:06:02.596969	2016-01-21 09:06:02.596969		annual-maximum	\N	\N
2286	TemplateField	196	Gym Reimbursement	15	2016-01-21 09:06:02.607702	2016-01-21 09:06:02.607702		gym-reimbursement	\N	\N
2287	TemplateField	196	Customer Service Days/Hours	16	2016-01-21 09:06:02.619805	2016-01-21 09:06:02.619805		customer-service-days-hours	\N	\N
2288	TemplateField	196	OON Reimbursement Level	17	2016-01-21 09:06:02.631007	2016-01-21 09:06:02.631007		oon-reimbursement-level	\N	\N
2289	TemplateField	196	Nurseline	18	2016-01-21 09:06:02.644909	2016-01-21 09:06:02.644909		nurseline	\N	\N
2290	TemplateField	196	International Employees Covered	19	2016-01-21 09:06:02.656499	2016-01-21 09:06:02.656499		international-employees-covered	\N	\N
2291	TemplateColumn	196	ENTRIES	1	2016-01-21 09:06:02.669589	2016-01-21 09:06:02.669589		entries	\N	\N
2292	TemplateField	197	Plan Type	1	2016-01-21 09:06:02.92324	2016-01-21 09:06:02.92324		plan-type	t	t
2293	TemplateField	197	Network	2	2016-01-21 09:06:02.93337	2016-01-21 09:06:02.93337		network	t	t
2294	TemplateField	197	Referrals Needed	3	2016-01-21 09:06:02.944408	2016-01-21 09:06:02.944408		referrals-needed	t	t
2295	TemplateField	197	Deductible Individual	4	2016-01-21 09:06:02.954531	2016-01-21 09:06:02.954531		deductible-individual	t	t
2296	TemplateField	197	Deductible Family	5	2016-01-21 09:06:02.965509	2016-01-21 09:06:02.965509		deductible-family	t	t
2297	TemplateField	197	Deductible - Family Multiple	6	2016-01-21 09:06:02.975938	2016-01-21 09:06:02.975938		deductible-family-multiple	\N	\N
2298	TemplateField	197	Deductible Format	7	2016-01-21 09:06:02.98623	2016-01-21 09:06:02.98623		deductible-format	\N	t
2299	TemplateField	197	Coinsurance	8	2016-01-21 09:06:02.996006	2016-01-21 09:06:02.996006		coinsurance	t	t
2300	TemplateField	197	Out-of-Pocket Max Individual (includes deductible)	9	2016-01-21 09:06:03.005738	2016-01-21 09:06:03.005738		out-of-pocket-max-individual-includes-deductible	t	t
2301	TemplateField	197	Out-of-Pocket Max Family (includes deductible)	10	2016-01-21 09:06:03.016142	2016-01-21 09:06:03.016142		out-of-pocket-max-family-includes-deductible	t	t
2302	TemplateField	197	Out-of-Pocket Maximum - Family Multiple	11	2016-01-21 09:06:03.02619	2016-01-21 09:06:03.02619		out-of-pocket-maximum-family-multiple	\N	\N
2303	TemplateField	197	In & Out-of-Network Deductibles Cross Accumulation	12	2016-01-21 09:06:03.036307	2016-01-21 09:06:03.036307		in-out-of-network-deductibles-cross-accumulation	\N	t
2304	TemplateField	197	In & Out-of-Network OOP Maximum Cross Accumulation	13	2016-01-21 09:06:03.046953	2016-01-21 09:06:03.046953		in-out-of-network-oop-maximum-cross-accumulation	\N	t
2305	TemplateField	197	OON Reimbursement Level	14	2016-01-21 09:06:03.058016	2016-01-21 09:06:03.058016		oon-reimbursement-level	t	t
2306	TemplateField	197	Annual Maximum	15	2016-01-21 09:06:03.068296	2016-01-21 09:06:03.068296		annual-maximum	\N	\N
2307	TemplateField	197	Gym Reimbursement	16	2016-01-21 09:06:03.079004	2016-01-21 09:06:03.079004		gym-reimbursement	\N	\N
2308	TemplateField	197	Customer Service Days/Hours	17	2016-01-21 09:06:03.089712	2016-01-21 09:06:03.089712		customer-service-days-hours	\N	\N
2309	TemplateField	197	Nurseline	18	2016-01-21 09:06:03.099865	2016-01-21 09:06:03.099865		nurseline	\N	\N
2310	TemplateField	197	International Employees Covered	19	2016-01-21 09:06:03.111228	2016-01-21 09:06:03.111228		international-employees-covered	\N	\N
2311	TemplateColumn	197	ENTRIES	1	2016-01-21 09:06:03.124173	2016-01-21 09:06:03.124173		entries	\N	\N
2312	TemplateField	198	General Radiology	4	2016-01-21 09:06:03.361811	2016-01-21 09:06:03.361811		general-radiology	\N	\N
2313	TemplateField	198	CT/MRI/PET Scans	5	2016-01-21 09:06:03.372294	2016-01-21 09:06:03.372294		ct-mri-pet-scans	\N	\N
2314	TemplateField	198	Annual Physical Exam	6	2016-01-21 09:06:03.383816	2016-01-21 09:06:03.383816		annual-physical-exam	\N	\N
2315	TemplateField	198	Well Child Exams	7	2016-01-21 09:06:03.39432	2016-01-21 09:06:03.39432		well-child-exams	\N	\N
2316	TemplateField	198	Pediatric Dental	8	2016-01-21 09:06:03.405255	2016-01-21 09:06:03.405255		pediatric-dental	\N	\N
2317	TemplateField	198	Hospitalization - Inpatient	9	2016-01-21 09:06:03.415451	2016-01-21 09:06:03.415451		hospitalization-inpatient	t	t
2318	TemplateField	198	Hospitalization - Outpatient	10	2016-01-21 09:06:03.425783	2016-01-21 09:06:03.425783		hospitalization-outpatient	\N	t
2319	TemplateField	198	Surgery/Anesthesiology	11	2016-01-21 09:06:03.436468	2016-01-21 09:06:03.436468		surgery-anesthesiology	\N	\N
2320	TemplateField	198	Emergency Room	12	2016-01-21 09:06:03.446427	2016-01-21 09:06:03.446427		emergency-room	t	t
2321	TemplateField	198	Specialist Office Visit	2	2016-01-21 09:06:03.456842	2016-01-21 09:06:03.456842		specialist-office-visit	t	t
2322	TemplateField	198	Lab	3	2016-01-21 09:06:03.467428	2016-01-21 09:06:03.467428		lab	\N	t
2323	TemplateField	198	Urgent Care	13	2016-01-21 09:06:03.477477	2016-01-21 09:06:03.477477		urgent-care	\N	t
2324	TemplateField	198	Mental Nervous - Outpatient Coverage	15	2016-01-21 09:06:03.487501	2016-01-21 09:06:03.487501		mental-nervous-outpatient-coverage	\N	t
2325	TemplateField	198	Substance Abuse - Inpatient Coverage	16	2016-01-21 09:06:03.49812	2016-01-21 09:06:03.49812		substance-abuse-inpatient-coverage	\N	t
2326	TemplateField	198	Substance Abuse - Outpatient Coverage	17	2016-01-21 09:06:03.509385	2016-01-21 09:06:03.509385		substance-abuse-outpatient-coverage	\N	t
2327	TemplateField	198	Physical Therapy - Inpatient Coverage	18	2016-01-21 09:06:03.520399	2016-01-21 09:06:03.520399		physical-therapy-inpatient-coverage	\N	\N
2328	TemplateField	198	Physical Therapy - Outpatient Coverage	19	2016-01-21 09:06:03.530885	2016-01-21 09:06:03.530885		physical-therapy-outpatient-coverage	\N	t
2329	TemplateField	198	Occupational Therapy - Inpatient Coverage	20	2016-01-21 09:06:03.541287	2016-01-21 09:06:03.541287		occupational-therapy-inpatient-coverage	\N	\N
2330	TemplateField	198	Occupational Therapy - Outpatient Coverage	21	2016-01-21 09:06:03.551719	2016-01-21 09:06:03.551719		occupational-therapy-outpatient-coverage	\N	\N
2331	TemplateField	198	Speech Therapy - Inpatient Coverage	22	2016-01-21 09:06:03.562092	2016-01-21 09:06:03.562092		speech-therapy-inpatient-coverage	\N	\N
2332	TemplateField	198	Speech Therapy - Outpatient Coverage	23	2016-01-21 09:06:03.572247	2016-01-21 09:06:03.572247		speech-therapy-outpatient-coverage	\N	\N
2333	TemplateField	198	Pregnancy & Maternity Care - Office Visits	24	2016-01-21 09:06:03.582141	2016-01-21 09:06:03.582141		pregnancy-maternity-care-office-visits	\N	\N
2334	TemplateField	198	Pregnancy & Maternity Care - Labor & Delivery	25	2016-01-21 09:06:03.592076	2016-01-21 09:06:03.592076		pregnancy-maternity-care-labor-delivery	\N	\N
2335	TemplateField	198	Chiropractic Services	26	2016-01-21 09:06:03.602493	2016-01-21 09:06:03.602493		chiropractic-services	\N	t
2336	TemplateField	198	Ambulance	27	2016-01-21 09:06:03.612619	2016-01-21 09:06:03.612619		ambulance	\N	\N
2337	TemplateField	198	Hospice	28	2016-01-21 09:06:03.623188	2016-01-21 09:06:03.623188		hospice	\N	\N
2338	TemplateField	198	Home Healthcare	29	2016-01-21 09:06:03.633776	2016-01-21 09:06:03.633776		home-healthcare	\N	\N
2339	TemplateField	198	Skilled Nursing	30	2016-01-21 09:06:03.644379	2016-01-21 09:06:03.644379		skilled-nursing	\N	\N
2340	TemplateField	198	Infertility Coverage	31	2016-01-21 09:06:03.655093	2016-01-21 09:06:03.655093		infertility-coverage	\N	t
2341	TemplateField	198	Prosthetics	32	2016-01-21 09:06:03.665556	2016-01-21 09:06:03.665556		prosthetics	\N	\N
2342	TemplateField	198	Durable Medical Equipment	33	2016-01-21 09:06:03.675963	2016-01-21 09:06:03.675963		durable-medical-equipment	\N	t
2343	TemplateField	198	Hearing Devices	34	2016-01-21 09:06:03.686377	2016-01-21 09:06:03.686377		hearing-devices	\N	\N
2344	TemplateField	198	Vision Exams	35	2016-01-21 09:06:03.696996	2016-01-21 09:06:03.696996		vision-exams	\N	\N
2345	TemplateField	198	Short Term Rehabilitation - Inpatient	36	2016-01-21 09:06:03.710739	2016-01-21 09:06:03.710739		short-term-rehabilitation-inpatient	\N	\N
2346	TemplateField	198	Short Term Rehabilitation - Outpatient	37	2016-01-21 09:06:03.721663	2016-01-21 09:06:03.721663		short-term-rehabilitation-outpatient	\N	\N
2347	TemplateField	198	Telemedicine	38	2016-01-21 09:06:03.731994	2016-01-21 09:06:03.731994		telemedicine	\N	\N
2348	TemplateField	198	Specialty Rx Coverage	39	2016-01-21 09:06:03.742266	2016-01-21 09:06:03.742266		specialty-rx-coverage	\N	\N
2349	TemplateField	198	Primary Care Office Visit	1	2016-01-21 09:06:03.753123	2016-01-21 09:06:03.753123		primary-care-office-visit	t	t
2350	TemplateField	198	Mental Nervous - Inpatient Coverage	14	2016-01-21 09:06:03.764019	2016-01-21 09:06:03.764019		mental-nervous-inpatient-coverage	\N	t
2351	TemplateColumn	198	Subject to Deductible & Coinsurance	1	2016-01-21 09:06:03.776829	2016-01-21 09:06:03.776829		subject-to-deductible-coinsurance	\N	\N
2352	TemplateColumn	198	LIMIT	2	2016-01-21 09:06:03.788038	2016-01-21 09:06:03.788038		limit	\N	\N
2353	TemplateColumn	198	SEPERATE DEDUCTIBLE	3	2016-01-21 09:06:03.799106	2016-01-21 09:06:03.799106		seperate-deductible	\N	\N
2354	TemplateColumn	198	SEPERATE COINSURANCE	4	2016-01-21 09:06:03.80981	2016-01-21 09:06:03.80981		seperate-coinsurance	\N	\N
2355	TemplateField	199	Subject to Medical Deductible	1	2016-01-21 09:06:05.548806	2016-01-21 09:06:05.548806		subject-to-medical-deductible	\N	t
2356	TemplateField	199	Subject to Medical Deductible & Coinsurance	2	2016-01-21 09:06:05.559722	2016-01-21 09:06:05.559722		subject-to-medical-deductible-coinsurance	\N	\N
2357	TemplateField	199	Network	3	2016-01-21 09:06:05.569926	2016-01-21 09:06:05.569926		network	\N	t
2358	TemplateField	199	Step Therapy/Precertification Applies	4	2016-01-21 09:06:05.580504	2016-01-21 09:06:05.580504		step-therapy-precertification-applies	\N	\N
2359	TemplateField	199	Mandatory Generic	5	2016-01-21 09:06:05.595519	2016-01-21 09:06:05.595519		mandatory-generic	\N	\N
2360	TemplateField	199	Oral Contraceptive	6	2016-01-21 09:06:05.605663	2016-01-21 09:06:05.605663		oral-contraceptive	\N	\N
2361	TemplateField	199	Are Prescriptions Covered Out of Network	7	2016-01-21 09:06:05.616081	2016-01-21 09:06:05.616081		are-prescriptions-covered-out-of-network	\N	\N
2362	TemplateColumn	199	ENTRIES	1	2016-01-21 09:06:05.631687	2016-01-21 09:06:05.631687		entries	\N	\N
2363	TemplateField	200	Rx Deductible - Family	1	2016-01-21 09:06:05.727044	2016-01-21 09:06:05.727044		rx-deductible-family	t	t
2364	TemplateField	200	Rx Deductible - Individual	2	2016-01-21 09:06:05.737748	2016-01-21 09:06:05.737748		rx-deductible-individual	t	t
2365	TemplateColumn	200	DEDUCTIBLE	1	2016-01-21 09:06:05.750837	2016-01-21 09:06:05.750837		deductible	\N	\N
2366	TemplateColumn	200	PER MEMBER	2	2016-01-21 09:06:05.762423	2016-01-21 09:06:05.762423		per-member	\N	\N
2367	TemplateField	201	Retail Rx (Tier 1)	1	2016-01-21 09:06:05.837102	2016-01-21 09:06:05.837102		retail-rx-tier-1	t	t
2368	TemplateField	201	Retail Rx (Tier 2)	2	2016-01-21 09:06:05.848863	2016-01-21 09:06:05.848863		retail-rx-tier-2	t	t
2369	TemplateField	201	Retail Rx (Tier 3)	3	2016-01-21 09:06:05.859349	2016-01-21 09:06:05.859349		retail-rx-tier-3	t	t
2370	TemplateField	201	Retail Rx (Tier 4)	4	2016-01-21 09:06:05.870342	2016-01-21 09:06:05.870342		retail-rx-tier-4	\N	\N
2371	TemplateField	201	Retail Rx (Tier 5)	5	2016-01-21 09:06:05.881643	2016-01-21 09:06:05.881643		retail-rx-tier-5	\N	\N
2372	TemplateField	201	Mail Order Rx (Tier 1)	6	2016-01-21 09:06:05.892355	2016-01-21 09:06:05.892355		mail-order-rx-tier-1	\N	t
2373	TemplateField	201	Mail Order Rx (Tier 2 )	7	2016-01-21 09:06:05.902994	2016-01-21 09:06:05.902994		mail-order-rx-tier-2	\N	t
2374	TemplateField	201	Mail Order Rx (Tier 3)	8	2016-01-21 09:06:05.913694	2016-01-21 09:06:05.913694		mail-order-rx-tier-3	\N	t
2375	TemplateField	201	Mail Order Rx (Tier 4)	9	2016-01-21 09:06:05.924692	2016-01-21 09:06:05.924692		mail-order-rx-tier-4	\N	\N
2376	TemplateField	201	Mail Order Rx (Tier 5)	10	2016-01-21 09:06:05.935259	2016-01-21 09:06:05.935259		mail-order-rx-tier-5	\N	\N
2377	TemplateField	201	Speciality Medications - Mail Order	11	2016-01-21 09:06:05.947103	2016-01-21 09:06:05.947103		speciality-medications-mail-order	\N	\N
2378	TemplateField	201	Speciality Medications - Retail	12	2016-01-21 09:06:05.959839	2016-01-21 09:06:05.959839		speciality-medications-retail	\N	\N
2379	TemplateColumn	201	SUBJECT TO RX DEDUCTIBLE	1	2016-01-21 09:06:05.972216	2016-01-21 09:06:05.972216		subject-to-rx-deductible	\N	\N
2380	TemplateColumn	201	COINSURANCE	2	2016-01-21 09:06:05.983224	2016-01-21 09:06:05.983224		coinsurance	\N	\N
2381	TemplateField	202	Disease Management	1	2016-01-21 09:06:06.275347	2016-01-21 09:06:06.275347		disease-management	\N	\N
2382	TemplateColumn	202	IDENTIFICATION METHODS	1	2016-01-21 09:06:06.288697	2016-01-21 09:06:06.288697		identification-methods	\N	\N
2383	TemplateColumn	202	NUMBER OF CONDITIONS TRACKED	2	2016-01-21 09:06:06.299864	2016-01-21 09:06:06.299864		number-of-conditions-tracked	\N	\N
2384	TemplateColumn	202	OUTREACH METHODS	3	2016-01-21 09:06:06.310866	2016-01-21 09:06:06.310866		outreach-methods	\N	\N
2385	TemplateColumn	202	REPORT FREQUENCY	4	2016-01-21 09:06:06.321495	2016-01-21 09:06:06.321495		report-frequency	\N	\N
2386	TemplateField	203	Integrated Wellness Plan	1	2016-01-21 09:06:06.386026	2016-01-21 09:06:06.386026		integrated-wellness-plan	\N	\N
2387	TemplateColumn	203	ENTRIES	1	2016-01-21 09:06:06.398177	2016-01-21 09:06:06.398177		entries	\N	\N
2388	TemplateField	204	Plan Type	1	2016-01-21 13:31:24.436875	2016-01-21 13:31:24.436875		plan-type	t	t
2389	TemplateField	204	Network	3	2016-01-21 13:31:24.453315	2016-01-21 13:31:24.453315		network	t	t
2390	TemplateField	204	Referrals Needed	5	2016-01-21 13:31:24.46398	2016-01-21 13:31:24.46398		referrals-needed	t	t
2391	TemplateField	204	Deductible Individual	7	2016-01-21 13:31:24.47453	2016-01-21 13:31:24.47453		deductible-individual	t	t
2392	TemplateField	204	Deductible Family	9	2016-01-21 13:31:24.485463	2016-01-21 13:31:24.485463		deductible-family	t	t
2393	TemplateField	204	Deductible - Family Multiple	11	2016-01-21 13:31:24.496859	2016-01-21 13:31:24.496859		deductible-family-multiple	\N	\N
2394	TemplateField	204	Deductible Format	13	2016-01-21 13:31:24.50969	2016-01-21 13:31:24.50969		deductible-format	\N	t
2395	TemplateField	204	Coinsurance	15	2016-01-21 13:31:24.522759	2016-01-21 13:31:24.522759		coinsurance	t	t
2396	TemplateField	204	Out-of-Pocket Max Family (includes deductible)	19	2016-01-21 13:31:24.53331	2016-01-21 13:31:24.53331		out-of-pocket-max-family-includes-deductible	t	t
2397	TemplateField	204	In & Out-of-Network Deductibles Cross Accumulation	23	2016-01-21 13:31:24.543617	2016-01-21 13:31:24.543617		in-out-of-network-deductibles-cross-accumulation	\N	t
2398	TemplateField	204	OON Reimbursement Level	27	2016-01-21 13:31:24.553869	2016-01-21 13:31:24.553869		oon-reimbursement-level	t	t
2399	TemplateField	204	Annual Maximum	29	2016-01-21 13:31:24.56422	2016-01-21 13:31:24.56422		annual-maximum	\N	\N
2400	TemplateField	204	Gym Reimbursement	31	2016-01-21 13:31:24.574164	2016-01-21 13:31:24.574164		gym-reimbursement	\N	\N
2401	TemplateField	204	Customer Service Days/Hours	33	2016-01-21 13:31:24.584624	2016-01-21 13:31:24.584624		customer-service-days-hours	\N	\N
2402	TemplateField	204	Nurseline	35	2016-01-21 13:31:24.594823	2016-01-21 13:31:24.594823		nurseline	\N	\N
2403	TemplateField	204	International Employees Covered	37	2016-01-21 13:31:24.60509	2016-01-21 13:31:24.60509		international-employees-covered	\N	\N
2404	TemplateField	204	Out-of-Pocket Max Individual (includes deductible)	17	2016-01-21 13:31:24.614984	2016-01-21 13:31:24.614984		out-of-pocket-max-individual-includes-deductible	t	t
2405	TemplateField	204	Out-of-Pocket Maximum - Family Multiple	21	2016-01-21 13:31:24.625102	2016-01-21 13:31:24.625102		out-of-pocket-maximum-family-multiple	\N	f
2406	TemplateField	204	In & Out-of-Network OOP Maximum Cross Accumulation	25	2016-01-21 13:31:24.6351	2016-01-21 13:31:24.6351		in-out-of-network-oop-maximum-cross-accumulation	\N	t
2408	TemplateField	205	Primary Care Office Visit	1	2016-01-21 13:31:24.871942	2016-01-21 13:31:24.871942		primary-care-office-visit	t	t
2409	TemplateField	205	Specialist Office Visit	2	2016-01-21 13:31:24.882395	2016-01-21 13:31:24.882395		specialist-office-visit	t	t
2410	TemplateField	205	Lab	3	2016-01-21 13:31:24.892743	2016-01-21 13:31:24.892743		lab	\N	t
2411	TemplateField	205	General Radiology	4	2016-01-21 13:31:24.903119	2016-01-21 13:31:24.903119		general-radiology	\N	\N
2412	TemplateField	205	CT/MRI/PET Scans	5	2016-01-21 13:31:24.914609	2016-01-21 13:31:24.914609		ct-mri-pet-scans	\N	\N
2413	TemplateField	205	Annual Physical Exam	6	2016-01-21 13:31:24.924869	2016-01-21 13:31:24.924869		annual-physical-exam	\N	\N
2414	TemplateField	205	Well Child Exams	7	2016-01-21 13:31:24.934653	2016-01-21 13:31:24.934653		well-child-exams	\N	\N
2415	TemplateField	205	Pediatric Dental	8	2016-01-21 13:31:24.944852	2016-01-21 13:31:24.944852		pediatric-dental	\N	\N
2416	TemplateField	205	Hospitalization - Inpatient	9	2016-01-21 13:31:24.954981	2016-01-21 13:31:24.954981		hospitalization-inpatient	t	t
2417	TemplateField	205	Hospitalization - Outpatient	10	2016-01-21 13:31:24.965551	2016-01-21 13:31:24.965551		hospitalization-outpatient	\N	t
2418	TemplateField	205	Surgery/Anesthesiology	11	2016-01-21 13:31:24.975638	2016-01-21 13:31:24.975638		surgery-anesthesiology	\N	\N
2419	TemplateField	205	Emergency Room	12	2016-01-21 13:31:24.985573	2016-01-21 13:31:24.985573		emergency-room	t	t
2420	TemplateField	205	Urgent Care	13	2016-01-21 13:31:24.995281	2016-01-21 13:31:24.995281		urgent-care	\N	t
2421	TemplateField	205	Mental Nervous - Inpatient Coverage	14	2016-01-21 13:31:25.004771	2016-01-21 13:31:25.004771		mental-nervous-inpatient-coverage	\N	t
2422	TemplateField	205	Mental Nervous - Outpatient Coverage	15	2016-01-21 13:31:25.014839	2016-01-21 13:31:25.014839		mental-nervous-outpatient-coverage	\N	t
2423	TemplateField	205	Substance Abuse - Inpatient Coverage	16	2016-01-21 13:31:25.024699	2016-01-21 13:31:25.024699		substance-abuse-inpatient-coverage	\N	t
2424	TemplateField	205	Physical Therapy - Inpatient Coverage	18	2016-01-21 13:31:25.034721	2016-01-21 13:31:25.034721		physical-therapy-inpatient-coverage	\N	\N
2425	TemplateField	205	Physical Therapy - Outpatient Coverage	19	2016-01-21 13:31:25.045091	2016-01-21 13:31:25.045091		physical-therapy-outpatient-coverage	\N	t
2426	TemplateField	205	Occupational Therapy - Inpatient Coverage	20	2016-01-21 13:31:25.054767	2016-01-21 13:31:25.054767		occupational-therapy-inpatient-coverage	\N	\N
2427	TemplateField	205	Occupational Therapy - Outpatient Coverage	21	2016-01-21 13:31:25.064726	2016-01-21 13:31:25.064726		occupational-therapy-outpatient-coverage	\N	\N
2428	TemplateField	205	Speech Therapy - Inpatient Coverage	22	2016-01-21 13:31:25.074494	2016-01-21 13:31:25.074494		speech-therapy-inpatient-coverage	\N	\N
2429	TemplateField	205	Speech Therapy - Outpatient Coverage	23	2016-01-21 13:31:25.084349	2016-01-21 13:31:25.084349		speech-therapy-outpatient-coverage	\N	\N
2430	TemplateField	205	Pregnancy & Maternity Care - Office Visits	24	2016-01-21 13:31:25.095539	2016-01-21 13:31:25.095539		pregnancy-maternity-care-office-visits	\N	\N
2431	TemplateField	205	Pregnancy & Maternity Care - Labor & Delivery	25	2016-01-21 13:31:25.105835	2016-01-21 13:31:25.105835		pregnancy-maternity-care-labor-delivery	\N	\N
2432	TemplateField	205	Chiropractic Services	26	2016-01-21 13:31:25.115451	2016-01-21 13:31:25.115451		chiropractic-services	\N	t
2433	TemplateField	205	Ambulance	27	2016-01-21 13:31:25.125087	2016-01-21 13:31:25.125087		ambulance	\N	\N
2434	TemplateField	205	Hospice	28	2016-01-21 13:31:25.134784	2016-01-21 13:31:25.134784		hospice	\N	\N
2435	TemplateField	205	Home Healthcare	29	2016-01-21 13:31:25.144923	2016-01-21 13:31:25.144923		home-healthcare	\N	\N
2436	TemplateField	205	Skilled Nursing	30	2016-01-21 13:31:25.155047	2016-01-21 13:31:25.155047		skilled-nursing	\N	\N
2437	TemplateField	205	Infertility Coverage	31	2016-01-21 13:31:25.164863	2016-01-21 13:31:25.164863		infertility-coverage	\N	t
2438	TemplateField	205	Prosthetics	32	2016-01-21 13:31:25.175067	2016-01-21 13:31:25.175067		prosthetics	\N	\N
2439	TemplateField	205	Durable Medical Equipment	33	2016-01-21 13:31:25.185002	2016-01-21 13:31:25.185002		durable-medical-equipment	\N	t
2440	TemplateField	205	Hearing Devices	34	2016-01-21 13:31:25.194681	2016-01-21 13:31:25.194681		hearing-devices	\N	\N
2441	TemplateField	205	Vision Exams	35	2016-01-21 13:31:25.205706	2016-01-21 13:31:25.205706		vision-exams	\N	\N
2442	TemplateField	205	Short Term Rehabilitation- Inpatient	36	2016-01-21 13:31:25.21646	2016-01-21 13:31:25.21646		short-term-rehabilitation-inpatient	\N	\N
2443	TemplateField	205	Short Term Rehabilitation - Outpatient	37	2016-01-21 13:31:25.226319	2016-01-21 13:31:25.226319		short-term-rehabilitation-outpatient	\N	\N
2444	TemplateField	205	Telemedicine	38	2016-01-21 13:31:25.236382	2016-01-21 13:31:25.236382		telemedicine	\N	\N
2445	TemplateField	205	Speciality Rx Coverage	39	2016-01-21 13:31:25.245934	2016-01-21 13:31:25.245934		speciality-rx-coverage	\N	\N
2446	TemplateField	205	Substance Abuse - Outpatient Coverage	17	2016-01-21 13:31:25.255804	2016-01-21 13:31:25.255804		substance-abuse-outpatient-coverage	\N	t
2447	TemplateColumn	205	Subject to Deductible & Coinsurance	1	2016-01-21 13:31:25.268067	2016-01-21 13:31:25.268067		subject-to-deductible-coinsurance	\N	\N
2452	TemplateColumn	205	Separate Coinsurance	6	2016-01-21 13:31:25.322976	2016-01-21 13:31:25.322976		separate-coinsurance	\N	\N
2453	TemplateField	206	Subject to Medical Deductible	1	2016-01-21 13:31:28.042948	2016-01-21 13:31:28.042948		subject-to-medical-deductible	\N	t
2454	TemplateField	206	Subject to Medical Deductible & Coinsurance	2	2016-01-21 13:31:28.053264	2016-01-21 13:31:28.053264		subject-to-medical-deductible-coinsurance	\N	\N
2455	TemplateField	206	Network	3	2016-01-21 13:31:28.063663	2016-01-21 13:31:28.063663		network	\N	t
2456	TemplateField	206	Mandatory Generic	4	2016-01-21 13:31:28.074166	2016-01-21 13:31:28.074166		mandatory-generic	\N	\N
2457	TemplateField	206	Step Therapy/Precertification Applies	5	2016-01-21 13:31:28.083815	2016-01-21 13:31:28.083815		step-therapy-precertification-applies	\N	\N
2458	TemplateField	206	Oral Contraceptive	6	2016-01-21 13:31:28.094207	2016-01-21 13:31:28.094207		oral-contraceptive	\N	\N
2459	TemplateField	206	Prescriptions Covered Out of Network	7	2016-01-21 13:31:28.108266	2016-01-21 13:31:28.108266		prescriptions-covered-out-of-network	\N	\N
2461	TemplateField	207	Rx Deductible - Individual	1	2016-01-21 13:31:28.21089	2016-01-21 13:31:28.21089		rx-deductible-individual	t	t
2462	TemplateField	207	Rx Deductible - Family	2	2016-01-21 13:31:28.221275	2016-01-21 13:31:28.221275		rx-deductible-family	t	t
2465	TemplateField	208	Retail Rx (Tier 1)	1	2016-01-21 13:31:28.308991	2016-01-21 13:31:28.308991		retail-rx-tier-1	t	t
2466	TemplateField	208	Retail Rx (Tier 3)	13	2016-01-21 13:31:28.319178	2016-01-21 13:31:28.319178		retail-rx-tier-3	t	t
2467	TemplateField	208	Retail Rx (Tier 4)	19	2016-01-21 13:31:28.329408	2016-01-21 13:31:28.329408		retail-rx-tier-4	\N	\N
2468	TemplateField	208	Retail Rx (Tier 5)	25	2016-01-21 13:31:28.339479	2016-01-21 13:31:28.339479		retail-rx-tier-5	\N	\N
2469	TemplateField	208	Mail Order Rx (Tier 1)	31	2016-01-21 13:31:28.349728	2016-01-21 13:31:28.349728		mail-order-rx-tier-1	\N	t
2470	TemplateField	208	Specialty Medications -Mail Order	67	2016-01-21 13:31:28.359919	2016-01-21 13:31:28.359919		specialty-medications-mail-order	\N	f
2471	TemplateField	208	Specialty Medications  -Retail	61	2016-01-21 13:31:28.370041	2016-01-21 13:31:28.370041		specialty-medications-retail	\N	\N
2472	TemplateField	208	Mail Order Rx (Tier 3)	43	2016-01-21 13:31:28.379786	2016-01-21 13:31:28.379786		mail-order-rx-tier-3	\N	t
2473	TemplateField	208	Mail Order Rx (Tier 4)	49	2016-01-21 13:31:28.389526	2016-01-21 13:31:28.389526		mail-order-rx-tier-4	\N	\N
2474	TemplateField	208	Mail Order Rx (Tier 5)	55	2016-01-21 13:31:28.399624	2016-01-21 13:31:28.399624		mail-order-rx-tier-5	\N	\N
2475	TemplateField	208	Retail Rx (Tier 2)	7	2016-01-21 13:31:28.409898	2016-01-21 13:31:28.409898		retail-rx-tier-2	t	t
2476	TemplateField	208	Mail Order Rx (Tier 2)	37	2016-01-21 13:31:28.41975	2016-01-21 13:31:28.41975		mail-order-rx-tier-2	\N	t
2482	TemplateField	209	Disease Management	1	2016-01-21 13:31:29.28102	2016-01-21 13:31:29.28102		disease-management	\N	\N
2487	TemplateField	210	Integrated Wellness Plan	1	2016-01-21 13:31:29.389085	2016-01-21 13:31:29.389085		integrated-wellness-plan	\N	\N
2488	TemplateColumn	210	Entries	1	2016-01-21 13:31:29.401068	2016-01-21 13:31:29.401068		entries	\N	\N
2489	TemplateField	211	Employee Eligibility Class #1	1	2016-01-21 13:31:29.449486	2016-01-21 13:31:29.449486		employee-eligibility-class-1	\N	\N
2449	TemplateColumn	205	Limit	3	2016-01-21 13:31:25.2929	2016-01-21 15:16:20.469879		limit	\N	\N
2450	TemplateColumn	205	Copay Limit	4	2016-01-21 13:31:25.302776	2016-01-21 15:16:39.843087		copay-limit	\N	\N
2451	TemplateColumn	205	Separate Deductible	5	2016-01-21 13:31:25.312695	2016-01-21 15:17:02.03296		separate-deductible	\N	\N
2460	TemplateColumn	206	Entries	1	2016-01-21 13:31:28.120985	2016-01-21 15:17:26.04085		entries	\N	\N
2463	TemplateColumn	207	Deductible	1	2016-01-21 13:31:28.234767	2016-01-21 15:17:47.015128		deductible	\N	\N
2464	TemplateColumn	207	Per Member	2	2016-01-21 13:31:28.245492	2016-01-21 15:18:00.299292		per-member	\N	\N
2478	TemplateColumn	208	Copay	2	2016-01-21 13:31:28.442361	2016-01-21 15:18:45.00737		copay	\N	\N
2479	TemplateColumn	208	Coinsurance	3	2016-01-21 13:31:28.452513	2016-01-21 15:19:00.069729		coinsurance	\N	\N
2480	TemplateColumn	208	Per Script Coinsurance Limit	4	2016-01-21 13:31:28.462404	2016-01-21 15:19:27.467224		per-script-coinsurance-limit	\N	\N
2481	TemplateColumn	208	Per Script Coinsurance Minimum	5	2016-01-21 13:31:28.472512	2016-01-21 15:19:56.850733		per-script-coinsurance-minimum	\N	\N
2483	TemplateColumn	209	Identification Mehtods	1	2016-01-21 13:31:29.294148	2016-01-21 15:20:37.887637		identification-mehtods	\N	\N
2484	TemplateColumn	209	Number of Conditions Tracked	2	2016-01-21 13:31:29.304734	2016-01-21 15:21:04.925045		number-of-conditions-tracked	\N	\N
2485	TemplateColumn	209	Outreach Methods	3	2016-01-21 13:31:29.315001	2016-01-21 15:21:25.583706		outreach-methods	\N	\N
2486	TemplateColumn	209	Report Frequency	4	2016-01-21 13:31:29.325825	2016-01-21 15:21:46.645872		report-frequency	\N	\N
2490	TemplateField	211	Employee Eligibility Class #2	2	2016-01-21 13:31:29.460022	2016-01-21 13:31:29.460022		employee-eligibility-class-2	\N	\N
2491	TemplateField	211	Employee Eligibility Class #3	3	2016-01-21 13:31:29.470683	2016-01-21 13:31:29.470683		employee-eligibility-class-3	\N	\N
2492	TemplateField	211	Employee Eligibility Class #4	4	2016-01-21 13:31:29.481259	2016-01-21 13:31:29.481259		employee-eligibility-class-4	\N	\N
2493	TemplateColumn	211	Employee Class	1	2016-01-21 13:31:29.493673	2016-01-21 13:31:29.493673		employee-class	\N	\N
2494	TemplateColumn	211	Employee Classes	2	2016-01-21 13:31:29.504717	2016-01-21 13:31:29.504717		employee-classes	\N	\N
2495	TemplateColumn	211	Waiting Period	3	2016-01-21 13:31:29.515307	2016-01-21 13:31:29.515307		waiting-period	\N	\N
2496	TemplateColumn	211	Hours Requirement	4	2016-01-21 13:31:29.525861	2016-01-21 13:31:29.525861		hours-requirement	\N	\N
2497	TemplateColumn	211	Spouse Eligible	5	2016-01-21 13:31:29.53648	2016-01-21 13:31:29.53648		spouse-eligible	\N	\N
2498	TemplateColumn	211	Dependent Children Eligible	6	2016-01-21 13:31:29.547372	2016-01-21 13:31:29.547372		dependent-children-eligible	\N	\N
2499	TemplateColumn	211	Domestic Partner Eligible	7	2016-01-21 13:31:29.557191	2016-01-21 13:31:29.557191		domestic-partner-eligible	\N	\N
2500	TemplateColumn	211	Dependent Child Limiting Age	8	2016-01-21 13:31:29.567382	2016-01-21 13:31:29.567382		dependent-child-limiting-age	\N	\N
2501	TemplateColumn	211	Dependent Child Coverage Ends	9	2016-01-21 13:31:29.577587	2016-01-21 13:31:29.577587		dependent-child-coverage-ends	\N	\N
2502	TemplateField	212	Plan Type	1	2016-01-21 13:31:30.053712	2016-01-21 13:31:30.053712		plan-type	t	t
2503	TemplateField	212	Network	2	2016-01-21 13:31:30.063957	2016-01-21 13:31:30.063957		network	t	t
2504	TemplateField	212	Referrals Needed	3	2016-01-21 13:31:30.074255	2016-01-21 13:31:30.074255		referrals-needed	t	t
2505	TemplateField	212	Deductible Individual	4	2016-01-21 13:31:30.085389	2016-01-21 13:31:30.085389		deductible-individual	t	t
2506	TemplateField	212	Deductible Family	5	2016-01-21 13:31:30.095535	2016-01-21 13:31:30.095535		deductible-family	t	t
2507	TemplateField	212	Deductible - Family Multiple	6	2016-01-21 13:31:30.105216	2016-01-21 13:31:30.105216		deductible-family-multiple	\N	\N
2508	TemplateField	212	Deductible Format	7	2016-01-21 13:31:30.11509	2016-01-21 13:31:30.11509		deductible-format	\N	t
2509	TemplateField	212	Coinsurance	8	2016-01-21 13:31:30.125368	2016-01-21 13:31:30.125368		coinsurance	t	t
2510	TemplateField	212	Out-of-Pocket Max Individual (includes deductible)	9	2016-01-21 13:31:30.13566	2016-01-21 13:31:30.13566		out-of-pocket-max-individual-includes-deductible	t	t
2511	TemplateField	212	Out-of-Pocket Max Family (includes deductible)	10	2016-01-21 13:31:30.164946	2016-01-21 13:31:30.164946		out-of-pocket-max-family-includes-deductible	t	t
2512	TemplateField	212	Out-of-Pocket Maximum - Family Multiple	11	2016-01-21 13:31:30.177821	2016-01-21 13:31:30.177821		out-of-pocket-maximum-family-multiple	\N	\N
2513	TemplateField	212	In & Out-of-Network Deductibles Cross Accumulation	12	2016-01-21 13:31:30.187951	2016-01-21 13:31:30.187951		in-out-of-network-deductibles-cross-accumulation	\N	t
2514	TemplateField	212	In & Out-of-Network OOP Maximum Cross Accumulation	13	2016-01-21 13:31:30.198239	2016-01-21 13:31:30.198239		in-out-of-network-oop-maximum-cross-accumulation	\N	t
2515	TemplateField	212	OON Reimbursement Level	14	2016-01-21 13:31:30.208549	2016-01-21 13:31:30.208549		oon-reimbursement-level	t	t
2516	TemplateField	212	Annual Maximum	15	2016-01-21 13:31:30.218705	2016-01-21 13:31:30.218705		annual-maximum	\N	\N
2517	TemplateField	212	Gym Reimbursement	16	2016-01-21 13:31:30.22915	2016-01-21 13:31:30.22915		gym-reimbursement	\N	\N
2518	TemplateField	212	Customer Service Days/Hours	17	2016-01-21 13:31:30.239436	2016-01-21 13:31:30.239436		customer-service-days-hours	\N	\N
2519	TemplateField	212	Nurseline	18	2016-01-21 13:31:30.252135	2016-01-21 13:31:30.252135		nurseline	\N	\N
2520	TemplateField	212	International Employees Covered	19	2016-01-21 13:31:30.271627	2016-01-21 13:31:30.271627		international-employees-covered	\N	\N
2522	TemplateField	213	General Radiology	4	2016-01-21 13:31:30.515957	2016-01-21 13:31:30.515957		general-radiology	\N	\N
2523	TemplateField	213	CT/MRI/PET Scans	5	2016-01-21 13:31:30.525713	2016-01-21 13:31:30.525713		ct-mri-pet-scans	\N	\N
2524	TemplateField	213	Annual Physical Exam	6	2016-01-21 13:31:30.535442	2016-01-21 13:31:30.535442		annual-physical-exam	\N	\N
2525	TemplateField	213	Well Child Exams	7	2016-01-21 13:31:30.545347	2016-01-21 13:31:30.545347		well-child-exams	\N	\N
2526	TemplateField	213	Pediatric Dental	8	2016-01-21 13:31:30.555001	2016-01-21 13:31:30.555001		pediatric-dental	\N	\N
2527	TemplateField	213	Hospitalization - Inpatient	9	2016-01-21 13:31:30.565272	2016-01-21 13:31:30.565272		hospitalization-inpatient	t	t
2528	TemplateField	213	Hospitalization - Outpatient	10	2016-01-21 13:31:30.575239	2016-01-21 13:31:30.575239		hospitalization-outpatient	\N	t
2529	TemplateField	213	Surgery/Anesthesiology	11	2016-01-21 13:31:30.585069	2016-01-21 13:31:30.585069		surgery-anesthesiology	\N	\N
2530	TemplateField	213	Emergency Room	12	2016-01-21 13:31:30.59699	2016-01-21 13:31:30.59699		emergency-room	t	t
2531	TemplateField	213	Specialist Office Visit	2	2016-01-21 13:31:30.608176	2016-01-21 13:31:30.608176		specialist-office-visit	t	t
2532	TemplateField	213	Lab	3	2016-01-21 13:31:30.619949	2016-01-21 13:31:30.619949		lab	\N	t
2533	TemplateField	213	Urgent Care	13	2016-01-21 13:31:30.630118	2016-01-21 13:31:30.630118		urgent-care	\N	t
2534	TemplateField	213	Mental Nervous - Outpatient Coverage	15	2016-01-21 13:31:30.640316	2016-01-21 13:31:30.640316		mental-nervous-outpatient-coverage	\N	t
2535	TemplateField	213	Substance Abuse - Inpatient Coverage	16	2016-01-21 13:31:30.650477	2016-01-21 13:31:30.650477		substance-abuse-inpatient-coverage	\N	t
2536	TemplateField	213	Substance Abuse - Outpatient Coverage	17	2016-01-21 13:31:30.661632	2016-01-21 13:31:30.661632		substance-abuse-outpatient-coverage	\N	t
2537	TemplateField	213	Physical Therapy - Inpatient Coverage	18	2016-01-21 13:31:30.672227	2016-01-21 13:31:30.672227		physical-therapy-inpatient-coverage	\N	\N
2538	TemplateField	213	Physical Therapy - Outpatient Coverage	19	2016-01-21 13:31:30.682306	2016-01-21 13:31:30.682306		physical-therapy-outpatient-coverage	\N	t
2539	TemplateField	213	Occupational Therapy - Inpatient Coverage	20	2016-01-21 13:31:30.692104	2016-01-21 13:31:30.692104		occupational-therapy-inpatient-coverage	\N	\N
2540	TemplateField	213	Occupational Therapy - Outpatient Coverage	21	2016-01-21 13:31:30.701751	2016-01-21 13:31:30.701751		occupational-therapy-outpatient-coverage	\N	\N
2541	TemplateField	213	Speech Therapy - Inpatient Coverage	22	2016-01-21 13:31:30.712716	2016-01-21 13:31:30.712716		speech-therapy-inpatient-coverage	\N	\N
2542	TemplateField	213	Speech Therapy - Outpatient Coverage	23	2016-01-21 13:31:30.722505	2016-01-21 13:31:30.722505		speech-therapy-outpatient-coverage	\N	\N
2543	TemplateField	213	Pregnancy & Maternity Care - Office Visits	24	2016-01-21 13:31:30.73237	2016-01-21 13:31:30.73237		pregnancy-maternity-care-office-visits	\N	\N
2544	TemplateField	213	Pregnancy & Maternity Care - Labor & Delivery	25	2016-01-21 13:31:30.742215	2016-01-21 13:31:30.742215		pregnancy-maternity-care-labor-delivery	\N	\N
2545	TemplateField	213	Chiropractic Services	26	2016-01-21 13:31:30.752085	2016-01-21 13:31:30.752085		chiropractic-services	\N	t
2546	TemplateField	213	Ambulance	27	2016-01-21 13:31:30.761878	2016-01-21 13:31:30.761878		ambulance	\N	\N
2547	TemplateField	213	Hospice	28	2016-01-21 13:31:30.771632	2016-01-21 13:31:30.771632		hospice	\N	\N
2548	TemplateField	213	Home Healthcare	29	2016-01-21 13:31:30.781691	2016-01-21 13:31:30.781691		home-healthcare	\N	\N
2549	TemplateField	213	Skilled Nursing	30	2016-01-21 13:31:30.791672	2016-01-21 13:31:30.791672		skilled-nursing	\N	\N
2550	TemplateField	213	Infertility Coverage	31	2016-01-21 13:31:30.801475	2016-01-21 13:31:30.801475		infertility-coverage	\N	t
2551	TemplateField	213	Prosthetics	32	2016-01-21 13:31:30.811607	2016-01-21 13:31:30.811607		prosthetics	\N	\N
2552	TemplateField	213	Durable Medical Equipment	33	2016-01-21 13:31:30.821712	2016-01-21 13:31:30.821712		durable-medical-equipment	\N	t
2553	TemplateField	213	Hearing Devices	34	2016-01-21 13:31:30.832004	2016-01-21 13:31:30.832004		hearing-devices	\N	\N
2554	TemplateField	213	Vision Exams	35	2016-01-21 13:31:30.842148	2016-01-21 13:31:30.842148		vision-exams	\N	\N
2555	TemplateField	213	Short Term Rehabilitation - Inpatient	36	2016-01-21 13:31:30.852122	2016-01-21 13:31:30.852122		short-term-rehabilitation-inpatient	\N	\N
2556	TemplateField	213	Short Term Rehabilitation - Outpatient	37	2016-01-21 13:31:30.862217	2016-01-21 13:31:30.862217		short-term-rehabilitation-outpatient	\N	\N
2557	TemplateField	213	Telemedicine	38	2016-01-21 13:31:30.872126	2016-01-21 13:31:30.872126		telemedicine	\N	\N
2558	TemplateField	213	Specialty Rx Coverage	39	2016-01-21 13:31:30.882277	2016-01-21 13:31:30.882277		specialty-rx-coverage	\N	\N
2559	TemplateField	213	Primary Care Office Visit	1	2016-01-21 13:31:30.892295	2016-01-21 13:31:30.892295		primary-care-office-visit	t	t
2560	TemplateField	213	Mental Nervous - Inpatient Coverage	14	2016-01-21 13:31:30.902123	2016-01-21 13:31:30.902123		mental-nervous-inpatient-coverage	\N	t
2561	TemplateColumn	213	Subject to Deductible & Coinsurance	1	2016-01-21 13:31:30.914432	2016-01-21 13:31:30.914432		subject-to-deductible-coinsurance	\N	\N
2565	TemplateField	214	Subject to Medical Deductible	1	2016-01-21 13:31:32.672334	2016-01-21 13:31:32.672334		subject-to-medical-deductible	\N	t
2566	TemplateField	214	Subject to Medical Deductible & Coinsurance	2	2016-01-21 13:31:32.683136	2016-01-21 13:31:32.683136		subject-to-medical-deductible-coinsurance	\N	\N
2567	TemplateField	214	Network	3	2016-01-21 13:31:32.69433	2016-01-21 13:31:32.69433		network	\N	t
2568	TemplateField	214	Step Therapy/Precertification Applies	4	2016-01-21 13:31:32.70454	2016-01-21 13:31:32.70454		step-therapy-precertification-applies	\N	\N
2569	TemplateField	214	Mandatory Generic	5	2016-01-21 13:31:32.715211	2016-01-21 13:31:32.715211		mandatory-generic	\N	\N
2570	TemplateField	214	Oral Contraceptive	6	2016-01-21 13:31:32.731914	2016-01-21 13:31:32.731914		oral-contraceptive	\N	\N
2571	TemplateField	214	Are Prescriptions Covered Out of Network	7	2016-01-21 13:31:32.74342	2016-01-21 13:31:32.74342		are-prescriptions-covered-out-of-network	\N	\N
2573	TemplateField	215	Rx Deductible - Family	1	2016-01-21 13:31:32.851765	2016-01-21 13:31:32.851765		rx-deductible-family	t	t
2574	TemplateField	215	Rx Deductible - Individual	2	2016-01-21 13:31:32.862168	2016-01-21 13:31:32.862168		rx-deductible-individual	t	t
2577	TemplateField	216	Retail Rx (Tier 1)	1	2016-01-21 13:31:32.954282	2016-01-21 13:31:32.954282		retail-rx-tier-1	t	t
2578	TemplateField	216	Retail Rx (Tier 2)	2	2016-01-21 13:31:32.965151	2016-01-21 13:31:32.965151		retail-rx-tier-2	t	t
2579	TemplateField	216	Retail Rx (Tier 3)	3	2016-01-21 13:31:32.976159	2016-01-21 13:31:32.976159		retail-rx-tier-3	t	t
2580	TemplateField	216	Retail Rx (Tier 4)	4	2016-01-21 13:31:32.986632	2016-01-21 13:31:32.986632		retail-rx-tier-4	\N	\N
2581	TemplateField	216	Retail Rx (Tier 5)	5	2016-01-21 13:31:32.997979	2016-01-21 13:31:32.997979		retail-rx-tier-5	\N	\N
2582	TemplateField	216	Mail Order Rx (Tier 1)	6	2016-01-21 13:31:33.008727	2016-01-21 13:31:33.008727		mail-order-rx-tier-1	\N	t
2583	TemplateField	216	Mail Order Rx (Tier 2 )	7	2016-01-21 13:31:33.019982	2016-01-21 13:31:33.019982		mail-order-rx-tier-2	\N	t
2584	TemplateField	216	Mail Order Rx (Tier 3)	8	2016-01-21 13:31:33.030428	2016-01-21 13:31:33.030428		mail-order-rx-tier-3	\N	t
2585	TemplateField	216	Mail Order Rx (Tier 4)	9	2016-01-21 13:31:33.0412	2016-01-21 13:31:33.0412		mail-order-rx-tier-4	\N	\N
2586	TemplateField	216	Mail Order Rx (Tier 5)	10	2016-01-21 13:31:33.051458	2016-01-21 13:31:33.051458		mail-order-rx-tier-5	\N	\N
2587	TemplateField	216	Speciality Medications - Mail Order	11	2016-01-21 13:31:33.061516	2016-01-21 13:31:33.061516		speciality-medications-mail-order	\N	\N
2588	TemplateField	216	Speciality Medications - Retail	12	2016-01-21 13:31:33.072234	2016-01-21 13:31:33.072234		speciality-medications-retail	\N	\N
2591	TemplateField	217	Disease Management	1	2016-01-21 13:31:33.371308	2016-01-21 13:31:33.371308		disease-management	\N	\N
2596	TemplateField	218	Integrated Wellness Plan	1	2016-01-21 13:31:33.481677	2016-01-21 13:31:33.481677		integrated-wellness-plan	\N	\N
2603	TemplateField	219	Deductible - Family Multiple	6	2016-01-21 13:31:33.581379	2016-01-21 13:31:33.581379		deductible-family-multiple	\N	\N
2562	TemplateColumn	213	Limit	2	2016-01-21 13:31:30.92967	2016-01-21 15:26:03.402511		limit	\N	\N
2563	TemplateColumn	213	Separate Deductible	3	2016-01-21 13:31:30.939369	2016-01-21 15:26:26.347997		separate-deductible	\N	\N
2564	TemplateColumn	213	Separate Coinsurance	4	2016-01-21 13:31:30.949271	2016-01-21 15:26:43.029344		separate-coinsurance	\N	\N
2572	TemplateColumn	214	Entries	1	2016-01-21 13:31:32.756502	2016-01-21 15:27:27.710627		entries	\N	\N
2575	TemplateColumn	215	Deductible	1	2016-01-21 13:31:32.875756	2016-01-21 15:27:57.746839		deductible	\N	\N
2576	TemplateColumn	215	Per Member	2	2016-01-21 13:31:32.886438	2016-01-21 15:28:17.26333		per-member	\N	\N
2590	TemplateColumn	216	Coinsurance	2	2016-01-21 13:31:33.095181	2016-01-21 15:29:01.837255		coinsurance	\N	\N
2592	TemplateColumn	217	Identification Methods	1	2016-01-21 13:31:33.384199	2016-01-21 15:29:37.427907		identification-methods	\N	\N
2593	TemplateColumn	217	Number of Conditions Tracked	2	2016-01-21 13:31:33.394371	2016-01-21 15:29:53.086357		number-of-conditions-tracked	\N	\N
2594	TemplateColumn	217	Outreach Methods	3	2016-01-21 13:31:33.408715	2016-01-21 15:30:11.590854		outreach-methods	\N	\N
2595	TemplateColumn	217	Report Frequency	4	2016-01-21 13:31:33.418959	2016-01-21 15:30:27.215287		report-frequency	\N	\N
2597	TemplateColumn	218	Wellness	1	2016-01-21 13:31:33.493871	2016-01-21 15:31:00.025552		wellness	\N	\N
2663	TemplateColumn	224	Employee Classes	2	2016-01-21 15:35:40.40511	2016-01-21 15:35:40.40511		employee-classes	\N	\N
2599	TemplateField	219	Network	2	2016-01-21 13:31:33.541564	2016-01-22 14:19:13.177937		network	t	t
2604	TemplateField	219	Deductible Format	7	2016-01-21 13:31:33.591079	2016-01-22 14:20:34.521628		deductible-format	\N	t
2598	TemplateField	219	Plan Type	1	2016-01-21 13:31:33.531358	2016-01-22 14:18:30.436871		plan-type	t	t
2601	TemplateField	219	Deductible Individual	4	2016-01-21 13:31:33.561201	2016-01-22 14:20:12.341722		deductible-individual	t	t
2602	TemplateField	219	Deductible Family	5	2016-01-21 13:31:33.571071	2016-01-22 14:20:12.31954		deductible-family	t	t
2606	TemplateField	219	Out-of-Pocket Max Individual (includes deductible)	9	2016-01-21 13:31:33.612436	2016-01-22 14:20:46.144951		out-of-pocket-max-individual-includes-deductible	t	t
2605	TemplateField	219	Coinsurance	8	2016-01-21 13:31:33.601367	2016-01-22 14:20:45.946775		coinsurance	t	t
2608	TemplateField	219	Out-of-Pocket Maximum - Family Multiple	11	2016-01-21 13:31:33.633827	2016-01-21 13:31:33.633827		out-of-pocket-maximum-family-multiple	\N	\N
2611	TemplateField	219	Annual Maximum	14	2016-01-21 13:31:33.664714	2016-01-21 13:31:33.664714		annual-maximum	\N	\N
2612	TemplateField	219	Gym Reimbursement	15	2016-01-21 13:31:33.675969	2016-01-21 13:31:33.675969		gym-reimbursement	\N	\N
2613	TemplateField	219	Customer Service Days/Hours	16	2016-01-21 13:31:33.686459	2016-01-21 13:31:33.686459		customer-service-days-hours	\N	\N
2615	TemplateField	219	Nurseline	18	2016-01-21 13:31:33.70754	2016-01-21 13:31:33.70754		nurseline	\N	\N
2616	TemplateField	219	International Employees Covered	19	2016-01-21 13:31:33.718081	2016-01-21 13:31:33.718081		international-employees-covered	\N	\N
2448	TemplateColumn	205	Copay	2	2016-01-21 13:31:25.278704	2016-01-21 13:40:44.44299		copay	\N	\N
2407	TemplateColumn	204	Entries	1	2016-01-21 13:31:24.6499	2016-01-21 15:15:58.242584		entries	\N	\N
2477	TemplateColumn	208	Subject To Rx Deductible	1	2016-01-21 13:31:28.432024	2016-01-21 15:18:34.380487		subject-to-rx-deductible	\N	\N
2521	TemplateColumn	212	Entries	1	2016-01-21 13:31:30.28376	2016-01-21 15:25:44.735981		entries	\N	\N
2589	TemplateColumn	216	Subject to Rx Deductible	1	2016-01-21 13:31:33.084662	2016-01-21 15:28:49.847951		subject-to-rx-deductible	\N	\N
2618	TemplateField	220	Plan Type	1	2016-01-21 15:35:39.441495	2016-01-21 15:35:39.441495		plan-type	t	t
2619	TemplateField	220	Network	2	2016-01-21 15:35:39.452771	2016-01-21 15:35:39.452771		network	\N	t
2620	TemplateField	220	Deductible Individual	3	2016-01-21 15:35:39.462236	2016-01-21 15:35:39.462236		deductible-individual	t	t
2621	TemplateField	220	Deductible Family	4	2016-01-21 15:35:39.471048	2016-01-21 15:35:39.471048		deductible-family	t	t
2622	TemplateField	220	Deductible Waived for Preventive Care	5	2016-01-21 15:35:39.480216	2016-01-21 15:35:39.480216		deductible-waived-for-preventive-care	t	t
2623	TemplateField	220	Deductible -Calendar or Policy Year	6	2016-01-21 15:35:39.488955	2016-01-21 15:35:39.488955		deductible-calendar-or-policy-year	\N	\N
2624	TemplateField	220	Annual Plan Maximum (Per Enrollee)	7	2016-01-21 15:35:39.498394	2016-01-21 15:35:39.498394		annual-plan-maximum-per-enrollee	t	t
2625	TemplateField	220	Annual Rollover Benefit	8	2016-01-21 15:35:39.506921	2016-01-21 15:35:39.506921		annual-rollover-benefit	\N	t
2626	TemplateField	220	Annual Rollover Maximum	9	2016-01-21 15:35:39.515682	2016-01-21 15:35:39.515682		annual-rollover-maximum	\N	t
2627	TemplateField	220	OON Reimbursement Level	10	2016-01-21 15:35:39.524438	2016-01-21 15:35:39.524438		oon-reimbursement-level	\N	t
2628	TemplateField	220	International Employees Covered	11	2016-01-21 15:35:39.533141	2016-01-21 15:35:39.533141		international-employees-covered	\N	\N
2629	TemplateField	220	Waiting Period for Services	12	2016-01-21 15:35:39.54202	2016-01-21 15:35:39.54202		waiting-period-for-services	\N	t
2630	TemplateColumn	220	Entries	1	2016-01-21 15:35:39.553522	2016-01-21 15:35:39.553522		entries	\N	\N
2631	TemplateField	221	Diagnostic and Preventive (Type A)	1	2016-01-21 15:35:39.701262	2016-01-21 15:35:39.701262		diagnostic-and-preventive-type-a	t	t
2632	TemplateField	221	Basic Services (Type B)	2	2016-01-21 15:35:39.710273	2016-01-21 15:35:39.710273		basic-services-type-b	t	t
2633	TemplateField	221	Major Services (Type C)	3	2016-01-21 15:35:39.71917	2016-01-21 15:35:39.71917		major-services-type-c	t	t
2634	TemplateColumn	221	Covered	1	2016-01-21 15:35:39.729644	2016-01-21 15:35:39.729644		covered	\N	\N
2635	TemplateColumn	221	Not Covered	2	2016-01-21 15:35:39.738285	2016-01-21 15:35:39.738285		not-covered	\N	\N
2636	TemplateColumn	221	Subject to Deductible	3	2016-01-21 15:35:39.7471	2016-01-21 15:35:39.7471		subject-to-deductible	\N	\N
2637	TemplateColumn	221	Coinsurance	4	2016-01-21 15:35:39.755562	2016-01-21 15:35:39.755562		coinsurance	\N	\N
2638	TemplateColumn	221	Copays Apply	5	2016-01-21 15:35:39.764253	2016-01-21 15:35:39.764253		copays-apply	\N	\N
2639	TemplateField	222	Cleanings	1	2016-01-21 15:35:39.941644	2016-01-21 15:35:39.941644		cleanings	\N	t
2640	TemplateField	222	Sealants	2	2016-01-21 15:35:39.950075	2016-01-21 15:35:39.950075		sealants	\N	t
2641	TemplateField	222	Fillings (Amalgam & Composite)	3	2016-01-21 15:35:39.958839	2016-01-21 15:35:39.958839		fillings-amalgam-composite	\N	t
2642	TemplateField	222	Oral Surgery	4	2016-01-21 15:35:39.971693	2016-01-21 15:35:39.971693		oral-surgery	\N	t
2643	TemplateField	222	Periodontics	5	2016-01-21 15:35:39.979887	2016-01-21 15:35:39.979887		periodontics	\N	t
2644	TemplateField	222	Endodontics	6	2016-01-21 15:35:39.988577	2016-01-21 15:35:39.988577		endodontics	\N	t
2645	TemplateField	222	Implants	7	2016-01-21 15:35:39.997212	2016-01-21 15:35:39.997212		implants	\N	t
2646	TemplateField	222	Prosthodontics (including Dentures)	8	2016-01-21 15:35:40.005742	2016-01-21 15:35:40.005742		prosthodontics-including-dentures	\N	t
2647	TemplateField	222	Inlays/Onlays/Crowns	9	2016-01-21 15:35:40.014101	2016-01-21 15:35:40.014101		inlays-onlays-crowns	\N	t
2648	TemplateField	222	Simple Extractions	10	2016-01-21 15:35:40.022578	2016-01-21 15:35:40.022578		simple-extractions	\N	t
2649	TemplateColumn	222	Entries	1	2016-01-21 15:35:40.03415	2016-01-21 15:35:40.03415		entries	\N	\N
2650	TemplateField	223	Child Orthodontia	2	2016-01-21 15:35:40.152498	2016-01-21 15:35:40.152498		child-orthodontia	t	t
2651	TemplateField	223	Adult Orthodontia	1	2016-01-21 15:35:40.161342	2016-01-21 15:35:40.161342		adult-orthodontia	t	t
2652	TemplateColumn	223	Covered	1	2016-01-21 15:35:40.172089	2016-01-21 15:35:40.172089		covered	\N	\N
2653	TemplateColumn	223	Not Covered	2	2016-01-21 15:35:40.180784	2016-01-21 15:35:40.180784		not-covered	\N	\N
2654	TemplateColumn	223	Ortho Deductible	3	2016-01-21 15:35:40.190027	2016-01-21 15:35:40.190027		ortho-deductible	\N	\N
2655	TemplateColumn	223	Coinsurance	4	2016-01-21 15:35:40.199408	2016-01-21 15:35:40.199408		coinsurance	\N	\N
2656	TemplateColumn	223	Lifetime Maximum	5	2016-01-21 15:35:40.208243	2016-01-21 15:35:40.208243		lifetime-maximum	\N	\N
2657	TemplateColumn	223	Copays Apply	6	2016-01-21 15:35:40.216927	2016-01-21 15:35:40.216927		copays-apply	\N	\N
2658	TemplateField	224	Employee Eligibility Class #3	3	2016-01-21 15:35:40.359282	2016-01-21 15:35:40.359282		employee-eligibility-class-3	\N	\N
2659	TemplateField	224	Employee Eligibility Class #1	1	2016-01-21 15:35:40.368131	2016-01-21 15:35:40.368131		employee-eligibility-class-1	\N	\N
2660	TemplateField	224	Employee Eligibility Class #2	2	2016-01-21 15:35:40.376875	2016-01-21 15:35:40.376875		employee-eligibility-class-2	\N	\N
2661	TemplateField	224	Employee Eligibility Class #4	4	2016-01-21 15:35:40.385511	2016-01-21 15:35:40.385511		employee-eligibility-class-4	\N	\N
2662	TemplateColumn	224	Employee Class	1	2016-01-21 15:35:40.39664	2016-01-21 15:35:40.39664		employee-class	\N	\N
2609	TemplateField	219	In & Out-of-Network Deductibles Cross Accumulation	12	2016-01-21 13:31:33.644081	2016-01-22 14:20:58.785771		in-out-of-network-deductibles-cross-accumulation	\N	t
2610	TemplateField	219	In & Out-of-Network OOP Maximum Cross Accumulation	13	2016-01-21 13:31:33.654255	2016-01-22 14:20:59.072888		in-out-of-network-oop-maximum-cross-accumulation	\N	t
2617	TemplateColumn	219	Entries	1	2016-01-21 13:31:33.730045	2016-01-23 15:26:53.341884		entries	\N	\N
2614	TemplateField	219	OON Reimbursement Level	17	2016-01-21 13:31:33.696866	2016-01-22 14:21:10.227815		oon-reimbursement-level	t	t
2664	TemplateColumn	224	Waiting Period	3	2016-01-21 15:35:40.413859	2016-01-21 15:35:40.413859		waiting-period	\N	\N
2665	TemplateColumn	224	Spouse Eligible	5	2016-01-21 15:35:40.422403	2016-01-21 15:35:40.422403		spouse-eligible	\N	\N
2666	TemplateColumn	224	Hours Requirement	4	2016-01-21 15:35:40.430824	2016-01-21 15:35:40.430824		hours-requirement	\N	\N
2667	TemplateColumn	224	Dependent Children Eligible	6	2016-01-21 15:35:40.439495	2016-01-21 15:35:40.439495		dependent-children-eligible	\N	\N
2668	TemplateColumn	224	Domestic Partner Eligible	7	2016-01-21 15:35:40.448023	2016-01-21 15:35:40.448023		domestic-partner-eligible	\N	\N
2669	TemplateColumn	224	Dependent Child Coverage Ends	9	2016-01-21 15:35:40.456365	2016-01-21 15:35:40.456365		dependent-child-coverage-ends	\N	\N
2670	TemplateColumn	224	Dependent Child Limiting Age	8	2016-01-21 15:35:40.46495	2016-01-21 15:35:40.46495		dependent-child-limiting-age	\N	\N
2671	TemplateField	225	Network	2	2016-01-21 15:35:40.868716	2016-01-21 15:35:40.868716		network	\N	t
2672	TemplateField	225	Deductible Individual	3	2016-01-21 15:35:40.880675	2016-01-21 15:35:40.880675		deductible-individual	t	t
2673	TemplateField	225	Deductible Family	4	2016-01-21 15:35:40.890707	2016-01-21 15:35:40.890707		deductible-family	t	t
2674	TemplateField	225	Deductible -Calendar or Policy Year	6	2016-01-21 15:35:40.899889	2016-01-21 15:35:40.899889		deductible-calendar-or-policy-year	\N	t
2675	TemplateField	225	Annual Plan Maximum (Per Enrollee)	7	2016-01-21 15:35:40.908635	2016-01-21 15:35:40.908635		annual-plan-maximum-per-enrollee	t	t
2676	TemplateField	225	Annual Rollover Benefit	8	2016-01-21 15:35:40.917248	2016-01-21 15:35:40.917248		annual-rollover-benefit	\N	t
2677	TemplateField	225	Annual Rollover Maximum	9	2016-01-21 15:35:40.925954	2016-01-21 15:35:40.925954		annual-rollover-maximum	\N	t
2678	TemplateField	225	OON Reimbursement Level	10	2016-01-21 15:35:40.939424	2016-01-21 15:35:40.939424		oon-reimbursement-level	\N	t
2679	TemplateField	225	International Employees Covered	11	2016-01-21 15:35:40.948482	2016-01-21 15:35:40.948482		international-employees-covered	\N	\N
2680	TemplateField	225	Waiting Period for Services	12	2016-01-21 15:35:40.957292	2016-01-21 15:35:40.957292		waiting-period-for-services	\N	t
2681	TemplateField	225	Plan Type	1	2016-01-21 15:35:40.966013	2016-01-21 15:35:40.966013		plan-type	t	t
2682	TemplateField	225	Deductible Waived for Preventive Care	5	2016-01-21 15:35:40.975119	2016-01-21 15:35:40.975119		deductible-waived-for-preventive-care	t	t
2683	TemplateColumn	225	ENTRIES	1	2016-01-21 15:35:40.985669	2016-01-21 15:35:40.985669		entries	\N	\N
2684	TemplateField	226	Diagnostic and Preventive (Type A)	1	2016-01-21 15:35:41.130258	2016-01-21 15:35:41.130258		diagnostic-and-preventive-type-a	t	t
2685	TemplateField	226	Basic Services (Type B)	2	2016-01-21 15:35:41.140051	2016-01-21 15:35:41.140051		basic-services-type-b	t	t
2686	TemplateField	226	Major Services (Type C)	3	2016-01-21 15:35:41.148965	2016-01-21 15:35:41.148965		major-services-type-c	t	t
2687	TemplateColumn	226	Covered	1	2016-01-21 15:35:41.159547	2016-01-21 15:35:41.159547		covered	\N	\N
2688	TemplateColumn	226	Not Covered	2	2016-01-21 15:35:41.169876	2016-01-21 15:35:41.169876		not-covered	\N	\N
2689	TemplateColumn	226	Subject to Deductible	3	2016-01-21 15:35:41.182911	2016-01-21 15:35:41.182911		subject-to-deductible	\N	\N
2690	TemplateColumn	226	Coinsurance	4	2016-01-21 15:35:41.191402	2016-01-21 15:35:41.191402		coinsurance	\N	\N
2691	TemplateField	227	Cleanings	1	2016-01-21 15:35:41.327488	2016-01-21 15:35:41.327488		cleanings	\N	t
2692	TemplateField	227	Sealants	2	2016-01-21 15:35:41.336049	2016-01-21 15:35:41.336049		sealants	\N	t
2693	TemplateField	227	Fillings (Amalgam & Composite)	3	2016-01-21 15:35:41.345242	2016-01-21 15:35:41.345242		fillings-amalgam-composite	\N	t
2694	TemplateField	227	Simple Extractions	4	2016-01-21 15:35:41.354137	2016-01-21 15:35:41.354137		simple-extractions	\N	t
2695	TemplateField	227	Oral Surgery	5	2016-01-21 15:35:41.362804	2016-01-21 15:35:41.362804		oral-surgery	\N	t
2696	TemplateField	227	Periodontics	6	2016-01-21 15:35:41.386597	2016-01-21 15:35:41.386597		periodontics	\N	t
2697	TemplateField	227	Endodontics	7	2016-01-21 15:35:41.39852	2016-01-21 15:35:41.39852		endodontics	\N	t
2698	TemplateField	227	Implants	8	2016-01-21 15:35:41.408019	2016-01-21 15:35:41.408019		implants	\N	t
2699	TemplateField	227	Prosthodontics (including Dentures)	9	2016-01-21 15:35:41.41688	2016-01-21 15:35:41.41688		prosthodontics-including-dentures	\N	t
2700	TemplateField	227	Inlays/Onlays/Crowns	10	2016-01-21 15:35:41.425807	2016-01-21 15:35:41.425807		inlays-onlays-crowns	\N	t
2701	TemplateColumn	227	Entries	1	2016-01-21 15:35:41.436351	2016-01-21 15:35:41.436351		entries	\N	\N
2702	TemplateField	228	Adult Orthodontia	1	2016-01-21 15:35:41.552715	2016-01-21 15:35:41.552715		adult-orthodontia	t	t
2703	TemplateField	228	Child Orthodontia	2	2016-01-21 15:35:41.561584	2016-01-21 15:35:41.561584		child-orthodontia	t	t
2704	TemplateColumn	228	Covered	1	2016-01-21 15:35:41.571998	2016-01-21 15:35:41.571998		covered	\N	\N
2705	TemplateColumn	228	Not Covered	2	2016-01-21 15:35:41.580575	2016-01-21 15:35:41.580575		not-covered	\N	\N
2706	TemplateColumn	228	Ortho Deductible	3	2016-01-21 15:35:41.588879	2016-01-21 15:35:41.588879		ortho-deductible	\N	\N
2707	TemplateColumn	228	Coinsurance	4	2016-01-21 15:35:41.597427	2016-01-21 15:35:41.597427		coinsurance	\N	\N
2708	TemplateColumn	228	Lifetime Maximum	5	2016-01-21 15:35:41.605969	2016-01-21 15:35:41.605969		lifetime-maximum	\N	\N
2709	TemplateField	229	Network	2	2016-01-21 15:35:41.730694	2016-01-21 15:35:41.730694		network	\N	t
2710	TemplateField	229	Deductible Individual	3	2016-01-21 15:35:41.739506	2016-01-21 15:35:41.739506		deductible-individual	t	t
2711	TemplateField	229	Deductible Family	4	2016-01-21 15:35:41.748042	2016-01-21 15:35:41.748042		deductible-family	t	t
2712	TemplateField	229	Deductible -Calendar or Policy Year	6	2016-01-21 15:35:41.756648	2016-01-21 15:35:41.756648		deductible-calendar-or-policy-year	\N	t
2713	TemplateField	229	Annual Plan Maximum (Per Enrollee)	7	2016-01-21 15:35:41.765349	2016-01-21 15:35:41.765349		annual-plan-maximum-per-enrollee	t	t
2714	TemplateField	229	Annual Rollover Benefit	8	2016-01-21 15:35:41.773895	2016-01-21 15:35:41.773895		annual-rollover-benefit	\N	t
2715	TemplateField	229	Annual Rollover Maximum	9	2016-01-21 15:35:41.782294	2016-01-21 15:35:41.782294		annual-rollover-maximum	\N	\N
2716	TemplateField	229	OON Reimbursement Level	10	2016-01-21 15:35:41.790874	2016-01-21 15:35:41.790874		oon-reimbursement-level	\N	t
2717	TemplateField	229	International Employees Covered	11	2016-01-21 15:35:41.799492	2016-01-21 15:35:41.799492		international-employees-covered	\N	\N
2718	TemplateField	229	Waiting Period for Services	12	2016-01-21 15:35:41.808252	2016-01-21 15:35:41.808252		waiting-period-for-services	\N	\N
2719	TemplateField	229	Plan Type	1	2016-01-21 15:35:41.816939	2016-01-21 15:35:41.816939		plan-type	t	t
2720	TemplateField	229	Deductible Waived for Preventive Care	5	2016-01-21 15:35:41.825943	2016-01-21 15:35:41.825943		deductible-waived-for-preventive-care	t	t
2721	TemplateColumn	229	ENTRIES	1	2016-01-21 15:35:41.836473	2016-01-21 15:35:41.836473		entries	\N	\N
2722	TemplateField	230	Diagnostic and Preventive (Type A)	1	2016-01-21 15:35:41.978778	2016-01-21 15:35:41.978778		diagnostic-and-preventive-type-a	t	t
2723	TemplateField	230	Basic Services (Type B)	2	2016-01-21 15:35:41.988148	2016-01-21 15:35:41.988148		basic-services-type-b	t	t
2724	TemplateField	230	Major Services (Type C)	3	2016-01-21 15:35:41.997183	2016-01-21 15:35:41.997183		major-services-type-c	t	t
2725	TemplateColumn	230	Covered	1	2016-01-21 15:35:42.008142	2016-01-21 15:35:42.008142		covered	\N	\N
2726	TemplateColumn	230	Not Covered	2	2016-01-21 15:35:42.016622	2016-01-21 15:35:42.016622		not-covered	\N	\N
2727	TemplateColumn	230	Subject to Deductible	3	2016-01-21 15:35:42.025496	2016-01-21 15:35:42.025496		subject-to-deductible	\N	\N
2728	TemplateColumn	230	Coinsurance	4	2016-01-21 15:35:42.033848	2016-01-21 15:35:42.033848		coinsurance	\N	\N
2729	TemplateColumn	230	Copays Apply	5	2016-01-21 15:35:42.042428	2016-01-21 15:35:42.042428		copays-apply	\N	\N
2730	TemplateField	231	Cleanings	1	2016-01-21 15:35:42.219597	2016-01-21 15:35:42.219597		cleanings	\N	t
2731	TemplateField	231	Sealants	2	2016-01-21 15:35:42.228432	2016-01-21 15:35:42.228432		sealants	\N	t
2732	TemplateField	231	Fillings (Amalgam & Composite)	3	2016-01-21 15:35:42.237093	2016-01-21 15:35:42.237093		fillings-amalgam-composite	\N	t
2733	TemplateField	231	Simple Extractions	4	2016-01-21 15:35:42.245477	2016-01-21 15:35:42.245477		simple-extractions	\N	t
2734	TemplateField	231	Oral Surgery	5	2016-01-21 15:35:42.25384	2016-01-21 15:35:42.25384		oral-surgery	\N	t
2735	TemplateField	231	Periodontics	6	2016-01-21 15:35:42.263377	2016-01-21 15:35:42.263377		periodontics	\N	t
2736	TemplateField	231	Endodontics	7	2016-01-21 15:35:42.271793	2016-01-21 15:35:42.271793		endodontics	\N	t
2737	TemplateField	231	Implants	8	2016-01-21 15:35:42.280093	2016-01-21 15:35:42.280093		implants	\N	t
2738	TemplateField	231	Prosthodontics (including Dentures)	9	2016-01-21 15:35:42.288318	2016-01-21 15:35:42.288318		prosthodontics-including-dentures	\N	t
2739	TemplateField	231	Inlays/Onlays/Crowns	10	2016-01-21 15:35:42.29679	2016-01-21 15:35:42.29679		inlays-onlays-crowns	\N	t
2740	TemplateColumn	231	Entries	1	2016-01-21 15:35:42.306892	2016-01-21 15:35:42.306892		entries	\N	\N
2741	TemplateField	232	Adult Orthodontia	1	2016-01-21 15:35:42.421478	2016-01-21 15:35:42.421478		adult-orthodontia	t	t
2742	TemplateField	232	Child Orthodontia	2	2016-01-21 15:35:42.430257	2016-01-21 15:35:42.430257		child-orthodontia	t	t
2743	TemplateColumn	232	Covered	1	2016-01-21 15:35:42.440712	2016-01-21 15:35:42.440712		covered	\N	\N
2744	TemplateColumn	232	Not Covered	2	2016-01-21 15:35:42.449132	2016-01-21 15:35:42.449132		not-covered	\N	\N
2745	TemplateColumn	232	Ortho Deductible	3	2016-01-21 15:35:42.457997	2016-01-21 15:35:42.457997		ortho-deductible	\N	\N
2746	TemplateColumn	232	Coinsurance	4	2016-01-21 15:35:42.467108	2016-01-21 15:35:42.467108		coinsurance	\N	\N
2747	TemplateColumn	232	Lifetime Maximum	5	2016-01-21 15:35:42.476445	2016-01-21 15:35:42.476445		lifetime-maximum	\N	\N
2748	TemplateColumn	232	Copays Apply	6	2016-01-21 15:35:42.485215	2016-01-21 15:35:42.485215		copays-apply	\N	\N
2749	TemplateField	233	Employee Eligibility Class #3	3	2016-01-21 16:46:00.27919	2016-01-21 16:46:00.27919		employee-eligibility-class-3	\N	\N
2750	TemplateField	233	Employee Eligibility Class #1	1	2016-01-21 16:46:00.288476	2016-01-21 16:46:00.288476		employee-eligibility-class-1	\N	\N
2751	TemplateField	233	Employee Eligibility Class #2	2	2016-01-21 16:46:00.298135	2016-01-21 16:46:00.298135		employee-eligibility-class-2	\N	\N
2752	TemplateField	233	Employee Eligibility Class #4	4	2016-01-21 16:46:00.30678	2016-01-21 16:46:00.30678		employee-eligibility-class-4	\N	\N
2753	TemplateColumn	233	Employee Classes	2	2016-01-21 16:46:00.317434	2016-01-21 16:46:00.317434		employee-classes	\N	\N
2754	TemplateColumn	233	Employee Class	1	2016-01-21 16:46:00.326176	2016-01-21 16:46:00.326176		employee-class	\N	\N
2755	TemplateColumn	233	Waiting Period	3	2016-01-21 16:46:00.33476	2016-01-21 16:46:00.33476		waiting-period	\N	\N
2756	TemplateColumn	233	Spouse Eligible	5	2016-01-21 16:46:00.343595	2016-01-21 16:46:00.343595		spouse-eligible	\N	\N
2757	TemplateColumn	233	Hours Requirement	4	2016-01-21 16:46:00.352658	2016-01-21 16:46:00.352658		hours-requirement	\N	\N
2758	TemplateColumn	233	Dependent Children Eligible	6	2016-01-21 16:46:00.361603	2016-01-21 16:46:00.361603		dependent-children-eligible	\N	\N
2759	TemplateColumn	233	Domestic Partner Eligible	7	2016-01-21 16:46:00.370452	2016-01-21 16:46:00.370452		domestic-partner-eligible	\N	\N
2760	TemplateColumn	233	Dependent Child Coverage Ends	9	2016-01-21 16:46:00.379579	2016-01-21 16:46:00.379579		dependent-child-coverage-ends	\N	\N
2761	TemplateColumn	233	Dependent Child Limiting Age	8	2016-01-21 16:46:00.388301	2016-01-21 16:46:00.388301		dependent-child-limiting-age	\N	\N
2762	TemplateField	234	Child Orthodontia	2	2016-01-21 16:46:00.76454	2016-01-21 16:46:00.76454		child-orthodontia	t	t
2763	TemplateField	234	Adult Orthodontia	1	2016-01-21 16:46:00.773293	2016-01-21 16:46:00.773293		adult-orthodontia	t	t
2764	TemplateColumn	234	Covered	1	2016-01-21 16:46:00.783554	2016-01-21 16:46:00.783554		covered	\N	\N
2765	TemplateColumn	234	Not Covered	2	2016-01-21 16:46:00.792823	2016-01-21 16:46:00.792823		not-covered	\N	\N
2766	TemplateColumn	234	Ortho Deductible	3	2016-01-21 16:46:00.80254	2016-01-21 16:46:00.80254		ortho-deductible	\N	\N
2767	TemplateColumn	234	Coinsurance	4	2016-01-21 16:46:00.811334	2016-01-21 16:46:00.811334		coinsurance	\N	\N
2768	TemplateColumn	234	Lifetime Maximum	5	2016-01-21 16:46:00.820315	2016-01-21 16:46:00.820315		lifetime-maximum	\N	\N
2769	TemplateColumn	234	Copays Apply	6	2016-01-21 16:46:00.828866	2016-01-21 16:46:00.828866		copays-apply	\N	\N
2770	TemplateField	235	Plan Type	1	2016-01-21 16:46:00.96476	2016-01-21 16:46:00.96476		plan-type	t	t
2771	TemplateField	235	Network	2	2016-01-21 16:46:00.973399	2016-01-21 16:46:00.973399		network	\N	t
2772	TemplateField	235	Deductible Individual	3	2016-01-21 16:46:00.982084	2016-01-21 16:46:00.982084		deductible-individual	t	t
2773	TemplateField	235	Deductible Family	4	2016-01-21 16:46:00.990943	2016-01-21 16:46:00.990943		deductible-family	t	t
2774	TemplateField	235	Deductible Waived for Preventive Care	5	2016-01-21 16:46:00.999795	2016-01-21 16:46:00.999795		deductible-waived-for-preventive-care	t	t
2775	TemplateField	235	Deductible -Calendar or Policy Year	6	2016-01-21 16:46:01.008775	2016-01-21 16:46:01.008775		deductible-calendar-or-policy-year	\N	\N
2776	TemplateField	235	Annual Plan Maximum (Per Enrollee)	7	2016-01-21 16:46:01.018378	2016-01-21 16:46:01.018378		annual-plan-maximum-per-enrollee	t	t
2777	TemplateField	235	Annual Rollover Benefit	8	2016-01-21 16:46:01.029609	2016-01-21 16:46:01.029609		annual-rollover-benefit	\N	t
2778	TemplateField	235	Annual Rollover Maximum	9	2016-01-21 16:46:01.038255	2016-01-21 16:46:01.038255		annual-rollover-maximum	\N	t
2779	TemplateField	235	OON Reimbursement Level	10	2016-01-21 16:46:01.047064	2016-01-21 16:46:01.047064		oon-reimbursement-level	\N	t
2780	TemplateField	235	International Employees Covered	11	2016-01-21 16:46:01.056977	2016-01-21 16:46:01.056977		international-employees-covered	\N	\N
2781	TemplateField	235	Waiting Period for Services	12	2016-01-21 16:46:01.066319	2016-01-21 16:46:01.066319		waiting-period-for-services	\N	t
2782	TemplateColumn	235	Entries	1	2016-01-21 16:46:01.07679	2016-01-21 16:46:01.07679		entries	\N	\N
2783	TemplateField	236	Diagnostic and Preventive (Type A)	1	2016-01-21 16:46:01.212269	2016-01-21 16:46:01.212269		diagnostic-and-preventive-type-a	t	t
2784	TemplateField	236	Basic Services (Type B)	2	2016-01-21 16:46:01.221259	2016-01-21 16:46:01.221259		basic-services-type-b	t	t
2785	TemplateField	236	Major Services (Type C)	3	2016-01-21 16:46:01.231506	2016-01-21 16:46:01.231506		major-services-type-c	t	t
2786	TemplateColumn	236	Covered	1	2016-01-21 16:46:01.262907	2016-01-21 16:46:01.262907		covered	\N	\N
2787	TemplateColumn	236	Not Covered	2	2016-01-21 16:46:01.271708	2016-01-21 16:46:01.271708		not-covered	\N	\N
2788	TemplateColumn	236	Subject to Deductible	3	2016-01-21 16:46:01.280845	2016-01-21 16:46:01.280845		subject-to-deductible	\N	\N
2789	TemplateColumn	236	Coinsurance	4	2016-01-21 16:46:01.290758	2016-01-21 16:46:01.290758		coinsurance	\N	\N
2790	TemplateColumn	236	Copays Apply	5	2016-01-21 16:46:01.300631	2016-01-21 16:46:01.300631		copays-apply	\N	\N
2791	TemplateField	237	Cleanings	1	2016-01-21 16:46:01.545072	2016-01-21 16:46:01.545072		cleanings	\N	t
2801	TemplateColumn	237	Entries	1	2016-01-21 16:46:01.655925	2016-01-21 16:46:01.655925		entries	\N	\N
2802	TemplateField	238	Adult Orthodontia	1	2016-01-21 16:46:01.793042	2016-01-21 16:46:01.793042		adult-orthodontia	t	t
2803	TemplateField	238	Child Orthodontia	2	2016-01-21 16:46:01.803337	2016-01-21 16:46:01.803337		child-orthodontia	t	t
2804	TemplateColumn	238	Covered	1	2016-01-21 16:46:01.815007	2016-01-21 16:46:01.815007		covered	\N	\N
2805	TemplateColumn	238	Not Covered	2	2016-01-21 16:46:01.824813	2016-01-21 16:46:01.824813		not-covered	\N	\N
2806	TemplateColumn	238	Ortho Deductible	3	2016-01-21 16:46:01.834081	2016-01-21 16:46:01.834081		ortho-deductible	\N	\N
2807	TemplateColumn	238	Coinsurance	4	2016-01-21 16:46:01.843454	2016-01-21 16:46:01.843454		coinsurance	\N	\N
2808	TemplateColumn	238	Lifetime Maximum	5	2016-01-21 16:46:01.852915	2016-01-21 16:46:01.852915		lifetime-maximum	\N	\N
2809	TemplateField	239	Cleanings	1	2016-01-21 16:46:01.979739	2016-01-21 16:46:01.979739		cleanings	\N	t
2810	TemplateField	239	Sealants	2	2016-01-21 16:46:01.989825	2016-01-21 16:46:01.989825		sealants	\N	t
2811	TemplateField	239	Fillings (Amalgam & Composite)	3	2016-01-21 16:46:01.999799	2016-01-21 16:46:01.999799		fillings-amalgam-composite	\N	t
2812	TemplateField	239	Simple Extractions	4	2016-01-21 16:46:02.013055	2016-01-21 16:46:02.013055		simple-extractions	\N	t
2813	TemplateField	239	Oral Surgery	5	2016-01-21 16:46:02.022334	2016-01-21 16:46:02.022334		oral-surgery	\N	t
2814	TemplateField	239	Periodontics	6	2016-01-21 16:46:02.031533	2016-01-21 16:46:02.031533		periodontics	\N	t
2815	TemplateField	239	Endodontics	7	2016-01-21 16:46:02.040233	2016-01-21 16:46:02.040233		endodontics	\N	t
2816	TemplateField	239	Implants	8	2016-01-21 16:46:02.048797	2016-01-21 16:46:02.048797		implants	\N	t
2817	TemplateField	239	Prosthodontics (including Dentures)	9	2016-01-21 16:46:02.057811	2016-01-21 16:46:02.057811		prosthodontics-including-dentures	\N	t
2818	TemplateField	239	Inlays/Onlays/Crowns	10	2016-01-21 16:46:02.066962	2016-01-21 16:46:02.066962		inlays-onlays-crowns	\N	t
2819	TemplateColumn	239	Entries	1	2016-01-21 16:46:02.07805	2016-01-21 16:46:02.07805		entries	\N	\N
2820	TemplateField	240	Diagnostic and Preventive (Type A)	1	2016-01-21 16:46:02.199874	2016-01-21 16:46:02.199874		diagnostic-and-preventive-type-a	t	t
2821	TemplateField	240	Basic Services (Type B)	2	2016-01-21 16:46:02.209048	2016-01-21 16:46:02.209048		basic-services-type-b	t	t
2822	TemplateField	240	Major Services (Type C)	3	2016-01-21 16:46:02.217916	2016-01-21 16:46:02.217916		major-services-type-c	t	t
2823	TemplateColumn	240	Covered	1	2016-01-21 16:46:02.235321	2016-01-21 16:46:02.235321		covered	\N	\N
2824	TemplateColumn	240	Not Covered	2	2016-01-21 16:46:02.244913	2016-01-21 16:46:02.244913		not-covered	\N	\N
2825	TemplateColumn	240	Subject to Deductible	3	2016-01-21 16:46:02.257541	2016-01-21 16:46:02.257541		subject-to-deductible	\N	\N
2826	TemplateColumn	240	Coinsurance	4	2016-01-21 16:46:02.26651	2016-01-21 16:46:02.26651		coinsurance	\N	\N
2827	TemplateField	241	Network	2	2016-01-21 16:46:02.409811	2016-01-21 16:46:02.409811		network	\N	t
2828	TemplateField	241	Deductible Individual	3	2016-01-21 16:46:02.418545	2016-01-21 16:46:02.418545		deductible-individual	t	t
2829	TemplateField	241	Deductible Family	4	2016-01-21 16:46:02.427793	2016-01-21 16:46:02.427793		deductible-family	t	t
2830	TemplateField	241	Deductible -Calendar or Policy Year	6	2016-01-21 16:46:02.43718	2016-01-21 16:46:02.43718		deductible-calendar-or-policy-year	\N	t
2831	TemplateField	241	Annual Plan Maximum (Per Enrollee)	7	2016-01-21 16:46:02.446501	2016-01-21 16:46:02.446501		annual-plan-maximum-per-enrollee	t	t
2832	TemplateField	241	Annual Rollover Benefit	8	2016-01-21 16:46:02.456003	2016-01-21 16:46:02.456003		annual-rollover-benefit	\N	t
2833	TemplateField	241	Annual Rollover Maximum	9	2016-01-21 16:46:02.46565	2016-01-21 16:46:02.46565		annual-rollover-maximum	\N	t
2834	TemplateField	241	OON Reimbursement Level	10	2016-01-21 16:46:02.475213	2016-01-21 16:46:02.475213		oon-reimbursement-level	\N	t
2835	TemplateField	241	International Employees Covered	11	2016-01-21 16:46:02.485285	2016-01-21 16:46:02.485285		international-employees-covered	\N	\N
2836	TemplateField	241	Waiting Period for Services	12	2016-01-21 16:46:02.496719	2016-01-21 16:46:02.496719		waiting-period-for-services	\N	t
2837	TemplateField	241	Plan Type	1	2016-01-21 16:46:02.505977	2016-01-21 16:46:02.505977		plan-type	t	t
2838	TemplateField	241	Deductible Waived for Preventive Care	5	2016-01-21 16:46:02.514692	2016-01-21 16:46:02.514692		deductible-waived-for-preventive-care	t	t
2840	TemplateField	242	Adult Orthodontia	1	2016-01-21 16:46:02.674369	2016-01-21 16:46:02.674369		adult-orthodontia	t	t
2841	TemplateField	242	Child Orthodontia	2	2016-01-21 16:46:02.684568	2016-01-21 16:46:02.684568		child-orthodontia	t	t
2842	TemplateColumn	242	Covered	1	2016-01-21 16:46:02.695951	2016-01-21 16:46:02.695951		covered	\N	\N
2843	TemplateColumn	242	Not Covered	2	2016-01-21 16:46:02.705438	2016-01-21 16:46:02.705438		not-covered	\N	\N
2844	TemplateColumn	242	Ortho Deductible	3	2016-01-21 16:46:02.714992	2016-01-21 16:46:02.714992		ortho-deductible	\N	\N
2845	TemplateColumn	242	Coinsurance	4	2016-01-21 16:46:02.724434	2016-01-21 16:46:02.724434		coinsurance	\N	\N
2846	TemplateColumn	242	Lifetime Maximum	5	2016-01-21 16:46:02.734084	2016-01-21 16:46:02.734084		lifetime-maximum	\N	\N
2847	TemplateColumn	242	Copays Apply	6	2016-01-21 16:46:02.743158	2016-01-21 16:46:02.743158		copays-apply	\N	\N
2848	TemplateField	243	Cleanings	1	2016-01-21 16:46:02.887618	2016-01-21 16:46:02.887618		cleanings	\N	t
2849	TemplateField	243	Sealants	2	2016-01-21 16:46:02.897238	2016-01-21 16:46:02.897238		sealants	\N	t
2850	TemplateField	243	Fillings (Amalgam & Composite)	3	2016-01-21 16:46:02.906872	2016-01-21 16:46:02.906872		fillings-amalgam-composite	\N	t
2851	TemplateField	243	Simple Extractions	4	2016-01-21 16:46:02.916209	2016-01-21 16:46:02.916209		simple-extractions	\N	t
2793	TemplateField	237	Fillings (Amalgam & Composite)	5	2016-01-21 16:46:01.567992	2016-01-21 16:47:26.259129		fillings-amalgam-composite	\N	t
2800	TemplateField	237	Simple Extractions	7	2016-01-21 16:46:01.643966	2016-01-21 16:47:26.324773		simple-extractions	\N	t
2795	TemplateField	237	Periodontics	11	2016-01-21 16:46:01.588818	2016-01-21 16:47:26.358533		periodontics	\N	t
2796	TemplateField	237	Endodontics	13	2016-01-21 16:46:01.603252	2016-01-21 16:47:26.390896		endodontics	\N	t
2797	TemplateField	237	Implants	15	2016-01-21 16:46:01.613472	2016-01-21 16:47:26.458578		implants	\N	t
2799	TemplateField	237	Inlays/Onlays/Crowns	19	2016-01-21 16:46:01.633716	2016-01-21 16:47:26.489417		inlays-onlays-crowns	\N	t
2794	TemplateField	237	Oral Surgery	9	2016-01-21 16:46:01.578238	2016-01-21 16:47:26.765957		oral-surgery	\N	t
2839	TemplateColumn	241	Entries	1	2016-01-21 16:46:02.525593	2016-01-23 15:08:57.104122		entries	\N	\N
2852	TemplateField	243	Oral Surgery	5	2016-01-21 16:46:02.925952	2016-01-21 16:46:02.925952		oral-surgery	\N	t
2853	TemplateField	243	Periodontics	6	2016-01-21 16:46:02.935742	2016-01-21 16:46:02.935742		periodontics	\N	t
2854	TemplateField	243	Endodontics	7	2016-01-21 16:46:02.945129	2016-01-21 16:46:02.945129		endodontics	\N	t
2855	TemplateField	243	Implants	8	2016-01-21 16:46:02.95522	2016-01-21 16:46:02.95522		implants	\N	t
2856	TemplateField	243	Prosthodontics (including Dentures)	9	2016-01-21 16:46:02.965015	2016-01-21 16:46:02.965015		prosthodontics-including-dentures	\N	t
2857	TemplateField	243	Inlays/Onlays/Crowns	10	2016-01-21 16:46:02.974683	2016-01-21 16:46:02.974683		inlays-onlays-crowns	\N	t
2858	TemplateColumn	243	Entries	1	2016-01-21 16:46:02.986677	2016-01-21 16:46:02.986677		entries	\N	\N
2859	TemplateField	244	Diagnostic and Preventive (Type A)	1	2016-01-21 16:46:03.108378	2016-01-21 16:46:03.108378		diagnostic-and-preventive-type-a	t	t
2860	TemplateField	244	Basic Services (Type B)	2	2016-01-21 16:46:03.117967	2016-01-21 16:46:03.117967		basic-services-type-b	t	t
2861	TemplateField	244	Major Services (Type C)	3	2016-01-21 16:46:03.127627	2016-01-21 16:46:03.127627		major-services-type-c	t	t
2862	TemplateColumn	244	Covered	1	2016-01-21 16:46:03.13923	2016-01-21 16:46:03.13923		covered	\N	\N
2863	TemplateColumn	244	Not Covered	2	2016-01-21 16:46:03.148323	2016-01-21 16:46:03.148323		not-covered	\N	\N
2864	TemplateColumn	244	Subject to Deductible	3	2016-01-21 16:46:03.157677	2016-01-21 16:46:03.157677		subject-to-deductible	\N	\N
2865	TemplateColumn	244	Coinsurance	4	2016-01-21 16:46:03.167445	2016-01-21 16:46:03.167445		coinsurance	\N	\N
2866	TemplateColumn	244	Copays Apply	5	2016-01-21 16:46:03.176905	2016-01-21 16:46:03.176905		copays-apply	\N	\N
2867	TemplateField	245	Network	2	2016-01-21 16:46:03.352401	2016-01-21 16:46:03.352401		network	\N	t
2868	TemplateField	245	Deductible Individual	3	2016-01-21 16:46:03.362801	2016-01-21 16:46:03.362801		deductible-individual	t	t
2869	TemplateField	245	Deductible Family	4	2016-01-21 16:46:03.372748	2016-01-21 16:46:03.372748		deductible-family	t	t
2870	TemplateField	245	Deductible -Calendar or Policy Year	6	2016-01-21 16:46:03.383327	2016-01-21 16:46:03.383327		deductible-calendar-or-policy-year	\N	t
2871	TemplateField	245	Annual Plan Maximum (Per Enrollee)	7	2016-01-21 16:46:03.393932	2016-01-21 16:46:03.393932		annual-plan-maximum-per-enrollee	t	t
2872	TemplateField	245	Annual Rollover Benefit	8	2016-01-21 16:46:03.404589	2016-01-21 16:46:03.404589		annual-rollover-benefit	\N	t
2873	TemplateField	245	Annual Rollover Maximum	9	2016-01-21 16:46:03.416207	2016-01-21 16:46:03.416207		annual-rollover-maximum	\N	\N
2874	TemplateField	245	OON Reimbursement Level	10	2016-01-21 16:46:03.425818	2016-01-21 16:46:03.425818		oon-reimbursement-level	\N	t
2875	TemplateField	245	International Employees Covered	11	2016-01-21 16:46:03.436063	2016-01-21 16:46:03.436063		international-employees-covered	\N	\N
2876	TemplateField	245	Waiting Period for Services	12	2016-01-21 16:46:03.445549	2016-01-21 16:46:03.445549		waiting-period-for-services	\N	\N
2877	TemplateField	245	Plan Type	1	2016-01-21 16:46:03.455441	2016-01-21 16:46:03.455441		plan-type	t	t
2878	TemplateField	245	Deductible Waived for Preventive Care	5	2016-01-21 16:46:03.465384	2016-01-21 16:46:03.465384		deductible-waived-for-preventive-care	t	t
2792	TemplateField	237	Sealants	3	2016-01-21 16:46:01.557543	2016-01-21 16:47:26.204583		sealants	\N	t
2798	TemplateField	237	Prosthodontics (including Dentures)	17	2016-01-21 16:46:01.623435	2016-01-21 16:47:26.422584		prosthodontics-including-dentures	\N	t
2880	TemplateColumn	246	Subject to Deductible & Coinsurance	1	2016-01-22 08:17:56.02779	2016-01-22 08:17:56.02779		subject-to-deductible-coinsurance	\N	\N
2881	TemplateColumn	246	Copay	2	2016-01-22 08:18:13.825748	2016-01-22 08:18:13.825748		copay	\N	\N
2882	TemplateColumn	246	Limit	3	2016-01-22 08:18:58.579978	2016-01-22 08:18:58.579978		limit	\N	\N
2883	TemplateColumn	246	Copay Limit	4	2016-01-22 08:19:15.383504	2016-01-22 08:19:15.383504		copay-limit	\N	\N
2884	TemplateColumn	246	Separate Deductible	5	2016-01-22 08:19:33.198668	2016-01-22 08:19:33.198668		separate-deductible	\N	\N
2885	TemplateColumn	246	Separate Coinsurance	6	2016-01-22 08:19:54.325956	2016-01-22 08:19:54.325956		separate-coinsurance	\N	\N
2892	TemplateColumn	251	Entries	1	2016-01-22 09:35:04.896161	2016-01-22 09:35:04.896161		entries	\N	\N
2893	TemplateField	251	Integrated Wellness Plan	1	2016-01-22 09:35:23.143674	2016-01-22 09:35:23.143674		integrated-wellness-plan	\N	\N
2894	TemplateField	250	Disease Management	1	2016-01-22 09:36:23.252373	2016-01-22 09:36:23.252373		disease-management	\N	\N
2895	TemplateColumn	250	Identification Methods	1	2016-01-22 09:36:43.744726	2016-01-22 09:36:43.744726		identification-methods	\N	\N
2896	TemplateColumn	250	Number of Conditions Tracked	2	2016-01-22 09:37:31.643996	2016-01-22 09:37:31.643996		number-of-conditions-tracked	\N	\N
2897	TemplateColumn	250	Outreach Methods	3	2016-01-22 09:38:18.609512	2016-01-22 09:38:18.609512		outreach-methods	\N	\N
2898	TemplateColumn	250	Report Frequency	4	2016-01-22 09:48:25.843438	2016-01-22 09:48:25.843438		report-frequency	\N	\N
2901	TemplateColumn	248	Deductible	1	2016-01-22 09:51:35.325652	2016-01-22 09:51:35.325652		deductible	\N	\N
2902	TemplateColumn	248	Per Member	2	2016-01-22 09:51:50.316854	2016-01-22 09:51:50.316854		per-member	\N	\N
2903	TemplateColumn	247	Entries	1	2016-01-22 09:54:09.423225	2016-01-22 09:54:09.423225		entries	\N	\N
2905	TemplateField	247	Subject to Medical Deductible & Coinsurance	2	2016-01-22 09:55:04.96624	2016-01-22 09:55:04.96624		subject-to-medical-deductible-coinsurance	\N	\N
2907	TemplateField	247	Mandatory Generic	4	2016-01-22 09:55:50.798193	2016-01-22 09:55:50.798193		mandatory-generic	\N	\N
2908	TemplateField	247	Step Therapy/Precertification Applies	5	2016-01-22 09:56:09.13385	2016-01-22 09:56:09.13385		step-therapy-precertification-applies	\N	\N
2909	TemplateField	247	Oral Contraceptive	6	2016-01-22 09:56:28.073696	2016-01-22 09:56:28.073696		oral-contraceptive	\N	\N
2910	TemplateField	247	Are Prescriptions Covered Out of Network	7	2016-01-22 09:56:47.248469	2016-01-22 09:56:47.248469		are-prescriptions-covered-out-of-network	\N	\N
2889	TemplateField	246	General Radiology	22	2016-01-22 08:21:55.60229	2016-01-23 15:25:21.327375		general-radiology	\N	\N
2904	TemplateField	247	Subject to Medical Deductible	1	2016-01-22 09:54:44.700441	2016-01-22 14:22:52.238115		subject-to-medical-deductible	\N	t
2906	TemplateField	247	Network	3	2016-01-22 09:55:21.629017	2016-01-22 14:22:57.102846		network	\N	t
2879	TemplateColumn	245	Entries	1	2016-01-21 16:46:03.477817	2016-01-23 15:09:46.48682		entries	\N	\N
2899	TemplateField	248	Rx Deductible - Individual	1	2016-01-22 09:49:31.983327	2016-01-22 14:23:10.232864		rx-deductible-individual	t	t
2900	TemplateField	248	Rx Deductible - Family	2	2016-01-22 09:49:47.510227	2016-01-22 14:23:10.474513		rx-deductible-family	t	t
2888	TemplateField	246	Lab	15	2016-01-22 08:21:40.857602	2016-01-23 15:25:21.27202		lab	\N	t
2890	TemplateField	246	CT/MRI/PET Scans	29	2016-01-22 08:24:07.015328	2016-01-23 15:25:21.358801		ct-mri-pet-scans	\N	\N
2891	TemplateField	246	Annual Physical Exam	36	2016-01-22 08:24:18.377682	2016-01-23 15:25:21.390408		annual-physical-exam	\N	\N
2911	TemplateField	246	Well Child Exams	43	2016-01-22 10:09:27.829365	2016-01-23 15:25:21.421855		well-child-exams	\N	\N
2947	TemplateField	249	Retail Rx (Tier 4)	4	2016-01-22 10:28:03.126754	2016-01-22 10:28:03.126754		retail-rx-tier-4	\N	\N
2948	TemplateField	249	Retail Rx (Tier 5)	5	2016-01-22 10:28:16.821767	2016-01-22 10:28:16.821767		retail-rx-tier-5	\N	\N
2952	TemplateField	249	Mail Order Rx (Tier 4)	9	2016-01-22 10:29:22.286545	2016-01-22 10:29:22.286545		mail-order-rx-tier-4	\N	\N
2953	TemplateField	249	Mail Order Rx (Tier 5)	10	2016-01-22 10:29:36.745465	2016-01-22 10:29:36.745465		mail-order-rx-tier-5	\N	\N
2954	TemplateField	249	Specialty Medications -Mail Order	11	2016-01-22 10:29:52.015679	2016-01-22 10:29:52.015679		specialty-medications-mail-order	\N	\N
2955	TemplateField	249	Specialty Medications  -Retail	12	2016-01-22 10:30:17.649398	2016-01-22 10:30:17.649398		specialty-medications-retail	\N	\N
2956	TemplateColumn	249	Subject to Rx Deductible	1	2016-01-22 10:30:56.188665	2016-01-22 10:30:56.188665		subject-to-rx-deductible	\N	\N
2957	TemplateColumn	249	Copay	2	2016-01-22 10:39:33.503568	2016-01-22 10:39:33.503568		copay	\N	\N
2958	TemplateColumn	249	Coinsurance	3	2016-01-22 11:34:28.978931	2016-01-22 11:34:28.978931		coinsurance	\N	\N
2959	TemplateColumn	249	Per Script Coinsurance Limit	4	2016-01-22 11:37:47.917295	2016-01-22 11:37:47.917295		per-script-coinsurance-limit	\N	\N
2960	TemplateColumn	249	Per Script Coinsurance Minimum	5	2016-01-22 11:43:54.407616	2016-01-22 11:43:54.407616		per-script-coinsurance-minimum	\N	\N
2961	TemplateField	254	Employee Eligibility Class #1	1	2016-01-22 13:25:29.300351	2016-01-22 13:25:29.300351		employee-eligibility-class-1	\N	\N
2962	TemplateField	254	Employee Eligibility Class #2	2	2016-01-22 13:25:29.317503	2016-01-22 13:25:29.317503		employee-eligibility-class-2	\N	\N
2963	TemplateField	254	Employee Eligibility Class #3	3	2016-01-22 13:25:29.347027	2016-01-22 13:25:29.347027		employee-eligibility-class-3	\N	\N
2964	TemplateField	254	Employee Eligibility Class #4	4	2016-01-22 13:25:29.359665	2016-01-22 13:25:29.359665		employee-eligibility-class-4	\N	\N
2965	TemplateColumn	254	Employee Class	1	2016-01-22 13:25:29.376812	2016-01-22 13:25:29.376812		employee-class	\N	\N
2966	TemplateColumn	254	Employee Classes	2	2016-01-22 13:25:29.391879	2016-01-22 13:25:29.391879		employee-classes	\N	\N
2967	TemplateColumn	254	Waiting Period	3	2016-01-22 13:25:29.403044	2016-01-22 13:25:29.403044		waiting-period	\N	\N
2968	TemplateColumn	254	Hours Requirement	4	2016-01-22 13:25:29.414462	2016-01-22 13:25:29.414462		hours-requirement	\N	\N
2915	TemplateField	246	Surgery/Anesthesiology	71	2016-01-22 10:10:54.963572	2016-01-23 15:25:21.557963		surgery-anesthesiology	\N	\N
2921	TemplateField	246	Substance Abuse - Outpatient Coverage	113	2016-01-22 10:16:26.004338	2016-01-23 15:25:21.783188		substance-abuse-outpatient-coverage	\N	t
2918	TemplateField	246	Mental Nervous - Inpatient Coverage	92	2016-01-22 10:12:19.566362	2016-01-23 15:25:21.661999		mental-nervous-inpatient-coverage	\N	t
2919	TemplateField	246	Mental Nervous - Outpatient Coverage	99	2016-01-22 10:13:40.641352	2016-01-23 15:25:21.713604		mental-nervous-outpatient-coverage	\N	t
2920	TemplateField	246	Substance Abuse - Inpatient Coverage	106	2016-01-22 10:16:07.322944	2016-01-23 15:25:21.748368		substance-abuse-inpatient-coverage	\N	t
2917	TemplateField	246	Urgent Care	85	2016-01-22 10:11:49.457747	2016-01-23 15:25:21.625359		urgent-care	\N	t
2922	TemplateField	246	Physical Therapy - Inpatient Coverage	120	2016-01-22 10:17:06.133596	2016-01-23 15:25:21.818852		physical-therapy-inpatient-coverage	\N	\N
2931	TemplateField	246	Ambulance	183	2016-01-22 10:21:52.338246	2016-01-23 15:25:22.140631		ambulance	\N	\N
2936	TemplateField	246	Prosthetics	218	2016-01-22 10:24:31.365546	2016-01-23 15:25:22.325667		prosthetics	\N	\N
2938	TemplateField	246	Hearing Devices	232	2016-01-22 10:25:11.719835	2016-01-23 15:25:22.39808		hearing-devices	\N	\N
2949	TemplateField	249	Mail Order Rx (Tier 1)	6	2016-01-22 10:28:47.215938	2016-01-22 14:23:48.092177		mail-order-rx-tier-1	\N	t
2950	TemplateField	249	Mail Order Rx (Tier 2)	7	2016-01-22 10:29:02.426481	2016-01-22 14:23:48.292631		mail-order-rx-tier-2	\N	t
2944	TemplateField	249	Retail Rx (Tier 1)	1	2016-01-22 10:27:31.372469	2016-01-22 14:23:30.85355		retail-rx-tier-1	t	t
2946	TemplateField	249	Retail Rx (Tier 3)	3	2016-01-22 10:27:52.926321	2016-01-22 14:23:30.650229		retail-rx-tier-3	t	t
2945	TemplateField	249	Retail Rx (Tier 2)	2	2016-01-22 10:27:43.801877	2016-01-22 14:23:30.855809		retail-rx-tier-2	t	t
2951	TemplateField	249	Mail Order Rx (Tier 3)	8	2016-01-22 10:29:12.026747	2016-01-22 14:23:48.293489		mail-order-rx-tier-3	\N	t
2912	TemplateField	246	Pediatric Dental	50	2016-01-22 10:09:50.928502	2016-01-23 15:25:21.455492		pediatric-dental	\N	\N
2914	TemplateField	246	Hospitalization - Outpatient	64	2016-01-22 10:10:30.10413	2016-01-23 15:25:21.523524		hospitalization-outpatient	\N	t
2916	TemplateField	246	Emergency Room	78	2016-01-22 10:11:16.872056	2016-01-23 15:25:21.591797		emergency-room	t	t
2924	TemplateField	246	Occupational Therapy - Inpatient Coverage	134	2016-01-22 10:18:25.927012	2016-01-23 15:25:21.888094		occupational-therapy-inpatient-coverage	\N	\N
2925	TemplateField	246	Occupational Therapy - Outpatient Coverage	141	2016-01-22 10:18:48.816481	2016-01-23 15:25:21.924031		occupational-therapy-outpatient-coverage	\N	\N
2926	TemplateField	246	Speech Therapy - Inpatient Coverage	148	2016-01-22 10:19:09.191079	2016-01-23 15:25:21.959592		speech-therapy-inpatient-coverage	\N	\N
2927	TemplateField	246	Speech Therapy - Outpatient Coverage	155	2016-01-22 10:19:34.390285	2016-01-23 15:25:21.994512		speech-therapy-outpatient-coverage	\N	\N
2928	TemplateField	246	Pregnancy & Maternity Care - Office Visits	162	2016-01-22 10:20:00.730445	2016-01-23 15:25:22.029508		pregnancy-maternity-care-office-visits	\N	\N
2929	TemplateField	246	Pregnancy & Maternity Care - Labor & Delivery	169	2016-01-22 10:20:21.499735	2016-01-23 15:25:22.065138		pregnancy-maternity-care-labor-delivery	\N	\N
2930	TemplateField	246	Chiropractic Services	176	2016-01-22 10:20:50.619483	2016-01-23 15:25:22.101824		chiropractic-services	\N	t
2932	TemplateField	246	Hospice	190	2016-01-22 10:22:07.647152	2016-01-23 15:25:22.178402		hospice	\N	\N
2933	TemplateField	246	Home Healthcare	197	2016-01-22 10:22:37.30942	2016-01-23 15:25:22.215322		home-healthcare	\N	\N
2934	TemplateField	246	Skilled Nursing	204	2016-01-22 10:23:01.693254	2016-01-23 15:25:22.251967		skilled-nursing	\N	\N
2935	TemplateField	246	Infertility Coverage	211	2016-01-22 10:23:58.020922	2016-01-23 15:25:22.288784		infertility-coverage	\N	t
2937	TemplateField	246	Durable Medical Equipment	225	2016-01-22 10:24:51.235947	2016-01-23 15:25:22.362682		durable-medical-equipment	\N	t
2939	TemplateField	246	Vision Exams	239	2016-01-22 10:25:33.544117	2016-01-23 15:25:22.434638		vision-exams	\N	\N
2940	TemplateField	246	Short Term Rehabilitation - Inpatient	246	2016-01-22 10:25:54.519259	2016-01-23 15:25:22.472677		short-term-rehabilitation-inpatient	\N	\N
2941	TemplateField	246	Short Term Rehabilitation - Outpatient	253	2016-01-22 10:26:14.809266	2016-01-23 15:25:22.510432		short-term-rehabilitation-outpatient	\N	\N
2943	TemplateField	246	Telemedicine	260	2016-01-22 10:26:57.478153	2016-01-23 15:25:22.553103		telemedicine	\N	\N
2942	TemplateField	246	Specialty Rx Coverage	267	2016-01-22 10:26:39.0338	2016-01-23 15:25:22.593619		specialty-rx-coverage	\N	\N
2969	TemplateColumn	254	Spouse Eligible	5	2016-01-22 13:25:29.426538	2016-01-22 13:25:29.426538		spouse-eligible	\N	\N
2970	TemplateColumn	254	Dependent Children Eligible	6	2016-01-22 13:25:29.437724	2016-01-22 13:25:29.437724		dependent-children-eligible	\N	\N
2971	TemplateColumn	254	Domestic Partner Eligible	7	2016-01-22 13:25:29.448807	2016-01-22 13:25:29.448807		domestic-partner-eligible	\N	\N
2972	TemplateColumn	254	Dependent Child Limiting Age	8	2016-01-22 13:25:29.46025	2016-01-22 13:25:29.46025		dependent-child-limiting-age	\N	\N
2973	TemplateColumn	254	Dependent Child Coverage Ends	9	2016-01-22 13:25:29.4716	2016-01-22 13:25:29.4716		dependent-child-coverage-ends	\N	\N
2974	TemplateField	255	Exams	1	2016-01-22 13:25:29.956806	2016-01-22 13:25:29.956806		exams	t	t
2975	TemplateField	255	Frames	2	2016-01-22 13:25:29.96857	2016-01-22 13:25:29.96857		frames	t	t
2976	TemplateField	255	Lenses - Single Vision	3	2016-01-22 13:25:29.980823	2016-01-22 13:25:29.980823		lenses-single-vision	t	t
2977	TemplateField	255	Lenses - Lined Bifocal	4	2016-01-22 13:25:29.992422	2016-01-22 13:25:29.992422		lenses-lined-bifocal	\N	\N
2978	TemplateField	255	Lenses - Lined Trifocal	5	2016-01-22 13:25:30.006475	2016-01-22 13:25:30.006475		lenses-lined-trifocal	\N	\N
2979	TemplateField	255	Lenses - Lenticular	6	2016-01-22 13:25:30.02178	2016-01-22 13:25:30.02178		lenses-lenticular	\N	\N
2980	TemplateField	255	Contacts	7	2016-01-22 13:25:30.038572	2016-01-22 13:25:30.038572		contacts	t	t
2981	TemplateColumn	255	Limit	1	2016-01-22 13:25:30.050984	2016-01-22 13:25:30.050984		limit	\N	\N
2982	TemplateColumn	255	Coinsurance	2	2016-01-22 13:25:30.061137	2016-01-22 13:25:30.061137		coinsurance	\N	\N
2983	TemplateColumn	255	copay	3	2016-01-22 13:25:30.07094	2016-01-22 13:25:30.07094		copay	\N	\N
2984	TemplateField	256	Retail Frames	1	2016-01-22 13:25:30.327434	2016-01-22 13:25:30.327434		retail-frames	t	t
2985	TemplateField	256	Contacts - Non Selection	3	2016-01-22 13:25:30.337158	2016-01-22 13:25:30.337158		contacts-non-selection	\N	\N
2986	TemplateField	256	Contacts - Elective Disposable	4	2016-01-22 13:25:30.346842	2016-01-22 13:25:30.346842		contacts-elective-disposable	\N	\N
2987	TemplateField	256	Contacts -Necessary Lenses	5	2016-01-22 13:25:30.356627	2016-01-22 13:25:30.356627		contacts-necessary-lenses	\N	\N
2988	TemplateField	256	Premium Progressive Lenses	6	2016-01-22 13:25:30.366037	2016-01-22 13:25:30.366037		premium-progressive-lenses	\N	\N
2989	TemplateField	256	Contacts - Covered Selection	2	2016-01-22 13:25:30.375498	2016-01-22 13:25:30.375498		contacts-covered-selection	t	t
2990	TemplateColumn	256	Discount (Overage or General)	1	2016-01-22 13:25:30.388577	2016-01-22 13:25:30.388577		discount-overage-or-general	\N	\N
2991	TemplateColumn	256	Allowance	2	2016-01-22 13:25:30.397747	2016-01-22 13:25:30.397747		allowance	\N	\N
2992	TemplateField	257	Materials Coverage	2	2016-01-22 13:25:30.561452	2016-01-22 13:25:30.561452		materials-coverage	t	t
2993	TemplateField	257	International Employees Covered	3	2016-01-22 13:25:30.570839	2016-01-22 13:25:30.570839		international-employees-covered	\N	\N
2994	TemplateField	257	Plan Type	1	2016-01-22 13:25:30.580903	2016-01-22 13:25:30.580903		plan-type	t	t
2995	TemplateColumn	257	Entries	1	2016-01-22 13:25:30.592644	2016-01-22 13:25:30.592644		entries	\N	\N
2996	TemplateField	258	Plan Type	1	2016-01-22 13:25:30.650877	2016-01-22 13:25:30.650877		plan-type	t	t
2997	TemplateField	258	Materials Coverage	2	2016-01-22 13:25:30.660545	2016-01-22 13:25:30.660545		materials-coverage	t	t
2998	TemplateField	258	International Employees Covered	3	2016-01-22 13:25:30.670193	2016-01-22 13:25:30.670193		international-employees-covered	\N	\N
2999	TemplateColumn	258	Entries	1	2016-01-22 13:25:30.682327	2016-01-22 13:25:30.682327		entries	\N	\N
3000	TemplateField	259	Exams	1	2016-01-22 13:25:30.73455	2016-01-22 13:25:30.73455		exams	t	t
3001	TemplateField	259	Frames	2	2016-01-22 13:25:30.744269	2016-01-22 13:25:30.744269		frames	t	t
3002	TemplateField	259	Lenses - Single Vision	3	2016-01-22 13:25:30.755071	2016-01-22 13:25:30.755071		lenses-single-vision	t	t
3003	TemplateField	259	Lenses - Lined Bifocal	4	2016-01-22 13:25:30.764785	2016-01-22 13:25:30.764785		lenses-lined-bifocal	\N	\N
3004	TemplateField	259	Lenses - Lined Trifocal	5	2016-01-22 13:25:30.774396	2016-01-22 13:25:30.774396		lenses-lined-trifocal	\N	\N
3005	TemplateField	259	Lenses - Lenticular	6	2016-01-22 13:25:30.784203	2016-01-22 13:25:30.784203		lenses-lenticular	\N	\N
3006	TemplateField	259	Contacts	7	2016-01-22 13:25:30.794102	2016-01-22 13:25:30.794102		contacts	t	t
3007	TemplateColumn	259	Limit	1	2016-01-22 13:25:30.806556	2016-01-22 13:25:30.806556		limit	\N	\N
3008	TemplateColumn	259	Coinsurance	2	2016-01-22 13:25:30.817263	2016-01-22 13:25:30.817263		coinsurance	\N	\N
3009	TemplateField	260	Retail Frames	1	2016-01-22 13:25:30.996744	2016-01-22 13:25:30.996744		retail-frames	t	t
3010	TemplateField	260	Contacts - Covered Selection	2	2016-01-22 13:25:31.006787	2016-01-22 13:25:31.006787		contacts-covered-selection	t	t
3011	TemplateField	260	Contacts - Non Selection	3	2016-01-22 13:25:31.017013	2016-01-22 13:25:31.017013		contacts-non-selection	\N	\N
3012	TemplateField	260	Contacts - Elective Disposable	4	2016-01-22 13:25:31.026712	2016-01-22 13:25:31.026712		contacts-elective-disposable	\N	\N
3013	TemplateField	260	Contacts -Necessary Lenses	5	2016-01-22 13:25:31.0362	2016-01-22 13:25:31.0362		contacts-necessary-lenses	\N	\N
3014	TemplateField	260	Premium Progressive Lenses	6	2016-01-22 13:25:31.045428	2016-01-22 13:25:31.045428		premium-progressive-lenses	\N	\N
3015	TemplateColumn	260	Allowance	1	2016-01-22 13:25:31.056864	2016-01-22 13:25:31.056864		allowance	\N	\N
3016	TemplateField	261	Employee Eligibility Class #1	1	2016-01-22 13:33:29.928267	2016-01-22 13:33:29.928267		employee-eligibility-class-1	\N	\N
3017	TemplateField	261	Employee Eligibility Class #2	2	2016-01-22 13:33:29.938471	2016-01-22 13:33:29.938471		employee-eligibility-class-2	\N	\N
3018	TemplateField	261	Employee Eligibility Class #3	3	2016-01-22 13:33:29.947677	2016-01-22 13:33:29.947677		employee-eligibility-class-3	\N	\N
3019	TemplateField	261	Employee Eligibility Class #4	4	2016-01-22 13:33:29.957238	2016-01-22 13:33:29.957238		employee-eligibility-class-4	\N	\N
3020	TemplateColumn	261	Employee Class	1	2016-01-22 13:33:29.968748	2016-01-22 13:33:29.968748		employee-class	\N	\N
3021	TemplateColumn	261	Employee Classes	2	2016-01-22 13:33:29.977933	2016-01-22 13:33:29.977933		employee-classes	\N	\N
3022	TemplateColumn	261	Waiting Period	3	2016-01-22 13:33:29.987529	2016-01-22 13:33:29.987529		waiting-period	\N	\N
3023	TemplateColumn	261	Hours Requirement	4	2016-01-22 13:33:29.996535	2016-01-22 13:33:29.996535		hours-requirement	\N	\N
3024	TemplateColumn	261	Spouse Eligible	5	2016-01-22 13:33:30.008048	2016-01-22 13:33:30.008048		spouse-eligible	\N	\N
3025	TemplateColumn	261	Dependent Children Eligible	6	2016-01-22 13:33:30.018819	2016-01-22 13:33:30.018819		dependent-children-eligible	\N	\N
3026	TemplateColumn	261	Domestic Partner Eligible	7	2016-01-22 13:33:30.027852	2016-01-22 13:33:30.027852		domestic-partner-eligible	\N	\N
3027	TemplateColumn	261	Dependent Child Limiting Age	8	2016-01-22 13:33:30.036973	2016-01-22 13:33:30.036973		dependent-child-limiting-age	\N	\N
3028	TemplateColumn	261	Dependent Child Coverage Ends	9	2016-01-22 13:33:30.046064	2016-01-22 13:33:30.046064		dependent-child-coverage-ends	\N	\N
3029	TemplateField	262	Exams	1	2016-01-22 13:33:30.433751	2016-01-22 13:33:30.433751		exams	t	t
3030	TemplateField	262	Frames	2	2016-01-22 13:33:30.443548	2016-01-22 13:33:30.443548		frames	t	t
3031	TemplateField	262	Lenses - Single Vision	3	2016-01-22 13:33:30.452752	2016-01-22 13:33:30.452752		lenses-single-vision	t	t
3032	TemplateField	262	Lenses - Lined Bifocal	4	2016-01-22 13:33:30.462494	2016-01-22 13:33:30.462494		lenses-lined-bifocal	\N	\N
3033	TemplateField	262	Lenses - Lined Trifocal	5	2016-01-22 13:33:30.471738	2016-01-22 13:33:30.471738		lenses-lined-trifocal	\N	\N
3034	TemplateField	262	Lenses - Lenticular	6	2016-01-22 13:33:30.481177	2016-01-22 13:33:30.481177		lenses-lenticular	\N	\N
3035	TemplateField	262	Contacts	7	2016-01-22 13:33:30.490203	2016-01-22 13:33:30.490203		contacts	t	t
3036	TemplateColumn	262	Limit	1	2016-01-22 13:33:30.501785	2016-01-22 13:33:30.501785		limit	\N	\N
3037	TemplateColumn	262	Coinsurance	2	2016-01-22 13:33:30.511237	2016-01-22 13:33:30.511237		coinsurance	\N	\N
3038	TemplateColumn	262	copay	3	2016-01-22 13:33:30.520439	2016-01-22 13:33:30.520439		copay	\N	\N
3039	TemplateField	263	Retail Frames	1	2016-01-22 13:33:30.760621	2016-01-22 13:33:30.760621		retail-frames	t	t
3040	TemplateField	263	Contacts - Non Selection	3	2016-01-22 13:33:30.770574	2016-01-22 13:33:30.770574		contacts-non-selection	\N	\N
3041	TemplateField	263	Contacts - Elective Disposable	4	2016-01-22 13:33:30.780153	2016-01-22 13:33:30.780153		contacts-elective-disposable	\N	\N
3042	TemplateField	263	Contacts -Necessary Lenses	5	2016-01-22 13:33:30.790308	2016-01-22 13:33:30.790308		contacts-necessary-lenses	\N	\N
3043	TemplateField	263	Premium Progressive Lenses	6	2016-01-22 13:33:30.799547	2016-01-22 13:33:30.799547		premium-progressive-lenses	\N	\N
3044	TemplateField	263	Contacts - Covered Selection	2	2016-01-22 13:33:30.809032	2016-01-22 13:33:30.809032		contacts-covered-selection	t	t
3046	TemplateColumn	263	Allowance	2	2016-01-22 13:33:30.830199	2016-01-22 13:33:30.830199		allowance	\N	\N
3047	TemplateField	264	Materials Coverage	2	2016-01-22 13:33:30.999708	2016-01-22 13:33:30.999708		materials-coverage	t	t
3048	TemplateField	264	International Employees Covered	3	2016-01-22 13:33:31.013961	2016-01-22 13:33:31.013961		international-employees-covered	\N	\N
3049	TemplateField	264	Plan Type	1	2016-01-22 13:33:31.02374	2016-01-22 13:33:31.02374		plan-type	t	t
3050	TemplateColumn	264	Entries	1	2016-01-22 13:33:31.036254	2016-01-22 13:33:31.036254		entries	\N	\N
3051	TemplateField	265	Exams	1	2016-01-22 13:33:31.092241	2016-01-22 13:33:31.092241		exams	t	t
3052	TemplateField	265	Frames	2	2016-01-22 13:33:31.102034	2016-01-22 13:33:31.102034		frames	t	t
3053	TemplateField	265	Lenses - Single Vision	3	2016-01-22 13:33:31.111732	2016-01-22 13:33:31.111732		lenses-single-vision	t	t
3054	TemplateField	265	Lenses - Lined Bifocal	4	2016-01-22 13:33:31.121686	2016-01-22 13:33:31.121686		lenses-lined-bifocal	\N	\N
3055	TemplateField	265	Lenses - Lined Trifocal	5	2016-01-22 13:33:31.131576	2016-01-22 13:33:31.131576		lenses-lined-trifocal	\N	\N
3056	TemplateField	265	Lenses - Lenticular	6	2016-01-22 13:33:31.141018	2016-01-22 13:33:31.141018		lenses-lenticular	\N	\N
3057	TemplateField	265	Contacts	7	2016-01-22 13:33:31.150739	2016-01-22 13:33:31.150739		contacts	t	t
3058	TemplateColumn	265	Limit	1	2016-01-22 13:33:31.162307	2016-01-22 13:33:31.162307		limit	\N	\N
3059	TemplateColumn	265	Coinsurance	2	2016-01-22 13:33:31.172966	2016-01-22 13:33:31.172966		coinsurance	\N	\N
3060	TemplateField	266	Retail Frames	1	2016-01-22 13:33:31.359222	2016-01-22 13:33:31.359222		retail-frames	t	t
3061	TemplateField	266	Contacts - Covered Selection	2	2016-01-22 13:33:31.369956	2016-01-22 13:33:31.369956		contacts-covered-selection	t	t
3062	TemplateField	266	Contacts - Non Selection	3	2016-01-22 13:33:31.379915	2016-01-22 13:33:31.379915		contacts-non-selection	\N	\N
3063	TemplateField	266	Contacts - Elective Disposable	4	2016-01-22 13:33:31.389519	2016-01-22 13:33:31.389519		contacts-elective-disposable	\N	\N
3064	TemplateField	266	Contacts -Necessary Lenses	5	2016-01-22 13:33:31.398748	2016-01-22 13:33:31.398748		contacts-necessary-lenses	\N	\N
3065	TemplateField	266	Premium Progressive Lenses	6	2016-01-22 13:33:31.409931	2016-01-22 13:33:31.409931		premium-progressive-lenses	\N	\N
3066	TemplateColumn	266	Allowance	1	2016-01-22 13:33:31.42138	2016-01-22 13:33:31.42138		allowance	\N	\N
3067	TemplateField	267	Plan Type	1	2016-01-22 13:33:31.522941	2016-01-22 13:33:31.522941		plan-type	t	t
3068	TemplateField	267	Materials Coverage	2	2016-01-22 13:33:31.532258	2016-01-22 13:33:31.532258		materials-coverage	t	t
3069	TemplateField	267	International Employees Covered	3	2016-01-22 13:33:31.541528	2016-01-22 13:33:31.541528		international-employees-covered	\N	\N
3070	TemplateColumn	267	Entries	1	2016-01-22 13:33:31.552553	2016-01-22 13:33:31.552553		entries	\N	\N
3045	TemplateColumn	263	Discount	1	2016-01-22 13:33:30.82072	2016-01-22 14:01:40.164214		discount	\N	\N
2600	TemplateField	219	Referrals Needed	3	2016-01-21 13:31:33.551447	2016-01-22 14:19:18.28256		referrals-needed	t	t
2607	TemplateField	219	Out-of-Pocket Max Family (includes deductible)	10	2016-01-21 13:31:33.623394	2016-01-22 14:20:46.183065		out-of-pocket-max-family-includes-deductible	t	t
2886	TemplateField	246	Primary Care Office Visit	1	2016-01-22 08:21:17.221264	2016-01-22 14:21:27.788434		primary-care-office-visit	t	t
2913	TemplateField	246	Hospitalization - Inpatient	57	2016-01-22 10:10:12.020236	2016-01-23 15:25:21.489984		hospitalization-inpatient	t	t
2923	TemplateField	246	Physical Therapy - Outpatient Coverage	127	2016-01-22 10:17:59.982416	2016-01-23 15:25:21.853274		physical-therapy-outpatient-coverage	\N	t
3071	TemplateField	268	Accelerated Death Benefit	1	2016-01-23 14:54:16.48376	2016-01-23 14:54:16.48376		accelerated-death-benefit	\N	\N
3072	TemplateColumn	268	Benefit Paid	1	2016-01-23 14:54:16.501515	2016-01-23 14:54:16.501515		benefit-paid	\N	\N
3073	TemplateColumn	268	Maximum	2	2016-01-23 14:54:16.51472	2016-01-23 14:54:16.51472		maximum	\N	\N
3074	TemplateColumn	268	Flat Amount	3	2016-01-23 14:54:16.525137	2016-01-23 14:54:16.525137		flat-amount	\N	\N
3075	TemplateField	269	Definition of Earnings	1	2016-01-23 14:54:16.58329	2016-01-23 14:54:16.58329		definition-of-earnings	t	t
3076	TemplateColumn	269	Earnings Determination	1	2016-01-23 14:54:16.596852	2016-01-23 14:54:16.596852		earnings-determination	\N	\N
3077	TemplateColumn	269	Plus Commission	2	2016-01-23 14:54:16.60759	2016-01-23 14:54:16.60759		plus-commission	\N	\N
3078	TemplateColumn	269	Plus Bonus	3	2016-01-23 14:54:16.61825	2016-01-23 14:54:16.61825		plus-bonus	\N	\N
3079	TemplateColumn	269	Plus Overtime	4	2016-01-23 14:54:16.629237	2016-01-23 14:54:16.629237		plus-overtime	\N	\N
3080	TemplateColumn	269	Value of	5	2016-01-23 14:54:16.640427	2016-01-23 14:54:16.640427		value-of	\N	\N
3081	TemplateColumn	269	Dollars	6	2016-01-23 14:54:16.65312	2016-01-23 14:54:16.65312		dollars	\N	\N
2887	TemplateField	246	Specialist Office Visit	8	2016-01-22 08:21:33.195296	2016-01-23 15:25:21.21434		specialist-office-visit	t	t
3082	TemplateField	270	Employee Eligibility Class #1	1	2016-01-23 14:54:16.744992	2016-01-23 14:54:16.744992		employee-eligibility-class-1	\N	\N
3083	TemplateField	270	Employee Eligibility Class #2	2	2016-01-23 14:54:16.756596	2016-01-23 14:54:16.756596		employee-eligibility-class-2	\N	\N
3084	TemplateField	270	Employee Eligibility Class #3	3	2016-01-23 14:54:16.785131	2016-01-23 14:54:16.785131		employee-eligibility-class-3	\N	\N
3085	TemplateField	270	Employee Eligibility Class #4	4	2016-01-23 14:54:16.797554	2016-01-23 14:54:16.797554		employee-eligibility-class-4	\N	\N
3086	TemplateColumn	270	Employee Class	1	2016-01-23 14:54:16.81065	2016-01-23 14:54:16.81065		employee-class	\N	\N
3087	TemplateColumn	270	Employee Classes	2	2016-01-23 14:54:16.821987	2016-01-23 14:54:16.821987		employee-classes	\N	\N
3088	TemplateColumn	270	Waiting Period	3	2016-01-23 14:54:16.832435	2016-01-23 14:54:16.832435		waiting-period	\N	\N
3089	TemplateColumn	270	Hours Requirement	4	2016-01-23 14:54:16.84255	2016-01-23 14:54:16.84255		hours-requirement	\N	\N
3090	TemplateField	271	Life Amount	1	2016-01-23 14:54:17.031894	2016-01-23 14:54:17.031894		life-amount	t	t
3091	TemplateColumn	271	X Multiple of Earnings	1	2016-01-23 14:54:17.044534	2016-01-23 14:54:17.044534		x-multiple-of-earnings	\N	\N
3092	TemplateColumn	271	Flat Amount	2	2016-01-23 14:54:17.054585	2016-01-23 14:54:17.054585		flat-amount	\N	\N
3093	TemplateField	272	Guaranteed Issue Multiple of Earnings Limit	3	2016-01-23 14:54:17.10204	2016-01-23 14:54:17.10204		guaranteed-issue-multiple-of-earnings-limit	\N	t
3094	TemplateField	272	Life Maximum Benefit	1	2016-01-23 14:54:17.113465	2016-01-23 14:54:17.113465		life-maximum-benefit	t	t
3095	TemplateField	272	Guaranteed Issue $ Limit	2	2016-01-23 14:54:17.12405	2016-01-23 14:54:17.12405		guaranteed-issue-limit	t	t
3096	TemplateField	272	Benefit Reduced from Original Amount at age 60 by	4	2016-01-23 14:54:17.134523	2016-01-23 14:54:17.134523		benefit-reduced-from-original-amount-at-age-60-by	\N	t
3097	TemplateField	272	Benefit Reduced from Original Amount at age 65 by	5	2016-01-23 14:54:17.144909	2016-01-23 14:54:17.144909		benefit-reduced-from-original-amount-at-age-65-by	\N	t
3098	TemplateField	272	Benefit Reduced from Original Amount at age 70 by	6	2016-01-23 14:54:17.155316	2016-01-23 14:54:17.155316		benefit-reduced-from-original-amount-at-age-70-by	\N	t
3099	TemplateField	272	Benefit Reduced from Original Amount at age 80 by	8	2016-01-23 14:54:17.166246	2016-01-23 14:54:17.166246		benefit-reduced-from-original-amount-at-age-80-by	\N	t
3100	TemplateField	272	Benefit Reduced from Original Amount at age 85 by	9	2016-01-23 14:54:17.176781	2016-01-23 14:54:17.176781		benefit-reduced-from-original-amount-at-age-85-by	\N	t
3101	TemplateField	272	Benefit Reduced from Original Amount at age 90 by	10	2016-01-23 14:54:17.187534	2016-01-23 14:54:17.187534		benefit-reduced-from-original-amount-at-age-90-by	\N	t
3102	TemplateField	272	Benefit Reduced from Original Amount at age 95 by	11	2016-01-23 14:54:17.197574	2016-01-23 14:54:17.197574		benefit-reduced-from-original-amount-at-age-95-by	\N	t
3103	TemplateField	272	Benefit Reduced from Original Amount at age 100 by	12	2016-01-23 14:54:17.207858	2016-01-23 14:54:17.207858		benefit-reduced-from-original-amount-at-age-100-by	\N	t
3104	TemplateField	272	Coverage disabled before age 60	13	2016-01-23 14:54:17.218403	2016-01-23 14:54:17.218403		coverage-disabled-before-age-60	t	t
3105	TemplateField	272	Coverage disabled after age 60	14	2016-01-23 14:54:17.228768	2016-01-23 14:54:17.228768		coverage-disabled-after-age-60	\N	\N
3106	TemplateField	272	Family Medical Leave Continuation	15	2016-01-23 14:54:17.238926	2016-01-23 14:54:17.238926		family-medical-leave-continuation	\N	\N
3107	TemplateField	272	Military Leave Continuation	16	2016-01-23 14:54:17.249085	2016-01-23 14:54:17.249085		military-leave-continuation	\N	\N
3108	TemplateField	272	Military Convalescence Support Continuation	17	2016-01-23 14:54:17.258832	2016-01-23 14:54:17.258832		military-convalescence-support-continuation	\N	\N
3109	TemplateField	272	Benefit Reduced from Original Amount at age 75 by	7	2016-01-23 14:54:17.268642	2016-01-23 14:54:17.268642		benefit-reduced-from-original-amount-at-age-75-by	\N	t
3110	TemplateColumn	272	Entries	1	2016-01-23 14:54:17.28066	2016-01-23 14:54:17.28066		entries	\N	\N
3111	TemplateField	273	Portability	2	2016-01-23 14:54:17.534093	2016-01-23 14:54:17.534093		portability	\N	\N
3112	TemplateField	273	International Employees Covered	3	2016-01-23 14:54:17.544306	2016-01-23 14:54:17.544306		international-employees-covered	\N	\N
3113	TemplateField	273	Suicide Exclusions	4	2016-01-23 14:54:17.554543	2016-01-23 14:54:17.554543		suicide-exclusions	\N	\N
3114	TemplateField	273	Military Service Exclusions	5	2016-01-23 14:54:17.564786	2016-01-23 14:54:17.564786		military-service-exclusions	\N	\N
3115	TemplateField	273	Travel Assistance	6	2016-01-23 14:54:17.575228	2016-01-23 14:54:17.575228		travel-assistance	\N	\N
3116	TemplateField	273	Bereavement Counseling	7	2016-01-23 14:54:17.586456	2016-01-23 14:54:17.586456		bereavement-counseling	\N	\N
3117	TemplateField	273	EAP	8	2016-01-23 14:54:17.596893	2016-01-23 14:54:17.596893		eap	\N	\N
3118	TemplateField	273	Act of  War Exclusion	9	2016-01-23 14:54:17.606796	2016-01-23 14:54:17.606796		act-of-war-exclusion	\N	\N
3119	TemplateField	273	Conversion	1	2016-01-23 14:54:17.616624	2016-01-23 14:54:17.616624		conversion	\N	t
3120	TemplateColumn	273	Entries	1	2016-01-23 14:54:17.629455	2016-01-23 14:54:17.629455		entries	\N	\N
3121	TemplateField	274	Life	1	2016-01-23 14:57:43.423278	2016-01-23 14:57:43.423278		life	\N	\N
3122	TemplateField	274	Both Hands	2	2016-01-23 14:57:43.434679	2016-01-23 14:57:43.434679		both-hands	\N	\N
3123	TemplateField	274	Both Feet	3	2016-01-23 14:57:43.444994	2016-01-23 14:57:43.444994		both-feet	\N	\N
3124	TemplateField	274	Sight of Both Eyes	4	2016-01-23 14:57:43.455605	2016-01-23 14:57:43.455605		sight-of-both-eyes	\N	\N
3125	TemplateField	274	Speech and Hearing	5	2016-01-23 14:57:43.465851	2016-01-23 14:57:43.465851		speech-and-hearing	\N	\N
3126	TemplateField	274	One Hand	6	2016-01-23 14:57:43.476227	2016-01-23 14:57:43.476227		one-hand	\N	\N
3127	TemplateField	274	One Foot	7	2016-01-23 14:57:43.486883	2016-01-23 14:57:43.486883		one-foot	\N	\N
3128	TemplateField	274	Sight of One Eye	8	2016-01-23 14:57:43.499363	2016-01-23 14:57:43.499363		sight-of-one-eye	\N	\N
3129	TemplateField	274	Speech	9	2016-01-23 14:57:43.509891	2016-01-23 14:57:43.509891		speech	\N	\N
3130	TemplateField	274	Hearing	10	2016-01-23 14:57:43.520501	2016-01-23 14:57:43.520501		hearing	\N	\N
3131	TemplateField	274	Quadraplegia	11	2016-01-23 14:57:43.535334	2016-01-23 14:57:43.535334		quadraplegia	\N	\N
3132	TemplateField	274	Paraplegia	12	2016-01-23 14:57:43.545934	2016-01-23 14:57:43.545934		paraplegia	\N	\N
3133	TemplateField	274	Hemiplegia	13	2016-01-23 14:57:43.556376	2016-01-23 14:57:43.556376		hemiplegia	\N	\N
3134	TemplateField	274	Uniplegia	14	2016-01-23 14:57:43.566822	2016-01-23 14:57:43.566822		uniplegia	\N	\N
3135	TemplateColumn	274	Entries	1	2016-01-23 14:57:43.581328	2016-01-23 14:57:43.581328		entries	\N	\N
3136	TemplateField	275	AD&D Amount	1	2016-01-23 14:57:43.754463	2016-01-23 14:57:43.754463		ad-d-amount	t	t
3137	TemplateColumn	275	X Multiple of Earnings	1	2016-01-23 14:57:43.767325	2016-01-23 14:57:43.767325		x-multiple-of-earnings	\N	\N
3138	TemplateColumn	275	Flat Amount	2	2016-01-23 14:57:43.777763	2016-01-23 14:57:43.777763		flat-amount	\N	\N
3139	TemplateField	276	Benefit Reduced from Original Amount at age 65 by	5	2016-01-23 14:57:43.82288	2016-01-23 14:57:43.82288		benefit-reduced-from-original-amount-at-age-65-by	\N	t
3198	TemplateField	282	Drug & Alcohol Limitation	13	2016-01-23 14:59:16.698686	2016-01-23 14:59:16.698686		drug-alcohol-limitation	t	t
3140	TemplateField	276	Benefit Reduced from Original Amount at age 60 by	4	2016-01-23 14:57:43.833675	2016-01-23 14:57:43.833675		benefit-reduced-from-original-amount-at-age-60-by	\N	t
3141	TemplateField	276	Guaranteed Issue $ Limit	2	2016-01-23 14:57:43.844068	2016-01-23 14:57:43.844068		guaranteed-issue-limit	t	t
3142	TemplateField	276	AD&D Maximum Benefit	1	2016-01-23 14:57:43.854521	2016-01-23 14:57:43.854521		ad-d-maximum-benefit	t	t
3143	TemplateField	276	Benefit Reduced from Original Amount at age 70 by	6	2016-01-23 14:57:43.865378	2016-01-23 14:57:43.865378		benefit-reduced-from-original-amount-at-age-70-by	\N	t
3144	TemplateField	276	Benefit Reduced from Original Amount at age 75 by	7	2016-01-23 14:57:43.875347	2016-01-23 14:57:43.875347		benefit-reduced-from-original-amount-at-age-75-by	\N	t
3145	TemplateField	276	Benefit Reduced from Original Amount at age 80 by	8	2016-01-23 14:57:43.8852	2016-01-23 14:57:43.8852		benefit-reduced-from-original-amount-at-age-80-by	\N	t
3146	TemplateField	276	Benefit Reduced from Original Amount at age 85 by	9	2016-01-23 14:57:43.895733	2016-01-23 14:57:43.895733		benefit-reduced-from-original-amount-at-age-85-by	\N	t
3147	TemplateField	276	Benefit Reduced from Original Amount at age 90 by	10	2016-01-23 14:57:43.906274	2016-01-23 14:57:43.906274		benefit-reduced-from-original-amount-at-age-90-by	\N	t
3148	TemplateField	276	Guaranteed Issue Multiple of Earnings Limit	3	2016-01-23 14:57:43.916596	2016-01-23 14:57:43.916596		guaranteed-issue-multiple-of-earnings-limit	\N	t
3149	TemplateField	276	Benefit Reduced from Original Amount at age 95 by	11	2016-01-23 14:57:43.926874	2016-01-23 14:57:43.926874		benefit-reduced-from-original-amount-at-age-95-by	\N	t
3150	TemplateField	276	Benefit Reduced from Original Amount at age 100 by	12	2016-01-23 14:57:43.937235	2016-01-23 14:57:43.937235		benefit-reduced-from-original-amount-at-age-100-by	\N	t
3151	TemplateColumn	276	Entries	1	2016-01-23 14:57:43.95037	2016-01-23 14:57:43.95037		entries	\N	\N
3152	TemplateField	277	Definition of Earnings	1	2016-01-23 14:57:44.118271	2016-01-23 14:57:44.118271		definition-of-earnings	t	t
3153	TemplateColumn	277	Earnings Determination	1	2016-01-23 14:57:44.130958	2016-01-23 14:57:44.130958		earnings-determination	\N	\N
3154	TemplateColumn	277	Plus Commission	2	2016-01-23 14:57:44.142331	2016-01-23 14:57:44.142331		plus-commission	\N	\N
3155	TemplateColumn	277	Plus Bonus	3	2016-01-23 14:57:44.153671	2016-01-23 14:57:44.153671		plus-bonus	\N	\N
3156	TemplateColumn	277	Plus Overtime	4	2016-01-23 14:57:44.163955	2016-01-23 14:57:44.163955		plus-overtime	\N	\N
3157	TemplateColumn	277	Value of	5	2016-01-23 14:57:44.174114	2016-01-23 14:57:44.174114		value-of	\N	\N
3158	TemplateColumn	277	Dollars	6	2016-01-23 14:57:44.184699	2016-01-23 14:57:44.184699		dollars	\N	\N
3221	TemplateField	286	Maximum Weekly Benefit	5	2016-01-23 15:01:20.557222	2016-01-23 15:01:20.557222		maximum-weekly-benefit	t	t
3159	TemplateField	278	Employee Eligibility Class #1	1	2016-01-23 14:57:44.283975	2016-01-23 14:57:44.283975		employee-eligibility-class-1	\N	\N
3160	TemplateField	278	Employee Eligibility Class #2	2	2016-01-23 14:57:44.294241	2016-01-23 14:57:44.294241		employee-eligibility-class-2	\N	\N
3161	TemplateField	278	Employee Eligibility Class #3	3	2016-01-23 14:57:44.304644	2016-01-23 14:57:44.304644		employee-eligibility-class-3	\N	\N
3162	TemplateField	278	Employee Eligibility Class #4	4	2016-01-23 14:57:44.314606	2016-01-23 14:57:44.314606		employee-eligibility-class-4	\N	\N
3163	TemplateColumn	278	Employee Class	1	2016-01-23 14:57:44.326759	2016-01-23 14:57:44.326759		employee-class	\N	\N
3164	TemplateColumn	278	Employee Classes	2	2016-01-23 14:57:44.337096	2016-01-23 14:57:44.337096		employee-classes	\N	\N
3165	TemplateColumn	278	Waiting Period	3	2016-01-23 14:57:44.347104	2016-01-23 14:57:44.347104		waiting-period	\N	\N
3166	TemplateColumn	278	Hours Requirement	4	2016-01-23 14:57:44.35713	2016-01-23 14:57:44.35713		hours-requirement	\N	\N
3167	TemplateField	279	LTD Amount	1	2016-01-23 14:59:16.185896	2016-01-23 14:59:16.185896		ltd-amount	t	t
3168	TemplateColumn	279	Flat Dollar Amount	2	2016-01-23 14:59:16.198505	2016-01-23 14:59:16.198505		flat-dollar-amount	\N	\N
3169	TemplateColumn	279	Percentage of Earnings	1	2016-01-23 14:59:16.208697	2016-01-23 14:59:16.208697		percentage-of-earnings	\N	\N
3170	TemplateField	280	Definition of Earnings	1	2016-01-23 14:59:16.250981	2016-01-23 14:59:16.250981		definition-of-earnings	\N	t
3171	TemplateColumn	280	Earnings Determination	1	2016-01-23 14:59:16.263385	2016-01-23 14:59:16.263385		earnings-determination	\N	\N
3172	TemplateColumn	280	Plus Bonus	3	2016-01-23 14:59:16.304732	2016-01-23 14:59:16.304732		plus-bonus	\N	\N
3173	TemplateColumn	280	Plus Commission	2	2016-01-23 14:59:16.315905	2016-01-23 14:59:16.315905		plus-commission	\N	\N
3174	TemplateColumn	280	PLUS OVERTIME	4	2016-01-23 14:59:16.326018	2016-01-23 14:59:16.326018		plus-overtime	\N	\N
3175	TemplateField	281	Maximum benefit	3	2016-01-23 14:59:16.385749	2016-01-23 14:59:16.385749		maximum-benefit	t	t
3176	TemplateField	281	Minimum Benefit	2	2016-01-23 14:59:16.395984	2016-01-23 14:59:16.395984		minimum-benefit	\N	t
3177	TemplateField	281	Definition of Disability	5	2016-01-23 14:59:16.406373	2016-01-23 14:59:16.406373		definition-of-disability	t	t
3178	TemplateField	281	Guaranteed Issue Limit	4	2016-01-23 14:59:16.416603	2016-01-23 14:59:16.416603		guaranteed-issue-limit	\N	t
3179	TemplateField	281	Elimination Period	1	2016-01-23 14:59:16.426658	2016-01-23 14:59:16.426658		elimination-period	t	t
3180	TemplateColumn	281	Entries	1	2016-01-23 14:59:16.439222	2016-01-23 14:59:16.439222		entries	\N	\N
3181	TemplateField	282	Earnings Test	3	2016-01-23 14:59:16.518394	2016-01-23 14:59:16.518394		earnings-test	\N	\N
3182	TemplateField	282	Partial Disability Provision	6	2016-01-23 14:59:16.528988	2016-01-23 14:59:16.528988		partial-disability-provision	\N	\N
3183	TemplateField	282	Benefit Integration Offsets	7	2016-01-23 14:59:16.539743	2016-01-23 14:59:16.539743		benefit-integration-offsets	\N	\N
3184	TemplateField	282	Recurrent Disability	8	2016-01-23 14:59:16.550584	2016-01-23 14:59:16.550584		recurrent-disability	\N	\N
3185	TemplateField	282	Work Incentive Benefit	9	2016-01-23 14:59:16.560936	2016-01-23 14:59:16.560936		work-incentive-benefit	\N	\N
3186	TemplateField	282	Social Security Integartion	2	2016-01-23 14:59:16.571041	2016-01-23 14:59:16.571041		social-security-integartion	\N	t
3187	TemplateField	282	Zero Day Residual	1	2016-01-23 14:59:16.581178	2016-01-23 14:59:16.581178		zero-day-residual	t	t
3188	TemplateField	282	Maximum Benefit Duration	4	2016-01-23 14:59:16.591679	2016-01-23 14:59:16.591679		maximum-benefit-duration	\N	t
3189	TemplateField	282	Partial Disability Covered	5	2016-01-23 14:59:16.602892	2016-01-23 14:59:16.602892		partial-disability-covered	\N	t
3190	TemplateField	282	Rehabilitation	10	2016-01-23 14:59:16.613774	2016-01-23 14:59:16.613774		rehabilitation	\N	\N
3191	TemplateField	282	Pre-existing Condition Exclusions	11	2016-01-23 14:59:16.62376	2016-01-23 14:59:16.62376		pre-existing-condition-exclusions	\N	\N
3192	TemplateField	282	Self Reported Disability Limitation	14	2016-01-23 14:59:16.637942	2016-01-23 14:59:16.637942		self-reported-disability-limitation	\N	\N
3193	TemplateField	282	W2 Services Provided	15	2016-01-23 14:59:16.648234	2016-01-23 14:59:16.648234		w2-services-provided	\N	\N
3194	TemplateField	282	Employer FICA match	16	2016-01-23 14:59:16.658056	2016-01-23 14:59:16.658056		employer-fica-match	\N	\N
3195	TemplateField	282	International Employees Covered	18	2016-01-23 14:59:16.668004	2016-01-23 14:59:16.668004		international-employees-covered	\N	\N
3196	TemplateField	282	EAP	19	2016-01-23 14:59:16.678457	2016-01-23 14:59:16.678457		eap	\N	\N
3197	TemplateField	282	Mental Nervous Limitation	12	2016-01-23 14:59:16.688531	2016-01-23 14:59:16.688531		mental-nervous-limitation	t	t
3199	TemplateField	282	Gross Up	17	2016-01-23 14:59:16.711799	2016-01-23 14:59:16.711799		gross-up	\N	t
3200	TemplateColumn	282	Entries	1	2016-01-23 14:59:16.725563	2016-01-23 14:59:16.725563		entries	\N	\N
3201	TemplateField	283	Employee Eligibility Class #1	1	2016-01-23 14:59:16.954697	2016-01-23 14:59:16.954697		employee-eligibility-class-1	\N	\N
3202	TemplateField	283	Employee Eligibility Class #2	2	2016-01-23 14:59:16.964817	2016-01-23 14:59:16.964817		employee-eligibility-class-2	\N	\N
3203	TemplateField	283	Employee Eligibility Class #3	2	2016-01-23 14:59:16.974826	2016-01-23 14:59:16.974826		employee-eligibility-class-3	\N	\N
3204	TemplateField	283	Employee Eligibility Class #4	3	2016-01-23 14:59:16.98462	2016-01-23 14:59:16.98462		employee-eligibility-class-4	\N	\N
3205	TemplateColumn	283	Employee Class	1	2016-01-23 14:59:16.996985	2016-01-23 14:59:16.996985		employee-class	\N	\N
3206	TemplateColumn	283	Employee Classes	2	2016-01-23 14:59:17.006987	2016-01-23 14:59:17.006987		employee-classes	\N	\N
3207	TemplateColumn	283	Waiting Period	3	2016-01-23 14:59:17.016737	2016-01-23 14:59:17.016737		waiting-period	\N	\N
3208	TemplateColumn	283	Hours Requirement	4	2016-01-23 14:59:17.026577	2016-01-23 14:59:17.026577		hours-requirement	\N	\N
3209	TemplateField	284	STD Amount	1	2016-01-23 15:01:20.345701	2016-01-23 15:01:20.345701		std-amount	t	t
3210	TemplateColumn	284	Entries	1	2016-01-23 15:01:20.358964	2016-01-23 15:01:20.358964		entries	\N	\N
3211	TemplateField	285	Definition of Earnings	1	2016-01-23 15:01:20.393325	2016-01-23 15:01:20.393325		definition-of-earnings	\N	t
3212	TemplateColumn	285	Earnings Determination	1	2016-01-23 15:01:20.405857	2016-01-23 15:01:20.405857		earnings-determination	\N	\N
3213	TemplateColumn	285	Plus Commissions	2	2016-01-23 15:01:20.41581	2016-01-23 15:01:20.41581		plus-commissions	\N	\N
3214	TemplateColumn	285	Plus Bonus	3	2016-01-23 15:01:20.428108	2016-01-23 15:01:20.428108		plus-bonus	\N	\N
3215	TemplateColumn	285	Plus Overtime	4	2016-01-23 15:01:20.43937	2016-01-23 15:01:20.43937		plus-overtime	\N	\N
3216	TemplateField	286	Pre-existing Condition Exclusions	7	2016-01-23 15:01:20.503448	2016-01-23 15:01:20.503448		pre-existing-condition-exclusions	t	t
3217	TemplateField	286	Definition of Disability	1	2016-01-23 15:01:20.513869	2016-01-23 15:01:20.513869		definition-of-disability	t	t
3218	TemplateField	286	Elimination Period	2	2016-01-23 15:01:20.524632	2016-01-23 15:01:20.524632		elimination-period	t	t
3219	TemplateField	286	Employer Gross Up	3	2016-01-23 15:01:20.535873	2016-01-23 15:01:20.535873		employer-gross-up	\N	t
3220	TemplateField	286	Maximum Benefit Duration	4	2016-01-23 15:01:20.545906	2016-01-23 15:01:20.545906		maximum-benefit-duration	t	t
3222	TemplateField	286	Minimum Weekly Benefit	6	2016-01-23 15:01:20.567989	2016-01-23 15:01:20.567989		minimum-weekly-benefit	\N	t
3223	TemplateColumn	286	Entries	1	2016-01-23 15:01:20.580232	2016-01-23 15:01:20.580232		entries	\N	\N
3224	TemplateField	287	Benefit Integration Offsets	1	2016-01-23 15:01:20.683174	2016-01-23 15:01:20.683174		benefit-integration-offsets	\N	t
3225	TemplateField	287	Employer FICA match	2	2016-01-23 15:01:20.693601	2016-01-23 15:01:20.693601		employer-fica-match	\N	\N
3226	TemplateField	287	Lump Sum Survivor Benefit	3	2016-01-23 15:01:20.704098	2016-01-23 15:01:20.704098		lump-sum-survivor-benefit	\N	\N
3227	TemplateField	287	Partial Disability	4	2016-01-23 15:01:20.715765	2016-01-23 15:01:20.715765		partial-disability	\N	\N
3228	TemplateField	287	Recurrent Disability	5	2016-01-23 15:01:20.726406	2016-01-23 15:01:20.726406		recurrent-disability	\N	\N
3229	TemplateField	287	Vocational Rehab Incentive	6	2016-01-23 15:01:20.737038	2016-01-23 15:01:20.737038		vocational-rehab-incentive	\N	\N
3230	TemplateField	287	W2 services provided	7	2016-01-23 15:01:20.74749	2016-01-23 15:01:20.74749		w2-services-provided	\N	\N
3231	TemplateColumn	287	Entries	1	2016-01-23 15:01:20.759829	2016-01-23 15:01:20.759829		entries	\N	\N
3232	TemplateField	288	Employee Eligibility Class #1	1	2016-01-23 15:01:20.863602	2016-01-23 15:01:20.863602		employee-eligibility-class-1	\N	\N
3233	TemplateField	288	Employee Eligibility Class #2	2	2016-01-23 15:01:20.873847	2016-01-23 15:01:20.873847		employee-eligibility-class-2	\N	\N
3234	TemplateField	288	Employee Eligibility Class #3	3	2016-01-23 15:01:20.883756	2016-01-23 15:01:20.883756		employee-eligibility-class-3	\N	\N
3235	TemplateField	288	Employee Eligibility Class #4	4	2016-01-23 15:01:20.89399	2016-01-23 15:01:20.89399		employee-eligibility-class-4	\N	\N
3236	TemplateColumn	288	Employee Class	1	2016-01-23 15:01:20.90781	2016-01-23 15:01:20.90781		employee-class	\N	\N
3237	TemplateColumn	288	Employee Classes	2	2016-01-23 15:01:20.917877	2016-01-23 15:01:20.917877		employee-classes	\N	\N
3238	TemplateColumn	288	Waiting Period	3	2016-01-23 15:01:20.927843	2016-01-23 15:01:20.927843		waiting-period	\N	\N
3239	TemplateColumn	288	Hours Requirement	4	2016-01-23 15:01:20.937836	2016-01-23 15:01:20.937836		hours-requirement	\N	\N
3240	TemplateField	289	Employee Eligibility Class #1	1	2016-01-23 15:03:34.288273	2016-01-23 15:03:34.288273		employee-eligibility-class-1	\N	\N
3241	TemplateField	289	Employee Eligibility Class #2	2	2016-01-23 15:03:34.298562	2016-01-23 15:03:34.298562		employee-eligibility-class-2	\N	\N
3242	TemplateField	289	Employee Eligibility Class #3	3	2016-01-23 15:03:34.308992	2016-01-23 15:03:34.308992		employee-eligibility-class-3	\N	\N
3243	TemplateField	289	Employee Eligibility Class #4	4	2016-01-23 15:03:34.320196	2016-01-23 15:03:34.320196		employee-eligibility-class-4	\N	\N
3244	TemplateColumn	289	Employee Class	1	2016-01-23 15:03:34.333131	2016-01-23 15:03:34.333131		employee-class	\N	\N
3245	TemplateColumn	289	Employee Classes	2	2016-01-23 15:03:34.344068	2016-01-23 15:03:34.344068		employee-classes	\N	\N
3246	TemplateColumn	289	Waiting Period	3	2016-01-23 15:03:34.355311	2016-01-23 15:03:34.355311		waiting-period	\N	\N
3247	TemplateColumn	289	Hours Requirement	4	2016-01-23 15:03:34.365543	2016-01-23 15:03:34.365543		hours-requirement	\N	\N
3248	TemplateColumn	289	Spouse Eligible	5	2016-01-23 15:03:34.375856	2016-01-23 15:03:34.375856		spouse-eligible	\N	\N
3249	TemplateColumn	289	Dependent Children Eligible	6	2016-01-23 15:03:34.385808	2016-01-23 15:03:34.385808		dependent-children-eligible	\N	\N
3250	TemplateColumn	289	Domestic Partner Eligible	7	2016-01-23 15:03:34.395879	2016-01-23 15:03:34.395879		domestic-partner-eligible	\N	\N
3251	TemplateColumn	289	Dependent Child Limiting Age	8	2016-01-23 15:03:34.407516	2016-01-23 15:03:34.407516		dependent-child-limiting-age	\N	\N
3252	TemplateColumn	289	Dependent Child Coverage Ends	9	2016-01-23 15:03:34.4177	2016-01-23 15:03:34.4177		dependent-child-coverage-ends	\N	\N
3253	TemplateField	290	Exams	1	2016-01-23 15:03:34.855197	2016-01-23 15:03:34.855197		exams	t	t
3254	TemplateField	290	Frames	2	2016-01-23 15:03:34.866429	2016-01-23 15:03:34.866429		frames	t	t
3255	TemplateField	290	Lenses - Single Vision	3	2016-01-23 15:03:34.876658	2016-01-23 15:03:34.876658		lenses-single-vision	t	t
3256	TemplateField	290	Lenses - Lined Bifocal	4	2016-01-23 15:03:34.887035	2016-01-23 15:03:34.887035		lenses-lined-bifocal	\N	\N
3257	TemplateField	290	Lenses - Lined Trifocal	5	2016-01-23 15:03:34.897186	2016-01-23 15:03:34.897186		lenses-lined-trifocal	\N	\N
3258	TemplateField	290	Lenses - Lenticular	6	2016-01-23 15:03:34.907638	2016-01-23 15:03:34.907638		lenses-lenticular	\N	\N
3259	TemplateField	290	Contacts	7	2016-01-23 15:03:34.91936	2016-01-23 15:03:34.91936		contacts	t	t
3260	TemplateColumn	290	Limit	1	2016-01-23 15:03:34.931794	2016-01-23 15:03:34.931794		limit	\N	\N
3261	TemplateColumn	290	Coinsurance	2	2016-01-23 15:03:34.942196	2016-01-23 15:03:34.942196		coinsurance	\N	\N
3262	TemplateColumn	290	copay	3	2016-01-23 15:03:34.952317	2016-01-23 15:03:34.952317		copay	\N	\N
3263	TemplateField	291	Retail Frames	1	2016-01-23 15:03:35.211237	2016-01-23 15:03:35.211237		retail-frames	t	t
3264	TemplateField	291	Contacts - Non Selection	3	2016-01-23 15:03:35.2217	2016-01-23 15:03:35.2217		contacts-non-selection	\N	\N
3265	TemplateField	291	Contacts - Elective Disposable	4	2016-01-23 15:03:35.231901	2016-01-23 15:03:35.231901		contacts-elective-disposable	\N	\N
3266	TemplateField	291	Contacts -Necessary Lenses	5	2016-01-23 15:03:35.241956	2016-01-23 15:03:35.241956		contacts-necessary-lenses	\N	\N
3267	TemplateField	291	Premium Progressive Lenses	6	2016-01-23 15:03:35.252061	2016-01-23 15:03:35.252061		premium-progressive-lenses	\N	\N
3268	TemplateField	291	Contacts - Covered Selection	2	2016-01-23 15:03:35.262047	2016-01-23 15:03:35.262047		contacts-covered-selection	t	t
3269	TemplateColumn	291	Allowance	2	2016-01-23 15:03:35.274716	2016-01-23 15:03:35.274716		allowance	\N	\N
3270	TemplateColumn	291	Discount	1	2016-01-23 15:03:35.28545	2016-01-23 15:03:35.28545		discount	\N	\N
3271	TemplateField	292	Materials Coverage	2	2016-01-23 15:03:35.456014	2016-01-23 15:03:35.456014		materials-coverage	t	t
3272	TemplateField	292	International Employees Covered	3	2016-01-23 15:03:35.466426	2016-01-23 15:03:35.466426		international-employees-covered	\N	\N
3273	TemplateField	292	Plan Type	1	2016-01-23 15:03:35.476428	2016-01-23 15:03:35.476428		plan-type	t	t
3274	TemplateColumn	292	Entries	1	2016-01-23 15:03:35.488449	2016-01-23 15:03:35.488449		entries	\N	\N
3275	TemplateField	293	Exams	1	2016-01-23 15:03:35.546141	2016-01-23 15:03:35.546141		exams	t	t
3276	TemplateField	293	Frames	2	2016-01-23 15:03:35.556787	2016-01-23 15:03:35.556787		frames	t	t
3277	TemplateField	293	Lenses - Single Vision	3	2016-01-23 15:03:35.567015	2016-01-23 15:03:35.567015		lenses-single-vision	t	t
3278	TemplateField	293	Lenses - Lined Bifocal	4	2016-01-23 15:03:35.578074	2016-01-23 15:03:35.578074		lenses-lined-bifocal	\N	\N
3279	TemplateField	293	Lenses - Lined Trifocal	5	2016-01-23 15:03:35.588374	2016-01-23 15:03:35.588374		lenses-lined-trifocal	\N	\N
3280	TemplateField	293	Lenses - Lenticular	6	2016-01-23 15:03:35.59879	2016-01-23 15:03:35.59879		lenses-lenticular	\N	\N
3281	TemplateField	293	Contacts	7	2016-01-23 15:03:35.609366	2016-01-23 15:03:35.609366		contacts	t	t
3282	TemplateColumn	293	Limit	1	2016-01-23 15:03:35.621735	2016-01-23 15:03:35.621735		limit	\N	\N
3283	TemplateColumn	293	Coinsurance	2	2016-01-23 15:03:35.631942	2016-01-23 15:03:35.631942		coinsurance	\N	\N
3284	TemplateField	294	Retail Frames	1	2016-01-23 15:03:35.815832	2016-01-23 15:03:35.815832		retail-frames	t	t
3285	TemplateField	294	Contacts - Covered Selection	2	2016-01-23 15:03:35.826048	2016-01-23 15:03:35.826048		contacts-covered-selection	t	t
3286	TemplateField	294	Contacts - Non Selection	3	2016-01-23 15:03:35.836413	2016-01-23 15:03:35.836413		contacts-non-selection	\N	\N
3287	TemplateField	294	Contacts - Elective Disposable	4	2016-01-23 15:03:35.846341	2016-01-23 15:03:35.846341		contacts-elective-disposable	\N	\N
3288	TemplateField	294	Contacts -Necessary Lenses	5	2016-01-23 15:03:35.856294	2016-01-23 15:03:35.856294		contacts-necessary-lenses	\N	\N
3289	TemplateField	294	Premium Progressive Lenses	6	2016-01-23 15:03:35.866335	2016-01-23 15:03:35.866335		premium-progressive-lenses	\N	\N
3290	TemplateColumn	294	Allowance	1	2016-01-23 15:03:35.87843	2016-01-23 15:03:35.87843		allowance	\N	\N
3291	TemplateField	295	Plan Type	1	2016-01-23 15:03:35.985814	2016-01-23 15:03:35.985814		plan-type	t	t
3292	TemplateField	295	Materials Coverage	2	2016-01-23 15:03:35.996289	2016-01-23 15:03:35.996289		materials-coverage	t	t
3293	TemplateField	295	International Employees Covered	3	2016-01-23 15:03:36.006342	2016-01-23 15:03:36.006342		international-employees-covered	\N	\N
3294	TemplateColumn	295	Entries	1	2016-01-23 15:03:36.018331	2016-01-23 15:03:36.018331		entries	\N	\N
2118	TemplateColumn	182	Dependent Children Eligible	6	2016-01-20 19:59:49.441571	2016-01-23 15:53:39.845399		dependent-children-eligible	\N	\N
3295	TemplateField	296	Primary Care Office Visit	1	2016-01-25 09:08:53.081034	2016-01-25 09:08:53.081034		primary-care-office-visit	t	t
3296	TemplateField	296	Specialist Office Visit	2	2016-01-25 09:08:53.095839	2016-01-25 09:08:53.095839		specialist-office-visit	t	t
3297	TemplateField	296	Lab	3	2016-01-25 09:08:53.106562	2016-01-25 09:08:53.106562		lab	\N	t
3298	TemplateField	296	General Radiology	4	2016-01-25 09:08:53.119521	2016-01-25 09:08:53.119521		general-radiology	\N	\N
3299	TemplateField	296	CT/MRI/PET Scans	5	2016-01-25 09:08:53.131677	2016-01-25 09:08:53.131677		ct-mri-pet-scans	\N	\N
3300	TemplateField	296	Annual Physical Exam	6	2016-01-25 09:08:53.141828	2016-01-25 09:08:53.141828		annual-physical-exam	\N	\N
3301	TemplateField	296	Well Child Exams	7	2016-01-25 09:08:53.152156	2016-01-25 09:08:53.152156		well-child-exams	\N	\N
3302	TemplateField	296	Pediatric Dental	8	2016-01-25 09:08:53.162132	2016-01-25 09:08:53.162132		pediatric-dental	\N	\N
3303	TemplateField	296	Hospitalization - Inpatient	9	2016-01-25 09:08:53.17216	2016-01-25 09:08:53.17216		hospitalization-inpatient	t	t
3304	TemplateField	296	Hospitalization - Outpatient	10	2016-01-25 09:08:53.182501	2016-01-25 09:08:53.182501		hospitalization-outpatient	\N	t
3305	TemplateField	296	Surgery/Anesthesiology	11	2016-01-25 09:08:53.192593	2016-01-25 09:08:53.192593		surgery-anesthesiology	\N	\N
3306	TemplateField	296	Emergency Room	12	2016-01-25 09:08:53.202567	2016-01-25 09:08:53.202567		emergency-room	t	t
3307	TemplateField	296	Urgent Care	13	2016-01-25 09:08:53.212745	2016-01-25 09:08:53.212745		urgent-care	\N	t
3308	TemplateField	296	Mental Nervous - Inpatient Coverage	14	2016-01-25 09:08:53.222861	2016-01-25 09:08:53.222861		mental-nervous-inpatient-coverage	\N	t
3309	TemplateField	296	Mental Nervous - Outpatient Coverage	15	2016-01-25 09:08:53.233647	2016-01-25 09:08:53.233647		mental-nervous-outpatient-coverage	\N	t
3310	TemplateField	296	Substance Abuse - Inpatient Coverage	16	2016-01-25 09:08:53.243841	2016-01-25 09:08:53.243841		substance-abuse-inpatient-coverage	\N	t
3311	TemplateField	296	Physical Therapy - Inpatient Coverage	18	2016-01-25 09:08:53.254382	2016-01-25 09:08:53.254382		physical-therapy-inpatient-coverage	\N	\N
3312	TemplateField	296	Physical Therapy - Outpatient Coverage	19	2016-01-25 09:08:53.264596	2016-01-25 09:08:53.264596		physical-therapy-outpatient-coverage	\N	t
3313	TemplateField	296	Occupational Therapy - Inpatient Coverage	20	2016-01-25 09:08:53.275142	2016-01-25 09:08:53.275142		occupational-therapy-inpatient-coverage	\N	\N
3314	TemplateField	296	Occupational Therapy - Outpatient Coverage	21	2016-01-25 09:08:53.285293	2016-01-25 09:08:53.285293		occupational-therapy-outpatient-coverage	\N	\N
3315	TemplateField	296	Speech Therapy - Inpatient Coverage	22	2016-01-25 09:08:53.295301	2016-01-25 09:08:53.295301		speech-therapy-inpatient-coverage	\N	\N
3316	TemplateField	296	Speech Therapy - Outpatient Coverage	23	2016-01-25 09:08:53.305205	2016-01-25 09:08:53.305205		speech-therapy-outpatient-coverage	\N	\N
3317	TemplateField	296	Pregnancy & Maternity Care - Office Visits	24	2016-01-25 09:08:53.315756	2016-01-25 09:08:53.315756		pregnancy-maternity-care-office-visits	\N	\N
3318	TemplateField	296	Pregnancy & Maternity Care - Labor & Delivery	25	2016-01-25 09:08:53.325931	2016-01-25 09:08:53.325931		pregnancy-maternity-care-labor-delivery	\N	\N
3319	TemplateField	296	Chiropractic Services	26	2016-01-25 09:08:53.336227	2016-01-25 09:08:53.336227		chiropractic-services	\N	t
3320	TemplateField	296	Ambulance	27	2016-01-25 09:08:53.36152	2016-01-25 09:08:53.36152		ambulance	\N	\N
3321	TemplateField	296	Hospice	28	2016-01-25 09:08:53.371816	2016-01-25 09:08:53.371816		hospice	\N	\N
3322	TemplateField	296	Home Healthcare	29	2016-01-25 09:08:53.382338	2016-01-25 09:08:53.382338		home-healthcare	\N	\N
3323	TemplateField	296	Skilled Nursing	30	2016-01-25 09:08:53.392872	2016-01-25 09:08:53.392872		skilled-nursing	\N	\N
3324	TemplateField	296	Infertility Coverage	31	2016-01-25 09:08:53.403117	2016-01-25 09:08:53.403117		infertility-coverage	\N	t
3325	TemplateField	296	Prosthetics	32	2016-01-25 09:08:53.413038	2016-01-25 09:08:53.413038		prosthetics	\N	\N
3326	TemplateField	296	Durable Medical Equipment	33	2016-01-25 09:08:53.423052	2016-01-25 09:08:53.423052		durable-medical-equipment	\N	t
3327	TemplateField	296	Hearing Devices	34	2016-01-25 09:08:53.433271	2016-01-25 09:08:53.433271		hearing-devices	\N	\N
3328	TemplateField	296	Vision Exams	35	2016-01-25 09:08:53.443614	2016-01-25 09:08:53.443614		vision-exams	\N	\N
3329	TemplateField	296	Short Term Rehabilitation- Inpatient	36	2016-01-25 09:08:53.453976	2016-01-25 09:08:53.453976		short-term-rehabilitation-inpatient	\N	\N
3330	TemplateField	296	Short Term Rehabilitation - Outpatient	37	2016-01-25 09:08:53.464487	2016-01-25 09:08:53.464487		short-term-rehabilitation-outpatient	\N	\N
3331	TemplateField	296	Telemedicine	38	2016-01-25 09:08:53.474687	2016-01-25 09:08:53.474687		telemedicine	\N	\N
3332	TemplateField	296	Speciality Rx Coverage	39	2016-01-25 09:08:53.484834	2016-01-25 09:08:53.484834		speciality-rx-coverage	\N	\N
3333	TemplateField	296	Substance Abuse - Outpatient Coverage	17	2016-01-25 09:08:53.494826	2016-01-25 09:08:53.494826		substance-abuse-outpatient-coverage	\N	t
3334	TemplateColumn	296	Subject to Deductible & Coinsurance	1	2016-01-25 09:08:53.509571	2016-01-25 09:08:53.509571		subject-to-deductible-coinsurance	\N	\N
3335	TemplateColumn	296	Limit	3	2016-01-25 09:08:53.522041	2016-01-25 09:08:53.522041		limit	\N	\N
3336	TemplateColumn	296	Copay Limit	4	2016-01-25 09:08:53.532105	2016-01-25 09:08:53.532105		copay-limit	\N	\N
3337	TemplateColumn	296	Separate Coinsurance	6	2016-01-25 09:08:53.54215	2016-01-25 09:08:53.54215		separate-coinsurance	\N	\N
3338	TemplateColumn	296	Separate Deductible	5	2016-01-25 09:08:53.553326	2016-01-25 09:08:53.553326		separate-deductible	\N	\N
3339	TemplateColumn	296	Copay	2	2016-01-25 09:08:53.563475	2016-01-25 09:08:53.563475		copay	\N	\N
3340	TemplateField	297	Subject to Medical Deductible	1	2016-01-25 09:08:56.189679	2016-01-25 09:08:56.189679		subject-to-medical-deductible	\N	t
3341	TemplateField	297	Subject to Medical Deductible & Coinsurance	2	2016-01-25 09:08:56.200635	2016-01-25 09:08:56.200635		subject-to-medical-deductible-coinsurance	\N	\N
3342	TemplateField	297	Network	3	2016-01-25 09:08:56.210921	2016-01-25 09:08:56.210921		network	\N	t
3343	TemplateField	297	Mandatory Generic	4	2016-01-25 09:08:56.221208	2016-01-25 09:08:56.221208		mandatory-generic	\N	\N
3344	TemplateField	297	Step Therapy/Precertification Applies	5	2016-01-25 09:08:56.231632	2016-01-25 09:08:56.231632		step-therapy-precertification-applies	\N	\N
3345	TemplateField	297	Oral Contraceptive	6	2016-01-25 09:08:56.241901	2016-01-25 09:08:56.241901		oral-contraceptive	\N	\N
3346	TemplateField	297	Prescriptions Covered Out of Network	7	2016-01-25 09:08:56.253036	2016-01-25 09:08:56.253036		prescriptions-covered-out-of-network	\N	\N
3347	TemplateColumn	297	Entries	1	2016-01-25 09:08:56.26629	2016-01-25 09:08:56.26629		entries	\N	\N
3348	TemplateField	298	Rx Deductible - Individual	1	2016-01-25 09:08:56.361412	2016-01-25 09:08:56.361412		rx-deductible-individual	t	t
3349	TemplateField	298	Rx Deductible - Family	2	2016-01-25 09:08:56.372357	2016-01-25 09:08:56.372357		rx-deductible-family	t	t
3350	TemplateColumn	298	Deductible	1	2016-01-25 09:08:56.385139	2016-01-25 09:08:56.385139		deductible	\N	\N
3351	TemplateColumn	298	Per Member	2	2016-01-25 09:08:56.395967	2016-01-25 09:08:56.395967		per-member	\N	\N
3352	TemplateField	299	Employee Eligibility Class #1	1	2016-01-25 09:08:56.462594	2016-01-25 09:08:56.462594		employee-eligibility-class-1	\N	\N
3353	TemplateField	299	Employee Eligibility Class #2	2	2016-01-25 09:08:56.473052	2016-01-25 09:08:56.473052		employee-eligibility-class-2	\N	\N
3354	TemplateField	299	Employee Eligibility Class #3	3	2016-01-25 09:08:56.484118	2016-01-25 09:08:56.484118		employee-eligibility-class-3	\N	\N
3355	TemplateField	299	Employee Eligibility Class #4	4	2016-01-25 09:08:56.495164	2016-01-25 09:08:56.495164		employee-eligibility-class-4	\N	\N
3356	TemplateColumn	299	Employee Class	1	2016-01-25 09:08:56.508326	2016-01-25 09:08:56.508326		employee-class	\N	\N
3357	TemplateColumn	299	Employee Classes	2	2016-01-25 09:08:56.518772	2016-01-25 09:08:56.518772		employee-classes	\N	\N
3358	TemplateColumn	299	Waiting Period	3	2016-01-25 09:08:56.529117	2016-01-25 09:08:56.529117		waiting-period	\N	\N
3359	TemplateColumn	299	Hours Requirement	4	2016-01-25 09:08:56.53968	2016-01-25 09:08:56.53968		hours-requirement	\N	\N
3360	TemplateColumn	299	Spouse Eligible	5	2016-01-25 09:08:56.550677	2016-01-25 09:08:56.550677		spouse-eligible	\N	\N
3361	TemplateColumn	299	Dependent Children Eligible	6	2016-01-25 09:08:56.561174	2016-01-25 09:08:56.561174		dependent-children-eligible	\N	\N
3362	TemplateColumn	299	Domestic Partner Eligible	7	2016-01-25 09:08:56.571656	2016-01-25 09:08:56.571656		domestic-partner-eligible	\N	\N
3363	TemplateColumn	299	Dependent Child Limiting Age	8	2016-01-25 09:08:56.582272	2016-01-25 09:08:56.582272		dependent-child-limiting-age	\N	\N
3364	TemplateColumn	299	Dependent Child Coverage Ends	9	2016-01-25 09:08:56.592793	2016-01-25 09:08:56.592793		dependent-child-coverage-ends	\N	\N
3365	TemplateField	300	Plan Type	1	2016-01-25 09:08:57.099644	2016-01-25 09:08:57.099644		plan-type	t	t
3366	TemplateField	300	Network	3	2016-01-25 09:08:57.109551	2016-01-25 09:08:57.109551		network	t	t
3367	TemplateField	300	Referrals Needed	5	2016-01-25 09:08:57.119401	2016-01-25 09:08:57.119401		referrals-needed	t	t
3368	TemplateField	300	Deductible Individual	7	2016-01-25 09:08:57.129317	2016-01-25 09:08:57.129317		deductible-individual	t	t
3369	TemplateField	300	Deductible Family	9	2016-01-25 09:08:57.139523	2016-01-25 09:08:57.139523		deductible-family	t	t
3370	TemplateField	300	Deductible - Family Multiple	11	2016-01-25 09:08:57.14965	2016-01-25 09:08:57.14965		deductible-family-multiple	\N	\N
3371	TemplateField	300	Deductible Format	13	2016-01-25 09:08:57.159391	2016-01-25 09:08:57.159391		deductible-format	\N	t
3372	TemplateField	300	Coinsurance	15	2016-01-25 09:08:57.169503	2016-01-25 09:08:57.169503		coinsurance	t	t
3373	TemplateField	300	Out-of-Pocket Max Family (includes deductible)	19	2016-01-25 09:08:57.179886	2016-01-25 09:08:57.179886		out-of-pocket-max-family-includes-deductible	t	t
3374	TemplateField	300	In & Out-of-Network Deductibles Cross Accumulation	23	2016-01-25 09:08:57.190114	2016-01-25 09:08:57.190114		in-out-of-network-deductibles-cross-accumulation	\N	t
3375	TemplateField	300	OON Reimbursement Level	27	2016-01-25 09:08:57.200037	2016-01-25 09:08:57.200037		oon-reimbursement-level	t	t
3376	TemplateField	300	Annual Maximum	29	2016-01-25 09:08:57.210064	2016-01-25 09:08:57.210064		annual-maximum	\N	\N
3377	TemplateField	300	Gym Reimbursement	31	2016-01-25 09:08:57.220089	2016-01-25 09:08:57.220089		gym-reimbursement	\N	\N
3378	TemplateField	300	Customer Service Days/Hours	33	2016-01-25 09:08:57.230083	2016-01-25 09:08:57.230083		customer-service-days-hours	\N	\N
3379	TemplateField	300	Nurseline	35	2016-01-25 09:08:57.240033	2016-01-25 09:08:57.240033		nurseline	\N	\N
3380	TemplateField	300	International Employees Covered	37	2016-01-25 09:08:57.250159	2016-01-25 09:08:57.250159		international-employees-covered	\N	\N
3381	TemplateField	300	Out-of-Pocket Max Individual (includes deductible)	17	2016-01-25 09:08:57.260283	2016-01-25 09:08:57.260283		out-of-pocket-max-individual-includes-deductible	t	t
3382	TemplateField	300	Out-of-Pocket Maximum - Family Multiple	21	2016-01-25 09:08:57.270319	2016-01-25 09:08:57.270319		out-of-pocket-maximum-family-multiple	\N	f
3383	TemplateField	300	In & Out-of-Network OOP Maximum Cross Accumulation	25	2016-01-25 09:08:57.280233	2016-01-25 09:08:57.280233		in-out-of-network-oop-maximum-cross-accumulation	\N	t
3384	TemplateColumn	300	Entries	1	2016-01-25 09:08:57.292883	2016-01-25 09:08:57.292883		entries	\N	\N
3385	TemplateField	301	Retail Rx (Tier 1)	1	2016-01-25 09:08:57.507871	2016-01-25 09:08:57.507871		retail-rx-tier-1	t	t
3386	TemplateField	301	Retail Rx (Tier 3)	13	2016-01-25 09:08:57.517686	2016-01-25 09:08:57.517686		retail-rx-tier-3	t	t
3387	TemplateField	301	Retail Rx (Tier 4)	19	2016-01-25 09:08:57.527439	2016-01-25 09:08:57.527439		retail-rx-tier-4	\N	\N
3388	TemplateField	301	Retail Rx (Tier 5)	25	2016-01-25 09:08:57.537398	2016-01-25 09:08:57.537398		retail-rx-tier-5	\N	\N
3389	TemplateField	301	Mail Order Rx (Tier 1)	31	2016-01-25 09:08:57.54734	2016-01-25 09:08:57.54734		mail-order-rx-tier-1	\N	t
3390	TemplateField	301	Specialty Medications -Mail Order	67	2016-01-25 09:08:57.557536	2016-01-25 09:08:57.557536		specialty-medications-mail-order	\N	f
3391	TemplateField	301	Specialty Medications  -Retail	61	2016-01-25 09:08:57.567422	2016-01-25 09:08:57.567422		specialty-medications-retail	\N	\N
3392	TemplateField	301	Mail Order Rx (Tier 3)	43	2016-01-25 09:08:57.577401	2016-01-25 09:08:57.577401		mail-order-rx-tier-3	\N	t
3393	TemplateField	301	Mail Order Rx (Tier 4)	49	2016-01-25 09:08:57.587389	2016-01-25 09:08:57.587389		mail-order-rx-tier-4	\N	\N
3394	TemplateField	301	Mail Order Rx (Tier 5)	55	2016-01-25 09:08:57.597196	2016-01-25 09:08:57.597196		mail-order-rx-tier-5	\N	\N
3395	TemplateField	301	Retail Rx (Tier 2)	7	2016-01-25 09:08:57.607375	2016-01-25 09:08:57.607375		retail-rx-tier-2	t	t
3396	TemplateField	301	Mail Order Rx (Tier 2)	37	2016-01-25 09:08:57.617463	2016-01-25 09:08:57.617463		mail-order-rx-tier-2	\N	t
3397	TemplateColumn	301	Copay	2	2016-01-25 09:08:57.629518	2016-01-25 09:08:57.629518		copay	\N	\N
3398	TemplateColumn	301	Coinsurance	3	2016-01-25 09:08:57.640588	2016-01-25 09:08:57.640588		coinsurance	\N	\N
3399	TemplateColumn	301	Per Script Coinsurance Limit	4	2016-01-25 09:08:57.655073	2016-01-25 09:08:57.655073		per-script-coinsurance-limit	\N	\N
3400	TemplateColumn	301	Per Script Coinsurance Minimum	5	2016-01-25 09:08:57.669843	2016-01-25 09:08:57.669843		per-script-coinsurance-minimum	\N	\N
3401	TemplateColumn	301	Subject To Rx Deductible	1	2016-01-25 09:08:57.680731	2016-01-25 09:08:57.680731		subject-to-rx-deductible	\N	\N
3402	TemplateField	302	Disease Management	1	2016-01-25 09:08:58.771204	2016-01-25 09:08:58.771204		disease-management	\N	\N
3403	TemplateColumn	302	Number of Conditions Tracked	2	2016-01-25 09:08:58.783332	2016-01-25 09:08:58.783332		number-of-conditions-tracked	\N	\N
3404	TemplateColumn	302	Outreach Methods	3	2016-01-25 09:08:58.79379	2016-01-25 09:08:58.79379		outreach-methods	\N	\N
3405	TemplateColumn	302	Report Frequency	4	2016-01-25 09:08:58.80359	2016-01-25 09:08:58.80359		report-frequency	\N	\N
3406	TemplateColumn	302	Identification Mehtods	1	2016-01-25 09:08:58.81323	2016-01-25 09:08:58.81323		identification-mehtods	\N	\N
3407	TemplateField	303	Integrated Wellness Plan	1	2016-01-25 09:08:58.872877	2016-01-25 09:08:58.872877		integrated-wellness-plan	\N	\N
3408	TemplateColumn	303	Entries	1	2016-01-25 09:08:58.885111	2016-01-25 09:08:58.885111		entries	\N	\N
3409	TemplateField	306	General Radiology	4	2016-01-25 09:08:58.945504	2016-01-25 09:08:58.945504		general-radiology	\N	\N
3410	TemplateField	306	CT/MRI/PET Scans	5	2016-01-25 09:08:58.955825	2016-01-25 09:08:58.955825		ct-mri-pet-scans	\N	\N
3411	TemplateField	306	Annual Physical Exam	6	2016-01-25 09:08:58.965625	2016-01-25 09:08:58.965625		annual-physical-exam	\N	\N
3412	TemplateField	306	Well Child Exams	7	2016-01-25 09:08:58.975366	2016-01-25 09:08:58.975366		well-child-exams	\N	\N
3413	TemplateField	306	Pediatric Dental	8	2016-01-25 09:08:58.985157	2016-01-25 09:08:58.985157		pediatric-dental	\N	\N
3414	TemplateField	306	Hospitalization - Inpatient	9	2016-01-25 09:08:58.994972	2016-01-25 09:08:58.994972		hospitalization-inpatient	t	t
3415	TemplateField	306	Hospitalization - Outpatient	10	2016-01-25 09:08:59.004675	2016-01-25 09:08:59.004675		hospitalization-outpatient	\N	t
3416	TemplateField	306	Surgery/Anesthesiology	11	2016-01-25 09:08:59.014428	2016-01-25 09:08:59.014428		surgery-anesthesiology	\N	\N
3417	TemplateField	306	Emergency Room	12	2016-01-25 09:08:59.024145	2016-01-25 09:08:59.024145		emergency-room	t	t
3418	TemplateField	306	Specialist Office Visit	2	2016-01-25 09:08:59.034041	2016-01-25 09:08:59.034041		specialist-office-visit	t	t
3419	TemplateField	306	Lab	3	2016-01-25 09:08:59.043769	2016-01-25 09:08:59.043769		lab	\N	t
3420	TemplateField	306	Urgent Care	13	2016-01-25 09:08:59.05329	2016-01-25 09:08:59.05329		urgent-care	\N	t
3421	TemplateField	306	Mental Nervous - Outpatient Coverage	15	2016-01-25 09:08:59.085147	2016-01-25 09:08:59.085147		mental-nervous-outpatient-coverage	\N	t
3422	TemplateField	306	Substance Abuse - Inpatient Coverage	16	2016-01-25 09:08:59.095547	2016-01-25 09:08:59.095547		substance-abuse-inpatient-coverage	\N	t
3423	TemplateField	306	Substance Abuse - Outpatient Coverage	17	2016-01-25 09:08:59.105573	2016-01-25 09:08:59.105573		substance-abuse-outpatient-coverage	\N	t
3424	TemplateField	306	Physical Therapy - Inpatient Coverage	18	2016-01-25 09:08:59.115465	2016-01-25 09:08:59.115465		physical-therapy-inpatient-coverage	\N	\N
3425	TemplateField	306	Physical Therapy - Outpatient Coverage	19	2016-01-25 09:08:59.125432	2016-01-25 09:08:59.125432		physical-therapy-outpatient-coverage	\N	t
3426	TemplateField	306	Occupational Therapy - Inpatient Coverage	20	2016-01-25 09:08:59.135622	2016-01-25 09:08:59.135622		occupational-therapy-inpatient-coverage	\N	\N
3427	TemplateField	306	Occupational Therapy - Outpatient Coverage	21	2016-01-25 09:08:59.145987	2016-01-25 09:08:59.145987		occupational-therapy-outpatient-coverage	\N	\N
3428	TemplateField	306	Speech Therapy - Inpatient Coverage	22	2016-01-25 09:08:59.156122	2016-01-25 09:08:59.156122		speech-therapy-inpatient-coverage	\N	\N
3429	TemplateField	306	Speech Therapy - Outpatient Coverage	23	2016-01-25 09:08:59.166243	2016-01-25 09:08:59.166243		speech-therapy-outpatient-coverage	\N	\N
3430	TemplateField	306	Pregnancy & Maternity Care - Office Visits	24	2016-01-25 09:08:59.176342	2016-01-25 09:08:59.176342		pregnancy-maternity-care-office-visits	\N	\N
3431	TemplateField	306	Pregnancy & Maternity Care - Labor & Delivery	25	2016-01-25 09:08:59.186588	2016-01-25 09:08:59.186588		pregnancy-maternity-care-labor-delivery	\N	\N
3432	TemplateField	306	Chiropractic Services	26	2016-01-25 09:08:59.19665	2016-01-25 09:08:59.19665		chiropractic-services	\N	t
3433	TemplateField	306	Ambulance	27	2016-01-25 09:08:59.206826	2016-01-25 09:08:59.206826		ambulance	\N	\N
3434	TemplateField	306	Hospice	28	2016-01-25 09:08:59.216746	2016-01-25 09:08:59.216746		hospice	\N	\N
3435	TemplateField	306	Home Healthcare	29	2016-01-25 09:08:59.226767	2016-01-25 09:08:59.226767		home-healthcare	\N	\N
3436	TemplateField	306	Skilled Nursing	30	2016-01-25 09:08:59.237118	2016-01-25 09:08:59.237118		skilled-nursing	\N	\N
3437	TemplateField	306	Infertility Coverage	31	2016-01-25 09:08:59.247869	2016-01-25 09:08:59.247869		infertility-coverage	\N	t
3438	TemplateField	306	Prosthetics	32	2016-01-25 09:08:59.258324	2016-01-25 09:08:59.258324		prosthetics	\N	\N
3439	TemplateField	306	Durable Medical Equipment	33	2016-01-25 09:08:59.268633	2016-01-25 09:08:59.268633		durable-medical-equipment	\N	t
3440	TemplateField	306	Hearing Devices	34	2016-01-25 09:08:59.279064	2016-01-25 09:08:59.279064		hearing-devices	\N	\N
3441	TemplateField	306	Vision Exams	35	2016-01-25 09:08:59.289076	2016-01-25 09:08:59.289076		vision-exams	\N	\N
3442	TemplateField	306	Short Term Rehabilitation - Inpatient	36	2016-01-25 09:08:59.299321	2016-01-25 09:08:59.299321		short-term-rehabilitation-inpatient	\N	\N
3443	TemplateField	306	Short Term Rehabilitation - Outpatient	37	2016-01-25 09:08:59.310131	2016-01-25 09:08:59.310131		short-term-rehabilitation-outpatient	\N	\N
3444	TemplateField	306	Telemedicine	38	2016-01-25 09:08:59.320351	2016-01-25 09:08:59.320351		telemedicine	\N	\N
3445	TemplateField	306	Specialty Rx Coverage	39	2016-01-25 09:08:59.330557	2016-01-25 09:08:59.330557		specialty-rx-coverage	\N	\N
3446	TemplateField	306	Primary Care Office Visit	1	2016-01-25 09:08:59.340688	2016-01-25 09:08:59.340688		primary-care-office-visit	t	t
3447	TemplateField	306	Mental Nervous - Inpatient Coverage	14	2016-01-25 09:08:59.350985	2016-01-25 09:08:59.350985		mental-nervous-inpatient-coverage	\N	t
3448	TemplateColumn	306	Subject to Deductible & Coinsurance	1	2016-01-25 09:08:59.363702	2016-01-25 09:08:59.363702		subject-to-deductible-coinsurance	\N	\N
3449	TemplateColumn	306	Limit	2	2016-01-25 09:08:59.373802	2016-01-25 09:08:59.373802		limit	\N	\N
3450	TemplateColumn	306	Separate Deductible	3	2016-01-25 09:08:59.384057	2016-01-25 09:08:59.384057		separate-deductible	\N	\N
3451	TemplateColumn	306	Separate Coinsurance	4	2016-01-25 09:08:59.394438	2016-01-25 09:08:59.394438		separate-coinsurance	\N	\N
3452	TemplateField	307	Plan Type	1	2016-01-25 09:09:01.017759	2016-01-25 09:09:01.017759		plan-type	t	t
3453	TemplateField	307	Network	2	2016-01-25 09:09:01.028172	2016-01-25 09:09:01.028172		network	t	t
3454	TemplateField	307	Referrals Needed	3	2016-01-25 09:09:01.038472	2016-01-25 09:09:01.038472		referrals-needed	t	t
3455	TemplateField	307	Deductible Individual	4	2016-01-25 09:09:01.048809	2016-01-25 09:09:01.048809		deductible-individual	t	t
3456	TemplateField	307	Deductible Family	5	2016-01-25 09:09:01.059057	2016-01-25 09:09:01.059057		deductible-family	t	t
3457	TemplateField	307	Deductible - Family Multiple	6	2016-01-25 09:09:01.069452	2016-01-25 09:09:01.069452		deductible-family-multiple	\N	\N
3458	TemplateField	307	Deductible Format	7	2016-01-25 09:09:01.079822	2016-01-25 09:09:01.079822		deductible-format	\N	t
3459	TemplateField	307	Coinsurance	8	2016-01-25 09:09:01.090267	2016-01-25 09:09:01.090267		coinsurance	t	t
3460	TemplateField	307	Out-of-Pocket Max Individual (includes deductible)	9	2016-01-25 09:09:01.100707	2016-01-25 09:09:01.100707		out-of-pocket-max-individual-includes-deductible	t	t
3461	TemplateField	307	Out-of-Pocket Max Family (includes deductible)	10	2016-01-25 09:09:01.111152	2016-01-25 09:09:01.111152		out-of-pocket-max-family-includes-deductible	t	t
3462	TemplateField	307	Out-of-Pocket Maximum - Family Multiple	11	2016-01-25 09:09:01.121529	2016-01-25 09:09:01.121529		out-of-pocket-maximum-family-multiple	\N	\N
3463	TemplateField	307	In & Out-of-Network Deductibles Cross Accumulation	12	2016-01-25 09:09:01.13177	2016-01-25 09:09:01.13177		in-out-of-network-deductibles-cross-accumulation	\N	t
3464	TemplateField	307	In & Out-of-Network OOP Maximum Cross Accumulation	13	2016-01-25 09:09:01.142616	2016-01-25 09:09:01.142616		in-out-of-network-oop-maximum-cross-accumulation	\N	t
3465	TemplateField	307	OON Reimbursement Level	14	2016-01-25 09:09:01.153172	2016-01-25 09:09:01.153172		oon-reimbursement-level	t	t
3466	TemplateField	307	Annual Maximum	15	2016-01-25 09:09:01.163626	2016-01-25 09:09:01.163626		annual-maximum	\N	\N
3467	TemplateField	307	Gym Reimbursement	16	2016-01-25 09:09:01.173908	2016-01-25 09:09:01.173908		gym-reimbursement	\N	\N
3468	TemplateField	307	Customer Service Days/Hours	17	2016-01-25 09:09:01.184248	2016-01-25 09:09:01.184248		customer-service-days-hours	\N	\N
3469	TemplateField	307	Nurseline	18	2016-01-25 09:09:01.19437	2016-01-25 09:09:01.19437		nurseline	\N	\N
3470	TemplateField	307	International Employees Covered	19	2016-01-25 09:09:01.206363	2016-01-25 09:09:01.206363		international-employees-covered	\N	\N
3471	TemplateColumn	307	Entries	1	2016-01-25 09:09:01.21898	2016-01-25 09:09:01.21898		entries	\N	\N
3472	TemplateField	308	Subject to Medical Deductible	1	2016-01-25 09:09:01.451406	2016-01-25 09:09:01.451406		subject-to-medical-deductible	\N	t
3473	TemplateField	308	Subject to Medical Deductible & Coinsurance	2	2016-01-25 09:09:01.461616	2016-01-25 09:09:01.461616		subject-to-medical-deductible-coinsurance	\N	\N
3474	TemplateField	308	Network	3	2016-01-25 09:09:01.471708	2016-01-25 09:09:01.471708		network	\N	t
3475	TemplateField	308	Step Therapy/Precertification Applies	4	2016-01-25 09:09:01.481862	2016-01-25 09:09:01.481862		step-therapy-precertification-applies	\N	\N
3476	TemplateField	308	Mandatory Generic	5	2016-01-25 09:09:01.492253	2016-01-25 09:09:01.492253		mandatory-generic	\N	\N
3477	TemplateField	308	Oral Contraceptive	6	2016-01-25 09:09:01.502495	2016-01-25 09:09:01.502495		oral-contraceptive	\N	\N
3478	TemplateField	308	Are Prescriptions Covered Out of Network	7	2016-01-25 09:09:01.512578	2016-01-25 09:09:01.512578		are-prescriptions-covered-out-of-network	\N	\N
3479	TemplateColumn	308	Entries	1	2016-01-25 09:09:01.524805	2016-01-25 09:09:01.524805		entries	\N	\N
3480	TemplateField	309	Rx Deductible - Family	1	2016-01-25 09:09:01.614627	2016-01-25 09:09:01.614627		rx-deductible-family	t	t
3481	TemplateField	309	Rx Deductible - Individual	2	2016-01-25 09:09:01.624738	2016-01-25 09:09:01.624738		rx-deductible-individual	t	t
3482	TemplateColumn	309	Deductible	1	2016-01-25 09:09:01.637213	2016-01-25 09:09:01.637213		deductible	\N	\N
3483	TemplateColumn	309	Per Member	2	2016-01-25 09:09:01.647335	2016-01-25 09:09:01.647335		per-member	\N	\N
3484	TemplateField	310	Retail Rx (Tier 1)	1	2016-01-25 09:09:01.711979	2016-01-25 09:09:01.711979		retail-rx-tier-1	t	t
3485	TemplateField	310	Retail Rx (Tier 2)	2	2016-01-25 09:09:01.72186	2016-01-25 09:09:01.72186		retail-rx-tier-2	t	t
3486	TemplateField	310	Retail Rx (Tier 3)	3	2016-01-25 09:09:01.731782	2016-01-25 09:09:01.731782		retail-rx-tier-3	t	t
3487	TemplateField	310	Retail Rx (Tier 4)	4	2016-01-25 09:09:01.743812	2016-01-25 09:09:01.743812		retail-rx-tier-4	\N	\N
3488	TemplateField	310	Retail Rx (Tier 5)	5	2016-01-25 09:09:01.754289	2016-01-25 09:09:01.754289		retail-rx-tier-5	\N	\N
3489	TemplateField	310	Mail Order Rx (Tier 1)	6	2016-01-25 09:09:01.764691	2016-01-25 09:09:01.764691		mail-order-rx-tier-1	\N	t
3490	TemplateField	310	Mail Order Rx (Tier 2 )	7	2016-01-25 09:09:01.775053	2016-01-25 09:09:01.775053		mail-order-rx-tier-2	\N	t
3491	TemplateField	310	Mail Order Rx (Tier 3)	8	2016-01-25 09:09:01.785174	2016-01-25 09:09:01.785174		mail-order-rx-tier-3	\N	t
3492	TemplateField	310	Mail Order Rx (Tier 4)	9	2016-01-25 09:09:01.795433	2016-01-25 09:09:01.795433		mail-order-rx-tier-4	\N	\N
3493	TemplateField	310	Mail Order Rx (Tier 5)	10	2016-01-25 09:09:01.805543	2016-01-25 09:09:01.805543		mail-order-rx-tier-5	\N	\N
3494	TemplateField	310	Speciality Medications - Mail Order	11	2016-01-25 09:09:01.815428	2016-01-25 09:09:01.815428		speciality-medications-mail-order	\N	\N
3495	TemplateField	310	Speciality Medications - Retail	12	2016-01-25 09:09:01.825275	2016-01-25 09:09:01.825275		speciality-medications-retail	\N	\N
3496	TemplateColumn	310	Coinsurance	2	2016-01-25 09:09:01.837158	2016-01-25 09:09:01.837158		coinsurance	\N	\N
3497	TemplateColumn	310	Subject to Rx Deductible	1	2016-01-25 09:09:01.846986	2016-01-25 09:09:01.846986		subject-to-rx-deductible	\N	\N
3498	TemplateField	311	Disease Management	1	2016-01-25 09:09:02.117984	2016-01-25 09:09:02.117984		disease-management	\N	\N
3499	TemplateColumn	311	Identification Methods	1	2016-01-25 09:09:02.131147	2016-01-25 09:09:02.131147		identification-methods	\N	\N
3500	TemplateColumn	311	Number of Conditions Tracked	2	2016-01-25 09:09:02.141815	2016-01-25 09:09:02.141815		number-of-conditions-tracked	\N	\N
3501	TemplateColumn	311	Outreach Methods	3	2016-01-25 09:09:02.152977	2016-01-25 09:09:02.152977		outreach-methods	\N	\N
3502	TemplateColumn	311	Report Frequency	4	2016-01-25 09:09:02.16366	2016-01-25 09:09:02.16366		report-frequency	\N	\N
3503	TemplateField	312	Integrated Wellness Plan	1	2016-01-25 09:09:02.22722	2016-01-25 09:09:02.22722		integrated-wellness-plan	\N	\N
3504	TemplateColumn	312	Wellness	1	2016-01-25 09:09:02.239625	2016-01-25 09:09:02.239625		wellness	\N	\N
3505	TemplateField	313	Lab	15	2016-01-25 09:09:02.277178	2016-01-25 09:09:02.277178		lab	\N	t
3506	TemplateField	313	General Radiology	22	2016-01-25 09:09:02.287614	2016-01-25 09:09:02.287614		general-radiology	\N	\N
3507	TemplateField	313	CT/MRI/PET Scans	29	2016-01-25 09:09:02.297633	2016-01-25 09:09:02.297633		ct-mri-pet-scans	\N	\N
3508	TemplateField	313	Annual Physical Exam	36	2016-01-25 09:09:02.31022	2016-01-25 09:09:02.31022		annual-physical-exam	\N	\N
3509	TemplateField	313	Well Child Exams	43	2016-01-25 09:09:02.320336	2016-01-25 09:09:02.320336		well-child-exams	\N	\N
3510	TemplateField	313	Pediatric Dental	50	2016-01-25 09:09:02.33064	2016-01-25 09:09:02.33064		pediatric-dental	\N	\N
3511	TemplateField	313	Hospitalization - Outpatient	64	2016-01-25 09:09:02.340824	2016-01-25 09:09:02.340824		hospitalization-outpatient	\N	t
3512	TemplateField	313	Surgery/Anesthesiology	71	2016-01-25 09:09:02.351357	2016-01-25 09:09:02.351357		surgery-anesthesiology	\N	\N
3513	TemplateField	313	Emergency Room	78	2016-01-25 09:09:02.361808	2016-01-25 09:09:02.361808		emergency-room	t	t
3514	TemplateField	313	Urgent Care	85	2016-01-25 09:09:02.372095	2016-01-25 09:09:02.372095		urgent-care	\N	t
3515	TemplateField	313	Mental Nervous - Inpatient Coverage	92	2016-01-25 09:09:02.382196	2016-01-25 09:09:02.382196		mental-nervous-inpatient-coverage	\N	t
3516	TemplateField	313	Mental Nervous - Outpatient Coverage	99	2016-01-25 09:09:02.392488	2016-01-25 09:09:02.392488		mental-nervous-outpatient-coverage	\N	t
3517	TemplateField	313	Substance Abuse - Inpatient Coverage	106	2016-01-25 09:09:02.403707	2016-01-25 09:09:02.403707		substance-abuse-inpatient-coverage	\N	t
3518	TemplateField	313	Substance Abuse - Outpatient Coverage	113	2016-01-25 09:09:02.414021	2016-01-25 09:09:02.414021		substance-abuse-outpatient-coverage	\N	t
3519	TemplateField	313	Physical Therapy - Inpatient Coverage	120	2016-01-25 09:09:02.424166	2016-01-25 09:09:02.424166		physical-therapy-inpatient-coverage	\N	\N
3520	TemplateField	313	Occupational Therapy - Inpatient Coverage	134	2016-01-25 09:09:02.434697	2016-01-25 09:09:02.434697		occupational-therapy-inpatient-coverage	\N	\N
3521	TemplateField	313	Occupational Therapy - Outpatient Coverage	141	2016-01-25 09:09:02.445001	2016-01-25 09:09:02.445001		occupational-therapy-outpatient-coverage	\N	\N
3522	TemplateField	313	Speech Therapy - Inpatient Coverage	148	2016-01-25 09:09:02.455727	2016-01-25 09:09:02.455727		speech-therapy-inpatient-coverage	\N	\N
3523	TemplateField	313	Speech Therapy - Outpatient Coverage	155	2016-01-25 09:09:02.466323	2016-01-25 09:09:02.466323		speech-therapy-outpatient-coverage	\N	\N
3524	TemplateField	313	Pregnancy & Maternity Care - Office Visits	162	2016-01-25 09:09:02.477035	2016-01-25 09:09:02.477035		pregnancy-maternity-care-office-visits	\N	\N
3525	TemplateField	313	Pregnancy & Maternity Care - Labor & Delivery	169	2016-01-25 09:09:02.487191	2016-01-25 09:09:02.487191		pregnancy-maternity-care-labor-delivery	\N	\N
3526	TemplateField	313	Chiropractic Services	176	2016-01-25 09:09:02.497127	2016-01-25 09:09:02.497127		chiropractic-services	\N	t
3527	TemplateField	313	Ambulance	183	2016-01-25 09:09:02.507086	2016-01-25 09:09:02.507086		ambulance	\N	\N
3528	TemplateField	313	Hospice	190	2016-01-25 09:09:02.517267	2016-01-25 09:09:02.517267		hospice	\N	\N
3529	TemplateField	313	Home Healthcare	197	2016-01-25 09:09:02.52738	2016-01-25 09:09:02.52738		home-healthcare	\N	\N
3530	TemplateField	313	Skilled Nursing	204	2016-01-25 09:09:02.537718	2016-01-25 09:09:02.537718		skilled-nursing	\N	\N
3531	TemplateField	313	Infertility Coverage	211	2016-01-25 09:09:02.548038	2016-01-25 09:09:02.548038		infertility-coverage	\N	t
3532	TemplateField	313	Prosthetics	218	2016-01-25 09:09:02.558604	2016-01-25 09:09:02.558604		prosthetics	\N	\N
3533	TemplateField	313	Durable Medical Equipment	225	2016-01-25 09:09:02.568668	2016-01-25 09:09:02.568668		durable-medical-equipment	\N	t
3534	TemplateField	313	Hearing Devices	232	2016-01-25 09:09:02.579186	2016-01-25 09:09:02.579186		hearing-devices	\N	\N
3535	TemplateField	313	Vision Exams	239	2016-01-25 09:09:02.589409	2016-01-25 09:09:02.589409		vision-exams	\N	\N
3536	TemplateField	313	Short Term Rehabilitation - Inpatient	246	2016-01-25 09:09:02.599374	2016-01-25 09:09:02.599374		short-term-rehabilitation-inpatient	\N	\N
3537	TemplateField	313	Short Term Rehabilitation - Outpatient	253	2016-01-25 09:09:02.609471	2016-01-25 09:09:02.609471		short-term-rehabilitation-outpatient	\N	\N
3538	TemplateField	313	Specialty Rx Coverage	267	2016-01-25 09:09:02.620439	2016-01-25 09:09:02.620439		specialty-rx-coverage	\N	\N
3539	TemplateField	313	Telemedicine	260	2016-01-25 09:09:02.630793	2016-01-25 09:09:02.630793		telemedicine	\N	\N
3540	TemplateField	313	Primary Care Office Visit	1	2016-01-25 09:09:02.641263	2016-01-25 09:09:02.641263		primary-care-office-visit	t	t
3541	TemplateField	313	Specialist Office Visit	8	2016-01-25 09:09:02.651568	2016-01-25 09:09:02.651568		specialist-office-visit	t	t
3542	TemplateField	313	Hospitalization - Inpatient	57	2016-01-25 09:09:02.662111	2016-01-25 09:09:02.662111		hospitalization-inpatient	t	t
3543	TemplateField	313	Physical Therapy - Outpatient Coverage	127	2016-01-25 09:09:02.672449	2016-01-25 09:09:02.672449		physical-therapy-outpatient-coverage	\N	t
3544	TemplateColumn	313	Subject to Deductible & Coinsurance	1	2016-01-25 09:09:02.685075	2016-01-25 09:09:02.685075		subject-to-deductible-coinsurance	\N	\N
3545	TemplateColumn	313	Copay	2	2016-01-25 09:09:02.695183	2016-01-25 09:09:02.695183		copay	\N	\N
3546	TemplateColumn	313	Limit	3	2016-01-25 09:09:02.705059	2016-01-25 09:09:02.705059		limit	\N	\N
3547	TemplateColumn	313	Copay Limit	4	2016-01-25 09:09:02.717581	2016-01-25 09:09:02.717581		copay-limit	\N	\N
3548	TemplateColumn	313	Separate Deductible	5	2016-01-25 09:09:02.727869	2016-01-25 09:09:02.727869		separate-deductible	\N	\N
3549	TemplateColumn	313	Separate Coinsurance	6	2016-01-25 09:09:02.738855	2016-01-25 09:09:02.738855		separate-coinsurance	\N	\N
3550	TemplateField	314	Subject to Medical Deductible	1	2016-01-25 09:09:05.227729	2016-01-25 09:09:05.227729		subject-to-medical-deductible	\N	t
3551	TemplateField	314	Subject to Medical Deductible & Coinsurance	2	2016-01-25 09:09:05.23783	2016-01-25 09:09:05.23783		subject-to-medical-deductible-coinsurance	\N	\N
3552	TemplateField	314	Network	3	2016-01-25 09:09:05.248007	2016-01-25 09:09:05.248007		network	\N	t
3553	TemplateField	314	Mandatory Generic	4	2016-01-25 09:09:05.258101	2016-01-25 09:09:05.258101		mandatory-generic	\N	\N
3554	TemplateField	314	Step Therapy/Precertification Applies	5	2016-01-25 09:09:05.268136	2016-01-25 09:09:05.268136		step-therapy-precertification-applies	\N	\N
3555	TemplateField	314	Oral Contraceptive	6	2016-01-25 09:09:05.278057	2016-01-25 09:09:05.278057		oral-contraceptive	\N	\N
3556	TemplateField	314	Are Prescriptions Covered Out of Network	7	2016-01-25 09:09:05.288093	2016-01-25 09:09:05.288093		are-prescriptions-covered-out-of-network	\N	\N
3557	TemplateColumn	314	Entries	1	2016-01-25 09:09:05.300295	2016-01-25 09:09:05.300295		entries	\N	\N
3558	TemplateField	315	Retail Rx (Tier 1)	1	2016-01-25 09:09:05.387959	2016-01-25 09:09:05.387959		retail-rx-tier-1	t	t
3559	TemplateField	315	Retail Rx (Tier 2)	2	2016-01-25 09:09:05.397963	2016-01-25 09:09:05.397963		retail-rx-tier-2	t	t
3560	TemplateField	315	Retail Rx (Tier 3)	3	2016-01-25 09:09:05.407926	2016-01-25 09:09:05.407926		retail-rx-tier-3	t	t
3561	TemplateField	315	Retail Rx (Tier 4)	4	2016-01-25 09:09:05.417911	2016-01-25 09:09:05.417911		retail-rx-tier-4	\N	\N
3562	TemplateField	315	Retail Rx (Tier 5)	5	2016-01-25 09:09:05.427913	2016-01-25 09:09:05.427913		retail-rx-tier-5	\N	\N
3563	TemplateField	315	Mail Order Rx (Tier 1)	6	2016-01-25 09:09:05.438076	2016-01-25 09:09:05.438076		mail-order-rx-tier-1	\N	t
3564	TemplateField	315	Mail Order Rx (Tier 2)	7	2016-01-25 09:09:05.447898	2016-01-25 09:09:05.447898		mail-order-rx-tier-2	\N	t
3565	TemplateField	315	Mail Order Rx (Tier 3)	8	2016-01-25 09:09:05.458253	2016-01-25 09:09:05.458253		mail-order-rx-tier-3	\N	t
3566	TemplateField	315	Mail Order Rx (Tier 4)	9	2016-01-25 09:09:05.468517	2016-01-25 09:09:05.468517		mail-order-rx-tier-4	\N	\N
3567	TemplateField	315	Mail Order Rx (Tier 5)	10	2016-01-25 09:09:05.478747	2016-01-25 09:09:05.478747		mail-order-rx-tier-5	\N	\N
3568	TemplateField	315	Specialty Medications -Mail Order	11	2016-01-25 09:09:05.488976	2016-01-25 09:09:05.488976		specialty-medications-mail-order	\N	\N
3569	TemplateField	315	Specialty Medications  -Retail	12	2016-01-25 09:09:05.499706	2016-01-25 09:09:05.499706		specialty-medications-retail	\N	\N
3570	TemplateColumn	315	Subject to Rx Deductible	1	2016-01-25 09:09:05.511724	2016-01-25 09:09:05.511724		subject-to-rx-deductible	\N	\N
3571	TemplateColumn	315	Copay	2	2016-01-25 09:09:05.521934	2016-01-25 09:09:05.521934		copay	\N	\N
3572	TemplateColumn	315	Coinsurance	3	2016-01-25 09:09:05.531814	2016-01-25 09:09:05.531814		coinsurance	\N	\N
3573	TemplateColumn	315	Per Script Coinsurance Limit	4	2016-01-25 09:09:05.541797	2016-01-25 09:09:05.541797		per-script-coinsurance-limit	\N	\N
3574	TemplateColumn	315	Per Script Coinsurance Minimum	5	2016-01-25 09:09:05.552095	2016-01-25 09:09:05.552095		per-script-coinsurance-minimum	\N	\N
3575	TemplateField	316	Disease Management	1	2016-01-25 09:09:06.239802	2016-01-25 09:09:06.239802		disease-management	\N	\N
3576	TemplateColumn	316	Identification Methods	1	2016-01-25 09:09:06.252373	2016-01-25 09:09:06.252373		identification-methods	\N	\N
3577	TemplateColumn	316	Number of Conditions Tracked	2	2016-01-25 09:09:06.262401	2016-01-25 09:09:06.262401		number-of-conditions-tracked	\N	\N
3578	TemplateColumn	316	Outreach Methods	3	2016-01-25 09:09:06.272462	2016-01-25 09:09:06.272462		outreach-methods	\N	\N
3579	TemplateColumn	316	Report Frequency	4	2016-01-25 09:09:06.282324	2016-01-25 09:09:06.282324		report-frequency	\N	\N
3580	TemplateField	317	Plan Type	1	2016-01-25 09:09:06.343477	2016-01-25 09:09:06.343477		plan-type	t	t
3581	TemplateField	317	Network	2	2016-01-25 09:09:06.353901	2016-01-25 09:09:06.353901		network	t	t
3582	TemplateField	317	Deductible Individual	4	2016-01-25 09:09:06.364172	2016-01-25 09:09:06.364172		deductible-individual	t	t
3583	TemplateField	317	Deductible Family	5	2016-01-25 09:09:06.374372	2016-01-25 09:09:06.374372		deductible-family	t	t
3584	TemplateField	317	Deductible - Family Multiple	6	2016-01-25 09:09:06.384331	2016-01-25 09:09:06.384331		deductible-family-multiple	\N	\N
3585	TemplateField	317	Deductible Format	7	2016-01-25 09:09:06.394255	2016-01-25 09:09:06.394255		deductible-format	\N	t
3586	TemplateField	317	Coinsurance	8	2016-01-25 09:09:06.404193	2016-01-25 09:09:06.404193		coinsurance	t	t
3587	TemplateField	317	Out-of-Pocket Max Individual (includes deductible)	9	2016-01-25 09:09:06.414246	2016-01-25 09:09:06.414246		out-of-pocket-max-individual-includes-deductible	t	t
3588	TemplateField	317	Out-of-Pocket Maximum - Family Multiple	11	2016-01-25 09:09:06.424358	2016-01-25 09:09:06.424358		out-of-pocket-maximum-family-multiple	\N	\N
3589	TemplateField	317	In & Out-of-Network Deductibles Cross Accumulation	12	2016-01-25 09:09:06.434369	2016-01-25 09:09:06.434369		in-out-of-network-deductibles-cross-accumulation	\N	t
3590	TemplateField	317	In & Out-of-Network OOP Maximum Cross Accumulation	13	2016-01-25 09:09:06.444582	2016-01-25 09:09:06.444582		in-out-of-network-oop-maximum-cross-accumulation	\N	t
3591	TemplateField	317	Annual Maximum	14	2016-01-25 09:09:06.454893	2016-01-25 09:09:06.454893		annual-maximum	\N	\N
3592	TemplateField	317	Gym Reimbursement	15	2016-01-25 09:09:06.464861	2016-01-25 09:09:06.464861		gym-reimbursement	\N	\N
3593	TemplateField	317	Customer Service Days/Hours	16	2016-01-25 09:09:06.474972	2016-01-25 09:09:06.474972		customer-service-days-hours	\N	\N
3594	TemplateField	317	OON Reimbursement Level	17	2016-01-25 09:09:06.485128	2016-01-25 09:09:06.485128		oon-reimbursement-level	t	t
3595	TemplateField	317	Nurseline	18	2016-01-25 09:09:06.495127	2016-01-25 09:09:06.495127		nurseline	\N	\N
3596	TemplateField	317	International Employees Covered	19	2016-01-25 09:09:06.505514	2016-01-25 09:09:06.505514		international-employees-covered	\N	\N
3597	TemplateField	317	Referrals Needed	3	2016-01-25 09:09:06.515477	2016-01-25 09:09:06.515477		referrals-needed	t	t
3598	TemplateField	317	Out-of-Pocket Max Family (includes deductible)	10	2016-01-25 09:09:06.525691	2016-01-25 09:09:06.525691		out-of-pocket-max-family-includes-deductible	t	t
3599	TemplateColumn	317	Entries	1	2016-01-25 09:09:06.538173	2016-01-25 09:09:06.538173		entries	\N	\N
3600	TemplateField	318	Rx Deductible - Individual	1	2016-01-25 09:09:06.754973	2016-01-25 09:09:06.754973		rx-deductible-individual	t	t
3601	TemplateField	318	Rx Deductible - Family	2	2016-01-25 09:09:06.764946	2016-01-25 09:09:06.764946		rx-deductible-family	t	t
3602	TemplateColumn	318	Deductible	1	2016-01-25 09:09:06.776891	2016-01-25 09:09:06.776891		deductible	\N	\N
3603	TemplateColumn	318	Per Member	2	2016-01-25 09:09:06.786706	2016-01-25 09:09:06.786706		per-member	\N	\N
3604	TemplateField	319	Integrated Wellness Plan	1	2016-01-25 09:09:06.84542	2016-01-25 09:09:06.84542		integrated-wellness-plan	\N	\N
3605	TemplateColumn	319	Entries	1	2016-01-25 09:09:06.858497	2016-01-25 09:09:06.858497		entries	\N	\N
3606	TemplateField	320	Primary Care Office Visit	1	2016-01-25 09:10:00.453155	2016-01-25 09:10:00.453155		primary-care-office-visit	t	t
3607	TemplateField	320	Specialist Office Visit	2	2016-01-25 09:10:00.464136	2016-01-25 09:10:00.464136		specialist-office-visit	t	t
3608	TemplateField	320	Lab	3	2016-01-25 09:10:00.474531	2016-01-25 09:10:00.474531		lab	\N	t
3609	TemplateField	320	General Radiology	4	2016-01-25 09:10:00.485491	2016-01-25 09:10:00.485491		general-radiology	\N	\N
3610	TemplateField	320	CT/MRI/PET Scans	5	2016-01-25 09:10:00.495845	2016-01-25 09:10:00.495845		ct-mri-pet-scans	\N	\N
3611	TemplateField	320	Annual Physical Exam	6	2016-01-25 09:10:00.506182	2016-01-25 09:10:00.506182		annual-physical-exam	\N	\N
3612	TemplateField	320	Well Child Exams	7	2016-01-25 09:10:00.5173	2016-01-25 09:10:00.5173		well-child-exams	\N	\N
3613	TemplateField	320	Pediatric Dental	8	2016-01-25 09:10:00.5277	2016-01-25 09:10:00.5277		pediatric-dental	\N	\N
3671	TemplateColumn	323	Spouse Eligible	5	2016-01-25 09:10:04.120868	2016-01-25 09:10:04.120868		spouse-eligible	\N	\N
3614	TemplateField	320	Hospitalization - Inpatient	9	2016-01-25 09:10:00.5387	2016-01-25 09:10:00.5387		hospitalization-inpatient	t	t
3615	TemplateField	320	Hospitalization - Outpatient	10	2016-01-25 09:10:00.549357	2016-01-25 09:10:00.549357		hospitalization-outpatient	\N	t
3616	TemplateField	320	Surgery/Anesthesiology	11	2016-01-25 09:10:00.559997	2016-01-25 09:10:00.559997		surgery-anesthesiology	\N	\N
3617	TemplateField	320	Emergency Room	12	2016-01-25 09:10:00.572197	2016-01-25 09:10:00.572197		emergency-room	t	t
3618	TemplateField	320	Urgent Care	13	2016-01-25 09:10:00.582859	2016-01-25 09:10:00.582859		urgent-care	\N	t
3619	TemplateField	320	Mental Nervous - Inpatient Coverage	14	2016-01-25 09:10:00.592973	2016-01-25 09:10:00.592973		mental-nervous-inpatient-coverage	\N	t
3620	TemplateField	320	Mental Nervous - Outpatient Coverage	15	2016-01-25 09:10:00.603509	2016-01-25 09:10:00.603509		mental-nervous-outpatient-coverage	\N	t
3621	TemplateField	320	Substance Abuse - Inpatient Coverage	16	2016-01-25 09:10:00.614261	2016-01-25 09:10:00.614261		substance-abuse-inpatient-coverage	\N	t
3622	TemplateField	320	Physical Therapy - Inpatient Coverage	18	2016-01-25 09:10:00.62461	2016-01-25 09:10:00.62461		physical-therapy-inpatient-coverage	\N	\N
3623	TemplateField	320	Physical Therapy - Outpatient Coverage	19	2016-01-25 09:10:00.634799	2016-01-25 09:10:00.634799		physical-therapy-outpatient-coverage	\N	t
3624	TemplateField	320	Occupational Therapy - Inpatient Coverage	20	2016-01-25 09:10:00.644995	2016-01-25 09:10:00.644995		occupational-therapy-inpatient-coverage	\N	\N
3625	TemplateField	320	Occupational Therapy - Outpatient Coverage	21	2016-01-25 09:10:00.654974	2016-01-25 09:10:00.654974		occupational-therapy-outpatient-coverage	\N	\N
3626	TemplateField	320	Speech Therapy - Inpatient Coverage	22	2016-01-25 09:10:00.665179	2016-01-25 09:10:00.665179		speech-therapy-inpatient-coverage	\N	\N
3627	TemplateField	320	Speech Therapy - Outpatient Coverage	23	2016-01-25 09:10:00.675168	2016-01-25 09:10:00.675168		speech-therapy-outpatient-coverage	\N	\N
3628	TemplateField	320	Pregnancy & Maternity Care - Office Visits	24	2016-01-25 09:10:00.685219	2016-01-25 09:10:00.685219		pregnancy-maternity-care-office-visits	\N	\N
3629	TemplateField	320	Pregnancy & Maternity Care - Labor & Delivery	25	2016-01-25 09:10:00.695185	2016-01-25 09:10:00.695185		pregnancy-maternity-care-labor-delivery	\N	\N
3630	TemplateField	320	Chiropractic Services	26	2016-01-25 09:10:00.705734	2016-01-25 09:10:00.705734		chiropractic-services	\N	t
3631	TemplateField	320	Ambulance	27	2016-01-25 09:10:00.716854	2016-01-25 09:10:00.716854		ambulance	\N	\N
3632	TemplateField	320	Hospice	28	2016-01-25 09:10:00.726853	2016-01-25 09:10:00.726853		hospice	\N	\N
3633	TemplateField	320	Home Healthcare	29	2016-01-25 09:10:00.738366	2016-01-25 09:10:00.738366		home-healthcare	\N	\N
3634	TemplateField	320	Skilled Nursing	30	2016-01-25 09:10:00.779156	2016-01-25 09:10:00.779156		skilled-nursing	\N	\N
3635	TemplateField	320	Infertility Coverage	31	2016-01-25 09:10:00.79643	2016-01-25 09:10:00.79643		infertility-coverage	\N	t
3636	TemplateField	320	Prosthetics	32	2016-01-25 09:10:00.816135	2016-01-25 09:10:00.816135		prosthetics	\N	\N
3637	TemplateField	320	Durable Medical Equipment	33	2016-01-25 09:10:00.833492	2016-01-25 09:10:00.833492		durable-medical-equipment	\N	t
3638	TemplateField	320	Hearing Devices	34	2016-01-25 09:10:00.848625	2016-01-25 09:10:00.848625		hearing-devices	\N	\N
3639	TemplateField	320	Vision Exams	35	2016-01-25 09:10:00.864965	2016-01-25 09:10:00.864965		vision-exams	\N	\N
3640	TemplateField	320	Short Term Rehabilitation- Inpatient	36	2016-01-25 09:10:00.879651	2016-01-25 09:10:00.879651		short-term-rehabilitation-inpatient	\N	\N
3641	TemplateField	320	Short Term Rehabilitation - Outpatient	37	2016-01-25 09:10:00.89122	2016-01-25 09:10:00.89122		short-term-rehabilitation-outpatient	\N	\N
3642	TemplateField	320	Telemedicine	38	2016-01-25 09:10:00.901872	2016-01-25 09:10:00.901872		telemedicine	\N	\N
3643	TemplateField	320	Speciality Rx Coverage	39	2016-01-25 09:10:00.912685	2016-01-25 09:10:00.912685		speciality-rx-coverage	\N	\N
3644	TemplateField	320	Substance Abuse - Outpatient Coverage	17	2016-01-25 09:10:00.924849	2016-01-25 09:10:00.924849		substance-abuse-outpatient-coverage	\N	t
3645	TemplateColumn	320	Subject to Deductible & Coinsurance	1	2016-01-25 09:10:00.938501	2016-01-25 09:10:00.938501		subject-to-deductible-coinsurance	\N	\N
3646	TemplateColumn	320	Limit	3	2016-01-25 09:10:00.949821	2016-01-25 09:10:00.949821		limit	\N	\N
3647	TemplateColumn	320	Copay Limit	4	2016-01-25 09:10:00.960914	2016-01-25 09:10:00.960914		copay-limit	\N	\N
3648	TemplateColumn	320	Separate Coinsurance	6	2016-01-25 09:10:00.972198	2016-01-25 09:10:00.972198		separate-coinsurance	\N	\N
3649	TemplateColumn	320	Separate Deductible	5	2016-01-25 09:10:00.982901	2016-01-25 09:10:00.982901		separate-deductible	\N	\N
3650	TemplateColumn	320	Copay	2	2016-01-25 09:10:00.993347	2016-01-25 09:10:00.993347		copay	\N	\N
3651	TemplateField	321	Subject to Medical Deductible	1	2016-01-25 09:10:03.758711	2016-01-25 09:10:03.758711		subject-to-medical-deductible	\N	t
3652	TemplateField	321	Subject to Medical Deductible & Coinsurance	2	2016-01-25 09:10:03.770343	2016-01-25 09:10:03.770343		subject-to-medical-deductible-coinsurance	\N	\N
3653	TemplateField	321	Network	3	2016-01-25 09:10:03.78241	2016-01-25 09:10:03.78241		network	\N	t
3654	TemplateField	321	Mandatory Generic	4	2016-01-25 09:10:03.79383	2016-01-25 09:10:03.79383		mandatory-generic	\N	\N
3655	TemplateField	321	Step Therapy/Precertification Applies	5	2016-01-25 09:10:03.804921	2016-01-25 09:10:03.804921		step-therapy-precertification-applies	\N	\N
3656	TemplateField	321	Oral Contraceptive	6	2016-01-25 09:10:03.81562	2016-01-25 09:10:03.81562		oral-contraceptive	\N	\N
3657	TemplateField	321	Prescriptions Covered Out of Network	7	2016-01-25 09:10:03.826397	2016-01-25 09:10:03.826397		prescriptions-covered-out-of-network	\N	\N
3658	TemplateColumn	321	Entries	1	2016-01-25 09:10:03.839937	2016-01-25 09:10:03.839937		entries	\N	\N
3659	TemplateField	322	Rx Deductible - Individual	1	2016-01-25 09:10:03.935846	2016-01-25 09:10:03.935846		rx-deductible-individual	t	t
3660	TemplateField	322	Rx Deductible - Family	2	2016-01-25 09:10:03.946931	2016-01-25 09:10:03.946931		rx-deductible-family	t	t
3661	TemplateColumn	322	Deductible	1	2016-01-25 09:10:03.960771	2016-01-25 09:10:03.960771		deductible	\N	\N
3662	TemplateColumn	322	Per Member	2	2016-01-25 09:10:03.971544	2016-01-25 09:10:03.971544		per-member	\N	\N
3663	TemplateField	323	Employee Eligibility Class #1	1	2016-01-25 09:10:04.033595	2016-01-25 09:10:04.033595		employee-eligibility-class-1	\N	\N
3664	TemplateField	323	Employee Eligibility Class #2	2	2016-01-25 09:10:04.043897	2016-01-25 09:10:04.043897		employee-eligibility-class-2	\N	\N
3665	TemplateField	323	Employee Eligibility Class #3	3	2016-01-25 09:10:04.054784	2016-01-25 09:10:04.054784		employee-eligibility-class-3	\N	\N
3666	TemplateField	323	Employee Eligibility Class #4	4	2016-01-25 09:10:04.065146	2016-01-25 09:10:04.065146		employee-eligibility-class-4	\N	\N
3667	TemplateColumn	323	Employee Class	1	2016-01-25 09:10:04.07812	2016-01-25 09:10:04.07812		employee-class	\N	\N
3668	TemplateColumn	323	Employee Classes	2	2016-01-25 09:10:04.088834	2016-01-25 09:10:04.088834		employee-classes	\N	\N
3669	TemplateColumn	323	Waiting Period	3	2016-01-25 09:10:04.099513	2016-01-25 09:10:04.099513		waiting-period	\N	\N
3670	TemplateColumn	323	Hours Requirement	4	2016-01-25 09:10:04.110589	2016-01-25 09:10:04.110589		hours-requirement	\N	\N
3672	TemplateColumn	323	Dependent Children Eligible	6	2016-01-25 09:10:04.131912	2016-01-25 09:10:04.131912		dependent-children-eligible	\N	\N
3673	TemplateColumn	323	Domestic Partner Eligible	7	2016-01-25 09:10:04.142107	2016-01-25 09:10:04.142107		domestic-partner-eligible	\N	\N
3674	TemplateColumn	323	Dependent Child Limiting Age	8	2016-01-25 09:10:04.152694	2016-01-25 09:10:04.152694		dependent-child-limiting-age	\N	\N
3675	TemplateColumn	323	Dependent Child Coverage Ends	9	2016-01-25 09:10:04.163145	2016-01-25 09:10:04.163145		dependent-child-coverage-ends	\N	\N
3676	TemplateField	324	Plan Type	1	2016-01-25 09:10:04.545224	2016-01-25 09:10:04.545224		plan-type	t	t
3677	TemplateField	324	Network	3	2016-01-25 09:10:04.555767	2016-01-25 09:10:04.555767		network	t	t
3678	TemplateField	324	Referrals Needed	5	2016-01-25 09:10:04.566465	2016-01-25 09:10:04.566465		referrals-needed	t	t
3679	TemplateField	324	Deductible Individual	7	2016-01-25 09:10:04.577312	2016-01-25 09:10:04.577312		deductible-individual	t	t
3680	TemplateField	324	Deductible Family	9	2016-01-25 09:10:04.587872	2016-01-25 09:10:04.587872		deductible-family	t	t
3681	TemplateField	324	Deductible - Family Multiple	11	2016-01-25 09:10:04.598661	2016-01-25 09:10:04.598661		deductible-family-multiple	\N	\N
3682	TemplateField	324	Deductible Format	13	2016-01-25 09:10:04.609099	2016-01-25 09:10:04.609099		deductible-format	\N	t
3683	TemplateField	324	Coinsurance	15	2016-01-25 09:10:04.62027	2016-01-25 09:10:04.62027		coinsurance	t	t
3684	TemplateField	324	Out-of-Pocket Max Family (includes deductible)	19	2016-01-25 09:10:04.631104	2016-01-25 09:10:04.631104		out-of-pocket-max-family-includes-deductible	t	t
3685	TemplateField	324	In & Out-of-Network Deductibles Cross Accumulation	23	2016-01-25 09:10:04.642253	2016-01-25 09:10:04.642253		in-out-of-network-deductibles-cross-accumulation	\N	t
3686	TemplateField	324	OON Reimbursement Level	27	2016-01-25 09:10:04.653155	2016-01-25 09:10:04.653155		oon-reimbursement-level	t	t
3687	TemplateField	324	Annual Maximum	29	2016-01-25 09:10:04.663909	2016-01-25 09:10:04.663909		annual-maximum	\N	\N
3688	TemplateField	324	Gym Reimbursement	31	2016-01-25 09:10:04.674355	2016-01-25 09:10:04.674355		gym-reimbursement	\N	\N
3689	TemplateField	324	Customer Service Days/Hours	33	2016-01-25 09:10:04.684842	2016-01-25 09:10:04.684842		customer-service-days-hours	\N	\N
3690	TemplateField	324	Nurseline	35	2016-01-25 09:10:04.69528	2016-01-25 09:10:04.69528		nurseline	\N	\N
3691	TemplateField	324	International Employees Covered	37	2016-01-25 09:10:04.705799	2016-01-25 09:10:04.705799		international-employees-covered	\N	\N
3692	TemplateField	324	Out-of-Pocket Max Individual (includes deductible)	17	2016-01-25 09:10:04.71666	2016-01-25 09:10:04.71666		out-of-pocket-max-individual-includes-deductible	t	t
3693	TemplateField	324	Out-of-Pocket Maximum - Family Multiple	21	2016-01-25 09:10:04.727539	2016-01-25 09:10:04.727539		out-of-pocket-maximum-family-multiple	\N	f
3694	TemplateField	324	In & Out-of-Network OOP Maximum Cross Accumulation	25	2016-01-25 09:10:04.738347	2016-01-25 09:10:04.738347		in-out-of-network-oop-maximum-cross-accumulation	\N	t
3695	TemplateColumn	324	Entries	1	2016-01-25 09:10:04.75296	2016-01-25 09:10:04.75296		entries	\N	\N
3696	TemplateField	325	Retail Rx (Tier 1)	1	2016-01-25 09:10:04.988499	2016-01-25 09:10:04.988499		retail-rx-tier-1	t	t
3697	TemplateField	325	Retail Rx (Tier 3)	13	2016-01-25 09:10:04.999556	2016-01-25 09:10:04.999556		retail-rx-tier-3	t	t
3698	TemplateField	325	Retail Rx (Tier 4)	19	2016-01-25 09:10:05.011109	2016-01-25 09:10:05.011109		retail-rx-tier-4	\N	\N
3699	TemplateField	325	Retail Rx (Tier 5)	25	2016-01-25 09:10:05.021372	2016-01-25 09:10:05.021372		retail-rx-tier-5	\N	\N
3700	TemplateField	325	Mail Order Rx (Tier 1)	31	2016-01-25 09:10:05.031834	2016-01-25 09:10:05.031834		mail-order-rx-tier-1	\N	t
3701	TemplateField	325	Specialty Medications -Mail Order	67	2016-01-25 09:10:05.042819	2016-01-25 09:10:05.042819		specialty-medications-mail-order	\N	f
3702	TemplateField	325	Specialty Medications  -Retail	61	2016-01-25 09:10:05.054615	2016-01-25 09:10:05.054615		specialty-medications-retail	\N	\N
3703	TemplateField	325	Mail Order Rx (Tier 3)	43	2016-01-25 09:10:05.065874	2016-01-25 09:10:05.065874		mail-order-rx-tier-3	\N	t
3704	TemplateField	325	Mail Order Rx (Tier 4)	49	2016-01-25 09:10:05.076537	2016-01-25 09:10:05.076537		mail-order-rx-tier-4	\N	\N
3705	TemplateField	325	Mail Order Rx (Tier 5)	55	2016-01-25 09:10:05.087059	2016-01-25 09:10:05.087059		mail-order-rx-tier-5	\N	\N
3706	TemplateField	325	Retail Rx (Tier 2)	7	2016-01-25 09:10:05.097428	2016-01-25 09:10:05.097428		retail-rx-tier-2	t	t
3707	TemplateField	325	Mail Order Rx (Tier 2)	37	2016-01-25 09:10:05.108113	2016-01-25 09:10:05.108113		mail-order-rx-tier-2	\N	t
3708	TemplateColumn	325	Copay	2	2016-01-25 09:10:05.1216	2016-01-25 09:10:05.1216		copay	\N	\N
3709	TemplateColumn	325	Coinsurance	3	2016-01-25 09:10:05.132029	2016-01-25 09:10:05.132029		coinsurance	\N	\N
3710	TemplateColumn	325	Per Script Coinsurance Limit	4	2016-01-25 09:10:05.142204	2016-01-25 09:10:05.142204		per-script-coinsurance-limit	\N	\N
3711	TemplateColumn	325	Per Script Coinsurance Minimum	5	2016-01-25 09:10:05.152869	2016-01-25 09:10:05.152869		per-script-coinsurance-minimum	\N	\N
3712	TemplateColumn	325	Subject To Rx Deductible	1	2016-01-25 09:10:05.163468	2016-01-25 09:10:05.163468		subject-to-rx-deductible	\N	\N
3713	TemplateField	326	Disease Management	1	2016-01-25 09:10:06.052906	2016-01-25 09:10:06.052906		disease-management	\N	\N
3714	TemplateColumn	326	Number of Conditions Tracked	2	2016-01-25 09:10:06.068069	2016-01-25 09:10:06.068069		number-of-conditions-tracked	\N	\N
3715	TemplateColumn	326	Outreach Methods	3	2016-01-25 09:10:06.078762	2016-01-25 09:10:06.078762		outreach-methods	\N	\N
3716	TemplateColumn	326	Report Frequency	4	2016-01-25 09:10:06.0891	2016-01-25 09:10:06.0891		report-frequency	\N	\N
3717	TemplateColumn	326	Identification Mehtods	1	2016-01-25 09:10:06.09991	2016-01-25 09:10:06.09991		identification-mehtods	\N	\N
3718	TemplateField	327	Integrated Wellness Plan	1	2016-01-25 09:10:06.164239	2016-01-25 09:10:06.164239		integrated-wellness-plan	\N	\N
3719	TemplateColumn	327	Entries	1	2016-01-25 09:10:06.176803	2016-01-25 09:10:06.176803		entries	\N	\N
3720	TemplateField	330	General Radiology	4	2016-01-25 09:10:06.248774	2016-01-25 09:10:06.248774		general-radiology	\N	\N
3721	TemplateField	330	CT/MRI/PET Scans	5	2016-01-25 09:10:06.259448	2016-01-25 09:10:06.259448		ct-mri-pet-scans	\N	\N
3722	TemplateField	330	Annual Physical Exam	6	2016-01-25 09:10:06.270074	2016-01-25 09:10:06.270074		annual-physical-exam	\N	\N
3723	TemplateField	330	Well Child Exams	7	2016-01-25 09:10:06.280409	2016-01-25 09:10:06.280409		well-child-exams	\N	\N
3724	TemplateField	330	Pediatric Dental	8	2016-01-25 09:10:06.290328	2016-01-25 09:10:06.290328		pediatric-dental	\N	\N
3725	TemplateField	330	Hospitalization - Inpatient	9	2016-01-25 09:10:06.300705	2016-01-25 09:10:06.300705		hospitalization-inpatient	t	t
3726	TemplateField	330	Hospitalization - Outpatient	10	2016-01-25 09:10:06.311251	2016-01-25 09:10:06.311251		hospitalization-outpatient	\N	t
3727	TemplateField	330	Surgery/Anesthesiology	11	2016-01-25 09:10:06.321536	2016-01-25 09:10:06.321536		surgery-anesthesiology	\N	\N
3728	TemplateField	330	Emergency Room	12	2016-01-25 09:10:06.331947	2016-01-25 09:10:06.331947		emergency-room	t	t
3729	TemplateField	330	Specialist Office Visit	2	2016-01-25 09:10:06.342375	2016-01-25 09:10:06.342375		specialist-office-visit	t	t
3730	TemplateField	330	Lab	3	2016-01-25 09:10:06.353022	2016-01-25 09:10:06.353022		lab	\N	t
3731	TemplateField	330	Urgent Care	13	2016-01-25 09:10:06.363787	2016-01-25 09:10:06.363787		urgent-care	\N	t
3732	TemplateField	330	Mental Nervous - Outpatient Coverage	15	2016-01-25 09:10:06.374382	2016-01-25 09:10:06.374382		mental-nervous-outpatient-coverage	\N	t
3733	TemplateField	330	Substance Abuse - Inpatient Coverage	16	2016-01-25 09:10:06.38485	2016-01-25 09:10:06.38485		substance-abuse-inpatient-coverage	\N	t
3734	TemplateField	330	Substance Abuse - Outpatient Coverage	17	2016-01-25 09:10:06.395188	2016-01-25 09:10:06.395188		substance-abuse-outpatient-coverage	\N	t
3735	TemplateField	330	Physical Therapy - Inpatient Coverage	18	2016-01-25 09:10:06.405275	2016-01-25 09:10:06.405275		physical-therapy-inpatient-coverage	\N	\N
3736	TemplateField	330	Physical Therapy - Outpatient Coverage	19	2016-01-25 09:10:06.416028	2016-01-25 09:10:06.416028		physical-therapy-outpatient-coverage	\N	t
3737	TemplateField	330	Occupational Therapy - Inpatient Coverage	20	2016-01-25 09:10:06.426701	2016-01-25 09:10:06.426701		occupational-therapy-inpatient-coverage	\N	\N
3738	TemplateField	330	Occupational Therapy - Outpatient Coverage	21	2016-01-25 09:10:06.436863	2016-01-25 09:10:06.436863		occupational-therapy-outpatient-coverage	\N	\N
3739	TemplateField	330	Speech Therapy - Inpatient Coverage	22	2016-01-25 09:10:06.447605	2016-01-25 09:10:06.447605		speech-therapy-inpatient-coverage	\N	\N
3740	TemplateField	330	Speech Therapy - Outpatient Coverage	23	2016-01-25 09:10:06.457753	2016-01-25 09:10:06.457753		speech-therapy-outpatient-coverage	\N	\N
3741	TemplateField	330	Pregnancy & Maternity Care - Office Visits	24	2016-01-25 09:10:06.468019	2016-01-25 09:10:06.468019		pregnancy-maternity-care-office-visits	\N	\N
3742	TemplateField	330	Pregnancy & Maternity Care - Labor & Delivery	25	2016-01-25 09:10:06.478254	2016-01-25 09:10:06.478254		pregnancy-maternity-care-labor-delivery	\N	\N
3743	TemplateField	330	Chiropractic Services	26	2016-01-25 09:10:06.488765	2016-01-25 09:10:06.488765		chiropractic-services	\N	t
3744	TemplateField	330	Ambulance	27	2016-01-25 09:10:06.499112	2016-01-25 09:10:06.499112		ambulance	\N	\N
3745	TemplateField	330	Hospice	28	2016-01-25 09:10:06.509501	2016-01-25 09:10:06.509501		hospice	\N	\N
3746	TemplateField	330	Home Healthcare	29	2016-01-25 09:10:06.519955	2016-01-25 09:10:06.519955		home-healthcare	\N	\N
3747	TemplateField	330	Skilled Nursing	30	2016-01-25 09:10:06.529999	2016-01-25 09:10:06.529999		skilled-nursing	\N	\N
3748	TemplateField	330	Infertility Coverage	31	2016-01-25 09:10:06.539971	2016-01-25 09:10:06.539971		infertility-coverage	\N	t
3749	TemplateField	330	Prosthetics	32	2016-01-25 09:10:06.550556	2016-01-25 09:10:06.550556		prosthetics	\N	\N
3750	TemplateField	330	Durable Medical Equipment	33	2016-01-25 09:10:06.560881	2016-01-25 09:10:06.560881		durable-medical-equipment	\N	t
3751	TemplateField	330	Hearing Devices	34	2016-01-25 09:10:06.571195	2016-01-25 09:10:06.571195		hearing-devices	\N	\N
3752	TemplateField	330	Vision Exams	35	2016-01-25 09:10:06.581843	2016-01-25 09:10:06.581843		vision-exams	\N	\N
3753	TemplateField	330	Short Term Rehabilitation - Inpatient	36	2016-01-25 09:10:06.592118	2016-01-25 09:10:06.592118		short-term-rehabilitation-inpatient	\N	\N
3754	TemplateField	330	Short Term Rehabilitation - Outpatient	37	2016-01-25 09:10:06.602264	2016-01-25 09:10:06.602264		short-term-rehabilitation-outpatient	\N	\N
3755	TemplateField	330	Telemedicine	38	2016-01-25 09:10:06.612576	2016-01-25 09:10:06.612576		telemedicine	\N	\N
3756	TemplateField	330	Specialty Rx Coverage	39	2016-01-25 09:10:06.623685	2016-01-25 09:10:06.623685		specialty-rx-coverage	\N	\N
3757	TemplateField	330	Primary Care Office Visit	1	2016-01-25 09:10:06.634277	2016-01-25 09:10:06.634277		primary-care-office-visit	t	t
3758	TemplateField	330	Mental Nervous - Inpatient Coverage	14	2016-01-25 09:10:06.644938	2016-01-25 09:10:06.644938		mental-nervous-inpatient-coverage	\N	t
3759	TemplateColumn	330	Subject to Deductible & Coinsurance	1	2016-01-25 09:10:06.657267	2016-01-25 09:10:06.657267		subject-to-deductible-coinsurance	\N	\N
3760	TemplateColumn	330	Limit	2	2016-01-25 09:10:06.667486	2016-01-25 09:10:06.667486		limit	\N	\N
3761	TemplateColumn	330	Separate Deductible	3	2016-01-25 09:10:06.677546	2016-01-25 09:10:06.677546		separate-deductible	\N	\N
3762	TemplateColumn	330	Separate Coinsurance	4	2016-01-25 09:10:06.688021	2016-01-25 09:10:06.688021		separate-coinsurance	\N	\N
3763	TemplateField	331	Plan Type	1	2016-01-25 09:10:08.392477	2016-01-25 09:10:08.392477		plan-type	t	t
3764	TemplateField	331	Network	2	2016-01-25 09:10:08.403191	2016-01-25 09:10:08.403191		network	t	t
3765	TemplateField	331	Referrals Needed	3	2016-01-25 09:10:08.413998	2016-01-25 09:10:08.413998		referrals-needed	t	t
3766	TemplateField	331	Deductible Individual	4	2016-01-25 09:10:08.425072	2016-01-25 09:10:08.425072		deductible-individual	t	t
3767	TemplateField	331	Deductible Family	5	2016-01-25 09:10:08.436075	2016-01-25 09:10:08.436075		deductible-family	t	t
3768	TemplateField	331	Deductible - Family Multiple	6	2016-01-25 09:10:08.447012	2016-01-25 09:10:08.447012		deductible-family-multiple	\N	\N
3769	TemplateField	331	Deductible Format	7	2016-01-25 09:10:08.45748	2016-01-25 09:10:08.45748		deductible-format	\N	t
3770	TemplateField	331	Coinsurance	8	2016-01-25 09:10:08.468145	2016-01-25 09:10:08.468145		coinsurance	t	t
3771	TemplateField	331	Out-of-Pocket Max Individual (includes deductible)	9	2016-01-25 09:10:08.478709	2016-01-25 09:10:08.478709		out-of-pocket-max-individual-includes-deductible	t	t
3772	TemplateField	331	Out-of-Pocket Max Family (includes deductible)	10	2016-01-25 09:10:08.48977	2016-01-25 09:10:08.48977		out-of-pocket-max-family-includes-deductible	t	t
3773	TemplateField	331	Out-of-Pocket Maximum - Family Multiple	11	2016-01-25 09:10:08.500559	2016-01-25 09:10:08.500559		out-of-pocket-maximum-family-multiple	\N	\N
3774	TemplateField	331	In & Out-of-Network Deductibles Cross Accumulation	12	2016-01-25 09:10:08.511244	2016-01-25 09:10:08.511244		in-out-of-network-deductibles-cross-accumulation	\N	t
3775	TemplateField	331	In & Out-of-Network OOP Maximum Cross Accumulation	13	2016-01-25 09:10:08.521882	2016-01-25 09:10:08.521882		in-out-of-network-oop-maximum-cross-accumulation	\N	t
3776	TemplateField	331	OON Reimbursement Level	14	2016-01-25 09:10:08.533105	2016-01-25 09:10:08.533105		oon-reimbursement-level	t	t
3777	TemplateField	331	Annual Maximum	15	2016-01-25 09:10:08.551152	2016-01-25 09:10:08.551152		annual-maximum	\N	\N
3778	TemplateField	331	Gym Reimbursement	16	2016-01-25 09:10:08.561853	2016-01-25 09:10:08.561853		gym-reimbursement	\N	\N
3779	TemplateField	331	Customer Service Days/Hours	17	2016-01-25 09:10:08.572408	2016-01-25 09:10:08.572408		customer-service-days-hours	\N	\N
3780	TemplateField	331	Nurseline	18	2016-01-25 09:10:08.583147	2016-01-25 09:10:08.583147		nurseline	\N	\N
3781	TemplateField	331	International Employees Covered	19	2016-01-25 09:10:08.593882	2016-01-25 09:10:08.593882		international-employees-covered	\N	\N
3782	TemplateColumn	331	Entries	1	2016-01-25 09:10:08.606304	2016-01-25 09:10:08.606304		entries	\N	\N
3783	TemplateField	332	Subject to Medical Deductible	1	2016-01-25 09:10:08.848712	2016-01-25 09:10:08.848712		subject-to-medical-deductible	\N	t
3784	TemplateField	332	Subject to Medical Deductible & Coinsurance	2	2016-01-25 09:10:08.860067	2016-01-25 09:10:08.860067		subject-to-medical-deductible-coinsurance	\N	\N
3785	TemplateField	332	Network	3	2016-01-25 09:10:08.870879	2016-01-25 09:10:08.870879		network	\N	t
3786	TemplateField	332	Step Therapy/Precertification Applies	4	2016-01-25 09:10:08.881465	2016-01-25 09:10:08.881465		step-therapy-precertification-applies	\N	\N
3787	TemplateField	332	Mandatory Generic	5	2016-01-25 09:10:08.891992	2016-01-25 09:10:08.891992		mandatory-generic	\N	\N
3788	TemplateField	332	Oral Contraceptive	6	2016-01-25 09:10:08.902714	2016-01-25 09:10:08.902714		oral-contraceptive	\N	\N
3789	TemplateField	332	Are Prescriptions Covered Out of Network	7	2016-01-25 09:10:08.913218	2016-01-25 09:10:08.913218		are-prescriptions-covered-out-of-network	\N	\N
3790	TemplateColumn	332	Entries	1	2016-01-25 09:10:08.925991	2016-01-25 09:10:08.925991		entries	\N	\N
3791	TemplateField	333	Rx Deductible - Family	1	2016-01-25 09:10:09.01976	2016-01-25 09:10:09.01976		rx-deductible-family	t	t
3792	TemplateField	333	Rx Deductible - Individual	2	2016-01-25 09:10:09.030373	2016-01-25 09:10:09.030373		rx-deductible-individual	t	t
3793	TemplateColumn	333	Deductible	1	2016-01-25 09:10:09.04286	2016-01-25 09:10:09.04286		deductible	\N	\N
3794	TemplateColumn	333	Per Member	2	2016-01-25 09:10:09.054728	2016-01-25 09:10:09.054728		per-member	\N	\N
3795	TemplateField	334	Retail Rx (Tier 1)	1	2016-01-25 09:10:09.120418	2016-01-25 09:10:09.120418		retail-rx-tier-1	t	t
3796	TemplateField	334	Retail Rx (Tier 2)	2	2016-01-25 09:10:09.131429	2016-01-25 09:10:09.131429		retail-rx-tier-2	t	t
3797	TemplateField	334	Retail Rx (Tier 3)	3	2016-01-25 09:10:09.14283	2016-01-25 09:10:09.14283		retail-rx-tier-3	t	t
3798	TemplateField	334	Retail Rx (Tier 4)	4	2016-01-25 09:10:09.153303	2016-01-25 09:10:09.153303		retail-rx-tier-4	\N	\N
3799	TemplateField	334	Retail Rx (Tier 5)	5	2016-01-25 09:10:09.164001	2016-01-25 09:10:09.164001		retail-rx-tier-5	\N	\N
3800	TemplateField	334	Mail Order Rx (Tier 1)	6	2016-01-25 09:10:09.17461	2016-01-25 09:10:09.17461		mail-order-rx-tier-1	\N	t
3801	TemplateField	334	Mail Order Rx (Tier 2 )	7	2016-01-25 09:10:09.185966	2016-01-25 09:10:09.185966		mail-order-rx-tier-2	\N	t
3802	TemplateField	334	Mail Order Rx (Tier 3)	8	2016-01-25 09:10:09.196856	2016-01-25 09:10:09.196856		mail-order-rx-tier-3	\N	t
3803	TemplateField	334	Mail Order Rx (Tier 4)	9	2016-01-25 09:10:09.207818	2016-01-25 09:10:09.207818		mail-order-rx-tier-4	\N	\N
3804	TemplateField	334	Mail Order Rx (Tier 5)	10	2016-01-25 09:10:09.218405	2016-01-25 09:10:09.218405		mail-order-rx-tier-5	\N	\N
3805	TemplateField	334	Speciality Medications - Mail Order	11	2016-01-25 09:10:09.230634	2016-01-25 09:10:09.230634		speciality-medications-mail-order	\N	\N
3806	TemplateField	334	Speciality Medications - Retail	12	2016-01-25 09:10:09.241198	2016-01-25 09:10:09.241198		speciality-medications-retail	\N	\N
3807	TemplateColumn	334	Coinsurance	2	2016-01-25 09:10:09.256805	2016-01-25 09:10:09.256805		coinsurance	\N	\N
3808	TemplateColumn	334	Subject to Rx Deductible	1	2016-01-25 09:10:09.268146	2016-01-25 09:10:09.268146		subject-to-rx-deductible	\N	\N
3809	TemplateField	335	Disease Management	1	2016-01-25 09:10:09.54213	2016-01-25 09:10:09.54213		disease-management	\N	\N
3810	TemplateColumn	335	Identification Methods	1	2016-01-25 09:10:09.55498	2016-01-25 09:10:09.55498		identification-methods	\N	\N
3811	TemplateColumn	335	Number of Conditions Tracked	2	2016-01-25 09:10:09.565838	2016-01-25 09:10:09.565838		number-of-conditions-tracked	\N	\N
3812	TemplateColumn	335	Outreach Methods	3	2016-01-25 09:10:09.576884	2016-01-25 09:10:09.576884		outreach-methods	\N	\N
3813	TemplateColumn	335	Report Frequency	4	2016-01-25 09:10:09.588085	2016-01-25 09:10:09.588085		report-frequency	\N	\N
3814	TemplateField	336	Integrated Wellness Plan	1	2016-01-25 09:10:09.654787	2016-01-25 09:10:09.654787		integrated-wellness-plan	\N	\N
3815	TemplateColumn	336	Wellness	1	2016-01-25 09:10:09.668407	2016-01-25 09:10:09.668407		wellness	\N	\N
3816	TemplateField	337	Lab	15	2016-01-25 09:10:09.712234	2016-01-25 09:10:09.712234		lab	\N	t
3817	TemplateField	337	General Radiology	22	2016-01-25 09:10:09.731568	2016-01-25 09:10:09.731568		general-radiology	\N	\N
3818	TemplateField	337	CT/MRI/PET Scans	29	2016-01-25 09:10:09.742544	2016-01-25 09:10:09.742544		ct-mri-pet-scans	\N	\N
3819	TemplateField	337	Annual Physical Exam	36	2016-01-25 09:10:09.753187	2016-01-25 09:10:09.753187		annual-physical-exam	\N	\N
3820	TemplateField	337	Well Child Exams	43	2016-01-25 09:10:09.764166	2016-01-25 09:10:09.764166		well-child-exams	\N	\N
3821	TemplateField	337	Pediatric Dental	50	2016-01-25 09:10:09.774836	2016-01-25 09:10:09.774836		pediatric-dental	\N	\N
3822	TemplateField	337	Hospitalization - Outpatient	64	2016-01-25 09:10:09.78571	2016-01-25 09:10:09.78571		hospitalization-outpatient	\N	t
3823	TemplateField	337	Surgery/Anesthesiology	71	2016-01-25 09:10:09.796355	2016-01-25 09:10:09.796355		surgery-anesthesiology	\N	\N
3824	TemplateField	337	Emergency Room	78	2016-01-25 09:10:09.80663	2016-01-25 09:10:09.80663		emergency-room	t	t
3825	TemplateField	337	Urgent Care	85	2016-01-25 09:10:09.817589	2016-01-25 09:10:09.817589		urgent-care	\N	t
3826	TemplateField	337	Mental Nervous - Inpatient Coverage	92	2016-01-25 09:10:09.82828	2016-01-25 09:10:09.82828		mental-nervous-inpatient-coverage	\N	t
3827	TemplateField	337	Mental Nervous - Outpatient Coverage	99	2016-01-25 09:10:09.838435	2016-01-25 09:10:09.838435		mental-nervous-outpatient-coverage	\N	t
3828	TemplateField	337	Substance Abuse - Inpatient Coverage	106	2016-01-25 09:10:09.848643	2016-01-25 09:10:09.848643		substance-abuse-inpatient-coverage	\N	t
3829	TemplateField	337	Substance Abuse - Outpatient Coverage	113	2016-01-25 09:10:09.858886	2016-01-25 09:10:09.858886		substance-abuse-outpatient-coverage	\N	t
3830	TemplateField	337	Physical Therapy - Inpatient Coverage	120	2016-01-25 09:10:09.86945	2016-01-25 09:10:09.86945		physical-therapy-inpatient-coverage	\N	\N
3831	TemplateField	337	Occupational Therapy - Inpatient Coverage	134	2016-01-25 09:10:09.879758	2016-01-25 09:10:09.879758		occupational-therapy-inpatient-coverage	\N	\N
3832	TemplateField	337	Occupational Therapy - Outpatient Coverage	141	2016-01-25 09:10:09.889785	2016-01-25 09:10:09.889785		occupational-therapy-outpatient-coverage	\N	\N
3833	TemplateField	337	Speech Therapy - Inpatient Coverage	148	2016-01-25 09:10:09.9003	2016-01-25 09:10:09.9003		speech-therapy-inpatient-coverage	\N	\N
3834	TemplateField	337	Speech Therapy - Outpatient Coverage	155	2016-01-25 09:10:09.910536	2016-01-25 09:10:09.910536		speech-therapy-outpatient-coverage	\N	\N
3835	TemplateField	337	Pregnancy & Maternity Care - Office Visits	162	2016-01-25 09:10:09.921023	2016-01-25 09:10:09.921023		pregnancy-maternity-care-office-visits	\N	\N
3836	TemplateField	337	Pregnancy & Maternity Care - Labor & Delivery	169	2016-01-25 09:10:09.931337	2016-01-25 09:10:09.931337		pregnancy-maternity-care-labor-delivery	\N	\N
3837	TemplateField	337	Chiropractic Services	176	2016-01-25 09:10:09.941553	2016-01-25 09:10:09.941553		chiropractic-services	\N	t
3838	TemplateField	337	Ambulance	183	2016-01-25 09:10:09.951904	2016-01-25 09:10:09.951904		ambulance	\N	\N
3839	TemplateField	337	Hospice	190	2016-01-25 09:10:09.962106	2016-01-25 09:10:09.962106		hospice	\N	\N
3840	TemplateField	337	Home Healthcare	197	2016-01-25 09:10:09.972488	2016-01-25 09:10:09.972488		home-healthcare	\N	\N
3841	TemplateField	337	Skilled Nursing	204	2016-01-25 09:10:09.982762	2016-01-25 09:10:09.982762		skilled-nursing	\N	\N
3842	TemplateField	337	Infertility Coverage	211	2016-01-25 09:10:09.993272	2016-01-25 09:10:09.993272		infertility-coverage	\N	t
3843	TemplateField	337	Prosthetics	218	2016-01-25 09:10:10.004208	2016-01-25 09:10:10.004208		prosthetics	\N	\N
3844	TemplateField	337	Durable Medical Equipment	225	2016-01-25 09:10:10.015137	2016-01-25 09:10:10.015137		durable-medical-equipment	\N	t
3845	TemplateField	337	Hearing Devices	232	2016-01-25 09:10:10.025483	2016-01-25 09:10:10.025483		hearing-devices	\N	\N
3846	TemplateField	337	Vision Exams	239	2016-01-25 09:10:10.038405	2016-01-25 09:10:10.038405		vision-exams	\N	\N
3906	TemplateField	341	Nurseline	18	2016-01-25 09:10:14.197343	2016-01-25 09:10:14.197343		nurseline	\N	\N
3847	TemplateField	337	Short Term Rehabilitation - Inpatient	246	2016-01-25 09:10:10.049047	2016-01-25 09:10:10.049047		short-term-rehabilitation-inpatient	\N	\N
3848	TemplateField	337	Short Term Rehabilitation - Outpatient	253	2016-01-25 09:10:10.05948	2016-01-25 09:10:10.05948		short-term-rehabilitation-outpatient	\N	\N
3849	TemplateField	337	Specialty Rx Coverage	267	2016-01-25 09:10:10.069641	2016-01-25 09:10:10.069641		specialty-rx-coverage	\N	\N
3850	TemplateField	337	Telemedicine	260	2016-01-25 09:10:10.080502	2016-01-25 09:10:10.080502		telemedicine	\N	\N
3851	TemplateField	337	Primary Care Office Visit	1	2016-01-25 09:10:10.090722	2016-01-25 09:10:10.090722		primary-care-office-visit	t	t
3852	TemplateField	337	Specialist Office Visit	8	2016-01-25 09:10:10.100754	2016-01-25 09:10:10.100754		specialist-office-visit	t	t
3853	TemplateField	337	Hospitalization - Inpatient	57	2016-01-25 09:10:10.111171	2016-01-25 09:10:10.111171		hospitalization-inpatient	t	t
3854	TemplateField	337	Physical Therapy - Outpatient Coverage	127	2016-01-25 09:10:10.121804	2016-01-25 09:10:10.121804		physical-therapy-outpatient-coverage	\N	t
3855	TemplateColumn	337	Subject to Deductible & Coinsurance	1	2016-01-25 09:10:10.134265	2016-01-25 09:10:10.134265		subject-to-deductible-coinsurance	\N	\N
3856	TemplateColumn	337	Copay	2	2016-01-25 09:10:10.144444	2016-01-25 09:10:10.144444		copay	\N	\N
3857	TemplateColumn	337	Limit	3	2016-01-25 09:10:10.154653	2016-01-25 09:10:10.154653		limit	\N	\N
3858	TemplateColumn	337	Copay Limit	4	2016-01-25 09:10:10.165138	2016-01-25 09:10:10.165138		copay-limit	\N	\N
3859	TemplateColumn	337	Separate Deductible	5	2016-01-25 09:10:10.175778	2016-01-25 09:10:10.175778		separate-deductible	\N	\N
3860	TemplateColumn	337	Separate Coinsurance	6	2016-01-25 09:10:10.186101	2016-01-25 09:10:10.186101		separate-coinsurance	\N	\N
3861	TemplateField	338	Subject to Medical Deductible	1	2016-01-25 09:10:12.844589	2016-01-25 09:10:12.844589		subject-to-medical-deductible	\N	t
3862	TemplateField	338	Subject to Medical Deductible & Coinsurance	2	2016-01-25 09:10:12.85671	2016-01-25 09:10:12.85671		subject-to-medical-deductible-coinsurance	\N	\N
3863	TemplateField	338	Network	3	2016-01-25 09:10:12.86767	2016-01-25 09:10:12.86767		network	\N	t
3864	TemplateField	338	Mandatory Generic	4	2016-01-25 09:10:12.878467	2016-01-25 09:10:12.878467		mandatory-generic	\N	\N
3865	TemplateField	338	Step Therapy/Precertification Applies	5	2016-01-25 09:10:12.889268	2016-01-25 09:10:12.889268		step-therapy-precertification-applies	\N	\N
3866	TemplateField	338	Oral Contraceptive	6	2016-01-25 09:10:12.89943	2016-01-25 09:10:12.89943		oral-contraceptive	\N	\N
3867	TemplateField	338	Are Prescriptions Covered Out of Network	7	2016-01-25 09:10:12.911157	2016-01-25 09:10:12.911157		are-prescriptions-covered-out-of-network	\N	\N
3868	TemplateColumn	338	Entries	1	2016-01-25 09:10:12.924024	2016-01-25 09:10:12.924024		entries	\N	\N
3869	TemplateField	339	Retail Rx (Tier 1)	1	2016-01-25 09:10:13.016243	2016-01-25 09:10:13.016243		retail-rx-tier-1	t	t
3870	TemplateField	339	Retail Rx (Tier 2)	2	2016-01-25 09:10:13.026996	2016-01-25 09:10:13.026996		retail-rx-tier-2	t	t
3871	TemplateField	339	Retail Rx (Tier 3)	3	2016-01-25 09:10:13.037323	2016-01-25 09:10:13.037323		retail-rx-tier-3	t	t
3872	TemplateField	339	Retail Rx (Tier 4)	4	2016-01-25 09:10:13.047797	2016-01-25 09:10:13.047797		retail-rx-tier-4	\N	\N
3873	TemplateField	339	Retail Rx (Tier 5)	5	2016-01-25 09:10:13.058866	2016-01-25 09:10:13.058866		retail-rx-tier-5	\N	\N
3874	TemplateField	339	Mail Order Rx (Tier 1)	6	2016-01-25 09:10:13.079473	2016-01-25 09:10:13.079473		mail-order-rx-tier-1	\N	t
3875	TemplateField	339	Mail Order Rx (Tier 2)	7	2016-01-25 09:10:13.089817	2016-01-25 09:10:13.089817		mail-order-rx-tier-2	\N	t
3876	TemplateField	339	Mail Order Rx (Tier 3)	8	2016-01-25 09:10:13.100489	2016-01-25 09:10:13.100489		mail-order-rx-tier-3	\N	t
3877	TemplateField	339	Mail Order Rx (Tier 4)	9	2016-01-25 09:10:13.11108	2016-01-25 09:10:13.11108		mail-order-rx-tier-4	\N	\N
3878	TemplateField	339	Mail Order Rx (Tier 5)	10	2016-01-25 09:10:13.121643	2016-01-25 09:10:13.121643		mail-order-rx-tier-5	\N	\N
3879	TemplateField	339	Specialty Medications -Mail Order	11	2016-01-25 09:10:13.132483	2016-01-25 09:10:13.132483		specialty-medications-mail-order	\N	\N
3880	TemplateField	339	Specialty Medications  -Retail	12	2016-01-25 09:10:13.143508	2016-01-25 09:10:13.143508		specialty-medications-retail	\N	\N
3881	TemplateColumn	339	Subject to Rx Deductible	1	2016-01-25 09:10:13.156059	2016-01-25 09:10:13.156059		subject-to-rx-deductible	\N	\N
3882	TemplateColumn	339	Copay	2	2016-01-25 09:10:13.166476	2016-01-25 09:10:13.166476		copay	\N	\N
3883	TemplateColumn	339	Coinsurance	3	2016-01-25 09:10:13.176597	2016-01-25 09:10:13.176597		coinsurance	\N	\N
3884	TemplateColumn	339	Per Script Coinsurance Limit	4	2016-01-25 09:10:13.186917	2016-01-25 09:10:13.186917		per-script-coinsurance-limit	\N	\N
3885	TemplateColumn	339	Per Script Coinsurance Minimum	5	2016-01-25 09:10:13.197413	2016-01-25 09:10:13.197413		per-script-coinsurance-minimum	\N	\N
3886	TemplateField	340	Disease Management	1	2016-01-25 09:10:13.918205	2016-01-25 09:10:13.918205		disease-management	\N	\N
3887	TemplateColumn	340	Identification Methods	1	2016-01-25 09:10:13.931604	2016-01-25 09:10:13.931604		identification-methods	\N	\N
3888	TemplateColumn	340	Number of Conditions Tracked	2	2016-01-25 09:10:13.94302	2016-01-25 09:10:13.94302		number-of-conditions-tracked	\N	\N
3889	TemplateColumn	340	Outreach Methods	3	2016-01-25 09:10:13.95446	2016-01-25 09:10:13.95446		outreach-methods	\N	\N
3890	TemplateColumn	340	Report Frequency	4	2016-01-25 09:10:13.965721	2016-01-25 09:10:13.965721		report-frequency	\N	\N
3891	TemplateField	341	Plan Type	1	2016-01-25 09:10:14.030384	2016-01-25 09:10:14.030384		plan-type	t	t
3892	TemplateField	341	Network	2	2016-01-25 09:10:14.041138	2016-01-25 09:10:14.041138		network	t	t
3893	TemplateField	341	Deductible Individual	4	2016-01-25 09:10:14.051732	2016-01-25 09:10:14.051732		deductible-individual	t	t
3894	TemplateField	341	Deductible Family	5	2016-01-25 09:10:14.06344	2016-01-25 09:10:14.06344		deductible-family	t	t
3895	TemplateField	341	Deductible - Family Multiple	6	2016-01-25 09:10:14.074211	2016-01-25 09:10:14.074211		deductible-family-multiple	\N	\N
3896	TemplateField	341	Deductible Format	7	2016-01-25 09:10:14.085447	2016-01-25 09:10:14.085447		deductible-format	\N	t
3897	TemplateField	341	Coinsurance	8	2016-01-25 09:10:14.096023	2016-01-25 09:10:14.096023		coinsurance	t	t
3898	TemplateField	341	Out-of-Pocket Max Individual (includes deductible)	9	2016-01-25 09:10:14.106519	2016-01-25 09:10:14.106519		out-of-pocket-max-individual-includes-deductible	t	t
3899	TemplateField	341	Out-of-Pocket Maximum - Family Multiple	11	2016-01-25 09:10:14.11725	2016-01-25 09:10:14.11725		out-of-pocket-maximum-family-multiple	\N	\N
3900	TemplateField	341	In & Out-of-Network Deductibles Cross Accumulation	12	2016-01-25 09:10:14.127718	2016-01-25 09:10:14.127718		in-out-of-network-deductibles-cross-accumulation	\N	t
3901	TemplateField	341	In & Out-of-Network OOP Maximum Cross Accumulation	13	2016-01-25 09:10:14.138515	2016-01-25 09:10:14.138515		in-out-of-network-oop-maximum-cross-accumulation	\N	t
3902	TemplateField	341	Annual Maximum	14	2016-01-25 09:10:14.149202	2016-01-25 09:10:14.149202		annual-maximum	\N	\N
3903	TemplateField	341	Gym Reimbursement	15	2016-01-25 09:10:14.159788	2016-01-25 09:10:14.159788		gym-reimbursement	\N	\N
3904	TemplateField	341	Customer Service Days/Hours	16	2016-01-25 09:10:14.17016	2016-01-25 09:10:14.17016		customer-service-days-hours	\N	\N
3905	TemplateField	341	OON Reimbursement Level	17	2016-01-25 09:10:14.180552	2016-01-25 09:10:14.180552		oon-reimbursement-level	t	t
3907	TemplateField	341	International Employees Covered	19	2016-01-25 09:10:14.208642	2016-01-25 09:10:14.208642		international-employees-covered	\N	\N
3908	TemplateField	341	Referrals Needed	3	2016-01-25 09:10:14.220007	2016-01-25 09:10:14.220007		referrals-needed	t	t
3909	TemplateField	341	Out-of-Pocket Max Family (includes deductible)	10	2016-01-25 09:10:14.230994	2016-01-25 09:10:14.230994		out-of-pocket-max-family-includes-deductible	t	t
3910	TemplateColumn	341	Entries	1	2016-01-25 09:10:14.243801	2016-01-25 09:10:14.243801		entries	\N	\N
3911	TemplateField	342	Rx Deductible - Individual	1	2016-01-25 09:10:14.477546	2016-01-25 09:10:14.477546		rx-deductible-individual	t	t
3912	TemplateField	342	Rx Deductible - Family	2	2016-01-25 09:10:14.48844	2016-01-25 09:10:14.48844		rx-deductible-family	t	t
3913	TemplateColumn	342	Deductible	1	2016-01-25 09:10:14.501697	2016-01-25 09:10:14.501697		deductible	\N	\N
3914	TemplateColumn	342	Per Member	2	2016-01-25 09:10:14.512245	2016-01-25 09:10:14.512245		per-member	\N	\N
3915	TemplateField	343	Integrated Wellness Plan	1	2016-01-25 09:10:14.57379	2016-01-25 09:10:14.57379		integrated-wellness-plan	\N	\N
3916	TemplateColumn	343	Entries	1	2016-01-25 09:10:14.586368	2016-01-25 09:10:14.586368		entries	\N	\N
\.


--
-- TOC entry 2951 (class 0 OID 0)
-- Dependencies: 354
-- Name: template_entities_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('template_entities_id_seq', 3916, true);


--
-- TOC entry 2834 (class 2606 OID 205604)
-- Name: template_entities_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY template_entities
    ADD CONSTRAINT template_entities_pkey PRIMARY KEY (id);


--
-- TOC entry 2831 (class 1259 OID 205681)
-- Name: index_template_entities_on_type_and_template_section_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_template_entities_on_type_and_template_section_id ON template_entities USING btree (type, template_section_id);


--
-- TOC entry 2832 (class 1259 OID 205682)
-- Name: index_template_entities_on_type_and_template_section_id_and_key; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_template_entities_on_type_and_template_section_id_and_key ON template_entities USING btree (type, template_section_id, key);


-- Completed on 2016-01-28 13:55:11 IST

--
-- PostgreSQL database dump complete
--

