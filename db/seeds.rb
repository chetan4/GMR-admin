# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
require 'rake'

Rake::Task['seed:populate_verticals'].invoke
Rake::Task['seed:populate_industries'].invoke

puts "Creating default company"
admin_vertical = Vertical.find_by_name("Admin")

default_company = Company.first_or_create(:name => "default",
                                          :vertical => admin_vertical,
                                          :website => "www.gmr.com",
                                          :address => "GMR HQ")

# Creating first employee in default company
puts "Creating Admin user"
user = default_company.users.first_or_create!(
    first_name: 'GMR',
    middle_name: 'M',
    last_name:  'Admin',
    email: 'admin@gmr.com',
    password: 'Test123',
    type: 'Admin',
    work_number: '+91-1235 987 123',
    cell_number: '+91-1235 987 123'
)

# Adding him as contact person in the company.
default_company.contact_person = user
default_company.save!

# Truncate complete permissions
ActiveRecord::Base.connection.execute("TRUNCATE permissions RESTART IDENTITY CASCADE")
# Truncate complete roles
ActiveRecord::Base.connection.execute("TRUNCATE roles RESTART IDENTITY CASCADE")
# Truncate complete role_permissions
ActiveRecord::Base.connection.execute("TRUNCATE role_permissions RESTART IDENTITY CASCADE")
# Rake task to create roles and permissions
Rake::Task['seed:populate_roles_and_permissions'].invoke

super_admin_role = Role.where(role_map: Role::SUPER_ADMIN_ROLE_MAP).first
user.roles << super_admin_role


SecurityQuestion.destroy_all

security_questions = [
    {question: "What was the last name of your favorite elementary / primary school teacher?"},
    {question: "What was your childhood nickname?"},
    {question: "What are the last 5 digits of your driver's license number?"},
    {question: "What is your oldest cousin's first and last name?"},
    {question: "What city or town did your mother and father meet?"},
    {question: "What was your ten digit childhood phone number including area code?"},
    {question: "What was the last name of your favorite childhood friend?"}
]

SecurityQuestion.create(security_questions)

# Truncate complete benefit_categories
ActiveRecord::Base.connection.execute("TRUNCATE benefit_categories RESTART IDENTITY CASCADE")
# Truncate complete benefit_types
ActiveRecord::Base.connection.execute("TRUNCATE benefit_types RESTART IDENTITY CASCADE")
# Truncate complete benefit_type_details
ActiveRecord::Base.connection.execute("TRUNCATE benefit_type_details RESTART IDENTITY CASCADE")

# Create Benefit Types data.
Rake::Task['seed:populate_benefit_types'].invoke


# Creating second employee in default company
puts "Creating different user"
user1 = default_company.users.first_or_create!(
    first_name: 'GMR1',
    last_name:  'Broker',
    email: 'broker@gmr.com',
    password: 'Test1234',
    type: 'Broker',
    work_number: '+91-2235 922 123',
    cell_number: '+91-2235 227 123'
)

# Adding him as contact person in the company.
default_company.contact_person = user1
default_company.save!

# Rake task to create notification types
ActiveRecord::Base.connection.execute("TRUNCATE notification_types RESTART IDENTITY CASCADE")
Rake::Task['seed:populate_notification_type'].invoke

# Dummy data to test notifications
notification_type = NotificationType.first
Notification.create(:sender_company_id => user.company.id,
                    :source_type => "Company",
                    :source_id => user.company.id,
                    :sender_user_id => user.id,
                    :message => "dummy text in notification",
                    :notification_type_id => notification_type.id,
                    :url => "http://www.google.com",
                    :receiver_company_id => user1.company.id,
                    :receiver_type => "company",
                    :receiver_id => user.id)

Notification.create(:sender_company_id => user.company.id,
                    :source_type => "Company",
                    :source_id => user.company.id,
                    :sender_user_id => user.id,
                    :message => "dummy text in notification1",
                    :notification_type_id => notification_type.id,
                    :url => "http://www.google.com",
                    :receiver_company_id => user1.company.id,
                    :receiver_type => "company",
                    :receiver_id => user.id)

Notification.create(:sender_company_id => user.company.id,
                    :source_type => "User",
                    :source_id => user.id,
                    :sender_user_id => user.id,
                    :message => "dummy text in notification 2",
                    :notification_type_id => notification_type.id,
                    :url => "http://www.google.com",
                    :receiver_company_id => user1.company.id,
                    :receiver_type => "company",
                    :receiver_id => user.id)

Notification.create(:sender_company_id => user.company.id,
                    :source_type => "User",
                    :source_id => user.id,
                    :sender_user_id => user.id,
                    :message => "dummy text in notification 3",
                    :notification_type_id => notification_type.id ,
                    :url => "http://www.google.com",
                    :receiver_company_id => user1.company.id,
                    :receiver_type => "company",
                    :receiver_id => user.id)

# Truncate complete rate_options
ActiveRecord::Base.connection.execute("TRUNCATE rate_options RESTART IDENTITY CASCADE")
# Truncate complete rate_option_labels
ActiveRecord::Base.connection.execute("TRUNCATE rate_option_labels RESTART IDENTITY CASCADE")
# Truncate complete plan_rate_options
ActiveRecord::Base.connection.execute("TRUNCATE plan_rate_options RESTART IDENTITY CASCADE")
# Truncate complete plan_rate_option_labels
ActiveRecord::Base.connection.execute("TRUNCATE plan_rate_option_labels RESTART IDENTITY CASCADE")
# Rake task to create rate options
Rake::Task['seed:populate_rate_options'].invoke