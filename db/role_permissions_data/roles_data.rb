ROLES = ActiveSupport::OrderedHash.new()

ROLES["Admin"] = [
    {name: 'Super Admin'},
    {name: 'Company Admin'},
    {name: 'Super Manager'},
    {name: 'Manager'}
]
ROLES["Broker"] = [
    {name: 'Company Admin'},
]
ROLES["Carrier"] = [
    {name: 'Company Admin'},
    {name: 'Sales Representative'},
    {name: 'Service Rep/Acct Exec'},
    {name: 'Underwriter'},
    {name: 'Sales Manager'},
    {name: 'Service Manager'},
    {name: 'Underwriting Manager'},
    {name: 'Market/regional Leader'},
    {name: 'Vendor Admin'},
    {name: 'IT Support'},
    {name: 'Sales Rep Assistant'},
    {name: 'Service Rep Assistant'},
    {name: 'Marketing Manage'}
]
ROLES["DataEntry"] = [
    {name: 'Company Admin'},
    {name: 'Operator'},
    {name: 'Reviewer'},
    {name: 'Operator and Reviewer'},
    {name: 'IT Support'},
    {name: 'Manager'}
]
ROLES["Employer"] = [
    {name: 'Company Admin'},
    {name: 'Management'},
    {name: 'Benefit Analyst'},
    {name: 'Employer Admin'},
    {name: 'IT'},
    {name: 'Broker'}
]