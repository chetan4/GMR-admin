carrier_roles_maps_except_it = ["company-admin", "sales-representative", "service-rep-acct-exec", "underwriter", "sales-manager", "service-manager", "underwriting-manager", "market-regional-leader", "vendor-admin", "sales-rep-assistant", "service-rep-assistant", "marketing-manage"]
employer_roles_maps_except_it = ["company-admin", "management", "benefit-analyst", "employer-admin", "broker"]#except it support
data_entry_roles_maps_except_it = ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
PERMISSIONS = [
    {
        :controller => "employer/employees", :action => "index", :flags => {}, :verticals => ["Employer"],
        :role_maps => ["it"]
    },
    {
        :controller => "employer/employees", :action => "new", :flags => {}, :verticals => ["Employer"],
        :role_maps => ["it"]
    },
    {
        :controller => "employer/employees", :action => "create", :flags => {}, :verticals => ["Employer"],
        :role_maps => ["it"]
    },
    {
        :controller => "carrier/employees", :action => "index", :flags => {}, :verticals => ["Carrier"],
        :role_maps => ["it-support"]
    },
    {
        :controller => "carrier/employees", :action => "new", :flags => {}, :verticals => ["Carrier"],
        :role_maps => ["it-support"]
    },
    {
        :controller => "carrier/employees", :action => "create", :flags => {}, :verticals => ["Carrier"],
        :role_maps => ["it-support"]
    },
    {
        :controller => "data_entry/employees", :action => "index", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["it-support"]
    },
    {
        :controller => "data_entry/employees", :action => "new", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["it-support"]
    },
    {
        :controller => "employer/onboardings", :action => "show", :flags => {}, :verticals => ["Employer"],
        :role_maps => ["benefit-analyst", "management", "employer-admin"]
    },
    {
        :controller => "employer/onboardings", :action => "summary_page", :flags => {}, :verticals => ["Employer"],
        :role_maps => ["benefit-analyst", "management", "employer-admin"]
    },
    {
        :controller => "employer/onboardings", :action => "done", :flags => {}, :verticals => ["Employer"],
        :role_maps => ["benefit-analyst", "management", "employer-admin"]
    },
    {
        :controller => "employer/onboardings", :action => "step2", :flags => {}, :verticals => ["Employer"],
        :role_maps => ["benefit-analyst", "management", "employer-admin"]
    },
    {
        :controller => "employer/plans", :action => "create", :flags => {}, :verticals => ["Employer"],
        :role_maps => ["benefit-analyst", "management", "employer-admin"]
    },
    {
        :controller => "employer/plans", :action => "new", :flags => {}, :verticals => ["Employer"],
        :role_maps => ["benefit-analyst", "management", "employer-admin"]
    },
    {
      :controller => "employer/underwritings", :action => "show", :flags => {}, :verticals => ["Employer"],
      :role_maps => ["benefit-analyst", "management", "employer-admin"]
    },
    {
      :controller => "employer/underwritings", :action => "index", :flags => {}, :verticals => ["Employer"],
      :role_maps => ["benefit-analyst", "management", "employer-admin"]
    },
    {
      :controller => "employer/underwritings", :action => "calculate", :flags => {}, :verticals => ["Employer"],
      :role_maps => ["benefit-analyst", "management", "employer-admin"]
    },
    {
      :controller => "employer/underwritings", :action => "show_details", :flags => {}, :verticals => ["Employer"],
      :role_maps => ["benefit-analyst", "management", "employer-admin"]
    },
    {
      :controller => "employer/underwritings", :action => "send_mail", :flags => {}, :verticals => ["Employer"],
      :role_maps => ["benefit-analyst", "management", "employer-admin"]
    },
    {
        :controller => "data_entry/dashboards", :action => "show", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager", "it-support"]
    },
    {
        :controller => "data_entry/tasks", :action => "task_details", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/tasks", :action => "rate_history", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/tasks", :action => "show_retag", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/tasks", :action => "employee_structure_request", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/tasks", :action => "save_retagged_files", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/tasks", :action => "save_rate_options", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/tasks", :action => "get_rate_option_labels", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/tasks", :action => "add_rate_options", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/tasks", :action => "add_comments", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/tasks", :action => "request_information", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/tasks", :action => "update_task_assignment", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/tasks", :action => "reset_rate_options", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/tasks", :action => "index", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/tasks", :action => "show", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
      :controller => "data_entry/tasks", :action => "set_actuary_percentage", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
      :controller => "data_entry/tasks", :action => "plan_options", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/plan_selections", :action => "step2", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/plan_selections", :action => "summary_page", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/plan_selections", :action => "done", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/plan_selections", :action => "create", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/plan_selections", :action => "new", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/plan_selections", :action => "edit", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/plan_selections", :action => "show", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/plan_selections", :action => "update", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/plan_selections", :action => "update", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/plan_selections", :action => "destroy", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/plans", :action => "add_effective_date", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/plans", :action => "setup", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/plans", :action => "create", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/plans", :action => "new", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/plans", :action => "search", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/tasks", :action => "preview_template", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/tasks", :action => "upgrade_template", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/tasks", :action => "search_plans", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/tasks", :action => "attach_plan", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/plan_designs", :action => "show", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/plan_designs", :action => "create", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/plan_designs", :action => "reset", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/medical_claims", :action => "show", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/medical_claims", :action => "create", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/medical_claims", :action => "reset", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/medical_claims", :action => "standard_template", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/medical_claims", :action => "import", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/medical_claims", :action => "history", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/medical_claims", :action => "filtered_history", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/medical_claims", :action => "current_data", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/medical_claims", :action => "historical_data", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/large_claims", :action => "show", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/large_claims", :action => "create", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/large_claims", :action => "reset", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/large_claims", :action => "standard_template", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/large_claims", :action => "import", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/large_claims", :action => "history", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/large_claims", :action => "filtered_history", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/large_claims", :action => "current_data", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/large_claims", :action => "historical_data", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/rates", :action => "create", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/rates", :action => "reset", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/plans", :action => "add_date", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/contributions", :action => "create", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/contributions", :action => "add", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/plans", :action => "add_date", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/tasks", :action => "conversion_utility", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/tasks", :action => "update_conversion_utility_data", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/tasks", :action => "process_census_subtask", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/tasks", :action => "census", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/censuses", :action => "import", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/censuses", :action => "filtered_history", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/censuses", :action => "history", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/censuses", :action => "historical_data", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/tasks", :action => "create_sub_task", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/tasks", :action => "reopen_sub_task", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {    :controller => "data_entry/contributions", :action => "get_contribution", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/contributions", :action => "remove_contribution", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/contributions", :action => "copy_contribution", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/contributions", :action => "update_contribution", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/contributions", :action => "reset", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
      :controller => "data_entry/plan_change_histories", :action => "perform_action", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
      :controller => "data_entry/plan_change_histories", :action => "add_or_update", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
      :controller => "data_entry/plan_change_histories", :action => "add_or_update_with_date", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
      :controller => "data_entry/plan_change_histories", :action => "delete", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
      :controller => "data_entry/plan_change_histories", :action => "fetch_plan_design", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
    :controller => "data_entry/underwritings", :action => "select_plan", :flags => {}, :verticals => ["DataEntry"],
    :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
      :controller => "data_entry/underwritings", :action => "search", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
      :controller => "data_entry/underwritings", :action => "reset", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/underwritings", :action => "history", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
      :controller => "data_entry/underwritings", :action => "attach_plan", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
      :controller => "data_entry/underwritings", :action => "search_plans", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
      :controller => "data_entry/underwritings", :action => "create_or_update", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
      :controller => "data_entry/underwritings", :action => "show", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
      :controller => "data_entry/underwritings", :action => "fetch_period", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
      :controller => "data_entry/underwritings", :action => "override_claim_period_cal", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/underwritings", :action => "fetch_retention", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => ["company-admin", "operator", "reviewer", "operator-and-reviewer", "manager"]
    },
    {
        :controller => "data_entry/renewals", :action => "show", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/renewals", :action => "get_renewal_search", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/renewals", :action => "create_renewal_plan", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/renewals", :action => "get_search_results", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/renewals", :action => "update_renewal_plan_name", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
      :controller => "data_entry/packages", :action => "index", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/packages", :action => "search_packages", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/packages", :action => "new_package_option", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/packages", :action => "update_package_option_name", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/packages", :action => "clone_package_plan", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/packages", :action => "delete_package_option", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/packages", :action => "add_renewal_plan", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/packages", :action => "delete_renewal_plan", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/packages", :action => "show", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/packages", :action => "preview_update_enrollments_and_rates", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },

    {
        :controller => "data_entry/packages", :action => "add_tags", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/packages", :action => "get_tag_data", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/packages", :action => "save_package_option", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/packages", :action => "delete_tagged_file", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/packages", :action => "upload_tag_file", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/packages", :action => "get_tagged_files", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/underwritings", :action => "package_list", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
      :controller => "data_entry/packages", :action => "create", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => data_entry_roles_maps_except_it
    },
    {
      :controller => "data_entry/packages", :action => "update_plan_package", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/renewals_task", :action => "get_rate_option_labels", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/renewals_task", :action => "package_plan_renewals_show", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/renewals_task", :action => "package_option_renewal_option_show", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/renewals_task", :action => "save_rate_options", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/renewals_task", :action => "reset_rate_options", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/renewals_task", :action => "update", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/renewals_task", :action => "reset", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/renewals_task", :action => "validate_plan", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/renewals_task", :action => "verify_renewal", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/renewals_task", :action => "get_renewal_estimates", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/renewals_task", :action => "package_plan_renewals_plan_design_show", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/renewals_task", :action => "submit_renewal", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/renewals_task", :action => "save_estimate_enrollments", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/renewals_task", :action => "save_volume", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quote_packages", :action => "new", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quote_packages", :action => "create", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quote_packages", :action => "show", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quote_packages", :action => "search", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quote_packages", :action => "new_quote_package_option", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quote_packages", :action => "update_quote_package_option_name", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quote_packages", :action => "delete_quote_package_option", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quote_packages", :action => "add_quote_plan", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quote_packages", :action => "delete_quote_plan", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quote_packages", :action => "add_tags", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quote_packages", :action => "get_tag_data", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quote_packages", :action => "preview_update_enrollments_and_rates_quote_package", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quote_packages", :action => "save_quote_package_option", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quote_packages", :action => "delete_tagged_file", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quote_packages", :action => "upload_tag_file", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quote_packages", :action => "get_tagged_files", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quotes", :action => "index", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quotes", :action => "new", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quotes", :action => "create", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quotes", :action => "search", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quotes", :action => "create_quote_plan_option", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quotes", :action => "update_quote_plan_name", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quote_tasks", :action => "package_plan_quote_show", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quote_tasks", :action => "get_rate_option_labels", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quote_tasks", :action => "save_rate_options", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quote_tasks", :action => "reset_rate_options", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quote_tasks", :action => "get_quote_estimates", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quote_tasks", :action => "save_estimate_enrollments", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quote_tasks", :action => "save_volume", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quote_tasks", :action => "verify_quote", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quote_tasks", :action => "submit_quote", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quote_tasks", :action => "quote_plan_design_show", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quote_tasks", :action => "reset_plan_design", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/quote_tasks", :action => "update", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/conversations", :action => "search", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/conversations", :action => "index", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/conversations", :action => "show", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/conversations", :action => "create_message", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/conversations", :action => "plan_design_sliders_info", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },


    {
        :controller => "carrier/conversations", :action => "create_message", :flags => {}, :verticals => ["Carrier"],
        :role_maps => carrier_roles_maps_except_it
    },
    {
        :controller => "carrier/conversations", :action => "search", :flags => {}, :verticals => ["Carrier"],
        :role_maps => carrier_roles_maps_except_it
    },
    {
        :controller => "carrier/conversations", :action => "plan_design_sliders_info", :flags => {}, :verticals => ["Carrier"],
        :role_maps => carrier_roles_maps_except_it
    },

    ### employer roles----------------------------
  {
    :controller => "employer/conversations", :action => "search", :flags => {}, :verticals => ["Employer"],
    :role_maps => employer_roles_maps_except_it
  },
  {
    :controller => "employer/conversations", :action => "create_message", :flags => {}, :verticals => ["Employer"],
    :role_maps => employer_roles_maps_except_it
  },
  {
    :controller => "employer/conversations", :action => "plan_design_sliders_info", :flags => {}, :verticals => ["Employer"],
    :role_maps => employer_roles_maps_except_it
  },
  {
    :controller => "employer/current_plans", :action => "get_plan_info", :flags => {}, :verticals => ["Employer"],
    :role_maps => employer_roles_maps_except_it
  },
  {
    :controller => "employer/current_plans", :action => "get_plan_details", :flags => {}, :verticals => ["Employer"],
    :role_maps => employer_roles_maps_except_it
  },
  {
    :controller => "employer/current_plans", :action => "employer_current_plans", :flags => {}, :verticals => ["Employer"],
    :role_maps => employer_roles_maps_except_it
  },
  {
    :controller => "employer/current_plans", :action => "employer_plan_demographics", :flags => {}, :verticals => ["Employer"],
    :role_maps => employer_roles_maps_except_it
  },
  {
    :controller => "employer/current_plans", :action => "show", :flags => {}, :verticals => ["Employer"],
    :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/quotes", :action => "get_plan_info", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/renewals", :action => "get_plan_info", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },

    ### Carrier roles----------------------------

  {
    :controller => "carrier/renewals", :action => "show", :flags => {}, :verticals => ["Carrier"],
    :role_maps => carrier_roles_maps_except_it
  },
  {
    :controller => "carrier/renewals", :action => "get_plan_info", :flags => {}, :verticals => ["Carrier"],
    :role_maps => carrier_roles_maps_except_it
  },
  {
    :controller => "carrier/renewals", :action => "accept_offer", :flags => {}, :verticals => ["Carrier"],
    :role_maps => carrier_roles_maps_except_it
  },
  {
    :controller => "carrier/renewals", :action => "get_renewals", :flags => {}, :verticals => ["Carrier"],
    :role_maps => carrier_roles_maps_except_it
  },
  {
    :controller => "carrier/quote_requests", :action => "show", :flags => {}, :verticals => ["Carrier"],
    :role_maps => carrier_roles_maps_except_it
  },
  {
    :controller => "carrier/quote_requests", :action => "accept_offer", :flags => {}, :verticals => ["Carrier"],
    :role_maps => carrier_roles_maps_except_it
  },
  {
    :controller => "carrier/quote_requests", :action => "filtered_quote_requests", :flags => {}, :verticals => ["Carrier"],
    :role_maps => carrier_roles_maps_except_it
  },
  {
    :controller => "carrier/quote_requests", :action => "get_plan_info", :flags => {}, :verticals => ["Carrier"],
    :role_maps => carrier_roles_maps_except_it
  },
  {
      :controller => "employer/renewal_dates", :action => "show", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/plan_design_sliders", :action => "index", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/renewals", :action => "plan_marketing", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/dashboard/vendor_market_place", :action => "index", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/quotes", :action => "plan_marketing", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
 {
    :controller => "carrier/employers", :action => "show", :flags => {}, :verticals => ["Carrier"],
    :role_maps => carrier_roles_maps_except_it
 },
  {
      :controller => "employer/dashboard/vendor_market_place", :action => "benefit_type", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/dashboard/vendor_market_place", :action => "quote_requests", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
    {
        :controller => "employer/employees", :action => "edit", :flags => {}, :verticals => ["Employer"],
        :role_maps => ["it"]
    },
    {
        :controller => "employer/employees", :action => "update", :flags => {}, :verticals => ["Employer"],
        :role_maps => ["it"]
    },
    {
        :controller => "employer/employees", :action => "update_status", :flags => {}, :verticals => ["Employer"],
        :role_maps => ["it"]
    },
    {
        :controller => "data_entry/conversations", :action => "conversation_tasks", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/conversations", :action => "get_conversation_tasks", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
    {
        :controller => "data_entry/recommendations", :action => "show", :flags => {}, :verticals => ["DataEntry"],
        :role_maps => data_entry_roles_maps_except_it
    },
  {
      :controller => "employer/renewals", :action => "show", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/employee_records", :action => "show", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/demographics", :action => "show", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/quotes", :action => "show", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/recommendations", :action => "index", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/recommendations", :action => "questionnaire", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/performance_indicators", :action => "show", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/renewals", :action => "get_plan_marketing_data", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/quotes", :action => "employer_quote_requests", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/contributions", :action => "show", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },

  #-------------------------------------------------Data Entry--------------------------------------------------

  {
      :controller => "data_entry/underwritings", :action => "publish", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => data_entry_roles_maps_except_it
  },
  {
      :controller => "data_entry/simple_project_renewals", :action => "new", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => data_entry_roles_maps_except_it
  },
  {
      :controller => "data_entry/simple_project_renewals", :action => "create_or_update", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => data_entry_roles_maps_except_it
  },
  {
      :controller => "data_entry/simple_project_renewals", :action => "destroy", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => data_entry_roles_maps_except_it
  },
  {
      :controller => "data_entry/simple_project_renewals", :action => "publish", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => data_entry_roles_maps_except_it
  },
  {
      :controller => "data_entry/simple_project_renewals", :action => "simple_project_renewal_form", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => data_entry_roles_maps_except_it
  },
  {
      :controller => "data_entry/simple_project_renewals", :action => "unpublish", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => data_entry_roles_maps_except_it
  },
  {
      :controller => "data_entry/underwritings", :action => "unpublish", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => data_entry_roles_maps_except_it
    },
    {
      :controller => "employer/quotes", :action => "get_plan_marketing_data", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/renewals", :action => "update_package_likes", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/quotes", :action => "update_package_likes", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/renewals", :action => "finalize_plans", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/quotes", :action => "finalize_plans", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/employee_records", :action => "get_employer_records", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/renewals", :action => "get_renewals_plans", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/renewal_dates", :action => "get_renewal_dates_plans", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/renewal_dates", :action => "request_date_now", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/renewal_dates", :action => "update_renewal_dates", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/performance_indicators", :action => "get_kpi_data", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/demographics", :action => "get_employee_graphics_info", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/renewals", :action => "compare_plans", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/renewals", :action => "get_rate_compare_data", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/quotes", :action => "confirm_package_option", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/home", :action => "get_graph_data", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/renewals", :action => "confirmed_packages", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/renewals", :action => "confirm_offer", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/renewals", :action => "change_package_status", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/renewals", :action => "add_terms_and_conditions", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/renewals", :action => "remove_attachment", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
    :controller => "carrier/employers", :action => "download_excel", :flags => {}, :verticals => ["Carrier"],
    :role_maps => carrier_roles_maps_except_it
  },
  {
      :controller => "employer/commissions", :action => "index", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "data_entry/commissions", :action => "update_commissions", :flags => {}, :verticals => ["DataEntry"],
    :role_maps => data_entry_roles_maps_except_it
  },
  {
      :controller => "data_entry/commissions", :action => "plans", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => data_entry_roles_maps_except_it
  },
  {
      :controller => "employer/quotes", :action => "compare_quotes", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/quotes", :action => "get_rate_compare_data", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/quotes", :action => "confirmed_packages", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/quotes", :action => "add_terms_and_conditions", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/quotes", :action => "remove_attachment", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/quotes", :action => "confirm_offer", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/quotes", :action => "change_package_status", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/quotes", :action => "get_plan_design_compare_data", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
    {
      :controller => "employer/quotes", :action => "get_plan_design_compare_data_report", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/quotes", :action => "get_file_viewer_compare_data", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/renewals", :action => "get_plan_design_compare_data", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
    {
        :controller => "employer/renewals", :action => "get_plan_design_compare_data_report", :flags => {}, :verticals => ["Employer"],
        :role_maps => employer_roles_maps_except_it
    },
  {
      :controller => "employer/renewals", :action => "get_file_viewer_compare_data", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/renewals", :action => "finalize_renewal_plans_status", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/renewals", :action => "add_renewal_plans_to_basket", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/renewals", :action => "confirm_renewal_plans", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/renewals", :action => "delete_renewal_plans", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "carrier/employers", :action => "get_rfp_content", :flags => {}, :verticals => ["Carrier"],
      :role_maps => carrier_roles_maps_except_it
  },
  {
      :controller => "carrier/employers", :action => "send_rfp", :flags => {}, :verticals => ["Carrier"],
      :role_maps => carrier_roles_maps_except_it
  },
  {
      :controller => "carrier/renewals", :action => "add_terms_and_conditions", :flags => {}, :verticals => ["Carrier"],
      :role_maps => carrier_roles_maps_except_it
  },
  {
      :controller => "carrier/quote_requests", :action => "add_terms_and_conditions", :flags => {}, :verticals => ["Carrier"],
      :role_maps => carrier_roles_maps_except_it
  },
  {
      :controller => "carrier/renewals", :action => "remove_attachment", :flags => {}, :verticals => ["Carrier"],
      :role_maps => carrier_roles_maps_except_it
  },
  {
      :controller => "carrier/quote_requests", :action => "remove_attachment", :flags => {}, :verticals => ["Carrier"],
      :role_maps => carrier_roles_maps_except_it
  },
  {
      :controller => "employer/current_plans", :action => "prior_plans", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/current_plans", :action => "employer_prior_plans", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },

  {
    :controller => "data_entry/plans", :action => "orphan_quote", :flags => {}, :verticals => ["DataEntry"],
    :role_maps => data_entry_roles_maps_except_it
  },
  {
    :controller => "data_entry/plans", :action => "create_orphan", :flags => {}, :verticals => ["DataEntry"],
    :role_maps => data_entry_roles_maps_except_it
  },

  {
    :controller => "data_entry/plans", :action => "create_quote", :flags => {}, :verticals => ["DataEntry"],
    :role_maps => data_entry_roles_maps_except_it
  },

  {
      :controller => "data_entry/packages", :action => "create_sub_package", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => data_entry_roles_maps_except_it
  },

  {
      :controller => "data_entry/quote_packages", :action => "create_sub_package", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => data_entry_roles_maps_except_it
  },
  {
      :controller => "data_entry/packages", :action => "associate_or_disassociate_sub_package", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => data_entry_roles_maps_except_it
  },

  {
      :controller => "data_entry/quote_packages", :action => "associate_or_disassociate_sub_package", :flags => {}, :verticals => ["DataEntry"],
      :role_maps => data_entry_roles_maps_except_it
  },
  {
      :controller => "employer/buyout_calculators", :action => "show", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/buyout_calculators", :action => "buyout_details", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
      :controller => "employer/buyout_calculators", :action => "delete_buyout_calculator", :flags => {}, :verticals => ["Employer"],
      :role_maps => employer_roles_maps_except_it
  },
  {
    :controller => "data_entry/large_claims", :action => "update_historical_data", :flags => {},
        :verticals => ["DataEntry"], :role_maps => data_entry_roles_maps_except_it
  }

]
