class UpdateStatusColumnToIntegerForPackagePlanRenewals < ActiveRecord::Migration
  def change
    remove_column :package_plan_renewals, :status
    add_column :package_plan_renewals, :status,  :integer
  end
end
