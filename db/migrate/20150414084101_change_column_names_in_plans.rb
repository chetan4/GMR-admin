class ChangeColumnNamesInPlans < ActiveRecord::Migration
  def change
    rename_column :plans, :carrier_id, :carrier_company_id
    rename_column :plans, :broker_id, :broker_company_id
    rename_column :plans, :employer_id, :employer_company_id
  end
end
