class AddColumnLikesToPackage < ActiveRecord::Migration
  def change
    add_column :packages, :current_plan_liked,:boolean, default: false
    add_column :packages, :renewal_liked, :boolean, default: false

    add_column :quote_packages, :current_plan_liked, :boolean, default: false
  end
end
