class AddColumnsEffectiveStartAndEndDateForQuotes < ActiveRecord::Migration
  def change
    add_column :quotes, :effective_start_date, :datetime
    add_column :quotes, :effective_end_date, :datetime
  end
end
