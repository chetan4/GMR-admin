class AddPlanIdToPlanDesignData < ActiveRecord::Migration
  def up
    add_column :plan_design_data, :plan_id, :integer, null: false
  end
end
