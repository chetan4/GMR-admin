class AddColumnAssociatedPlansToPlanRenewals < ActiveRecord::Migration
  def change
    add_column :plan_renewals, :associated_plan_ids, :hstore
  end
end
