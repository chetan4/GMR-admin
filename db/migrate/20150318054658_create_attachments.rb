class CreateAttachments < ActiveRecord::Migration
  def change
    create_table :attachments do |t|
      t.string  :attachable_type
      t.integer :attachable_id
      t.string  :name
      t.integer  :sub_type  # Here sub type signifies how this attachment is to be used in the system.
      t.timestamps  null:false
    end

    add_index :attachments, [:attachable_type, :attachable_id, :sub_type], name: 'attachment_index'
  end
end