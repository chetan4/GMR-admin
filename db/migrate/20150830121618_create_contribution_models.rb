class CreateContributionModels < ActiveRecord::Migration
  def change
    create_table :contribution_models do |t|
      t.string :name, null: false
      t.integer :model_type, null: false
      t.integer :employer_company_id, null: false
      t.references :benefit_type
      t.integer :status, default: 1
      t.integer :created_by, null: false
      t.integer :last_updated_by

      t.timestamps null: false
    end

    add_index :contribution_models, [:employer_company_id, :benefit_type_id, :name], unique: true, name: 'UNIQUE1'
  end
end
