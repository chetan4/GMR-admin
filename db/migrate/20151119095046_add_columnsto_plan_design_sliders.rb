class AddColumnstoPlanDesignSliders < ActiveRecord::Migration
  def change
    add_column :plan_design_sliders, :name, :string
    add_column :plan_design_sliders, :employer_company_id, :integer
    add_column :plan_design_sliders, :carrier_company_id, :integer
    add_column :plan_design_sliders, :benefit_type_id, :integer
    add_column :plan_design_sliders, :deleted, :boolean
  end
end
