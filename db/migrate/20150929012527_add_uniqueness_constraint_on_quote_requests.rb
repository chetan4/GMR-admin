class AddUniquenessConstraintOnQuoteRequests < ActiveRecord::Migration
  def change
    # Rake::Task['onetime:clean_up_old_quote_requests_and_plan_requests'].invoke
    change_column_null :quote_requests, :benefit_type_id, false
    add_index :quote_requests, [:employer_company_id, :carrier_company_id, :benefit_type_id], :unique=>true, name: 'unique_index_quote_per_employer_per_carrier_per_benefit_type'
  end
end
