class AddColumnsToEmployeeClass < ActiveRecord::Migration
  def change
    add_column :employee_classes, :spouse_eligibility, :boolean
    add_column :employee_classes, :domestic_partner, :boolean
    add_column :employee_classes, :child_limiting_age, :integer
    add_column :employee_classes, :dependent_child_coverage, :boolean

    remove_column :employee_classes, :hours
    add_column :employee_classes, :hours, :integer
    remove_column :employee_classes, :waiting_period, :integer
    add_column :employee_classes, :waiting_period, :integer
  end
end
