class CreateEmployerLocations < ActiveRecord::Migration
  def change
    create_table :employer_locations do |t|
      t.integer :employer_id
      t.integer :business_unit_id, null: false
      t.string :address
      t.string :city, null: false
      t.string :state, null: false
      t.string :country, null: false
      t.integer :pin_code, null: false
      t.integer :no_of_employees, null: false

      t.timestamps null: false
    end
  end
end
