class AddColumnReserveFactorToUnderwritingPeriod < ActiveRecord::Migration
  def change
    add_column :underwriting_periods, :reserve_factor, :integer, precision: 10, scale: 7
  end
end
