class ChangePackageOptionStatus < ActiveRecord::Migration
  def change
    PackageOption.all.each do |package_option|
      renewal_plan_ids = PackageOptionRenewalOption.where(:package_option_id => package_option.id).pluck(:package_plan_renewal_id)
      renewal_plan_status = PackagePlanRenewal.where(id: renewal_plan_ids).map(&:status).uniq
      if renewal_plan_status == [PackagePlanRenewal.statuses.invert[PackagePlanRenewal.statuses['reviewed']]]
        package_option.update_attributes!(status: PackageOption.statuses['reviewed'])
      elsif package_option.status ==  PackageOption.statuses.invert[PackageOption.statuses['reviewed']]
        package_option.update_attributes!(status: PackageOption.statuses['review_in_progress'])
      end
    end
  end
end
