class UserBenefitTypes < ActiveRecord::Migration
  def change
    create_table :user_benefit_types do |t|
      t.belongs_to :user,          null: false
      t.belongs_to :benefit_type,  null: false
      t.timestamps                 null: false
    end

    add_index :user_benefit_types, [:user_id, :benefit_type_id], unique: true

    add_foreign_key :user_benefit_types, :users, name: "carrier_benefit_types_fkey_on_user_id", on_delete: :cascade
    add_foreign_key :user_benefit_types, :benefit_types, name: "carrier_benefit_types_fkey_on_benefit_type_id", on_delete: :cascade
  end
end
