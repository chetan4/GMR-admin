class AddNameColumnToDataEntryTask < ActiveRecord::Migration
  def change
    add_column :data_entry_tasks, :name, :string
  end
end
