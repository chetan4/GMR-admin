class ModifyIndexOnRenewalPlanRateOption < ActiveRecord::Migration
  def up
    remove_index :renewal_plan_rate_options, name: 'index_renewal_pro_on_renewable_id'
    add_index :renewal_plan_rate_options, [:renewable_id, :renewable_type], name: "index_renewal_pro_on_renewable_id", using: :btree
  end

  def down
    remove_index :renewal_plan_rate_options, name: 'index_renewal_pro_on_renewable_id'
    add_index :renewal_plan_rate_options, [:renewable_id, :renewable_type], name: "index_renewal_pro_on_renewable_id", unique: true,  using: :btree
  end
end
