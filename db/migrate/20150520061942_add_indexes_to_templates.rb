class AddIndexesToTemplates < ActiveRecord::Migration
  def change
    add_index :templates, [:benefit_type_id, :version], unique: true
  end
end
