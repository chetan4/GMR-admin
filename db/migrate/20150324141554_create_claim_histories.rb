class CreateClaimHistories < ActiveRecord::Migration
  def change
    create_table :claim_histories do |t|
      t.integer :plan_id, null: false
      t.integer :employer_id, null: false
      t.integer :no_of_claimant, null: false
      t.decimal :premium, precision: 17, scale: 2, null: false
      t.decimal :expense, precision: 17, scale: 2, null: false
      t.decimal :loss_ratio, precision: 17, scale: 2
      t.decimal :average_monthly_claim_cost, precision: 17, scale: 2
      t.decimal :quarterly_claim_cost, precision: 17, scale: 2
      t.decimal :quarterly_loss_ratio, precision: 17, scale: 2
      t.decimal :yearly_claim_cost, precision: 17, scale: 2
      t.hstore :data

      t.timestamps null: false
    end
  end
end
