class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.integer :sender_company_id
      t.string :source_type
      t.integer :source_id
      t.integer :sender_user_id
      t.text :message
      t.integer :notification_type_id
      t.string :url
      t.integer :receiver_company_id
      t.string :receiver_type
      t.integer :receiver_id

      t.timestamps null: false
    end

    add_index :notifications, :notification_type_id
    add_index :notifications, [:receiver_company_id, :receiver_type, :receiver_id],
              :name => 'receiver_company_type_id'
  end
end
