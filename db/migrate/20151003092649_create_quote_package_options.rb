class CreateQuotePackageOptions < ActiveRecord::Migration
  def change
    create_table :quote_package_options do |t|
      t.integer :quote_package_id, null: false
      t.string :name, null: false
      t.integer :status
      t.hstore :tag
      t.timestamps null: false
    end
    add_index :quote_package_options, [:quote_package_id], name: 'QUOTE_PACKAGE_OPTION_INDEX'
  end
end
