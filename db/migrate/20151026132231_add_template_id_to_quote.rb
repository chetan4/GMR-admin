class AddTemplateIdToQuote < ActiveRecord::Migration
  def up
    add_column :quotes, :template_id, :integer
  end

  def down
    remove_column :quotes, :template_id
  end
end
