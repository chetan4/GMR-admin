class RemoveTablePackageLikeBackup < ActiveRecord::Migration
  def up
    drop_table :package_likes_backup if ActiveRecord::Base.connection.table_exists? :package_likes_backup
  end
end
