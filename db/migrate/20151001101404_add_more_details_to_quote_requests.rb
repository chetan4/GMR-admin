class AddMoreDetailsToQuoteRequests < ActiveRecord::Migration
  def change
    add_column :quote_requests, :is_current_plan, :boolean, default: false
    add_column :quote_requests, :quote_based_on_profile, :boolean, default: false
    add_column :quote_requests, :option_percentage, :decimal, precision: 5, scale: 2, default: 0.0
    add_column :quote_requests, :commission, :decimal, precision: 5, scale: 2, default: 0.0
  end
end
