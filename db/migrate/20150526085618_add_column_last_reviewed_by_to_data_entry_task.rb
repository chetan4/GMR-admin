class AddColumnLastReviewedByToDataEntryTask < ActiveRecord::Migration
  def change
    add_column :data_entry_tasks, :last_reviewed_by_id, :integer
  end
end
