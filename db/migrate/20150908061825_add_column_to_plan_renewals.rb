class AddColumnToPlanRenewals < ActiveRecord::Migration
  def change
    add_column :plan_renewals, :funding_arrangement, :integer, default: 0
    add_column :plan_renewals, :plan_inception_date, :date
  end
end
