class CreateSubpackages < ActiveRecord::Migration
  def change
    create_table :subpackages do |t|
      t.string  :unique_id
      t.integer :packagable_id
      t.string  :packagable_type
      t.timestamps null: false
    end
    add_index :subpackages, [:packagable_type, :packagable_id], name: 'subpackage_index'
  end
end
