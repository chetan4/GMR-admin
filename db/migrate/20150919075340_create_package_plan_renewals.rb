class CreatePackagePlanRenewals < ActiveRecord::Migration
  def change
    create_table :package_plan_renewals do |t|
      t.references :plan
      t.string :name
      t.string :type # package_plan_renewal, package_plan_renewal_option
      t.integer :status # enum package_created, renewal_requested
      t.hstore :metadata

      t.timestamps null: false
    end

    add_index :package_plan_renewals, [:plan_id, :name], unique: true
  end
end
