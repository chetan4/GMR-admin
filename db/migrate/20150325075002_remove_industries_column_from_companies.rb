class RemoveIndustriesColumnFromCompanies < ActiveRecord::Migration
  def up
    remove_column :companies, :industries
  end

  def down
    add_column :companies, :industries, :text
  end
end
