class CreateRenewalPlanRateOptionLabels < ActiveRecord::Migration
  def change
    create_table :renewal_plan_rate_option_labels do |t|
      t.integer   :renewal_plan_rate_option_id
      t.integer   :rate_option_label_id
      t.hstore    :metadata
      t.integer   :enrollment

      t.timestamps null: false
    end
  end
end
