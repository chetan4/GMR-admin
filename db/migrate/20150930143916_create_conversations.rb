class CreateConversations < ActiveRecord::Migration
  def change
    create_table :conversations do |t|
      t.integer :employer_company_id
      t.integer :carrier_company_id
      t.references :benefit_type, null: false
      t.string :type, null: false
      t.integer :created_by_id, null: false
      t.string :unique_id, unique: true, index: true, null: false
      t.integer :parent_id, null: false, default: 0

      t.timestamps null: false
    end

    add_index :conversations, [:employer_company_id, :benefit_type_id, :carrier_company_id], name: 'CONVERSATION_INDEX'
  end
end
