class RemoveColumnsFromUnderwritingPeriods < ActiveRecord::Migration
  def change
    remove_column :underwriting_periods, :funding_arrangement
    remove_column :underwriting_periods, :plan_inception_date
    remove_column :underwriting_periods, :plan_id
  end
end
