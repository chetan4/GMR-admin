class MakeIndustriesOptional < ActiveRecord::Migration
  def up
    change_column :companies, :industries, :string, null: true
  end
  def down
    Company.update_all(:industries => ["ADVERTISEMENT"].to_json)
    change_column :companies, :industries, :string, null: false
  end
end
