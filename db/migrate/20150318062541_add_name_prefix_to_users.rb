class AddNamePrefixToUsers < ActiveRecord::Migration
  def change
    add_column :users, :name_prefix, :string, limit: 10
  end
end
