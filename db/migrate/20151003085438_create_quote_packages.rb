class CreateQuotePackages < ActiveRecord::Migration
  def change
    create_table :quote_packages do |t|
      t.string :unique_id
      t.string :name
      t.integer :employer_company_id, null: false
      t.integer :benefit_type_id, null: false
      t.integer :created_by_user_id, null: false
      t.timestamps null: false
    end
    add_index :quote_packages, [:employer_company_id, :benefit_type_id], name: 'QUOTE_PACKAGE_INDEX'
    add_index :quote_packages, :unique_id, unique: true
  end
end
