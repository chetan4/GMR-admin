class CreateBusinessPartners < ActiveRecord::Migration
  def change
    create_table :business_partners do |t|
      t.integer :carrier_id, null: false
      t.integer :partner_id, null: false
      t.timestamps null: false
    end
  end
end
