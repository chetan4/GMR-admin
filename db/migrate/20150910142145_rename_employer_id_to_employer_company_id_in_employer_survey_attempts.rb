class RenameEmployerIdToEmployerCompanyIdInEmployerSurveyAttempts < ActiveRecord::Migration
  def change
    rename_column(:employer_survey_attempts, :employer_id, :employer_company_id)
  end
end
