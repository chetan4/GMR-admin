class AddTerminationStatusForOpenCensus < ActiveRecord::Migration
  def up
    conversion_utilities = ConversionUtility.open
    status = { "termination_status" => {"Terminated" => "Y"} }
    conversion_utilities.each do |utility|
      unless utility.metadata.has_key?("termination_status")
        utility.metadata.merge!(status)
        utility.save!
      end
    end
  end

  def down
    conversion_utilities = ConversionUtility.open
    conversion_utilities.each do |utility|
      unless utility.metadata.key?("termination_status")
        utility.metadata.delete("termination_status")
        utility.save!
      end
    end
  end
end
