class CreateConversationMessages < ActiveRecord::Migration
  def change
    create_table :conversation_messages do |t|
      t.references :conversation, index: true
      t.text :message
      t.integer :created_by_id, null: false
      t.timestamps null: false
    end
  end
end
