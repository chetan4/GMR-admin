class AddColumnsToTemplateInputs < ActiveRecord::Migration
  def change
    add_column :template_inputs, :template_section_id, :integer, null: false
    add_column :template_inputs, :template_field_id, :integer, null: false
    add_column :template_inputs, :template_column_id, :integer, null: false
  end
end
