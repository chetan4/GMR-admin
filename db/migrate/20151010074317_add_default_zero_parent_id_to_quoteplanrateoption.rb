class AddDefaultZeroParentIdToQuoteplanrateoption < ActiveRecord::Migration
  def change
    change_column :quote_plan_rate_options, :parent_id, :integer, :default => 0
  end
end
