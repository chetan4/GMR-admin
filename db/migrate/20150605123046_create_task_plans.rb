class CreateTaskPlans < ActiveRecord::Migration
  def change
    create_table :task_plans do |t|
      t.integer :task_id, null: false
      t.integer :associated_plan_id, null: false

      t.timestamps null: false
    end
  end
end
