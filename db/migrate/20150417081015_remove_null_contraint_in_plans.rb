class RemoveNullContraintInPlans < ActiveRecord::Migration
  def up
    change_column :plans, :carrier_company_id, :integer, null: true
  end

  def down
    change_column :plans, :carrier_company_id, :integer, null: false
  end
end
