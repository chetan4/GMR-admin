class CreateDataEntryTaskLogs < ActiveRecord::Migration
  def change
    create_table :data_entry_task_logs do |t|
      t.references :loggable, polymorphic: true, index: true
      t.integer :activity_type
      t.text :content
      t.integer :created_by_id

      t.timestamps null: false
    end

  end
end
