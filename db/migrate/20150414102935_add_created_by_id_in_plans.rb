class AddCreatedByIdInPlans < ActiveRecord::Migration
  def change
    add_column :plans, :created_by_id, :integer
  end
end
