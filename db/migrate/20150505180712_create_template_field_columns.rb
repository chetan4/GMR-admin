class CreateTemplateFieldColumns < ActiveRecord::Migration
  def change
    create_table :template_field_columns do |t|
      t.integer :type, null: false
      t.hstore :properties

      t.timestamps null: false
    end
  end
end
