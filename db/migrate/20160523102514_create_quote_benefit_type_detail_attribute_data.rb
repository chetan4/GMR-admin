class CreateQuoteBenefitTypeDetailAttributeData < ActiveRecord::Migration
  def change
    create_table :quote_benefit_type_detail_attribute_data do |t|
      t.integer :benefit_type_detail_attribute_id
      t.integer :quote_id
      t.string :value

      t.timestamps null: false
    end
    add_index :quote_benefit_type_detail_attribute_data, [:benefit_type_detail_attribute_id, :quote_id], unique: true, name: :quote_btd_attribute_data_uniq_index
  end
end
