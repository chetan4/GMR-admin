class ModifyIndexOnQuoteplanrateoption < ActiveRecord::Migration
  def up
    remove_index :quote_plan_rate_options, name: 'index_quote_pro_on_quotable_id'
    add_index :quote_plan_rate_options, [:quotable_id, :quotable_type], name: "index_renewal_pro_on_quotable_id", using: :btree
  end

  def down
    remove_index :quote_plan_rate_options, name: 'index_quote_pro_on_quotable_id'
    add_index :quote_plan_rate_options, [:quotable_id, :quotable_type], name: "index_renewal_pro_on_quotable_id", unique: true,  using: :btree
  end
end
