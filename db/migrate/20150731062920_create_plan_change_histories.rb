class CreatePlanChangeHistories < ActiveRecord::Migration
  def change
    create_table :plan_change_histories do |t|
      t.integer :sub_task_id, null: false
      t.integer :plan_id, null: false
      t.integer :status, null: false, default: 0
      t.date :plan_change_date, null: false
      t.integer :template_field_id, null: false
      t.integer :template_column_id, null: false
      t.decimal :from, null: false, precision: 17, scale: 7
      t.decimal :to, null: false, precision: 17, scale: 7
      t.decimal :medical_change_value, null: false, precision: 17, scale: 7
      t.decimal :rx_change_value, null: false, precision: 17, scale: 7

      t.timestamps null: false
    end
  end
end
