class AddColumnsEffectiveStartAndDateForPackagePlanRenewals < ActiveRecord::Migration
  def change
    add_column :package_plan_renewals, :effective_start_date, :datetime
    add_column :package_plan_renewals, :effective_end_date, :datetime
  end
end
