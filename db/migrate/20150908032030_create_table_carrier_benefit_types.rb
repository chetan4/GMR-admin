class CreateTableCarrierBenefitTypes < ActiveRecord::Migration
  def change
    create_table :carrier_benefit_types do |t|
      t.integer    :carrier_company_id, null: false
      t.belongs_to :benefit_type,       null: false
      t.boolean    :status,             default:  true
      t.timestamps
    end
    add_foreign_key :carrier_benefit_types, :companies, column: "carrier_company_id", name: "carrier_benefit_types_fkey_on_carrier_company_id", on_delete: :cascade
    add_foreign_key :carrier_benefit_types, :benefit_types, name: "carrier_benefit_types_fkey_on_benefit_type_id", on_delete: :cascade
  end
end
