class AddColumnsCommentsActionStatusAcceptedByUserIdToQuotes < ActiveRecord::Migration
  def change
    add_column :quotes, :comment, :text
    add_column :quotes, :action_status, :integer
    add_column :quotes, :accepted_by_user_id, :integer
  end
end
