class CreateEmployerSurveys < ActiveRecord::Migration
  def change
    create_table :employer_surveys do |t|
      t.string :name
      t.text :description

      t.timestamps null: false
    end
  end
end
