class CreatePlanRateOptionLabels < ActiveRecord::Migration
  def change
    create_table :plan_rate_option_labels do |t|
      t.references :plan_rate_option
      t.references :rate_option_label
      t.string :value

      t.timestamps null: false
    end
  end
end
