class RemoveCompanyIdColumnFromLocations < ActiveRecord::Migration
  def change
    remove_column :locations, :company_id, :integer, null: false
  end
end
