class AddColumnsToEmployeeClasses < ActiveRecord::Migration
  def change
  	add_column :employee_classes, :description, :text
  	change_column :employee_classes, :eligibility, :string, :null => true
  end
end
