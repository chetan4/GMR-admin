class CreateAuditLogs < ActiveRecord::Migration
  def change
    enable_extension :hstore

    create_table :audit_logs do |t|
      t.integer :vertical_id
      t.string :auditee_type
      t.integer :auditee_id
      t.string :event
      t.string :initiator_type
      t.integer :initiator_id
      t.hstore :activity_info

      t.timestamps null: false
    end
  end
end
