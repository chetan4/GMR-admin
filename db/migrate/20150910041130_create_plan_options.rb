class CreatePlanOptions < ActiveRecord::Migration
  def change
    create_table :plan_options do |t|
      t.references :plan, index: true, null: false
      t.string :name, null: false
      t.integer :option_type, null: false
      t.decimal :value, default: 0, null: false

      t.timestamps null: false
    end

    add_index :plan_options, [:plan_id, :name], unique: true
  end
end
