class AddIndexOnPlatformFiles < ActiveRecord::Migration
  def change
    add_index :platform_files, [:benefit_type_id, :carrier_company_id]
    add_index :platform_files, [:benefit_type_id, :carrier_company_id, :name], unique: true, name: "unique_index_on_platform_files_by_benefit_and_company_and_name"
  end
end
