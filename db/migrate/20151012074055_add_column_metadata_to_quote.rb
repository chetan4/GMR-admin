class AddColumnMetadataToQuote < ActiveRecord::Migration
  def change
    add_column :quotes, :metadata, :hstore
  end
end
