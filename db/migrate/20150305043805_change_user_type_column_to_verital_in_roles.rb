class ChangeUserTypeColumnToVeritalInRoles < ActiveRecord::Migration
  def change
    rename_column :roles, :user_type, :vertical
  end
end
