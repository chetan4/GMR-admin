class CreateRoles < ActiveRecord::Migration
  def up
    create_table :roles do |t|
      t.string :user_type
      t.belongs_to :company
      t.string :name
    end
    add_index :roles, [:user_type, :company_id]
  end
end
