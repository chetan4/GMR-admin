class AddUniqueIndexOnVertical < ActiveRecord::Migration
  def up
    Vertical.destroy_all
    Rake::Task['seed:populate_verticals'].invoke
    ActiveRecord::Base.connection.reset_pk_sequence!('verticals')
    add_index :verticals, :name, :unique => true
  end
  def down
    remove_index :verticals, :name
  end
end
