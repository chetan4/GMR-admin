class AddTermsAndConditionsDescriptionToRenewalPlans < ActiveRecord::Migration
  def change
  	add_column :package_plan_renewals, :terms_and_conditions_description, :text
  	add_column :package_plan_renewals, :attachment_id, :integer
  end
end
