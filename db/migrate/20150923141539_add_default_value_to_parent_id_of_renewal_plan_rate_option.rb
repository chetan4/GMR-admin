class AddDefaultValueToParentIdOfRenewalPlanRateOption < ActiveRecord::Migration
  def change
    change_column :renewal_plan_rate_options, :parent_id, :integer, :default => 0
  end
end
