class CreateCensusesEmployeeBenefitPlan < ActiveRecord::Migration
  def change
    create_table :censuses_employee_benefit_plan do |t|
      t.references :census_employee
      t.integer :employer_company_id
      t.references :plan
      t.references :benefit_type
      t.references :census

      t.timestamps null: false
    end

    add_index :censuses_employee_benefit_plan, [:employer_company_id, :benefit_type_id, :census_employee_id], unique: true, name: 'UNIQUE_INDEX'
    add_index :censuses_employee_benefit_plan, [:census_employee_id, :benefit_type_id], name: 'INDEX1'
  end
end
