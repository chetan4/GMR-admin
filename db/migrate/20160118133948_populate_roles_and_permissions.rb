class PopulateRolesAndPermissions < ActiveRecord::Migration
  def change
    #TODO:: at the current time, permissions are not being populated by deply script
    #TODO:: so adding this migration to populate roles and permissions.
    #TODO:: this can be removed later.
    Rake::Task['seed:populate_roles_and_permissions'].invoke
  end
end
