class CreateEmployeeClasses < ActiveRecord::Migration
  def change
    create_table :employee_classes do |t|
      t.integer :business_unit_id, null: false
      t.integer :location_id, null: false
      t.string  :name, null:false
      t.string  :code
      t.string  :eligibility, null:false
      t.string  :waiting_period, null:false
      t.string  :hours, null:false
      t.boolean :key_employee
      t.boolean :dependent_eligible
      t.timestamps null: false
    end
    add_index :employee_classes, [:business_unit_id, :location_id]
  end
end
