class AddNameMapToIndustry < ActiveRecord::Migration
  def up
    add_column :industries, :name_map, :string
    Rake::Task['seed:populate_industries'].invoke
    add_index :industries, :name_map, unique: true, name: "UNIQUE_INDUSTRY_NAME_MAP"
  end

  def down
    remove_index :industries, name: "UNIQUE_INDUSTRY_NAME_MAP"
    remove_column :industries, :name_map
  end
end
