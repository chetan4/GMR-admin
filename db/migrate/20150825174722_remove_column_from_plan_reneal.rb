class RemoveColumnFromPlanReneal < ActiveRecord::Migration
  def change
    remove_column :plan_renewals, :funding_arrangement
    remove_column :plan_renewals, :plan_inception_date
  end
end
