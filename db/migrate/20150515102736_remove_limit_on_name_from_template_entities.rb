class RemoveLimitOnNameFromTemplateEntities < ActiveRecord::Migration
  def change
    change_column :template_entities, :name, :string
  end
end
