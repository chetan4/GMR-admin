class AddRoleMapStatusVerticalToRoles < ActiveRecord::Migration
  def change
    add_column :roles, :role_map, :string
    add_column :roles, :available_in_vertical, :string, default: 'Admin'
    add_column :roles, :status, :boolean, default: true
  end
end
