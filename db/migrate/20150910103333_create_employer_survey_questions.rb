class CreateEmployerSurveyQuestions < ActiveRecord::Migration
  def change
    create_table :employer_survey_questions do |t|
      t.string :question
      t.integer :weightage, :default => 1
      t.integer :question_type
      t.integer :display_order
      t.integer :employer_survey_id

      t.timestamps null: false
    end
  end
end
