class AlterColumnValueForPlanRateOptionLabels < ActiveRecord::Migration
  def up
    new_values = {}
    PlanRateOptionLabel.find_each do |prol|
      new_values[prol.id] = prol.value
    end

    remove_column :plan_rate_option_labels, :value
    add_column :plan_rate_option_labels, :metadata, :hstore
    PlanRateOptionLabel.reset_column_information

    new_values.each do |id, val|
      prol = PlanRateOptionLabel.find(id)
      prol.update_attributes!(metadata: {value: val})
    end
  end

  def down
    new_values = {}
    PlanRateOptionLabel.find_each do |prol|
      new_values[prol.id] = prol[:metadata][:value]
    end

    remove_column :plan_rate_option_labels, :metadata
    add_column :plan_rate_option_labels, :value, :string
    PlanRateOptionLabel.reset_column_information

    new_values.each do |id, val|
      prol = PlanRateOptionLabel.find(id)
      prol.update_attributes!(value: val)
    end
  end
end
