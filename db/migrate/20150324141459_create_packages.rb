class CreatePackages < ActiveRecord::Migration
  def change
    create_table :packages do |t|
      t.integer :carrier_id, null: false
      t.integer :employer_id, null: false
      t.string :name, null: false

      t.timestamps null: false
    end
  end
end
