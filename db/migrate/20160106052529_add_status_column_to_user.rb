class AddStatusColumnToUser < ActiveRecord::Migration
  def change
    add_column :users, :status, :string, default: 'Activate'
  end
end
