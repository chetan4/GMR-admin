class CreateDataEntryTaskAttachments < ActiveRecord::Migration
  def change
    create_table :data_entry_task_attachments do |t|
      t.belongs_to :data_entry_task, null: false
      t.integer :attachment_id, null: false
      t.integer :tagged_by

      t.timestamps null: false
    end

    add_index :data_entry_task_attachments, [:data_entry_task_id, :attachment_id], unique: true, name: 'UNIQUE'
  end
end
