class RenameAvailableInVerticalToAvailableInVerticalId < ActiveRecord::Migration
  def up
    add_column :roles, :available_in_vertical_id, :integer
    Rake::Task['seed:move_available_in_vertical_to_available_in_vertical_id'].invoke
    remove_column :roles, :available_in_vertical
  end
  def down
    add_column :roles, :available_in_vertical, :string
    Rake::Task['seed:move_available_in_vertical_id_to_available_in_vertical'].invoke
    remove_column :roles, :available_in_vertical_id
  end
end
