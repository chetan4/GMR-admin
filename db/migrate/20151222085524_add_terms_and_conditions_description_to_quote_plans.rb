class AddTermsAndConditionsDescriptionToQuotePlans < ActiveRecord::Migration
  def change
  	add_column :quotes, :terms_and_conditions_description, :text
  	add_column :quotes, :attachment_id, :integer
  end
end
