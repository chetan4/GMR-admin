class RemoveBenefitTypeDetailsIdAndMetadataFromPackageOptionRenewalOption < ActiveRecord::Migration

  def self.up
    remove_column :package_option_renewal_options, :benefit_type_detail_id
    remove_column :package_option_renewal_options, :metadata
  end

  def self.down
    add_column :package_option_renewal_options, :benefit_type_detail_id, :string
    add_column :package_option_renewal_options, :metadata, :hstore
  end

end
