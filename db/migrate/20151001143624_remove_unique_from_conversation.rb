class RemoveUniqueFromConversation < ActiveRecord::Migration
  def change
    remove_index :conversations, column: :unique_id # Removing uniqueness constraint
    add_index :conversations, :unique_id, unique: false
  end
end
