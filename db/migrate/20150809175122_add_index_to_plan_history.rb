class AddIndexToPlanHistory < ActiveRecord::Migration
  def change
    add_index :plan_change_histories, [:sub_task_id, :plan_change_date, :template_field_id, :template_column_id], unique: true, name: 'UNIQUE_PLAN_CHANGE_RECORD'
  end
end
