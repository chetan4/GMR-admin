class CreateContributionModellingData < ActiveRecord::Migration
  def change
    create_table :contribution_modelling_data do |t|
      t.integer   :contribution_model_id, null: false
      t.integer   :plan_id, null: false
      t.integer   :employee_class_id, null: false
      t.integer   :state, null: false, default: 0
      t.integer  :contribution_type, default: 0
      t.integer  :contribution_scheme, default: 0
      t.string    :contribution_name, null: false
      t.integer   :frequency
      t.decimal   :buyout_value, precision: 17, scale: 2
      t.integer  :modelling_type, default: 0
      t.hstore    :data

      t.timestamps null: false
    end

    add_index :contribution_modelling_data, [:contribution_model_id, :plan_id, :employee_class_id], name: 'contribution_modelling_data_index', unique: true
  end
end