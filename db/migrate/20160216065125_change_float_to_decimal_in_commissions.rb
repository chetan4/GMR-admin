class ChangeFloatToDecimalInCommissions < ActiveRecord::Migration
  def change
    change_column :commissions, :compensation, :decimal, precision: 17, scale: 2
    change_column :commissions, :estimated_annual_compensation, :decimal, precision: 17, scale: 2
    change_column :commissions, :contingent_commission, :decimal, precision: 17, scale: 2
    rename_column :commissions, :general_paid_agency, :general_agency_paid
  end
end
