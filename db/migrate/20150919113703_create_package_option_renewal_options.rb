class CreatePackageOptionRenewalOptions < ActiveRecord::Migration
  def change
    create_table :package_option_renewal_options do |t|
      t.references :package_option
      t.references :package_plan_renewal
      t.references :benefit_type_detail
      t.integer :status # entry_in_progress, entry_complete, review_in_progress, verified
      t.hstore :metadata

      t.timestamps null: false
    end

    add_index :package_option_renewal_options, [:package_option_id, :package_plan_renewal_id, :benefit_type_detail_id ], unique: true, name: "PACKAGE_OPTION_RO_UNIQUE_INDEX"
  end
end
