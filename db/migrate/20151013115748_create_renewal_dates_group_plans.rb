class CreateRenewalDatesGroupPlans < ActiveRecord::Migration
  def change
    create_table :renewal_dates_group_plans do |t|
      t.references :renewal_dates_group
      t.references :plan
      t.timestamps null: false
    end
  end
end
