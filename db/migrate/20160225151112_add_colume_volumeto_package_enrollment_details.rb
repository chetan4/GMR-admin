class AddColumeVolumetoPackageEnrollmentDetails < ActiveRecord::Migration
  def change
    add_column :package_enrollment_details, :volume, :decimal, precision: 17, scale: 2, default: 0
    add_column :package_enrollment_details, :multiply_factor, :decimal, precision: 17, scale: 2, default: 0
  end
end
