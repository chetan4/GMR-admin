class ChangeColumnAttachmentIdOfQuotes < ActiveRecord::Migration
  def up
    remove_column :quotes, :attachment_id
    add_column :quotes, :attachment_id, :text
  end

  def down
  end
end
