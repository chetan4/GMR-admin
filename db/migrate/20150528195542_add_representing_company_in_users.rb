class AddRepresentingCompanyInUsers < ActiveRecord::Migration
  def change
    add_column :users, :representing_company_id, :integer
  end
end
