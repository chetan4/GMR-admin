class CreateRateOptionLabels < ActiveRecord::Migration
  def change
    create_table :rate_option_labels do |t|
      t.references :rate_option
      t.string :name
      t.string :name_map
      t.boolean :mandatory, default: false

      t.timestamps null: false
    end
  end
end
