class CreateSimpleCurrentRateComponents < ActiveRecord::Migration
  def change
    create_table :simple_current_rate_components do |t|
      t.integer :underwriting_id, null: false
      t.integer :members
      t.decimal :annual_premium, precision: 17, scale: 2
      t.decimal :pooling, precision: 17, scale: 2
      t.decimal :expenses, precision: 17, scale: 2
      t.decimal :claims, precision: 17, scale: 2
      t.decimal :margin, precision: 17, scale: 2
      t.decimal :reserve, precision: 17, scale: 2
      t.integer :publish_status, default: 0
      t.integer :funding_arrangement
      t.datetime :from_date
      t.datetime :to_date
      t.decimal :credibility, precision: 17, scale: 2
      t.decimal :manual_rate_action, precision: 17, scale: 2
      t.integer :flag

      t.timestamps null: false
    end
    add_index :simple_current_rate_components, :underwriting_id
  end
end
