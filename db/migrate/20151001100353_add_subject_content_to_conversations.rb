class AddSubjectContentToConversations < ActiveRecord::Migration
  def change
    add_column :conversations, :subject, :text
    add_column :conversations, :content, :text
  end
end
