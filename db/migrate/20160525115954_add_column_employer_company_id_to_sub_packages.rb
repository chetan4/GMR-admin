class AddColumnEmployerCompanyIdToSubPackages < ActiveRecord::Migration
  def change
    add_column :subpackages, :employer_company_id, :integer
  end
end
