class AddInformationColumnsToDataEntryTasks < ActiveRecord::Migration
  def change
    add_column :data_entry_tasks, :request_information, :integer, default: 0
    add_column :data_entry_tasks, :requested_by_id, :integer
  end
end
