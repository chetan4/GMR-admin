class ChangeColumnAttachmentIdOfPackagePlanRenewals < ActiveRecord::Migration
  def up
    remove_column :package_plan_renewals, :attachment_id
    add_column :package_plan_renewals, :attachment_id, :text
  end

  def down
  end
end
