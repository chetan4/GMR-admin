class UpdateContributionDatumMetadata < ActiveRecord::Migration
  def up
    ContributionDatum.all.each do |contribution|
      unless contribution.salary_based_or_tiered?
        contribution.data['metadata'] = { 'salary_bracket' => ContributionService::DEFAULT_SALARY_BRACKET_HASH}
        contribution.save!
      end
    end
  end

  def down
    ContributionDatum.all.each do |contribution|
      unless contribution.salary_based_or_tiered?
        contribution.data['metadata'] = {}
        contribution.save!
      end
    end
  end
end
