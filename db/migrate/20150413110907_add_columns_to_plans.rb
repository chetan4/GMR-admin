class AddColumnsToPlans < ActiveRecord::Migration
  def up
    change_column :plans, :name, :string, limit: 50

    add_column :plans, :employer_id, :integer

    add_column :plans, :carrier_contact_name, :string, limit: 50
    add_column :plans, :carrier_contact_email, :string, limit: 50
    add_column :plans, :carrier_work_number, :string, limit: 20
    add_column :plans, :carrier_work_extension, :string, limit: 6

    add_column :plans, :years_with_carrier, :integer
    add_column :plans, :old_carrier_id, :integer

    add_column :plans, :policy_number, :string, limit: 50
    add_column :plans, :contribution, :string

    add_column :plans, :broker_id, :integer
    add_column :plans, :broker_contact_name, :string, limit: 50
    add_column :plans, :broker_contact_email, :string, limit: 50
    add_column :plans, :broker_work_number, :string, limit: 20
    add_column :plans, :broker_work_extension, :string, limit: 6

    add_column :plans, :authorized_to_contact, :boolean, default: false
  end
  def down
    change_column :plans, :name, :string

    remove_column :plans, :employer_id

    remove_column :plans, :carrier_contact_name
    remove_column :plans, :carrier_contact_email
    remove_column :plans, :carrier_work_number
    remove_column :plans, :carrier_work_extension

    remove_column :plans, :years_with_carrier
    remove_column :plans, :old_carrier_id

    remove_column :plans, :policy_number
    remove_column :plans, :contribution

    remove_column :plans, :broker_id
    remove_column :plans, :broker_contact_name
    remove_column :plans, :broker_contact_email
    remove_column :plans, :broker_work_number
    remove_column :plans, :broker_work_extension

    remove_column :plans, :authorized_to_contact
  end
end
