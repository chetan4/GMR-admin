class AddVersionToTemplates < ActiveRecord::Migration
  def change
    add_column :templates, :version, :float
  end
end
