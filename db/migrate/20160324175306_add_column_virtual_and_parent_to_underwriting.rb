class AddColumnVirtualAndParentToUnderwriting < ActiveRecord::Migration
  def change
    add_column :underwritings, :underwriting_type, :integer, default: 0, null: false
    add_column :underwritings, :parent_id, :integer, default: -1
  end
end
