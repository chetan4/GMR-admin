class UpdateColumeVolumeToQuotePackageEnrollmentDetail < ActiveRecord::Migration
  def change
    change_column :quote_package_enrollment_details, :volume, :decimal, precision: 17, scale: 3, default: 0
  end
end
