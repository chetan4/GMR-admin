class ModifyDataEntryTaskTable < ActiveRecord::Migration
  def change
    drop_table :data_entry_tasks, {}

    create_table :data_entry_tasks do |t|
      t.string :type
      t.integer :parent_task_id, null: false, default: 0
      t.belongs_to :benefit_type_detail
      t.belongs_to :plan
      t.references :taskable, polymorphic: true, index: true
      t.integer :level, null: false, default: 0 # default priority normal
      t.integer :activity, null: false, default: 0 # default quote
      t.integer :status, null: false, default: 0 # default open
      t.integer :assigned_to_id
      t.integer :assigned_by_id

      t.timestamps null: false
    end
  end
end
