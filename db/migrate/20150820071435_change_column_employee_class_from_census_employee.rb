class ChangeColumnEmployeeClassFromCensusEmployee < ActiveRecord::Migration
  def up
    census_employee_class_hash = {}
    CensusEmployee.all.each do |ce|
      census_employee_class = EmployeeClass.overall.find_by(name: ce.employee_class)
      if census_employee_class.present?
        census_employee_class_hash[ce.id] = census_employee_class.id
      else
        census_employee_class_hash[ce.id] = EmployeeClass.first.id
      end
    end
    remove_column :censuses_employee, :employee_class
    add_column :censuses_employee, :employee_class_id, :integer
    CensusEmployee.reset_column_information

    CensusEmployee.all.each do |ce|
      ce.employee_class_id = census_employee_class_hash[ce.id]
      ce.save
    end
  end

  def down
    remove_column :censuses_employee, :employee_class_id, :integer
    add_column :censuses_employee, :employee_class, :string
  end
end
