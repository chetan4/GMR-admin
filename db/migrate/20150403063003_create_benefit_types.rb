class CreateBenefitTypes < ActiveRecord::Migration
  def change
    create_table :benefit_types do |t|
      t.string :name, null: false
      t.string :name_map, null: false
      t.timestamps null: false
    end

    add_index :benefit_types, :name_map, unique: true
  end
end
