class RenameColumnInUserSecurityQuestion < ActiveRecord::Migration
  def change
    rename_column :user_security_questions, :question_id, :security_question_id
  end
end
