class CreateSimpleProjectRenewals < ActiveRecord::Migration
  def change
    create_table :simple_project_renewals do |t|
      t.integer :underwriting_id, null: false
      t.integer :members
      t.decimal :annual_premium, precision: 17, scale: 2
      t.decimal :pooling, precision: 17, scale: 2
      t.decimal :expenses, precision: 17, scale: 2
      t.decimal :claims, precision: 17, scale: 2
      t.decimal :margin, precision: 17, scale: 2
      t.decimal :reserve, precision: 17, scale: 2
      t.integer :publish_status, default: 0
      t.timestamps null: false
    end
    add_index :simple_project_renewals, :underwriting_id
  end
end
