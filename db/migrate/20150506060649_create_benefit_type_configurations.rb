class CreateBenefitTypeConfigurations < ActiveRecord::Migration
  def change
    create_table :benefit_type_configurations do |t|
      t.integer :benefit_type_id, null: false
      t.integer :type, null: false
      t.string :key, limit: 100, null: false
      t.text :value

      t.timestamps null: false
    end
  end
end
