class CreateServiceRequests < ActiveRecord::Migration
  def change
    create_table :service_requests do |t|
      t.integer :employer_location_id, null: false
      t.integer :broker_id
      t.integer :carrier_id, null: false
      t.integer :plan_id, null: false
      t.string  :status
      t.integer :service_representative_id
      t.timestamps null: false
    end
  end
end
