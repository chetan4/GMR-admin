class AddColumnFinalizeStatusToPackagePlanRenewal < ActiveRecord::Migration
  def up
    add_column :package_plan_renewals, :finalize_status, :integer, default: 0
  end

  def down
    remove_column :package_plan_renewals, :finalize_status
  end
end
