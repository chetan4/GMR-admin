class CreateBuyoutCalculators < ActiveRecord::Migration
  def change
    create_table :buyout_calculators do |t|
      t.integer :company_id
      t.integer :current_enrollment
      t.integer :current_waivers
      t.decimal :proposed_annual_buyout_amount, precision: 17, scale: 2
      t.integer :additional_waivers
      t.timestamps null: false
    end
    add_index :buyout_calculators, [:company_id]
  end
end
