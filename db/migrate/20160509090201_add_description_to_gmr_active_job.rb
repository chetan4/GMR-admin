class AddDescriptionToGMRActiveJob < ActiveRecord::Migration
  def change
    add_column :gmr_active_jobs, :description, :string
    add_column :gmr_active_jobs, :source_id, :integer
    add_column :gmr_active_jobs, :source_type, :string
    add_column :gmr_active_jobs, :metadata, :hstore
    add_column :gmr_active_jobs, :job_type, :string
    add_index :gmr_active_jobs, :source_id
  end
end
