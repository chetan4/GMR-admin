class AllowNullBenefitCategory < ActiveRecord::Migration
  def up
    change_column :benefit_types, :benefit_category_id, :integer, null: true
  end
  def down
    change_column :benefit_types, :benefit_category_id, :integer, null: false
  end
end
