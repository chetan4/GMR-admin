class AddColumnReserveFactorToPlanRenewals < ActiveRecord::Migration
  def change
    add_column :plan_renewals, :reserve_factor, :integer
  end
end
