class RemoveNameColumnFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :name, :string, limit: 50
  end
end
