class AddColumnVolumeToQuotePlanRateOptions < ActiveRecord::Migration
  def change
    add_column :quote_plan_rate_options, :volume, :decimal, precision: 17, scale: 2
  end
end
