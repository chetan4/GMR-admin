class UpdateEmployerCompanyToUnderwritting < ActiveRecord::Migration
  def change
    underwritings = Underwriting.all
    underwritings.each do |underwriting|
      employer_company_id = underwriting.plan_renewals.first.plan.employer_company_id rescue nil
      next if employer_company_id.nil? || underwriting.employer_company_id.present?
      underwriting.employer_company_id = employer_company_id
      underwriting.save
    end
  end
end
