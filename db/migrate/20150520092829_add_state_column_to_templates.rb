class AddStateColumnToTemplates < ActiveRecord::Migration
  def change
    add_column :templates, :state, :integer, default: 0, null: false
  end
end
