class UpdateRateAndQuoteEffectiveDateInQuotes < ActiveRecord::Migration
  def change
    change_column :quotes, :rate_effective_date, :date, :null => true
    change_column :quotes, :quote_effective_date, :date, :null => true
  end
end
