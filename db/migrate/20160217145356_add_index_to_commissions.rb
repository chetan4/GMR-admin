class AddIndexToCommissions < ActiveRecord::Migration
  def change
    add_index :commission_plans, :commission_id, unique: true
  end
end
