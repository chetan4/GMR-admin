class CreateRenewalDatesGroups < ActiveRecord::Migration
  def change
    create_table :renewal_dates_groups do |t|
      t.date :renewal_date
      t.integer :status, default: 0
      t.timestamps null: false
    end
  end
end
