class PopulateUserSizes < ActiveRecord::Migration
  def up
    Rake::Task['seed:populate_user_sizes'].invoke
    Rake::Task['onetime:assign_priority_to_benefit_types'].invoke
  end

  def down

  end
end
