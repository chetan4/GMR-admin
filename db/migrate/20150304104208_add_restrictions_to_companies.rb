class AddRestrictionsToCompanies < ActiveRecord::Migration
  def up
    Company.destroy_all
    change_column :companies, :name, :string, null: false
    change_column :companies, :vertical, :string, null: false
    change_column :companies, :industries, :string, null: false
    change_column :companies, :website, :string, null: false
    change_column :companies, :address, :text, null: false
  end

  def down
    change_column :companies, :name, :string
    change_column :companies, :vertical, :string
    change_column :companies, :industries, :string
    change_column :companies, :website, :string
    change_column :companies, :address, :text
  end
end
