class AddColumnsProjectedRenewalCredibilityAndmanualRateTosimpleProjectRenewals < ActiveRecord::Migration
  def change
    add_column :simple_project_renewals, :projected_renewal, :decimal, precision: 17, scale: 2
    add_column :simple_project_renewals, :credibility, :decimal, precision: 17, scale: 2
    add_column :simple_project_renewals, :manual_rate_action, :decimal, precision: 17, scale: 2
  end
end
