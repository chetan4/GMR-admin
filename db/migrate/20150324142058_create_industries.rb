class CreateIndustries < ActiveRecord::Migration
  def up
    create_table :industries do |t|
      t.string :name, null: false
    end
    # Rake task commented by Kapil, populate industries rake task is now executed after name_map addition
    # to the table, in migration 20151008093101_add_name_map_to_industry
    # Rake::Task['seed:populate_industries'].invoke
  end

  def down
    drop_table :industries
  end
end
