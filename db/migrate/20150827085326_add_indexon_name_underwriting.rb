class AddIndexonNameUnderwriting < ActiveRecord::Migration
  def change
    add_index :underwritings, [:name], unique: true
  end
end
