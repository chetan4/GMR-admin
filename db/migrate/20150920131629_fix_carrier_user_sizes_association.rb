class FixCarrierUserSizesAssociation < ActiveRecord::Migration
  def change
    rename_table :user_sizes, :user_handling_sizes
    drop_table :carrier_user_sizes
    drop_table :carrier_user_locations
    drop_table :user_benefit_types
    create_table :carrier_user_benefit_type_details do |t|
      t.belongs_to :user, null: false
      t.belongs_to :benefit_type
      t.belongs_to :user_handling_size
      t.text       :locations, array:true, default: []
      t.timestamps        null: false
    end
    add_foreign_key :carrier_user_benefit_type_details, :users, name: "carrier_user_benefit_type_details_fkey_on_user_id", on_delete: :cascade
  end
end
