class ModifyConversations < ActiveRecord::Migration
  def change
    remove_index :conversations, name: 'CONVERSATION_INDEX'
    drop_table :conversations

    create_table :conversations do |t|
      t.integer :employer_company_id
      t.integer :carrier_company_id
      t.references :benefit_type, null: false
      t.string :type, null: false
      t.text :subject
      t.integer :created_by_id, null: false

      t.timestamps null: false
    end

    add_index :conversations, [:employer_company_id, :benefit_type_id, :carrier_company_id], name: 'CONVERSATION_INDEX'
  end
end
