class AddTemplateIdToDataEntryTasks < ActiveRecord::Migration
  def change
    add_column :data_entry_tasks, :template_id, :integer
  end
end
