class RemoveColumnReserveFactorFromUnderwritingPeriods < ActiveRecord::Migration
  def change
    remove_column :underwriting_periods, :reserve_factor
  end
end
