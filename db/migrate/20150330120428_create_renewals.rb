class CreateRenewals < ActiveRecord::Migration
  def change
    create_table :renewals do |t|
      t.integer :plan_id, null: false
      t.boolean :out_to_bid, null: false
      t.integer :renewal_representative_id
      t.timestamps null: false
    end
  end
end
