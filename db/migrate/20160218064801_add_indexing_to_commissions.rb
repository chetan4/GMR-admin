class AddIndexingToCommissions < ActiveRecord::Migration
  def change
    add_index :commissions, :plan_id, unique: false
    add_index :commissions, :employer_company_id, unique: false
  end
end
