class CreateRenewalBenefitTypeDetailAttributeData < ActiveRecord::Migration
  def change
    create_table :renewal_benefit_type_detail_attribute_data do |t|
      t.integer :benefit_type_detail_attribute_id
      t.integer :renewal_id
      t.string :value

      t.timestamps null: false
    end
    add_index :renewal_benefit_type_detail_attribute_data, [:benefit_type_detail_attribute_id, :renewal_id], unique: true, name: :renewal_benefit_type_detail_attribute_data_uniq_index
  end
end
