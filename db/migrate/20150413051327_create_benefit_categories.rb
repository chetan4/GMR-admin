class CreateBenefitCategories < ActiveRecord::Migration
  def change
    create_table :benefit_categories do |t|
      t.string :permalink, null: false, limit: 150
      t.string :name, null: false, limit: 100
      t.string :description
    end
  end
end
