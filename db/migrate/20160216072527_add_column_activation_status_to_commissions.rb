class AddColumnActivationStatusToCommissions < ActiveRecord::Migration
  def change
    add_column :commissions, :activation_status, :integer, default: 0
  end
end
