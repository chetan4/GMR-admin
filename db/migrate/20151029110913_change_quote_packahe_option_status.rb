class ChangeQuotePackaheOptionStatus < ActiveRecord::Migration
  def change
    QuotePackageOption.all.each do |quote_package_option|
      quote_plan_ids = QuotePackageOptionQuote.where(:quote_package_option_id => quote_package_option.id).pluck(:quote_id)
      quote_plan_status = Quote.where(id: quote_plan_ids).map(&:status).uniq
      if quote_plan_status == [Quote.statuses.invert[Quote.statuses['reviewed']]]
        quote_package_option.update_attributes!(status: QuotePackageOption.statuses['reviewed'])
      elsif quote_package_option.status ==  QuotePackageOption.statuses.invert[QuotePackageOption.statuses['reviewed']]
        quote_package_option.update_attributes!(status: QuotePackageOption.statuses['review_in_progress'])
      end
    end
  end
end
