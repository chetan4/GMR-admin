class AddWorkExtensionToUsers < ActiveRecord::Migration
  def change
    add_column :users, :work_extension, :string, limit: 6
  end
end
