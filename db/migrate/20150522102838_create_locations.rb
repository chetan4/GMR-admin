class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :name, null:false
      t.string :code, null:false
      t.integer :company_id, null:false
      t.timestamps null: false
    end
    add_index :locations, :company_id
  end
end
