class AddColumnToEmployerSurveyAttempt < ActiveRecord::Migration
  def change
    add_column :employer_survey_attempts, :employer_survey_id, :integer
  end
end
