class AddBuyoutBenefitType < ActiveRecord::Migration
  def up
    Rake::Task['seed:populate_benefit_types'].invoke
    Rake::Task['seed:populate_rate_options'].invoke
  end

  def down
  end
end
