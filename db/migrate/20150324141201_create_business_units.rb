class CreateBusinessUnits < ActiveRecord::Migration
  def up
    create_table :business_units do |t|
      t.string :name, null: false
      t.string :code
      t.integer :company_id
    end
    Rake::Task['seed:populate_business_units'].invoke
  end

  def down
    drop_table :business_units
  end
end
