class CreateEmployerSurveyAttempts < ActiveRecord::Migration
  def change
    create_table :employer_survey_attempts do |t|
      t.integer :employer_survey_question_id, null: false
      t.integer :employer_survey_answer_id
      t.integer :employer_id,  null: false

      t.timestamps null: false
    end

    add_index :employer_survey_attempts, [:employer_id, :employer_survey_question_id], unique: true, name: 'UNIQUE_EMPLOYER_SURVEY_ATTEMPTS'
  end
end
