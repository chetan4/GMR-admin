class AddMetadataAndEventAssignedByColumnToDataEntryTasks < ActiveRecord::Migration
  def change
    add_column :data_entry_tasks, :assigned_by, :integer, after: :assigned_to
    add_column :data_entry_tasks, :event, :integer, after: :assigned_by
    add_column :data_entry_tasks, :metadata, :hstore, after: :event
  end
end
