class DropCensusDatum < ActiveRecord::Migration
  def change
    drop_table :census_data, {}
  end
end
