class ChangeEmployeeClasses < ActiveRecord::Migration
  def change
    remove_column :employee_classes, :dependent_child_coverage
    add_column :employee_classes, :dependent_child_coverage, :integer
  end
end
