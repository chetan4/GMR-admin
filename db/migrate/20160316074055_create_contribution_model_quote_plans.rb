class CreateContributionModelQuotePlans < ActiveRecord::Migration
  def change
    create_table :contribution_model_quote_plans do |t|
      t.references :contribution_model
      t.integer :quote_type, default: 0
      t.hstore :quote_type_data
      t.hstore :data

      t.timestamps null: false
    end

    add_index :contribution_model_quote_plans, [:contribution_model_id], unique: true, name: 'UNIQUE_CONTRIBUTION_MODEL_QUOTE_PLANS'

  end
end
