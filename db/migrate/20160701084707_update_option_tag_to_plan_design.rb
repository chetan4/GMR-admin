class UpdateOptionTagToPlanDesign < ActiveRecord::Migration
  def change
    quote_pkg_options = QuotePackageOption.where.not(tag: nil)
    quote_pkg_options.each do |quote_pkg|
      if quote_pkg.tag["quote_option"].present? && quote_pkg.tag["quote_option"] == 'option'
        tags =  quote_pkg.tag
        tags["quote_option"] = 'plan_design'
        quote_pkg.update_attributes!(tag: tags)
      end
    end

    pkg_options = PackageOption.where.not(tag: nil)
    pkg_options.each do |pkg|
      if pkg.tag["quote_option"].present? && pkg.tag["quote_option"] == 'option'
        tags =  pkg.tag
        tags["quote_option"] = 'plan_design'
        pkg.update_attributes!(tag: tags)
      end
    end
  end
end
