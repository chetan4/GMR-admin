class AddColumnCommentSubTypeToComments < ActiveRecord::Migration
  def change
    add_column :comments, :sub_type, :integer
  end
end
