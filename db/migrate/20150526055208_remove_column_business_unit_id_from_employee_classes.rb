class RemoveColumnBusinessUnitIdFromEmployeeClasses < ActiveRecord::Migration
  def change
    remove_column :employee_classes, :business_unit_id, :integer
  end
end
