class CreateUploads < ActiveRecord::Migration
  def change
    create_table :uploads do |t|
      t.integer :company_id, null: false
      t.integer :created_by_user_id, null: false
      t.timestamps null: false
    end
    add_index  :uploads, :company_id
    add_index  :uploads, :created_by_user_id
  end
end
