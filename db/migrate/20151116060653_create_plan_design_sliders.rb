class CreatePlanDesignSliders < ActiveRecord::Migration
  def change
    create_table :plan_design_sliders do |t|
      t.references  :plan
      t.hstore      :sliders_data
      t.timestamps null: false
    end
  end
end
