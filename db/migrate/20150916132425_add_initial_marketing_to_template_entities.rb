class AddInitialMarketingToTemplateEntities < ActiveRecord::Migration
  def change
    add_column :template_entities, :initial_marketing, :boolean
  end
end
