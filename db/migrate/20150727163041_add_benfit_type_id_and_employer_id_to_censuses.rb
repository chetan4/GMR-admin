class AddBenfitTypeIdAndEmployerIdToCensuses < ActiveRecord::Migration
  def change
    add_column :censuses, :benefit_type_id, :integer
    add_column :censuses, :employer_company_id, :integer
  end
end

