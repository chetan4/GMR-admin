class CreatePlanRateOptions < ActiveRecord::Migration
  def change
    create_table :plan_rate_options do |t|
      t.references :plan
      t.integer :sub_task_id
      t.references :rate_option
      t.timestamps null: false
    end
  end
end
