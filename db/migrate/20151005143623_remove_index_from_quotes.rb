class RemoveIndexFromQuotes < ActiveRecord::Migration
  def change
    remove_index :quotes, name: 'QUOTE_INDEX'
    add_index :quotes, [:quote_package_id, :carrier_company_id, :plan_id], name: 'QUOTE_INDEX'
  end
end
