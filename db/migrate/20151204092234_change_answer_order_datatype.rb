class ChangeAnswerOrderDatatype < ActiveRecord::Migration
  def change
  	change_column :employer_survey_attempts, :answer_order, :string
  end
end
