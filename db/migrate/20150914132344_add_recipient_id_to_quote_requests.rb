class AddRecipientIdToQuoteRequests < ActiveRecord::Migration
  def change
    add_column :quote_requests, :recipient_id, :integer, :null => false
    add_foreign_key :quote_requests, :users, column: "recipient_id", name: "quote_requests_fkey_on_recipient_id", on_delete: :cascade
  end
end
