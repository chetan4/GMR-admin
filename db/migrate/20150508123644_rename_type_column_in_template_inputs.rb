class RenameTypeColumnInTemplateInputs < ActiveRecord::Migration
  def change
    rename_column :template_inputs, :type, :input_type
  end
end
