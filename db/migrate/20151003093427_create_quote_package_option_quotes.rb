class CreateQuotePackageOptionQuotes < ActiveRecord::Migration
  def change
    create_table :quote_package_option_quotes do |t|
      t.integer :quote_package_option_id, null: false
      t.integer :quote_id, null:false
      t.timestamps null: false
    end
    add_index :quote_package_option_quotes, [:quote_package_option_id, :quote_id], unique: true, name: "QUOTE_PACKAGE_OPTION_QUOTE_INDEX"
  end
end
