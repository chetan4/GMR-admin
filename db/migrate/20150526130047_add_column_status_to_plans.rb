class AddColumnStatusToPlans < ActiveRecord::Migration
  def change
    add_column :plans, :status, :integer, default: 0
  end
end
