class AddColumnStatusToEmployerSurveyAnswer < ActiveRecord::Migration
  def change
    add_column :employer_survey_answers, :status, :boolean, default: :false
  end
end
