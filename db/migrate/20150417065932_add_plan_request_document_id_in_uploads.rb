class AddPlanRequestDocumentIdInUploads < ActiveRecord::Migration
  def change
    add_column :uploads, :plan_document_request_id, :integer
  end
end
