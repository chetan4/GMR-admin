class AddDefaultToFinalizeStatus < ActiveRecord::Migration
  def change
    change_column_default(:quote_package_options, :finalize_status, 0)
    change_column_default(:package_options, :finalize_status, 0)
  end
end
