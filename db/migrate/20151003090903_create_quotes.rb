class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.integer :quote_package_id, null: false
      t.integer :plan_id, null: false
      t.string :name, null: false
      t.integer :carrier_company_id, null: false
      t.date :rate_effective_date
      t.date :quote_effective_date
      t.string :type # quote_plan, quote_plan_option
      t.integer :status
      t.integer :completed_by_user_id
      t.integer :reviewed_by_user_id
      t.hstore :tag
      t.timestamps null: false
    end
    add_index :quotes, [:quote_package_id, :carrier_company_id, :plan_id], unique: true, name: 'QUOTE_INDEX'
  end
end
