class AddColumnsToUser < ActiveRecord::Migration
  def up
    add_column :users, :name, :string, limit: 50
    add_column :users, :work_number, :string, limit: 12
    add_column :users, :cell_number, :string, limit: 12
    add_column :users, :note, :text, limit: 200

    User.update_all(:name => 'Dummy name', :work_number => '456', :cell_number => '123')

    change_column :users, :name, :string, null: false
    change_column :users, :work_number, :string, null: false
    change_column :users, :cell_number, :string, null: false
  end

  def down
    remove_column :users, :name
    remove_column :users, :work_number
    remove_column :users, :cell_number
    remove_column :users, :note
  end

end
