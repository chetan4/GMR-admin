class CreateConversionUtilities < ActiveRecord::Migration
  def change
    create_table :conversion_utilities do |t|
      t.integer :sub_task_id, null: false
      t.integer :created_by
      t.integer :last_updated_by
      t.integer :status
      t.hstore  :metadata, null: false
      t.timestamps null: false
    end
    add_index :conversion_utilities, :sub_task_id
  end
end
