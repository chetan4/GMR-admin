class ChangeVerticalColumnInRoles < ActiveRecord::Migration
  def up
    add_column :roles, :domain_id, :integer
    Rake::Task['seed:populate_domain_id_from_vertical_for_roles'].invoke
    remove_column :roles, :vertical
  end
  def down
    add_column :roles, :vertical, :string, limit: 50
    Rake::Task['seed:populate_verticals_from_domain_id_for_roles'].invoke
    remove_column :roles, :domain_id
  end
end