class AddIndexToPlanDesignSliders < ActiveRecord::Migration
  def change
    add_index :plan_design_sliders, :plan_id, unique: true
    add_index :plan_design_sliders, [:employer_company_id, :carrier_company_id, :benefit_type_id], name: :index_on_companies
  end
end
