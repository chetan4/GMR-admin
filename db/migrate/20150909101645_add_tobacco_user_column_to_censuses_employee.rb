class AddTobaccoUserColumnToCensusesEmployee < ActiveRecord::Migration

  def up
    add_column :censuses_employee, :tobacco_user, :boolean, default: false
  end

  def down
    remove_column :censuses_employee, :tobacco_user
  end

end
