class AddPublishStatusColumnToUnderwritings < ActiveRecord::Migration
  def up
    add_column :underwritings, :publish_status, :integer, default: 0
    add_column :underwritings, :employer_company_id, :integer, index: true

    underwritings = Underwriting.all.includes(:plan_renewals => [:plan => [:employer_company]])
    underwritings.each do |underwriting|
      employer_company_id = underwriting.plan_renewals.first.plan.employer_company.id
      underwriting.employer_company_id = employer_company_id
      underwriting.save!
    end
  end

  def down
    remove_column :underwritings, :employer_company_id
    remove_column :underwritings, :publish_status
  end
end
