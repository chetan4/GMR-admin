class AddPropertiesColumnToTemplateEntities < ActiveRecord::Migration
  def change
    add_column :template_entities, :properties, :hstore
  end
end
