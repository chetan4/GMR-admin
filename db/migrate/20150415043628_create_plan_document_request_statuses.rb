class CreatePlanDocumentRequestStatuses < ActiveRecord::Migration
  def change
    create_table :plan_document_request_statuses do |t|
      t.integer :plan_document_request_id
      t.integer :plan_id
      t.integer :status

      t.timestamps null: false
    end
  end
end
