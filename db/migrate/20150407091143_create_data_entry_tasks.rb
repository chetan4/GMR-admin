class CreateDataEntryTasks < ActiveRecord::Migration
  def change
    create_table :data_entry_tasks do |t|
      t.integer :company_id, null: false
      t.integer :carrier_id, null: false
      t.integer :broker_id
      t.integer :line_of_coverage
      t.integer :plan_id
      t.integer :activity, null: false, default: 0
      t.integer :status, null: false, default: 0
      t.integer :level, null: false, default: 0
      t.integer :assigned_to

      t.timestamps null: false
    end
  end
end
