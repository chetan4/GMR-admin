class CreateTemplateSections < ActiveRecord::Migration
  def change
    create_table :template_sections do |t|
      t.integer :template_sheet_id, null: false
      t.string :name, limit: 50, null: false
      t.integer :order, default: -1, null: false

      t.timestamps null: false
    end
  end
end
