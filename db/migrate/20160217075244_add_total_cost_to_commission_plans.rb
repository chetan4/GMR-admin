class AddTotalCostToCommissionPlans < ActiveRecord::Migration
  def change
    remove_column :commissions, :no_of_employees, :string
    add_column :commission_plans, :no_of_employees,:string
    add_column :commission_plans, :total_cost, :decimal, precision: 17, scale: 2
  end
end
