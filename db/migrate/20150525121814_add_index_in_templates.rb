class AddIndexInTemplates < ActiveRecord::Migration
  def change
    add_index :template_sheets, :template_id
    add_index :template_sections, :template_sheet_id
    add_index :template_entities, [:type, :template_section_id]
    add_index :template_inputs, [:template_section_id, :template_field_id, :template_column_id], unique: true, name: "unique_index_on_template_inputs_by_section_and_field_and_column"
    add_index :template_inputs, :template_section_id
    add_index :template_inputs, :template_field_id
    add_index :template_inputs, :template_column_id
  end
end
