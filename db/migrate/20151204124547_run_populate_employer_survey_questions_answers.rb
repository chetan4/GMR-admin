class RunPopulateEmployerSurveyQuestionsAnswers < ActiveRecord::Migration
  def up
    Rake::Task['seed:populate_employer_survey_questions_answers'].invoke
  end

  def down

  end
end
