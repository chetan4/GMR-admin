class AddColumnRenewalRequestDateToPlanRateHistoryAttributes < ActiveRecord::Migration
  def change
    add_column :plan_rate_history_attributes, :renewal_request_date, :date
  end
end
