class RemoveForeginKeyConstraintOnQuoteRequests < ActiveRecord::Migration
  def change
    remove_foreign_key "quote_requests", name: "quote_requests_fkey_on_requested_plan_id"
    change_column :quote_requests, :requested_plan_id, :integer
  end
end
