class ChangeColumnPlanChangeHistory < ActiveRecord::Migration
  def change
    change_column :plan_change_histories, :from, :text, limit: 50
  end
end
