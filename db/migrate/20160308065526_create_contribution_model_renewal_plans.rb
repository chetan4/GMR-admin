class CreateContributionModelRenewalPlans < ActiveRecord::Migration
  def change
    create_table :contribution_model_renewal_plans do |t|
      t.references :contribution_model
      t.integer :renewal_type, default: 0
      t.hstore :renewal_type_data
      t.hstore :data

      t.timestamps null: false
    end

    add_index :contribution_model_renewal_plans, [:contribution_model_id], unique: true, name: 'UNIQUE_CONTRIBUTION_MODEL_RENEWAL_PLANS'
  end
end

