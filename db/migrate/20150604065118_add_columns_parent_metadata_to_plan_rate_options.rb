class AddColumnsParentMetadataToPlanRateOptions < ActiveRecord::Migration
  def up
    add_column :plan_rate_options, :parent_id, :integer, default: 0
    add_column :plan_rate_options, :metadata, :hstore
  end

  def down
    remove_column :plan_rate_options, :parent_id
    remove_column :plan_rate_options, :metadata
  end
end
