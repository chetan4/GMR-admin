class ChangeIndexOnCompanies < ActiveRecord::Migration
  def up
    remove_index :companies, :permalink
    add_index :companies, :permalink, unique: true

    remove_index :companies, [:vertical, :name]
    add_index :companies, [:vertical, :permalink], unique: true
  end

  def down
    remove_index :companies, :permalink
    add_index :companies, :permalink

    remove_index :companies, [:vertical, :permalink]
    add_index :companies, [:vertical, :name], unique: true
  end
end
