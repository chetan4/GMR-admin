class AddDateOfTerminatorToCensusemployee < ActiveRecord::Migration
  def up
    add_column :censuses_employee, :date_of_termination, :date
    add_column :censuses_employee, :status, :integer, default: 0, null: false
  end
end
