class AddColumnStatusToEmployeeClasses < ActiveRecord::Migration
  def change
    add_column :employee_classes, :class_type, :integer, default: 0, null: false
  end
end
