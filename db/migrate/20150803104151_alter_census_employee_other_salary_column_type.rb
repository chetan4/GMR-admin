class AlterCensusEmployeeOtherSalaryColumnType < ActiveRecord::Migration
  def change
    remove_column :censuses_employee, :other_salary, :boolean
    add_column :censuses_employee, :other_salary, :decimal
  end
end
