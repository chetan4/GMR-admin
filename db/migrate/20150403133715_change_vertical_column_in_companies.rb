class ChangeVerticalColumnInCompanies < ActiveRecord::Migration
  def up
    add_column :companies, :domain_id, :integer
    add_index :companies, [:domain_id, :permalink], unique: true
    Rake::Task['seed:populate_domain_id_from_vertical_for_companies'].invoke

    remove_index :companies, [:vertical, :permalink]
    remove_column :companies, :vertical
  end
  def down

    add_column :companies, :vertical, :string, limit: 50
    add_index :companies, [:vertical, :permalink], unique: true
    Rake::Task['seed:populate_verticals_from_domain_id_for_companies'].invoke

    remove_index :companies, [:domain_id, :permalink]
    remove_column :companies, :domain_id
  end
end
