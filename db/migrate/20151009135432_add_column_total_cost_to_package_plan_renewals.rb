class AddColumnTotalCostToPackagePlanRenewals < ActiveRecord::Migration
  def change
    add_column :package_plan_renewals, :total_estimated_rate, :decimal, precision: 17, scale: 2
  end
end
