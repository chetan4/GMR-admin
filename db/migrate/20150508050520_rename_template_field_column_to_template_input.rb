class RenameTemplateFieldColumnToTemplateInput < ActiveRecord::Migration
  def change
    rename_table :template_field_columns, :template_inputs
  end
end
