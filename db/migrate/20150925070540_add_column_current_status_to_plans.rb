class AddColumnCurrentStatusToPlans < ActiveRecord::Migration
  def change
    add_column :plans, :current_status, :integer, default: 0
  end
end
