class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.string  :initiator_type
      t.integer :initiator_id
      t.string  :subject
      t.text    :message
      t.timestamps null: false
    end
  end
end
