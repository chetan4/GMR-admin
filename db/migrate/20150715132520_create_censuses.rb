class CreateCensuses < ActiveRecord::Migration
  def change
    create_table :censuses do |t|
      t.integer :sub_task_id
      t.integer :attachment_id
      t.integer :conversion_utility_id
      t.integer :created_by
      t.integer :last_updated_by
      t.integer :status
      t.hstore :metadata
      t.timestamps null: false
    end
    add_index :censuses, :sub_task_id
  end
end
