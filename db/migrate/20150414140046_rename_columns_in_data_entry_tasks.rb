class RenameColumnsInDataEntryTasks < ActiveRecord::Migration
  def change
    rename_column :data_entry_tasks, :company_id, :employer_company_id
    rename_column :data_entry_tasks, :carrier_id, :carrier_company_id
    rename_column :data_entry_tasks, :broker_id, :broker_company_id
    rename_column :data_entry_tasks, :line_of_coverage, :benefit_type_id
    rename_column :data_entry_tasks, :assigned_to, :assigned_to_id
    rename_column :data_entry_tasks, :assigned_by, :assigned_by_id
  end
end
