class AddColumnsToAttachments < ActiveRecord::Migration
  def change
    add_column :attachments, :original_filename, :string
    add_column :attachments, :file_size, :integer
  end
end
