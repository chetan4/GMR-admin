class CreateTemplates < ActiveRecord::Migration
  def change
    create_table :templates do |t|
      t.integer :benefit_type_id, null: false
      t.string :name, limit: 50, null: false

      t.timestamps null: false
    end
  end
end
