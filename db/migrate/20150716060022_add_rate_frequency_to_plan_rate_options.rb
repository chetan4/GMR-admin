class AddRateFrequencyToPlanRateOptions < ActiveRecord::Migration
  def change
    add_column :plan_rate_options, :rate_frequency, :integer
  end
end
