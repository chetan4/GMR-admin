class RemoveNullConstraintOnLocationsCode < ActiveRecord::Migration
  def up
    change_column :locations, :code, :string, null: true
  end
  def down
    change_column :locations, :code, :string, null: false
  end
end
