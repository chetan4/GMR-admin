class AddCurrentPlansToTemplateEntities < ActiveRecord::Migration
  def change
    add_column :template_entities, :current_plans, :boolean
  end
end
