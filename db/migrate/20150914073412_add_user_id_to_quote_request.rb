class AddUserIdToQuoteRequest < ActiveRecord::Migration
  def change
    add_column :quote_requests, :initiator_id, :integer, :null => false
    add_foreign_key :quote_requests, :users, column: "initiator_id", name: "quote_requests_fkey_on_initiator_id", on_delete: :cascade
  end
end
