class CreatePlanRenewals < ActiveRecord::Migration
  def change
    create_table :plan_renewals do |t|
      t.references :underwriting
      t.references :plan

      t.integer :scenario, default: 0
      t.date :date_issue, null: false

      t.decimal :initial_renewal, precision: 10, scale: 7, null: false
      t.decimal :current_renewal_offer, precision: 10, scale: 7
      t.decimal :final_renewal, precision: 10, scale: 7

      t.integer :funding_arrangement, default: 0
      t.date :plan_inception_date

      t.date :renewal_begin_date
      t.date :renewal_end_date

      t.decimal :pooling_point, precision: 17, scale: 7, null: false
      t.decimal :pooling_point_max_claim, precision: 10, scale: 7, null: false
      t.decimal :medical_trend, precision: 10, scale: 7, null: false
      t.decimal :rx_trend, precision: 10, scale: 7, null: false
      t.decimal :margin, precision: 10, scale: 7, null: false

      t.integer :live_basis, default: 0
      t.decimal :experience_credibility, precision: 10, scale: 7, null: false

      t.hstore :capitation, null: false
      t.hstore :pool_charges, null: false

      t.hstore :retention, null: false
      t.hstore :taxes_1, null: false
      t.hstore :taxes_2, null: false
      t.hstore :taxes_3, null: false
      t.hstore :commission, null: false

      t.references :created_by
      t.timestamps null: false
    end
  end
end
