class AddDateOfPaymentToCommissions < ActiveRecord::Migration
  def change
    add_column :commissions, :date_of_payment, :date
    add_column :commissions, :contingent_commission, :float
  end
end
