class NewQuoteRequests < ActiveRecord::Migration
  def change
    drop_table :quote_requests
    create_table :quote_requests do |t|
      t.belongs_to :benefit_type
      t.integer    :carrier_company_id, null: false
      t.integer    :employer_company_id, null: false
      t.boolean    :status,             default:  true
      t.integer    :requested_plan_id, null: false
      t.timestamps

    end
    add_foreign_key :quote_requests, :companies, column: "carrier_company_id", name: "quote_requests_fkey_on_carrier_company_id", on_delete: :cascade
    add_foreign_key :quote_requests, :companies, column: "employer_company_id", name: "quote_requests_fkey_on_employer_company_id", on_delete: :cascade
    add_foreign_key :quote_requests, :benefit_types, name: "quote_requests_fkey_on_benefit_type_id", on_delete: :cascade
    add_foreign_key :quote_requests, :plans, column: "requested_plan_id", name: "quote_requests_fkey_on_requested_plan_id", on_delete: :cascade
    Rake::Task['seed:populate_notification_type'].invoke
  end
end
