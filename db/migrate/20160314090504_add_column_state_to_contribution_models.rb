class AddColumnStateToContributionModels < ActiveRecord::Migration
  def change
    add_column :contribution_models, :state, :smallint, default: 0
  end
end
