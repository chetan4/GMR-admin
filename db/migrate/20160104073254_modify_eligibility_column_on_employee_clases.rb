class ModifyEligibilityColumnOnEmployeeClases < ActiveRecord::Migration
  def up
    change_column :employee_classes, :eligibility, :string, :null => true
  end

  def down
    change_column :employee_classes, :eligibility, :string, :null => true
  end
end
