class AddColumnVolumeToRenewalPlanRateOptions < ActiveRecord::Migration
  def change
    add_column :renewal_plan_rate_options, :volume, :decimal, precision: 17, scale: 2
  end
end
