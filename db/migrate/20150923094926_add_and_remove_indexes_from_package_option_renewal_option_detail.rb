class AddAndRemoveIndexesFromPackageOptionRenewalOptionDetail < ActiveRecord::Migration
  def change
    add_index :package_option_renewal_option_details, [:package_option_renewal_option_id, :benefit_type_detail_id], unique: true, name: 'RENEWALOPTIONUNIQUE'
  end
end
