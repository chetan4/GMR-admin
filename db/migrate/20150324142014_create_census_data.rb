class CreateCensusData < ActiveRecord::Migration
  def change
    create_table :census_data do |t|
      t.integer :employer_id, null: false
      t.integer :plan_id, null: false
      t.integer :gender, null: false
      t.integer :residential_zip_code, null: false
      t.date :date_of_birth, null: false
      t.date :date_of_hire, null: false
      t.float :hours, null: false
      t.decimal :salary, precision: 17, scale: 2, null: false
      t.string :title, null: false
      t.integer :coverage_tier, null: false
      t.integer :coverage_class, null: false
      t.string :life_volume_1
      t.string :life_volume_2
      t.string :voluntary_life_volume_1
      t.string :voluntary_life_volume_2
      t.string :ltd_volume_1
      t.string :ltd_volume_2
      t.string :std_volume_1
      t.string :std_volume_2
      t.integer :spouse_age
      t.string :spouse_ad_n_d_volume
      t.string :spouse_voluntary_life_volume_or_units
      t.string :child_ad_n_d_volume
      t.string :child_life_volume

      t.timestamps null: false
    end
  end
end
