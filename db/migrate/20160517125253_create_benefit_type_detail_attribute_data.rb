class CreateBenefitTypeDetailAttributeData < ActiveRecord::Migration
  def change
    create_table :benefit_type_detail_attribute_data do |t|
      t.integer :benefit_type_detail_attribute_id
      t.integer :plan_id
      t.string :value

      t.timestamps null: false
    end
    add_index :benefit_type_detail_attribute_data, [:benefit_type_detail_attribute_id, :plan_id], unique: true, name: :benefit_type_detail_attribute_data_uniq_index
  end
end
