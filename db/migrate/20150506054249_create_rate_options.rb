class CreateRateOptions < ActiveRecord::Migration
  def change
    create_table :rate_options do |t|
      t.references :benefit_type
      t.integer :parent_id, default: 0
      t.string :name
      t.string :name_map
      t.timestamps null: false
    end
  end
end
