class CreateCensusesEmployee < ActiveRecord::Migration
  def change
    create_table :censuses_employee do |t|
      t.integer   :employer_company_id, null: false
      t.string    :employee_identifier, null: false
      t.string    :title
      t.string    :last_name
      t.string    :first_name
      t.integer   :gender, null: false # enunm
      t.string    :marital_status
      t.string    :street_address_1
      t.string    :street_address_2
      t.string    :city, null: false
      t.string    :state, null: false
      t.string    :residential_zip_code
      t.date	    :date_of_birth, null: false
      t.date 	    :date_of_hire, null: false
      t.integer   :hours_per_week, null: false
      t.integer   :full_time_status, null: false # enunm
      t.decimal   :salary, null: false
      t.decimal   :bonus
      t.decimal   :commission
      t.integer   :overtime
      t.boolean   :other_salary
      t.integer   :key_employee, null: false # enunm

      t.string    :business_unit, null: false
      t.string    :location, null: false
      t.string    :employee_class, null: false

      t.timestamps null: false
    end
    add_index :censuses_employee, [:employer_company_id, :employee_identifier], unique: true, name: 'company_employee_index'
  end
end
