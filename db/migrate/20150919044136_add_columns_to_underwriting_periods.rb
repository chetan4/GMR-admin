class AddColumnsToUnderwritingPeriods < ActiveRecord::Migration
  def change
    add_column :underwriting_periods, :experience_adjustment, :integer, precision: 10, scale: 7
    add_column :underwriting_periods, :manual_rate_adjustment, :integer, precision: 10, scale: 7
  end
end
