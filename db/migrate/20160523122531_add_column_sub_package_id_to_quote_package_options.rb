class AddColumnSubPackageIdToQuotePackageOptions < ActiveRecord::Migration
  def change
    add_column :quote_package_options, :sub_package_id, :integer
  end
end
