class CreateRateArrangements < ActiveRecord::Migration
  def change
    create_table :rate_arrangements do |t|
      t.string :name
      t.string :name_map, null: false, unique: true
      t.hstore :metadata
      t.integer :benefit_type_id

      t.timestamps null: false
    end
    add_index :rate_arrangements, [:benefit_type_id, :name_map], unique: true
  end
end
