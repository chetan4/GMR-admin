class CreateTemplateSheets < ActiveRecord::Migration
  def change
    create_table :template_sheets do |t|
      t.integer :template_id, null: false
      t.string :name, limit: 50
      t.integer :order, default: -1, null: false

      t.timestamps null: false
    end
  end
end
