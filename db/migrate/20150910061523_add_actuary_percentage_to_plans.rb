class AddActuaryPercentageToPlans < ActiveRecord::Migration
  def change
    add_column :plans, :actuary_percentage, :decimal, default: 0.0
  end
end
