class ChangeBenefitTypeNameMapVstdVltd < ActiveRecord::Migration
  def up
    voluntary_std = BenefitType.find_by(name_map: 'voluntary_short_term_disability_(vstd)')
    voluntary_std.update_attributes(name_map: 'voluntary_short_term_disability_(std)') if voluntary_std.present?

    voluntary_ltd = BenefitType.find_by(name_map: 'voluntary_long_term_disability_(vltd)')
    voluntary_ltd.update_attributes(name_map: 'voluntary_long_term_disability_(ltd)') if voluntary_ltd.present?
  end

  def down
    voluntary_std = BenefitType.find_by(name_map: 'voluntary_short_term_disability_(std)')
    voluntary_std.update_attributes(name_map: 'voluntary_short_term_disability_(vstd)') if voluntary_std.present?

    voluntary_ltd = BenefitType.find_by(name_map: 'voluntary_long_term_disability_(ltd)')
    voluntary_ltd.update_attributes(name_map: 'voluntary_long_term_disability_(vltd)') if voluntary_ltd.present?
  end
end
