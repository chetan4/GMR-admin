class ChangeColumnStateInCensusEmployees < ActiveRecord::Migration
  def change
  	change_column :censuses_employee, :state, :string, null: true
  end
end
