class AddFinalizStatusToPackageOption < ActiveRecord::Migration
  def up
    add_column :package_options, :finalize_status, :integer
  end

  def down
    remove_column :package_options, :finalize_status
  end
end
