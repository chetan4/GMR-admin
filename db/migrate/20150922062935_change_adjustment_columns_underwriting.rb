class ChangeAdjustmentColumnsUnderwriting < ActiveRecord::Migration

  def down
    remove_column :underwriting_periods, :experience_adjustment
    remove_column :underwriting_periods, :manual_rate_adjustment
  end

  def up
    add_column :plan_renewals, :experience_adjustment, :decimal, precision: 10, scale: 7
    add_column :plan_renewals, :manual_rate_adjustment, :decimal, precision: 10, scale: 7
  end

end
