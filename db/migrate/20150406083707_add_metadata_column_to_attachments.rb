class AddMetadataColumnToAttachments < ActiveRecord::Migration
  def change
    add_column :attachments, :metadata, :hstore
  end
end
