class ChangeColumnTypePlanChangeHistories < ActiveRecord::Migration
  def change
    change_column :plan_change_histories, :to, :text
  end
end
