class CreateMedicalClaimData < ActiveRecord::Migration
  def change
    create_table :medical_claim_data do |t|
      t.integer :task_id, null: false
      t.integer :plan_id, null: false
      t.integer :state, null: false, default: 0
      t.hstore :data

      t.timestamps null: false
    end
  end
end
