class AllowNulPlanIdInQuoteRequest < ActiveRecord::Migration
  def change
    change_column :quote_requests, :requested_plan_id, :integer, null: true
  end
end
