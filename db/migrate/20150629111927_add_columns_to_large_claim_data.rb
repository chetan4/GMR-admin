class AddColumnsToLargeClaimData < ActiveRecord::Migration
  def change
    add_column :large_claim_data, :data_entry_by_id, :integer
    add_column :large_claim_data, :reviewed_by_id, :integer
  end
end
