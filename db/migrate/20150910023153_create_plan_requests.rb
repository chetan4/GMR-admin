class CreatePlanRequests < ActiveRecord::Migration
  def change
    create_table :plan_requests do |t|
      t.string :description, null: false
      t.belongs_to :benefit_type, null: false

      t.timestamps null: false
    end
    add_foreign_key :plan_requests, :benefit_types, name: "plan_requests_fkey_on_benefit_type_id", on_delete: :cascade
  end
end
