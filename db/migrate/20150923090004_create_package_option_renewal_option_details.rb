class CreatePackageOptionRenewalOptionDetails < ActiveRecord::Migration
  def change
    create_table :package_option_renewal_option_details do |t|
      t.references :package_option_renewal_option
      t.references :benefit_type_detail
      t.integer :status # entry_in_progress, entry_complete, review_in_progress, verified
      t.hstore :metadata
      t.timestamps null: false
    end
  end
end
