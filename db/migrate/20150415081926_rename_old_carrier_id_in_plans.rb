class RenameOldCarrierIdInPlans < ActiveRecord::Migration
  def change
    rename_column :plans, :old_carrier_id, :old_carrier_company_id
  end
end
