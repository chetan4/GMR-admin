class AddMoreDetailsToPackagePlanRenewals < ActiveRecord::Migration
  def change
    add_column :package_plan_renewals, :template_id, :integer
    add_column :package_plan_renewals, :completed_by_user_id, :integer
    add_column :package_plan_renewals, :reviewed_by_user_id, :integer
    add_foreign_key :package_plan_renewals, :templates, name: 'package_plan_renewals_fkey_on_template_id', on_delete: :cascade
    add_foreign_key :package_plan_renewals, :users, column: 'completed_by_user_id', name: 'package_plan_renewals_fkey_on_completed_by_user_id', on_delete: :cascade
    add_foreign_key :package_plan_renewals, :users, column: 'reviewed_by_user_id', name: 'package_plan_renewals_fkey_on_reviewed_by_user_id', on_delete: :cascade
  end
end
