class ModifyPackagesTable < ActiveRecord::Migration
  def change
    drop_table :packages, {}

    create_table :packages do |t|
      t.string :name, null: false
      t.string :unique_id
      t.integer :employer_company_id, null: false
      t.integer :carrier_company_id, null: false
      t.integer :benefit_type_id, null: false

      t.timestamps null: false
    end

    add_index :packages, [:employer_company_id, :carrier_company_id, :benefit_type_id], name: 'PACKAGE_INDEX'
    add_index :packages, :unique_id, unique: true
  end
end
