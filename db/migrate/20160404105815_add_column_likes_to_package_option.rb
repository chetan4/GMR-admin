class AddColumnLikesToPackageOption < ActiveRecord::Migration
  def up
    add_column :package_options, :is_liked, :boolean, default: false
    add_column :quote_package_options, :is_liked, :boolean, default: false
    Rake::Task['onetime:populate_data_from_package_likes'].invoke

    rename_table :package_likes, :package_likes_backup
  end

  def down
    rename_table :package_likes_backup, :package_likes
    # task revert not required
    remove_column :quote_package_options, :is_liked
    remove_column :package_options, :is_liked
  end
end
