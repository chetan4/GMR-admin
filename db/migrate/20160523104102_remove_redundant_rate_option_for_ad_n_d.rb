class RemoveRedundantRateOptionForAdND < ActiveRecord::Migration
  def up
    ad_n_d_name_maps = %w(ad&d-rate-per-1000 ad&d-rate-per-employee ad&d-rate-age-based)
    ad_n_d_rate_options = BenefitType.find_by(name_map: "ad&d").rate_options.where(name_map: ad_n_d_name_maps)
    ad_n_d_name_maps.each do |name_map|
      rate_option_entries = ad_n_d_rate_options.where(name_map: name_map).order(id: :asc)
      next if rate_option_entries.size < 2
      redundant_entry = rate_option_entries.last
      redundant_entry.rate_option_labels.destroy_all
      redundant_entry.destroy
    end
  end

  def down
  end
end
