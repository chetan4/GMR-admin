class CreatePlanDesignData < ActiveRecord::Migration
  def change
    create_table :plan_design_data do |t|
      t.integer :task_id, null: false
      t.boolean :is_draft, null: false, default: false
      t.hstore :data
      t.timestamps null: false
    end
  end
end
