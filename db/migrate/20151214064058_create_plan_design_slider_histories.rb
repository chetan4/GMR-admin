class CreatePlanDesignSliderHistories < ActiveRecord::Migration
  def change
    create_table :plan_design_slider_histories do |t|
      t.references  :plan_design_slider
      t.integer     :plan_id
      t.hstore      :sliders_data
      t.timestamps null: false
    end

    add_index :plan_design_slider_histories, :plan_design_slider_id
  end
end
