class CreateCommissionPlans < ActiveRecord::Migration
  def change
    create_table :commission_plans do |t|
      t.integer :commission_id
      t.string :plan_name
      t.string :vendor_name
      t.string :coverage_name

      t.timestamps null: false
    end
  end
end
