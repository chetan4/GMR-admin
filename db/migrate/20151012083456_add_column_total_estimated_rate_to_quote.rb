class AddColumnTotalEstimatedRateToQuote < ActiveRecord::Migration
  def up
    add_column :quotes, :total_estimated_rate, :decimal, precision: 17, scale: 2
  end

  def down
    remove_column :quotes, :total_estimated_rate
  end
end
