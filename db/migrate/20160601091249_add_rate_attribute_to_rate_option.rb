class AddRateAttributeToRateOption < ActiveRecord::Migration
  def change
    add_column :rate_options, :rate_arrangement_id, :integer
  end
end
