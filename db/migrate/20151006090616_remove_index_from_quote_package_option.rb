class RemoveIndexFromQuotePackageOption < ActiveRecord::Migration
  def change
    remove_index :quote_package_options, name: 'QUOTE_PACKAGE_OPTION_INDEX'
    add_index :quote_package_options, [:quote_package_id, :name], unique: true
  end
end
