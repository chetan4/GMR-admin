class MoveNotesColumnFromUsersToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :note, :text, limit: 200
    remove_column :users, :note, :text
  end
end
