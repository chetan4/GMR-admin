class UpdateColumeVolumetoPackageEnrollmentDetails < ActiveRecord::Migration
  def change
    change_column :package_enrollment_details, :volume, :decimal, precision: 17, scale: 3, default: 0
  end
end
