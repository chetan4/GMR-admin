class CreatePackageLikes < ActiveRecord::Migration
  def change
    create_table :package_likes do |t|
    	t.integer :package_id, null: false
    	t.string :likable_type, null: false
    	t.integer :likable_id, null: :false
    	t.boolean :current_plan_like, default: :false
    	t.boolean :renewal_like, default: :false
    	t.hstore :package_option_likes


      t.timestamps null: false
    end
  end
end
