class AddColumnBusinessUnitIdToLocations < ActiveRecord::Migration
  def change
    add_column :locations, :business_unit_id, :integer
  end
end
