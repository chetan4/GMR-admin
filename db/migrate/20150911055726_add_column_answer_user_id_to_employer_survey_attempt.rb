class AddColumnAnswerUserIdToEmployerSurveyAttempt < ActiveRecord::Migration
  def change
    add_column :employer_survey_attempts, :answered_by_user_id, :integer
  end
end
