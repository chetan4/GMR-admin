class CreateQuoteRequestPlannables < ActiveRecord::Migration
  def change
    create_table :quote_request_plannables do |t|
      t.belongs_to :quote_request
      t.string     :plannable_type
      t.integer    :plannable_id
    end
    create_table :quote_request_recipients do |t|
      t.belongs_to :quote_request_plannable
      t.integer    :recipient_id
    end
    remove_column :quote_requests, :requested_plan_id
    remove_column :quote_requests, :recipient_id
    add_foreign_key "quote_request_recipients", "users", column: "recipient_id", name: "quote_request_recipients_fkey_on_recipient_id", on_delete: :cascade
  end
end
