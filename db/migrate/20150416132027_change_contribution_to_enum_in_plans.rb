class ChangeContributionToEnumInPlans < ActiveRecord::Migration
  def change
    remove_column :plans, :contribution, :string
    add_column :plans, :contribution, :integer
  end
end
