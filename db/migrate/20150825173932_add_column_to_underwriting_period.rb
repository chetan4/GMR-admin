class AddColumnToUnderwritingPeriod < ActiveRecord::Migration
  def change
    add_column :underwriting_periods, :funding_arrangement, :integer, default: 0
    add_column :underwriting_periods, :plan_inception_date, :date

    remove_column :underwriting_periods, :medical_claim_datum_id
  end
end
