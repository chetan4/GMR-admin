class AddColumnEnrollmentDataToQuotePackageOption < ActiveRecord::Migration
  def change
    add_column :quote_package_options, :enrollment_data, :hstore
  end
end
