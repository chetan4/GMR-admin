class DeleteUnwantedBenefitTypeDetails < ActiveRecord::Migration
  def change
    BenefitTypeDetail.where(field: ["employees", "benefit details", "dependent age limitations"]).delete_all
  end
end
