class CreateEmployeeClassPlans < ActiveRecord::Migration
  def change
    create_table :employee_class_plans do |t|
      t.integer :employee_class_id, null:false
      t.integer :plan_id, null:false
      t.timestamps null: false
    end
  end
end
