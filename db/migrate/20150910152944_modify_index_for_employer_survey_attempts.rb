class ModifyIndexForEmployerSurveyAttempts < ActiveRecord::Migration
  def change
    remove_index :employer_survey_attempts, name: 'UNIQUE_EMPLOYER_SURVEY_ATTEMPTS'
    add_index :employer_survey_attempts, [:employer_company_id, :employer_survey_question_id, :employer_survey_answer_id], unique: true, name: 'UNIQUE_EMPLOYER_SURVEY_ATTEMPTS'
  end
end
