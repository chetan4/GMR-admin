class CreatePlanDesigns < ActiveRecord::Migration
  def change
    create_table :plan_designs do |t|

      t.integer :plan_id, null: false
      t.integer :plan_profile, null: false
      t.boolean :open_access
      t.string :network
      t.string :deductible
      t.integer :co_insurance
      t.string :out_of_pocket_maximum
      t.string :max_cross_accumulation
      t.string :specialist_copay
      t.string :lab
      t.string :radiology
      t.string :annual_physical_exam_copay
      t.string :inpatient_hospitalization
      t.string :outpatient_hospitalization
      t.string :emergency_room_copay
      t.string :out_of_network_reimbursement_level
      t.string :rx_deductible
      t.string :rx_deductible_applies
      t.string :rx_drug_copay
      t.string :mail_order_copay
      t.boolean :spouse, default: false
      t.boolean :children, default: false
      t.boolean :family, default: false

      t.timestamps null: false
    end
  end
end
