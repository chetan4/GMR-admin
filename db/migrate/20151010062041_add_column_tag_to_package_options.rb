class AddColumnTagToPackageOptions < ActiveRecord::Migration
  def change
    add_column :package_options, :tag, :hstore
  end
end
