class CreateRequestedPlans < ActiveRecord::Migration
  def change
    create_table :requested_plans do |t|
      t.integer :plan_design_id, null: false
      t.hstore  :overrides
      t.timestamps null: false
    end
  end
end
