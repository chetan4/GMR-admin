class AddColumnSubPackageIdToPackageOptions < ActiveRecord::Migration
  def change
    add_column :package_options, :sub_package_id, :integer
  end
end
