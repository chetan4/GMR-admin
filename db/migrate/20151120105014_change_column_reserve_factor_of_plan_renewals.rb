class ChangeColumnReserveFactorOfPlanRenewals < ActiveRecord::Migration
  def self.up
    change_column :plan_renewals, :reserve_factor, :decimal, precision: 10, scale: 7
  end

  def self.down
    change_column :plan_renewals, :reserve_factor, :integer
  end
end
