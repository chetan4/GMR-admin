class CreateEmployerSurveyAnswers < ActiveRecord::Migration
  def change
    create_table :employer_survey_answers do |t|
      t.string :answer_text,  null: false
      t.integer :employer_survey_question_id,  null: false

      t.timestamps null: false
    end

    add_index :employer_survey_answers, :employer_survey_question_id
  end
end
