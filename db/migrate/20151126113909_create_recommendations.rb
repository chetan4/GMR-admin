class CreateRecommendations < ActiveRecord::Migration
  def change
    create_table :recommendations do |t|
      t.integer :employer_company_id, null: false
      t.integer :recommendation_type, default: 0
      t.string :recommendation_text
      t.hstore :data
      t.string :impact
      t.integer :created_by_id
      t.integer :updated_by_id
      t.timestamps null: false
    end

    add_index :recommendations, :employer_company_id
  end
end
