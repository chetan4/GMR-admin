class AddLastOperatedByColumnInDataEntryTask < ActiveRecord::Migration
  def change
    add_column :data_entry_tasks, :last_operated_by_id, :integer
  end
end
