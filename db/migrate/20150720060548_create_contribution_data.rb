class CreateContributionData < ActiveRecord::Migration
  def change
    create_table :contribution_data do |t|
      t.integer :task_id, null: false
      t.integer :plan_id, null: false
      t.integer :employee_class_id, null: false
      t.integer :state, null: false, default: 0
      t.hstore :data

      t.timestamps null: false
    end

    add_index :contribution_data, [:plan_id, :employee_class_id], unique: true
  end
end
