class CreateDentalClaimData < ActiveRecord::Migration
  def change
    create_table :dental_claim_data do |t|
      t.integer :task_id, null: false
      t.integer :plan_id, null: false
      t.integer :state, null: false, default: 0
      t.hstore :data
      t.integer :data_entry_by_id
      t.integer :reviewed_by_id

      t.timestamps null: false
    end
  end
end
