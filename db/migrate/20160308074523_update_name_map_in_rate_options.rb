class UpdateNameMapInRateOptions < ActiveRecord::Migration
  def up
    Rake::Task['seed:populate_rate_options'].invoke
    new_name_hash = {
        'basic-life-rate-per-1000' => {
            name: 'AD&D Rate per 1000',
            name_map: 'ad&d-rate-per-1000'
        },
        'basic-rate-per-employee' => {
            name: 'AD&D Rate per Employee',
            name_map: 'ad&d-rate-per-employee'
        },
        'basic-rate-age-based' => {
            name: 'AD&D Rate age based',
            name_map: 'ad&d-rate-age-based'
        }
    }
    rate_options = BenefitType.find_by(name_map: "ad&d")
                       .rate_options.where(name_map: new_name_hash.keys)
    rate_options.each do |rate_option|
      name_detail = new_name_hash[rate_option.name_map]
      rate_option.name = name_detail[:name]
      rate_option.name_map = name_detail[:name_map]
      rate_option.save!
    end
  end

  def down
    old_name_hash = {
        'ad&d-rate-per-1000' => {
            name: 'Basic Life Rate per 1000',
            name_map: 'basic-life-rate-per-1000'
        },
        'ad&d-rate-per-employee' => {
            name: 'Basic Rate per Employee',
            name_map: 'basic-rate-per-employee'
        },
        'ad&d-rate-age-based' => {
            name: 'Basic Rate age based',
            name_map: 'basic-rate-age-based'
        }
    }

    rate_options = BenefitType.find_by(name_map: "ad&d")
                       .rate_options.where(name_map: old_name_hash.keys)
    rate_options.each do |rate_option|
      name_detail = old_name_hash[rate_option.name_map]
      rate_option.name = name_detail[:name]
      rate_option.name_map = name_detail[:name_map]
    end
  end
end
