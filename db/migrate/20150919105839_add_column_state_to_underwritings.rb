class AddColumnStateToUnderwritings < ActiveRecord::Migration
  def change
    add_column :underwritings, :status, :integer, default: 0
  end
end
