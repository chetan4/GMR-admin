class RemoveAvailableInVerticalIdFromRoles < ActiveRecord::Migration
  def change
    remove_column :roles, :available_in_vertical_id, :integer
  end
end
