class CreateFileViewerTags < ActiveRecord::Migration
  def change
    create_table :file_viewer_tags do |t|
      t.integer :package_id
      t.integer :attachment_id
      t.integer :package_type # enum
      t.integer :file_tag #enum
      t.string :file_name
      t.timestamps null: false
    end
  end
end
