class CreateTemplateEntities < ActiveRecord::Migration
  def change
    create_table :template_entities do |t|
      t.string :type
      t.integer :template_section_id, null: false
      t.string :name, null: false, limit: 50
      t.integer :order, null: false, default: -1

      t.timestamps null: false
    end
  end
end
