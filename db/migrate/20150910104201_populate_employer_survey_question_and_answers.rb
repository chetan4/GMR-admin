class PopulateEmployerSurveyQuestionAndAnswers < ActiveRecord::Migration
  def up
    Rake::Task['seed:populate_employer_survey_questions_answers'].invoke
  end
end
