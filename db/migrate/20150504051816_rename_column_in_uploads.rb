class RenameColumnInUploads < ActiveRecord::Migration
  def change
    rename_column :uploads, :plan_document_request_id, :plan_document_request_status_id
  end
end
