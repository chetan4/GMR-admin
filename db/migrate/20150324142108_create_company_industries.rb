class CreateCompanyIndustries < ActiveRecord::Migration
  def up
    create_table :company_industries do |t|
      t.integer :company_id, null: false
      t.integer :industry_id, null: false
    end
    Rake::Task['seed:populate_company_industries'].invoke
  end

  def down
    Rake::Task['seed:unpopulate_company_industries'].invoke
    drop_table :company_industries
  end
end
