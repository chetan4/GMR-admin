class CreateVerticals < ActiveRecord::Migration
  def up
    create_table :verticals do |t|
      t.string :name, null: false, limit: 50
    end
    Rake::Task['seed:populate_verticals'].invoke
  end
  def down
    drop_table :verticals
  end
end
