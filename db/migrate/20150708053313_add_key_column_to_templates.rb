class AddKeyColumnToTemplates < ActiveRecord::Migration
  def up
    add_column :template_sheets, :key, :string, limit: 50
    add_column :template_sections, :key, :string, limit: 50
    add_column :template_entities, :key, :string, limit: 50


    [TemplateSheet, TemplateSection, TemplateEntity].each do |model|
      model.all.each do |model_obj|
        model_obj.key = model_obj.name.parameterize
        begin
          model_obj.save!
        rescue Exception => e
          model_obj.name = model_obj.name[0...48]
          model_obj.name = model_obj.name + (0..99).to_a.sample.to_s
          model_obj.key = model_obj.name.parameterize
          model_obj.save!
        end
      end
    end

    add_index :template_sheets, [:template_id, :key], unique: true
    add_index :template_sections, [:template_sheet_id, :key], unique: true
    add_index :template_entities, [:type, :template_section_id, :key], unique: true
  end
  def down
    remove_index :template_sheets, [:template_id, :key]
    remove_index :template_sections, [:template_sheet_id, :key]
    remove_index :template_entities, [:type, :template_section_id, :key]

    remove_column :template_sheets, :key
    remove_column :template_sections, :key
    remove_column :template_entities, :key
  end
end
