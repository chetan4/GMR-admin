class CreateCommissions < ActiveRecord::Migration
  def change
    create_table :commissions do |t|
      t.string :type
      t.integer :plan_id
      t.string :payee
      t.integer :general_paid_agency
      t.integer :commission_type
      t.float :compensation
      t.integer :no_of_members
      t.integer :no_of_employees
      t.float :estimated_annual_compensation

      t.timestamps null: false
    end
  end
end
