class CreateEmployeeClassModels < ActiveRecord::Migration
  def change
    create_table :employee_class_models do |t|
      t.references :contribution_model
      t.references :employee_class
      t.string  :source_type
      t.integer :source_id

      t.timestamps null: false
    end

    add_index :employee_class_models, [:contribution_model_id, :employee_class_id, :source_type, :source_id], name: 'employee_class_models_index'
  end
end
