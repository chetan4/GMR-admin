class AddColumnsDatesAndFundingArrangementToSimpleProjectRenewal < ActiveRecord::Migration
  def change
    add_column :simple_project_renewals, :funding_arrangement, :integer
    add_column :simple_project_renewals, :from_date, :datetime
    add_column :simple_project_renewals, :to_date, :datetime
  end
end
