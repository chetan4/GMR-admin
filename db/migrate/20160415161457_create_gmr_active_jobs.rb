class CreateGMRActiveJobs < ActiveRecord::Migration
  def change
    create_table :gmr_active_jobs do |t|
      t.string :job_id
      t.integer :status, null: false, default: 0

      t.timestamps null: false
    end

    add_index :gmr_active_jobs, :job_id, name: 'INDEX_ON_JOB_ID_GMR_ACTIVE_JOBS'
  end
end
