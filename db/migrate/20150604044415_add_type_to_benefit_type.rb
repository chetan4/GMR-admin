class AddTypeToBenefitType < ActiveRecord::Migration
  def change
    add_column :benefit_types, :type, :string
    BenefitType.update_all(:type => MainBenefitType)
  end
end
