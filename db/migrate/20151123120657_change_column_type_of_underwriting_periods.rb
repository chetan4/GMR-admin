class ChangeColumnTypeOfUnderwritingPeriods < ActiveRecord::Migration
  def self.up
    change_column :underwriting_periods, :medical_claim_adjusted, :decimal, precision: 10, scale: 7
    change_column :underwriting_periods, :rx_claim_adjusted, :decimal, precision: 10, scale: 7
  end

  def self.down
    change_column :underwriting_periods, :medical_claim_adjusted, :integer
    change_column :underwriting_periods, :rx_claim_adjusted, :integer
  end
end
