class AddUniqueIndexOnQuoteRequestPlannable < ActiveRecord::Migration
  def change
    # Rake::Task['onetime:clean_up_old_quote_requests_and_plan_requests'].invoke
    add_index :quote_request_plannables, [:quote_request_id, :plannable_type, :plannable_id], :unique => true, name: 'index_quote_request_plannables_on_quote_request_and_plannable'
  end
end
