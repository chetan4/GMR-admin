class CreatePackageEnrollmentDetails < ActiveRecord::Migration
  def change
    create_table :package_enrollment_details do |t|
      t.references :package_option
      t.references :package_option_renewal_option
      t.references :renewal_plan_rate_option_label
      t.integer :enrollment, null: false, default: 0

      t.timestamps null: false
    end
    add_index :package_enrollment_details, [:package_option_id, :package_option_renewal_option_id, :renewal_plan_rate_option_label_id], name: "index_package_enrollment_details", unique: true, using: :btree
  end
end
