class RemoveIndexOnUsers < ActiveRecord::Migration
  def change
    remove_index :users, column: :invitations_count
    remove_index :users, column: :invited_by_id
  end
end
