class CreateUnderwritingPeriods < ActiveRecord::Migration
  def change
    create_table :underwriting_periods do |t|
      t.references :plan_renewal
      t.references :plan

      t.references :medical_claim_datum
      t.references :large_claim_datum

      t.date :begin_date
      t.date :end_date
      t.integer :weighting, precision: 10, scale: 7

      t.decimal :medical_lag, precision: 17, scale: 7
      t.decimal :rx_lag, precision: 17, scale: 7
      t.decimal :medical_maturity, precision: 17, scale: 7
      t.decimal :rx_maturity, precision: 17, scale: 7

      t.decimal :plan_change, precision: 17, scale: 7
      t.integer :number_large_claims
      t.integer :period_no

      t.timestamps null: false
    end
  end
end
