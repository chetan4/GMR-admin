class AddColumnsCommentsActionStatusAcceptedByUserIdToPackagePlanRenewals < ActiveRecord::Migration
  def change
    add_column :package_plan_renewals, :comment, :text
    add_column :package_plan_renewals, :action_status, :integer
    add_column :package_plan_renewals, :accepted_by_user_id, :integer
  end
end
