class CreateEmployeeAssociations < ActiveRecord::Migration
  def change
    create_table :employee_associations do |t|
      t.integer :user_id, null: false
      t.integer :company_id, null: false
      t.timestamps null: false
    end

    add_index :employee_associations, [:user_id, :company_id]
  end
end
