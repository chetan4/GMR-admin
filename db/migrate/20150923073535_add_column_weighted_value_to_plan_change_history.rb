class AddColumnWeightedValueToPlanChangeHistory < ActiveRecord::Migration
  def change
    add_column :plan_change_histories, :medical_weighting, :decimal, precision: 10, scale: 7
    add_column :plan_change_histories, :rx_weighting, :decimal, precision: 10, scale: 7
  end
end
