class CreatePackageOptions < ActiveRecord::Migration
  def change
    create_table :package_options do |t|
      t.references :package
      t.string :name
      t.integer :status # entry_in_progress, entry_complete, review_in_progress, verified
      t.timestamps null: false
    end

    add_index :package_options, [:package_id, :name], unique: true
  end
end
