class AddCobraStatusToCensusEmployee < ActiveRecord::Migration
  def change
    add_column :censuses_employee, :is_cobra, :boolean, default: false
    add_column :censuses_employee, :terminated_at, :datetime
  end
end
