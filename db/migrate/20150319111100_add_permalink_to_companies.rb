class AddPermalinkToCompanies < ActiveRecord::Migration
  def up
    add_column :companies, :permalink, :string
    Company.all.each do |company|
      company.permalink = company.name.to_s.parameterize
      company.save
    end
    change_column_null :companies, :permalink, false
    add_index  :companies, :permalink
  end

  def down
    remove_index  :companies, :permalink
    remove_column :companies, :permalink
  end
end
