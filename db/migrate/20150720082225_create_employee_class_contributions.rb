class CreateEmployeeClassContributions < ActiveRecord::Migration
  def change
    create_table :employee_class_contributions do |t|
      t.integer :employee_class_id, null: false
      t.integer :contribution_datum_id, null: false

      t.timestamps null: false
    end
  end
end
