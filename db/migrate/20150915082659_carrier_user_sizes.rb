class CarrierUserSizes < ActiveRecord::Migration
  def change
    create_table :user_sizes do |t|
      t.string :size, null: false
      t.timestamps    null: false
    end

    create_table :carrier_user_sizes do |t|
      t.belongs_to :user, null: false
      t.belongs_to :user_size, null: false
      t.timestamps        null: false
    end
    add_foreign_key :carrier_user_sizes, :users, name: "carrier_user_sizes_fkey_on_user_id", on_delete: :cascade
    add_foreign_key :carrier_user_sizes, :user_sizes, name: "carrier_user_sizes_fkey_on_size_id", on_delete: :cascade

    create_table :carrier_user_locations do |t|
      t.belongs_to :user,        null: false
      t.text       :locations,   array:true, default: []
      t.timestamps               null: false
    end
    add_foreign_key :carrier_user_locations, :users, name: "carrier_user_locations_fkey_on_carrier_company_id", on_delete: :cascade
  end
end
