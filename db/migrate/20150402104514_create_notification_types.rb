class CreateNotificationTypes < ActiveRecord::Migration
  def change
    create_table :notification_types do |t|
      t.string :activity_type
      t.integer :level
      t.integer :sender_vertical_id
      t.integer :receiver_vertical_id

      t.timestamps null: false
    end

    add_index :notification_types, [:sender_vertical_id, :receiver_vertical_id, :activity_type],
              :name => 'sender_receiver_id_activity_type', unique: true
  end
end