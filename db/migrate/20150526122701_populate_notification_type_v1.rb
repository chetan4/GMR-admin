class PopulateNotificationTypeV1 < ActiveRecord::Migration
  def up
    Rake::Task['seed:populate_notification_type'].invoke
  end

  def down
  end
end
