class AddColumnsToCompanies < ActiveRecord::Migration
  def up
    add_column :companies, :vertical, :string
    add_column :companies, :industries, :string
    add_column :companies, :website, :string, limit: 50
    add_column :companies, :address, :string, limit: 100

    change_column :companies, :name, :string, limit: 50
    add_index  :companies, [:vertical, :name], :unique => true
  end

  def down
    remove_index  :companies, [:vertical, :name]

    remove_column :companies, :vertical
    remove_column :companies, :industries
    remove_column :companies, :website
    remove_column :companies, :address

    change_column :companies, :name, :string
  end
end
