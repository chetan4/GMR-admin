class AddDefaultsToEmployeeClasses < ActiveRecord::Migration
  def change
    change_column :employee_classes, :dependent_child_coverage, :integer, default: 0, null: false
  end
end
