class ChangeColumnPackageIdInPackageLike < ActiveRecord::Migration
  def change
    change_column :package_likes, :package_id, :integer, null: true
  end
end
