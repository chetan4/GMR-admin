class CreateQuoteRequests < ActiveRecord::Migration
  def change
    create_table :quote_requests do |t|
      t.integer :employer_location_id, null: false
      t.integer :broker_id
      t.integer :benefit_type_id, null: false
      t.integer :carrier_id, null: false
      t.string  :status
      t.integer :renewal_id
      t.integer :requested_plan_id, null: false
      t.date    :effective_date
      t.integer :parent_quote_request_id
      t.timestamps null: false
    end
  end
end
