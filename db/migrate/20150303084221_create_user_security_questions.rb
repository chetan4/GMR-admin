class CreateUserSecurityQuestions < ActiveRecord::Migration
  def change
    create_table :user_security_questions do |t|
      t.integer :user_id
      t.integer :question_id
      t.string :encrypted_answer

      t.timestamps null: false
    end
  end
end
