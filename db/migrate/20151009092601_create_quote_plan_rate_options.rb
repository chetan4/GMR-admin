class CreateQuotePlanRateOptions < ActiveRecord::Migration
  def change
    create_table :quote_plan_rate_options do |t|
      t.integer :quotable_id
      t.string :quotable_type
      t.integer :rate_option_id
      t.integer :parent_id
      t.hstore :metadata
      t.integer :rate_frequency

      t.timestamps null: false
    end
    add_index :quote_plan_rate_options, [:quotable_id, :quotable_type], name: "index_quote_pro_on_quotable_id", unique: true, using: :btree

  end
end
