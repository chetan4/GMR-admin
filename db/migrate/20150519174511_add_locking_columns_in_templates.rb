class AddLockingColumnsInTemplates < ActiveRecord::Migration
  def change
    add_column :templates, :locked_uptil, :datetime
    add_column :templates, :locked_by_id, :integer
    add_column :templates, :locking_uuid, :uuid
  end
end
