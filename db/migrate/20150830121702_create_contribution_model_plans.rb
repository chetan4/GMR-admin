class CreateContributionModelPlans < ActiveRecord::Migration
  def change
    create_table :contribution_model_plans do |t|
      t.references :plan
      t.references :contribution_model
      t.references :employee_class
      t.hstore :data

      t.timestamps null: false
    end

    add_index :contribution_model_plans, [:contribution_model_id, :plan_id, :employee_class_id], unique: true, name: 'UNIQUE_CONTRIBUTION_MODEL_PLANS'
  end
end
