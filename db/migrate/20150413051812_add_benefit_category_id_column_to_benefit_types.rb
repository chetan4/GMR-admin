class AddBenefitCategoryIdColumnToBenefitTypes < ActiveRecord::Migration
  def up
    BenefitType.destroy_all
    add_column :benefit_types, :benefit_category_id, :integer, null: false
    add_column :benefit_types, :subtypes, :text
    BenefitType.reset_column_information
  end
  def down
    remove_column :benefit_types, :benefit_category_id
    remove_column :benefit_types, :subtypes
  end
end
