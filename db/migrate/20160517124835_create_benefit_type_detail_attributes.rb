class CreateBenefitTypeDetailAttributes < ActiveRecord::Migration
  def change
    create_table :benefit_type_detail_attributes do |t|
      t.integer :benefit_type_detail_id
      t.string :name_map
      t.string :display_name
      t.integer :status

      t.timestamps null: false
    end
    add_index :benefit_type_detail_attributes, [:benefit_type_detail_id, :name_map], unique: true, name: :benefit_type_detail_attributes_uniq_index
  end
end
