class AddClaimAdjustedToUnderwritingPeriods < ActiveRecord::Migration
  def change
    add_column :underwriting_periods, :medical_claim_adjusted, :integer, precision: 10, scale: 7
    add_column :underwriting_periods, :rx_claim_adjusted, :integer, precision: 10, scale: 7
  end
end
