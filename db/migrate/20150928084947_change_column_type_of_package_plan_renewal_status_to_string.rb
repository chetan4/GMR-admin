class ChangeColumnTypeOfPackagePlanRenewalStatusToString < ActiveRecord::Migration
  def change
    change_column :package_plan_renewals, :status, :string
  end
end
