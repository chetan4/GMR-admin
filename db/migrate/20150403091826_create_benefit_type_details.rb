class CreateBenefitTypeDetails < ActiveRecord::Migration
  def change
    create_table :benefit_type_details do |t|
      t.integer :benefit_type_id, null: false
      t.string  :field, null: false
      t.boolean :mandatory_field
      t.timestamps null: false
    end
    add_index :benefit_type_details, [:benefit_type_id, :field], unique: true
  end
end
