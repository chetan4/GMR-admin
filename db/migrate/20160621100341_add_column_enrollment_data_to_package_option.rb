class AddColumnEnrollmentDataToPackageOption < ActiveRecord::Migration
  def change
    add_column :package_options, :enrollment_data, :hstore
  end
end
