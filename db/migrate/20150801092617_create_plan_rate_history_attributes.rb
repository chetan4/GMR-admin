class CreatePlanRateHistoryAttributes < ActiveRecord::Migration
  def change
    create_table :plan_rate_history_attributes do |t|
      t.references :plan, null: false
      t.integer :sub_task_id, null: false
      t.date :effective_start_date
      t.date :effective_end_date
      t.timestamps null: false
    end

    add_index :plan_rate_history_attributes, :sub_task_id, unique: true, name: 'UNIQUE_SUB_TASK'
    add_index :plan_rate_history_attributes, :plan_id, unique: true, name: 'UNIQUE_PLAN'
  end
end
