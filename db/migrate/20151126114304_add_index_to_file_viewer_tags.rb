class AddIndexToFileViewerTags < ActiveRecord::Migration
  def change
    add_index :file_viewer_tags, [:package_id, :package_type, :file_tag], name: 'file_viewer_tag_index'
  end
end
