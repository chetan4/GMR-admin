class ChangeColumnMultiplyFactorOfPackageEnrollmentDetails < ActiveRecord::Migration
  def change
    change_column :package_enrollment_details, :multiply_factor, :decimal, precision: 17, scale: 7
    change_column :package_enrollment_details, :volume, :decimal, precision: 17, scale: 7

    change_column :quote_package_enrollment_details, :multiply_factor, :decimal, precision: 17, scale: 7
    change_column :quote_package_enrollment_details, :volume, :decimal, precision: 17, scale: 7

  end
end
