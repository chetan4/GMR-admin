class CreateContributionModelCbPlans < ActiveRecord::Migration
  def change
    create_table :contribution_model_cb_plans do |t|
      t.references :contribution_model
      t.references :plan
      t.integer :cb_type, null: false

      t.timestamps null: false
    end

    add_index :contribution_model_cb_plans, [:contribution_model_id, :plan_id], unique: true, name: 'UNIQUE_CONTRIBUTION_MODEL_CB_PLANS'

  end
end
