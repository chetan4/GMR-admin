class AddColumnIsTaskToConversationMessages < ActiveRecord::Migration
  def change
    add_column :conversation_messages, :is_task, :boolean, null: false, default: false
  end
end
