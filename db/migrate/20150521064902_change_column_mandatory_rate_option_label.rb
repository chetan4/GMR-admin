class ChangeColumnMandatoryRateOptionLabel < ActiveRecord::Migration
  def up
    remove_column :rate_option_labels, :mandatory
    add_column :rate_option_labels, :presence_type, :integer, default: 0
  end

  def down
    add_column :rate_option_labels, :mandatory, :boolean, default: false
    remove_column :rate_option_labels, :presence_type
  end
end