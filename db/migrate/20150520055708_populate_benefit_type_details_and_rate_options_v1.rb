class PopulateBenefitTypeDetailsAndRateOptionsV1 < ActiveRecord::Migration
  def up
    Rake::Task['seed:populate_benefit_types'].invoke
    # Update the Rate options name map retiree if its is not in proper format
    # example retiree medical => retiree
    RateOption.where(name_map: 'retiree-medicare').update_all(name_map: 'retiree')
  end

  def down
  end
end
