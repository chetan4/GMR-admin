class CreateUnderwritings < ActiveRecord::Migration
  def change
    create_table :underwritings do |t|
      t.string :name, unique: true

      t.timestamps null: false
    end
  end
end
