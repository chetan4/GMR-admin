class AddNullConstraintInUsers < ActiveRecord::Migration
  def up
    change_column :users, :first_name, :string, limit: 30, null: false
    change_column :users, :last_name, :string, limit: 30, null: false
  end
  def down
    change_column :users, :first_name, :string, limit: 30, null: true
    change_column :users, :last_name, :string, limit: 30, null: true
  end
end
