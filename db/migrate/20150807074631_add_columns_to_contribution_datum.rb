class AddColumnsToContributionDatum < ActiveRecord::Migration
  def change
    add_column :contribution_data, :contribution_type, :integer, null: false
    add_column :contribution_data, :contribution_scheme, :integer, null: false
    add_column :contribution_data, :contribution_name, :string, null: false
    add_column :contribution_data, :frequency, :integer, null: false
    add_column :contribution_data, :buyout_value, :decimal, precision: 17, scale: 2
  end
end
