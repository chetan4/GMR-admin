class AddOnboardedColumnInCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :onboarded, :boolean, default: false
  end
end
