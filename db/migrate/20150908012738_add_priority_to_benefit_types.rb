class AddPriorityToBenefitTypes < ActiveRecord::Migration
  def change
    add_column :benefit_types, :priority, :integer
  end
end
