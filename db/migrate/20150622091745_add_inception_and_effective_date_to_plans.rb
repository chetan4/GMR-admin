class AddInceptionAndEffectiveDateToPlans < ActiveRecord::Migration
  def change
    add_column :plans, :inception_date, :datetime
    add_column :plans, :effective_date, :datetime
  end
end
