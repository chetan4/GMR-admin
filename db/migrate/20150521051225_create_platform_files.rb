class CreatePlatformFiles < ActiveRecord::Migration
  def change
    create_table :platform_files do |t|
      t.string :name, limit: 50, null: false
      t.integer :carrier_company_id, null: false
      t.integer :benefit_type_id, null: false
      t.hstore :data

      t.timestamps null: false
    end
  end
end
