class RenameDomainIdToVerticalId < ActiveRecord::Migration
  def change
    rename_column :companies, :domain_id, :vertical_id
    rename_column :roles, :domain_id, :vertical_id
  end
end
