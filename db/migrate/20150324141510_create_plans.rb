class CreatePlans < ActiveRecord::Migration
  def change
    create_table :plans do |t|
      t.integer :carrier_id, null: false
      t.integer :package_id
      t.integer :benefit_type_id, null: false
      t.string :name, null: false

      t.timestamps null: false
    end
  end
end
