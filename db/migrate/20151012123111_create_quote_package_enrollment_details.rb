class CreateQuotePackageEnrollmentDetails < ActiveRecord::Migration
  def change
    create_table :quote_package_enrollment_details do |t|
      t.integer :enrollment, null: false, default: 0
      t.references :quote_package_option
      t.references :quote_package_option_quote
      t.references :quote_plan_rate_option_label

      t.timestamps null: false
    end
    add_index :quote_package_enrollment_details, [:quote_package_option_id, :quote_package_option_quote_id, :quote_plan_rate_option_label_id], name: "index_quote_package_enrollment_details", unique: true, using: :btree
  end
end
