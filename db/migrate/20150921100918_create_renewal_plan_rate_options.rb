class CreateRenewalPlanRateOptions < ActiveRecord::Migration
  def change
    create_table :renewal_plan_rate_options do |t|
      t.integer     :renewable_id
      t.string      :renewable_type
      t.integer     :rate_option_id
      t.integer     :parent_id
      t.hstore      :metadata
      t.integer     :rate_frequency
      t.timestamps null: false
    end
    add_index :renewal_plan_rate_options, [:renewable_id, :renewable_type], name: "index_renewal_pro_on_renewable_id", unique: true, using: :btree
  end

end
