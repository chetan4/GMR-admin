UserRole.destroy_all
UserRole._validate_callbacks.clear
UserRole.before_save.clear
UserRole.create!([
  {id: 1, user_id: 1, role_id: 1, created_at: "2015-05-15 07:01:36", updated_at: "2015-05-15 07:01:36"},
  {id: 2, user_id: 2, role_id: 6, created_at: "2015-05-15 07:25:22", updated_at: "2015-05-15 07:25:22"},
  {id: 3, user_id: 3, role_id: 7, created_at: "2015-05-15 07:29:02", updated_at: "2015-05-15 07:29:02"},
  {id: 4, user_id: 4, role_id: 6, created_at: "2015-05-15 07:32:56", updated_at: "2015-05-15 07:32:56"},
  {id: 5, user_id: 5, role_id: 7, created_at: "2015-05-15 07:34:47", updated_at: "2015-05-15 07:34:47"},
  {id: 6, user_id: 6, role_id: 6, created_at: "2015-05-15 07:37:42", updated_at: "2015-05-15 07:37:42"},
  {id: 7, user_id: 7, role_id: 7, created_at: "2015-05-15 07:39:26", updated_at: "2015-05-15 07:39:26"},
  {id: 8, user_id: 8, role_id: 25, created_at: "2015-05-15 08:15:03", updated_at: "2015-05-15 08:15:03"},
  {id: 9, user_id: 9, role_id: 27, created_at: "2015-05-15 08:18:51", updated_at: "2015-05-15 08:18:51"},
  {id: 10, user_id: 10, role_id: 25, created_at: "2015-05-15 08:22:48", updated_at: "2015-05-15 08:22:48"},
  {id: 11, user_id: 11, role_id: 27, created_at: "2015-05-15 08:24:46", updated_at: "2015-05-15 08:24:46"},
  {id: 12, user_id: 12, role_id: 25, created_at: "2015-05-15 08:27:44", updated_at: "2015-05-15 08:27:44"},
  {id: 13, user_id: 13, role_id: 27, created_at: "2015-05-15 08:29:35", updated_at: "2015-05-15 08:29:35"},
  {id: 14, user_id: 14, role_id: 19, created_at: "2015-05-15 08:33:26", updated_at: "2015-05-15 08:33:26"},
  {id: 15, user_id: 15, role_id: 24, created_at: "2015-05-15 08:37:34", updated_at: "2015-05-15 08:37:34"},
  {id: 16, user_id: 16, role_id: 20, created_at: "2015-05-15 08:39:20", updated_at: "2015-05-15 08:39:20"},
  {id: 17, user_id: 17, role_id: 21, created_at: "2015-05-15 08:40:20", updated_at: "2015-05-15 08:40:20"},
  {id: 18, user_id: 18, role_id: 22, created_at: "2015-05-15 08:43:15", updated_at: "2015-05-15 08:43:15"},
  {id: 19, user_id: 19, role_id: 23, created_at: "2015-05-15 08:44:30", updated_at: "2015-05-15 08:44:30"}
])
ActiveRecord::Base.connection.execute("ALTER SEQUENCE user_roles_id_seq RESTART WITH 20")
