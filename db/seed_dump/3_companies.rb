Company.destroy_all
Company._validate_callbacks.clear
Company.before_save.clear
Company.create!([
  {id: 1, name: "default", created_at: "2015-05-15 07:01:35", updated_at: "2015-05-15 07:01:35", website: "www.gmr.com", address: "GMR HQ", contact_person_id: 1, note: nil, permalink: "default", vertical_id: 1, onboarded: false},
  {id: 2, name: "Aetna", created_at: "2015-05-15 07:25:16", updated_at: "2015-05-15 07:25:22", website: "www.aetna.com", address: "Aetna Inc.\r\n151 Farmington Avenue\r\nHartford, CT 06156\r\nUSA\r\nDriving directions\r\nSee driving directions ", contact_person_id: 2, note: "", permalink: "aetna", vertical_id: 3, onboarded: false},
  {id: 3, name: "Cigna", created_at: "2015-05-15 07:32:52", updated_at: "2015-05-15 07:32:56", website: "www.cigna.com", address: "Cigna Corporate Headquarters\r\n900 Cottage Grove Road\r\nBloomfield, CT 06002", contact_person_id: 4, note: "", permalink: "cigna", vertical_id: 3, onboarded: false},
  {id: 4, name: "UNM", created_at: "2015-05-15 07:37:38", updated_at: "2015-05-15 07:37:42", website: "www.unm.edu", address: "The University of New Mexico, \r\nAlbuquerque, \r\nNM 87131", contact_person_id: 6, note: "", permalink: "unm", vertical_id: 3, onboarded: false},
  {id: 6, name: "Yahoo", created_at: "2015-05-15 08:22:45", updated_at: "2015-05-15 08:22:48", website: "www.yahoo.com", address: "701 1st Ave, \r\nSunnyvale, \r\nCA 94089, \r\nUnited States", contact_person_id: 10, note: "", permalink: "yahoo", vertical_id: 5, onboarded: false},
  {id: 7, name: "Apple", created_at: "2015-05-15 08:27:41", updated_at: "2015-05-15 08:27:44", website: "www.apple.com", address: "Cupertino, \r\nCalifornia, \r\nU.S", contact_person_id: 12, note: "", permalink: "apple", vertical_id: 5, onboarded: false},
  {id: 8, name: "GMR  Group Data Entry", created_at: "2015-05-15 08:33:18", updated_at: "2015-05-15 08:33:26", website: "www.gmr.com", address: "GMR Headquarters\r\nNY", contact_person_id: 14, note: "", permalink: "gmr-group-data-entry", vertical_id: 4, onboarded: false},
  {id: 5, name: "Tesla", created_at: "2015-05-15 08:14:58", updated_at: "2015-05-18 13:04:19", website: "www.teslamotors.com", address: "Tesla Headquarters\r\n3500 Deer Creek Road\r\nPalo Alto, CA 94304", contact_person_id: 8, note: "", permalink: "tesla", vertical_id: 5, onboarded: true}
])
ActiveRecord::Base.connection.execute("ALTER SEQUENCE companies_id_seq RESTART WITH 9")
