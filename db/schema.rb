# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160701084707) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "activities", force: :cascade do |t|
    t.string   "initiator_type"
    t.integer  "initiator_id"
    t.string   "subject"
    t.text     "message"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "attachments", force: :cascade do |t|
    t.string   "attachable_type"
    t.integer  "attachable_id"
    t.string   "name"
    t.integer  "sub_type"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.hstore   "metadata"
    t.string   "original_filename"
    t.integer  "file_size"
  end

  add_index "attachments", ["attachable_type", "attachable_id", "sub_type"], name: "attachment_index", using: :btree

  create_table "audit_logs", force: :cascade do |t|
    t.integer  "vertical_id"
    t.string   "auditee_type"
    t.integer  "auditee_id"
    t.string   "event"
    t.string   "initiator_type"
    t.integer  "initiator_id"
    t.hstore   "activity_info"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "benefit_categories", force: :cascade do |t|
    t.string "permalink",   limit: 150, null: false
    t.string "name",        limit: 100, null: false
    t.string "description"
  end

  create_table "benefit_type_configurations", force: :cascade do |t|
    t.integer  "benefit_type_id",             null: false
    t.integer  "type",                        null: false
    t.string   "key",             limit: 100, null: false
    t.text     "value"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "benefit_type_detail_attribute_data", force: :cascade do |t|
    t.integer  "benefit_type_detail_attribute_id"
    t.integer  "plan_id"
    t.string   "value"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "benefit_type_detail_attribute_data", ["benefit_type_detail_attribute_id", "plan_id"], name: "benefit_type_detail_attribute_data_uniq_index", unique: true, using: :btree

  create_table "benefit_type_detail_attributes", force: :cascade do |t|
    t.integer  "benefit_type_detail_id"
    t.string   "name_map"
    t.string   "display_name"
    t.integer  "status"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "benefit_type_detail_attributes", ["benefit_type_detail_id", "name_map"], name: "benefit_type_detail_attributes_uniq_index", unique: true, using: :btree

  create_table "benefit_type_details", force: :cascade do |t|
    t.integer  "benefit_type_id", null: false
    t.string   "field",           null: false
    t.boolean  "mandatory_field"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "benefit_type_details", ["benefit_type_id", "field"], name: "index_benefit_type_details_on_benefit_type_id_and_field", unique: true, using: :btree

  create_table "benefit_types", force: :cascade do |t|
    t.string   "name",                null: false
    t.string   "name_map",            null: false
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "benefit_category_id"
    t.text     "subtypes"
    t.string   "type"
    t.integer  "priority"
  end

  add_index "benefit_types", ["name_map"], name: "index_benefit_types_on_name_map", unique: true, using: :btree

  create_table "business_partners", force: :cascade do |t|
    t.integer  "carrier_id", null: false
    t.integer  "partner_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "business_units", force: :cascade do |t|
    t.string  "name",       null: false
    t.string  "code"
    t.integer "company_id"
  end

  create_table "buyout_calculators", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "current_enrollment"
    t.integer  "current_waivers"
    t.decimal  "proposed_annual_buyout_amount", precision: 17, scale: 2
    t.integer  "additional_waivers"
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
  end

  add_index "buyout_calculators", ["company_id"], name: "index_buyout_calculators_on_company_id", using: :btree

  create_table "carrier_benefit_types", force: :cascade do |t|
    t.integer  "carrier_company_id",                null: false
    t.integer  "benefit_type_id",                   null: false
    t.boolean  "status",             default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "carrier_user_benefit_type_details", force: :cascade do |t|
    t.integer  "user_id",                            null: false
    t.integer  "benefit_type_id"
    t.integer  "user_handling_size_id"
    t.text     "locations",             default: [],              array: true
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  create_table "censuses", force: :cascade do |t|
    t.integer  "sub_task_id"
    t.integer  "attachment_id"
    t.integer  "conversion_utility_id"
    t.integer  "created_by"
    t.integer  "last_updated_by"
    t.integer  "status"
    t.hstore   "metadata"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "benefit_type_id"
    t.integer  "employer_company_id"
  end

  add_index "censuses", ["sub_task_id"], name: "index_censuses_on_sub_task_id", using: :btree

  create_table "censuses_employee", force: :cascade do |t|
    t.integer  "employer_company_id",                  null: false
    t.string   "employee_identifier",                  null: false
    t.string   "title"
    t.string   "last_name"
    t.string   "first_name"
    t.integer  "gender",                               null: false
    t.string   "marital_status"
    t.string   "street_address_1"
    t.string   "street_address_2"
    t.string   "city",                                 null: false
    t.string   "state"
    t.string   "residential_zip_code"
    t.date     "date_of_birth",                        null: false
    t.date     "date_of_hire",                         null: false
    t.integer  "hours_per_week",                       null: false
    t.integer  "full_time_status",                     null: false
    t.decimal  "salary",                               null: false
    t.decimal  "bonus"
    t.decimal  "commission"
    t.integer  "overtime"
    t.integer  "key_employee",                         null: false
    t.string   "business_unit",                        null: false
    t.string   "location",                             null: false
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.decimal  "other_salary"
    t.integer  "employee_class_id"
    t.boolean  "tobacco_user",         default: false
    t.date     "date_of_termination"
    t.integer  "status",               default: 0,     null: false
    t.boolean  "is_cobra",             default: false
    t.datetime "terminated_at"
  end

  add_index "censuses_employee", ["employer_company_id", "employee_identifier"], name: "company_employee_index", unique: true, using: :btree

  create_table "censuses_employee_benefit_plan", force: :cascade do |t|
    t.integer  "census_employee_id"
    t.integer  "employer_company_id"
    t.integer  "plan_id"
    t.integer  "benefit_type_id"
    t.integer  "census_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "censuses_employee_benefit_plan", ["census_employee_id", "benefit_type_id"], name: "INDEX1", using: :btree
  add_index "censuses_employee_benefit_plan", ["employer_company_id", "benefit_type_id", "census_employee_id"], name: "UNIQUE_INDEX", unique: true, using: :btree

  create_table "claim_histories", force: :cascade do |t|
    t.integer  "plan_id",                                             null: false
    t.integer  "employer_id",                                         null: false
    t.integer  "no_of_claimant",                                      null: false
    t.decimal  "premium",                    precision: 17, scale: 2, null: false
    t.decimal  "expense",                    precision: 17, scale: 2, null: false
    t.decimal  "loss_ratio",                 precision: 17, scale: 2
    t.decimal  "average_monthly_claim_cost", precision: 17, scale: 2
    t.decimal  "quarterly_claim_cost",       precision: 17, scale: 2
    t.decimal  "quarterly_loss_ratio",       precision: 17, scale: 2
    t.decimal  "yearly_claim_cost",          precision: 17, scale: 2
    t.hstore   "data"
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
  end

  create_table "comments", force: :cascade do |t|
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.text     "content"
    t.integer  "created_by_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "sub_type"
  end

  add_index "comments", ["commentable_type", "commentable_id"], name: "index_comments_on_commentable_type_and_commentable_id", using: :btree

  create_table "commission_plans", force: :cascade do |t|
    t.integer  "commission_id"
    t.string   "plan_name"
    t.string   "vendor_name"
    t.string   "coverage_name"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "no_of_employees"
    t.decimal  "total_cost",      precision: 17, scale: 2
  end

  add_index "commission_plans", ["commission_id"], name: "index_commission_plans_on_commission_id", unique: true, using: :btree

  create_table "commissions", force: :cascade do |t|
    t.string   "type"
    t.integer  "plan_id"
    t.string   "payee"
    t.integer  "general_agency_paid"
    t.integer  "commission_type"
    t.decimal  "compensation",                  precision: 17, scale: 2
    t.integer  "no_of_members"
    t.decimal  "estimated_annual_compensation", precision: 17, scale: 2
    t.datetime "created_at",                                                         null: false
    t.datetime "updated_at",                                                         null: false
    t.date     "date_of_payment"
    t.decimal  "contingent_commission",         precision: 17, scale: 2
    t.integer  "activation_status",                                      default: 0
    t.integer  "employer_company_id"
  end

  add_index "commissions", ["employer_company_id"], name: "index_commissions_on_employer_company_id", using: :btree
  add_index "commissions", ["plan_id"], name: "index_commissions_on_plan_id", using: :btree

  create_table "companies", force: :cascade do |t|
    t.string   "name",                              null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "website",                           null: false
    t.text     "address",                           null: false
    t.integer  "contact_person_id"
    t.text     "note"
    t.string   "permalink",                         null: false
    t.integer  "vertical_id"
    t.boolean  "onboarded",         default: false
  end

  add_index "companies", ["permalink"], name: "index_companies_on_permalink", unique: true, using: :btree
  add_index "companies", ["vertical_id", "permalink"], name: "index_companies_on_vertical_id_and_permalink", unique: true, using: :btree

  create_table "company_industries", force: :cascade do |t|
    t.integer "company_id",  null: false
    t.integer "industry_id", null: false
  end

  create_table "contribution_data", force: :cascade do |t|
    t.integer  "task_id",                                                  null: false
    t.integer  "plan_id",                                                  null: false
    t.integer  "employee_class_id",                                        null: false
    t.integer  "state",                                        default: 0, null: false
    t.hstore   "data"
    t.datetime "created_at",                                               null: false
    t.datetime "updated_at",                                               null: false
    t.integer  "contribution_type",                                        null: false
    t.integer  "contribution_scheme",                                      null: false
    t.string   "contribution_name",                                        null: false
    t.integer  "frequency",                                                null: false
    t.decimal  "buyout_value",        precision: 17, scale: 2
  end

  add_index "contribution_data", ["plan_id", "employee_class_id"], name: "index_contribution_data_on_plan_id_and_employee_class_id", unique: true, using: :btree

  create_table "contribution_model_cb_plans", force: :cascade do |t|
    t.integer  "contribution_model_id"
    t.integer  "plan_id"
    t.integer  "cb_type",               null: false
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "contribution_model_cb_plans", ["contribution_model_id", "plan_id"], name: "UNIQUE_CONTRIBUTION_MODEL_CB_PLANS", unique: true, using: :btree

  create_table "contribution_model_plans", force: :cascade do |t|
    t.integer  "plan_id"
    t.integer  "contribution_model_id"
    t.integer  "employee_class_id"
    t.hstore   "data"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "contribution_model_plans", ["contribution_model_id", "plan_id", "employee_class_id"], name: "UNIQUE_CONTRIBUTION_MODEL_PLANS", unique: true, using: :btree

  create_table "contribution_model_quote_plans", force: :cascade do |t|
    t.integer  "contribution_model_id"
    t.integer  "quote_type",            default: 0
    t.hstore   "quote_type_data"
    t.hstore   "data"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  add_index "contribution_model_quote_plans", ["contribution_model_id"], name: "UNIQUE_CONTRIBUTION_MODEL_QUOTE_PLANS", unique: true, using: :btree

  create_table "contribution_model_renewal_plans", force: :cascade do |t|
    t.integer  "contribution_model_id"
    t.integer  "renewal_type",          default: 0
    t.hstore   "renewal_type_data"
    t.hstore   "data"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  add_index "contribution_model_renewal_plans", ["contribution_model_id"], name: "UNIQUE_CONTRIBUTION_MODEL_RENEWAL_PLANS", unique: true, using: :btree

  create_table "contribution_modelling_data", force: :cascade do |t|
    t.integer  "contribution_model_id",                                      null: false
    t.integer  "plan_id",                                                    null: false
    t.integer  "employee_class_id",                                          null: false
    t.integer  "state",                                          default: 0, null: false
    t.integer  "contribution_type",                              default: 0
    t.integer  "contribution_scheme",                            default: 0
    t.string   "contribution_name",                                          null: false
    t.integer  "frequency"
    t.decimal  "buyout_value",          precision: 17, scale: 2
    t.integer  "modelling_type",                                 default: 0
    t.hstore   "data"
    t.datetime "created_at",                                                 null: false
    t.datetime "updated_at",                                                 null: false
  end

  add_index "contribution_modelling_data", ["contribution_model_id", "plan_id", "employee_class_id"], name: "contribution_modelling_data_index", unique: true, using: :btree

  create_table "contribution_models", force: :cascade do |t|
    t.string   "name",                                      null: false
    t.integer  "model_type",                                null: false
    t.integer  "employer_company_id",                       null: false
    t.integer  "benefit_type_id"
    t.integer  "status",                        default: 1
    t.integer  "created_by",                                null: false
    t.integer  "last_updated_by"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.integer  "state",               limit: 2, default: 0
  end

  add_index "contribution_models", ["employer_company_id", "benefit_type_id", "name"], name: "UNIQUE1", unique: true, using: :btree

  create_table "conversation_messages", force: :cascade do |t|
    t.integer  "conversation_id"
    t.text     "message"
    t.integer  "created_by_id",                   null: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.boolean  "is_task",         default: false, null: false
  end

  add_index "conversation_messages", ["conversation_id"], name: "index_conversation_messages_on_conversation_id", using: :btree

  create_table "conversations", force: :cascade do |t|
    t.integer  "employer_company_id"
    t.integer  "carrier_company_id"
    t.integer  "benefit_type_id",     null: false
    t.string   "type",                null: false
    t.text     "subject"
    t.integer  "created_by_id",       null: false
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "conversations", ["employer_company_id", "benefit_type_id", "carrier_company_id"], name: "CONVERSATION_INDEX", using: :btree

  create_table "conversion_utilities", force: :cascade do |t|
    t.integer  "sub_task_id",     null: false
    t.integer  "created_by"
    t.integer  "last_updated_by"
    t.integer  "status"
    t.hstore   "metadata",        null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "conversion_utilities", ["sub_task_id"], name: "index_conversion_utilities_on_sub_task_id", using: :btree

  create_table "data_entry_task_attachments", force: :cascade do |t|
    t.integer  "data_entry_task_id", null: false
    t.integer  "attachment_id",      null: false
    t.integer  "tagged_by"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "data_entry_task_attachments", ["data_entry_task_id", "attachment_id"], name: "UNIQUE", unique: true, using: :btree

  create_table "data_entry_task_logs", force: :cascade do |t|
    t.integer  "loggable_id"
    t.string   "loggable_type"
    t.integer  "activity_type"
    t.text     "content"
    t.integer  "created_by_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "data_entry_task_logs", ["loggable_type", "loggable_id"], name: "index_data_entry_task_logs_on_loggable_type_and_loggable_id", using: :btree

  create_table "data_entry_tasks", force: :cascade do |t|
    t.string   "type"
    t.integer  "parent_task_id",         default: 0, null: false
    t.integer  "benefit_type_detail_id"
    t.integer  "plan_id"
    t.integer  "taskable_id"
    t.string   "taskable_type"
    t.integer  "level",                  default: 0, null: false
    t.integer  "activity",               default: 0, null: false
    t.integer  "status",                 default: 0, null: false
    t.integer  "assigned_to_id"
    t.integer  "assigned_by_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "request_information",    default: 0
    t.integer  "requested_by_id"
    t.integer  "last_operated_by_id"
    t.integer  "last_reviewed_by_id"
    t.integer  "template_id"
  end

  add_index "data_entry_tasks", ["taskable_type", "taskable_id"], name: "index_data_entry_tasks_on_taskable_type_and_taskable_id", using: :btree

  create_table "dental_claim_data", force: :cascade do |t|
    t.integer  "task_id",                      null: false
    t.integer  "plan_id",                      null: false
    t.integer  "state",            default: 0, null: false
    t.hstore   "data"
    t.integer  "data_entry_by_id"
    t.integer  "reviewed_by_id"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "employee_associations", force: :cascade do |t|
    t.integer  "user_id",    null: false
    t.integer  "company_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "employee_associations", ["user_id", "company_id"], name: "index_employee_associations_on_user_id_and_company_id", using: :btree

  create_table "employee_class_contributions", force: :cascade do |t|
    t.integer  "employee_class_id",     null: false
    t.integer  "contribution_datum_id", null: false
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "employee_class_models", force: :cascade do |t|
    t.integer  "contribution_model_id"
    t.integer  "employee_class_id"
    t.string   "source_type"
    t.integer  "source_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "employee_class_models", ["contribution_model_id", "employee_class_id", "source_type", "source_id"], name: "employee_class_models_index", using: :btree

  create_table "employee_class_plans", force: :cascade do |t|
    t.integer  "employee_class_id", null: false
    t.integer  "plan_id",           null: false
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "employee_classes", force: :cascade do |t|
    t.integer  "location_id",                          null: false
    t.string   "name",                                 null: false
    t.string   "code"
    t.string   "eligibility"
    t.boolean  "key_employee"
    t.boolean  "dependent_eligible"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.boolean  "spouse_eligibility"
    t.boolean  "domestic_partner"
    t.integer  "child_limiting_age"
    t.integer  "hours"
    t.integer  "waiting_period"
    t.integer  "dependent_child_coverage", default: 0, null: false
    t.text     "description"
    t.integer  "class_type",               default: 0, null: false
  end

  create_table "employer_locations", force: :cascade do |t|
    t.integer  "employer_id"
    t.integer  "business_unit_id", null: false
    t.string   "address"
    t.string   "city",             null: false
    t.string   "state",            null: false
    t.string   "country",          null: false
    t.integer  "pin_code",         null: false
    t.integer  "no_of_employees",  null: false
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "employer_survey_answers", force: :cascade do |t|
    t.string   "answer_text",                                 null: false
    t.integer  "employer_survey_question_id",                 null: false
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.boolean  "status",                      default: false
  end

  add_index "employer_survey_answers", ["employer_survey_question_id"], name: "index_employer_survey_answers_on_employer_survey_question_id", using: :btree

  create_table "employer_survey_attempts", force: :cascade do |t|
    t.integer  "employer_survey_question_id", null: false
    t.integer  "employer_survey_answer_id"
    t.integer  "employer_company_id",         null: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "employer_survey_id"
    t.integer  "answered_by_user_id"
    t.string   "answer_order"
  end

  add_index "employer_survey_attempts", ["employer_company_id", "employer_survey_question_id", "employer_survey_answer_id", "answer_order"], name: "UNIQUE_EMPLOYER_SURVEY_ATTEMPTS", unique: true, using: :btree

  create_table "employer_survey_questions", force: :cascade do |t|
    t.string   "question"
    t.integer  "weightage",          default: 1
    t.integer  "question_type"
    t.integer  "display_order"
    t.integer  "employer_survey_id"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "employer_surveys", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "file_viewer_tags", force: :cascade do |t|
    t.integer  "package_id"
    t.integer  "attachment_id"
    t.integer  "package_type"
    t.integer  "file_tag"
    t.string   "file_name"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "file_viewer_tags", ["package_id", "package_type", "file_tag"], name: "file_viewer_tag_index", using: :btree

  create_table "gmr_active_jobs", force: :cascade do |t|
    t.string   "job_id"
    t.integer  "status",      default: 0, null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "description"
    t.integer  "source_id"
    t.string   "source_type"
    t.hstore   "metadata"
    t.string   "job_type"
  end

  add_index "gmr_active_jobs", ["job_id"], name: "INDEX_ON_JOB_ID_GMR_ACTIVE_JOBS", using: :btree
  add_index "gmr_active_jobs", ["source_id"], name: "index_gmr_active_jobs_on_source_id", using: :btree

  create_table "industries", force: :cascade do |t|
    t.string "name",     null: false
    t.string "name_map"
  end

  add_index "industries", ["name_map"], name: "UNIQUE_INDUSTRY_NAME_MAP", unique: true, using: :btree

  create_table "large_claim_data", force: :cascade do |t|
    t.integer  "task_id",                      null: false
    t.integer  "plan_id",                      null: false
    t.integer  "state",            default: 0, null: false
    t.hstore   "data"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "data_entry_by_id"
    t.integer  "reviewed_by_id"
  end

  create_table "locations", force: :cascade do |t|
    t.string   "name",             null: false
    t.string   "code"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "business_unit_id"
  end

  create_table "medical_claim_data", force: :cascade do |t|
    t.integer  "task_id",                      null: false
    t.integer  "plan_id",                      null: false
    t.integer  "state",            default: 0, null: false
    t.hstore   "data"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "data_entry_by_id"
    t.integer  "reviewed_by_id"
  end

  create_table "notification_types", force: :cascade do |t|
    t.string   "activity_type"
    t.integer  "level"
    t.integer  "sender_vertical_id"
    t.integer  "receiver_vertical_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "notification_types", ["sender_vertical_id", "receiver_vertical_id", "activity_type"], name: "sender_receiver_id_activity_type", unique: true, using: :btree

  create_table "notifications", force: :cascade do |t|
    t.integer  "sender_company_id"
    t.string   "source_type"
    t.integer  "source_id"
    t.integer  "sender_user_id"
    t.text     "message"
    t.integer  "notification_type_id"
    t.string   "url"
    t.integer  "receiver_company_id"
    t.string   "receiver_type"
    t.integer  "receiver_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "notifications", ["notification_type_id"], name: "index_notifications_on_notification_type_id", using: :btree
  add_index "notifications", ["receiver_company_id", "receiver_type", "receiver_id"], name: "receiver_company_type_id", using: :btree

  create_table "package_enrollment_details", force: :cascade do |t|
    t.integer  "package_option_id"
    t.integer  "package_option_renewal_option_id"
    t.integer  "renewal_plan_rate_option_label_id"
    t.integer  "enrollment",                                                 default: 0,   null: false
    t.datetime "created_at",                                                               null: false
    t.datetime "updated_at",                                                               null: false
    t.decimal  "volume",                            precision: 17, scale: 7, default: 0.0
    t.decimal  "multiply_factor",                   precision: 17, scale: 7, default: 0.0
  end

  add_index "package_enrollment_details", ["package_option_id", "package_option_renewal_option_id", "renewal_plan_rate_option_label_id"], name: "index_package_enrollment_details", unique: true, using: :btree

  create_table "package_option_renewal_option_details", force: :cascade do |t|
    t.integer  "package_option_renewal_option_id"
    t.integer  "benefit_type_detail_id"
    t.integer  "status"
    t.hstore   "metadata"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "package_option_renewal_option_details", ["package_option_renewal_option_id", "benefit_type_detail_id"], name: "RENEWALOPTIONUNIQUE", unique: true, using: :btree

  create_table "package_option_renewal_options", force: :cascade do |t|
    t.integer  "package_option_id"
    t.integer  "package_plan_renewal_id"
    t.integer  "status"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "package_options", force: :cascade do |t|
    t.integer  "package_id"
    t.string   "name"
    t.integer  "status"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.hstore   "tag"
    t.integer  "finalize_status", default: 0
    t.boolean  "is_liked",        default: false
    t.integer  "sub_package_id"
    t.hstore   "enrollment_data"
  end

  add_index "package_options", ["package_id", "name"], name: "index_package_options_on_package_id_and_name", unique: true, using: :btree

  create_table "package_plan_renewals", force: :cascade do |t|
    t.integer  "plan_id"
    t.string   "name"
    t.string   "type"
    t.hstore   "metadata"
    t.datetime "created_at",                                                            null: false
    t.datetime "updated_at",                                                            null: false
    t.integer  "template_id"
    t.integer  "completed_by_user_id"
    t.integer  "reviewed_by_user_id"
    t.integer  "status"
    t.decimal  "total_estimated_rate",             precision: 17, scale: 2
    t.text     "terms_and_conditions_description"
    t.text     "comment"
    t.integer  "action_status"
    t.integer  "accepted_by_user_id"
    t.text     "attachment_id"
    t.datetime "effective_start_date"
    t.datetime "effective_end_date"
    t.integer  "finalize_status",                                           default: 0
  end

  add_index "package_plan_renewals", ["plan_id", "name"], name: "index_package_plan_renewals_on_plan_id_and_name", unique: true, using: :btree

  create_table "packages", force: :cascade do |t|
    t.string   "unique_id"
    t.integer  "employer_company_id",                 null: false
    t.integer  "carrier_company_id",                  null: false
    t.integer  "benefit_type_id",                     null: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.boolean  "current_plan_liked",  default: false
    t.boolean  "renewal_liked",       default: false
  end

  add_index "packages", ["employer_company_id", "carrier_company_id", "benefit_type_id"], name: "PACKAGE_INDEX", using: :btree
  add_index "packages", ["unique_id"], name: "index_packages_on_unique_id", unique: true, using: :btree

  create_table "permissions", force: :cascade do |t|
    t.string   "name"
    t.string   "controller"
    t.string   "action"
    t.text     "flags"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "plan_change_histories", force: :cascade do |t|
    t.integer  "sub_task_id",                                               null: false
    t.integer  "plan_id",                                                   null: false
    t.integer  "status",                                        default: 0, null: false
    t.date     "plan_change_date",                                          null: false
    t.integer  "template_field_id",                                         null: false
    t.integer  "template_column_id",                                        null: false
    t.text     "from",                                                      null: false
    t.text     "to",                                                        null: false
    t.decimal  "medical_change_value", precision: 17, scale: 7,             null: false
    t.decimal  "rx_change_value",      precision: 17, scale: 7,             null: false
    t.datetime "created_at",                                                null: false
    t.datetime "updated_at",                                                null: false
    t.decimal  "medical_weighting",    precision: 10, scale: 7
    t.decimal  "rx_weighting",         precision: 10, scale: 7
  end

  add_index "plan_change_histories", ["sub_task_id", "plan_change_date", "template_field_id", "template_column_id"], name: "UNIQUE_PLAN_CHANGE_RECORD", unique: true, using: :btree

  create_table "plan_design_data", force: :cascade do |t|
    t.integer  "task_id",                    null: false
    t.boolean  "is_draft",   default: false, null: false
    t.hstore   "data"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "plan_id",                    null: false
  end

  create_table "plan_design_slider_histories", force: :cascade do |t|
    t.integer  "plan_design_slider_id"
    t.integer  "plan_id"
    t.hstore   "sliders_data"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "plan_design_slider_histories", ["plan_design_slider_id"], name: "index_plan_design_slider_histories_on_plan_design_slider_id", using: :btree

  create_table "plan_design_sliders", force: :cascade do |t|
    t.integer  "plan_id"
    t.hstore   "sliders_data"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "name"
    t.integer  "employer_company_id"
    t.integer  "carrier_company_id"
    t.integer  "benefit_type_id"
    t.boolean  "deleted"
  end

  add_index "plan_design_sliders", ["employer_company_id", "carrier_company_id", "benefit_type_id"], name: "index_on_companies", using: :btree
  add_index "plan_design_sliders", ["plan_id"], name: "index_plan_design_sliders_on_plan_id", unique: true, using: :btree

  create_table "plan_document_request_statuses", force: :cascade do |t|
    t.integer  "plan_document_request_id"
    t.integer  "plan_id"
    t.integer  "status"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "plan_document_requests", force: :cascade do |t|
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "created_by_id"
  end

  create_table "plan_options", force: :cascade do |t|
    t.integer  "plan_id",                   null: false
    t.string   "name",                      null: false
    t.integer  "option_type",               null: false
    t.decimal  "value",       default: 0.0, null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "plan_options", ["plan_id", "name"], name: "index_plan_options_on_plan_id_and_name", unique: true, using: :btree
  add_index "plan_options", ["plan_id"], name: "index_plan_options_on_plan_id", using: :btree

  create_table "plan_rate_history_attributes", force: :cascade do |t|
    t.integer  "plan_id",              null: false
    t.integer  "sub_task_id",          null: false
    t.date     "effective_start_date"
    t.date     "effective_end_date"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.date     "renewal_request_date"
  end

  add_index "plan_rate_history_attributes", ["plan_id"], name: "UNIQUE_PLAN", unique: true, using: :btree
  add_index "plan_rate_history_attributes", ["sub_task_id"], name: "UNIQUE_SUB_TASK", unique: true, using: :btree

  create_table "plan_rate_option_labels", force: :cascade do |t|
    t.integer  "plan_rate_option_id"
    t.integer  "rate_option_label_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.hstore   "metadata"
  end

  create_table "plan_rate_options", force: :cascade do |t|
    t.integer  "plan_id"
    t.integer  "sub_task_id"
    t.integer  "rate_option_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "parent_id",      default: 0
    t.hstore   "metadata"
    t.integer  "rate_frequency"
  end

  create_table "plan_renewals", force: :cascade do |t|
    t.integer  "underwriting_id"
    t.integer  "plan_id"
    t.integer  "scenario",                                         default: 0
    t.date     "date_issue",                                                   null: false
    t.decimal  "initial_renewal",         precision: 10, scale: 7,             null: false
    t.decimal  "current_renewal_offer",   precision: 10, scale: 7
    t.decimal  "final_renewal",           precision: 10, scale: 7
    t.date     "renewal_begin_date"
    t.date     "renewal_end_date"
    t.decimal  "pooling_point",           precision: 17, scale: 7,             null: false
    t.decimal  "pooling_point_max_claim", precision: 10, scale: 7,             null: false
    t.decimal  "medical_trend",           precision: 10, scale: 7,             null: false
    t.decimal  "rx_trend",                precision: 10, scale: 7,             null: false
    t.decimal  "margin",                  precision: 10, scale: 7,             null: false
    t.integer  "live_basis",                                       default: 0
    t.decimal  "experience_credibility",  precision: 10, scale: 7,             null: false
    t.hstore   "capitation",                                                   null: false
    t.hstore   "pool_charges",                                                 null: false
    t.hstore   "retention",                                                    null: false
    t.hstore   "taxes_1",                                                      null: false
    t.hstore   "taxes_2",                                                      null: false
    t.hstore   "taxes_3",                                                      null: false
    t.hstore   "commission",                                                   null: false
    t.integer  "created_by_id"
    t.datetime "created_at",                                                   null: false
    t.datetime "updated_at",                                                   null: false
    t.integer  "funding_arrangement",                              default: 0
    t.date     "plan_inception_date"
    t.hstore   "associated_plan_ids"
    t.decimal  "experience_adjustment",   precision: 10, scale: 7
    t.decimal  "manual_rate_adjustment",  precision: 10, scale: 7
    t.decimal  "reserve_factor",          precision: 10, scale: 7
  end

  create_table "plan_requests", force: :cascade do |t|
    t.string   "description",     null: false
    t.integer  "benefit_type_id", null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "plans", force: :cascade do |t|
    t.integer  "carrier_company_id"
    t.integer  "package_id"
    t.integer  "benefit_type_id",                                   null: false
    t.string   "name",                   limit: 50,                 null: false
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.integer  "employer_company_id"
    t.string   "carrier_contact_name",   limit: 50
    t.string   "carrier_contact_email",  limit: 50
    t.string   "carrier_work_number",    limit: 20
    t.string   "carrier_work_extension", limit: 6
    t.integer  "years_with_carrier"
    t.integer  "old_carrier_company_id"
    t.string   "policy_number",          limit: 50
    t.integer  "broker_company_id"
    t.string   "broker_contact_name",    limit: 50
    t.string   "broker_contact_email",   limit: 50
    t.string   "broker_work_number",     limit: 20
    t.string   "broker_work_extension",  limit: 6
    t.boolean  "authorized_to_contact",             default: false
    t.integer  "created_by_id"
    t.integer  "contribution"
    t.integer  "status",                            default: 0
    t.datetime "inception_date"
    t.datetime "effective_date"
    t.decimal  "actuary_percentage",                default: 0.0
    t.integer  "current_status",                    default: 0
    t.string   "description"
  end

  create_table "platform_files", force: :cascade do |t|
    t.string   "name",               limit: 50, null: false
    t.integer  "carrier_company_id",            null: false
    t.integer  "benefit_type_id",               null: false
    t.hstore   "data"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "platform_files", ["benefit_type_id", "carrier_company_id", "name"], name: "unique_index_on_platform_files_by_benefit_and_company_and_name", unique: true, using: :btree
  add_index "platform_files", ["benefit_type_id", "carrier_company_id"], name: "index_platform_files_on_benefit_type_id_and_carrier_company_id", using: :btree

  create_table "quote_benefit_type_detail_attribute_data", force: :cascade do |t|
    t.integer  "benefit_type_detail_attribute_id"
    t.integer  "quote_id"
    t.string   "value"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "quote_benefit_type_detail_attribute_data", ["benefit_type_detail_attribute_id", "quote_id"], name: "quote_btd_attribute_data_uniq_index", unique: true, using: :btree

  create_table "quote_package_enrollment_details", force: :cascade do |t|
    t.integer  "enrollment",                                               default: 0,   null: false
    t.integer  "quote_package_option_id"
    t.integer  "quote_package_option_quote_id"
    t.integer  "quote_plan_rate_option_label_id"
    t.datetime "created_at",                                                             null: false
    t.datetime "updated_at",                                                             null: false
    t.decimal  "volume",                          precision: 17, scale: 7, default: 0.0
    t.decimal  "multiply_factor",                 precision: 17, scale: 7, default: 0.0
  end

  add_index "quote_package_enrollment_details", ["quote_package_option_id", "quote_package_option_quote_id", "quote_plan_rate_option_label_id"], name: "index_quote_package_enrollment_details", unique: true, using: :btree

  create_table "quote_package_option_quotes", force: :cascade do |t|
    t.integer  "quote_package_option_id", null: false
    t.integer  "quote_id",                null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "quote_package_option_quotes", ["quote_package_option_id", "quote_id"], name: "QUOTE_PACKAGE_OPTION_QUOTE_INDEX", unique: true, using: :btree

  create_table "quote_package_options", force: :cascade do |t|
    t.integer  "quote_package_id",                 null: false
    t.string   "name",                             null: false
    t.integer  "status"
    t.hstore   "tag"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "finalize_status",  default: 0
    t.boolean  "is_liked",         default: false
    t.integer  "sub_package_id"
    t.hstore   "enrollment_data"
  end

  add_index "quote_package_options", ["quote_package_id", "name"], name: "index_quote_package_options_on_quote_package_id_and_name", unique: true, using: :btree

  create_table "quote_packages", force: :cascade do |t|
    t.string   "unique_id"
    t.string   "name"
    t.integer  "employer_company_id",                 null: false
    t.integer  "benefit_type_id",                     null: false
    t.integer  "created_by_user_id",                  null: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.boolean  "current_plan_liked",  default: false
  end

  add_index "quote_packages", ["employer_company_id", "benefit_type_id"], name: "QUOTE_PACKAGE_INDEX", using: :btree
  add_index "quote_packages", ["unique_id"], name: "index_quote_packages_on_unique_id", unique: true, using: :btree

  create_table "quote_plan_rate_option_labels", force: :cascade do |t|
    t.integer  "quote_plan_rate_option_id"
    t.integer  "rate_option_label_id"
    t.hstore   "metadata"
    t.integer  "enrollment"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "quote_plan_rate_options", force: :cascade do |t|
    t.integer  "quotable_id"
    t.string   "quotable_type"
    t.integer  "rate_option_id"
    t.integer  "parent_id",                               default: 0
    t.hstore   "metadata"
    t.integer  "rate_frequency"
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.decimal  "volume",         precision: 17, scale: 2
  end

  add_index "quote_plan_rate_options", ["quotable_id", "quotable_type"], name: "index_renewal_pro_on_quotable_id", using: :btree

  create_table "quote_request_plannables", force: :cascade do |t|
    t.integer "quote_request_id"
    t.string  "plannable_type"
    t.integer "plannable_id"
  end

  add_index "quote_request_plannables", ["quote_request_id", "plannable_type", "plannable_id"], name: "index_quote_request_plannables_on_quote_request_and_plannable", unique: true, using: :btree

  create_table "quote_request_recipients", force: :cascade do |t|
    t.integer "quote_request_plannable_id"
    t.integer "recipient_id"
  end

  create_table "quote_requests", force: :cascade do |t|
    t.integer  "benefit_type_id",                                                null: false
    t.integer  "carrier_company_id",                                             null: false
    t.integer  "employer_company_id",                                            null: false
    t.boolean  "status",                                         default: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "initiator_id",                                                   null: false
    t.boolean  "is_current_plan",                                default: false
    t.boolean  "quote_based_on_profile",                         default: false
    t.decimal  "option_percentage",      precision: 5, scale: 2, default: 0.0
    t.decimal  "commission",             precision: 5, scale: 2, default: 0.0
  end

  add_index "quote_requests", ["employer_company_id", "carrier_company_id", "benefit_type_id"], name: "unique_index_quote_per_employer_per_carrier_per_benefit_type", unique: true, using: :btree

  create_table "quotes", force: :cascade do |t|
    t.integer  "quote_package_id",                                          null: false
    t.integer  "plan_id",                                                   null: false
    t.string   "name",                                                      null: false
    t.integer  "carrier_company_id",                                        null: false
    t.date     "rate_effective_date"
    t.date     "quote_effective_date"
    t.string   "type"
    t.integer  "status"
    t.integer  "completed_by_user_id"
    t.integer  "reviewed_by_user_id"
    t.hstore   "tag"
    t.datetime "created_at",                                                null: false
    t.datetime "updated_at",                                                null: false
    t.hstore   "metadata"
    t.decimal  "total_estimated_rate",             precision: 17, scale: 2
    t.integer  "template_id"
    t.text     "terms_and_conditions_description"
    t.text     "comment"
    t.integer  "action_status"
    t.integer  "accepted_by_user_id"
    t.text     "attachment_id"
    t.datetime "effective_start_date"
    t.datetime "effective_end_date"
  end

  add_index "quotes", ["quote_package_id", "carrier_company_id", "plan_id"], name: "QUOTE_INDEX", using: :btree

  create_table "rate_arrangements", force: :cascade do |t|
    t.string   "name"
    t.string   "name_map",        null: false
    t.hstore   "metadata"
    t.integer  "benefit_type_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "rate_arrangements", ["benefit_type_id", "name_map"], name: "index_rate_arrangements_on_benefit_type_id_and_name_map", unique: true, using: :btree

  create_table "rate_data", force: :cascade do |t|
    t.integer  "task_id",                    null: false
    t.integer  "plan_id",                    null: false
    t.boolean  "is_draft",   default: false, null: false
    t.hstore   "data"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "rate_option_labels", force: :cascade do |t|
    t.integer  "rate_option_id"
    t.string   "name"
    t.string   "name_map"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "presence_type",  default: 0
  end

  create_table "rate_options", force: :cascade do |t|
    t.integer  "benefit_type_id"
    t.integer  "parent_id",           default: 0
    t.string   "name"
    t.string   "name_map"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "rate_arrangement_id"
  end

  create_table "recommendations", force: :cascade do |t|
    t.integer  "employer_company_id",             null: false
    t.integer  "recommendation_type", default: 0
    t.string   "recommendation_text"
    t.hstore   "data"
    t.string   "impact"
    t.integer  "created_by_id"
    t.integer  "updated_by_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "recommendations", ["employer_company_id"], name: "index_recommendations_on_employer_company_id", using: :btree

  create_table "renewal_benefit_type_detail_attribute_data", force: :cascade do |t|
    t.integer  "benefit_type_detail_attribute_id"
    t.integer  "renewal_id"
    t.string   "value"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "renewal_benefit_type_detail_attribute_data", ["benefit_type_detail_attribute_id", "renewal_id"], name: "renewal_benefit_type_detail_attribute_data_uniq_index", unique: true, using: :btree

  create_table "renewal_dates_group_plans", force: :cascade do |t|
    t.integer  "renewal_dates_group_id"
    t.integer  "plan_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "renewal_dates_groups", force: :cascade do |t|
    t.date     "renewal_date"
    t.integer  "status",       default: 0
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "renewal_plan_rate_option_labels", force: :cascade do |t|
    t.integer  "renewal_plan_rate_option_id"
    t.integer  "rate_option_label_id"
    t.hstore   "metadata"
    t.integer  "enrollment"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "renewal_plan_rate_options", force: :cascade do |t|
    t.integer  "renewable_id"
    t.string   "renewable_type"
    t.integer  "rate_option_id"
    t.integer  "parent_id",                               default: 0
    t.hstore   "metadata"
    t.integer  "rate_frequency"
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.decimal  "volume",         precision: 17, scale: 2
  end

  add_index "renewal_plan_rate_options", ["renewable_id", "renewable_type"], name: "index_renewal_pro_on_renewable_id", using: :btree

  create_table "renewals", force: :cascade do |t|
    t.integer  "plan_id",                   null: false
    t.boolean  "out_to_bid",                null: false
    t.integer  "renewal_representative_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "requested_plans", force: :cascade do |t|
    t.integer  "plan_design_id", null: false
    t.hstore   "overrides"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "role_permissions", force: :cascade do |t|
    t.integer  "role_id"
    t.integer  "permission_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "role_permissions", ["role_id", "permission_id"], name: "index_role_permissions_on_role_id_and_permission_id", unique: true, using: :btree
  add_index "role_permissions", ["role_id"], name: "index_role_permissions_on_role_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.integer "company_id"
    t.string  "name"
    t.string  "role_map"
    t.boolean "status",      default: true
    t.integer "vertical_id"
  end

  create_table "security_questions", force: :cascade do |t|
    t.string   "question"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "service_requests", force: :cascade do |t|
    t.integer  "employer_location_id",      null: false
    t.integer  "broker_id"
    t.integer  "carrier_id",                null: false
    t.integer  "plan_id",                   null: false
    t.string   "status"
    t.integer  "service_representative_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "simple_current_rate_components", force: :cascade do |t|
    t.integer  "underwriting_id",                                          null: false
    t.integer  "members"
    t.decimal  "annual_premium",      precision: 17, scale: 2
    t.decimal  "pooling",             precision: 17, scale: 2
    t.decimal  "expenses",            precision: 17, scale: 2
    t.decimal  "claims",              precision: 17, scale: 2
    t.decimal  "margin",              precision: 17, scale: 2
    t.decimal  "reserve",             precision: 17, scale: 2
    t.integer  "publish_status",                               default: 0
    t.integer  "funding_arrangement"
    t.datetime "from_date"
    t.datetime "to_date"
    t.decimal  "credibility",         precision: 17, scale: 2
    t.decimal  "manual_rate_action",  precision: 17, scale: 2
    t.integer  "flag"
    t.datetime "created_at",                                               null: false
    t.datetime "updated_at",                                               null: false
  end

  add_index "simple_current_rate_components", ["underwriting_id"], name: "index_simple_current_rate_components_on_underwriting_id", using: :btree

  create_table "simple_project_renewals", force: :cascade do |t|
    t.integer  "underwriting_id",                                          null: false
    t.integer  "members"
    t.decimal  "annual_premium",      precision: 17, scale: 2
    t.decimal  "pooling",             precision: 17, scale: 2
    t.decimal  "expenses",            precision: 17, scale: 2
    t.decimal  "claims",              precision: 17, scale: 2
    t.decimal  "margin",              precision: 17, scale: 2
    t.decimal  "reserve",             precision: 17, scale: 2
    t.integer  "publish_status",                               default: 0
    t.datetime "created_at",                                               null: false
    t.datetime "updated_at",                                               null: false
    t.integer  "funding_arrangement"
    t.datetime "from_date"
    t.datetime "to_date"
    t.decimal  "projected_renewal",   precision: 17, scale: 2
    t.decimal  "credibility",         precision: 17, scale: 2
    t.decimal  "manual_rate_action",  precision: 17, scale: 2
  end

  add_index "simple_project_renewals", ["underwriting_id"], name: "index_simple_project_renewals_on_underwriting_id", using: :btree

  create_table "subpackages", force: :cascade do |t|
    t.string   "unique_id"
    t.integer  "packagable_id"
    t.string   "packagable_type"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "employer_company_id"
  end

  add_index "subpackages", ["packagable_type", "packagable_id"], name: "subpackage_index", using: :btree

  create_table "task_plans", force: :cascade do |t|
    t.integer  "task_id",            null: false
    t.integer  "associated_plan_id", null: false
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "template_entities", force: :cascade do |t|
    t.string   "type"
    t.integer  "template_section_id",                         null: false
    t.string   "name",                                        null: false
    t.integer  "order",                          default: -1, null: false
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.hstore   "properties"
    t.string   "key",                 limit: 50
    t.boolean  "initial_marketing"
    t.boolean  "current_plans"
  end

  add_index "template_entities", ["type", "template_section_id", "key"], name: "index_template_entities_on_type_and_template_section_id_and_key", unique: true, using: :btree
  add_index "template_entities", ["type", "template_section_id"], name: "index_template_entities_on_type_and_template_section_id", using: :btree

  create_table "template_inputs", force: :cascade do |t|
    t.integer  "input_type",          null: false
    t.hstore   "properties"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "template_section_id", null: false
    t.integer  "template_field_id",   null: false
    t.integer  "template_column_id",  null: false
  end

  add_index "template_inputs", ["template_column_id"], name: "index_template_inputs_on_template_column_id", using: :btree
  add_index "template_inputs", ["template_field_id"], name: "index_template_inputs_on_template_field_id", using: :btree
  add_index "template_inputs", ["template_section_id", "template_field_id", "template_column_id"], name: "unique_index_on_template_inputs_by_section_and_field_and_column", unique: true, using: :btree
  add_index "template_inputs", ["template_section_id"], name: "index_template_inputs_on_template_section_id", using: :btree

  create_table "template_sections", force: :cascade do |t|
    t.integer  "template_sheet_id",                         null: false
    t.string   "name",              limit: 50,              null: false
    t.integer  "order",                        default: -1, null: false
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.string   "key",               limit: 50
  end

  add_index "template_sections", ["template_sheet_id", "key"], name: "index_template_sections_on_template_sheet_id_and_key", unique: true, using: :btree
  add_index "template_sections", ["template_sheet_id"], name: "index_template_sections_on_template_sheet_id", using: :btree

  create_table "template_sheets", force: :cascade do |t|
    t.integer  "template_id",                         null: false
    t.string   "name",        limit: 50
    t.integer  "order",                  default: -1, null: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "key",         limit: 50
  end

  add_index "template_sheets", ["template_id", "key"], name: "index_template_sheets_on_template_id_and_key", unique: true, using: :btree
  add_index "template_sheets", ["template_id"], name: "index_template_sheets_on_template_id", using: :btree

  create_table "templates", force: :cascade do |t|
    t.integer  "benefit_type_id",                        null: false
    t.string   "name",            limit: 50,             null: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.datetime "locked_uptil"
    t.integer  "locked_by_id"
    t.uuid     "locking_uuid"
    t.float    "version"
    t.integer  "state",                      default: 0, null: false
  end

  add_index "templates", ["benefit_type_id", "version"], name: "index_templates_on_benefit_type_id_and_version", unique: true, using: :btree

  create_table "underwriting_periods", force: :cascade do |t|
    t.integer  "plan_renewal_id"
    t.integer  "large_claim_datum_id"
    t.date     "begin_date"
    t.date     "end_date"
    t.integer  "weighting"
    t.decimal  "medical_lag",            precision: 17, scale: 7
    t.decimal  "rx_lag",                 precision: 17, scale: 7
    t.decimal  "medical_maturity",       precision: 17, scale: 7
    t.decimal  "rx_maturity",            precision: 17, scale: 7
    t.decimal  "plan_change",            precision: 17, scale: 7
    t.integer  "number_large_claims"
    t.integer  "period_no"
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.decimal  "medical_claim_adjusted", precision: 10, scale: 7
    t.decimal  "rx_claim_adjusted",      precision: 10, scale: 7
    t.integer  "experience_adjustment"
    t.integer  "manual_rate_adjustment"
  end

  create_table "underwritings", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "status",              default: 0
    t.integer  "publish_status",      default: 0
    t.integer  "employer_company_id"
    t.integer  "underwriting_type",   default: 0,  null: false
    t.integer  "parent_id",           default: -1
  end

  add_index "underwritings", ["name"], name: "index_underwritings_on_name", unique: true, using: :btree

  create_table "uploads", force: :cascade do |t|
    t.integer  "company_id",                      null: false
    t.integer  "created_by_user_id",              null: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "plan_document_request_status_id"
  end

  add_index "uploads", ["company_id"], name: "index_uploads_on_company_id", using: :btree
  add_index "uploads", ["created_by_user_id"], name: "index_uploads_on_created_by_user_id", using: :btree

  create_table "user_handling_sizes", force: :cascade do |t|
    t.string   "size",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_roles", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "role_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_roles", ["user_id", "role_id"], name: "index_user_roles_on_user_id_and_role_id", unique: true, using: :btree
  add_index "user_roles", ["user_id"], name: "index_user_roles_on_user_id", using: :btree

  create_table "user_security_questions", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "security_question_id"
    t.string   "encrypted_answer"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                                default: "", null: false
    t.string   "encrypted_password",                   default: ""
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                        default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "type"
    t.integer  "company_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.integer  "invited_by_id"
    t.string   "invited_by_type"
    t.integer  "invitations_count",                    default: 0
    t.integer  "failed_attempts",                      default: 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "work_number",                                       null: false
    t.string   "cell_number",                                       null: false
    t.string   "name_prefix",               limit: 10
    t.string   "work_extension",            limit: 6
    t.string   "first_name",                limit: 30,              null: false
    t.string   "middle_name",               limit: 30
    t.string   "last_name",                 limit: 30,              null: false
    t.datetime "last_notification_read_at"
    t.integer  "representing_company_id"
    t.integer  "status",                               default: 0
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["invitation_token"], name: "index_users_on_invitation_token", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

  create_table "verticals", force: :cascade do |t|
    t.string "name", limit: 50, null: false
  end

  add_index "verticals", ["name"], name: "index_verticals_on_name", unique: true, using: :btree

  add_foreign_key "carrier_benefit_types", "benefit_types", name: "carrier_benefit_types_fkey_on_benefit_type_id", on_delete: :cascade
  add_foreign_key "carrier_benefit_types", "companies", column: "carrier_company_id", name: "carrier_benefit_types_fkey_on_carrier_company_id", on_delete: :cascade
  add_foreign_key "carrier_user_benefit_type_details", "users", name: "carrier_user_benefit_type_details_fkey_on_user_id", on_delete: :cascade
  add_foreign_key "package_plan_renewals", "templates", name: "package_plan_renewals_fkey_on_template_id", on_delete: :cascade
  add_foreign_key "package_plan_renewals", "users", column: "completed_by_user_id", name: "package_plan_renewals_fkey_on_completed_by_user_id", on_delete: :cascade
  add_foreign_key "package_plan_renewals", "users", column: "reviewed_by_user_id", name: "package_plan_renewals_fkey_on_reviewed_by_user_id", on_delete: :cascade
  add_foreign_key "plan_requests", "benefit_types", name: "plan_requests_fkey_on_benefit_type_id", on_delete: :cascade
  add_foreign_key "quote_request_recipients", "users", column: "recipient_id", name: "quote_request_recipients_fkey_on_recipient_id", on_delete: :cascade
  add_foreign_key "quote_requests", "benefit_types", name: "quote_requests_fkey_on_benefit_type_id", on_delete: :cascade
  add_foreign_key "quote_requests", "companies", column: "carrier_company_id", name: "quote_requests_fkey_on_carrier_company_id", on_delete: :cascade
  add_foreign_key "quote_requests", "companies", column: "employer_company_id", name: "quote_requests_fkey_on_employer_company_id", on_delete: :cascade
  add_foreign_key "quote_requests", "users", column: "initiator_id", name: "quote_requests_fkey_on_initiator_id", on_delete: :cascade
end
