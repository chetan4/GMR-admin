source 'https://rubygems.org'
# ruby '2.1.3'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.0'

# Use postgresql as the database for Active Record
gem 'pg'
# gem 'mongoid'
## mongoid.yml is present if finally we are completly removing it, we must delete it.

# gem for caching
gem 'dalli-elasticache'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0.0'

# Icon library
gem 'font-awesome-sass', '>= 4.4.0'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'jquery-ui-rails'

# Bootstrap Datetime picker
gem 'momentjs-rails'

# Mustache
gem 'mustache-js-rails'

# Pagination
gem 'kaminari'

# Select2 for autocompletors
gem 'select2-rails', '~> 3.5.9.3'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Authorization
gem 'devise'
gem 'devise_invitable'
gem 'devise-async'

gem 'sinatra', :require => nil
gem 'sidekiq'
gem 'aws-sdk'
gem 'rmagick'

gem 'airbrake', '~> 4.3.4'
gem 'nested-hstore', '~> 0.1.2'
gem 'hstore_accessor', '~> 1.0.3'
gem 'spreadsheet'
gem 'jshint'
gem 'whenever'
gem 'wkhtmltopdf-binary-edge', '~> 0.12.2.1'
gem 'wicked_pdf'
# gem 'turbo-sprockets-rails3'
gem 'activerecord-import'
gem 'newrelic_rpm'
gem 'god'

group :development do
  # Using mina for deployment
  gem 'mina'
  # Use thin as the app server
  gem 'thin'
  gem 'mina-sidekiq'
  gem 'sextant'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'awesome_print', :require => 'ap'
  gem 'hirb'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'

  gem 'annotate', '~> 2.6.5'
  gem 'quiet_assets'
  gem 'mailcatcher'
  gem 'pry'
  gem 'pry-nav'
  gem 'seed_dump'
  gem 'bullet'
  gem 'dalli'
  # gem 'active-record-query-trace'
  gem 'zeus'
  gem 'foreman'
  gem 'rubocop'
  gem 'brakeman'
end

group :test do
  # MiniTest
  gem 'minitest-spec-rails', '5.2.0'
  gem 'minitest-rails-capybara', '2.1.1'
  gem 'minitest-reporters', '1.0.5'
  gem 'guard-minitest', '2.3.1'
  gem 'launchy'
end

group :development, :test do
  gem 'rspec-rails', '~> 3.0'
  gem 'factory_girl_rails', "~> 4.2.1"
  gem 'faker', '1.4.3'
  gem 'simplecov', '0.9.2', require: false
  gem 'database_cleaner', '1.3.0'
end

# group :assets do
#   gem 'turbo-sprockets-rails3'
#   gem 'sprockets', '2.11.0'
# end
