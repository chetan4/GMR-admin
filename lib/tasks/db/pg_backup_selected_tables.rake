namespace :db do
  desc "Runs pg_dump against the environment's database."
  task pg_backup_selected_tables: :environment do
    @db_config = Rails.application.config.database_configuration[Rails.env]

    TABLES_TO_DUMP = %w(benefit_types benefit_categories
      benefit_type_details templates template_sheets template_sections template_inputs template_entities)

    TABLES_TO_DUMP.each do |table_name|
      create_seed_data(table_name)
    end
  end

  def create_seed_data(table_name)
    puts "\n\n\n\n============================= STARTED DUMP OF #{table_name} =============================\n\n\n\n"
    file_name = "#{Rails.root}/db/table_backup/#{table_name}.sql"

    system "PGPASSWORD=#{@db_config['password']} pg_dump --verbose --clean --no-owner --no-acl "\
    "--host=#{@db_config['host']} "\
    "--port=#{@db_config['port']} "\
    "--username=#{@db_config['username']} "\
    "--dbname=#{@db_config['database']} "\
    "-t #{table_name}" \
    "> #{file_name}"
    puts "\n\n\n\n============================= COMPLETED DUMP OF #{table_name} =============================\n\n\n\n"
  end
end

# "-t users "\
# "-t templates "\

# "-t companies" \
# "-t benefit_categories" \

# "-t benefit_types" \
# "-t benefit_type_details" \

# "-t template_sheets" \
# "-t template_sections" \

# "-t template_inputs" \
# "-t template_entities" \