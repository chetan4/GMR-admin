namespace :db do
  desc "Runs pg_dump against the environment's database."
  task pg_dump: :environment do
    db_config = Rails.application.config.database_configuration[Rails.env]

    #NOTE:: create folder /var/app/gmr/db_dumps with 0777 permission

    #TODO Kapil: Dump it to appropriate folder based on rails env
    file_name = "#{Rails.env}_dump_#{Time.now.to_i}.sql"
    # backup_file = "/var/app/gmr/db_dumps/#{file_name}"
    backup_file = "#{Rails.root}/db/table_backup/#{file_name}"

    system "PGPASSWORD=#{db_config['password']} pg_dump --verbose --clean --no-owner --no-acl --host=#{db_config['host']} --port=#{db_config['port']} --username=#{db_config['username']} --dbname=#{db_config['database']} > #{backup_file}"
    puts "Dump created on local machine."
    puts "Please wait uploaidng dump to s3.."
    compress_n_upload_dump_to_s3(backup_file, file_name)

    puts "Deleting local file."
    system "rm #{backup_file}"
  end

  def compress_n_upload_dump_to_s3(backup_file, file_name)
    options = {acl: 'public-read'}
    bucket = 'gmrdata'
    url = "#{Rails.root}/db/table_backup/#{file_name}.tar.gz"

    compressed_file_name = "#{backup_file}.tar.gz"
    system "tar cvzf #{compressed_file_name} #{backup_file}"

    response = Util::AwsUtils.upload_to_s3(compressed_file_name, bucket, url, options)
    if response[:status] != 'error'
      puts "Dump Uploaded to s3..!"
    else
      puts "Error in uploading, Dump to s3..!"
    end
  end

  desc "Loads a pg_dump file into the environment's database."
  task pg_restore: :environment do
    db_config = Rails.application.config.database_configuration[Rails.env]
    input = ''
    STDOUT.puts "Please provide the full path to the .sql backup file you want to restore:"
    input = STDIN.gets.chomp
    raise "File not found." unless File.exist?(input)
    system "psql -U #{db_config['username']} -d #{db_config['database']} -f #{input}"
  end
end
