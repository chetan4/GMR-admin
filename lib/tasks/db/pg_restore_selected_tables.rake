namespace :db do
  desc "Loads a pg_dump file into the environment's database."
  task pg_restore_selected_tables: :environment do
    @db_config = Rails.application.config.database_configuration[Rails.env]

    TABLES_TO_RESTORE = %w(benefit_types benefit_categories
      benefit_type_details templates template_sheets template_sections template_inputs template_entities)

    TABLES_TO_RESTORE.each do |table_name|
      dump_seed_data(table_name)
      update_user_in_templates if table_name == 'templates'
    end
  end

  def dump_seed_data(table_name)
    file_path = File.expand_path("#{Rails.root}/db/table_backup/#{table_name}.sql")
    raise "File not found." unless File.exist?(file_path)

    puts "\n\n\n\n============================= STARTED IMPORT FOR #{table_name} =============================\n\n\n\n"
    # system "PGPASSWORD=#{@db_config['password']} psql -U #{@db_config['username']} -d #{@db_config['database']}  -f #{file_path}"
    system "PGPASSWORD=#{@db_config['password']} psql --host=#{@db_config['host']} --port=#{@db_config['port']} --username=#{@db_config['username']} --dbname=#{@db_config['database']}  --file=#{file_path}"
    puts "\n\n\n\n============================= COMPLETED IMPORT FOR #{table_name} =============================\n\n\n\n"
  end

  def update_user_in_templates
    admin_user_id = Admin.first.id
    Template.all.update_all(locked_by_id: admin_user_id)
  end
end
