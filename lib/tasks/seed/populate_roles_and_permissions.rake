namespace :seed do
  #Rake task to pupulate roles
  desc "rake seed:populate_roles RAILS_ENV=<environment_name> --trace"
  task populate_roles: :environment do
    puts "Creating Roles..."
    admin_vertical = Vertical.find_by_name("Admin")
    default_company = Company.first_or_create(:name => "default",
                                              :vertical => admin_vertical,
                                              :website => "www.goodmoonrunning.com",
                                              :address => "GMR HQ")

    require Rails.root.join('db', 'role_permissions_data', 'roles_data.rb').to_s
    roles_data_hash = ROLES
    vertical_by_name = Vertical.all.index_by(&:name)
    roles_data_hash.each do |vertical, role_hash|
      role_hash.each do |role|
        default_company.roles.where(vertical: vertical_by_name[vertical.to_s],
                                    name: role[:name]).first_or_create
      end
    end
    puts "Roles created..."
  end

  desc "rake seed:populate_roles_and_permissions RAILS_ENV=<environment_name> --trace"
  task populate_roles_and_permissions: [:populate_roles] do
    puts "Creating Permissions..."

    PERMISSIONS = YAML.load_file(Rails.root.join('db/role_permissions_data/permission.yml'))[:PERMISSIONS]
    # Start processing on sheet starting with second row
    PERMISSIONS.each do |vertical_name, controller_data|
      verticals = [vertical_name.strip]
      controller_data.each do |controller_name, actions_data|
        controller = controller_name.strip
        actions_data.each do |action_data|
          begin
            action = action_data[:action]
            permission_name = controller + '-' + action
            flags = JSON.unparse(action_data[:flags] || {})
            role_maps = action_data[:role_maps]
            permission = Permission.where(name: permission_name, controller: controller, action: action, flags: flags).first_or_create
            Role.where(vertical: (Vertical.where(name: verticals).pluck(:id)), role_map: role_maps).each do |role|
              RolePermission.where(role: role, permission_id: permission.id).first_or_create
            end
          rescue Exception => e
            puts e.message
            puts e.backtrace
            Airbrake.notify(e)
            puts "Exception with permission:: controller: #{controller}\t, action: #{action}, flags: #{flags}\n\n"
            next
          end
        end
      end
    end


    # Start processing on sheet starting with second row
    # PERMISSIONS.each_with_index do |permission, i|
    #   begin
    #     controller = permission[:controller].strip
    #     action = permission[:action].strip
    #     permission_name = controller + '-' + action
    #     flags = JSON.unparse(permission[:flags])
    #     verticals = permission[:verticals]
    #     role_maps = permission[:role_maps]
    #   rescue Exception => e
    #     puts e.message
    #     puts e.backtrace
    #     Airbrake.notify(e)
    #     puts "Exception with permission:: #{permission}"
    #     puts "Data: controller: #{controller}\t, action: #{action}, flags: #{flags}\n\n"
    #     next
    #   end
    #   permission = Permission.where(name: permission_name, controller: controller, action: action, flags: flags).first_or_create
    #   Role.where(vertical: (Vertical.where(name: verticals).pluck(:id)), role_map: role_maps).each do |role|
    #     RolePermission.where(role: role, permission_id: permission.id).first_or_create
    #   end
    # end
    puts "Permissions created...\n\n"
  end
end