namespace :seed do
  desc "Populate Domain IDs in roles."
  task populate_domain_id_from_vertical_for_roles: :environment do
    verticals_by_name = Vertical.all.index_by(&:name)

    Role.all.each do |role|
      role.domain = verticals_by_name[role.vertical]
      role.save!
    end
  end
end