namespace :seed do
  desc "Populates Company Industries."
  task populate_company_industries: :environment do
    Company.all.each do |company|
      company_industries = (company.industries || [])
      company_industries = JSON.parse company_industries if company_industries.is_a?(String)
      company_industries.first(3).each do |industry_name|
        industry_obj = Industry.find_by_name(industry_name.titlecase)
        next if industry_obj.nil?
        CompanyIndustry.create(:company_id => company.id, :industry_id => industry_obj.id)
      end
    end
  end
end