namespace :seed do
  desc "Add Verticals Data."
  task populate_verticals: :environment do
    puts "Populating Vertical."
    ["Admin", "Broker", "Carrier", "DataEntry", "Employer"].each_with_index do |vertical_name, index|
      Vertical.find_or_create_by(:id => (index + 1), :name => vertical_name)
    end
    puts "Verticals populated."
  end
end