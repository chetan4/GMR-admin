namespace :seed do
  desc "Populate Benefit Type Rate options."
  task populate_rate_options: :environment do
    puts "populating rate options"
    get_rate_options_hash.each do |benefit_type_string, rate_options_hash|
      benefit_type = BenefitType.find_by(name_map: benefit_type_string)
      if benefit_type.present?
        rate_options_hash.each do |rate_option_hash|
          if rate_option_hash.has_key?(:childrens)
            parent_rate_option = benefit_type.rate_options.find_or_create_by(rate_option_hash.except(:labels,:childrens))

            rate_option_hash[:childrens].each do |child_rate_option_hash|
              child_rate_option = parent_rate_option.childrens.find_or_create_by(child_rate_option_hash.except(:labels).merge(benefit_type_id: benefit_type.id))

              child_rate_option_hash[:labels].each do |label|
                child_rate_option.rate_option_labels.find_or_create_by(label)
              end
            end
          else
            rate_option = benefit_type.rate_options.find_or_create_by(rate_option_hash.except(:labels))
            rate_option_hash[:labels].each do |label|
              rate_option.rate_option_labels.find_or_create_by(label)
            end
          end
        end
      end
    end
  end

  def get_rate_options_hash
    {
        "long_term_disability_(ltd)" => [
            {name: "Per $100 of Benefit per month", labels: [{name: "Rate", presence_type: RateOptionLabel.presence_types[:mandatory]}]},
            {name: "Per $100 of monthly covered payroll", labels: [{name: "Rate", presence_type: RateOptionLabel.presence_types[:mandatory]}]}
        ],

        "medical" => [
            {name: "1 Tier", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "2 Tier", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "3 Tier", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee+1", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "4 Tier", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee Children", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee Spouse", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "5 Tier", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee+child", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee Children", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee Spouse", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]},
            {name: "Retiree", name_map: "retiree",
             childrens: [{name: "2 Tier",labels: [{name: "Employee (Medicare Primary)", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "Family (Both Medicare Primary)", presence_type: RateOptionLabel.presence_types[:mandatory]}]},
                         {name: "3 Tier",labels: [{name: "Employee (Medicare Primary)", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "Employee & spouse (Both Medicare Primary)", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "One Medicare primary & One Medicare  secondary", presence_type: RateOptionLabel.presence_types[:mandatory]}]},
                         {name: "5 Tier",labels: [{name: "Employee (Medicare Primary)", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "Employee & spouse (Both Medicare Primary)", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "One medicare primary & one medicare secondary", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "Family (Both Medicare  Primary)", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "Family (One Medicare  Primary)", presence_type: RateOptionLabel.presence_types[:mandatory]}]}]},

            {name: 'Dependents', labels: [{name: "Employee",  presence_type: RateOptionLabel.presence_types[:mandatory]},
                                          {name: "Employee+1 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                          {name: "Employee+2 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                          {name: "Employee+3 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                          {name: "Employee+4 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                          {name: "Employee+5 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                          {name: "Employee+6 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]}]}
        ],

        "life_insurance" => [
            {name: "Basic Life Rate per 1000", labels: [{name: "Rate", presence_type: RateOptionLabel.presence_types[:mandatory]}]},
            {name: "Basic Rate per Employee", labels: [{name: "Rate", presence_type: RateOptionLabel.presence_types[:mandatory]}]},
            {name: "Basic Rate age based", labels: [{name: "Under 20 years", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "20 - 24", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "25 - 29", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "30 - 34", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "35 - 39", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "40 - 44", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "45 - 49", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "50 - 54", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "55 - 59", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "60 - 64", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "65 - 69", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "70 - 74", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "75 - 79", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "80 - 84", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "85 - 89", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "90 +", presence_type: RateOptionLabel.presence_types[:at_least_one]}]},
            {name: "Retiree", name_map: "retiree",
             childrens: [{name: "2 Tier",labels: [{name: "Employee (Primary)", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "Family (Primary)", presence_type: RateOptionLabel.presence_types[:mandatory]}]},
                         {name: "3 Tier",labels: [{name: "Employee (Primary)", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "Employee & spouse (Primary)", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "One primary & one secondary", presence_type: RateOptionLabel.presence_types[:mandatory]}]},
                         {name: "5 Tier",labels: [{name: "Employee (Primary)", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "Employee & spouse (Primary)", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "One primary & one secondary", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "Family (Both Primary)", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "Family (One Primary)", presence_type: RateOptionLabel.presence_types[:mandatory]}]}]},
        ],

        "dental" => [
            {name: "1 Tier", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "2 Tier", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "3 Tier", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee+1", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "4 Tier", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee Children", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee Spouse", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "5 Tier", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee+Child", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee Children", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee Spouse", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: 'Dependents', labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                          {name: "Employee+1 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                          {name: "Employee+2 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                          {name: "Employee+3 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                          {name: "Employee+4 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                          {name: "Employee+5 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                          {name: "Employee+6 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "Retiree", name_map: "retiree",
             childrens: [{name: "2 Tier",labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]},
                         {name: "3 Tier",labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "Employee+1", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]},
                         {name: "4 Tier",labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "Employee Children", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "Employee Spouse", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]},
                         {name: "5 Tier",labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "Employee+Child", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "Employee Children", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "Employee Spouse", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "Employee Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]},
                         {name: "Dependents",labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                      {name: "Employee+1 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                      {name: "Employee+2 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                      {name: "Employee+3 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                      {name: "Employee+4 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                      {name: "Employee+5 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                      {name: "Employee+6 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]}]},
             ]}
        ],
        "buyout" => [
            { name: "1 Tier", labels: [{ name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory] }] },

            { name: "2 Tier", labels: [{ name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory] },
                                       { name: "Family", presence_type: RateOptionLabel.presence_types[:mandatory] }] },

            { name: "3 Tier", labels: [{ name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory] },
                                       { name: "Employee+1", presence_type: RateOptionLabel.presence_types[:mandatory] },
                                       { name: "Family", presence_type: RateOptionLabel.presence_types[:mandatory] }] },

            { name: "4 Tier", labels: [{ name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory] },
                                       { name: "Employee Children", presence_type: RateOptionLabel.presence_types[:mandatory] },
                                       { name: "Employee Spouse", presence_type: RateOptionLabel.presence_types[:mandatory] },
                                       { name: "Family", presence_type: RateOptionLabel.presence_types[:mandatory] }] },
            ],

        "vision" => [
            {name: "2 Tier", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "3 Tier", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee+1", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "4 Tier", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee Children", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee Spouse", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "5 Tier", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee+child", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee Children", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee Spouse", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: 'Dependents', labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                          {name: "Employee+1 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                          {name: "Employee+2 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                          {name: "Employee+3 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                          {name: "Employee+4 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                          {name: "Employee+5 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                          {name: "Employee+6 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "Retiree", name_map: "retiree",
             childrens: [{name: "2 Tier",labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]},
                         {name: "3 Tier",labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "Employee+1", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]},
                         {name: "4 Tier", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                   {name: "Employee Children", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                   {name: "Employee Spouse", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                   {name: "Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]},
                         {name: "5 Tier", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                   {name: "Employee+child", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                   {name: "Employee Children", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                   {name: "Employee Spouse", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                   {name: "Employee Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]},
                         {name: 'Dependents', labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                       {name: "Employee+1 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                       {name: "Employee+2 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                       {name: "Employee+3 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                       {name: "Employee+4 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                       {name: "Employee+5 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                       {name: "Employee+6 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]}]}
             ]}
        ],

        "critical_illness" => [
            {name: "1 Tier(Employee only)", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "2 Tier(Employee & Family)", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                         {name: "Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "2 Tier(Employee & Employee/Spouse)", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                                  {name: "Employee/Spouse", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "2 Tier(Employee only & Spouse only)", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                                   {name: "Spouse", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "3 Tier", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee+1", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "4 Tier", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee Children", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee Spouse", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "5 Tier", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee+child", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee Children", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee Spouse", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]}
        ],

        "wellness" => [
            {name: "1 Tier", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "2 Tier", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]}
        ],

        "employee_assistance_program_(eap)" => [
            {name: "1 Tier", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]}]}
        ],

        "accidental_insurance" => [
            {name: "2 Tier", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "3 Tier", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee+1", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "4 Tier", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee Children", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee Spouse", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "5 Tier", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee+child", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee Children", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee Spouse", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                      {name: "Employee Family", presence_type: RateOptionLabel.presence_types[:mandatory]}]},
            {name: 'Dependents', labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                          {name: "Employee+1 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                          {name: "Employee+2 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                          {name: "Employee+3 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                          {name: "Employee+4 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                          {name: "Employee+5 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                          {name: "Employee+6 dependent", presence_type: RateOptionLabel.presence_types[:mandatory]}]}
        ],

        "ad&d" => [
            {name: "AD&D Rate per 1000", labels: [{name: "Rate", presence_type: RateOptionLabel.presence_types[:mandatory]}]},
            {name: "AD&D Rate per Employee", labels: [{name: "Rate", presence_type: RateOptionLabel.presence_types[:mandatory]}]},
            {name: "AD&D Rate age based", labels: [{name: "Under 20 years", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "20 - 24", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "25 - 29", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "30 - 34", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "35 - 39", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "40 - 44", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "45 - 49", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "50 - 54", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "55 - 59", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "60 - 64", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "65 - 69", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "70 - 74", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "75 - 79", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "80 - 84", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "85 - 89", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                    {name: "90 +", presence_type: RateOptionLabel.presence_types[:at_least_one]}]},
            {name: "Retiree", name_map: "retiree",
             childrens: [{name: "2 Tier",labels: [{name: "Employee (Primary)", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "Family (Primary)", presence_type: RateOptionLabel.presence_types[:mandatory]}]},
                         {name: "3 Tier",labels: [{name: "Employee (Primary)", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "Employee & spouse (Primary)", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "One primary & one secondary", presence_type: RateOptionLabel.presence_types[:mandatory]}]},
                         {name: "5 Tier",labels: [{name: "Employee (Primary)", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "Employee & spouse (Primary)", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "One primary & one secondary", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "Family (Both Primary)", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                  {name: "Family (One Primary)", presence_type: RateOptionLabel.presence_types[:mandatory]}]}]},
        ],
        "short_term_disability_(std)" =>  [
            {name: "1 Tier", labels: [{name: "Employee", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "Per Employee Per Month (PEPM)", labels: [{name: "Rate", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "Per $10 of Covered Weekly Benefit", labels: [{name: "Rate", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "Per $10 of Covered Weekly Payroll", labels: [{name: "Rate", presence_type: RateOptionLabel.presence_types[:mandatory]}]}
        ],
        "voluntary_short_term_disability_(std)" =>  [
            {name: "Per $10 of Covered Weekly Benefit", labels: [{name: "Rate", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "Per $10 of Covered Weekly Payroll", labels: [{name: "Rate", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "Per Employee Per Month (PEPM)", labels: [{name: "Rate", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "Other Rating Methodology", labels: [{name: "Estimated monthly premium", presence_type: RateOptionLabel.presence_types[:mandatory]}]}
        ],
        "voluntary_long_term_disability_(ltd)" =>  [
            {name: "Per $100 of monthly covered payroll", labels: [{name: "Per $100 of monthly covered payroll", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "Per $100 of Benefit per month", labels: [{name: "Per $100 of Benefit per month", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "Per Employee Per Month (PEPM)", labels: [{name: "Per Employee Per Month (PEPM)", presence_type: RateOptionLabel.presence_types[:mandatory]}]},

            {name: "Other Rating Methodology", labels: [{name: "Estimated monthly premium", presence_type: RateOptionLabel.presence_types[:mandatory]}]}
        ],
        "healthcare_spending_account_(hsa)" => [
            {name: "Default", labels: [{name: "Estimated Annual Employer Funding", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                       {name: "Per Enrolee per month", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                       {name: "Annual Administration Fee"},
                                       {name: "Minimum Monthly Charge"},
                                       {name: "Implementation Fee"}]}
        ],
        "flexible_spending_account" => [
            {name: "Default", labels: [{name: "Estimated Annual Employer Funding", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                       {name: "Per Enrolee per month", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                       {name: "Annual Administration Fee"},
                                       {name: "Minimum Monthly Charge"},
                                       {name: "Implementation Fee"}]}
        ],
        "hra_-_gap_plan" => [
            {name: "Default", labels: [{name: "Estimated Annual Employer Funding", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                       {name: "Per Enrolee per month", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                       {name: "Percentage of Saving"},
                                       {name: "Vendor Saving Estimate"},
                                       {name: "Vendor Fees Estimate"},
                                       {name: "Annual Administration Fee"},
                                       {name: "Minimum Monthly Charge"},
                                       {name: "Implementation Fee"}]}
        ],
        "hra_-_reimbursement" => [
            {name: "Default", labels: [{name: "Estimated Annual Employer Funding", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                       {name: "Per Enrolee per month", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                       {name: "Annual Administration Fee"},
                                       {name: "Minimum Monthly Charge"},
                                       {name: "Implementation Fee"}]}
        ],

        "voluntary_ad&d" => [
            {name: "Basic Life Rate per 1000", labels: [{name: "Rate", presence_type: RateOptionLabel.presence_types[:mandatory]}]},
            {name: "Basic Rate per Employee", labels: [{name: "Rate", presence_type: RateOptionLabel.presence_types[:mandatory]}]},
        ],
        "voluntary_life_insurance" => [
          {name: "5 Year age band (per $1000)", labels: [{name: "Employee Rate", presence_type: RateOptionLabel.presence_types[:at_least_one]},
                                                         {name: "Spouse Rate", presence_type: RateOptionLabel.presence_types[:optional]},
                                                         {name: "Child Rate", presence_type: RateOptionLabel.presence_types[:optional]}]},
          {name: "Flat Rate (per $1000)", labels: [{name: "Employee Rate", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                                   {name: "Spouse Rate", presence_type: RateOptionLabel.presence_types[:optional]},
                                                   {name: "Child Rate", presence_type: RateOptionLabel.presence_types[:optional]}]},
          {name: "Per Employee Rate", labels: [{name: "Employee Rate", presence_type: RateOptionLabel.presence_types[:mandatory]},
                                               {name: "Spouse Rate", presence_type: RateOptionLabel.presence_types[:optional]},
                                               {name: "Child Rate", presence_type: RateOptionLabel.presence_types[:optional]}]}
        ]
    }
  end
end