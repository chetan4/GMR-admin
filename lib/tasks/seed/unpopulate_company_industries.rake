namespace :seed do
  desc "Rollback Population of Company Industries."
  task unpopulate_company_industries: :environment do
    CompanyIndustry.all.group_by(&:company_id).each do |company_id, company_industry_objs|
      industry_ids = company_industry_objs.collect(&:industry_id)
      company = Company.find(company_id)
      company.industries = Industry.find(industry_ids).collect(&:name).to_json
      company.save!
    end
  end
end