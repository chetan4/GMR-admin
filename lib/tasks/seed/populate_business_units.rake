namespace :seed do
  desc "Populates Business Units."
  task populate_business_units: :environment do
    # TODO:Harsh. get more business units.
    ["Financial"].each do |business_unit|
      BusinessUnit.create(:name => business_unit)
    end
  end
end