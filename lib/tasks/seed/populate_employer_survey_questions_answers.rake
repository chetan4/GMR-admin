namespace :seed do
  desc "Populate Employer Survey questions and answers."
  task populate_employer_survey_questions_answers: :environment do
    puts "Create Employer Survey"
    survey = EmployerSurvey.find_or_create_by(:name => "Initial Survey")
    survey.update_attributes(description: "Recommendations are derived from your response to the questionnaire, your plan benchmark, an analysis of your Demographic Data.")

    puts "populating questions and answers"
      EmployerSurveyQuestion.destroy_all
      EmployerSurveyAnswer.destroy_all
        ActiveRecord::Base.connection.reset_pk_sequence!('employer_survey_questions')
        ActiveRecord::Base.connection.reset_pk_sequence!('employer_survey_answers')
      get_survey_questions_answers.each do |question_answers|
        

        question = EmployerSurveyQuestion.find_or_create_by({:question => question_answers[:question],
                                          :question_type => question_answers[:multiple_answers],
                                          :display_order => question_answers[:display_order],
                                          :employer_survey_id => survey.id})

        question_answers[:answers].each do |answer|
          EmployerSurveyAnswer.find_or_create_by({:answer_text => answer[:text], :employer_survey_question_id => question.id})
        end

      end
  end

  def get_survey_questions_answers
    [
      {:question => 'Which statement best describes your strategy for providing benefits to your employees?',
       :multiple_answers => false,
       :display_order => 1,
       :answers => [{:text => 'I want to provide a superior level of benefits when compared to other employers in my industry'},
                    {:text => 'I want to provide an average level of benefits when compared to other employers in my industry'},
                    {:text => 'I want to provide a minimum level of benefits when compared to other employers in my industry'}]
      },
      {:question => 'What do you want your benefit plan to accomplish for your employees?',
       :multiple_answers => false,
       :display_order => 2,
       :answers => [{:text => 'Provide our employees with basic protection from catastrophic financial risk (such as serious ongoing medical conditions, disability or death)'},
                    {:text => 'Provide basic catastrophic  protection and allow employees to purchase additional protection if they need it'},
                    {:text => 'Provide comprehensive protection'}]
      },
      {:question => 'Which statement best describes your philosophy in regards to cost sharing with employees?',
       :multiple_answers => false,
       :display_order => 3,
       :answers => [{:text => 'I believe all employees should pay about the same regardless of plan use and would offset costs with higher contributions for all'},
                    {:text => 'People who use the plan more should absorb more of the cost through copays or deductibles than those who don’t use the plan as much, so we can keep contributions lower'}]
      },
      {:question => ' Which statement do you think best describes the financial condition of your lowest paid employees?',
       :multiple_answers => false,
       :display_order => 4,
       :answers => [{:text => 'I think they live paycheck to paycheck'},
                    {:text => 'They have less than 6 months of savings'},
                    {:text => 'I think they have more than 6 months of savings'}]
      },
      {:question => 'What employee contribution methods would you consider (check all that apply)?',
       :multiple_answers => true,
       :display_order => 5,
       :answers => [{:text => 'Employees pay a percentage of cost based on which plan they choose'},
                    {:text => 'Employer pays a fixed amount to lock in their cost regardless of which plan employees choose. (Core-Buy up strategy)'},
                    {:text => 'Salary tiered contributions where higher salaried employees pay more for benefits'},
                    {:text => 'Employees that participate in a wellness program pay a lower contribution'},
                    {:text => 'Employees with spouses that have access to medical coverage through their own employers pay more to cover spouses'},
                    {:text => 'Families with more dependents pay more than families with fewer dependents'}]
      },
      {:question => 'Please rank these 5 most important goals of your benefit program in order of importance',
       :multiple_answers => true,
       :display_order => 6,
       :answers => [{:text => 'Control Employee Benefit costs'},
                    {:text => 'Attract employees'},
                    {:text => 'Retain employees'},
                    {:text => 'Effectively communicate benefits information to employees'},
                    {:text => 'Improve the health and fitness of employees'},
                    {:text => 'Educate employees about healthcare'},
                    {:text => 'Proactively address the diverse needs of employees'},
                    {:text => 'Provide employees with financial security'},
                    {:text => 'Increase employee productivity'},
                    {:text => 'Reduce HR administrative workload'},
                    {:text => 'Reduce employee absenteeism'},
                    {:text => 'Comply with healthcare reform'},
                    {:text => 'Reward key employees'}]
      },
      {:question => 'How important is it to provide a reasonable out of network benefit in at least one of your medical plan options?',
       :multiple_answers => false,
       :display_order => 7,
       :answers => [{:text => 'Very important'},
                    {:text => 'Somewhat Important'},
                    {:text => 'Not Important'}]
      },
      {:question => 'With respect to controlling costs, what are your priorities?',
       :multiple_answers => false,
       :display_order => 8,
       :answers => [{:text => 'Controlling renewal costs are most important but long term costs are important too'},
                    {:text => 'Managing long term costs are most important but renewal costs are important too'},
                    {:text => 'Controlling renewal and long term costs are equally important'},
                    {:text => 'Renewal costs only'},
                    {:text => 'Long term costs only'},
                    {:text => 'Costs are not a priority at this time'}]
      },
      {:question => 'If you are willing to take some risk, you can potentially save money. In many instances, the amount of risk you take correlates to the potential savings. How much risk would you consider taking, if any?',
       :multiple_answers => false,
       :display_order => 9,
       :answers => [{:text => 'I do not want to take any risk at this time'},
                    {:text => '2-4% of premium'},
                    {:text => '5-10% of premium'},
                    {:text => '10-15% of premium'},
                    {:text => '20-25% of premium'}]
      },
      {:question => 'Which of the following methods do you use to communicate your benefit program? (check all that apply)',
       :multiple_answers => true,
       :display_order => 10,
       :answers => [{:text => 'Group meetings'},
                    {:text => 'One on one empoyee meetings'},
                    {:text => 'Benefit Fairs'},
                    {:text => 'Mailing to home'},
                    {:text => 'Email'},
                    {:text => 'Online communication and enrollment'},
                    {:text => 'Individual total compensation statements'},
                    {:text => 'Social networking site'}]
      },
      {:question => 'Please check all that apply with respect to your employee communication portal/website.',
        :multiple_answers => true,
        :display_order => 11,
        :answers => [{:text => 'We are paper based and do not have one'},
                     {:text => 'Employees can review what benefits they are eligible for'},
                     {:text => 'Employees can enroll in coverage at open enrollment'},
                     {:text => 'It passes enrollment information directly to the carrier/vendor'},
                     {:text => 'The system can process changes for current employees and enroll new employees hired throughout the entire year'},
                     {:text => 'My current process meets my needs'}]
      },
      {:question => 'With respect to your company’s philosophy on wellness, check all that apply.',
       :multiple_answers => true,
       :display_order => 12,
       :answers => [{:text => 'We are not sure if a wellness program would benefit our organization'},
                    {:text => 'We think wellness would be of minimal value to our organization'},
                    {:text => 'We believe wellness should be an important part of our program'},
                    {:text => 'Our senior leadership would support promoting health and wellbeing in our organization'},
                    {:text => 'Our senior leadership would likely take a hands off approach to promoting and supporting a wellness program'}]
      },
      {:question => 'What wellness strategies are you currently employing?',
       :multiple_answers => true,
       :display_order => 13,
       :answers => [{:text => 'None'},
                    {:text => 'Biometric Screening'},
                    {:text => 'Health Risk Assessment'},
                    {:text => 'Contribution incentives'},
                    {:text => 'Smoking cessation'},
                    {:text => 'Nutrition programs'},
                    {:text => 'Weight loss programs'},
                    {:text => 'Disease management programs'},
                    {:text => 'Health coaching'},
                    {:text => 'Onsite medical clinic'}]
      }
    ]
  end
end
