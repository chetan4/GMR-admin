namespace :seed do
  desc 'seed:populate_stoploss_rate_options RAILS_ENV=<environment_name> --trace'
  task populate_stoploss_rate_options: :environment do
    puts '*' * 100
    puts '-------------START CREATING RATE ATTRIBUTES-------------------'
    attributes = read_rate_attributes

    attributes.each do |benefit_type_name_map, rate_attributes|
      benefit_type = BenefitType.find_by(name_map: benefit_type_name_map)
      rate_attributes.each do |attribute|
        rate_arrangement = RateArrangement.find_or_create_by(name_map: attribute[:name_map], benefit_type_id: benefit_type.id)
        rate_arrangement.name = attribute[:name]
        rate_arrangement.metadata = attribute[:fields]
        rate_arrangement.save!
        puts "\n---------RATE ARRANGEMENT '#{rate_arrangement.name}' IS CREATED FOR BENEFIT TYPE '#{benefit_type.name}'...!!! \n\n"
        puts "\n\n\n**************START CREATING RATE OPTIONS FOR RATE ARRANGEMENT '#{rate_arrangement.name}' **************n\n\n\n\n"
        attribute[:rate_options][:options].each do |rate_option_hash|
          rate_option = benefit_type.rate_options.find_or_create_by(name_map: rate_option_hash[:name_map], rate_arrangement_id: rate_arrangement.id)
          rate_option.name = rate_option_hash[:name]
          rate_option.save!
          puts "\n---------RATE OPTION '#{rate_option.name}' IS CREATED FOR BENEFIT TYPE '#{benefit_type.name}'...!!! \n\n"
          rate_option_hash[:labels].each do |label|
            rate_option_label = rate_option.rate_option_labels.find_or_create_by(name_map: label[:name_map])
            rate_option_label.name = label[:name]
            rate_option_label.presence_type = label[:presence_type]
            rate_option_label.save!
            puts "\n---------------------------RATE OPTION LABEL '#{rate_option_label.name}' IS CREATED FOR RATE OPTION '#{rate_option.name}'...!!! \n\n"
          end
        end
        puts "\n\n**************ENDED CREATING RATE OPTIONS FOR RATE ARRANGEMENT '#{rate_arrangement.name}' **************\n\n\n\n"
      end
    end
    puts '-------------END CREATING RATE ATTRIBUTES-------------'
    puts '*' * 100
  end

  def read_rate_attributes
    {
      'stop_loss' => [
        {
          name: 'Individual Stop Loss Rates',
          name_map: 'individual_stop_loss',
          fields: {
            optional_fields:
            [{
              name_map: 'deductible',
              name: 'Deductible',
              mandatory: false
            }, {
              name_map: 'incurred',
              name: 'Incurred',
              mandatory: false
            }, {
              name_map: 'paid',
              name: 'Paid',
              mandatory: false
            }]
          },
          rate_options: fetch_rate_options
        },
        {
          name: 'Aggregate Stop Loss Rates',
          name_map: 'aggregate_stop_loss',
          fields: {
            optional_fields:
            [{
              name_map: 'annual_premium',
              name: 'Annual Premium',
              mandatory: true
            }, {
              name_map: 'attachment_point',
              name: 'Attachment point',
              mandatory: false
            }, {
              name_map: 'incurred',
              name: 'Incurred',
              mandatory: false
            }, {
              name_map: 'paid',
              name: 'Paid',
              mandatory: false
            }]
          },
          rate_options: fetch_rate_options
        },
        {
          name: 'Additional Run Out Rates',
          name_map: 'additional_run_out',
          fields: {
            optional_fields:
            [{
              name_map: 'inception_premium',
              name: 'Inception Premium',
              mandatory: true
            }, {
              name_map: 'annual_premium',
              name: 'Annual Premium',
              mandatory: true
            }, {
              name_map: 'no_of_months',
              name: 'Number of months',
              mandatory: false
            }, {
              name_map: 'lump_sum',
              name: 'Lump Sum',
              mandatory: false
            }]
          },
          rate_options: fetch_rate_options
        }
      ]
    }
  end

  def fetch_rate_options
    rate_option_label_mandatory = RateOptionLabel.presence_types[:mandatory]
    {
      options: [
        {
          name: '1 Tier',
          name_map: '1-tier',
          labels: [
            {
              name: 'Employee',
              name_map: 'employee',
              presence_type: rate_option_label_mandatory
            }
          ]
        }, {
          name: '2 Tier',
          name_map: '2-tier',
          labels: [
            {
              name: 'Employee',
              name_map: 'employee',
              presence_type: rate_option_label_mandatory
            }, {
              name: 'Family',
              name_map: 'family',
              presence_type: rate_option_label_mandatory
            }
          ]
        }, {
          name: '3 Tier',
          name_map: '3-tier',
          labels: [
            {
              name: 'Employee',
              name_map: 'employee',
              presence_type: rate_option_label_mandatory
            }, {
              name: 'Employee+1',
              name_map: 'employee-1',
              presence_type: rate_option_label_mandatory
            }, {
              name: 'Family',
              name_map: 'family',
              presence_type: rate_option_label_mandatory
            }
          ]
        }, {
          name: '4 Tier',
          name_map: '4-tier',
          labels: [
            {
              name: 'Employee',
              name_map: 'employee',
              presence_type: rate_option_label_mandatory
            }, {
              name: 'Employee Children',
              name_map: 'employee-children',
              presence_type: rate_option_label_mandatory
            }, {
              name: 'Employee Spouse',
              name_map: 'employee-spouse',
              presence_type: rate_option_label_mandatory
            }, {
              name: 'Family',
              name_map: 'family',
              presence_type: rate_option_label_mandatory
            }
          ]
        }
      ]
    }
  end
end
