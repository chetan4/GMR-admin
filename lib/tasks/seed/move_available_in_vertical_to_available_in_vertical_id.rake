namespace :seed do
  desc "Move available_in_vertical to available_in_vertical_id."
  task move_available_in_vertical_to_available_in_vertical_id: :environment do
    Role.all.each do |role|
      vertical_name = role.available_in_vertical
      vertical = Vertical.where(:name => vertical_name).first || Vertical.find_by_name("Admin")
      role.available_in_vertical_id = vertical.id
      role.save!
    end
  end
end