namespace :seed do
  desc "Loads database from custom seed file."
  task load_custom: :environment do
    Dir.glob("db/seed_dump/*.rb").sort.each do |file|
      file_path = File.expand_path("../../../#{file}", File.dirname(__FILE__))
      puts "Requiring file #{file_path}"
      require file_path
      puts "File required #{file_path}"
    end
  end
end