namespace :seed do
  desc "Populate Verticals in roles."
  task populate_verticals_from_domain_id_for_roles: :environment do
    verticals_by_id = Vertical.all.index_by(&:id)

    Role.all.each do |role|
      role.vertical = verticals_by_id[role.domain_id].present? ? verticals_by_id[role.domain_id].name : "Admin"
      role.save!
    end
  end
end