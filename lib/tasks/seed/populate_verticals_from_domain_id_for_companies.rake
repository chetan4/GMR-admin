desc "Populate Verticals in companies."
task populate_verticals_from_domain_id_for_companies: :environment do
  verticals_by_id = Vertical.all.index_by(&:id)

  Company.all.each do |company|
    company.vertical = verticals_by_id[company.domain_id].present? ? verticals_by_id[company.domain_id].name : "Admin"
    company.save!
  end
end