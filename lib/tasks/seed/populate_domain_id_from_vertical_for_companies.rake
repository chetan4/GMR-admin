namespace :seed do
  desc "Populate Domain IDs in companies."
  task populate_domain_id_from_vertical_for_companies: :environment do
    verticals_by_name = Vertical.all.index_by(&:name)

    Company.all.each do |company|
      company.domain = verticals_by_name[company.vertical]
      company.save!
    end
  end
end