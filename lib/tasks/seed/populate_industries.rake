namespace :seed do
  desc "Populates Industries."
  task populate_industries: :environment do
    ActiveRecord::Base.connection.execute('TRUNCATE TABLE industries RESTART IDENTITY CASCADE')

    ["Agriculture", "Grocery", "Accounting", "Health Care", "Advertising", "Internet Publishing",
     "Aerospace", "Investment Banking", "Aircraft", "Legal", "Airline", "Manufacturing",
     "Apparel &amp; Accessories", "Motion Picture &amp; Video", "Automotive", "Music", "Banking",
     "Newspaper Publishers", "Broadcasting", "Online Auctions", "Brokerage", "Pension Funds",
     "Biotechnology", "Pharmaceuticals", "Call Centers", "Private Equity", "Cargo Handling",
     "Publishing", "Chemical", "Real Estate", "Computer", "Retail &amp; Wholesale", "Consulting",
     "Securities &amp; Commodity Exchanges", "Consumer Products", "Service", "Cosmetics",
     "Soap &amp; Detergent", "Defense", "Software", "Department Stores", "Sports", "Education",
     "Technology", "Electronics", "Telecommunications", "Energy", "Television",
     "Entertainment &amp; Leisure", "Transportation", "Executive Search", "Trucking", "Financial Services",
     "Venture Capital", "Food, Beverage &amp; Tobacco", "Others"].each do |industry_name|
      Industry.find_or_create_by(:name => industry_name)
    end
  end
end