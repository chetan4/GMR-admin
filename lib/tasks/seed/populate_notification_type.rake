namespace :seed do
  desc "rake seed:populate_notification_type RAILS_ENV=<environment_name> --trace"
  task populate_notification_type: [:populate_verticals] do
    ["Admin", "Broker", "Carrier", "DataEntry", "Employer"].each_with_index do |vertical_name, index|
      Vertical.find_or_create_by(:id => (index + 1), :name => vertical_name)
    end
    populate_notification_type(notification_type_data)
  end

  def populate_notification_type(notification_data)
    notification_data.each do |record|
      puts "Processing NotificationType record:
                       activity_type: #{record[:activity_type]}
                               level: #{record[:level]}
                  sender_vertical_id: #{record[:sender_vertical_id]}
                receiver_vertical_id: #{record[:receiver_vertical_id]}\n\n"

      NotificationType.where(activity_type: record[:activity_type],
                             level: record[:level],
                             sender_vertical_id: record[:sender_vertical_id],
                             receiver_vertical_id: record[:receiver_vertical_id]).first_or_create
    end
  end

# NOTE: If notification type data is changed, please ensure corresponding
# notifier valid_arguments is also changed accordingly
  def notification_type_data
    vertical_by_name = Vertical.all.index_by(&:name)
    [
        {
            activity_type: 'add_task',
            level: NotificationType.levels[:high],
            sender_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:carrier]].id,
            receiver_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:data_entry]].id
        },
        {
            activity_type: 'add_task_for_data_entry',
            level: NotificationType.levels[:high],
            sender_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:data_entry]].id,
            receiver_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:data_entry]].id
        },
        {
            activity_type: 'assign_task',
            level: NotificationType.levels[:high],
            sender_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:data_entry]].id,
            receiver_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:data_entry]].id
        },
        {
            activity_type: 'complete_task',
            level: NotificationType.levels[:high],
            sender_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:data_entry]].id,
            receiver_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:data_entry]].id
        },
        {
            activity_type: 'plan_document_requested',
            level: NotificationType.levels[:high],
            sender_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:employer]].id,
            receiver_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:carrier]].id
        },
        {
            activity_type: 'plan_document_requested_from_data_entry',
            level: NotificationType.levels[:high],
            sender_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:data_entry]].id,
            receiver_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:data_entry]].id
        },
        {
            activity_type: 'request_information',
            level: NotificationType.levels[:high],
            sender_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:data_entry]].id,
            receiver_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:carrier]].id
        },
        {
            activity_type: 'update_sub_task',
            level: NotificationType.levels[:high],
            sender_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:carrier]].id,
            receiver_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:data_entry]].id
        },
        {
            activity_type: 'sent_task_for_rework',
            level: NotificationType.levels[:high],
            sender_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:data_entry]].id,
            receiver_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:data_entry]].id
        },
        {
            activity_type: 'reworked_task_for_review',
            level: NotificationType.levels[:high],
            sender_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:data_entry]].id,
            receiver_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:data_entry]].id
        },
        {
            activity_type: 'employee_structure_request',
            level: NotificationType.levels[:high],
            sender_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:data_entry]].id,
            receiver_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:data_entry]].id
        },
        {
            activity_type: 'quote_request',
            level: NotificationType.levels[:high],
            sender_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:employer]].id,
            receiver_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:carrier]].id
        },
        {
            activity_type: 'confirmed_plan_request',
            level: NotificationType.levels[:high],
            sender_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:employer]].id,
            receiver_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:carrier]].id
        },
        {
            activity_type: 'confirmed_renewal_plan_request',
            level: NotificationType.levels[:high],
            sender_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:employer]].id,
            receiver_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:carrier]].id
        },
        {
            activity_type: 'request_renewal_date',
            level: NotificationType.levels[:high],
            sender_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:employer]].id,
            receiver_vertical_id: vertical_by_name[Vertical::AVAILABLE_VERTICALS_HASH[:carrier]].id
        }
    ]
  end
end