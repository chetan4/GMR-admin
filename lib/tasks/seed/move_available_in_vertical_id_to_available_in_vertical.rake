namespace :seed do
  desc "Move available_in_vertical_id to available_in_vertical."
  task move_available_in_vertical_id_to_available_in_vertical: :environment do
    Role.all.each do |role|
      vertical_id = role.available_in_vertical_id
      vertical = Vertical.where(:id => vertical_id).first || Vertical.find_by_name("Admin")
      role.available_in_vertical = vertical.name
      role.save!
    end
  end
end