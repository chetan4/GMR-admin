namespace :seed do
  desc "Dumps database to custom seed file."
  task build_custom: :environment do
    puts "This will overwrite the custom data on your local. Do not commit that if not intended for everyone on the project. Understand? (Y/N)?"
    get_input
  end

  def get_input
    STDOUT.flush
    input = STDIN.gets.chomp
    case input.upcase
      when "Y"
        puts "building..."
        Rake::Task['seed:continue'].invoke
        puts "built."
      when "N"
        puts "aborting"
      else
        puts "Please enter Y or N"
        get_input
    end
  end

  task continue: :environment do
    Dir.glob("db/seed_dump/*.rb").each do |file|
      file_path = File.expand_path("../../../#{file}", File.dirname(__FILE__))
      File.delete(file_path)
    end
    [User, UserSecurityQuestion, Company, CompanyIndustry, EmployeeAssociation, UserRole].each_with_index do |model, index|
      file = "db/seed_dump/#{index + 1}_#{model.to_s.underscore.pluralize}.rb"

      dump = SeedDump.dump(model, exclude: [])
      next if dump.nil?

      ai_val = model.last.id + 1 rescue next
      open(file, 'a') { |f|
        f.puts "#{model}.destroy_all"
        f.puts "#{model}._validate_callbacks.clear"
        f.puts "#{model}.before_save.clear"
        f.puts dump
        f.puts 'ActiveRecord::Base.connection.execute("ALTER SEQUENCE ' + model.table_name + '_id_seq RESTART WITH ' + ai_val.to_s + '")'

      }
    end
  end
end