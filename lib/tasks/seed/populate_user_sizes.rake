namespace :seed do
  desc "Add User Sizes Data."
  task populate_user_sizes: :environment do
    # Truncate if existing user sizes
    ActiveRecord::Base.connection.execute('TRUNCATE TABLE user_handling_sizes RESTART IDENTITY CASCADE')
    ["1 - 10", "11 - 50", "51 - 100", "101 - 500", "501 - 1000", "1000+"].each do |size_name|
      UserHandlingSize.create!(:size => size_name)
    end
  end
end