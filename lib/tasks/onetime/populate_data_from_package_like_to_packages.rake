namespace :onetime do
  #Rake task to pupulate package likes
  desc "rake onetime:populate_data_from_package_likes RAILS_ENV=<environment_name> --trace"
  task populate_data_from_package_likes: :environment do

    # copy data from table package_likes to packages
    PackageLike.all.includes(:likable).each do |package_like|
      package_option_likes = package_like.package_option_likes || {}
      package = package_like.likable
      package_option_ids = package_option_likes.inject([]){|ids,k| ids << k[0] if k[1].eql?('true'); ids}
      if package_like.likable.is_a?(Package)
        package_options = PackageOption.where(id: package_option_ids)
        package.renewal_liked = package_like.renewal_like
      else
        package_options = QuotePackageOption.where(id: package_option_ids)
      end
      package_options.update_all(is_liked: true)

      package.current_plan_liked = package_like.current_plan_like
      package.save
    end
    p 'package likes updated successfully...!!!'

  end
end
