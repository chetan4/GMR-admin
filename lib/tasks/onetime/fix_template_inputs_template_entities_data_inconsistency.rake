namespace :onetime do
  desc "bundle exec rake onetime:fix_template_inputs_template_entities_data_inconsistency"
  task :fix_template_inputs_template_entities_data_inconsistency => :environment do
    TemplateInput.includes([:template_field,:template_column]).find_in_batches do |group|
      group.each do |template_input|
        if (template_input.template_section_id != template_input.template_field.template_section_id)
          puts "TO DELETE TEMPLATE INPUT ID : #{template_input.id}"
          template_input.delete
        end
        if (template_input.template_section_id != template_input.template_column.template_section_id)
          puts "TO DELETE TEMPLATE INPUT ID : #{template_input.id}"
          template_input.delete
        end
      end
    end
  end
end
