namespace :onetime do
  # Rake task to pupulate package likes
  desc 'rake onetime:update_rate_option_label_metadata_of_self_funded RAILS_ENV=<environment_name> --trace'
  task update_rate_option_label_metadata_of_self_funded: :environment do
    def perform_updation(prols, name)
      prols.each do |prol|
        next unless prol.metadata.present?
        value = prol.metadata['value']
        next unless value['other_admin'].present?
        sum = value['administration_fee'].to_f + value['advisor_fee'].to_f
        value['sum'] = sum
        prol.save!
        puts"====== SUM OF #{name}RateOptionLabel ID = #{prol.id} IS UPDATED ======\n"
      end
    end

    def get_self_funded_hash(name)
      eval"{
        self_funded_plan: #{name}RateService::FUNDING_ARRANGEMENT['self_funded'],
        self_funded_renewal: #{name}RenewalRateService::FUNDING_ARRANGEMENT['self_funded'],
        self_funded_quote: #{name}QuoteRateService::FUNDING_ARRANGEMENT['self_funded']
      }"
    end

    benefit_types = BenefitType.where(name_map: ['medical', 'dental', 'short_term_disability_(std)'])
    benefit_types.each do |benefit_type|
      name_map = benefit_type.name_map.upcase
      self_funded = case name_map.downcase
                    when 'medical'
                      get_self_funded_hash('Medical')
                    when 'dental'
                      get_self_funded_hash('Dental')
                    when 'short_term_disability_(std)'
                      get_self_funded_hash('Std')
                    end

      puts "\n\n============================================================================================"
      puts "===================== UPDATION STARTED for #{name_map}  ==================================\n"

      plans = benefit_type.plans.includes(plan_rate_options: :plan_rate_option_labels,
                                          package_plan_renewals: { renewal_plan_rate_options: :renewal_plan_rate_option_labels },
                                          quotes: { quote_plan_rate_options: :quote_plan_rate_option_labels })

      plan_rate_options = []
      plans.map { |plan| plan_rate_options << plan.plan_rate_options }
      plan_rate_options.flatten!

      puts"\n\n"
      plan_rate_options.each do |pro|
        if pro.metadata.present? && pro.metadata['funding_arrangement'] == self_funded[:self_funded_plan].to_s
          perform_updation(pro.plan_rate_option_labels, 'Plan')
        end
      end
      puts"\n"

      package_plan_renewals = []
      plans.map { |plan| package_plan_renewals << plan.package_plan_renewals }
      package_plan_renewals.flatten!

      renewal_plan_rate_options = []
      package_plan_renewals.map { |ppr| renewal_plan_rate_options << ppr.renewal_plan_rate_options }
      renewal_plan_rate_options.flatten!

      renewal_plan_rate_options.each do |pro|
        if pro.metadata.present? && pro.metadata['funding_arrangement'] == self_funded[:self_funded_renewal].to_s
          perform_updation(pro.renewal_plan_rate_option_labels, 'Renewal')
        end
      end
      puts"\n"

      quotes = []
      plans.map { |plan| quotes << plan.quotes }
      quotes.flatten!

      quote_plan_rate_options = []
      quotes.map { |quote| quote_plan_rate_options << quote.quote_plan_rate_options }
      quote_plan_rate_options.flatten!

      quote_plan_rate_options.each do |pro|
        if pro.metadata.present? && pro.metadata['funding_arrangement'] == self_funded[:self_funded_quote].to_s
          perform_updation(pro.quote_plan_rate_option_labels, 'Quote')
        end
      end

      puts "\n===================== UPDATION ENDED for #{name_map}  ==================================\n"
      puts "================================================================================================\n\n\n"
    end
  end
end
