namespace :onetime do
	desc "onetime:assign_priority_to_benefit_types"
	task :assign_priority_to_benefit_types => :environment do
		benefit_type_priorities = {
			"medical" => 1, "dental" => 2, "vision" => 3, "life_insurance" => 4,
			"ad&d" => 5,"long_term_disability_(ltd)" => 6, "short_term_disability_(std)" => 7
		}
		BenefitType.where(name_map: benefit_type_priorities.keys).each  do |benefit_type| 
			benefit_type.priority = benefit_type_priorities[benefit_type.name_map]
			benefit_type.save!
		end
	end
end
