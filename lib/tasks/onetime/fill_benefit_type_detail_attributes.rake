namespace :onetime do
  #Rake task to pupulate package likes
  desc "rake onetime:fill_benefit_type_detail_attributes RAILS_ENV=<environment_name> --trace"
  task fill_benefit_type_detail_attributes: :environment do
    attribute_statuses = BenefitTypeDetailAttribute.statuses
    active = BenefitTypeDetailAttribute.statuses['active']
    inactive = BenefitTypeDetailAttribute.statuses['inactive']
    benefit_type_detail_attributes_map = {
      'medical' => {
        'administration' => [
          {
            name_map: 'network',
            display_name: 'Network',
            status: active
          },
          {
            name_map: 'reporting',
            display_name: 'Reporting',
            status: active
          },
          {
            name_map: 'additional_reporting',
            display_name: 'Additional reporting',
            status: active
          },
          {
            name_map: 'utilisation_review',
            display_name: 'Utilisation Review',
            status: active
          },
          {
            name_map: 'case_management',
            display_name: 'Case Management',
            status: active
          },
          {
            name_map: 'disease_management',
            display_name: 'Disease management',
            status: active
          },
          {
            name_map: 'fiduciary_services',
            display_name: 'Fiduciary services',
            status: active
          },
          {
            name_map: 'spc',
            display_name: 'SPC',
            status: active
          },
          {
            name_map: 'participant_certification',
            display_name: 'Participant certification',
            status: active
          },
          {
            name_map: 'claim_services',
            display_name: 'Claim Services',
            status: active
          },
          {
            name_map: 'other_clinical_services',
            display_name: 'Other Clinical Services',
            status: active
          }
        ]
      },
      'dental' => {
        'administration' => [
          {
            name_map: 'network',
            display_name: 'Network',
            status: active
          },
          {
            name_map: 'reporting',
            display_name: 'Reporting',
            status: active
          },
          {
            name_map: 'additional_reporting',
            display_name: 'Additional reporting',
            status: active
          },
          {
            name_map: 'fiduciary_services',
            display_name: 'Fiduciary services',
            status: active
          }
        ]
      },
      'vision' => {
        'administration' => [
          {
            name_map: 'network',
            display_name: 'Network',
            status: active
          },
          {
            name_map: 'reporting',
            display_name: 'Reporting',
            status: active
          },
          {
            name_map: 'additional_reporting',
            display_name: 'Additional reporting',
            status: active
          },
          {
            name_map: 'fiduciary_services',
            display_name: 'Fiduciary services',
            status: active
          }
        ]
      },
      "short_term_disability_(std)" => {
        'administration' => [
          {
            name_map: 'additional_reporting',
            display_name: 'Additional reporting',
            status: active
          },
          {
            name_map: 'case_management',
            display_name: 'Case Management',
            status: active
          },
          {
            name_map: 'fiduciary_services',
            display_name: 'Fiduciary services',
            status: active
          },
          {
            name_map: 'fica',
            display_name: 'FICA',
            status: active
          },
          {
            name_map: 'w_2',
            display_name: 'W-2',
            status: active
          },
          {
            name_map: 'intake',
            display_name: 'Intake',
            status: active
          }
        ]
      }
    }


    benefit_type_detail_attributes_map.each do |benefit_type_name_map, benefit_type_detail_attributes|
      benefit_type = BenefitType.where(name_map: benefit_type_name_map).includes(:benefit_type_details).first
      puts "\n\n======STARTING BENEFIT TYPE :: #{benefit_type.name}=========\n\n"
      benefit_type_detail_attributes.each do |benefit_type_detail_name_map, attributes|
        benefit_type_detail = benefit_type.benefit_type_details.where(field: benefit_type_detail_name_map).first
        attributes.each do |attribute_params|
          puts "\n\n======STARTING BENEFIT TYPE DETAIL ATTRIBUTE #{benefit_type_detail.field}=========\n"
          btd_attribute = BenefitTypeDetailAttribute.where(benefit_type_detail_id: benefit_type_detail.id, name_map: attribute_params[:name_map]).first_or_create
          btd_attribute.attributes = attribute_params
          btd_attribute.save!
          puts "\n======PROCESSED ATTRIBUTE #{attribute_params.inspect}=========\n"
          puts "\n======ENDING BENEFIT TYPE DETAIL ATTRIBUTE #{benefit_type_detail.field}=========\n"
        end
      end
      puts "\n\n\n======ENDING BENEFIT TYPE ::#{benefit_type.name}=========\n\n\n"
    end

  end
end
