task :employee_mark_terminated => :environment do
  employees = CensusEmployee.where('date_of_termination <= ?', Date.today).active # must exclude already terminated employees.
  employees_count = employees.count
  puts"_______Total_emplooyee______#{employees.count}"

  employees.update_all(status: CensusEmployee::statuses['terminated'])
  puts"_______Updated employees___________________"

  ## flushing cache based on the termination data
  if employees_count > 0
    puts "________________________________________________"
    puts "Complete cache Flushed, from Employee termination cron."
    Util::CacheUtils.flush_all
    puts "__________Flush Completed________________"
  end
  ##
end