class Util::CacheUtils

  # purpose of this call is to centralize the cache get and set methods.
  # get and set methods are actually our custom wrappers to cache operations
  # such as Handling server timeout and cache server failure.
  def self.get(cache_key)
    begin
      return GMR_CACHE.get(cache_key)
    rescue Exception => ex
      Rails.logger.error("*" * 80)
      Rails.logger.error("Cache server error #{ex.message}")
      Rails.logger.error("*" * 80)
      return nil
    end
  end

  def self.set(cache_key, data_to_cached, expiry_time = 1.month.to_i)
    begin
      GMR_CACHE.set(cache_key, data_to_cached, expiry_time)
    rescue Exception => ex
      Rails.logger.error("*" * 80)
      Rails.logger.error("Cache server error #{ex.message}")
      Rails.logger.error("*" * 80)
      return false
    end
    return true
  end

  def self.flush(cache_key)
    begin
      GMR_CACHE.delete(cache_key)
    rescue Exception => ex
      Rails.logger.error("*" * 80)
      Rails.logger.error("Cache server error #{ex.message}")
      Rails.logger.error("*" * 80)
      return false
    end
    return true
  end

  def self.flush_all
    begin
      GMR_CACHE.flush_all
    rescue Exception => ex
      Rails.logger.error("*" * 80)
      Rails.logger.error("Cache server error #{ex.message}")
      Rails.logger.error("*" * 80)
      return false
    end
    return true
  end

end