class Util::ImageUtils
  def self.resize_image(image, width, height)
    begin
      img = Magick::Image.read(image).first
    rescue Magick::ImageMagickError => e
      raise InvalidFormat.new("Invalid image format. Please check the file uploaded.")
    end
    if img.nil?
      raise InvalidFormat.new("Invalid image. Please check the file uploaded.")
    end
    result_img = img.resize(width, height)

    # save resized image to tmp/uploads
    thumbnail = image.split('.')
    thumbnail_url = "#{thumbnail[0]}_thumb.#{thumbnail[1]}"
    result_img.write(thumbnail_url)
    return thumbnail_url
  end
end

class InvalidFormat < StandardError

end
