module Auditable
  module MakeAuditable
    extend ActiveSupport::Concern

    module ClassMethods
      def make_auditable(options = {})
        if self < ActiveRecord::Base
          send :has_many, :audit_logs, as: :auditee
          send :include, ModelInstanceMethods

          options[:on].each do |action|
            if action.is_a?(Symbol)
              filter_name = "after_#{action}"
              on_fields = nil
            elsif action.is_a?(Hash)
              filter_name = "after_#{action.keys.first}"
              on_fields = action.values.first
            end

            unless on_fields
              self.send("#{filter_name}".to_sym, "_audit_#{filter_name}".to_sym)
            else
              self.send("#{filter_name}".to_sym, "_audit_#{filter_name}".to_sym,
                        if: Proc.new { |obj|
                          on_fields.inject(false) do |flag, field|
                            flag ||= obj.send("#{field}_changed?")
                          end
                        }
              )
            end
          end
          self.send(:after_commit, :_audits_complete)
        elsif self < ApplicationController
          send :include, ControllerInstanceMethods
          send :after_filter, :_audit_after, options
        else
          send :include, ClassInstanceMethods
        end
      end
    end

    module ClassInstanceMethods
      def create_audit(vertical, auditee, event, initiator, activity_info)
        if auditee.is_a?(String)
          audit_log = AuditLog.create(vertical: vertical,
                                      auditee_type: auditee,
                                      event: event,
                                      initiator_type: initiator.class.to_s,
                                      initiator_id: initiator.id,
                                      activity_info: activity_info)
        else
          audit_log = AuditLog.create(vertical: vertical,
                          auditee: auditee,
                          event: event,
                          initiator_type: initiator.class.to_s,
                          initiator_id: initiator.id,
                          activity_info: activity_info)
        end
        audit_log
      end
    end

    module ModelInstanceMethods

      def create_audit(vertical, event, initiator, activity_info)
        unless @_audit_log
          @_audit_log = self.audit_logs.create(vertical: vertical,
                                               event: event,
                                               initiator_type: initiator.class.to_s,
                                               initiator_id: initiator.id,
                                               activity_info: activity_info)
        end
        @_audit_log
      end

      def audit_initiator(initiator)
        @_initiator = initiator
      end

      def audit_vertical(vertical)
        @_vertical = vertical
      end

      private

      def _create_audit(vertical, event, initiator, activity_info)
        begin
          create_audit(vertical, event, initiator, activity_info)
        rescue Exception => e
          Airbrake.notify(e)
          Rails.logger.debug("Exception in Audit Logging for #{event}: Message: #{e.message}\n\n Backtrace: #{e.backtrace}")
        end
      end

      def get_vertical
        defined?(self.vertical) ? self.vertical : @_vertical
      end

      def get_initiator
        @_initiator
      end

      def _audit_after_create
        _create_audit(get_vertical, "create", get_initiator, self.as_json)
      end

      def _audit_after_save
        _create_audit(get_vertical, "save", get_initiator, self.changes)
      end

      def _audit_after_destroy
        _create_audit(get_vertical, "destroy", get_initiator, self.as_json)
      end

      def _audits_complete
        @_audit_log = nil
      end
    end

    module ControllerInstanceMethods

      def create_audit
        if is_auditee_string?
          @_audit_log = AuditLog.create(vertical: get_vertical,
                                      event: get_event,
                                      auditee_type: get_auditee,
                                      initiator_type: get_initiator.class.to_s,
                                      initiator_id: get_initiator.id,
                                      activity_info: get_activity_info)
        else
          @_audit_log = AuditLog.create(vertical: get_vertical,
                                      event: get_event,
                                      auditee: get_auditee,
                                      initiator_type: get_initiator.class.to_s,
                                      initiator_id: get_initiator.id,
                                      activity_info: get_activity_info)
        end
        @_audit_log
      end

      def auditee(auditee)
        @_auditee = auditee
      end

      def audit_vertical(vertical)
        @_vertical = vertical
      end

      def audit_initiator(initiator)
        @_initiator = initiator
      end

      def audit_response(response)
        @_resp = response
      end

      def audit_event(event)
        @_event = event
      end

      private

      def _create_audit
        begin
          create_audit
        rescue Exception => e
          Airbrake.notify(e)
          Rails.logger.debug("Exception in Audit Logging for #{params[:controller]}/#{params[:action]}: Message: #{e.message}\n\n Backtrace: #{e.backtrace}")
        end
      end

      def _audit_after
        _create_audit unless @_audit_log
      end

      def get_vertical
        @_vertical || Vertical.find_by_name(@current_vertical.camelize)
      end

      def get_auditee
        @_auditee
      end

      def is_auditee_string?
        return @_auditee.is_a?(String)
      end

      def get_initiator
        @_initiator || @current_user
      end

      def get_response
        (@_resp || {}).reverse_merge(create_response)
      end

      def get_event
        @_event || params[:action]
      end

      def get_activity_info
        {:response => get_response, :request => params}
      end

      def create_response
        return {:status => response.status, :content_type => response.content_type}
      end

    end

  end
end

ActiveRecord::Base.send :include, Auditable::MakeAuditable